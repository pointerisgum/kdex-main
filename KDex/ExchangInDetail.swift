//
//  ExchangInDetail.swift
//  crevolc
//
//  Created by crovolc on 2018. 1. 2..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//
//

import Alamofire
import UIKit
import Foundation
import WebKit

enum TickSzieType : Int {
    case A = 0// 1
    case B = 1// 0.1
    case C = 2// 0.01
}

class ExchangInDetail : UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, RealTimeDelegate, WKUIDelegate, WKNavigationDelegate, WKScriptMessageHandler{
    
    var activityIndicator : UIActivityIndicatorView = UIActivityIndicatorView()
    
    //주후 in,out page 통합하기위한 리펙토리 변수
    //var pageName : ExchageInOutSimbol = .In
    
    var webView : WKWebView!
    
    var coinSimbol : String? // 코인 전체 아이디
    
    var coinName : String? // 코인 이름
    
    var dataJsondic : [String : Any]? // 데이타시세 값
    
    var jsonArray : [[String : Any]]? //매칭 데이터 json array 값

    var jsondic : NSMutableDictionary? //매칭 데이터 json 값 파싱
    
    var buyAndSell = ExchageInOutSimbol.In
    
    var lastprice : String? // 시가
    
    var openprice : String?
    
    var marksimbol : String? // 마켓 심볼
    
    var simbole : String? // 코인 심볼
    
    var buyActionValue : String?
    
    var currentPrice = 0.0
    
    var tickType : TickSzieType = .A
    
//    var realTimeBool : Bool = false
    
    //var inputvalue = ExchangInput()
    
    var selljsonvalueArray : NSArray?
    
    var buyjsonvalueArray : NSArray?
    
    var textvaluedictionry : [String : Any]?
    
    var bid : Array<Any>?
    
    var ask : Array<Any>?
    
    var tickSice : String?
    
    var askFee = 0.0 //매도전송수수료
    
    var bidFee = 0.0 //매수전송수수료
    
    var krwFee = 0.0
    
    var etherfee = 0.0
    
    var etherAbleValue = 0.0
    
    //상단 메뉴 추가
    
    @IBOutlet var setupView: UIView!
    
    @IBOutlet var totalCoinCountLabel: UILabel! // 보유코인
    
    @IBOutlet var buyAndSellBtn: UIButton! // 매수매도 버튼
    
    @IBOutlet var priceTf: UITextField! // 가격 텍스트필드
    
    @IBOutlet var quantityTf: UITextField! // 수량 텍스트 필드
    
    @IBOutlet var exIntable: UITableView! // 호가테이블
    
    @IBOutlet var orderAbleAmountLabel: UILabel! //주문 가능 수량
    
    @IBOutlet var feeRateLabel: UILabel! // 수수료
    
    @IBOutlet var minPriceLabel: UILabel! // 최소 주문 금액
    
    @IBOutlet var orderAmountLabel: UILabel! //주문 코인의 총액
    
    @IBOutlet var orderAbleLabel: UILabel! // 매수매도가능 라벨
    
    @IBOutlet var orderAmountLb: UILabel! // 매수매도총액 라벨
    
    @IBOutlet var oerderAmountKRWLabel: UILabel! // 주문 코인의 총액_KRW
    
    @IBOutlet var sendFeeLabel: UILabel! //전송수수료라벨
    
    @IBOutlet var sendETHAbleLabel: UILabel!
    //전송가능수량
    
    @IBOutlet var sendFeeValueLabel: UILabel!
    //전송수수료값
    
    @IBOutlet var sendEthAbleValue: UILabel!
    //전송수량값
    
    //주문수량 체크 버튼
    @IBAction func segmentSelecterBtn(_ sender: UISegmentedControl) {
        
//        let doubleOrder = stringtoDouble(str: self.strMarkSlice(str: orderAmountLabel.text ?? "0")) ?? 0.0
        let orderableprc = stringtoDouble(str: self.strMarkSlice(str: orderAbleAmountLabel.text ?? "0")) ?? 0.0 //주문 코인의 총액
        let minprc = stringtoDouble(str: self.strMarkSlice(str: minPriceLabel.text ?? "0")) ?? 0.0
        
//        guard let minprc = self.stringtoDouble(str: self.minPriceLabel.text!) else {
//            return
//        }
//
        
        guard var lastprc = self.stringtoDouble(str: self.lastprice!) else{
            return
        }
        
//        guard let orderableprc = self.stringtoDouble(str: self.orderAbleAmountLabel.text!) else {
//            return
//        }
        
        print("exchange text value : \(minprc), \(lastprc), \(orderableprc)")
        
        let index = sender.selectedSegmentIndex
       
//        if lastprc == 0 {
//            lastprc = 1.0
//        }
        
        let price_lastprc = stringtoDouble(str: priceTf.text ?? "0") ?? 0
    
        
        if price_lastprc != 0 {
            
            if buyAndSell == .In {
                //매수
                switch index {
                case 0:
//                    if orderableprc > minprc {
                        let askQty = minprc / price_lastprc
                        
                        let strQty = String(format: "%0.3f", askQty)
                        var doubleStrQty = stringtoDouble(str: strQty) ?? 0
                        if minprc > (doubleStrQty * price_lastprc){
                            doubleStrQty = doubleStrQty + 0.001
                            self.quantityTf.text = "\(String(format: "%0.3f", doubleStrQty))"
                        }else {
                            self.quantityTf.text = "\(String(format: "%0.3f", askQty))"
                        }
//                    }else{
//                        self.quantityTf.text = "0.0"
//                    }
                case 1:
                    let askQty = (orderableprc * 0.25) / price_lastprc
                    self.quantityTf.text = "\(String(format: "%0.3f", askQty))"
                case 2:
                    let askQty = (orderableprc * 0.5) / price_lastprc
                    self.quantityTf.text = "\(String(format: "%0.3f", askQty))"
                case 3:
                    
                    let askQty = orderableprc / price_lastprc
                    var feevalue = 0.0
                    
                    if self.marksimbol! == "KRW"{
                        
                        feevalue = askQty * (self.krwFee / 100)
                        
                        print("krw fee : \(self.krwFee), krw feevalue : \(feevalue)")
                        
                    }else {
                        
                        feevalue = self.bidFee
                        
                    }
                    
                    let strQty = String(format: "%0.3f", askQty)
                    var doubleStrQty = stringtoDouble(str: strQty) ?? 0
                    
                    if orderableprc < (doubleStrQty * price_lastprc){
                        doubleStrQty = doubleStrQty - 0.001
                        
                        let value = floor((doubleStrQty - feevalue)*1000)/1000
                        self.quantityTf.text = "\(String(format: "%0.3f", value))"
                    }else {
                        
                        let value = floor((doubleStrQty - feevalue)*1000)/1000
                        self.quantityTf.text = "\(String(format: "%0.3f", value))"
                    }
                default:
                    print("error")
                }
                
            }else {
                //매도
                switch index {
                case 0:
//                    if (orderableprc * price_lastprc) > minprc{
                        let askQty = minprc / price_lastprc
                        print("min : \(minprc), \(lastprc)")
                        
                        let strQty = String(format: "%0.3f", askQty)
                        var doubleStrQty = stringtoDouble(str: strQty) ?? 0
                        if minprc > (doubleStrQty * price_lastprc){
                            doubleStrQty = doubleStrQty + 0.001
                            self.quantityTf.text = "\(String(format: "%0.3f", doubleStrQty))"
                        }else {
                            self.quantityTf.text = "\(String(format: "%0.3f", askQty))"
                        }
//                    }else{
//                        self.quantityTf.text = "0.0"
//                    }
                case 1:
                    let askQty = orderableprc / 4
                    self.quantityTf.text = "\(String(format: "%0.3f", askQty))"
                case 2:
                    let askQty = orderableprc / 2
                    self.quantityTf.text = "\(String(format: "%0.3f", askQty))"
                case 3:
                    
                    let askQty = orderableprc
                    var feevalue = 0.0
                    
                    feevalue = self.askFee
                    
                    if self.simbole == "KDA" || self.simbole == "DPN" || self.simbole == "FNK" || self.simbole == "MCVW" || self.simbole == "BTR"{
                        
                        self.quantityTf.text = "\(String(format: "%0.3f", orderableprc))"
                        
                    }else {
                        
                        let strQty = String(format: "%0.3f", askQty)
                        var doubleStrQty = stringtoDouble(str: strQty) ?? 0
                        if orderableprc < doubleStrQty {
                            doubleStrQty = doubleStrQty - 0.001
                            let value = floor((doubleStrQty - feevalue)*1000)/1000
                            self.quantityTf.text = "\(String(format: "%0.3f", value))"
                        }else {
                            let value = floor((doubleStrQty - feevalue)*1000)/1000
                            self.quantityTf.text = "\(String(format: "%0.3f", value))"
                        }
                        
                    }
                    
                default:
                    print("error")
                }
            }
            
            self.textfieldChageEvent()
            
        }else {
            let msg = "pop_price_input".localized
            let alert = UIAlertController(title: "\(buyAndSell.rawValue)", message: msg, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
            self.present(alert, animated: false, completion: nil)
        }
    
    }
    
    // 가격 상승 버튼
    @IBAction func pricePlusAction(_ sender: UIButton) {
        
        print("---------------pricePlusAction")
        
        if self.priceTf.text == "" {
            self.priceTf.text = "0"
        }
        
        guard let priceSize = stringtoDouble(str: self.priceTf.text!) else {
            return
        }
        
        guard let size = stringtoDouble(str: self.tickSice!) else {
            return
        }
        
        print("-----------pricesize : \(priceSize), size : \(size)")
        
        if self.marksimbol! == "BTC"{
            self.priceTf.text = String(format: "%.8f", priceSize + size)
        }else if self.marksimbol == "ETH"{
            self.priceTf.text = String(format: "%.8f", priceSize + size)
        }else {
        
            let namergi = priceSize.truncatingRemainder(dividingBy: size)
            
            var amount = 0.0
            
            if namergi == 0.0 {
                amount = priceSize
            }else {
                amount = (priceSize - namergi)
            }
            
            print("-----------namergin : \(namergi)")
            
            switch self.tickType {
            case .A :
                self.priceTf.text = String(Int(amount + size)).insertComma
            case .B :
                self.priceTf.text = String(format: "%.1f", priceSize + size).insertComma
            case .C :
                print("-----------ticktype : ")
                self.priceTf.text = String(format: "%.2f", priceSize + size).insertComma
            }
            
        }
       
        self.textfieldChageEvent()
    }
    
    // 가격 하락 버튼
    @IBAction func priceMinusAction(_ sender: Any) {
        
         print("---------------priceMinusAction")
        
        if self.priceTf.text == "" {
            self.priceTf.text = "0"
        }
        
        guard let priceSize = stringtoDouble(str: self.priceTf.text!) else {
            return
        }
        
        guard let size = stringtoDouble(str: self.tickSice!) else {
            return
        }
        
        print("-----------pricesize : \(priceSize), size : \(size)")
        
        if priceSize <= 0 || priceSize < size{
            self.priceTf.text = "0"
        }else{
            if self.marksimbol! == "BTC"{
                self.priceTf.text = String(format: "%.8f", priceSize - size)
            }else if self.marksimbol == "ETH"{
                self.priceTf.text = String(format: "%.8f", priceSize - size)
            }else {
                
                let namergi = priceSize.truncatingRemainder(dividingBy: size)
                var amount = 0.0
                
                if namergi == 0.0 {
                    amount = priceSize - size
                }else {
                    amount = priceSize - namergi
                }
                
                switch self.tickType {
                case .A :
                    self.priceTf.text = String(Int(amount)).insertComma
                case .B :
                    self.priceTf.text = String(format: "%.1f", priceSize - size).insertComma
                case .C :
                    self.priceTf.text = String(format: "%.2f", priceSize - size).insertComma
                }
            }
        }
        
        self.textfieldChageEvent()
    }
    
    // 매수매도 버튼 클릭시 action 메소드
    @IBAction func coinInAction(_ sender: Any) {
        
        let doubleOrder = stringtoDouble(str: self.strMarkSlice(str: orderAmountLabel.text ?? "0")) ?? 0.0 //매도 총액?
        let doubleAmount = stringtoDouble(str: self.strMarkSlice(str: orderAbleAmountLabel.text ?? "0")) ?? 0.0  //주문가능수량
        let minAmount = stringtoDouble(str: self.strMarkSlice(str: minPriceLabel.text ?? "0")) ?? 0.0 //최소주문금액
        let orderAmount = stringtoDouble(str: (quantityTf.text ?? "0")) ?? 0.0 //주문수량
        
        print("buy and sell : \(doubleOrder), \(doubleAmount), \(minAmount)")
        
        var butType = ""
        
        switch buyAndSell {
        case ExchageInOutSimbol.In:
            butType = "fragment_title_bid".localized
        default:
            butType = "fragment_title_ask".localized
        }
        
        print("\(butType)")
        if priceTf.text == ""{
            let msg = "pop_coinPrice_input".localized
            let alert = UIAlertController(title: "\(butType)", message: msg, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
            self.present(alert, animated: false, completion: nil)
        }else if quantityTf.text == "" {
            let msg = "pop_coinVolue_input".localized
            let alert = UIAlertController(title: "\(butType)", message: msg, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
            self.present(alert, animated: false, completion: nil)
        }else if orderAmount > doubleAmount  {
            let msg = "pop_ableVol_add".localized
            let alert = UIAlertController(title: "\(butType)", message: msg, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
            self.present(alert, animated: false, completion: nil)
        }
//        else if doubleOrder > doubleAmount  {
//            let msg = "pop_ableVol_add".localized
//            let alert = UIAlertController(title: "\(butType)", message: msg, preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
//            self.present(alert, animated: false, completion: nil)
//        }
        else if doubleOrder < minAmount {
            
            let msg = "bid_ask_min".localized + " \(butType) " + "pop_ableVol_confirm".localized
            let alert = UIAlertController(title: "\(butType)", message: msg, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
            self.present(alert, animated: false, completion: nil)
            
        }else if self.simbole == "KDA" && self.buyAndSell == .Out {
            
//            if self.etherfee > self.etherAbleValue {
//                let msg = "ETH 수량이 부족합니다. "
//                let alert = UIAlertController(title: "\(butType)", message: msg, preferredStyle: .alert)
//                alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
//                self.present(alert, animated: false, completion: nil)
//            }else {
                
                let msg = "pop_trande_current".localized + " \(butType) " + "pop_trande_doit".localized
                let alert = UIAlertController(title: "\(butType)", message: msg, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "common_no_btn".localized, style: .cancel, handler: nil))
                alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .destructive, handler: { (_) in
                    self.buyAction()
                }))
                self.present(alert, animated: false, completion: nil)
                
//            }
            
        }else if self.simbole == "DPN" && self.buyAndSell == .Out {
            
//            if self.etherfee > self.etherAbleValue {
//                let msg = "ETH 수량이 부족합니다. "
//                let alert = UIAlertController(title: "\(butType)", message: msg, preferredStyle: .alert)
//                alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
//                self.present(alert, animated: false, completion: nil)
//            }else {
                
                let msg = "pop_trande_current".localized + " \(butType) " + "pop_trande_doit".localized
                let alert = UIAlertController(title: "\(butType)", message: msg, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "common_no_btn".localized, style: .cancel, handler: nil))
                alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .destructive, handler: { (_) in
                    self.buyAction()
                }))
                self.present(alert, animated: false, completion: nil)
                
//            }
            
        }else if self.simbole == "FNK" && self.buyAndSell == .Out {
            
//            if self.etherfee > self.etherAbleValue {
//                let msg = "ETH 수량이 부족합니다. "
//                let alert = UIAlertController(title: "\(butType)", message: msg, preferredStyle: .alert)
//                alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
//                self.present(alert, animated: false, completion: nil)
//            }else {
                
                let msg = "pop_trande_current".localized + " \(butType) " + "pop_trande_doit".localized
                let alert = UIAlertController(title: "\(butType)", message: msg, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "common_no_btn".localized, style: .cancel, handler: nil))
                alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .destructive, handler: { (_) in
                    self.buyAction()
                }))
                self.present(alert, animated: false, completion: nil)
                
//            }
            
        }else if self.simbole == "MCVW" && self.buyAndSell == .Out {
            
//            if self.etherfee > self.etherAbleValue {
//                let msg = "ETH 수량이 부족합니다. "
//                let alert = UIAlertController(title: "\(butType)", message: msg, preferredStyle: .alert)
//                alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
//                self.present(alert, animated: false, completion: nil)
//            }else {
//
                let msg = "pop_trande_current".localized + " \(butType) " + "pop_trande_doit".localized
                let alert = UIAlertController(title: "\(butType)", message: msg, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "common_no_btn".localized, style: .cancel, handler: nil))
                alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .destructive, handler: { (_) in
                    self.buyAction()
                }))
                self.present(alert, animated: false, completion: nil)
                
//            }
            
        }else if self.simbole == "BTR" && self.buyAndSell == .Out {
            
//            if self.etherfee > self.etherAbleValue {
//                let msg = "ETH 수량이 부족합니다. "
//                let alert = UIAlertController(title: "\(butType)", message: msg, preferredStyle: .alert)
//                alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
//                self.present(alert, animated: false, completion: nil)
//            }else {
                
                let msg = "pop_trande_current".localized + " \(butType) " + "pop_trande_doit".localized
                let alert = UIAlertController(title: "\(butType)", message: msg, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "common_no_btn".localized, style: .cancel, handler: nil))
                alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .destructive, handler: { (_) in
                    self.buyAction()
                }))
                self.present(alert, animated: false, completion: nil)
                
//            }
            
        }else{
            
            let tempBool = self.diffPriceAction(curValue: self.lastprice ?? "", dffValue: priceTf.text ?? "")
            
            if tempBool{
                let msg = "[주의] 현재가의 30% 넘는 주문가격입니다.계속 주문 하시겠습니까?"
                let alert = UIAlertController(title: "\(butType)", message: msg, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "common_no_btn".localized, style: .cancel, handler: nil))
                alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .destructive, handler: { (_) in
                    self.buyAction()
                }))
                self.present(alert, animated: false, completion: nil)
                
            }else {
                
                let msg = "pop_trande_current".localized + " \(butType) " + "pop_trande_doit".localized
                let alert = UIAlertController(title: "\(butType)", message: msg, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "common_no_btn".localized, style: .cancel, handler: nil))
                alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .destructive, handler: { (_) in
                    self.buyAction()
                }))
                self.present(alert, animated: false, completion: nil)
            }
            
            
        }
    }
    
    let tfView : UIView = {
        let vi = UIView()
        vi.backgroundColor = UIColor.clear
        vi.frame = CGRect(x: 0, y: 0, width: 4, height: 35)
        return vi
    }()
    
    let quatfView : UIView = {
        let vi = UIView()
        vi.backgroundColor = UIColor.clear
        vi.frame = CGRect(x: 0, y: 0, width: 4, height: 35)
        return vi
    }()
    
    override func loadView() {
        super.loadView()
        
        webView = WKWebView()
        webView.backgroundColor = UIColor.DPViewBackgroundColor
        webView.translatesAutoresizingMaskIntoConstraints = false
        
        webView.uiDelegate = self
        webView.navigationDelegate = self
        
        self.view.addSubview( self.webView!)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sendFeeValue(simbol: self.simbole ?? "", type: "ask" ) //매수
        sendFeeValue(simbol: self.marksimbol ?? "", type: "bid") //매도
        
        print("coinsimbole : \(coinSimbol), marketsimbole: \(marksimbol), simbole: \(simbole), coinname : \(coinName)" )
        
        initSetup()
        webViewSetup()
        textfieldViewSetup()
//        chatMoving()
//        userAmount()
        delegateSetup()
        textfieldsAddtarget()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {

        TcpSocket.sharedInstance.receiverThread?.delegate = self //실시간 알림 델리게이트
        
        initSetup()
        chatMoving()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.priceTf.text = ""
        self.quantityTf.text = ""
        
    }
    
    func initSetup(){
        
        switch buyAndSell {
        case ExchageInOutSimbol.In:
            print("buyandsell : \(buyAndSell.rawValue)")
            
            buyAndSellBtn.setTitle("fragment_title_bid".localized, for: .normal)
            buyAndSellBtn.backgroundColor = UIColor.DPBuyButtonColor
            orderAmountLb.text = "bid_ask_bid_total".localized
//            orderAbleLabel.text = "bid_ask_total_amount".localized
            orderAbleLabel.text = "bid_ask_bid_available".localized
            buyActionValue = "B"
            
            if self.marksimbol! == "KRW" {
                self.sendETHAbleLabel.isHidden = true
                self.sendEthAbleValue.isHidden = true
                self.sendFeeLabel.isHidden = true
                self.sendFeeValueLabel.isHidden = true
                
//                if self.simbole! == "KDA" {
//
//                    self.sendFeeLabel.isHidden = false
//                    self.sendFeeValueLabel.isHidden = false
//
//                }
                
            }else {
                
                self.sendETHAbleLabel.isHidden = true
                self.sendEthAbleValue.isHidden = true
                self.sendFeeLabel.isHidden = false
                self.sendFeeValueLabel.isHidden = false
                
            }
            
            
        default:
            buyAndSellBtn.setTitle("fragment_title_ask".localized, for: .normal)
            buyAndSellBtn.backgroundColor = UIColor.DPSellButtonColor
            orderAmountLb.text = "bid_ask_ask_total".localized
//            orderAbleLabel.text = "bid_ask_total_amount".localized
            orderAbleLabel.text = "bid_ask_ask_available".localized
            buyActionValue = "S"
            
            if self.marksimbol! == "KRW" {
                self.sendETHAbleLabel.isHidden = true
                self.sendEthAbleValue.isHidden = true
                self.sendFeeLabel.isHidden = false
                self.sendFeeValueLabel.isHidden = false
                
                if self.simbole! == "KDA" || self.simbole! == "DPN" || self.simbole! == "FNK" || self.simbole == "MCVW" || self.simbole == "BTR" {
                    self.sendETHAbleLabel.isHidden = false
                    self.sendEthAbleValue.isHidden = false
                }
                
            }else {
                
                self.sendETHAbleLabel.isHidden = true
                self.sendEthAbleValue.isHidden = true
                self.sendFeeLabel.isHidden = false
                self.sendFeeValueLabel.isHidden = false
                
            }
            
        }
        
        oerderAmountKRWLabel.textColor = UIColor.DPsubTextColor
        oerderAmountKRWLabel.font = UIFont.systemFont(ofSize: 12)
        
        if self.marksimbol! == "BTC" {
//            self.orderAmountLabel.text = "0 BTC"
            self.orderAmountLabel.attributedText = self.labelAttributen(str: "0", sim: self.marksimbol!)
            self.oerderAmountKRWLabel.text = "0 KRW"
            
        }else if self.marksimbol! == "ETH" {
//            self.orderAmountLabel.text = "0 ETH"
            self.orderAmountLabel.attributedText = self.labelAttributen(str: "0", sim: self.marksimbol!)
            self.oerderAmountKRWLabel.text = "0 KRW"
            
        }else {
            
            self.orderAmountLabel.text = "0 KRW"
            self.orderAmountLabel.attributedText = self.labelAttributen(str: "0", sim: "KRW")
            self.oerderAmountKRWLabel.isHidden = true
            
        }
        
        self.exIntable.backgroundColor = UIColor.DPtableViewBgColor
        self.exIntable.layoutMargins = UIEdgeInsets.zero
        self.exIntable.separatorInset = UIEdgeInsets.zero
        self.view.backgroundColor = UIColor.DPViewBackgroundColor
        self.navigationController?.addShadowToBar(vi: self)
        
    }
    
    func delegateSetup() {
        
        exIntable.delegate = self
        exIntable.dataSource = self
        priceTf.delegate = self
        quantityTf.delegate = self
        
    }
    
    func textfieldViewSetup(){
        
        self.priceTf.leftView = tfView
        self.priceTf.leftViewMode = .always
        self.priceTf.attributedPlaceholder = NSAttributedString(string: "bid_ask_price".localized, attributes: [NSAttributedString.Key.foregroundColor : UIColor.DPmainTextColor])
        self.priceTf.backgroundColor = UIColor.DPTextFieldBgColor
        self.priceTf.addBorderBottom(height: 1.0, width: self.priceTf.frame.width , color: UIColor.DPLineBgColor)
        textfiledView(simName: self.marksimbol!, textFi: self.priceTf, rigthMode: true)
        
        self.quantityTf.leftView = quatfView
        self.quantityTf.leftViewMode = .always
        self.quantityTf.attributedPlaceholder = NSAttributedString(string: "bid_ask_qty".localized, attributes: [NSAttributedString.Key.foregroundColor : UIColor.DPmainTextColor])
        self.quantityTf.backgroundColor = UIColor.DPTextFieldBgColor
        self.quantityTf.addBorderBottom(height: 1.0, width: self.quantityTf.frame.width, color: UIColor.DPLineBgColor)
        textfiledView(simName: self.simbole!, textFi: self.quantityTf, rigthMode: true)
    }
    
    func textfieldsAddtarget(){
        
        self.priceTf.addTarget(self, action: #selector(textfieldChageEvent), for: .editingChanged)
        self.quantityTf.addTarget(self, action: #selector(textfieldChageEvent), for: .editingChanged)
        
    }
    
    func webViewSetup(){
        
        webView.topAnchor.constraint(equalTo: self.setupView.bottomAnchor, constant: 5).isActive = true
        webView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -8).isActive = true
        webView.leadingAnchor.constraint(equalTo: self.exIntable.trailingAnchor, constant: 8).isActive = true
        webView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -8).isActive = true
        
        var tempSimbole = ""
        
        if (coinSimbol?.contains("_"))! {
            tempSimbole = coinSimbol ?? ""
        }else {
            tempSimbole = "\(coinSimbol ?? "")_KRW"
        }
        
        let myUrl = "\(AppDelegate.url)mobile/chartDepth?simbol=\(tempSimbole)"
        
        print("small chart myURL : \(myUrl)")
        
        let url = URL(string: myUrl)
        
        let request = URLRequest(url: url!)
        
        webView.load(request)
        
    }
    
    //호가 및 코인 정보 호출 메소드
    func chatMoving(){
        
        print("tableview chatMoving ")
        
        print("coinsimbol : \(self.coinSimbol)")
        
        
        let urlComponents = URLComponents(string: "\(AppDelegate.url)/app/orderList?simbol=\(self.coinSimbol!)")!
        
        print("exch orderList : \(AppDelegate.url)/app/orderList?simbol=\(self.coinSimbol!)")

        let amo = Alamofire.request(urlComponents)
        amo.responseJSON(completionHandler: { response in
            print("\(response.result.value)")
            if let tempJsonObj = response.result.value as? [String : Any]{
                
                self.lastprice = (tempJsonObj["lastPrice"] as? String) ?? "0" //현재 코인 시가
                self.openprice = (tempJsonObj["openPrice"] as? String) ?? "0" //전일 코인 시가
                
                let intLast_price = self.stringtoDouble(str: self.lastprice!) ?? 0
                
//                self.tickSice = (tempJsonObj["tickSize"] as? String) ?? "0" //가격 조정 tick
//                let tempstr = (tempJsonObj["minPrice"] as? String) ?? "0" //최소 주문 수량
                
                 self.userAmount()
                
//                if self.marksimbol! == "BTC" {
//                    self.minPriceLabel.text = "\(tempstr) BTC"
//                } else if self.marksimbol! == "ETH" {
//                    self.minPriceLabel.text = "\(tempstr) ETH"
//                } else {
//                    self.minPriceLabel.text = "\(tempstr) KRW"
//                }
                
                //호가단위 설정
                if self.marksimbol! == "BTC" || self.marksimbol == "ETH"{
                    self.priceTf.text = String(format: "%.8f", self.stringtoDouble(str: self.lastprice!) ?? 0)
                    self.tickSice = "0.00000001"
                    
                }else {
//                    self.priceTf.text = String(format: "%.0f", self.stringtoDouble(str: self.lastprice!) ?? 0).insertComma
                    
                    if intLast_price >= 2000000 {
                        self.tickSice = "1000"
                        self.tickType = .A
                    }else if intLast_price >= 1000000 {
                        self.tickSice = "500"
                        self.tickType = .A
                    }else if intLast_price >= 500000 {
                        self.tickSice = "100"
                        self.tickType = .A
                    }else if intLast_price >= 100000 {
                        self.tickSice = "50"
                        self.tickType = .A
                    }else if intLast_price >= 10000 {
                        self.tickSice = "10"
                        self.tickType = .A
                    }else if intLast_price >= 1000 {
                        self.tickSice = "5"
                        self.tickType = .A
                    }else if intLast_price >= 100 {
                        self.tickSice = "1"
                        self.tickType = .A
                    }else if intLast_price >= 10 {
                        self.tickSice = "0.1"
                        self.tickType = .B
                    }else {
                        self.tickSice = "0.01"
                        self.tickType = .C
                    }
                }
                
                if let datatemp = tempJsonObj["bidInfoList"] as? NSArray{
                    self.buyjsonvalueArray = datatemp
                }
                
                if let datatemp = tempJsonObj["askInfoList"] as? NSArray{
                    self.selljsonvalueArray = datatemp
                }
                
                let doublePriceStr = self.stringtoDouble(str: self.lastprice!) ?? 0.0
                let doubletickSize = self.stringtoDouble(str: self.tickSice!) ?? 0.0
                
                
                let namergi = doublePriceStr.truncatingRemainder(dividingBy: doubletickSize)
                var amount = 0.0
                
                if namergi == 0.0 {
                    amount = doublePriceStr
                }else {
                    amount = (doublePriceStr - namergi)
                }
                
                switch self.tickType {
                case .A :
                    self.priceTf.text = String(format: "%.0f", amount).insertComma
                case .B :
                    self.priceTf.text = String(format: "%.1f", doublePriceStr).insertComma
                case .C :
                    self.priceTf.text = String(format: "%.2f", doublePriceStr).insertComma
                }
                
            }
            
            self.exIntable.reloadData()
            
            let indexPath = NSIndexPath(item: 3 , section: 0)
            self.exIntable?.scrollToRow(at: indexPath as IndexPath , at: .top, animated: false)

        })
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        print("tableview cellforRowat ")
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ex_InOutCell") as? ExchangInOutCell
        cell?.backgroundColor = UIColor.DPtableCellBgColor
        cell?.quarityLabel.textColor = UIColor.DPmainTextColor
        cell?.krwLabel.textColor = UIColor.DPsubTextColor
        cell?.krwLabel.font = UIFont.systemFont(ofSize: 10)
        cell?.layoutMargins = UIEdgeInsets.zero
        
        var doublelastPrice = 0.0
        
        if self.lastprice != nil {
            doublelastPrice = stringtoDouble(str: self.lastprice ?? "0") ?? 0
            
            currentPrice = doublelastPrice
        }
        
        if self.buyjsonvalueArray != nil {
        
        if indexPath.row > 9 {
            
            // 매수
            cell?.backgroundColor = UIColor.DPaskBgColor.withAlphaComponent(0.05)
            
            cell?.grafBar.backgroundColor = UIColor.DPaskBgColor.withAlphaComponent(0.1)
            
            cell?.priceLabel.textColor = UIColor.DPBuyDefaultColor
            
            for temp in self.buyjsonvalueArray! {
                
                var buyjson = temp as? [String : String]
                
                if buyjson!["dealType"] == "bid" && buyjson!["order"] == "\(indexPath.row - 9)" {
                    
                    let doubleprice = stringtoDouble(str: (buyjson?["price"])!)
                    let updnSign = buyjson?["pricePer"] ?? "0"
                    let doubleUpdnSign = stringtoDouble(str: updnSign)
                    
                    if buyjson?["price"] == "0.0"{
//                        cell?.priceLabel.text = "0"
                        cell?.priceLabel.text = String(format: "%.8f", 0.0)
                    }else{
                        
                        if self.marksimbol! == "BTC" || self.marksimbol! == "ETH" {
                            let price = stringtoDouble(str: (buyjson?["price"])!)
                            cell?.priceLabel.text = String(format: "%.8f", price!)
                        }else {
                            cell?.priceLabel.text = buyjson?["price"]?.insertComma
                        }
                    }
                    
                    let tempobj = buyjson?["matchingReady"] ?? ""
                    print("\(tempobj)")
                    
                    
                    let attrConValueText = NSMutableAttributedString()
                    
                    attrConValueText.append(NSAttributedString(string: "\(tempobj)", attributes: [NSAttributedString.Key.foregroundColor :UIColor(rgb: 0x00b9b2)]))
                    
                    
                    if buyjson?["qty"] == "0.0" {
                        attrConValueText.append(NSAttributedString(string: "  0", attributes: [NSAttributedString.Key.foregroundColor :UIColor.DPmainTextColor]))
                        
                        cell?.quarityLabel.attributedText = attrConValueText
//                        cell?.quarityLabel.text = "0"
                    }else {
                        attrConValueText.append(NSAttributedString(string: "  \(buyjson?["qty"] ?? "0")", attributes: [NSAttributedString.Key.foregroundColor: UIColor.DPmainTextColor]))
//                        cell?.quarityLabel.text = buyjson?["qty"]
                        cell?.quarityLabel.attributedText = attrConValueText
                        
                    }
                    
                    if buyjson?["pricePer"] == nil {
                        print("inout real time")
                        print("inout real time row: \(indexPath.row)")
                        let pricePer = self.makePricePer(price: doubleprice!, openprice: self.openprice ?? "0.0")
                        print("inout priceper : \(pricePer)")
                        if pricePer < 0.0 {
                            print("inout priceper : -----")
                            cell?.priceLabel.textColor = UIColor.DPMinTextColor
//                            cell?.quarityLabel.textColor = UIColor.DPMinTextColor
                        }else if pricePer == 0.0 {
                            print("inout priceper : =========")
                            cell?.priceLabel.textColor = UIColor.DPmainTextColor
//                            cell?.quarityLabel.textColor = UIColor.DPmainTextColor
                            
                        }else {
                            print("inout priceper : +++++++")
                            cell?.priceLabel.textColor = UIColor.DPPlusTextColor
//                            cell?.quarityLabel.textColor = UIColor.DPPlusTextColor
                        }

                    }else {
                        if doubleUpdnSign! < 0.0 {
                            cell?.priceLabel.textColor = UIColor.DPMinTextColor
//                            cell?.quarityLabel.textColor = UIColor.DPMinTextColor
                        }else if doubleUpdnSign! == 0.0 {
                            cell?.priceLabel.textColor = UIColor.DPmainTextColor
//                            cell?.quarityLabel.textColor = UIColor.DPmainTextColor
                        }else {
                            cell?.priceLabel.textColor = UIColor.DPPlusTextColor
//                            cell?.quarityLabel.textColor = UIColor.DPPlusTextColor
                        }
                    }
                    
                    if self.marksimbol! == "BTC" {
                        var doubleValue = doubleprice! * AppDelegate.BTC
                        let krwstr = "\(Int(doubleValue.roundToPlaces(places: 0)))"
                        cell?.krwLabel.text = "\(krwstr.insertComma) KRW"
                        
                    }else if self.marksimbol! == "ETH" {
                        var doubleValue = doubleprice! * AppDelegate.ETH
                        let krwstr = "\(Int(doubleValue.roundToPlaces(places: 0)))"
                        cell?.krwLabel.text = "\(krwstr.insertComma) KRW"
                    }else {
//                        cell?.priceLabel.frame.origin.y = (cell?.quarityLabel.frame.origin.y)!
//                        cell?.krwLabel.isHidden = true
                        var doubleValue = stringtoDouble(str: buyjson?["pricePer"] as? String ?? "0.0")
                        let krwstr = doubleValue?.roundToPlaces(places: 2) ?? 0.00
                        let tempstr = "\(krwstr)".insertComma
                        cell?.krwLabel.text = "\(tempstr) %"
                    }
                    
                    
                    
                    let per = CGFloat(((buyjson?["percent"] as? NSString)?.floatValue)!)
                    
                    if per.isEqual(to: 0){
                        cell?.grafBar.frame.origin.x = (cell?.frame.width)!
                    } else if per.isEqual(to: 100) {
                        cell?.grafBar.frame.origin.x = 0
                    } else {
                        let graper : CGFloat = (((cell?.frame.width)! / 100) * per)
                        cell?.grafBar.frame.origin.x = (cell?.frame.width)! - graper
                    }
                    
                    
                    if doubleprice == doublelastPrice && doublelastPrice != 0 {
                        print("test doubleprince\(doublelastPrice)")
                        print("test doublelastPrice\(doublelastPrice)")
                        print("test bid : \(indexPath.row)")

                        cell?.layer.borderColor = UIColor.black.cgColor
                        cell?.layer.borderWidth = 1

                    }else {
                        
                        cell?.layer.borderWidth = 0
                    }

                }
                
            }
            
        }
        
        }
            
        if self.selljsonvalueArray != nil {
        if indexPath.row < 10 {
            //매도
            var num = 0
            cell?.backgroundColor = UIColor.DPbdidBgColor.withAlphaComponent(0.05)
            
            cell?.grafBar.backgroundColor = UIColor.DPbdidBgColor.withAlphaComponent(0.1)
            
            cell?.priceLabel.textColor = UIColor.DPSellDefaultColor
            
            if indexPath.row == 0 {
                num = 10 + indexPath.row
            }else{
                num = 10 - indexPath.row
            }
            
            for temp in self.selljsonvalueArray! {
                
                var askjson = temp as? [String : String]
                
                if askjson!["dealType"] == "ask" && askjson!["order"] == "\(num)" {
                    
                    let doubleprice = stringtoDouble(str: (askjson?["price"])!)
                    let updnSign = askjson?["pricePer"] ?? "0"
                    let doubleUpdnSign = stringtoDouble(str: updnSign)
                    
                    if askjson?["price"] == "0.0"{
//                        cell?.priceLabel.text = "0"
                        cell?.priceLabel.text = String(format: "%.8f", 0.0)
                    }else{
                        if self.marksimbol! == "BTC" || self.marksimbol! == "ETH" {
                            let price = stringtoDouble(str: (askjson?["price"])!)
                            cell?.priceLabel.text = String(format: "%.8f", price!)
                        }else {
                            cell?.priceLabel.text = askjson?["price"]?.insertComma
                        }
                    }
                    
                    let tempobj = askjson?["matchingReady"] ?? ""
                    print("\(tempobj)")
                    
                    let attrConValueText = NSMutableAttributedString()
                    
                    attrConValueText.append(NSAttributedString(string: "\(tempobj)", attributes: [NSAttributedString.Key.foregroundColor: UIColor(rgb: 0x00b9b2)]))
                    
                    
                    if askjson?["qty"] == "0.0" {
                        attrConValueText.append(NSAttributedString(string: "  0", attributes: [NSAttributedString.Key.foregroundColor: UIColor.DPmainTextColor]))
                        
                        cell?.quarityLabel.attributedText = attrConValueText
                        //                        cell?.quarityLabel.text = "0"
                    }else {
                        attrConValueText.append(NSAttributedString(string: "  \(askjson?["qty"] ?? "0")", attributes: [NSAttributedString.Key.foregroundColor: UIColor.DPmainTextColor]))
                        //                        cell?.quarityLabel.text = buyjson?["qty"]
                        cell?.quarityLabel.attributedText = attrConValueText
                        
                    }
                    
                    if askjson?["pricePer"] == nil {
                        print("inout real time")
                        let pricePer = self.makePricePer(price: doubleprice!, openprice: self.openprice ?? "0.0")
                        print("inout priceper ask: \(pricePer)")
                        if pricePer < 0.0 {
                            print("inout priceper : ----------------")
                            cell?.priceLabel.textColor = UIColor.DPMinTextColor
//                            cell?.quarityLabel.textColor = UIColor.DPMinTextColor
                        }else if pricePer == 0.0 {
                            print("inout priceper : ===============")
                            cell?.priceLabel.textColor = UIColor.DPmainTextColor
//                            cell?.quarityLabel.textColor = UIColor.DPmainTextColor
                        }else {
                            print("inout priceper : ++++++++++++++++")
                            cell?.priceLabel.textColor = UIColor.DPPlusTextColor
//                            cell?.quarityLabel.textColor = UIColor.DPPlusTextColor
                        }
                        
                    }else {
                        print("inout pr")
                        if doubleUpdnSign! < 0.0 {
                            cell?.priceLabel.textColor = UIColor.DPMinTextColor
//                            cell?.quarityLabel.textColor = UIColor.DPMinTextColor
                        }else if doubleUpdnSign! == 0.0 {
                            cell?.priceLabel.textColor = UIColor.DPmainTextColor
//                            cell?.quarityLabel.textColor = UIColor.DPmainTextColor
                        }else {
                            cell?.priceLabel.textColor = UIColor.DPPlusTextColor
//                            cell?.quarityLabel.textColor = UIColor.DPPlusTextColor
                        }
                    }
                    
//                    if doubleUpdnSign! < 0.0 {
//                        cell?.priceLabel.textColor = UIColor.DPMinTextColor
//                        cell?.quarityLabel.textColor = UIColor.DPMinTextColor
//                    }else if doubleUpdnSign! == 0.0 {
//                        cell?.priceLabel.textColor = UIColor.DPmainTextColor
//                        cell?.quarityLabel.textColor = UIColor.DPmainTextColor
//                    }else {
//                        cell?.priceLabel.textColor = UIColor.DPPlusTextColor
//                        cell?.quarityLabel.textColor = UIColor.DPPlusTextColor
//                    }
                    
                    if self.marksimbol! == "BTC" {
                        var doubleValue = doubleprice! * AppDelegate.BTC
                         let krwstr = "\(Int(doubleValue.roundToPlaces(places: 0)))"
                        cell?.krwLabel.text = "\(krwstr.insertComma) KRW"
                    }else if self.marksimbol! == "ETH" {
                        var doubleValue = doubleprice! * AppDelegate.ETH
                        let krwstr = "\(Int(doubleValue.roundToPlaces(places: 0)))"
                        cell?.krwLabel.text = "\(krwstr.insertComma) KRW"
                    }else {
//                        cell?.priceLabel.frame.origin.y = (cell?.quarityLabel.frame.origin.y)!
//                        cell?.krwLabel.isHidden = true
                        var doubleValue = stringtoDouble(str: askjson?["pricePer"] as? String ?? "0.0")
                        let krwstr = doubleValue?.roundToPlaces(places: 2) ?? 0.00
                        let tempstr = "\(krwstr)".insertComma
                        cell?.krwLabel.text = "\(tempstr) %"
                        
                    }
                    
                    let per = CGFloat(((askjson?["percent"] as? NSString)?.floatValue)!)
                    
                    if per.isEqual(to: 0){
                        cell?.grafBar.frame.origin.x = (cell?.frame.width)!
                    } else if per.isEqual(to: 100) {
                        cell?.grafBar.frame.origin.x = 0
                    } else {
                        let graper : CGFloat = (((cell?.frame.width)! / 100) * per)
                        cell?.grafBar.frame.origin.x = (cell?.frame.width)! - graper
                    }
                    
                    if doubleprice == doublelastPrice && doublelastPrice != 0 {
                        print("test doubleprince\(doublelastPrice)")
                        print("test doublelastPrice\(doublelastPrice)")
                        print("test ask : \(indexPath.row)")

                        cell?.layer.borderColor = UIColor.black.cgColor
                        cell?.layer.borderWidth = 1

                    }else {
                        
                        cell?.layer.borderWidth = 0
                    }
                    
                }
                
            }
    
        }

        }
        
        cell?.selectionStyle = .none
        return cell!
    }
 
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 41
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        
        if indexPath.row < 10 {
            //매도
            print("ask")
            var num = 0
            
            if indexPath.row == 0 {
                num = 10 + indexPath.row
            }else{
                num = 10 - indexPath.row
            }
            
            for temp in self.selljsonvalueArray! {
                
                var askjson = temp as? [String : String]
                
                if askjson!["dealType"] == "ask" && askjson!["order"] == "\(num)" {
                    
                    let askPrice = askjson?["price"] ?? "0"
                    
                    let doublePrice = stringtoDouble(str: askPrice)
                    let doubleQty = stringtoDouble(str: (askjson?["qty"])!)
                    
                    if askPrice.contains(".") || self.marksimbol! == "BTC" || self.marksimbol! == "ETH" {
                        self.priceTf.text = String(format: "%.8f", doublePrice!)
                    }else {
                        //호가 표기 수정
                        switch self.tickType {
                        case .A :
                            self.priceTf.text = String(format: "%.0f", doublePrice!).insertComma
                        case .B :
                            self.priceTf.text = String(format: "%.1f", doublePrice!).insertComma
                        case .C :
                            self.priceTf.text = String(format: "%.2f", doublePrice!).insertComma
                        }
                        
//                        self.priceTf.text = askPrice.insertComma
                    }
                    
//                    askjson?["price"]
                    //self.quantityTf.text = askjson?["qty"]
                    
                    let amount = doublePrice! * doubleQty!
                    
                    let intnum = Int(amount)
                    
                    if self.marksimbol! == "BTC" {
                        let krwAmount = amount * AppDelegate.BTC
                        let str = "\(Int(krwAmount))".insertComma
//                        self.orderAmountLabel.text = "\(String(format: "%.8f", amount)) BTC"
                        
                        self.orderAmountLabel.attributedText = labelAttributen(str: "\(String(format: "%.8f", amount))", sim: self.marksimbol!)
                        self.oerderAmountKRWLabel.text = "\(str) KRW"
                    }else if self.marksimbol! == "ETH" {
                        let krwAmount = amount * AppDelegate.ETH
                        let str = "\(Int(krwAmount))".insertComma
//                        self.orderAmountLabel.text = "\(String(format: "%.8f", amount)) ETH"
                        
                        self.orderAmountLabel.attributedText = labelAttributen(str: "\(String(format: "%.8f", amount))", sim: self.marksimbol!)
                        self.oerderAmountKRWLabel.text = "\(str) KRW"
                    }else {
                        let str = "\(intnum)".insertComma
//                        self.orderAmountLabel.text = "\(str) KRW"
                        
                        self.orderAmountLabel.attributedText = labelAttributen(str: "\(str)", sim: "KRW")
                        self.oerderAmountKRWLabel.isHidden = true
                    }
                }
            }
        }else{
            print("bid")
            for temp in self.buyjsonvalueArray! {
                
                var bidjson = temp as? [String : String]
                
                if bidjson!["dealType"] == "bid" && bidjson!["order"] == "\(indexPath.row - 9)" {
                    
                    let bidprice = bidjson?["price"] ?? "0"
                    
                    let doublePrice = stringtoDouble(str: bidprice)
                    let doubleQty = stringtoDouble(str: (bidjson?["qty"])!)
                    
                    if bidprice.contains(".")  || self.marksimbol! == "BTC" || self.marksimbol! == "ETH" {
                        self.priceTf.text = String(format: "%.8f", doublePrice!)
                    }else{
                        //호가 표기 수정
                        switch self.tickType {
                        case .A :
                            self.priceTf.text = String(format: "%.0f", doublePrice!).insertComma
                        case .B :
                            self.priceTf.text = String(format: "%.1f", doublePrice!).insertComma
                        case .C :
                            self.priceTf.text = String(format: "%.2f", doublePrice!).insertComma
                        }
                        
//                        self.priceTf.text = bidprice.insertComma
                    }
                    
                    
                    //self.quantityTf.text = bidjson?["qty"]
                    
                    let amount = doublePrice! * doubleQty!
                    
                    let intnum = Int(amount)
                    
                    if self.marksimbol! == "BTC" {
                        var krwAmount = amount * AppDelegate.BTC
                        let str = "\(Int(krwAmount.roundToPlaces(places: 0)))".insertComma
//                        self.orderAmountLabel.text = "\(String(format: "%.8f", amount)) BTC"
                        
                        self.orderAmountLabel.attributedText = labelAttributen(str: "\(String(format: "%.8f", amount))", sim: self.marksimbol!)
                        self.oerderAmountKRWLabel.text = "\(str) KRW"
                    }else if self.marksimbol! == "ETH" {
                        var krwAmount = amount * AppDelegate.ETH
                        let str = "\(Int(krwAmount.roundToPlaces(places: 0)))".insertComma
//                        self.orderAmountLabel.text = "\(String(format: "%.8f", amount)) ETH"
                        
                        self.orderAmountLabel.attributedText = labelAttributen(str: "\(String(format: "%.8f", amount))", sim: self.marksimbol!)
                        self.oerderAmountKRWLabel.text = "\(str) KRW"
                    }else {
                        let str = "\(intnum)".insertComma
//                        self.orderAmountLabel.text = "\(str) KRW"
                        
                        self.orderAmountLabel.attributedText = labelAttributen(str: "\(str)", sim: "KRW")
                        self.oerderAmountKRWLabel.isHidden = true

                    }
                    
                }
            }
        }
        
    }
    
    func buyAction() {

        let quanity = self.quantityTf.text?.components(separatedBy: [","]).joined()
        
        let price = self.priceTf.text?.components(separatedBy: [","]).joined()
        
        let url = URL(string: "\(AppDelegate.url)auth/orderAdd")
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let param = "uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&simbol=\(coinSimbol!)&priceType=2&orderType=\(buyActionValue!)&waysType=M&orderQty=\(quanity!)&orderPrice=\(price!)&privKey1&privKey2&saveKeyType"
        
        print("param : \(param)")
        let paramData = param.data(using: .utf8)
        
        request.httpBody = paramData
        
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        request.setValue(String(describing: paramData!.count), forHTTPHeaderField: "Content-Length")
        
        //laucnhing
        let session = URLSession.shared
        
        let task = session.dataTask(with:request as URLRequest) {data, response, error in
            
            if error == nil {

                print("buy error sueccess")
                DispatchQueue.main.async {
                    
                    //activity.stopAnimating()
                    do {
                        let object = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        
                        guard let jsonObject = object else { return }
                        print("inout result msg : \(jsonObject["returnMsg"] ?? "")")
                        
                        let msg = "\(jsonObject["returnMsg"] ?? "")"
                        let alert = UIAlertController(title: "\(self.buyAndSell.rawValue)", message: msg, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: { (_) in
                            self.chatMoving()
//                            self.userAmount()
                            self.view.endEditing(true)
                        }))
                        
                        self.present(alert, animated: false, completion: nil)
                        
                    }catch{
                        print("cCaught an error:\(error)")
                        
                        let msg = "\(self.buyAndSell.rawValue) " + "pop_trande_error".localized
                        let alert = UIAlertController(title: "\(self.buyAndSell.rawValue)", message: msg, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: {(_) in
                            self.view.endEditing(true)
                        }))
                        self.present(alert, animated: false, completion: nil)

                    }
                    
                }
            }else{
                print("error:\(error)")
            }
            
        }
        task.resume()
   
    }
    
    // 매수매도코인 심볼 정보 호출
    func userAmount() {
        print("tableview userAmount ")
        
        let activity = UIActivityIndicatorView()
        activity.style = .whiteLarge
        activity.color = UIColor.DPActivityColor
        activity.hidesWhenStopped = true
        activity.startAnimating()
        
        var url = ""
        
        if marksimbol! == "ETH" || marksimbol! == "BTC"{
            if buyAndSell == .In {
                
                url = "\(AppDelegate.url)/auth/userAbleAmount?simbol=\(self.marksimbol!)&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&simbolMarket=\(self.coinSimbol ?? "")"
                print("wallet url : \(url)")
                
            }else {
                
                url = "\(AppDelegate.url)/auth/userAbleAmount?simbol=\(self.simbole!)&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&simbolMarket=\(self.coinSimbol ?? "")"
                print("wallet url : \(url)")
                
            }
            
        }else {
            if buyAndSell == .In {
                
                url = "\(AppDelegate.url)/auth/userAmount?market=KRW&simbol=\(self.simbole!)&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&simbolMarket=\(self.coinSimbol ?? "")"
                print("wallet url : \(url)")
            }else {
                
                url = "\(AppDelegate.url)/auth/userAbleAmount?simbol=\(self.simbole!)&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&simbolMarket=\(self.coinSimbol ?? "")"
                print("wallet url : \(url)")
            }
            
        }
        
        print("exchange buysell url :\(url)")
        let amo = Alamofire.request(url)
        amo.responseJSON(completionHandler: { response in
            
            if response.result.isSuccess == true {
                
                var buyjson = response.result.value as? [String : Any]
                
                let tempstr = (buyjson?["minPrice"] as? String) ?? "0" //최소 주문 수량
                
                if self.marksimbol == "KRW"{
                    
                    if self.buyAndSell == .In {
                        print("krw simboel ~~~~~~~~ IN ")
                        let tempstr = buyjson?["ordrAbleAmount"] as? String ?? "" //ordrAbleAmount
                        let totalstr = buyjson?["amount"] as? String ?? ""
                        
                        let joinStr = tempstr.components(separatedBy: [","]).joined()
                        let totaljoinStr = totalstr.components(separatedBy: [","]).joined()
                        
                        var doubleStr = self.stringtoDouble(str: joinStr) ?? 0.0
                        var doubletotalStr = self.stringtoDouble(str: totaljoinStr) ?? 0.0
                        
                        let IntStr = Int(doubleStr.roundToPlaces(places: 0))
                        let IntTotalStr = Int(doubletotalStr.roundToPlaces(places: 0))
                            
                        let str = "\(IntStr)".insertComma
                        let sttr = "\(IntTotalStr)".insertComma
        
                        self.orderAbleAmountLabel.text = "\(str) KRW"
                        self.totalCoinCountLabel.text = "\(sttr) KRW"
                        
                    }else{
                        print("krw simboel ~~~~~~~~ OUT")
                        
//                        let tempstr = buyjson?["dpoQty"] as? String ?? ""
                        let tempstr = buyjson?["ableQty"] as? String ?? ""
                        let totalstr = buyjson?["openQty"] as? String ?? ""
                        
                        self.orderAbleAmountLabel.text = "\(String(format: "%.8f", self.stringtoDouble(str: tempstr) ?? 0.0)) \(self.simbole!)"
                        self.totalCoinCountLabel.text = "\(String(format: "%.8f", self.stringtoDouble(str: totalstr) ?? 0.0)) \(self.simbole!)"
                        
                    }
                    
                    self.minPriceLabel.text = "\(tempstr) KRW"
                    
                }else {
                    
                    if self.buyAndSell == .In {
                        let tempstr = buyjson?["ableQty"] as? String ?? ""
                        let totalstr = buyjson?["openQty"] as? String ?? ""
                        
                        self.orderAbleAmountLabel.text = "\(String(format: "%.8f", self.stringtoDouble(str: tempstr) ?? 0.0)) \(self.marksimbol!)"
                        self.totalCoinCountLabel.text = "\(String(format: "%.8f", self.stringtoDouble(str: totalstr) ?? 0.0)) \(self.marksimbol!)"
                    }else{
                        let tempstr = buyjson?["ableQty"] as? String ?? ""
                        let totalstr = buyjson?["openQty"] as? String ?? ""
                        
                        self.orderAbleAmountLabel.text = "\(String(format: "%.8f", self.stringtoDouble(str: tempstr) ?? 0.0)) \(self.simbole!)"
                        self.totalCoinCountLabel.text = "\(String(format: "%.8f", self.stringtoDouble(str: totalstr) ?? 0.0)) \(self.simbole!)"
                    }
                    
                    if self.marksimbol! == "BTC" {
                        self.minPriceLabel.text = "\(tempstr) BTC"
                    } else if self.marksimbol! == "ETH" {
                        self.minPriceLabel.text = "\(tempstr) ETH"
                    }
                }
                
                self.feeRateLabel.text = "\((buyjson?["feeRate"] as? String) ?? "0") %"
                
                self.krwFee = self.stringtoDouble(str: "\((buyjson?["feeRate"] as? String) ?? "0")") ?? 0.0
            }
            
            activity.stopAnimating()
            
        })
    }
    
    func stringChage(value1: String?, value2: String, origin: String) -> String {
        
        var str : String = origin
        
        if value1 == nil {
            str = "\(str) \(value2)"
            
        }else {
            str = "\(value1!) \(str) \(value2)"
        }
        return str
    }
  
    func stringtoDouble(str : String) -> Double?{
        let temp = str.components(separatedBy: [","]).joined()
        let double = Double(temp)
        return double ?? 0.0
    }

    //테스트필드 변경 이벤트
    @objc func textfieldChageEvent() {
        print("tester field changeEvent")
        
        let tempStr = self.quantityTf.text
        
        print("testert : \(tempStr)")
        
        let strIndex = tempStr?.index(of: ".")
        
        if (tempStr?.contains("."))! {
            let countStr = tempStr?.substring(from: (tempStr?.index(after: strIndex!))!)
            
            print("textfield str : \(countStr)")
            print("textfield count : \(countStr?.count)")
            print("alltextfield coutn : \(tempStr)")
            
            if (countStr?.count)! > 3 {
                print("textfield start")
                
                self.quantityTf.text = String(format: "%.3f", stringtoDouble(str: (tempStr?.substring(from: 0, to: ((tempStr?.count)!) - 1))!)! )
            }
        }
        print("testert after : \(self.quantityTf.text)")
        
        let priceStr = self.priceTf.text
        
        let priceIndex = priceStr?.index(of: ".")
        
        
        //호가 자릿수 잡기
        if (priceStr?.contains("."))! {
            let countStr = priceStr?.substring(from: (priceStr?.index(after: priceIndex!))!)
            print("textfield str : \(countStr)")
            print("textfield count : \(countStr?.count)")
            print("alltextfield coutn : \(priceStr)")
            
            if (countStr?.count)! > 8 {
                print("textfield start")
                self.priceTf.text = String(format: "%.8f", stringtoDouble(str: (priceStr?.substring(from: 0, to: ((priceStr?.count)!) - 1))!)!)
            }else {
                print("test price after: \(priceStr)")
                
                if self.marksimbol == "KRW"{
                    switch self.tickType {
                    case .A :
                       self.priceTf.text = String(format: "%.0f", stringtoDouble(str: priceStr!)!).insertComma
                    case .B :
                        print("KRW B type")
                        if (countStr?.count)! > 1 {
                            self.priceTf.text = String(format: "%.1f", stringtoDouble(str: (priceStr?.substring(from: 0, to: ((priceStr?.count)!) - 1))!)!).insertComma
                        }
                    case .C :
                        if (countStr?.count)! > 2 {
                            self.priceTf.text = String(format: "%.2f", stringtoDouble(str: (priceStr?.substring(from: 0, to: ((priceStr?.count)!) - 1))!)!).insertComma
                        }
                    }
                }
            }
        }else {
            if self.marksimbol == "KRW"{
                
            let doublePriceStr = stringtoDouble(str: priceStr!) ?? 0.0
            let doubletickSize = stringtoDouble(str: self.tickSice!) ?? 0.0
            let namergi = doublePriceStr.truncatingRemainder(dividingBy: doubletickSize)
                
                print("krw price : \(doublePriceStr), befor : \(self.tickSice)size : \(doubletickSize), namergi : \(namergi)")
                
                if !(self.tickSice?.contains("."))! {
                     self.priceTf.text = String(format: "%.0f", doublePriceStr).insertComma
                    
                    
// 입력시 호가처리
//                    if (doublePriceStr > doubletickSize){
//                        if namergi == 0.0 {
//                            self.priceTf.text = String(format: "%.0f", doublePriceStr).insertComma
//                        }else {
//                            self.priceTf.text = String(format: "%.0f", doublePriceStr - namergi).insertComma
//                        }
//                    }
                }else {
                    self.priceTf.text = String(format: "%.0f", stringtoDouble(str: priceStr!)!).insertComma
                }
                
            }
        }
        
        let num1 = self.stringtoDouble(str: self.quantityTf.text!)!
        let num2 = self.stringtoDouble(str: self.priceTf.text!)!
        print("num1 : \(num1)")
        
        if self.quantityTf.text == "." {
           self.quantityTf.text = ""
        }else if self.priceTf.text == "." {
            self.priceTf.text = ""
        }
        
        
        //int max 값이상일경우 예외 처리 추가
        let amount = num1 * num2
        
        var intnum = 0
        
        if Double(Int.max) < amount {
            
        }else {
            intnum = Int(amount)
        }
        
        if self.marksimbol! == "BTC" {
            var krwAmount = amount * AppDelegate.BTC
            let str = "\(Int(krwAmount.roundToPlaces(places: 0)))".insertComma
//            self.orderAmountLabel.text = "\(String(format: "%.8f", amount)) BTC"
            
            self.orderAmountLabel.attributedText = labelAttributen(str: "\(String(format: "%.8f", amount))", sim: self.marksimbol!)
            self.oerderAmountKRWLabel.text = "\(str) KRW"
        }else if self.marksimbol! == "ETH" {
            var krwAmount = amount * AppDelegate.ETH
            let str = "\(Int(krwAmount.roundToPlaces(places: 0)))".insertComma
//            self.orderAmountLabel.text = "\(String(format: "%.8f", amount)) ETH"
            
            self.orderAmountLabel.attributedText = labelAttributen(str: "\(String(format: "%.8f", amount))", sim: self.marksimbol!)
            self.oerderAmountKRWLabel.text = "\(str) KRW"
            
        }else {
            let str = "\(intnum)".insertComma
//            self.orderAmountLabel.text = "\(str) KRW"
            
            self.orderAmountLabel.attributedText = labelAttributen(str: "\(str)", sim: "KRW")
            self.oerderAmountKRWLabel.isHidden = true
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        print("textFieldShouldEndEditing -----------------------")
        /*
        guard let priceSize = stringtoDouble(str: self.priceTf.text!) else {
            return true
        }
        
        guard let size = stringtoDouble(str: self.tickSice!) else {
            return true
        }
        
        if self.marksimbol! == "KRW"{
            
            
            let namergi = priceSize.truncatingRemainder(dividingBy: size)
            var amount = 0.0
            
            if namergi == 0.0 {
                amount = priceSize
            }else {
                amount = (priceSize - namergi)
            }
            
            print("namergi : \(namergi), pricesize : \(priceSize), size : \(size), amount : \(amount) ")
            
            switch self.tickType {
            case .A :
                self.priceTf.text = String(Int(amount)).insertComma
            case .B :
                self.priceTf.text = String(format: "%.1f", priceSize + size).insertComma
            case .C :
                self.priceTf.text = String(format: "%.2f", priceSize + size).insertComma
            }
            
        }
 */
        
        return true
 
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        priceTf.resignFirstResponder()
        quantityTf.resignFirstResponder()
    }
    
    func trandeListRT(bt: NPriceInfo) {
        print("RT : OuttrandeListRT")
        let symbol = bt.symbol.toString().components(separatedBy: [" "]).joined()
        
        if coinSimbol == symbol {
            
            let tempArray = bt.toJSON()
            self.buyjsonvalueArray = tempArray.0 //bid
            self.selljsonvalueArray = tempArray.1 //ask
            
            print("RT tempArray : \(tempArray)")
            
            DispatchQueue.main.async {
                print("RT ReloadDate")
                self.exIntable.reloadData()
            }
        }
    }
    
    func exchangListRT(bt: NPriceTick) {
        //현재가리스트 델리게이트
        
        let symbol = bt.symbol.toString().components(separatedBy: [" "]).joined()
        let delta_rate = bt.delta_rate.toString()
        let last_volume = bt.last_volume.toString()
        let last_price = bt.last_price.toString()
        let intLast_volume = stringtoDouble(str: last_volume)
        let intLast_price = stringtoDouble(str: last_price) ?? 0
        
        print("RT : wallet main view simbol : \(symbol)")
        print("RT : wallet main view coin simbol : \(self.coinSimbol!)")
        print("RT : wallet main view last_price : \(last_price)")
        
        if symbol == "ETH" {
            AppDelegate.ETH = stringtoDouble(str: last_price) ?? 0
            
            DispatchQueue.main.async {
                self.exIntable.reloadData()
            }
            
        }else if symbol == "BTC" {
            AppDelegate.BTC = stringtoDouble(str: last_price) ?? 0
            
            DispatchQueue.main.async {
                self.exIntable.reloadData()
            }
        }
        
        //호가단위 수정
        if symbol == coinSimbol && marksimbol == "KRW"{
            
            if intLast_price >= 2000000 {
                self.tickSice = "1000"
                self.tickType = .A
            }else if intLast_price >= 1000000 {
                self.tickSice = "500"
                self.tickType = .A
            }else if intLast_price >= 500000 {
                self.tickSice = "100"
                self.tickType = .A
            }else if intLast_price >= 100000 {
                self.tickSice = "50"
                self.tickType = .A
            }else if intLast_price >= 10000 {
                self.tickSice = "10"
                self.tickType = .A
            }else if intLast_price >= 1000 {
                self.tickSice = "5"
                self.tickType = .A
            }else if intLast_price >= 100 {
                self.tickSice = "1"
                self.tickType = .A
            }else if intLast_price >= 10 {
                self.tickSice = "0.1"
                self.tickType = .B
            }else {
                self.tickSice = "0.01"
                self.tickType = .C
            }
            
        }else if marksimbol == "ETH" || marksimbol == "BTC"  {
            
            print("krw other coin")
            self.tickSice = "0.00000001"
        }
        
    }
    
    func exchangDetailRT(bt: NPriceTick) {
        
    }
    
    func openListRT(bt: NOpenInfo) {
        
    }
    
    func krwChangeRT(value: Double) {
        DispatchQueue.main.async {
            self.exIntable.reloadData()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var allowCharatorSet = CharacterSet.decimalDigits
        let charaterset = CharacterSet(charactersIn: string)
        allowCharatorSet.insert(charactersIn: ".")
        return allowCharatorSet.isSuperset(of: charaterset)
    }
    
    func strMarkSlice(str : String) -> String {
        let index = str.count
        let tempindex = index - 4
        print("\(index)")
        return String(str.prefix(tempindex))
    }
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        
    }
    
    @available(iOS 8.0, *)
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping () -> Void) {
        
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .default, handler: { (action) in
            completionHandler()
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @available(iOS 8.0, *)
    func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping (Bool) -> Void) {
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .default, handler: { (action) in
            completionHandler(true)
        }))
        alertController.addAction(UIAlertAction(title: "common_no_btn".localized, style: .default, handler: { (action) in
            completionHandler(false)
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @available(iOS 8.0, *)
    func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping (String?) -> Void) {
        let alertController = UIAlertController(title: "", message: prompt, preferredStyle: .alert)
        alertController.addTextField { (textField) in
            textField.text = defaultText
        }
        alertController.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .default, handler: { (action) in
            if let text = alertController.textFields?.first?.text {
                completionHandler(text)
            } else {
                completionHandler(defaultText)
            }
        }))
        
        alertController.addAction(UIAlertAction(title: "common_no_btn".localized, style: .default, handler: { (action) in
            completionHandler(nil)
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @available(iOS 8.0, *)
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
        activityIndicator.frame = CGRect(x: self.webView.frame.midX-50, y: self.webView.frame.midY-50, width: 100, height: 100)
        activityIndicator.style = .whiteLarge
        activityIndicator.color = UIColor(rgb: 0x00A6ED)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        
    }
    
    @available(iOS 8.0, *)
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.activityIndicator.removeFromSuperview()
    }
    
    public func webViewWebContentProcessDidTerminate(_ webView: WKWebView) {
        // 중복적으로 리로드가 일어나지 않도록 처리 필요.
        self.webView.reload()
    }
    
    func textfiledView(simName: String, textFi: UITextField, rigthMode : Bool) {
        
        let tfvi = UIView()
        tfvi.backgroundColor = UIColor.clear
        
        let simLable = UILabel()
        simLable.text = simName
        simLable.textColor = UIColor.DPsubTextColor
        simLable.font = UIFont.systemFont(ofSize: 10)
        simLable.adjustsFontSizeToFitWidth = true
        simLable.sizeToFit()
        
        simLable.frame = CGRect(x: 0, y: 0, width: 25, height: textFi.frame.height)
        tfvi.frame = CGRect(x: 0, y: 0, width: 25, height: textFi.frame.height)

        tfvi.addSubview(simLable)
        
        if rigthMode {
            textFi.rightView = tfvi
            textFi.rightViewMode = .always
            
        }else {
            textFi.leftView = tfvi
            textFi.rightViewMode = .always
        }
    }
    
    func labelAttributen(str: String, sim: String) -> NSAttributedString{
        
        let attr = NSMutableAttributedString()
        
        attr.append(NSAttributedString(string: "\(str) ", attributes: [NSAttributedString.Key.foregroundColor : UIColor.DPMinTextColor]))
        attr.append(NSAttributedString(string: sim, attributes: [NSAttributedString.Key.foregroundColor : UIColor.DPmainTextColor]))
        
        return attr
    }
    
    func makePricePer(price: Double, openprice: String) -> Double {
        
        let doubleLastPri = stringtoDouble(str: openprice) ?? 0.0
        print("inout price : \(price)")
        print("inout lastpri : \(doubleLastPri)")
        if price == 0 {
            return 0
        }else {
            return price-doubleLastPri
        }
    }
    
    func sendFeeValue(simbol : String, type : String) {
        
        let url = "\(AppDelegate.url)/auth/getSendFee?uid=\(AppDelegate.uid)&sessionid=\(AppDelegate.sessionid)&simbol=\(simbol)&procType=1&calcType=0&gasLimit=0&gwei=0"
        
        var value = 0.0
        
        print("sendfeebalue \(url)")
        let amo = Alamofire.request(url)
        amo.responseJSON{ response in
            if response.result.isSuccess == true {
                
                let result = response.result.value as? [String : Any]
                
                let fee = result!["fee"] as? String ?? "0"
                
                value = self.stringtoDouble(str: fee) ?? 0.0
                
                if type == "ask" {
                    //매도
                    self.askFee = value
                    
                }else {
                    //매수
                    self.bidFee = value
                }
                
                if self.buyAndSell == .In {
                    self.sendFeeValueLabel.text = String(format: "%0.8f", self.bidFee) + " \(self.marksimbol ?? "")"
                }else {
                    if self.simbole == "KDA" || self.simbole == "DPN" || self.simbole == "FNK" || self.simbole == "MCVW" || self.simbole == "BTR"{
                        self.sendFeeValueLabel.text = String(format: "%0.8f", self.askFee) + " ETH"
                        self.etherfee = self.askFee
                        self.ethEableAmount()
                    }else {
                        self.sendFeeValueLabel.text = String(format: "%0.8f", self.askFee) + " \(self.simbole ?? "")"
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func ethEableAmount() {
        print("tableview userAmount ")
        
        let activity = UIActivityIndicatorView()
        activity.style = .whiteLarge
        activity.color = UIColor.DPActivityColor
        activity.hidesWhenStopped = true
        activity.startAnimating()
        
        var url = "\(AppDelegate.url)/auth/userAbleAmount?simbol=ETH&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&simbolMarket=\(self.coinSimbol ?? "")"
        print("wallet url : \(url)")

        let amo = Alamofire.request(url)
        amo.responseJSON(completionHandler: { response in
            
            if response.result.isSuccess == true {
                
                var buyjson = response.result.value as? [String : Any]
                
                let tempstr = (buyjson?["ableQty"] as? String) ?? "0"
                
                let doubleStrValue = self.stringtoDouble(str: tempstr) ?? 0.0
                
                self.sendEthAbleValue.text = String(format: "%0.8f", doubleStrValue) + " ETH"
                
                self.etherAbleValue = doubleStrValue
                
            }
            
            activity.stopAnimating()
            
        })
        
    }
    
    func diffPriceAction(curValue: String, dffValue: String) -> Bool{
        
        print("diffprice Action curValue : \(curValue), dffvalue : \(dffValue)")
        
        let tempCur = stringtoDouble(str: curValue) ?? 0.0
        
        let tempDff = stringtoDouble(str: dffValue) ?? 0.0
        
        let tempSub = tempCur - tempDff
        
        let differ = fabs(1 - (tempCur - tempSub) / tempCur)
        
        if differ > 0.3 {
            
            return true
            
        }else {
            return false
        }
        
        
    }
    
    
    
}
