//
//  FifthViewController.swift
//  crevolc
//
//  Created by crovolc on 2017. 12. 20..
//  Copyright © 2017년 hanbiteni. All rights reserved.
//
//  거래소 다섯번째 탭바 컨트롤러

import UIKit
import Foundation
import Alamofire

class FifthViewController : UITableViewController, AlertShowDelegate {
    
    var revealDelegate : RevealViewController?
    
    let circle : UIView = {
        let vi = UIView()
        vi.backgroundColor = UIColor(rgb: 0xe9a01b)
        vi.frame = CGRect(x: 40, y: 5, width: 5, height: 5)
        vi.layer.cornerRadius = vi.frame.size.width / 2
        vi.clipsToBounds = true
        vi.isHidden = true
        return vi
    }()
    
    @IBOutlet var secuLevel: UILabel!
    
    @IBOutlet var rateLevel: UILabel!
    
    @IBOutlet var userNickName: UILabel!
    
    @IBOutlet var userEmail: UILabel!
    
    @IBOutlet var userPhoneNumber: UILabel!
    
    @IBOutlet var linkCopyBtn: UIButton!
    //    @IBOutlet var finalLogin: UILabel!
  
    @IBOutlet var footerView: UIView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.DPViewBackgroundColor
        tableView.backgroundColor = UIColor.DPtableViewBgColor
        tableView.layoutMargins = UIEdgeInsets.zero
        tableView.separatorInset = UIEdgeInsets.zero
        
        initTitle()
        initbarbtn()
        initNextbtn()
        setupUserInfo()
        
        linkCopyBtn.layer.cornerRadius = 5
        
        let dragLeft = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(moveSide(_:)))
        dragLeft.edges = UIRectEdge.left
        self.view.addGestureRecognizer(dragLeft)
        
        let dragRight = UISwipeGestureRecognizer(target: self, action: #selector(moveSide(_:)))
        
        dragRight.direction = .left
        self.view.addGestureRecognizer(dragRight)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.alertReadValue(uid: AppDelegate.uid, sessionid: AppDelegate.sessionid)
        
        AlertShowController.sharedInstance.alertDelegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        setupUserInfo()
    }
    
    func initUserInfo() {
        
        print("log out action")
//        let plist = UserDefaults.standard
//
//        let type = plist.object(forKey: "sType") as? String ?? ""
//
//        var temptype = ""
        
//        switch type {
//        case "K":
//            print("kakao")
//            temptype = "Kakao 계정"
//        case "F":
//            print("facebook")
//            temptype = "Facebook 계정"
//        case "G":
//            print("google")
//            temptype = "Google 계정"
//        case "N":
//            print("naver")
//            temptype = "Naver 계정"
//        default:
//            print("other")
//            temptype = "Email 계정"
//        }
        
        print("login message : \(AppDelegate.userPhoneNumber)")
        
        userEmail.text = AppDelegate.userEmail
        userNickName.text = AppDelegate.userName
        userPhoneNumber.text = AppDelegate.userPhoneNumber == "" ? "my_info_phone_detail".localized : AppDelegate.userPhoneNumber
//        finalLogin.text = "2018년 4월 30일"
        secuLevel.text = "\(AppDelegate.userSecretLevel)"
        rateLevel.text = "1"
//        snsAmount.text = temptype
        
    }
    
    
    @IBAction func linkCopyBtnAction(_ sender: Any) {
        
        var email = AppDelegate.userEmail
        
        email.replace(originalString: "@", withString: "★")
        
        let url = "https://kdex.io/auth/\(email)/welcome"
        
        print("url : \(url)")
        
        UIPasteboard.general.string = url
        
        if email != "" {
            self.showToast(message: "링크가 복사되었습니다", textcolor: UIColor.white, frame: CGRect(x: self.view.frame.size.width/2 - 100, y: self.view.frame.size.height-100, width: 200, height: 35), time: 0.2, animation: true)
        }
        
//        if let url = URL(string: "https://kdex.io/auth/\(email)/welcome") {
//            UIApplication.shared.open(url, options: [:])
//        }
        
    }
    
    func initTitle(){
        
        let nTitle = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 40))
        
        nTitle.textAlignment = .center
        nTitle.font = UIFont.boldSystemFont(ofSize: 15)
        nTitle.textColor = UIColor.white
        nTitle.text = "main_myinfo".localized
        nTitle.textAlignment = .left
        
        self.navigationItem.titleView = nTitle
        
        self.navigationItem.titleView?.frame.size = CGSize(width: 300, height: 40)
        self.navigationController?.navigationBar.barTintColor = UIColor.DPNaviBarTintColor
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationController?.addShadowToBar(vi: self)
    }
    
    func initbarbtn(){
        
        let icon = UIImage(named: "icn_appbar_menu")
        
        let item = UIBarButtonItem(image:icon , style: .plain, target: self, action: nil)
        
        item.tintColor = UIColor.white
        
        item.target = self
        
        item.action = #selector(moveSide)
        
        self.navigationItem.leftBarButtonItem = item
        
    }
    
    func initNextbtn(){
        let totalVI = UIView()
        totalVI.backgroundColor = UIColor.clear
        totalVI.frame = CGRect(x: 0, y: 0, width: 70, height: 37)
        totalVI.tintColor = UIColor.DPNaviBartextTintColor
        
        let icon3 = UIImage(named: "alarm")
        
        //알림 리스트 버튼
        let rightbtn3 = UIButton(type: .system)
        rightbtn3.frame = CGRect(x: 35, y: 5, width: 30, height: 30)
        rightbtn3.setImage(icon3, for: .normal)
        rightbtn3.addTarget(self, action: #selector(alertAction), for: .touchUpInside)
        
        // totalVI.addSubview(rightbtn2)
        totalVI.addSubview(rightbtn3)
        totalVI.addSubview(circle)
        
        let icon4 = UIImage(named: "homt")
        
        let rightbtn4 = UIButton(type: .system)
        rightbtn4.frame = CGRect(x: 0, y: 8, width: 25, height: 25)
        rightbtn4.setImage(icon4, for: .normal)
        rightbtn4.addTarget(self, action: #selector(homeAction), for: .touchUpInside)
        
        totalVI.addSubview(rightbtn4)
        
        let item = UIBarButtonItem(customView: totalVI)
        self.navigationItem.rightBarButtonItem = item
        
    }
    
    @objc func homeAction() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        if let detail = storyboard.instantiateViewController(withIdentifier: "homeViPop") as? HomeViewController  {
            
            guard let appdel = UIApplication.shared.delegate as? AppDelegate else {
                return
            }
            
            appdel.window?.rootViewController?.present(detail, animated: true, completion: nil)
        }
        
    }
    
    
    @objc func moveSide(_ sender: Any) {
        
        let torchEvent = UITapGestureRecognizer(target: self, action: #selector(moveSide(_:)))
        
        if sender is UIScreenEdgePanGestureRecognizer{
            self.revealDelegate?.openSideBar(nil)
        }else if sender is UISwipeGestureRecognizer{
            self.revealDelegate?.closeSideBar(nil)
        }else if sender is UITapGestureRecognizer{
            self.revealDelegate?.closeSideBar(nil)
            self.view.removeGestureRecognizer((sender as? UITapGestureRecognizer)!)
        }else if sender is UIBarButtonItem{
            if self.revealDelegate?.isSideBarShowing == false{
                self.view.addGestureRecognizer(torchEvent)
                self.revealDelegate?.openSideBar(nil)
            } else {
                self.revealDelegate?.closeSideBar(nil)
            }
        }
    }
    @IBAction func logOutAt(_ sender: Any) {
        print("log out action")
        let plist = UserDefaults.standard
        
        let type = plist.object(forKey: "sType") as? String ?? ""
        
        plist.set(false, forKey: "loginIs")
        plist.set(false, forKey: "appLock")
        plist.set(false, forKey: "touchIdIs")
        
        plist.synchronize()
        
        if type != "" {
            AppDelegate.logout(stype: type)
        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        if let detail = storyboard.instantiateViewController(withIdentifier: "loginPage") as? EmailLoginViewController {
            present(detail, animated: true, completion: nil)
        }
        
    }
    
    @objc func alertAction(){
        let alertView = self.storyboard?.instantiateViewController(withIdentifier: "alertDetail") as? AlertDetailViewController
        self.navigationController?.pushViewController(alertView!, animated: true)
    }
    
    func setupUserInfo(){
        var urlComponents = URLComponents(string:"\(AppDelegate.url)app/getUserInfo?uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)")!
        print("url : \(urlComponents)")
        
        let amo = Alamofire.request(urlComponents)
        amo.responseJSON(completionHandler:
            { response in
                
                //json 파싱
                guard let val = response.result.value as? NSDictionary else {
                    return
                }
                
                AppDelegate.userName = val["userName"] as? String ?? ""
                AppDelegate.userEmail = val["userEmail"] as? String ?? ""
                AppDelegate.userPhoneNumber = val["userMobile"] as? String ?? ""
                AppDelegate.userSecretLevel = val["userLevel"] as? Int ?? 1
                
                self.initUserInfo()

        })
        
    }
    
    func alerShowAction() {
        
        if AppDelegate.alertShow {
            circle.isHidden = false
        }else {
            circle.isHidden = true
        }
        
    }
    
    func alertReadValue(uid: String, sessionid: String) {
        
        let urlComponents = URLComponents(string:"\(AppDelegate.url)/ajax/getPushLog?uid=\(uid)&sessionId=\(sessionid)&language=\(AppDelegate.appLang)")!
        
        print("urlcomponents : \(urlComponents)")
        
        let amo = Alamofire.request(urlComponents)
        amo.responseJSON { response in
            
            guard let value = response.result.value as? [[String : Any]] else {
                return
            }
            
            var tempValue = [[String:Any]]()
            
            for raw in value {
                let tempType = raw["readYn"] as? String ?? ""
                if tempType == "N"{
                    tempValue.append(raw)
                }
            }
            
            UIApplication.shared.applicationIconBadgeNumber = tempValue.count
            
            print("BadgeNumber : \(UIApplication.shared.applicationIconBadgeNumber)")
            
            
            if tempValue.count != 0 {
                self.circle.isHidden = false
            }else {
                self.circle.isHidden = true
            }
            
        }
    }
    
}
