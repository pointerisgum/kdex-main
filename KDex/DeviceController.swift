//
//  DeviceController.swift
//  KDex
//
//  Created by park heewon on 2018. 9. 19..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class DeviceController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var deviceArray = [[String : Any]]()
    
    var morebtnSelecteindexRow : IndexPath?
    
    @IBOutlet var titleLb: UILabel!
    
    @IBOutlet var deviceListTb: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLb.text = "\(AppDelegate.userName)" + "device_main_title_name".localized
        
        deviceList()
        
        deviceListTb.delegate = self
        deviceListTb.dataSource = self
        deviceListTb.allowsSelection = false
        deviceListTb.separatorStyle = .none
        
        self.navigationController?.addShadowToBar(vi: self)
        
        deviceListTb.register(UINib(nibName: "DeviceCell", bundle: nil), forCellReuseIdentifier: "DeviceCellId")
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return deviceArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let value = self.deviceArray[indexPath.row]
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "DeviceCellId", for: indexPath) as? DeviceCell else {
            return UITableViewCell()
        }
        
        let deleteBool = value["deleteYn"] as? String ?? "N"
        
        let openBool = value["openBool"] as? String ?? "N"
        
        cell.deleteBool = deleteBool
        cell.indexPathRow = indexPath
        

        cell.delegate = self
        cell.deviceId = value["deviceId"] as? String ?? ""
        cell.deviceOs.text = value["deviceOs"] as? String ?? ""
        cell.deviceModelName.text = value["deviceModel"] as? String ?? ""
        
        if deleteBool == "N"{
            
            cell.bgView.backgroundColor = UIColor.white
            cell.privateKeyInitBtn.isHidden = true
            cell.deviceRegsterDate.text = "\(makeTime(date: value["createDate"] as? String ?? "")) " + "device_register".localized
            cell.privateLb.text = "PRIVATE KEY"
            
            if (value["deviceMainYn"] as? String ?? "N") == "N"  {
                
                cell.mainCheckImg.image = UIImage(named: "main_star_off")
                
            }else {
             
                cell.mainCheckImg.image = UIImage(named: "main_star_on")
                
            }
            
            if (value["privateKeyYn"] as? String ?? "N") == "N"  {
                cell.privateKeyImg.isHidden = false
                cell.privateKeyImg.image = UIImage(named: "privatekey_sync")
                
            }else {
                
                cell.privateKeyImg.isHidden = true
                
            }
            
            if AppDelegate.mainDeviceBool {
                
                if cell.deviceId == AppDelegate.devicevo.deviceId {
                    cell.currentDviceImg.isHidden = false
                    cell.moreBtn.isHidden = true
                    
                }else {
                    
                    cell.moreBtn.isHidden = false
                    cell.currentDviceImg.isHidden = true
                    
                    if (value["privateKeyYn"] as? String ?? "N") == "N"  {
                        cell.privateKeyImg.isHidden = false
                        cell.privateKeyImg.image = UIImage(named: "privatekey_sync")
//                        cell.mainChangeBtn.backgroundColor = UIColor(rgb: 0xf2f2f2)
//                        cell.mainChangeBtn.isEnabled = false
                        
                    }else {
//                        cell.mainChangeBtn.backgroundColor = UIColor.white
//                        cell.mainChangeBtn.isEnabled = true
                        cell.privateKeyImg.isHidden = true
                        
                    }
                }
                
            }else {
                
                if cell.deviceId == AppDelegate.devicevo.deviceId {
                    cell.currentDviceImg.isHidden = false
                    cell.moreBtn.isHidden = false
                    
                    if (value["privateKeyYn"] as? String ?? "N") == "N"  {
                        cell.privateKeyImg.isHidden = false
                        cell.privateKeyImg.image = UIImage(named: "privatekey_sync")
//                        cell.mainChangeBtn.backgroundColor = UIColor(rgb: 0xf2f2f2)
//                        cell.mainChangeBtn.isEnabled = false
                        
                    }else {
//                        cell.mainChangeBtn.backgroundColor = UIColor.white
//                        cell.mainChangeBtn.isEnabled = true
                        cell.privateKeyImg.isHidden = true
                        
                    }
                    
                }else {
                    
                    cell.currentDviceImg.isHidden = true
                    cell.moreBtn.isHidden = true
                
                }
                
            }
            
        }else {
            
            cell.bgView.backgroundColor = UIColor(rgb: 0xf7f7f7)
            cell.deviceRegsterDate.text = "\(makeTime(date: value["createDate"] as? String ?? "")) " + "device_register".localized
            cell.mainCheckImg.image = UIImage(named: "del_device")
            cell.currentDviceImg.isHidden = true
            cell.moreBtn.isHidden = true
            
            if AppDelegate.mainDeviceBool {
                
                cell.privateKeyInitBtn.isHidden = false
                cell.privateKeyInitBtn.setTitle("device_delete".localized, for: .normal)
                cell.privateKeyInitBtn.setTitleColor(UIColor(rgb: 0x333333), for: .normal)
                cell.privateKeyInitBtn.backgroundColor = UIColor(rgb: 0xf7f7f7)
                
            }else {
                cell.privateKeyInitBtn.isHidden = true
            }
            
            cell.privateLb.text = "device_stoped_is".localized
            cell.privateKeyImg.isHidden = true
            
        }
        
        print("cell devici morebtnselectindexrow : \(morebtnSelecteindexRow?.row)")
        print("cell devici name : \(value["deviceModel"] as? String ?? "")")
        print("cell open Bool :\(indexPath.row) \(cell.morbtnOpenBool)")
        
//        if morebtnSelecteindexRow?.row == indexPath.row {
//
//            if cell.morbtnOpenBool {
//                cell.moreBtn.setImage(UIImage(named: "more_off"), for: .normal)
//                cell.morbtnOpenBool = false
//                cell.popVi.isHidden = true
//            }else {
//                cell.moreBtn.setImage(UIImage(named: "more_on"), for: .normal)
//                cell.morbtnOpenBool = true
//                cell.popVi.isHidden = false
//            }
//
//        }else {
//            cell.moreBtn.setImage(UIImage(named: "more_off"), for: .normal)
//            cell.morbtnOpenBool = false
//            cell.popVi.isHidden = true
//        }
        
        if openBool == "Y" {
            cell.moreBtn.setImage(UIImage(named: "more_on"), for: .normal)
            cell.popVi.isHidden = false
        }else {
            cell.moreBtn.setImage(UIImage(named: "more_off"), for: .normal)
            cell.popVi.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func deviceList() {
        
        //activity.startAnimating()
        
        self.deviceArray.removeAll()
        
        let url = "\(AppDelegate.url)/ajax/getDeviceList?simbol&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)"
        
        print("device url \(url)")
        
        let amo = Alamofire.request(url)
        amo.responseJSON(completionHandler: { response in
            
            guard let tempArray = response.result.value as? [[String : Any]] else {
                return
            }
            
            for row in tempArray {
                
                var value = row
                
                value.updateValue("N", forKey: "popOpenBool")
                
                self.deviceArray.append(value)
                
            }
            
            self.deviceListTb.reloadData()
            
        })
    }
    
    //날짜 형식 포멧
    func makeTime(date : String) -> String {
        
        let df : DateFormatter = DateFormatter()
        
        df.dateFormat = "yyyyMMddHHmmss"
        
        let date_time : NSDate = (df.date(from: date) as? NSDate)!
        
        df.dateFormat = "yyyy.MM.dd HH:mm:ss"
        
        let newDate : String = df.string(from: date_time as Date) as String
        
        return newDate
    }
    
    func stopWallet(dv: String){
        
        var url = ""
        
        if AppDelegate.mainDeviceBool {
            
            url = "\(AppDelegate.url)/ajax/deleteDevice?deviceId=\(dv)&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)"
            
            print("stop device url \(url)")
            
            let amo = Alamofire.request(url)
            amo.responseString(completionHandler: { response in
                
                guard let rs = response.result.value else {
                    return
                }
                
                if rs == "OK" {
                    print("stop device is good")
                    let alert = UIAlertController(title: "device_stop".localized, message: "device_crrent_device_stop".localized, preferredStyle:.alert)
                    alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .default, handler: { (_) in
                        self.deviceList()
                    }))
                    
                    self.present(alert, animated: false, completion: nil)
                    
                }
                
            })
            
        }else if dv == AppDelegate.devicevo.deviceId && AppDelegate.mainDeviceBool == false {
            
            url = "\(AppDelegate.url)/ajax/deleteDevice?deviceId=\(dv)&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)"
            
            print("stop device url \(url)")
            
            let amo = Alamofire.request(url)
            amo.responseString(completionHandler: { response in
                
                guard let rs = response.result.value else {
                    return
                }
                
                if rs == "OK" {
                    print("stop device is good")
                    
                    let alert = UIAlertController(title: "device_stop".localized, message: "device_device_stop_app_exit".localized, preferredStyle:.alert)
                    alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .default, handler: { (_) in
                        exit(0)
                    }))
                    
                    self.present(alert, animated: false, completion: nil)
                    
                }
                
            })
            
        }else {
            
            url = "\(AppDelegate.url)/ajax/deleteDeviceConfirm?deviceId=\(dv)&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)"
            
            print("stop device url \(url)")
            
            let amo = Alamofire.request(url)
            amo.responseString(completionHandler: { response in
                
                guard let tempResult = response.result.value else {
                    return
                }
                
                if tempResult == "OK" {
                    
                    let alert = UIAlertController(title: "device_stop".localized, message: "device_main_requst".localized, preferredStyle:.alert)
                    alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .default, handler: { (_) in
//                        exit(0)
//                        self.deviceList()
                    }))
                    
                    self.present(alert, animated: false, completion: nil)
                    
                }
                
            })
            
        }
        
    }
    
    func mainDeviceChange(dv: String){
        
        if AppDelegate.mainDeviceBool {
            
            let url = "\(AppDelegate.url)/ajax/changeMainDevice?deviceId=\(dv)&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)"
            
            print("change device url \(url)")
            
            let amo = Alamofire.request(url)
            amo.responseString(completionHandler: { response in
                
                guard let rs = response.result.value else {
                    return
                }
                
                if rs == "OK" {
                    
                    self.deviceList()
                    
                }else {
                    
                    let alert = UIAlertController(title: "device_exchange".localized, message: rs, preferredStyle:.alert)
                    alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .default, handler: { (_) in
                        //                        exit(0)
                        self.deviceList()
                    }))
                    
                    self.present(alert, animated: false, completion: nil)
                    
                    
                }
                
                
            })
            
        } else {
            
            let url = "\(AppDelegate.url)/ajax/changeMainDeviceConfirm?deviceId=\(dv)&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)"
            
            print("stop device url \(url)")
            
            let amo = Alamofire.request(url)
            amo.responseString(completionHandler: { response in
                
                guard let tempResult = response.result.value else {
                    return
                }
                
                print("device main change is good")
                
                if tempResult == "OK" {
                    
                    let alert = UIAlertController(title: "device_exchange".localized, message: "device_main_requst_reboot".localized, preferredStyle:.alert)
                    alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .default, handler: { (_) in
//                        exit(0)
                    }))
                    
                    self.present(alert, animated: false, completion: nil)
                    
                }
                
            })
            
        }
        
    }
    
    func initDeleteDeviceAction(dv: String, deleteBool: String){
        
        if deleteBool == "N" {
            
            guard let ad = UIApplication.shared.delegate as? AppDelegate else {
                return
            }
            
            ad.initAction()
            
        }else {
            
            let url = "\(AppDelegate.url)/ajax/deleteDeviceDatabase?deviceId=\(dv)&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)"
            
            print("delete device url \(url)")
            
            let amo = Alamofire.request(url)
            amo.responseString(completionHandler: { response in
                
                guard let rs = response.result.value else {
                    return
                }
                
                if rs == "OK" {
                    print("stop device is good")
                    
                    self.deviceList()
                    
                }
                
            })
            
        }
        
    }
    
}
