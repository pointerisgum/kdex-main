//
//  CreateWebDetailViewController.swift
//  crevolc
//
//  Created by crovolc on 2018. 1. 8..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import UIKit
import Foundation

class CreateWebDetailViewController : UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initTitle()
        self.navigationController?.navigationBar.barTintColor = UIColor.DPNaviBarTintColor
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = ""
        
    }
    
    @IBAction func createWalletAction(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let detail = storyboard.instantiateViewController(withIdentifier: "walletPager") as? WalletPageViewController {
            self.navigationController?.pushViewController(detail, animated: true)
            
        }
    }
    
    func initTitle(){
        let nTitle = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 40))
        
        nTitle.textAlignment = .center
        nTitle.font = UIFont.boldSystemFont(ofSize: 15)
        nTitle.textColor = UIColor.white
        nTitle.text = "지갑생성"
        nTitle.textAlignment = .left
        
        self.navigationItem.titleView = nTitle
        self.navigationItem.titleView?.frame.size = CGSize(width: 300, height: 40)
        
    }
    
}
