//
//  FourthViewController.swift
//  crevolc
//
//  Created by crovolc on 2017. 12. 20..
//  Copyright © 2017년 hanbiteni. All rights reserved.
//
//  거래소 네번째 탭바 컨트롤러

import UIKit
import Foundation
import Alamofire
import Charts
import Kingfisher

class FourthViewController : UIViewController, UITableViewDelegate, UITableViewDataSource, RealTimeDelegate, AlertShowDelegate, TokenOpenAble {
    @IBOutlet var qrPayVi: UIView!
    
    var realTimeBool = false
    
    var tokenOpenBool = false
    
    var tokenBtnClick = false
    
    var ethIndexNumber = 0 {
        didSet {
            print("ethIndexNumb : \(ethIndexNumber)")
            self.openToken()
        }
    }
    
    var openChartBool = false
    
    @IBOutlet var openCloseBtn: UIButton!
    
    @IBOutlet var chartVi: UIView!
    
    let activity = UIActivityIndicatorView()
    
    let circle : UIView = {
        
        let vi = UIView()
        vi.backgroundColor = UIColor(rgb: 0xe9a01b)
        vi.frame = CGRect(x: 40, y: 5, width: 5, height: 5)
        vi.layer.cornerRadius = vi.frame.size.width / 2
        vi.clipsToBounds = true
        vi.isHidden = true
        return vi
    }()
    
    let circleChart : PieChartView = {
        let ch = PieChartView()
        return ch
    }()
    
    @IBOutlet var walletViewHeader: UIView!
    
    //월렛 테이블
    @IBOutlet var walletTV: UITableView!
    
    //총 코인 보유 금액
    @IBOutlet var totalGiveCoinValue: UILabel!
    
    @IBOutlet var walletCount: UILabel!
    
    //보유코인체크버튼
    @IBOutlet var ownCoinCheck: UIButton!
    
    //매인키복사버튼
    @IBOutlet var mainKeyCopy: UIButton!
    
    @IBOutlet var shoppingBtn: UIButton!
    
    @IBOutlet var payLb: UILabel!
    
    //보유지갑체크버튼
    @IBAction func ownCoinCheckAction(_ sender: Any) {
        
        if AppDelegate.ownCoinList {
            
            tokenOpenBool = false
            AppDelegate.ownCoinList = false
            ownCoinCheck.backgroundColor = UIColor.DPWhiteBtnColor
            ownCoinCheck.setTitleColor(UIColor.DPCompanyColor, for: .normal)
            ownCoinCheck.layer.borderWidth = 1
            ownCoinCheck.layer.borderColor = UIColor.DPCompanyColor.cgColor
            
            walletList()
            
        }else {
            
            tokenOpenBool = false
            AppDelegate.ownCoinList = true
            ownCoinCheck.backgroundColor = UIColor.DPOrengeBtnColor
            ownCoinCheck.setTitleColor(UIColor.white, for: .normal)
            ownCoinCheck.layer.borderWidth = 0
            
            walletList()
            
        }
        
    }
    
    @IBOutlet var chartViheightValue: NSLayoutConstraint!
    
    @IBOutlet var mainKeyTrailValue: NSLayoutConstraint!
    
    @IBOutlet var ownCoinBtnTrailValue: NSLayoutConstraint!
    
    @IBAction func openCloseBtnAction(_ sender: Any) {
        if openChartBool {
            
            openChartBool = false
            chartViheightValue.constant = 320
            ownCoinBtnTrailValue.constant = 13
            mainKeyTrailValue.constant = 8
            chartVi.isHidden = false
            
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: {
                
                self.openCloseBtn.transform = CGAffineTransform(rotationAngle: -CGFloat.pi * 2)
            }, completion: nil)
            
        }else {
            
            openChartBool = true
            ownCoinBtnTrailValue.constant = self.openCloseBtn.frame.width + 13
            mainKeyTrailValue.constant = self.openCloseBtn.frame.width + 8
            chartViheightValue.constant = 40
            chartVi.isHidden = true
            
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
                
                self.openCloseBtn.transform = CGAffineTransform(rotationAngle: -CGFloat.pi)
            }, completion: nil)
            
        }
    }
    
    @IBAction func shoppingBtnAction(_ sender: Any) {
        
    }
    
    @IBAction func coinQrPay(_ sender: Any) {
        
        let msg = "service_ready".localized
        let alert = UIAlertController(title: "qr_pay".localized, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: {(_) in
            self.view.endEditing(true)
        }))
        self.present(alert, animated: false, completion: nil)
        
    }
    
    @IBAction func mainKeyCopyAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        if let detail = storyboard.instantiateViewController(withIdentifier: "privateRequestPop") as? PrivateKeyController {
            
            let alertWindow = UIWindow(frame: UIScreen.main.bounds)
            alertWindow.rootViewController = UIViewController()
            alertWindow.windowLevel = UIWindow.Level.alert + 1;
            alertWindow.makeKeyAndVisible()
            alertWindow.rootViewController?.present(detail, animated: true, completion: nil)
            
        }
        //                            self.requstPkCopyAction()
        
    }
    
    var walletArray = [[String : Any]]()
    
    var tokenWalletArrya = [[String : Any]]()
    
    var finacalAmount = 0 {
        didSet{
            let tempKrwStr = "\(finacalAmount)".insertComma
            
            setChart(dataValue: self.walletArray)
            
            //            let paragraphStyle = NSMutableParagraphStyle()
            //            paragraphStyle.alignment = .center
            //            paragraphStyle.paragraphSpacing = 0.6
            //
            //            let attrStr = NSMutableAttributedString(string: "총 보유 자산 \n", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16), .paragraphStyle:paragraphStyle])
            //
            //            attrStr.append(NSMutableAttributedString(string: "\(tempKrwStr)\nKRW", attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 14), .paragraphStyle:paragraphStyle]))
            //             circleChart.centerAttributedText = attrStr
            
            
            AppDelegate.totalCoinValue = "\(tempKrwStr) KRW"
        }
    }
    
    //사이드바 메인 컨트롤러 델리게이트
    var revealDelegate : RevealViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        walletTV.register(UINib(nibName: "TokenFourthCell", bundle: nil), forCellReuseIdentifier: "tokenWallCell")
        
        self.view.backgroundColor = UIColor(red:0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        self.walletViewHeader.backgroundColor = UIColor.DPViewBackgroundColor
        
        activity.frame = CGRect(x: self.view.frame.width/2 - 50, y: self.walletTV.frame.height/2 - 100, width: 100, height: 100)
        activity.style = .whiteLarge
        activity.color = UIColor.DPActivityColor
        activity.hidesWhenStopped = true
        walletTV.addSubview(activity)
        
        payLb.text = "pay".localized
        
        walletTV.delegate = self
        walletTV.dataSource = self
        
        initTitle() //네비 타이틀 설정
        initbarbtn() // left버튼 설정
        //        initNextbtn() // rigth버튼 설정
        
        shoppingBtn.layer.borderWidth = 1
        
        shoppingBtn.layer.borderColor = UIColor.DPLineBgColor.cgColor
        
        qrPayVi.layer.borderWidth = 1
        
        qrPayVi.layer.cornerRadius = 5
        
        //왼쪽 스크린 드래그 제스처 설정
        let dragLeft = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(moveSide(_:)))
        dragLeft.edges = UIRectEdge.left
        
        
        //오른쪽 스크린 드래그 제스처 설정
        let dragRight = UISwipeGestureRecognizer(target: self, action: #selector(moveSide(_:)))
        dragRight.direction = .left
        
        //터치 제스처 등록
        //let torchEvent = UITapGestureRecognizer(target: self, action: #selector(moveSide(_:)))
        
        self.view.addGestureRecognizer(dragLeft)
        self.view.addGestureRecognizer(dragRight)
        
        self.walletTV.backgroundColor = UIColor.DPtableViewBgColor
        self.walletTV.layoutMargins = UIEdgeInsets.zero
        self.walletTV.separatorInset = UIEdgeInsets.zero
        
    }
    
    func chartSetup(){
        
        self.chartVi.addSubview(self.circleChart)
        circleChart.frame = CGRect(x: 0, y: 0, width: self.chartVi.frame.width, height: (self.chartVi.frame.height) - 5)
        
        setChart(dataValue: self.walletArray)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let appdel = UIApplication.shared.delegate as? AppDelegate
        
        appdel?.walletTapBool = false
        
        openChartBool = false
        chartViheightValue.constant = 320
        ownCoinBtnTrailValue.constant = 13
        mainKeyTrailValue.constant = 8
        chartVi.isHidden = false
        openCloseBtn.transform = CGAffineTransform(rotationAngle: -CGFloat.pi * 2)
        
        self.alertReadValue(uid: AppDelegate.uid, sessionid: AppDelegate.sessionid)
        
        if AppDelegate.ownCoinList {
            ownCoinCheck.backgroundColor = UIColor.DPOrengeBtnColor
            ownCoinCheck.setTitleColor(UIColor.white, for: .normal)
            ownCoinCheck.layer.borderWidth = 0
            walletList()
            
        }else {
            ownCoinCheck.backgroundColor = UIColor.DPWhiteBtnColor
            ownCoinCheck.setTitleColor(UIColor.DPCompanyColor, for: .normal)
            ownCoinCheck.layer.borderWidth = 1
            ownCoinCheck.layer.borderColor = UIColor.DPCompanyColor.cgColor
            walletList()
        }
        //메인키 복사 사용 안함
        mainKeyCopy.isHidden = true
        
        //        if AppDelegate.mainDeviceBool {
        //            mainKeyCopy.isHidden = true
        //
        //        }else {
        //            if AppDelegate.newPkBool {
        //                mainKeyCopy.isHidden = false
        //                ownCoinCheck.isHidden = true
        //                mainKeyCopy.layer.borderColor = UIColor.DPCompanyColor.cgColor
        //                mainKeyCopy.layer.borderWidth = 1
        //            }else {
        //                mainKeyCopy.isHidden = true
        //                ownCoinCheck.isHidden = false
        //            }
        //
        //        }
        
        //        ownCoinCheck.backgroundColor = UIColor.DPWhiteBtnColor
        //        ownCoinCheck.setTitleColor(UIColor.DPmainTextColor, for: .normal)
        //        ownCoinCheck.layer.borderWidth = 1
        
        TcpSocket.sharedInstance.receiverThread?.delegate = self
        AlertShowController.sharedInstance.alertDelegate = self
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("view controller viewdidappear")
        initNextbtn()
    }
    
    func initTitle(){
        
        let nTitle = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 40))
        
        nTitle.textAlignment = .center
        nTitle.font = UIFont.boldSystemFont(ofSize: 15)
        nTitle.textColor = UIColor.white
        nTitle.text = "pop_wallet".localized
        nTitle.textAlignment = .left
        
        self.navigationItem.titleView = nTitle
        
        self.navigationItem.titleView?.frame.size = CGSize(width: 300, height: 40)
        
        self.navigationController?.navigationBar.barTintColor = UIColor.DPNaviBarTintColor
    }
    
    func initbarbtn(){
        
        //if let revealVC = self.revealViewController() {
        
        let icon = UIImage(named: "icn_appbar_menu")
        
        let item = UIBarButtonItem(image:icon , style: .plain, target: self, action: nil)
        
        item.tintColor = UIColor.white
        
        item.target = self
        
        item.action = #selector(moveSide)
        
        self.navigationItem.leftBarButtonItem = item
        
        self.navigationController?.addShadowToBar(vi: self)
        
    }
    
    func initNextbtn(){
        
        let totalVI = UIView()
        totalVI.backgroundColor = UIColor.clear
        totalVI.frame = CGRect(x: 0, y: 0, width: 70, height: 37)
        totalVI.tintColor = UIColor.DPNaviBartextTintColor
        
        let icon3 = UIImage(named: "alarm")
        
        //알림 리스트 버튼
        let rightbtn3 = UIButton(type: .system)
        rightbtn3.frame = CGRect(x: 35, y: 5, width: 30, height: 30)
        rightbtn3.setImage(icon3, for: .normal)
        rightbtn3.addTarget(self, action: #selector(alertAction), for: .touchUpInside)
        
        totalVI.addSubview(rightbtn3)
        totalVI.addSubview(circle)
        
        let icon4 = UIImage(named: "homt")
        
        let rightbtn4 = UIButton(type: .system)
        rightbtn4.frame = CGRect(x: 0, y: 8, width: 25, height: 25)
        rightbtn4.setImage(icon4, for: .normal)
        rightbtn4.addTarget(self, action: #selector(homeAction), for: .touchUpInside)
        
        totalVI.addSubview(rightbtn4)
        
        let item = UIBarButtonItem(customView: totalVI)
        self.navigationItem.rightBarButtonItem = item
    }
    
    @objc func homeAction() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        if let detail = storyboard.instantiateViewController(withIdentifier: "homeViPop") as? HomeViewController  {
            
            guard let appdel = UIApplication.shared.delegate as? AppDelegate else {
                return
            }
            
            appdel.window?.rootViewController?.present(detail, animated: true, completion: nil)
        }
        
    }
    
    @objc func moveSide(_ sender: Any) {
        
        let torchEvent = UITapGestureRecognizer(target: self, action: #selector(moveSide(_:)))
        
        if sender is UIScreenEdgePanGestureRecognizer{
            self.revealDelegate?.openSideBar(nil)
        }else if sender is UISwipeGestureRecognizer{
            self.revealDelegate?.closeSideBar(nil)
        }else if sender is UITapGestureRecognizer{
            self.revealDelegate?.closeSideBar(nil)
            self.view.removeGestureRecognizer((sender as? UITapGestureRecognizer)!)
        }else if sender is UIBarButtonItem{
            if self.revealDelegate?.isSideBarShowing == false{
                self.view.addGestureRecognizer(torchEvent)
                self.revealDelegate?.openSideBar(nil)
            } else {
                self.revealDelegate?.closeSideBar(nil)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return walletArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //이곳 확인
        if walletArray.count == 0 || (walletArray.count - 1) < indexPath.row {
            print("send wallet count zero error")
            return UITableViewCell()
        }
        
        let walletJson = walletArray[indexPath.row]
        
        let id = "fourthCell"
        
        let simbole = "\(walletJson["simbol"]!)"
        
        let pkBool = walletJson["walletkPkBool"] as? String ?? "N"
        
        print("wallet simbole : \(simbole), pkBool : \(pkBool)")
        
        let favoriteTokenCount = AppDelegate.favoriesTokenWalletListArray.count
        
        if simbole == "ETH" && favoriteTokenCount > 0 {
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "tokenWallCell", for: indexPath) as? TokenFourthCell else {
                return UITableViewCell()
            }
            
            //            if tokenOpenBool {
            //                cell.openBtn.setTitle("▲", for: .normal)
            //            }else {
            //                cell.openBtn.setTitle("▼", for: .normal)
            //            }
            
            cell.openBtn.setTitleColor(UIColor.DPsubTextColor, for: .normal)
            
            self.ethIndexNumber = indexPath.row
            
            if AppDelegate.ownCoinList {
                cell.tonkenOpenBtn(cell.openBtn)
            }
            
            cell.tableVi.reloadData()
            
            cell.layoutMargins = UIEdgeInsets.zero
            
            cell.coinKRWLabel.isHidden = true //temp write
            
            if walletArray.count == 0 || (walletArray.count - 1) < indexPath.row {
                print("send wallet count zero")
                return cell
            }
            
            cell.tokenDelegate = self
            
            var coinValueStr = ""
            
            let tempPadding = walletJson["dpoPdngQty"] as? String ?? "0" //패딩 값
            
            let doublePadding = stringtoDouble(str: tempPadding) ?? 0
            
            var dpoQty = walletJson["dpoQty"] as? String ?? "0"
            
            if simbole == "KRW" || simbole == "KDP" || simbole == "KDMP" {
                dpoQty = String(format: "%.0f", stringtoDouble(str: dpoQty) ?? 0).insertComma
            }
            
            let attrStr = NSMutableAttributedString(string: dpoQty, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14)])
            
            
            if doublePadding != 0.0 && simbole == "KRW" {
                if doublePadding > 0.0 {
                    attrStr.append(NSAttributedString(string: " (\(tempPadding))", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.DPBuyDefaultColor]))
                }else {
                    attrStr.append(NSAttributedString(string: " (\(tempPadding))", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.DPSellDefaultColor]))
                }
                
            }else if doublePadding != 0.0 && simbole != "KRW" {
                if doublePadding > 0.0 {
                    attrStr.append(NSAttributedString(string: " (\(tempPadding)) \n", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.DPBuyDefaultColor]))
                }else {
                    attrStr.append(NSAttributedString(string: " (\(tempPadding)) \n", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.DPSellDefaultColor]))
                }
                
            }else if simbole != "KRW" {
                attrStr.append(NSAttributedString(string: "\n", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14)]))
            }
            
            cell.coinNameLabel.text = "\(walletJson["simbolName"]!)"
            cell.coinSimbol.text = simbole
            cell.coinNameLabel.adjustsFontSizeToFitWidth = true
            cell.coinNameLabel.sizeToFit()
            
            //            cell.coinImage.kf.setImage(with: AppDelegate.dataImageUrl(str: "\(walletJson["simbol"]!)"))
            cell.coinImage.image = UIImage(named: "\(walletJson["simbol"]!)")
            
            
            cell.coinKRWLabel.textColor = UIColor.DPsubTextColor
            
            if realTimeBool {
                var tempAmount = 0
                
                for row in walletArray {
                    //                let tempKrwValue = row["openAmt"] as? String ?? ""
                    
                    guard let price = self.stringtoDouble(str: row["lastPrice"] as? String ?? "") else {
                        break
                    }
                    guard let qty = self.stringtoDouble(str: row["dpoQty"] as? String ?? "") else {
                        break
                    }
                    
                    var amount = price * qty
                    
                    tempAmount = tempAmount + Int(amount.roundToPlaces(places: 0))
                    
                }
                
                self.finacalAmount = tempAmount
                self.realTimeBool = false
            }
            
            //        else {
            //
            //            let tempArrayJson = AppDelegate.KRWjsonArray
            //
            //            for row in tempArrayJson {
            //                let tempSimbol = row["simbol"] as? String ?? ""
            //
            //                print("wallet simbol : \(tempSimbol)")
            //
            //                if tempSimbol == "\(walletJson["simbol"]!)" {
            //
            //                    guard let price = stringtoDouble(str: row["lastPrice"] as? String ?? "") else {
            //                        return cell!
            //                    }
            //                    guard let qty = stringtoDouble(str: walletJson["openQty"] as? String ?? "") else {
            //                        return cell!
            //                    }
            //
            //                    var amount = price * qty
            //                    let str = "\(Int(amount.roundToPlaces(places: 0)))".insertComma
            //                    cell?.coinKRWLabel.text = "\(str) KRW"
            //                }
            //            }
            
            guard let price = stringtoDouble(str: walletJson["lastPrice"] as? String ?? "") else {
                return cell
            }
            guard let qty = stringtoDouble(str: walletJson["dpoQty"] as? String ?? "") else {
                return cell
            }
            
            var amount = price * qty
            let str = "\(Int(amount.roundToPlaces(places: 0)))".insertComma
            cell.coinKRWLabel.text = "\(str) KRW"
            
            if  simbole == "KRW" {
                //            print("wallet list KRW : \(indexPath.row)")
                //            cell?.coinKRWLabel.isHidden = true
                //            centerlayout?.isActive = true
            }else {
                
                //            cell?.coinKRWLabel.isHidden = false
                //            centerlayout?.isActive = false
                //            cell?.coinValueLabel.centerYAnchor.constraint(equalTo: (cell?.centerYAnchor)!).isActive = false
            }
            
            if simbole != "KRW" {
                attrStr.append(NSAttributedString(string: "\(str) KRW", attributes: [NSAttributedString.Key.foregroundColor: UIColor.DPsubTextColor, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12)]))
            }
            
            coinValueStr = "\(dpoQty) (\(tempPadding)) \n \(str) KRW"
            
            let widht = coinValueStr.calcWidth(forHeight: 20, font: UIFont.systemFont(ofSize: 14))
            
            cell.coinValueLabel.frame.size = CGSize(width: widht, height: (cell.frame.height) - 5)
            
            cell.coinValueLabel.attributedText = attrStr
            cell.coinValueLabel.numberOfLines = 2
            cell.coinValueLabel.adjustsFontSizeToFitWidth = true
            cell.coinValueLabel.sizeToFit()
            cell.backgroundColor = UIColor.DPtableCellBgColor
            
            return cell
            
        }else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: id, for: indexPath) as? FourthCell else {
                return UITableViewCell()
            }
            
            cell.delegate = self
            cell.tempWalletArray = walletJson
            cell.createBtn.setTitleColor(UIColor.white, for: .normal)
            cell.createBtn.backgroundColor = UIColor.DPCompanyColor
            cell.backVi.isHidden = true
            cell.createBtn.isHidden = true
            cell.createBtn.tag = indexPath.row
            
            
            cell.layoutMargins = UIEdgeInsets.zero
            
            cell.coinKRWLabel.isHidden = true
            
            if walletArray.count == 0 || (walletArray.count - 1) < indexPath.row {
                print("send wallet count zero")
                return cell
            }
            var coinValueStr = ""
            
            
            let tempPadding = walletJson["dpoPdngQty"] as? String ?? "0"
            
            let doublePadding = stringtoDouble(str: tempPadding) ?? 0
            
            var dpoQty = walletJson["dpoQty"] as? String ?? "0"
            
            if simbole == "KRW" || simbole == "KDP" || simbole == "KDMP" {
                dpoQty = String(format: "%.0f", stringtoDouble(str: dpoQty) ?? 0).insertComma
            }
            
            let attrStr = NSMutableAttributedString(string: dpoQty.insertComma, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14)])
            
            
            if doublePadding != 0.0 && simbole == "KRW" {
                if doublePadding > 0.0 {
                    attrStr.append(NSAttributedString(string: " (\(tempPadding))", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.DPBuyDefaultColor]))
                }else {
                    attrStr.append(NSAttributedString(string: " (\(tempPadding))", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.DPSellDefaultColor]))
                }
                
            }else if doublePadding != 0.0 && simbole != "KRW" {
                if doublePadding > 0.0 {
                    attrStr.append(NSAttributedString(string: " (\(tempPadding)) \n", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.DPBuyDefaultColor]))
                }else {
                    attrStr.append(NSAttributedString(string: " (\(tempPadding)) \n", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.DPSellDefaultColor]))
                }
                
            }else if simbole != "KRW" {
                attrStr.append(NSAttributedString(string: "\n", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14)]))
            }
            
            cell.coinNameLabel.text = "\(walletJson["simbolName"]!)"
            cell.coinSimbol.text = simbole
            cell.coinNameLabel.adjustsFontSizeToFitWidth = true
            cell.coinNameLabel.sizeToFit()
            cell.coinImage.image = AppDelegate.dataImage(str: "\(walletJson["simbol"]!)")
            //                UIImage(named: "\(walletJson["simbol"]!)")
            cell.coinKRWLabel.textColor = UIColor.DPsubTextColor
            
            if realTimeBool {
                var tempAmount = 0
                
                for row in walletArray {
                    //                let tempKrwValue = row["openAmt"] as? String ?? ""
                    
                    guard let price = self.stringtoDouble(str: row["lastPrice"] as? String ?? "") else {
                        break
                    }
                    guard let qty = self.stringtoDouble(str: row["dpoQty"] as? String ?? "") else {
                        break
                    }
                    
                    var amount = price * qty
                    
                    tempAmount = tempAmount + Int(amount.roundToPlaces(places: 0))
                    
                }
                
                self.finacalAmount = tempAmount
                self.realTimeBool = false
            }
            
            //        else {
            //
            //            let tempArrayJson = AppDelegate.KRWjsonArray
            //
            //            for row in tempArrayJson {
            //                let tempSimbol = row["simbol"] as? String ?? ""
            //
            //                print("wallet simbol : \(tempSimbol)")
            //
            //                if tempSimbol == "\(walletJson["simbol"]!)" {
            //
            //                    guard let price = stringtoDouble(str: row["lastPrice"] as? String ?? "") else {
            //                        return cell!
            //                    }
            //                    guard let qty = stringtoDouble(str: walletJson["openQty"] as? String ?? "") else {
            //                        return cell!
            //                    }
            //
            //                    var amount = price * qty
            //                    let str = "\(Int(amount.roundToPlaces(places: 0)))".insertComma
            //                    cell?.coinKRWLabel.text = "\(str) KRW"
            //                }
            //            }
            
            guard let price = stringtoDouble(str: walletJson["lastPrice"] as? String ?? "") else {
                return cell
            }
            guard let qty = stringtoDouble(str: walletJson["dpoQty"] as? String ?? "") else {
                return cell
            }
            
            var amount = price * qty
            let str = "\(Int(amount.roundToPlaces(places: 0)))".insertComma
            cell.coinKRWLabel.text = "\(str) KRW"
            
            //            if  simbole == "KRW" {
            //                //            print("wallet list KRW : \(indexPath.row)")
            //                //            cell?.coinKRWLabel.isHidden = true
            //                //            centerlayout?.isActive = true
            //            }else {
            //                print("wallet list falses : \(indexPath.row)")
            //                //            cell?.coinKRWLabel.isHidden = false
            //                //            centerlayout?.isActive = false
            //                //            cell?.coinValueLabel.centerYAnchor.constraint(equalTo: (cell?.centerYAnchor)!).isActive = false
            //            }
            
            if simbole != "KRW" {
                attrStr.append(NSAttributedString(string: "\(str) KRW", attributes: [NSAttributedString.Key.foregroundColor: UIColor.DPsubTextColor, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12)]))
            }
            
            coinValueStr = "\(dpoQty) (\(tempPadding)) \n \(str) KRW"
            
            let widht = coinValueStr.calcWidth(forHeight: 20, font: UIFont.systemFont(ofSize: 14))
            
            cell.coinValueLabel.frame.size = CGSize(width: widht, height: (cell.frame.height) - 5)
            
            cell.coinValueLabel.attributedText = attrStr
            cell.coinValueLabel.numberOfLines = 2
            cell.coinValueLabel.adjustsFontSizeToFitWidth = true
            cell.coinValueLabel.sizeToFit()
            cell.backgroundColor = UIColor.DPtableCellBgColor
            
            
            if AppDelegate.mainDeviceBool && pkBool == "N"{
            
                if simbole != "KRW" && simbole != "KDP" && simbole != "KDMP" && simbole != "KDA"{
                    
                    cell.isUserInteractionEnabled = true
                    cell.createBtn.isHidden = false
                    cell.coinValueLabel.isHidden = true
                    cell.selectionStyle = .none
                    cell.createBtn.isEnabled = true
                    
                }else{
                    
                    cell.isUserInteractionEnabled = true
                    cell.createBtn.isHidden = true
                    cell.coinValueLabel.isHidden = false
                    cell.selectionStyle = .default
                    
                }
                
            }else if AppDelegate.mainDeviceBool && pkBool == "Y"  {
                
                cell.isUserInteractionEnabled = true
                cell.coinValueLabel.isHidden = false
                cell.createBtn.isHidden = true
                cell.backVi.isHidden = true
                cell.selectionStyle = .default
                
            }else if !AppDelegate.mainDeviceBool && pkBool == "N" {
                
                cell.createBtn.isHidden = true
                
                if simbole != "KRW" && simbole != "KDP" && simbole != "KDMP" {
                    
                    cell.isUserInteractionEnabled = false
                    cell.backVi.isHidden = false
                    cell.selectionStyle = .none
                    
                }else{
                    
                    cell.isUserInteractionEnabled = true
                    cell.backVi.isHidden = true
                    cell.selectionStyle = .default
                    
                }
                
            }else if !AppDelegate.mainDeviceBool && pkBool == "Y" {
                
                cell.isUserInteractionEnabled = true
                cell.backVi.isHidden = true
                cell.selectionStyle = .default
                
            }
            
            return cell
        }
        
    }
    /*
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
     //이곳 확인
     if walletArray.count == 0 || (walletArray.count - 1) < indexPath.row {
     print("send wallet count zero error")
     return UITableViewCell()
     }
     
     let walletJson = walletArray[indexPath.row]
     
     let id = "fourthCell"
     
     let simbole = "\(walletJson["simbol"]!)"
     
     let pkBool = walletJson["walletkPkBool"] as? String ?? "N"
     
     print("wallet simbole : \(simbole), pkBool : \(pkBool)")
     
     let favoriteTokenCount = AppDelegate.favoriesTokenWalletListArray.count
     
     if simbole == "ETH" && favoriteTokenCount > 0 {
     
     guard let cell = tableView.dequeueReusableCell(withIdentifier: "tokenWallCell", for: indexPath) as? TokenFourthCell else {
     return UITableViewCell()
     }
     
     //            if tokenOpenBool {
     //                cell.openBtn.setTitle("▲", for: .normal)
     //            }else {
     //                cell.openBtn.setTitle("▼", for: .normal)
     //            }
     
     cell.openBtn.setTitleColor(UIColor.DPsubTextColor, for: .normal)
     
     self.ethIndexNumber = indexPath.row
     
     if AppDelegate.ownCoinList {
     cell.tonkenOpenBtn(cell.openBtn)
     }
     
     cell.tableVi.reloadData()
     
     cell.layoutMargins = UIEdgeInsets.zero
     
     cell.coinKRWLabel.isHidden = true //temp write
     
     if walletArray.count == 0 || (walletArray.count - 1) < indexPath.row {
     print("send wallet count zero")
     return cell
     }
     
     cell.tokenDelegate = self
     
     var coinValueStr = ""
     
     let tempPadding = walletJson["dpoPdngQty"] as? String ?? "0"
     
     let doublePadding = stringtoDouble(str: tempPadding) ?? 0
     
     var dpoQty = walletJson["dpoQty"] as? String ?? "0"
     
     if simbole == "KRW" || simbole == "KDP" {
     dpoQty = String(format: "%.0f", stringtoDouble(str: dpoQty) ?? 0).insertComma
     }
     
     let attrStr = NSMutableAttributedString(string: dpoQty, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14)])
     
     if doublePadding != 0.0 && simbole == "KRW" {
     attrStr.append(NSAttributedString(string: " (\(tempPadding))", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.DPBuyDefaultColor]))
     }else if doublePadding != 0.0 && simbole != "KRW" {
     attrStr.append(NSAttributedString(string: " (\(tempPadding)) \n", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.DPBuyDefaultColor]))
     }else if simbole != "KRW" {
     attrStr.append(NSAttributedString(string: "\n", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14)]))
     }
     
     cell.coinNameLabel.text = "\(walletJson["simbolName"]!)"
     cell.coinSimbol.text = simbole
     cell.coinNameLabel.adjustsFontSizeToFitWidth = true
     cell.coinNameLabel.sizeToFit()
     
     //            cell.coinImage.kf.setImage(with: AppDelegate.dataImageUrl(str: "\(walletJson["simbol"]!)"))
     cell.coinImage.image = UIImage(named: "\(walletJson["simbol"]!)")
     
     
     cell.coinKRWLabel.textColor = UIColor.DPsubTextColor
     
     if realTimeBool {
     var tempAmount = 0
     
     for row in walletArray {
     //                let tempKrwValue = row["openAmt"] as? String ?? ""
     
     guard let price = self.stringtoDouble(str: row["lastPrice"] as? String ?? "") else {
     break
     }
     guard let qty = self.stringtoDouble(str: row["dpoQty"] as? String ?? "") else {
     break
     }
     
     var amount = price * qty
     
     tempAmount = tempAmount + Int(amount.roundToPlaces(places: 0))
     
     }
     
     self.finacalAmount = tempAmount
     self.realTimeBool = false
     }
     
     //        else {
     //
     //            let tempArrayJson = AppDelegate.KRWjsonArray
     //
     //            for row in tempArrayJson {
     //                let tempSimbol = row["simbol"] as? String ?? ""
     //
     //                print("wallet simbol : \(tempSimbol)")
     //
     //                if tempSimbol == "\(walletJson["simbol"]!)" {
     //
     //                    guard let price = stringtoDouble(str: row["lastPrice"] as? String ?? "") else {
     //                        return cell!
     //                    }
     //                    guard let qty = stringtoDouble(str: walletJson["openQty"] as? String ?? "") else {
     //                        return cell!
     //                    }
     //
     //                    var amount = price * qty
     //                    let str = "\(Int(amount.roundToPlaces(places: 0)))".insertComma
     //                    cell?.coinKRWLabel.text = "\(str) KRW"
     //                }
     //            }
     
     guard let price = stringtoDouble(str: walletJson["lastPrice"] as? String ?? "") else {
     return cell
     }
     
     guard let qty = stringtoDouble(str: walletJson["dpoQty"] as? String ?? "") else {
     return cell
     }
     
     var amount = price * qty
     let str = "\(Int(amount.roundToPlaces(places: 0)))".insertComma
     cell.coinKRWLabel.text = "\(str) KRW"
     
     if  simbole == "KRW" {
     //            print("wallet list KRW : \(indexPath.row)")
     //            cell?.coinKRWLabel.isHidden = true
     //            centerlayout?.isActive = true
     }else {
     
     //            cell?.coinKRWLabel.isHidden = false
     //            centerlayout?.isActive = false
     //            cell?.coinValueLabel.centerYAnchor.constraint(equalTo: (cell?.centerYAnchor)!).isActive = false
     }
     
     if simbole != "KRW" {
     attrStr.append(NSAttributedString(string: "\(str) KRW", attributes: [NSAttributedString.Key.foregroundColor: UIColor.DPsubTextColor, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12)]))
     }
     
     coinValueStr = "\(dpoQty) (\(tempPadding)) \n \(str) KRW"
     
     let widht = coinValueStr.calcWidth(forHeight: 20, font: UIFont.systemFont(ofSize: 14))
     
     cell.coinValueLabel.frame.size = CGSize(width: widht, height: (cell.frame.height) - 5)
     
     cell.coinValueLabel.attributedText = attrStr
     cell.coinValueLabel.numberOfLines = 2
     cell.coinValueLabel.adjustsFontSizeToFitWidth = true
     cell.coinValueLabel.sizeToFit()
     cell.backgroundColor = UIColor.DPtableCellBgColor
     
     return cell
     
     }else {
     guard let cell = tableView.dequeueReusableCell(withIdentifier: id, for: indexPath) as? FourthCell else {
     return UITableViewCell()
     }
     
     cell.delegate = self
     cell.tempWalletArray = walletJson
     cell.createBtn.setTitleColor(UIColor.white, for: .normal)
     cell.createBtn.backgroundColor = UIColor.DPCompanyColor
     cell.createBtn.isHidden = true
     cell.backVi.isHidden = true
     cell.createBtn.tag = indexPath.row
     
     cell.layoutMargins = UIEdgeInsets.zero
     
     
     
     if walletArray.count == 0 || (walletArray.count - 1) < indexPath.row {
     print("send wallet count zero")
     return cell
     }
     var coinValueStr = ""
     
     let tempPadding = walletJson["dpoPdngQty"] as? String ?? "0"
     
     let doublePadding = stringtoDouble(str: tempPadding) ?? 0
     
     var dpoQty = walletJson["dpoQty"] as? String ?? "0"
     
     if simbole == "KRW" || simbole == "KDP" {
     dpoQty = String(format: "%.0f", stringtoDouble(str: dpoQty) ?? 0).insertComma
     }
     
     let attrStr = NSMutableAttributedString(string: dpoQty, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14)])
     
     
     if doublePadding != 0.0 && simbole == "KRW" {
     attrStr.append(NSAttributedString(string: " (\(tempPadding))", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.DPBuyDefaultColor]))
     }else if doublePadding != 0.0 && simbole != "KRW" {
     attrStr.append(NSAttributedString(string: " (\(tempPadding)) \n", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.DPBuyDefaultColor]))
     }else if simbole != "KRW" {
     attrStr.append(NSAttributedString(string: "\n", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14)]))
     }
     
     // pk 유무에 따라 작업
     //            if !AppDelegate.mainDeviceBool && pkBool == "N"{
     //
     //                cell.createBtn.isHidden = true
     //                if simbole == "KRW" || simbole == "KDP" {
     //                    cell.isUserInteractionEnabled = true
     //                    cell.backVi.isHidden = true
     //                }else {
     //                    cell.isUserInteractionEnabled = false
     //                    cell.backVi.isHidden = false
     //                }
     //            }else
     
     if AppDelegate.mainDeviceBool && pkBool == "N"{
     
     if simbole != "KRW" && simbole != "KDP" {
     cell.createBtn.isHidden = false
     cell.backVi.isHidden = true
     cell.isUserInteractionEnabled = true
     cell.coinValueLabel.isHidden = true
     cell.coinKRWLabel.isHidden = true
     cell.selectionStyle = .none
     }else {
     
     cell.isUserInteractionEnabled = true
     cell.coinValueLabel.isHidden = false
     cell.coinKRWLabel.isHidden = true
     cell.selectionStyle = .default
     
     }
     
     }else {
     
     
     cell.coinValueLabel.isHidden = false
     cell.coinKRWLabel.isHidden = true
     cell.createBtn.isHidden = true
     cell.backVi.isHidden = true
     cell.isUserInteractionEnabled = true
     
     }
     //            cell.coinKRWLabel.isHidden = true
     
     cell.coinNameLabel.text = "\(walletJson["simbolName"]!)"
     cell.coinSimbol.text = simbole
     cell.coinNameLabel.adjustsFontSizeToFitWidth = true
     cell.coinNameLabel.sizeToFit()
     cell.coinImage.image = AppDelegate.dataImage(str: "\(walletJson["simbol"]!)")
     //                UIImage(named: "\(walletJson["simbol"]!)")
     cell.coinKRWLabel.textColor = UIColor.DPsubTextColor
     
     if realTimeBool {
     var tempAmount = 0
     
     for row in walletArray {
     //                let tempKrwValue = row["openAmt"] as? String ?? ""
     
     guard let price = self.stringtoDouble(str: row["lastPrice"] as? String ?? "") else {
     break
     }
     guard let qty = self.stringtoDouble(str: row["dpoQty"] as? String ?? "") else {
     break
     }
     
     var amount = price * qty
     
     tempAmount = tempAmount + Int(amount.roundToPlaces(places: 0))
     
     }
     
     self.finacalAmount = tempAmount
     self.realTimeBool = false
     
     }
     
     //        else {
     //
     //            let tempArrayJson = AppDelegate.KRWjsonArray
     //
     //            for row in tempArrayJson {
     //                let tempSimbol = row["simbol"] as? String ?? ""
     //
     //                print("wallet simbol : \(tempSimbol)")
     //
     //                if tempSimbol == "\(walletJson["simbol"]!)" {
     //
     //                    guard let price = stringtoDouble(str: row["lastPrice"] as? String ?? "") else {
     //                        return cell!
     //                    }
     //                    guard let qty = stringtoDouble(str: walletJson["openQty"] as? String ?? "") else {
     //                        return cell!
     //                    }
     //
     //                    var amount = price * qty
     //                    let str = "\(Int(amount.roundToPlaces(places: 0)))".insertComma
     //                    cell?.coinKRWLabel.text = "\(str) KRW"
     //                }
     //            }
     
     guard let price = stringtoDouble(str: walletJson["lastPrice"] as? String ?? "") else {
     return cell
     }
     guard let qty = stringtoDouble(str: walletJson["dpoQty"] as? String ?? "") else {
     return cell
     }
     
     var amount = price * qty
     let str = "\(Int(amount.roundToPlaces(places: 0)))".insertComma
     cell.coinKRWLabel.text = "\(str) KRW"
     
     //            if  simbole == "KRW" {
     //                //            print("wallet list KRW : \(indexPath.row)")
     //                //            cell?.coinKRWLabel.isHidden = true
     //                //            centerlayout?.isActive = true
     //            }else {
     //                print("wallet list falses : \(indexPath.row)")
     //                //            cell?.coinKRWLabel.isHidden = false
     //                //            centerlayout?.isActive = false
     //                //            cell?.coinValueLabel.centerYAnchor.constraint(equalTo: (cell?.centerYAnchor)!).isActive = false
     //            }
     
     if simbole != "KRW" {
     attrStr.append(NSAttributedString(string: "\(str) KRW", attributes: [NSAttributedString.Key.foregroundColor: UIColor.DPsubTextColor, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12)]))
     }
     
     coinValueStr = "\(dpoQty) (\(tempPadding)) \n \(str) KRW"
     
     let widht = coinValueStr.calcWidth(forHeight: 20, font: UIFont.systemFont(ofSize: 14))
     
     cell.coinValueLabel.frame.size = CGSize(width: widht, height: (cell.frame.height) - 5)
     
     cell.coinValueLabel.attributedText = attrStr
     cell.coinValueLabel.numberOfLines = 2
     cell.coinValueLabel.adjustsFontSizeToFitWidth = true
     cell.coinValueLabel.sizeToFit()
     cell.backgroundColor = UIColor.DPtableCellBgColor
     
     
     
     return cell
     }
     
     }
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if self.tokenOpenBool && indexPath.row == self.ethIndexNumber{
            
            let num = AppDelegate.favoriesTokenWalletListArray.count
            
            return CGFloat((num * 70) + 70)
        }
        
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let appdel = UIApplication.shared.delegate as? AppDelegate
        
        guard let walletJson = walletArray[indexPath.row] as? [String : Any] else {
            return
        }
        
        let simbole = walletJson["simbol"] as? String ?? ""
        
        let pkBool = walletJson["walletkPkBool"] as? String ?? "N"
        
        if simbole == "ETH" && self.tokenBtnClick {
            
            self.tokenBtnClick = false
            
            if self.tokenOpenBool {
                self.tokenOpenBool = false
            }else {
                self.tokenOpenBool = true
            }
            
            //            var index = 0
            //
            //            var indexpaths = [IndexPath]()
            //
            //            for row in walletArray {
            //                let indexpath = IndexPath(row: index, section: 0)
            //                indexpaths.append(indexpath)
            //                index += 1
            //            }
            
            tableView.beginUpdates()
            tableView.endUpdates()
            //            tableView.reloadRows(at: indexpaths, with: .automatic)
        }else {
            
            if simbole == "KRW" || simbole == "KDP" || pkBool != "N" || simbole == "KDA" || simbole == "KDMP" {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                if let detail = storyboard.instantiateViewController(withIdentifier: "walletPager") as? WalletPageViewController {
                    detail.coinsimbol = "\(walletJson["simbol"]!)"
                    detail.coinValue = "\(walletJson["dpoQty"]!)"
                    detail.lastPrice = "\(walletJson["lastPrice"]!)"
                    detail.coinName = "\(walletJson["simbolName"]!)"
                    detail.coinType = "\(walletJson["coinType"]!)"
                    if "\(walletJson["simbol"]!)" == "KRW" || "\(walletJson["simbol"]!)" == "KDP" || "\(walletJson["simbol"]!)" == "KDA" || "\(walletJson["simbol"]!)" == "ETH" || "\(walletJson["simbol"]!)" == "520"  {
                        appdel?.walletTapBool = false
                    }else {
                        
                       appdel?.walletTapBool = true
                        
                    }
                    
                    self.navigationController?.pushViewController(detail, animated: true)
                }
                
            }else {
                appdel?.walletTapBool = true
                
                
            }
            
        }
        
    }
    
    @objc func alertAction(){
        
        let alertView = self.storyboard?.instantiateViewController(withIdentifier: "alertDetail") as? AlertDetailViewController
        
        self.navigationController?.pushViewController(alertView!, animated: true)
        
    }
    
    func walletList() {
        
        print("wallet list start")
        
        //디비에 저장된 지갑 조회
        guard let savePKwalletList = DBManager.sharedInstance.selecttWalletKey() as? [[String : Any]] else {
            return
        }
        
        print("wallet save list : \(savePKwalletList)")
        
        activity.startAnimating()
        
        self.walletArray.removeAll()
        AppDelegate.walletListArray.removeAll()
        AppDelegate.favoriesTokenWalletListArray.removeAll()
        
        var tempAmount = 0
        
        var num = 0
        
        let url = "\(AppDelegate.url)/auth/walletList?simbol&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&type=2&language=\(AppDelegate.appLang)"
        
        print("url : \(url)")
        
        let amo = Alamofire.request(url)
        amo.responseJSON(completionHandler: { response in
            
            guard let tempArray = response.result.value as? [[String : Any]] else {
                return
            }
            
            for row in tempArray {
                //                let tempKrwValue = row["openAmt"] as? String ?? ""
                
                var value = row
                
                guard let price = self.stringtoDouble(str: row["lastPrice"] as? String ?? "") else {
                    return
                }
                
                guard let qty = self.stringtoDouble(str: row["dpoQty"] as? String ?? "") else {
                    return
                }
                
                let cointype = row["coinType"] as? String ?? ""
                
                let simbole = row["simbol"] as? String ?? ""
                
                if savePKwalletList.count == 0 {
                    
                    print("wallet savePkwalletlist count 0")
                    value.updateValue("N", forKey: "walletkPkBool")
                    
                    
                }else {
                    
                    for rs in savePKwalletList  {
                        
                        let pkSimbole = rs["symbol"] as? String
                        
                        
                        if simbole == pkSimbole {
                            value.updateValue("Y", forKey: "walletkPkBool")
                            break
                        }else if pkSimbole == "ETH" && (simbole == "KDA" || simbole == "XRP" || simbole == "DPN" || simbole == "FNK" || simbole == "MCVW" || simbole == "BTR" || simbole == "520" || simbole == "EYEAL" || simbole == "METAC" || simbole == "AGO"){
                            value.updateValue("Y", forKey: "walletkPkBool")
                            break
                        }else{
                            value.updateValue("N", forKey: "walletkPkBool")
                        }
                        
                    }
                    
                }
                
                //                AppDelegate.walletListArray.append(value)
                
                //제외 코인
                if simbole != AppDelegate.removeCoin {
                    
                    AppDelegate.walletListArray.append(value)
                    
                }
                
                if AppDelegate.ownCoinList {
                    
                    if qty != 0.0 {
                        
                        if simbole == "KRW" || simbole == "KDP" || simbole == "KDMP" {
                            num += 1
                        }
                        
                        if cointype == "2" {
                            AppDelegate.favoriesTokenWalletListArray.append(value)
                        }else {
                            
//                            self.walletArray.append(value)
                            if simbole != AppDelegate.removeCoin {
                                self.walletArray.append(value)
                            }
                            
                        }
                        
                        var amount = price * qty
                        tempAmount = tempAmount + Int(amount.roundToPlaces(places: 0))
                        
                        if simbole != AppDelegate.removeCoin {
                            
                            var amount = price * qty
                            tempAmount = tempAmount + Int(amount.roundToPlaces(places: 0))
                            
                        }
                        
                    }
                    
                }else {
                    
                    if simbole == "KRW" || simbole == "KDP" || simbole == "KDMP" {
                        num += 1
                    }
                    
                    if cointype == "2" {
                        AppDelegate.favoriesTokenWalletListArray.append(value)
                    }else {
                        
                        //                            self.walletArray.append(value)
                        if simbole != AppDelegate.removeCoin {
                            self.walletArray.append(value)
                        }
                        
                    }
                    
                    
                    var amount = price * qty
                    tempAmount = tempAmount + Int(amount.roundToPlaces(places: 0))
                    
                    
                    
                    //                        if simbole != "DPN" {
                    //
                    //                            var amount = price * qty
                    //                            tempAmount = tempAmount + Int(amount.roundToPlaces(places: 0))
                    //
                    //                        }else {
                    //                            self.walletArray.append(value)
                    //                        }
                    //
                    //                        var amount = price * qty
                    //                        tempAmount = tempAmount + Int(amount.roundToPlaces(places: 0))
                    
                }
                
            }
            
            self.finacalAmount = tempAmount
            
            self.walletCount.text = "\(self.walletArray.count == 0 ? 0 : self.walletArray.count - num)"
            
            if !self.openChartBool {
                
                self.chartSetup()
                
            }
            
            self.walletTV.reloadData()
            
            self.activity.stopAnimating()
        })
    }
    
    func exchangListRT(bt: NPriceTick) {
        print("RT : wallet list --------------------------------------")
        
        var index = 0
        var num = 0
        var dic = [String : Any]()
        
        // 값 수정 ~~ 확인
        let symbol = bt.symbol.toString().components(separatedBy: ["_"]).joined()
        let delta_rate = bt.delta_rate.toString()
        let last_volume = bt.last_volume.toString()
        let last_price = bt.last_price.toString()
        
        print("RT : wallet list last_price : \(last_price)")
        
        let intLast_volume = stringtoDouble(str: last_volume)
        
        //        dic.updateValue(symbol, forKey: "simbol")
        //        dic.updateValue(delta_rate, forKey: "updnRate" )
        //        dic.updateValue(String(intLast_volume!), forKey: "totalVol")
        
        print("RT : wallet list symbole : \(symbol)")
        
        for row in walletArray {
            
            let sym = row as [String : Any]
            
            let tempSymbole = sym["simbol"] as? String ?? ""
            
            print("RT : wallet list symbol : \(tempSymbole)")
            
            print("RT : wallet list symbols : \(symbol)")
            
            print("RT : wallet list test : \(symbol == tempSymbole)")
            
            if symbol == tempSymbole {
                
                num = index
                
                let coinname = sym["coinName"] as? String ?? ""
                
                print("RT coinname : \(coinname)")
                
                //                dic.updateValue(coinname, forKey: "coinName" )
                
                if let intLast_price = Int(last_price){
                    walletArray[index].updateValue(String(intLast_price), forKey: "lastPrice")
                }else {
                    let intLast_price = stringtoDouble(str: last_price)
                    walletArray[index].updateValue(String(intLast_price!), forKey: "lastPrice")
                }
                
                self.realTimeBool = true
                print("RT coinname : \(coinname)")
                print("RT index : \(index)")
                
            }
            
            index = index + 1
        }
        
        print("RT : wallet list Tick : \(dic)")
        let indexpath = IndexPath(row: num, section: 0)
        
        print("RT : wallet list num : \(num)")
        
        DispatchQueue.main.async {
            //            self.walletTV.reloadRows(at: [indexpath], with: .none)
            self.walletTV.reloadData()
        }
        
        
    }
    
    func exchangDetailRT(bt: NPriceTick) {
        
    }
    
    func trandeListRT(bt: NPriceInfo) {
        
    }
    
    func openListRT(bt: NOpenInfo) {
        
    }
    
    func krwChangeRT(value: Double) {
        realTimeBool = true
        
        DispatchQueue.main.async {
            self.walletTV.reloadData()
        }
    }
    
    func stringtoDouble(str : String) -> Double?{
        let temp = str.components(separatedBy: [","]).joined()
        let double = Double(temp)
        return double ?? 0.0
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        if targetContentOffset.pointee.y == 0.0 && targetContentOffset.pointee.y > scrollView.contentOffset.y {
            //            walletList()
        }
        
    }
    
    func alerShowAction() {
        
        if AppDelegate.alertShow {
            circle.isHidden = false
        }else {
            circle.isHidden = true
        }
        
    }
    
    //차트 값 설정 메소드
    func setChart(dataValue : [[String : Any]]) {
        
        var dataEntries: [ChartDataEntry] = []
        
        var colors: [UIColor] = []
        
        if dataValue.count != 0 {
            
            for row in dataValue {
                
                guard let price = self.stringtoDouble(str: row["lastPrice"] as? String ?? "") else {
                    return
                }
                
                guard let qty = self.stringtoDouble(str: row["dpoQty"] as? String ?? "") else {
                    return
                }
                
                let simbole = row["simbol"] as? String ?? ""
                
                var amount = price * qty
                
                print("circle value \(simbole): \(amount) price: \(price), qty : \(qty)")
                
                let value = (amount).roundToPlaces(places: 0)
                
                if amount != 0.0 {
                    
                    let datEntry = PieChartDataEntry(value: value)
                    datEntry.label = simbole
                    
                    dataEntries.append(datEntry)
                    
                    var color = UIColor()
                    
                    switch simbole {
                        
                    case "KRW" :
                        color = UIColor(rgb: 0x21509b)
                    case "KDP" :
                        color = UIColor(rgb: 0x00b9b2)
                    case "BTC" :
                        color = UIColor(rgb: 0xFF9B00)
                    case "ETH" :
                        color = UIColor(rgb: 0x719E71)
                    case "BCH" :
                        color = UIColor(rgb: 0xFFB914)
                    case "LTC" :
                        color = UIColor(rgb: 0x46B4B4)
                    case "KDA" :
                        color = UIColor(rgb: 0x17afa9)
                    case "ETC" :
                        color = UIColor(rgb: 0x006400)
                    case "QTUM" :
                        color = UIColor(rgb: 0x3fa8da)
                    case "TRX" :
                        color = UIColor(rgb: 0x000000)
                    case "XRP" :
                        color = UIColor(rgb: 0x00a5df)
                    case "DPN" :
                        color = UIColor(rgb: 0x0071bd)
                    case "FNK" :
                        color = UIColor(rgb: 0xED6D01)
                    case "MCVW" :
                        color = UIColor(rgb: 0xbca04c)
                    case "BTR" :
                        color = UIColor(rgb: 0xc4afd4)
                    case "520" :
                        color = UIColor(rgb: 0x0076c6)
                    case "EYEAL" :
                        color = UIColor(rgb: 0xf0c600)
                    case "METAC" :
                        color = UIColor(rgb: 0x2f3690)
                    case "AGO" :
                        color = UIColor(rgb: 0x008f9b)
                    default :
                        let red = Double(arc4random_uniform(256))
                        let green = Double(arc4random_uniform(256))
                        let blue = Double(arc4random_uniform(256))
                        
                        color = UIColor(red: CGFloat(red/255), green: CGFloat(green/255), blue: CGFloat(blue/255), alpha: 1)
                    }
                    
                    colors.append(color)
                    
                }
                
            }
            
            if self.finacalAmount == 0 {
                
                let datEntry = PieChartDataEntry(value: 100)
                datEntry.label = ""
                
                dataEntries.append(datEntry)
                
                let color = UIColor.DPsubTextColor
                colors.append(color)
                
            }
            
            circleChart.legendRenderer.legend = circleChart.legend
//            circleChart.legendRenderer.legend = nil
            
        }else {
            
            let datEntry = PieChartDataEntry(value: 100)
            
            dataEntries.append(datEntry)
            
            let color = UIColor.DPsubTextColor
            colors.append(color)
            
            circleChart.legendRenderer.legend = nil
            
        }
        
        let pieChartDataSet = PieChartDataSet(entries: dataEntries, label: "")
        pieChartDataSet.colors = colors
        pieChartDataSet.drawValuesEnabled = false
        pieChartDataSet.entryLabelFont = UIFont.systemFont(ofSize: 8)
        
        let pieChartData = PieChartData(dataSet: pieChartDataSet)
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        paragraphStyle.paragraphSpacing = 0.6
        
        let attrStr = NSMutableAttributedString(string: "navi_total_asset_enter".localized, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16), .paragraphStyle:paragraphStyle])
        
        let tempKrwStr = "\(finacalAmount)".insertComma
        
        attrStr.append(NSMutableAttributedString(string: "\(tempKrwStr)\nKRW", attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 14), .paragraphStyle:paragraphStyle]))
        
        circleChart.drawEntryLabelsEnabled = false
        circleChart.data = pieChartData
        circleChart.animate(xAxisDuration: 1.0, yAxisDuration: 1.0)
        
        circleChart.centerAttributedText = attrStr
        
        circleChart.holeColor = UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1)
        
        circleChart.drawSlicesUnderHoleEnabled = true
        
        circleChart.usePercentValuesEnabled = true
        
        circleChart.drawEntryLabelsEnabled = false
        
        circleChart.holeRadiusPercent = 0.65
        
        circleChart.transparentCircleColor = UIColor.white
        
        circleChart.transparentCircleRadiusPercent = CGFloat(0.8)
        
        circleChart.chartDescription?.text = ""
        
        circleChart.legendRenderer.legend?.horizontalAlignment = .center
        
    }
    
    func openToken() {
        
        if ethIndexNumber != 0 {
            
            self.tokenBtnClick = true
            
            let indexpath = IndexPath(row: self.ethIndexNumber, section: 0)
            tableView(walletTV, didSelectRowAt: indexpath)
            
        }
    }
    
    func selectTokenSwAction(sender: UISwitch) {
        
    }
    
    func sendWalletViAction(value: [String : Any]) {
        
        let appdel = UIApplication.shared.delegate as? AppDelegate
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let detail = storyboard.instantiateViewController(withIdentifier: "walletPager") as? WalletPageViewController {
            detail.coinsimbol = "\(value["simbol"]!)"
            detail.coinValue = "\(value["dpoQty"]!)"
            detail.coinName = "\(value["simbolName"]!)"
            detail.coinType = "\(value["coinType"]!)"
            
            if "\(value["simbol"]!)" == "KRW" || "\(value["simbol"]!)" == "KDMP" || "\(value["simbol"]!)" == "KDP" || "\(value["coinType"]!)" == "2"{
                appdel?.walletTapBool = true
            }else {
                appdel?.walletTapBool = false
            }
            
            self.navigationController?.pushViewController(detail, animated: true)
            
        }
        
    }
    
    func alertReadValue(uid: String, sessionid: String) {
        
        let urlComponents = URLComponents(string:"\(AppDelegate.url)/ajax/getPushLog?uid=\(uid)&sessionId=\(sessionid)&language=\(AppDelegate.appLang)")!
        
        print("urlcomponents : \(urlComponents)")
        
        let amo = Alamofire.request(urlComponents)
        amo.responseJSON { response in
            
            guard let value = response.result.value as? [[String : Any]] else {
                return
            }
            
            var tempValue = [[String:Any]]()
            
            for raw in value {
                let tempType = raw["readYn"] as? String ?? ""
                if tempType == "N"{
                    tempValue.append(raw)
                }
            }
            
            UIApplication.shared.applicationIconBadgeNumber = tempValue.count
            
            print("BadgeNumber : \(UIApplication.shared.applicationIconBadgeNumber)")
            
            
            if tempValue.count != 0 {
                self.circle.isHidden = false
            }else {
                self.circle.isHidden = true
            }
            
        }
        
    }
    
    
    // 해당 코인 심볼 저장
    func privateKeyCreate(simbole : String) {
        
        //        let indexInt = Int(index) ?? 0
        //
        //        let walletJson = walletArray[indexInt]
        //
        //
        //        let id = "fourthCell"
        //
        //        let simbole = "\(walletJson["simbol"]!)"
        //
        //        let pkBool = walletJson["walletkPkBool"] as? String ?? "N"
        
        
        
        activity.startAnimating()
        
        let url = URL(string: "\(AppDelegate.url)auth/makeWallet")
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let param = "uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&symbol=\(simbole)"
        
        print("param : \(param)")
        let paramData = param.data(using: .utf8)
        
        request.httpBody = paramData
        
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        request.setValue(String(describing: paramData!.count), forHTTPHeaderField: "Content-Length")
        
        //laucnhing
        let session = URLSession.shared
        
        let task = session.dataTask(with:request as URLRequest) {data, response, error in
            
            if error == nil {
                
                DispatchQueue.main.async {
                    
                    //activity.stopAnimating()
                    do {
                        let object = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        
                        guard let jsonObject = object else { return }
                        print("inout result : \(jsonObject)")
                        
                        let rsult = jsonObject["success"] as? Int ?? 0
                        let value = jsonObject["model"]  as? [String : Any] ?? [String : Any ]()
                        
                        print("create result : \(rsult)")
                        
                        if rsult == 1 {
                            
                            self.activity.stopAnimating()
                            
                            let pk = value["wletPrikey"] as? String ?? ""
                            let walletNo = value["wletNo"] as? String ?? ""
                            let walletTp = value["wletTp"] as? String ?? ""
                            
                            let saveResult = DBManager.sharedInstance.insertWalletKey(simbol: simbole, privateKey: pk , wletNo: walletNo, wletTp: walletTp )
                            
                            if saveResult {
                                self.privateSaveDone(simbole: simbole, walletNo: walletNo, walletTp: walletTp, procYn: "Y")
                            }else {
                                self.privateSaveDone(simbole: simbole, walletNo: walletNo, walletTp: walletTp, procYn: "N")
                            }
                        }else {
                            self.activity.stopAnimating()
                            
                            let msg = "지갑생성에 실패하였습니다. 잠시 후 다시 시도 부탁드립니다."
                            let alert = UIAlertController(title: "지갑생성", message: msg, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: {(_) in
                                self.view.endEditing(true)
                            }))
                            self.present(alert, animated: false, completion: nil)
                            
                        }
                        
                    }catch{
                        print("cCaught an error:\(error)")
                        
                        self.activity.stopAnimating()
                        
                        let msg = "지갑생성에 실패하였습니다. 잠시 후 다시 시도 부탁드립니다."
                        let alert = UIAlertController(title: "지갑생성", message: msg, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: {(_) in
                            self.view.endEditing(true)
                        }))
                        self.present(alert, animated: false, completion: nil)
                        
                    }
                }
            }else{
                
                print("error:\(error)")
                
            }
        }
        task.resume()
        
    }
    
    func privateSaveDone(simbole: String, walletNo: String, walletTp: String, procYn: String){
        
        //        let walletList = DBManager.sharedInstance.selecttWalletKey()
        
        guard let ad = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let plist = UserDefaults.standard
        guard let uid = plist.string(forKey: "uid") else {
            return
        }
        guard let sessionid = plist.string(forKey: "sessionId") else {
            return
        }
        
        let url = "\(AppDelegate.url)mobile/checkWalletResult?uid=\(uid)&sessionId=\(sessionid)&symbol=\(simbole)&wletNo=\(walletNo)&wletTp=\(walletTp)&procYn=\(procYn)"
        
        print("wallet url : \(url)")
        
        let amo = Alamofire.request(url)
        amo.responseString { response in
            
            if response.result.isSuccess == true {
                guard let result = response.result.value as? String else {
                    return
                }
                
                self.activity.stopAnimating()
                
                print("wallet save checkWalletResult Done : \(result)")
                
                ad.privateSaveDone()
                
                let msg = "\(simbole) 지갑생성이 완료되었습니다."
                let alert = UIAlertController(title: "지갑생성", message: msg, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: {(_) in
                    
                }))
                self.present(alert, animated: false, completion: nil)
                
                self.walletList()
                
            }
            
        }
        
    }
    
}
