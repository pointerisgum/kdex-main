//
//  SideBarViewController.swift
//  crevolc
//
//  Created by crovolc on 2017. 12. 20..
//  Copyright © 2017년 hanbiteni. All rights reserved.
//
// 사이드 메뉴 컨트롤러

import UIKit
import Foundation
import Alamofire

class SideBarViewController : UIViewController, UITableViewDelegate, UITableViewDataSource, RealTimeDelegate {
    
    var revealDelegate : RevealViewController?
    
    let titles = ["main_myinfo".localized, "navi_notification".localized, "navi_guide".localized, "navi_customer_center".localized, "navi_setting".localized]
    
    let icons = [
        
        UIImage(named: "sd_icon_myinfo"),
        UIImage(named: "sd_icon_notice"),
        UIImage(named: "sd_icon_info"),
        UIImage(named: "sd_icon_guide"),
        UIImage(named: "sd_icon_setting")
        
    ]
    
    let deicons = [
        
        UIImage(named: "sd_icon_myinfo"),
        UIImage(named: "sd_icon_notice"),
        UIImage(named: "sd_icon_info"),
        UIImage(named: "sd_icon_guide"),
        UIImage(named: "sd_icon_setting")
    ]
    
    var walletArray = [[String : Any]]()
    
    @IBOutlet var shadowVi: UIView!
    
    let nameLabel = UILabel()
    let coinKRWLabel = UILabel()
    let totalAccetLabel = UILabel()
    let totalInputLabel = UILabel()
    let totalEvalLabel = UILabel()
    let totalProfitLabel = UILabel()
    let profitRadioLabel = UILabel()
    
    let coinKRWValue = UILabel()
    let totalAccetValue = UILabel()
    let totalInputValue = UILabel()
    let totalEvalValue = UILabel()
    let totalProfitValue = UILabel()
    let profitRadioValue = UILabel()
    let closeBtn = UIButton()
    let barHeaderVi = UIView()
    
    var allAccet = 0.0 //총보유자산
    var allAmount = 0.0 //총매수
    var allTest = 0.0 //총평가
    var allTestProfit = 0.0 //평가손익
    var profitRate = 0.0 //수익률
    
    let line = UIView()
    let line2 = UIView()
    
    @IBOutlet var sideHeaderView: UIView!
    
    @IBOutlet var sideTableView: UITableView!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        sideHeaderView.translatesAutoresizingMaskIntoConstraints = false
//        
//        sideHeaderView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        
        var topLayoutGuide: CGFloat = sideHeaderView.frame.size.height
        var topPadding : CGFloat = 0
        
        if let nav = self.navigationController {
            print("side header navigationController")
            topLayoutGuide += nav.navigationBar.frame.size.height
        }
        
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            topPadding = window?.safeAreaInsets.top ?? 0.0
            if topPadding != 0.0 {
                topLayoutGuide += 24
                topPadding = 24
            }
            
        }
        
        sideTableView.register(UINib(nibName: "sideCell", bundle: nil), forCellReuseIdentifier: "sideCellId")
        
        sideHeaderView.frame.size.height += topLayoutGuide
        
        sideTableView.frame.origin.y = topLayoutGuide
        sideTableView.layoutMargins = UIEdgeInsets.zero
        sideTableView.separatorInset = UIEdgeInsets.zero
        
        sideTableView.delegate = self
        sideTableView.dataSource = self
        
        sideHeaderView.backgroundColor = UIColor(rgb: 0xf2f2f2)
        
        sideHeaderView.addSubview(nameLabel)
        sideHeaderView.addSubview(closeBtn)
        sideHeaderView.addSubview(barHeaderVi)
        
//        25 + topPadding
        self.nameLabel.frame = CGRect(x: 10, y: 3 + topPadding , width: 100, height: 30)
        self.nameLabel.text = "\(AppDelegate.userName)"
        self.nameLabel.textColor = UIColor(rgb: 0x1b2f42)
        self.nameLabel.font = UIFont.boldSystemFont(ofSize: 15)
        self.nameLabel.backgroundColor = UIColor.clear
        
        let img = UIImage(named: "sd_btn_close")
        
//        30 + topPadding
        self.closeBtn.frame = CGRect(x: 230, y: 5 + topPadding , width: 25, height: 25)
        self.closeBtn.setImage(img, for: .normal)
        self.closeBtn.addTarget(self, action: #selector(closeSidebar), for: .touchUpInside)
        
        print("header heigth : \(sideHeaderView.frame.height)")
        
        self.barHeaderVi.frame = CGRect(x: 0, y: 62, width: sideHeaderView.frame.width, height: 1)
        self.barHeaderVi.backgroundColor = UIColor(rgb: 0x1b2f42)
        
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 190))
        headerView.backgroundColor = UIColor(rgb: 0xf2f2f2)
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 1))
        footerView.backgroundColor = UIColor.DPtableFooterBgColor
        
        self.line.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width, height: 50)
        self.line.backgroundColor = UIColor.white
        
        self.line2.frame = CGRect(x: 0, y: 190 , width: self.view.frame.width, height: 1)
        self.line2.backgroundColor = UIColor.gray
        
        self.sideTableView.tableHeaderView = headerView
        self.sideTableView.tableFooterView = footerView
        
//        self.view.backgroundColor = UIColor.DPViewBackgroundColor
        
        headerView.addSubview(self.line)
//        headerView.addSubview(self.line2)
        
        self.coinKRWLabel.frame = CGRect(x: 10, y: 10, width: 100, height: 30)
        self.coinKRWLabel.text = "navi_my_krw_money".localized
        self.coinKRWLabel.textColor = UIColor.DPmainTextColor
        self.coinKRWLabel.textAlignment = .left
        self.coinKRWLabel.font = UIFont.boldSystemFont(ofSize: 13)
        self.coinKRWLabel.backgroundColor = UIColor.clear
        
        self.coinKRWValue.frame = CGRect(x: 110, y: 10, width: 130, height: 30)
        self.coinKRWValue.text = "0"
        self.coinKRWValue.textColor = UIColor.DPmainTextColor
        self.coinKRWValue.textAlignment = .right
        self.coinKRWValue.font = UIFont.boldSystemFont(ofSize: 13)
        self.coinKRWValue.backgroundColor = UIColor.clear
        
//        headerView.addSubview(self.coinKRWLabel)
//        headerView.addSubview(self.coinKRWValue)
        
//        self.totalAccetLabel.frame = CGRect(x: 10, y: 40, width: 100, height: 30)
        self.totalAccetLabel.frame = CGRect(x: 10, y: 10, width: 100, height: 30)
        self.totalAccetLabel.text = "navi_total_asset".localized
        self.totalAccetLabel.textColor = UIColor.DPmainTextColor
        self.totalAccetLabel.font = UIFont.boldSystemFont(ofSize: 13)
        self.totalAccetLabel.backgroundColor = UIColor.clear
        
//        self.totalAccetValue.frame = CGRect(x: 110, y: 40, width: 130, height: 30)
        self.totalAccetValue.frame = CGRect(x: 110, y: 10, width: 140, height: 30)
        self.totalAccetValue.text = AppDelegate.totalCoinValue
        self.totalAccetValue.textColor = UIColor.DPmainTextColor
        self.totalAccetValue.textAlignment = .right
        self.totalAccetValue.font = UIFont.boldSystemFont(ofSize: 13)
        self.totalAccetValue.backgroundColor = UIColor.clear
        
        headerView.addSubview(self.totalAccetLabel)
        headerView.addSubview(self.totalAccetValue)
        
//        self.totalInputLabel.frame = CGRect(x: 10, y: 90, width: 100, height: 30)
        self.totalInputLabel.frame = CGRect(x: 10, y: 60, width: 100, height: 30)
        self.totalInputLabel.text = "navi_total_bid".localized
        self.totalInputLabel.textColor = UIColor.DPmainTextColor
        self.totalInputLabel.font = UIFont.systemFont(ofSize: 12)
        self.totalInputLabel.backgroundColor = UIColor.clear
        
//        self.totalInputValue.frame = CGRect(x: 110, y: 90, width: 130, height: 30)
        self.totalInputValue.frame = CGRect(x: 110, y: 60, width: 140, height: 30)
        self.totalInputValue.text = "0"
        self.totalInputValue.textColor = UIColor.DPmainTextColor
        self.totalInputValue.textAlignment = .right
        self.totalInputValue.font = UIFont.systemFont(ofSize: 13)
        self.totalInputValue.backgroundColor = UIColor.clear
        
        headerView.addSubview(self.totalInputLabel)
        headerView.addSubview(self.totalInputValue)
        
//        self.totalEvalLabel.frame = CGRect(x: 10, y: 120, width: 100, height: 30)
        self.totalEvalLabel.frame = CGRect(x: 10, y: 90, width: 100, height: 30)
        self.totalEvalLabel.text = "navi_total_eval".localized
        self.totalEvalLabel.textColor = UIColor.DPmainTextColor
        self.totalEvalLabel.font = UIFont.systemFont(ofSize: 12)
        self.totalEvalLabel.backgroundColor = UIColor.clear
        
//        self.totalEvalValue.frame = CGRect(x: 110, y: 120, width: 130, height: 30)
        self.totalEvalValue.frame = CGRect(x: 110, y: 90, width: 140, height: 30)
        self.totalEvalValue.text = "0"
        self.totalEvalValue.textColor = UIColor.DPmainTextColor
        self.totalEvalValue.textAlignment = .right
        self.totalEvalValue.font = UIFont.systemFont(ofSize: 13)
        self.totalEvalValue.backgroundColor = UIColor.clear
        
        headerView.addSubview(self.totalEvalLabel)
        headerView.addSubview(self.totalEvalValue)
        
//        self.totalProfitLabel.frame = CGRect(x: 10, y: 150, width: 100, height: 30)
        self.totalProfitLabel.frame = CGRect(x: 10, y: 120, width: 100, height: 30)
        self.totalProfitLabel.text = "navi_eval_pl".localized
        self.totalProfitLabel.textColor = UIColor.DPmainTextColor
        self.totalProfitLabel.font = UIFont.systemFont(ofSize: 12)
        self.totalProfitLabel.backgroundColor = UIColor.clear
        
//        self.totalProfitValue.frame = CGRect(x: 110, y: 150, width: 130, height: 30)
        self.totalProfitValue.frame = CGRect(x: 110, y: 120, width: 140, height: 30)
        self.totalProfitValue.text = "0"
        self.totalProfitValue.textColor = UIColor.DPSellDefaultColor
        self.totalProfitValue.textAlignment = .right
        self.totalProfitValue.font = UIFont.systemFont(ofSize: 13)
        self.totalProfitValue.backgroundColor = UIColor.clear
        
        headerView.addSubview(self.totalProfitLabel)
        headerView.addSubview(self.totalProfitValue)
        
//        self.profitRadioLabel.frame = CGRect(x: 10, y: 180, width: 100, height: 30)
        self.profitRadioLabel.frame = CGRect(x: 10, y: 150, width: 100, height: 30)
        self.profitRadioLabel.text = "navi_rate".localized
        self.profitRadioLabel.textColor = UIColor.DPmainTextColor
        self.profitRadioLabel.font = UIFont.systemFont(ofSize: 12)
        self.profitRadioLabel.backgroundColor = UIColor.clear
        
//        self.profitRadioValue.frame = CGRect(x: 110, y: 180, width: 130, height: 30)
        self.profitRadioValue.frame = CGRect(x: 110, y: 150, width: 140, height: 30)
        self.profitRadioValue.text = "0 %"
        self.profitRadioValue.textColor = UIColor.DPSellDefaultColor
        self.profitRadioValue.textAlignment = .right
        self.profitRadioValue.font = UIFont.systemFont(ofSize: 13)
        self.profitRadioValue.backgroundColor = UIColor.clear
        
        headerView.addSubview(self.profitRadioLabel)
        headerView.addSubview(self.profitRadioValue)
        
        let image = UIImage(named: "shadow")
        
        self.shadowVi.backgroundColor = UIColor(patternImage: image!)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("sidebar viewWillAppear")
        walletList()
        TcpSocket.sharedInstance.receiverThread?.delegate = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.titles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        print("\(indexPath.row)")
        
        let id = "sideCellId"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: id) as? sideCell else {
            return UITableViewCell()
        }
        
        
        cell.textLb?.text = self.titles[indexPath.row]
        
        cell.img?.image = self.icons[indexPath.row]
        
        cell.layoutMargins = UIEdgeInsets.zero
        
        cell.textLb?.font = UIFont.systemFont(ofSize: 14)
        cell.textLb?.textColor = UIColor.DPmainTextColor
        
        cell.backgroundColor = UIColor.DPtableCellBgColor
        
        return cell
    }
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.reloadData()
        guard let cell = tableView.cellForRow(at: indexPath) as? sideCell else {
            return
        }
        
        let row = indexPath.row
        
        switch row {
        case 0 :
            
//            if let vi = self.parent as? RevealViewController {
//                vi.pageNum = 4
//            }
            
            AppDelegate.sideBarInfoBool = true
            
            closeSidebar()
            
//            let alert = UIAlertController(title: "pop_alert".localized, message: "pop_Ready".localized, preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
//            self.present(alert, animated: true, completion: nil)
            
        case 1 :
            cell.textLb?.textColor = UIColor.DPTabBarIndicateTintColor
//            cell.imageView?.image = self.deicons[1]
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let detail = storyboard.instantiateViewController(withIdentifier: "NoticeNav") as? UINavigationController {
                self.present(detail, animated: true, completion: {
                    print("texter~~~~closesideBar")
                    self.revealDelegate?.closeSideBar(nil)
                })
            }
//            let alert = UIAlertController(title: "알림", message: "지원 준비중입니다.", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "확인", style: .cancel, handler: nil))
//            self.present(alert, animated: true, completion: nil)
            
        case 2 :
            cell.textLb?.textColor = UIColor.DPTabBarIndicateTintColor
//            cell.imageView?.image = self.deicons[2]
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let detail = storyboard.instantiateViewController(withIdentifier: "InfoNav") as? UINavigationController {
                self.present(detail, animated: true, completion: {
                    print("texter~~~~closesideBar")
                    self.revealDelegate?.closeSideBar(nil)
                })
            }
            
//            let alert = UIAlertController(title: "알림", message: "지원 준비중입니다.", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "확인", style: .cancel, handler: nil))
//            self.present(alert, animated: true, completion: nil)
            
        case 3 :
            cell.textLb?.textColor = UIColor.DPTabBarIndicateTintColor
//            cell.imageView?.image = self.deicons[2]
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let detail = storyboard.instantiateViewController(withIdentifier: "customNav") as? UINavigationController {
                self.present(detail, animated: true, completion: {
                    print("texter~~~~closesideBar")
                    self.revealDelegate?.closeSideBar(nil)
                })
            }
            
//            let alert = UIAlertController(title: "알림", message: "지원 준비중입니다.", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "확인", style: .cancel, handler: nil))
//            self.present(alert, animated: true, completion: nil)
            
        case 4 :
            cell.textLb?.textColor = UIColor.DPTabBarIndicateTintColor
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let detail = storyboard.instantiateViewController(withIdentifier: "setupNav") as? UINavigationController {
                self.present(detail, animated: true, completion: {
                    print("texter~~~~closesideBar")
                    self.revealDelegate?.closeSideBar(nil)
                })
            }
            print("test")
            
        default:
            print("no page")
        }
    }
    
    @objc func closeSidebar(){
        self.revealDelegate?.closeSideBar(nil)
    }
    
    func walletList() {
        
        allAccet = 0 //총보유자산
        allAmount = 0 //총매수
        allTest = 0 //총평가
        allTestProfit = 0 //평가손익
        profitRate = 0 //수익률
        
//        activity.startAnimating()
        
        let url = "\(AppDelegate.url)/auth/walletList?simbol&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&type=2"
        
        print("walleturl \(url)")
        let amo = Alamofire.request(url)
        amo.responseJSON(completionHandler: { response in
            
            guard let tempArray = response.result.value as? [[String : Any]] else {
                return
            }
            
            self.walletArray = tempArray
            
            var tempAmount = 0
            
            for row in tempArray {
                //                let tempKrwValue = row["openAmt"] as? String ?? ""
                
                guard let price = self.stringtoDouble(str: row["lastPrice"] as? String ?? "") else {
                    return
                }
                guard let qty = self.stringtoDouble(str: row["dpoQty"] as? String ?? "") else {
                    return
                }
                guard let openAmt = self.stringtoDouble(str: row["openAmt"] as? String ?? "") else {
                    return
                }
                guard let evalAmt = self.stringtoDouble(str: row["evalAmt"] as? String ?? "") else {
                    return
                }
                
                
                self.allAmount = openAmt + self.allAmount
                self.allTest = evalAmt + self.allTest
                
                var amount = price * qty
                
                tempAmount = tempAmount + Int(amount.roundToPlaces(places: 0))
                
            }
            
            self.allAccet = self.stringtoDouble(str: "\(tempAmount)") ?? 0
            self.allTestProfit = self.allTest - self.allAmount
            
            if self.allAmount == 0 {
                self.profitRate = 0
            }else {
                self.profitRate = (self.allTestProfit / self.allAmount) * 100
            }
            
            self.totalAccetValue.text = "\(tempAmount)".insertComma + " KRW"
            self.totalInputValue.text = "\(Int(self.allAmount.roundToPlaces(places: 0)))".insertComma + " KRW"
            self.totalEvalValue.text = "\(Int(self.allTest.roundToPlaces(places: 0)))".insertComma + " KRW"
            self.totalProfitValue.text = "\(Int(self.allTestProfit.roundToPlaces(places: 0)))".insertComma + " KRW"
            self.profitRadioValue.text = "\(String(format: "%.2f", self.profitRate)) %"
            
            if self.allTestProfit < 0 {
                self.totalProfitValue.textColor = UIColor.DPMinTextColor
            }else if self.allTestProfit == 0 {
                self.totalProfitValue.textColor = UIColor.DPmainTextColor
            }else {
                self.totalProfitValue.textColor = UIColor.DPPlusTextColor
            }
            
            if self.profitRate < 0 {
                self.profitRadioValue.textColor = UIColor.DPMinTextColor
            }else if self.profitRate == 0 {
                self.profitRadioValue.textColor = UIColor.DPmainTextColor
            }else {
                self.profitRadioValue.textColor = UIColor.DPPlusTextColor
            }
        })
    }
    
    public func stringtoDouble(str : String) -> Double?{
        let temp = str.components(separatedBy: [","]).joined()
        let double = Double(temp)
        return double ?? 0.0
    }
    
    func exchangListRT(bt: NPriceTick) {
        
        print("RT sidebar real Time")
        
        allAccet = 0 //총보유자산
        allAmount = 0 //총매수
        allTest = 0 //총평가
        allTestProfit = 0 //평가손익
        profitRate = 0 //수익률
        
        var index = 0
        var num = 0
        var dic = [String : Any]()
        
        let realSymbol = bt.symbol.toString().components(separatedBy: [" "]).joined()
        let delta_rate = bt.delta_rate.toString()
        let last_volume = bt.last_volume.toString()
        let last_price = bt.last_price.toString()
        let intLast_volume = stringtoDouble(str: last_volume)
        let doubleLastPrice = stringtoDouble(str: last_price) ?? 0
        
        dic.updateValue(delta_rate, forKey: "lastPrice" )
        
            var tempAmount = 0
            
            for row in self.walletArray {
                //                let tempKrwValue = row["openAmt"] as? String ?? ""
                num = index
                
                guard let simbol = row["simbol"] as? String else {
                    return
                }
                guard let price = self.stringtoDouble(str: row["lastPrice"] as? String ?? "") else {
                    return
                }
                guard let qty = self.stringtoDouble(str: row["dpoQty"] as? String ?? "") else {
                    return
                }
                guard let openAmt = self.stringtoDouble(str: row["openAmt"] as? String ?? "") else {
                    return
                }
                guard let evalAmt = self.stringtoDouble(str: row["evalAmt"] as? String ?? "") else {
                    return
                }
                
//                var oldLatPrice = price //과거 시세
                let oldEvalQty =  evalAmt / price //평가수량
                
                var amount = 0.0
                var tempEvalAmt = 0.0
                print("RT sidebar real Time simbol : \(simbol)")
                print("RT sidebar real Time realsimbol : \(realSymbol)")
                print("RT sidebar real Time price : \(price)")
                print("RT sidebar real Time doubleLastPrice : \(doubleLastPrice)")
                print("RT sidebar real Time last_volume : \(last_volume)")
                
                if simbol == realSymbol {
                    print("RT sidebar real Time true")
                    tempEvalAmt = oldEvalQty * doubleLastPrice //현재 평가액
                    self.allTest = tempEvalAmt + self.allTest
                    amount = qty * doubleLastPrice
                    self.walletArray[num].updateValue("\(doubleLastPrice)", forKey: "lastPrice")
                    self.walletArray[num].updateValue("\(tempEvalAmt)", forKey: "evalAmt")
                }else {
                    self.allTest = evalAmt + self.allTest
                    amount = qty * price
                }
                
                self.allAmount = openAmt + self.allAmount
                
                tempAmount = tempAmount + Int(amount.roundToPlaces(places: 0))
                
                index = index + 1
                
            }
        
            self.allAccet = self.stringtoDouble(str: "\(tempAmount)") ?? 0
            self.allTestProfit = self.allTest - self.allAmount
            self.profitRate = (self.allTestProfit / self.allAmount) * 100
        
        AppDelegate.walletListArray = self.walletArray
        
        DispatchQueue.main.async {
            print("RT sidebar real Time main async queue")
            print("RT sidebar real Time tempAmount :\(tempAmount)")
            print("RT sidebar real Time tempAmount :\(self.allTest)")
            
            self.totalAccetValue.text = "\(tempAmount)".insertComma + " KRW"
            self.totalInputValue.text = "\(Int(self.allAmount.roundToPlaces(places: 0)))".insertComma + " KRW"
            self.totalEvalValue.text = "\(Int(self.allTest.roundToPlaces(places: 0)))".insertComma + " KRW"
            self.totalProfitValue.text = "\(Int(self.allTestProfit.roundToPlaces(places: 0)))".insertComma + " KRW"
            self.profitRadioValue.text = "\(String(format: "%.2f", self.profitRate)) %"
            
            if self.allTestProfit < 0 {
                self.totalProfitLabel.textColor = UIColor.DPMinTextColor
            }else if self.allTestProfit == 0 {
                self.totalProfitLabel.textColor = UIColor.DPmainTextColor
            }else {
                self.totalProfitLabel.textColor = UIColor.DPPlusTextColor
            }
            
            if self.profitRate < 0 {
                self.profitRadioValue.textColor = UIColor.DPMinTextColor
            }else if self.profitRate == 0 {
                self.profitRadioValue.textColor = UIColor.DPmainTextColor
            }else {
                self.profitRadioValue.textColor = UIColor.DPPlusTextColor
            }
            
        }
        
    }
    
    func exchangDetailRT(bt: NPriceTick) {
        
        
    }
    
    func trandeListRT(bt: NPriceInfo) {
        
        
    }
    
    func openListRT(bt: NOpenInfo) {
        
        
    }
    
    func krwChangeRT(value: Double) {
        
        
    }
    
}
