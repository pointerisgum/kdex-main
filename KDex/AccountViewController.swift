//
//  AccountViewController.swift
//  KDex
//
//  Created by parkheewon on 2021/07/13.
//  Copyright © 2021 hanbiteni. All rights reserved.
//

import Foundation
import UIKit

class AccountViewController: UIViewController {
    
    @IBOutlet weak var accountNameField: UITextField!
    
    @IBOutlet weak var bankNameField: UITextField!
    
    @IBOutlet weak var accountNumField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = "출금계좌"
        
    }
    
    @IBAction func accountModBtn(_ sender: Any) {
        
        var message = ""
        var validataionBool = false
        
        if self.accountNameField.text ?? "" == "" {
            validataionBool = true
            message = "예금주를 입력해주세요"
            
        }else if self.bankNameField.text ?? "" == "" {
            validataionBool = true
            message = "은행명을 입력해주세요"
        }else if self.accountNumField.text ?? "" == "" {
            validataionBool = true
            message = "은행명을 입력해주세요"
        }
        
        if validataionBool {
            self.showToast(message: message, textcolor: UIColor.white, frame:CGRect(x: self.view.frame.size.width/2 - 100, y: self.view.frame.size.height-100, width: 200, height: 35), time: 0.2, animation: true)
        }else {
            self.callSave()
        }
        
    }
    
    func callSave(){
        let activity = UIActivityIndicatorView()
        activity.style = .whiteLarge
        activity.color = UIColor.DPActivityColor
        activity.hidesWhenStopped = true
        activity.startAnimating()
        
        var accountNum = ""
        
        var accountNumber = [
             "한국" : "001" ,
             "산업" : "002" ,
             "기업" : "003" ,
             "국민" : "004" ,
             "수협" : "007" ,
             "수출입" : "008" ,
             "농협" : "011" ,
             "축협" : "012" ,
             "우리" : "020" ,
             "제일" : "023" ,
             "씨티" : "027" ,
             "대구" : "031" ,
             "부산" : "032" ,
             "광주" : "034" ,
             "제주" : "035" ,
             "전북" : "037" ,
             "경남" : "039" ,
             "새마을" : "045" ,
             "신협" : "048" ,
             "우체국" : "071" ,
             "하나" : "081" ,
             "신한" : "088" ,
             "케이" : "089" ,
             "카카오" : "090"
        ]
        
        for (key, value) in accountNumber {
            let backName = self.bankNameField.text ?? ""
            if backName.contains(key){
                accountNum = value
                break
            }
        }
        
        let param = "uid=\(AppDelegate.uid)&holder_name=\(self.accountNameField.text ?? "")&bank_name=\(self.bankNameField.text ?? "")&acct_no=\(self.accountNumField.text ?? "")&bank_code=\(accountNum)"
        
        print(param)
        
        let paramData = param.data(using: .utf8)
        
        let url = URL(string: "\(AppDelegate.url)/mypage/acctNumSave")
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        request.httpBody = paramData
        
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        request.setValue(String(describing: paramData!.count), forHTTPHeaderField: "Content-Length")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if let err = error  {
                
                DispatchQueue.main.async {
                    let msg = "통신 장애로 수정에 실패하였습니다."
                    let alert = UIAlertController(title: "출금계좌", message: msg, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "확인", style: .cancel, handler: nil))
                    self.present(alert, animated: false, completion: nil)
                    
                    activity.stopAnimating()
                }
                
                return
                
            }else{
                DispatchQueue.main.async {
                let msg = "정상적으로 수정되었습니다."
                let alert = UIAlertController(title: "출금계좌", message: msg, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "확인", style: .cancel, handler: { (_) in
                        self.dismiss(animated: true, completion: nil)
                    }))
                self.present(alert, animated: false, completion: nil)
                
                activity.stopAnimating()
                
                }
              
            }
            
        }
        task.resume()
        
    }
    
}
