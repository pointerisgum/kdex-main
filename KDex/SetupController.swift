//
//  SetupController.swift
//  crevolc
//
//  Created by crovolc on 2018. 1. 29..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import SystemConfiguration

class SetupController : UITableViewController {
    
    let appID = Bundle.bundleIdentifier
    
    let appIDNum = "1387360646"

    let message = "setup_version_update_ready".localized
    
    let okBtnTitle = "setup_version_now".localized
    
    let cancelBtnTitle = "setup_version_late".localized
    
    private var updateBool = false
    
    @IBOutlet var applockLb: UILabel!
    
//    @IBOutlet var alertSW: UISwitch!
//    //@IBOutlet var loginSW: UISwitch!
//    @IBOutlet var tradeSW: UISwitch!
    
    @IBOutlet var versionLabel: UILabel!
    
    @IBOutlet var updateBtn: UIButton!
    
    @IBAction func backBtn(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }

    @IBAction func updateAction(_ sender: Any) {
        versionUpdate()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.backgroundColor = UIColor.DPtableViewBgColor
        self.tableView.layoutMargins = UIEdgeInsets.zero
        self.tableView.separatorInset = UIEdgeInsets.zero
        updateBtn.isHidden = true
//        alertSW.onTintColor = UIColor.DPbdidBgColor
//        tradeSW.onTintColor = UIColor.DPbdidBgColor
        
        initTitle()
        
//        tradeSW.addTarget(self, action: #selector(trandAction), for: .valueChanged)
//
//        alertSW.addTarget(self, action: #selector(allAction), for: .valueChanged)
        
        updateCheck()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.initstart()
    }
    
    func initTitle(){
        
//        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
//
//        print("version : \(version)")
        
        let nTitle = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 40))
        
        nTitle.textAlignment = .center
        nTitle.font = UIFont.boldSystemFont(ofSize: 15)
        nTitle.textColor = UIColor.white
        nTitle.text = "action_bar_setting".localized
        nTitle.textAlignment = .center
        
        self.navigationItem.titleView = nTitle
        
        
        self.navigationItem.titleView?.frame.size = CGSize(width: 300, height: 40)
        
        self.navigationController?.navigationBar.topItem?.title = ""
        
        self.navigationController?.navigationBar.barTintColor = UIColor.DPNaviBarTintColor
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            self.performSegue(withIdentifier: "applock", sender: self)
        }
    }
    
    func initstart(){
        let plist = UserDefaults.standard
        
        if plist.bool(forKey:"appLock") {
            applockLb.text = "On"
        }else {
            applockLb.text = "Off"
        }
        
        var urlComponents = URLComponents(string:"\(AppDelegate.url)/mobile/getOrderAlram?uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)")!
        let amo = Alamofire.request(urlComponents)
        amo.responseString { (respons) in
            
            if respons.result.isSuccess {
                
                guard let result = respons.result.value else {
                    return
                }
//                if result == "Y" {
//                    self.tradeSW.isOn = true
//                }else {
//                    self.tradeSW.isOn = false
//                }
            }
        }
        
    }
    
    @objc func trandAction(){
        var procYn = ""
        
//        if tradeSW.isOn {
//            procYn = "Y"
//        }else{
//            procYn = "N"
//        }
        
        print("trans Action")
        
        var urlComponents = URLComponents(string:"\(AppDelegate.url)/mobile/setOrderAlram?uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&procYn=\(procYn)")!
        let amo = Alamofire.request(urlComponents)
        amo.responseString { (respons) in
            
            if respons.result.isSuccess {
                print("success")
            }
        }
    }
    
    @objc func allAction(){
//        if alertSW.isOn == false {
//            tradeSW.isOn = false
//            self.trandAction()
//        }else {
//            print("on")
//        }
    }
    
    
    // app update start
    
    private func isConnectedWork() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1){
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        })
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
        
    }
    
    func  updateCheck ()  {
        if ( isConnectedWork() ) {
            print("app update connetion success")
            
            let checkVersion = needUpdate()
            
            let ckappVersion = checkVersion["ckappVersion"] as! String
            
            print("ckappversion: \(ckappVersion)")
            print("bundele version : \(Bundle.appVersion)")
            
            if checkVersion["storeVersion"] != nil {
                
                let storeVersion = checkVersion["storeVersion"] as! String
                
                print("storeVersion: \(storeVersion)")
                self.versionLabel.text = "\(ckappVersion)"
                
                print("needupdate : \(checkVersion["needUpdate"])")
                
                if checkVersion["needUpdate"] as! Bool {
                    self.updateBool = true
                     updateBtn.isHidden = false
                    
//                    self.appLastDownloadbtn.setTitle("최신 버전은 \(storeVersion) 입니다.", for: .normal)
//                    self.appLastDownloadbtn.addTarget(self, action: #selector(versionUpdate), for: .touchUpInside)
                }else {
                    self.updateBool = false
                    updateBtn.isHidden = true
//                    self.appVersionLabel.text = "현재 버전 정보 \(ckappVersion)"
//                    self.appLastDownloadbtn.setTitle("현재 최신 버전입니다.", for: .normal)
                }
                
            }else{
                
                self.versionLabel.text = "\(Bundle.appVersion)" //.\(Bundle.appBuildVersion)
                
//                self.appLastDownloadbtn.setTitle("최신버전을 확인 할 수 없는 상태입니다", for: .normal)
            }
            
        } else {
            let versionCode = Bundle.appVersion
//            let buildNumber = Bundle.appBuildVersion
            self.versionLabel.text = "setup_version_not_network".localized //번들 정보 .\(buildNumber)
//            self.appLastDownloadbtn.setTitle("네트워크 비활성화로 최신버전을 알 수가 없습니다.", for: .normal)
        }
        
    }
    
    func needUpdate() -> NSDictionary {
        
        let ret = NSMutableDictionary()
        
        let jsonData = getJSON(urlToRequest: "http://itunes.apple.com/kr/lookup?bundleId="+self.appID)
        let lookup = try? JSONSerialization.jsonObject(with: jsonData as Data, options:  JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
        
        print("lookup version : \(lookup)")
        
        let ckappVersion = Bundle.appVersion //번들 버전
        ret.setValue(ckappVersion, forKey: "ckappVersion")
        
        
        if ( (lookup!["resultCount"] as AnyObject).integerValue == 1 ) {
            let resultsArray = lookup!["results"] as! NSArray // 결과값
            let resultsDic = resultsArray[0] as! NSDictionary
            let storeVersion = resultsDic["version"] as! String //스토어 버전
            ret.setValue(storeVersion, forKey: "storeVersion")
            
            if (storeVersion as NSString).compare(ckappVersion, options:.caseInsensitive) == ComparisonResult.orderedDescending {
                ret.setValue(true, forKey: "needUpdate")
            }else {
                ret.setValue(false, forKey: "needUpdate")
            }
            ret.setValue(storeVersion, forKey: "storeVersion")
        }
        return ret
    }
    
    func getJSON(urlToRequest: String) -> NSData {
        
        print("url version : \(urlToRequest)")
        var data = NSData()
        do {
            data = try NSData(contentsOf: NSURL(string: urlToRequest)! as URL, options: NSData.ReadingOptions() )
        } catch let error as NSError {
            print( error.description )
        } catch {}
        return data
    }
    
    private func versionUpdate(){
        
        let alert = UIAlertController(title: "app_update".localized, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: okBtnTitle, style: .default, handler: { Void in
            guard let url = URL(string: "itms-apps://itunes.apple.com/app/id\(self.appIDNum)") else { return }
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        })
        
        alert.addAction(okAction)
        
        let cancelAction = UIAlertAction(title: cancelBtnTitle, style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)

    }
    
}

