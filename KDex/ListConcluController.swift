//
//  ListConcluController.swift
//  crevolc
//
//  Created by crovolc on 2018. 1. 19..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import Kingfisher

class ListConcluController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var coinSimbol : String? //코인 아이디
    
    var coinName : String? //코인 이름
    
//    var coinSimbol : String? //코인 아이디
    
    var jsonArray : [[String : Any]]?
    
    var concluArray = [[String : Any]]()
    
    var jsondic : NSMutableDictionary?
    
    var allconluBool : Bool = false
    
    var activity = UIActivityIndicatorView()
    
    var marksimbol : String?
    
    @IBOutlet var listconclutable: UITableView!
    
    //테이블 헤더 작업 -----------
    
    let headerView : UIView = {
        let uv = UIView()
        uv.backgroundColor = UIColor.DPtableHeaderBgColor
        return uv
    }()
    
    let tb_Label : UILabel = {
        let lb = UILabel()
        lb.text = "pending_all_select".localized
        lb.textColor = UIColor(rgb: 0x00b9b2)
        lb.textAlignment = .left
        lb.baselineAdjustment = .alignCenters
        lb.font = UIFont.systemFont(ofSize: 14)
        lb.translatesAutoresizingMaskIntoConstraints  = false
        return lb
    }()
    
    let tb_allBtn : UIButton = {
        let btn = UIButton()
        
        btn.setImage(UIImage(named: "checkbox_off"), for: .normal)
        btn.backgroundColor = UIColor.clear
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(selectConclu), for: .touchUpInside)
        return btn
    }()
    
    let tb_selectBtn : UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("pending_cancel_select_order".localized, for: .normal)
        btn.titleLabel?.textAlignment = .left
        btn.setTitleColor(UIColor(rgb: 0x666666), for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.backgroundColor = UIColor.clear
        btn.addTarget(self, action: #selector(selectConclu), for: .touchUpInside)
        return btn
    }()
    
    //테이블 헤더 작업 end -----------
    
    let footerView: UIView = {
        let uv = UIView()
        uv.backgroundColor = UIColor(rgb: 0xf3f4f5)
        return uv
    }()
    
    let footer_Label : UILabel = {
        let lb = UILabel()
        lb.text = "pending_list_no_list_txt".localized
        lb.textColor = UIColor.DPmainTextColor
        lb.textAlignment = .center
        lb.font = UIFont.systemFont(ofSize: 16)
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activity.frame = CGRect(x: self.view.frame.width/2 - 50, y: self.listconclutable.frame.height/2 - 100, width: 100, height: 100)
        activity.style = .whiteLarge
        activity.color = UIColor.DPActivityColor
        activity.hidesWhenStopped = true
        listconclutable.addSubview(activity)
        
        listconclutable.backgroundColor = UIColor(rgb: 0xf3f4f5)
        listconclutable.layoutMargins = UIEdgeInsets.zero
        listconclutable.separatorStyle = .none
        self.view.backgroundColor = UIColor.DPViewBackgroundColor
        
        let nib = UINib(nibName: "ListConCluCellNib", bundle: nil)
        listconclutable.register(nib, forCellReuseIdentifier: "listcell")
        
        //트레이딩 뷰에서 사용
        if coinSimbol != nil {
            let plist = UserDefaults.standard
            self.marksimbol = plist.string(forKey: "marketSimbol")
            
            self.navigationController?.navigationBar.tintColor = UIColor.white
            
            self.navigationController?.navigationBar.topItem?.title = "\(coinName)(\(coinSimbol!.replacingOccurrences(of: "_", with: "/")))"
        }
        
        setupheaderView()
//        chatMoving()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        chatMoving()
    }
    
    //매칭 테이블(listTable)헤더 설정
    func setupheaderView(){
        
        headerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30)
        headerView.backgroundColor = UIColor(rgb: 0xf3f4f5)
        
        [tb_Label, tb_allBtn, tb_selectBtn].forEach { self.headerView.addSubview($0) }
        
        footerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 100)
        footerView.backgroundColor = UIColor(rgb: 0xf3f4f5)
        
        footerView.addSubview(footer_Label)
        
        setupconstrains()
        
    }
    
    func setupconstrains() {
       
        self.tb_allBtn.bottomAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
        self.tb_allBtn.widthAnchor.constraint(equalToConstant: 20).isActive = true
        self.tb_allBtn.heightAnchor.constraint(equalToConstant: 20).isActive = true
        self.tb_allBtn.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 16).isActive = true
        
        self.tb_Label.centerYAnchor.constraint(equalTo: tb_allBtn.centerYAnchor).isActive = true
        self.tb_Label.leadingAnchor.constraint(equalTo: tb_allBtn.trailingAnchor, constant: 6).isActive = true
        self.tb_Label.widthAnchor.constraint(equalToConstant: 100).isActive = true
        self.tb_Label.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        self.tb_selectBtn.centerYAnchor.constraint(equalTo: tb_allBtn.centerYAnchor).isActive = true
        self.tb_selectBtn.widthAnchor.constraint(equalToConstant: 100).isActive = true
        self.tb_selectBtn.heightAnchor.constraint(equalToConstant: 15).isActive = true
        self.tb_selectBtn.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: -10).isActive = true
        
        self.footer_Label.topAnchor.constraint(equalTo: footerView.topAnchor).isActive = true
        self.footer_Label.leadingAnchor.constraint(equalTo: footerView.leadingAnchor).isActive = true
        self.footer_Label.rightAnchor.constraint(equalTo: footerView.rightAnchor).isActive = true
        self.footer_Label.bottomAnchor.constraint(equalTo: footerView.bottomAnchor).isActive = true
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.jsonArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listcell", for: indexPath) as? ListConCluCell
        cell?.delegate = self
        cell?.indexRow = indexPath.row
        cell?.selectionStyle = .none
        cell?.backgroundColor = UIColor(rgb: 0xf3f4f5)
        cell?.layoutMargins = UIEdgeInsets.zero
        
        guard let tempData = self.jsonArray?[indexPath.row] else {
            return cell!
        }
        
        print("\(tempData["ordrNo"])")
        let buyType = tempData["byslTp"] as? String ?? ""
        let dateStr = tempData["ordrTime"] as? String ?? ""
        let ordrPrc = tempData["ordrPrc"] as? String ?? ""
        let doubleOrdrPrc = stringtoDouble(str: ordrPrc) ?? 0.0
        var simbole = tempData["instCd"] as? String ?? ""
        let mtchPrc = tempData["mtchPrc"] as? String ?? "0"
        let mtchQty = tempData["remnQty"] as? String ?? "0"
        let doubleMtcQty = stringtoDouble(str: mtchQty) ?? 0.0
        let ordrQty = tempData["ordrQty"] as? String ?? "0"
        let doubleOrdrQty = stringtoDouble(str: ordrQty) ?? 0.0
        
        var market = ""
        
//        cell?.coinLabel.text = simbole.replacingOccurrences(of: "_", with: "/")
        
        cell?.dateLabel.text = self.makeTime(date: dateStr ?? "")

        if simbole.contains("_") {
            let simMaket = simbole.components(separatedBy: "_")
            simbole = simMaket[0]
            market = simMaket[1]
        }else {
            market = ""
        }
        
        
        if market == "KRW" || market == "" {
            cell?.orderPriceLabe.text = "\(ordrPrc.insertComma)"
        }else {
            cell?.orderPriceLabe.text = "\(String(format: "%0.8f", doubleOrdrPrc))"
        }
        
        var attr = NSMutableAttributedString(attributedString: NSAttributedString(string: "\(simbole)", attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor(rgb: 0x1b2f42)]))
        
        if market != "" {
            attr.append(NSAttributedString(string: "/\(market)", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12), NSAttributedString.Key.foregroundColor : UIColor(rgb: 0x8c8c8d)]))
        }
        
        cell?.coinLabel.attributedText = attr
        
        cell?.orderCountLabel.text = "\(ordrQty)"
        cell?.confirmCountLabel.text = "\(String(format: "%0.8f", doubleOrdrQty - doubleMtcQty))"
        cell?.concluCountLabel.text = "\(mtchQty)"
        
        if buyType == "B" {
            cell?.sellBuyLabel.text = "fragment_title_bid".localized
            cell?.sellBuyLabel.textColor = UIColor.DPBuyDefaultColor
        }else {
            cell?.sellBuyLabel.text = "fragment_title_ask".localized
            cell?.sellBuyLabel.textColor = UIColor.DPSellDefaultColor
        }
        
        let isCheck = (tempData["isClick"] as? String) ?? ""
        
        if isCheck == "Y" {
            let deimg = UIImage(named: "checkbox_on")
            cell?.isSelector.image = deimg
        }else if allconluBool {
            let deimg = UIImage(named: "checkbox_on")
            cell?.isSelector.image = deimg
        }else {
            let img = UIImage(named: "checkbox_off")
            cell?.isSelector.image = img
        }
        
        cell?.simboleImg.kf.setImage(with: AppDelegate.dataImageUrl(str: simbole))
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 129
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentCell = tableView.cellForRow(at: indexPath) as? ListConCluCell
        
        let img = UIImage(named: "checkbox_off")
        let deimg = UIImage(named: "checkbox_on")
        
        if (currentCell?.selectBtn)! {
            print("con detail isclick N ")
            currentCell?.isSelector.image = img
            self.jsonArray![indexPath.row].updateValue("N", forKey: "isClick")
            currentCell?.selectBtn = false
        }else{
            print("con detail isclick N ")
            currentCell?.isSelector.image = deimg
            self.jsonArray![indexPath.row].updateValue("Y", forKey: "isClick")
            currentCell?.selectBtn = true
        }
        
        print("con detail : \(self.jsonArray![indexPath.row]) ")
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return self.headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return self.footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 100
    }
    
    func chatMoving(){
        
        activity.startAnimating()
        
        var urlComponents = URLComponents()
        
        //트래딩 뷰를 위해 설정
        if coinSimbol != nil {
            
            urlComponents = URLComponents(string: "\(AppDelegate.url)/auth/matchingReady?simbol=\(coinSimbol!)&sessionId=\(AppDelegate.sessionid)&uid=\(AppDelegate.uid)")!
            
        }else {
            urlComponents = URLComponents(string: "\(AppDelegate.url)/auth/matchingReady?simbol&sessionId=\(AppDelegate.sessionid)&uid=\(AppDelegate.uid)")!
        }
        
        let amo = Alamofire.request(urlComponents)
        amo.responseJSON(completionHandler:
            { response in
                
                self.jsonArray = [[String : Any]]()
                
                print(response.result.isSuccess)
                
                guard let tempJson = response.result.value as? [[String : Any]] else {
                    return
                }
                
                for row in tempJson {
                    
                    print("row : \(row)")
                    var value = row
                    value.updateValue("N", forKey: "isClick")
                    
                    print("value : \(value)")
                    
                    self.jsonArray?.append(value)
                }
                
                print("conclu jsonarray count : \(self.jsonArray?.count)")
                
                self.activity.stopAnimating()
                
                self.listconclutable.reloadData()
                
                if self.jsonArray?.count == 0 {
                    self.footer_Label.isHidden = false
                    self.footerView.isHidden = false
                    
                }else {
                    self.footer_Label.isHidden = true
                    self.footerView.isHidden = true
                }
        })
        
    }
    
    //날짜 형식 포멧
    func makeTime(date : String) -> String {
        
        let df : DateFormatter = DateFormatter()
        
        df.dateFormat = "yyyyMMddHHmmss"
        
        let date_time : NSDate = (df.date(from: date) as? NSDate)!
        
        df.dateFormat = "yy.MM.dd HH:mm:ss"
        
        let newDate : String = df.string(from: date_time as Date) as String
        
        return newDate
    }
    
    @objc func selectConclu(sender : UIButton){
        
        print("")
        
        if sender == tb_allBtn {
            if allconluBool {
                allconluBool = false
                self.listconclutable.reloadData()
            }else{
                allconluBool = true
                self.listconclutable.reloadData()
            }
        }else {
            
            if self.jsonArray?.count == 0 {
                showToast(message: "pending_list_no_list_txt".localized, textcolor: UIColor.white, frame: CGRect(x: self.view.frame.size.width/2 - 100, y: self.view.frame.size.height - 100  , width: 200, height: 35), time: 1, animation: true)
                
            }else {
                
                let url = URL(string: "\(AppDelegate.url)/auth/orderCancle")
                
                var request = URLRequest(url: url!)
                
                request.httpMethod = "POST"
                
                for row in self.jsonArray! {
                    
                    print("con detail select action: \(row["isClick"])")
                    
                    guard let isCli = row["isClick"] as? String else {
                        return
                    }
                    
                    if allconluBool {
                        
                        httppostAction(row: row)
                        
                    }else {
                        
                        if  isCli == "Y" {
                            
                            httppostAction(row: row)
                            
                        }
                        
                    }
                }
                
            }
            
            
        }
        
        
        
    }
    
    func httppostAction(row : [String : Any]){
        
        let url = URL(string: "\(AppDelegate.url)/auth/orderCancle")
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        var param = ""
        
        if coinSimbol != nil {
            
            param = "uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&simbol=\(coinSimbol!)&oriOrderNo=\(row["ordrNo"]!)&cancleOrderQty=\(row["remnQty"]!)&waysType=M"
            
        }else {
            param = "uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&simbol=\(row["instCd"]!)&oriOrderNo=\(row["ordrNo"]!)&cancleOrderQty=\(row["remnQty"]!)&waysType=M"
            
        }
        
        print("param : \(param)")
        
        let paramData = param.data(using: .utf8)
        
        request.httpBody = paramData
        
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        request.setValue(String(describing: paramData!.count), forHTTPHeaderField: "Content-Length")
        
        let session = URLSession.shared
        
        let task = session.dataTask(with:request as URLRequest) {data, response, error in
            
            let result = String(data: data!, encoding: String.Encoding.utf8) as? String
            
            if error == nil {
                
                print("con detail inout success concluDetail : \(result)")
                
                self.chatMoving()
                
            }else{
                
                print("error:\(error)")
                
            }
            
        }
        
        task.resume()
        
    }
    
    func stringtoDouble(str : String) -> Double?{
        let temp = str.components(separatedBy: [","]).joined()
        let double = Double(temp)
        return double ?? 0.0
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        if targetContentOffset.pointee.y == 0.0 && targetContentOffset.pointee.y > scrollView.contentOffset.y {
            chatMoving()
        }
        
    }
}
