//
//  showHoldingController.swift
//  KDex
//
//  Created by park heewon on 2018. 6. 27..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class ShowHoldingController: UIViewController, UITextFieldDelegate, RealTimeDelegate {
    
    var superViewController : NewHodingController?
 
    var walletInfo = [String : Any]()
    
    var modifyBool = false

    @IBOutlet var closeBtn: UIButton!
    
    @IBOutlet var conformBtn: UIButton!
    
    @IBOutlet var coinImage: UIImageView!
    
    @IBOutlet var currentLb: UILabel!
    
    @IBOutlet var openQty: UILabel! //보유코인
    
    @IBOutlet var openAmt: UILabel! //총 매수금액
    
    @IBOutlet var evalAmt: UILabel! //총 평가금액
    
    @IBOutlet var evalPl: UILabel! //총 평가손익
    
    @IBOutlet var evalPlRt: UILabel! //총 수익률
    
    @IBOutlet var evalBidPrice: UITextField! //매수 평균가
    
    let rigthVi : UIView = {
        let vi = UIView()
        vi.backgroundColor = UIColor.clear
        return vi
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        closeBtn.layer.borderColor = UIColor.DPmainTextColor.cgColor
        closeBtn.layer.borderWidth = 1
        
        evalBidPrice.backgroundColor = UIColor.DPPopupBgColor
        rigthVi.frame = CGRect(x: 0, y: 0, width: 5, height: evalBidPrice.frame.height)
        evalBidPrice.rightView = rigthVi
        evalBidPrice.rightViewMode = .always
        evalBidPrice.layer.borderColor = UIColor.DPLineBgColor.cgColor
        evalBidPrice.layer.borderWidth = 1
            
        evalBidPrice.addTarget(self, action: #selector(textfieldChageEvent), for:.editingChanged)
        
        self.view.backgroundColor = UIColor.DPPopupBgColor
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        initValue()
        
        TcpSocket.sharedInstance.receiverThread?.delegate = self
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let str = textField.text ?? ""
        
        let dbstr = stringtoDouble(str: str)
        
        return true
    }
    
    func initValue() {
        
        //        openQty : 보유수량
        //        openAmt : 매수금액
        //        argPrice : 매수평균가
        //        evalAmt : 평가금액
        //        evalPlRt : 손익율
        //        evalPl : 평가손익
        
        let imageName = walletInfo["simbol"] as? String ?? ""
        let openQty = walletInfo["openQty"] as? String ?? ""
        let openAmt = walletInfo["openAmt"] as? String ?? ""
        let lastPrice = walletInfo["lastPrice"] as? String ?? ""
        let argPrice = walletInfo["argPrice"] as? String ?? ""
        let evalAmt = walletInfo["evalAmt"] as? String ?? ""
        let evalPlRt = walletInfo["evalPlRt"] as? String ?? ""
        let evalPl = walletInfo["evalPl"] as? String ?? ""
        let flag = walletInfo["editFlag"] as? String ?? ""
        
        if flag == "1" {
            closeBtn.setTitle("my_asset_init".localized, for: .normal)
        }else {
            closeBtn.setTitle("but_close".localized, for: .normal)
        }
        
        self.coinImage.image = UIImage(named: imageName)
        self.currentLb.text = "\(self.intToStringComma(data: lastPrice)) KRW"
        self.openQty.text = "\(openQty) \(imageName)"
        self.openAmt.text = "\(self.intToStringComma(data: openAmt)) KRW"
        self.evalBidPrice.text = "\(self.intToStringComma(data: argPrice))"
        self.evalAmt.text = "\(self.intToStringComma(data: evalAmt)) KRW"
        self.evalPlRt.text = "\(String(format: "%.2f", stringtoDouble(str: evalPlRt) ?? 0.0))%"
        self.evalPl.text = "\(self.intToStringComma(data: evalPl)) KRW"
        
        let dbevalPl = stringtoDouble(str: evalPl) ?? 0.0
        let dbevalPlRt = stringtoDouble(str: evalPlRt) ?? 0.0
        
        if dbevalPl < 0 {
            self.evalPl.textColor = UIColor.DPMinTextColor
        }else if dbevalPl == 0 {
            self.evalPl.textColor = UIColor.DPmainTextColor
        }else {
            self.evalPl.textColor = UIColor.DPPlusTextColor
        }
        
        if dbevalPlRt < 0 {
            self.evalPlRt.textColor = UIColor.DPMinTextColor
        }else if dbevalPlRt == 0 {
            self.evalPlRt.textColor = UIColor.DPmainTextColor
        }else {
            self.evalPlRt.textColor = UIColor.DPPlusTextColor
        }
        
        
    }
    
    func setupValue(value: String) {
        
        //        openQty : 보유수량
        
        //        openAmt : 매수금액
        //        argPrice : 매수평균가
        //        evalAmt : 평가금액
        //        evalPlRt : 손익율
        //        evalPl : 평가손익
        
        let argPrice = value
        let openQty = walletInfo["openQty"] as? String ?? ""
        let lastPrice = walletInfo["lastPrice"] as? String ?? ""
        let imageName = walletInfo["simbol"] as? String ?? ""
        
        let dbOpenQty = stringtoDouble(str: openQty) ?? 0.0
        let dblastPrice = stringtoDouble(str: lastPrice) ?? 0.0
        let dbargPrice = stringtoDouble(str: value) ?? 0.0
        let dbopenAmt = dbOpenQty * dbargPrice
        let dbevalAmt = dbOpenQty * dblastPrice
        let dbevalPl = dbevalAmt - dbopenAmt
        let dbevalPlRt = (dbevalPl / dbopenAmt) * 100
        
        let openAmt = "\(dbopenAmt)"
        let evalAmt = "\(dbevalAmt)"
        let evalPl = "\(dbevalPl)"
        let evalPlRt = "\(dbevalPlRt)"
        
        self.coinImage.image = UIImage(named: imageName)
        self.currentLb.text = "\(self.intToStringComma(data: lastPrice)) KRW"
        
        self.openQty.text = "\(openQty) \(imageName)"
        self.openAmt.text = "\(self.intToStringComma(data: openAmt)) KRW"
        self.evalBidPrice.text = "\(self.intToStringComma(data: argPrice))"
        self.evalAmt.text = "\(self.intToStringComma(data: evalAmt)) KRW"
        
        self.evalPl.text = "\(self.intToStringComma(data: evalPl)) KRW"
        
        if argPrice != "0" {
            self.evalPlRt.text = "\(String(format: "%.2f", stringtoDouble(str: evalPlRt) ?? 0.0))%"
        }else {
            self.evalPlRt.text = "0%"
        }
        
        if dbevalPl < 0 {
            self.evalPl.textColor = UIColor.DPMinTextColor
        }else if dbevalPl == 0 {
            self.evalPl.textColor = UIColor.DPmainTextColor
        }else {
            self.evalPl.textColor = UIColor.DPPlusTextColor
        }
        
        if dbevalPlRt < 0 {
            self.evalPlRt.textColor = UIColor.DPMinTextColor
        }else if dbevalPlRt == 0 {
            self.evalPlRt.textColor = UIColor.DPmainTextColor
        }else {
            self.evalPlRt.textColor = UIColor.DPPlusTextColor
        }
        
    }
    
    @IBAction func closeBtnAction(_ sender: Any) {
        
        let flag = walletInfo["editFlag"] as? String ?? ""
        
        if flag == "1" {
            
            let alert = UIAlertController(title: "my_asset_init_title".localized, message: "my_asset_init_inform".localized, preferredStyle: .alert )
            alert.addAction(UIAlertAction(title: "확인", style: .default, handler: { (_) in
                
                self.corfirmAction(type: "2", price: "")
                
                UIView.animate(withDuration: 0.25, animations: {
                    self.view.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
                    self.view.alpha = 0.0
                }) { (finished: Bool) in
                    if(finished)
                    {
                        self.view.removeFromSuperview()
                        self.superViewController?.walletList()
                    }
                }
                
            }))
            
            alert.addAction(UIAlertAction(title: "common_no_btn".localized, style: .cancel, handler: nil))
            
            self.present(alert, animated: false, completion: nil)
            
        }else {
            
            UIView.animate(withDuration: 0.25, animations: {
                self.view.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
                self.view.alpha = 0.0
            }) { (finished: Bool) in
                if(finished)
                {
                    self.view.removeFromSuperview()
                }
            }
            
        }
        
    }
    
    @IBAction func conformBtnAction(_ sender: Any) {
        
        let value = evalBidPrice.text ?? ""
        
        if modifyBool {
            let alert = UIAlertController(title: "my_asset_modify_title".localized, message: "my_asset_modify_inform".localized, preferredStyle: .alert )
            alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .default, handler: { (_) in
                
                self.corfirmAction(type: "1", price: value)
                
                UIView.animate(withDuration: 0.25, animations: {
                    self.view.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
                    self.view.alpha = 0.0
                }) { (finished: Bool) in
                    if(finished)
                    {
                        self.view.removeFromSuperview()
                        self.superViewController?.walletList()
                    }
                }
                
            }))
            
            alert.addAction(UIAlertAction(title: "common_no_btn".localized, style: .cancel, handler: nil))
            
            self.present(alert, animated: false, completion: nil)
        }else {
            
            UIView.animate(withDuration: 0.25, animations: {
                self.view.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
                self.view.alpha = 0.0
            }) { (finished: Bool) in
                if(finished)
                {
                    self.view.removeFromSuperview()
                    
                }
            }
            
        }
        
    }
    
    func intToStringComma(data : String) -> String {
        
        var doubleData = stringtoDouble(str: data) ?? 0.0
        let intData = Int(doubleData.roundToPlaces(places: 0))
        
        let strData = "\(intData)"
        
        return strData.insertComma
        
    }
    
    func stringtoDouble(str : String) -> Double?{
        let temp = str.components(separatedBy: [","]).joined()
        let double = Double(temp)
        return double ?? 0.0
    }
    
    @objc func textfieldChageEvent() {
        
        modifyBool = true
        
        self.conformBtn.setTitle("my_asset_modify".localized, for: .normal)
        
        var str = evalBidPrice.text ?? ""
        
        if str == "" {
            str = "0"
        }
        
        setupValue(value: str)
    
    }
    
    func exchangListRT(bt: NPriceTick) {
        print("RT sidebar real Time")
        
        let realSymbol = bt.symbol.toString().components(separatedBy: [" "]).joined()
//        let delta_rate = bt.delta_rate.toString()
        let last_volume = bt.last_volume.toString()
        let last_price = bt.last_price.toString()
//        let intLast_volume = stringtoDouble(str: last_volume)
//        let doubleLastPrice = stringtoDouble(str: last_price) ?? 0
        
        let simbole = walletInfo["simbol"] as? String ?? ""
        
        
        if realSymbol == simbole {
            walletInfo.updateValue(last_price, forKey: "lastPrice")
            
            DispatchQueue.main.async {
                let argPrice = self.evalBidPrice.text ?? ""
                
                self.setupValue(value: argPrice)
                self.currentLb.text = "\(self.intToStringComma(data: last_price)) KRW"
            }
            
        }
        
        
    }
    
    func exchangDetailRT(bt: NPriceTick) {
        
    }
    
    func trandeListRT(bt: NPriceInfo) {
        
    }
    
    func openListRT(bt: NOpenInfo) {
        
    }
    
    func krwChangeRT(value: Double) {
        
    }
    
    func corfirmAction(type: String, price : String){
        let simbole = walletInfo["simbol"] as? String ?? ""
        
        let tempPrice = Int(stringtoDouble(str: price) ?? 0.0)
        
        let urlComponents = URLComponents(string:"\(AppDelegate.url)auth/editAvgPrice?uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&symbol=\(simbole)&type=\(type)&price=\(tempPrice)")!
        
        print("urlComponents : \(urlComponents)")
        
        let amo = Alamofire.request(urlComponents)
        amo.responseJSON(completionHandler:
            { response in
                
                print("respone : \(response)")
                guard let tempjson = response.result.value as? [String : Any] else {
                    return
                }
                
                print("temp : \(tempjson)")
                
        })
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        evalBidPrice.resignFirstResponder()
    }
    
}

