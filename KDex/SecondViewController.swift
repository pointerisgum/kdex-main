//
//  SecondViewController.swift
//  crevolc
//
//  Created by park heewon on 2017. 12. 12..
//  Copyright © 2017년 hanbiteni. All rights reserved.
//
//  거래소 두번째 탭바 컨트롤러

import Foundation
import UIKit
import Alamofire

class SecondViewController : PagerController, PagerDataSource, AlertShowDelegate {
    
    var revealDelegate : RevealViewController?
    
    var showSideBar = true 
    
    let circle : UIView = {
        let vi = UIView()
        vi.backgroundColor = UIColor(rgb: 0xe9a01b)
        vi.frame = CGRect(x: 40, y: 5, width: 5, height: 5)
        vi.layer.cornerRadius = vi.frame.size.width / 2
        vi.clipsToBounds = true
        vi.isHidden = true
        return vi
    }()


    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor(red:0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        
        self.dataSource = self
        
        registerStoryboardControllers()
        customizeTab()
        
        initTitle()
        initbarbtn()
//        initNextbtn()
        
        let dragLeft = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(moveSide(_:)))
        dragLeft.edges = UIRectEdge.left
        self.view.addGestureRecognizer(dragLeft)
        
        let dragRight = UISwipeGestureRecognizer(target: self, action: #selector(moveSide(_:)))
       // let torchEvent = UITapGestureRecognizer(target: self, action: #selector(moveSide(_:)))
        
        dragRight.direction = .left
        self.view.addGestureRecognizer(dragRight)
        //self.view.addGestureRecognizer(torchEvent)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("view controller viewdidappear")
        initNextbtn()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.alertReadValue(uid: AppDelegate.uid, sessionid: AppDelegate.sessionid)
        
        AlertShowController.sharedInstance.alertDelegate = self
    }
  
    func initTitle(){
        
        let nTitle = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 40))
        
        nTitle.textAlignment = .center
        nTitle.font = UIFont.boldSystemFont(ofSize: 15)
        nTitle.textColor = UIColor.white
        nTitle.text = "action_bar_transaction_history".localized
        nTitle.textAlignment = .left
        
        self.navigationItem.titleView = nTitle
        self.navigationItem.titleView?.frame.size = CGSize(width: 300, height: 40)
        self.navigationController?.navigationBar.barTintColor = UIColor.DPNaviBarTintColor
        
    }
    
    func initbarbtn(){
        
        //if let revealVC = self.revealViewController() {
        
        let icon = UIImage(named: "icn_appbar_menu")
        
        let item = UIBarButtonItem(image:icon , style: .plain, target: self, action: nil)
        
        item.tintColor = UIColor.white
        
        item.target = self
        
        item.action = #selector(moveSide)
        
        self.navigationItem.leftBarButtonItem = item
        
        self.navigationController?.addShadowToBar(vi: self)
        
        //self.view.addGestureRecognizer(revealVC.panGestureRecognizer())
        
        //}
        
    }
    
    func initNextbtn(){
        
        let totalVI = UIView()
        totalVI.backgroundColor = UIColor.clear
        totalVI.frame = CGRect(x: 0, y: 0, width: 70, height: 37)
        totalVI.tintColor = UIColor.DPNaviBartextTintColor
        
        let icon3 = UIImage(named: "alarm")
        
        //알림 리스트 버튼
        let rightbtn3 = UIButton(type: .system)
        rightbtn3.frame = CGRect(x: 35, y: 5, width: 30, height: 30)
        rightbtn3.setImage(icon3, for: .normal)
        rightbtn3.addTarget(self, action: #selector(alertAction), for: .touchUpInside)
        
        totalVI.addSubview(rightbtn3)
        totalVI.addSubview(circle)
        
        let icon4 = UIImage(named: "homt")
        
        let rightbtn4 = UIButton(type: .system)
        rightbtn4.frame = CGRect(x: 0, y: 8, width: 25, height: 25)
        rightbtn4.setImage(icon4, for: .normal)
        rightbtn4.addTarget(self, action: #selector(homeAction), for: .touchUpInside)
        
        totalVI.addSubview(rightbtn4)
        
        let item = UIBarButtonItem(customView: totalVI)
        self.navigationItem.rightBarButtonItem = item
    }
    
    @objc func moveSide(_ sender: Any) {
        
        let torchEvent = UITapGestureRecognizer(target: self, action: #selector(moveSide(_:)))
        
        if sender is UIScreenEdgePanGestureRecognizer{
            self.revealDelegate?.openSideBar(nil)
        }else if sender is UISwipeGestureRecognizer{
            self.revealDelegate?.closeSideBar(nil)
        }else if sender is UITapGestureRecognizer{
            self.revealDelegate?.closeSideBar(nil)
            self.view.removeGestureRecognizer((sender as? UITapGestureRecognizer)!)
        }else if sender is UIBarButtonItem{
            if self.revealDelegate?.isSideBarShowing == false{
                self.view.addGestureRecognizer(torchEvent)
                self.revealDelegate?.openSideBar(nil)
            } else {
                self.revealDelegate?.closeSideBar(nil)
            }
        }
        
    }
    
    func registerStoryboardControllers() {
        
        let storyboard = UIStoryboard(name: "List", bundle: nil)
        
        let controller1 = storyboard.instantiateViewController(withIdentifier: "Li_trade") as? ListTradeController
        let controller2 = storyboard.instantiateViewController(withIdentifier: "Li_conclu") as? ListConcluController
        
        
        self.setupPager(tabNames: ["action_bar_transaction_history".localized, "fragment_title_pending".localized], tabControllers: [controller1!, controller2!])
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("view touchesBegan")
    }
    
    func customizeTab() {
        indicatorColor = UIColor.white
        tabsViewBackgroundColor = UIColor.DPtableViewBgColor
        contentViewBackgroundColor = UIColor.DPtableViewBgColor
        
        startFromSecondTab = false
        centerCurrentTab = true
        tabLocation = PagerTabLocation.top
        tabHeight = 49
        tabOffset = 36
        tabWidth = self.view.frame.size.width / 2
        fixFormerTabsPositions = false
        fixLaterTabsPosition = false
        animation = PagerAnimation.during
        selectedTabTextColor = UIColor.DPmainTextColor
        tabsTextFont = UIFont.boldSystemFont(ofSize: 15)
        tabsTextColor = UIColor.DPsubTextColor
    }
    
    // Programatically selecting a tab. This function is getting called on AppDelegate
    func changeTab() {
        self.selectTabAtIndex(2)
    }
    
    @objc func homeAction() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        if let detail = storyboard.instantiateViewController(withIdentifier: "homeViPop") as? HomeViewController  {
            
            guard let appdel = UIApplication.shared.delegate as? AppDelegate else {
                return
            }
            
            appdel.window?.rootViewController?.present(detail, animated: true, completion: nil)
        }
        
    }
    
    @objc func alertAction(){
        
        let alertView = self.storyboard?.instantiateViewController(withIdentifier: "alertDetail") as? AlertDetailViewController
        
        self.navigationController?.pushViewController(alertView!, animated: true)
        
    }
    
    func alerShowAction() {
        
        if AppDelegate.alertShow {
            circle.isHidden = false
        }else {
            circle.isHidden = true
        }
        
    }
    
    func alertReadValue(uid: String, sessionid: String) {
        
        let urlComponents = URLComponents(string:"\(AppDelegate.url)/ajax/getPushLog?uid=\(uid)&sessionId=\(sessionid)&language=\(AppDelegate.appLang)")!
        
        print("urlcomponents : \(urlComponents)")
        
        let amo = Alamofire.request(urlComponents)
        amo.responseJSON { response in
            
            guard let value = response.result.value as? [[String : Any]] else {
                return
            }
            
            var tempValue = [[String:Any]]()
            
            for raw in value {
                let tempType = raw["readYn"] as? String ?? ""
                if tempType == "N"{
                    tempValue.append(raw)
                }
            }
            
            UIApplication.shared.applicationIconBadgeNumber = tempValue.count
            
            print("BadgeNumber : \(UIApplication.shared.applicationIconBadgeNumber)")
            
            
            if tempValue.count != 0 {
                self.circle.isHidden = false
            }else {
                self.circle.isHidden = true
            }
            
        }
    }
    
}
