//
//  ListController.swift
//  crevolc
//
//  Created by crovolc on 2018. 1. 19..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import UIKit
import Foundation


//삭제 예정 컨트롤러
class ListController : PagerController, PagerDataSource {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Listcontroller viewdidLoad")
        
        self.view.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        
        self.dataSource = self 
        
        registerStoryboardControllers()
        
        customizeTab()
        
        initNextbtn()
        
    }
    
    func initNextbtn(){
        let totalVI = UIView()
        totalVI.frame = CGRect(x: 0, y: 0, width: 120, height: 37)
        totalVI.tintColor = UIColor.white
        
        let icon1 = UIImage(named: "icn_appbar_alert")
        let icon2 = UIImage(named: "icn_appbar_talk")
        
        let rightbtn1 = UIButton(type: .system)
        rightbtn1.frame = CGRect(x: 45, y: 5, width: 30, height: 30)
        rightbtn1.setImage(icon1, for: .normal)
        let rightbtn2 = UIButton(type: .system)
        rightbtn2.frame = CGRect(x: 85, y: 5, width: 30, height: 30)
        rightbtn2.setImage(icon2, for: .normal)
        
        totalVI.addSubview(rightbtn1)
        totalVI.addSubview(rightbtn2)
        
        let item = UIBarButtonItem(customView: totalVI)
        
        self.navigationItem.rightBarButtonItem = item
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = ""
        
    }
    
    func registerStoryboardControllers() {
        
        let storyboard = UIStoryboard(name: "List", bundle: nil)
        
        let controller1 = storyboard.instantiateViewController(withIdentifier: "Li_trade") as? ListTradeController
        let controller2 = storyboard.instantiateViewController(withIdentifier: "Li_conclu") as? ListConcluController
        
        self.setupPager(tabNames: ["action_bar_transaction_history".localized, "fragment_title_pending".localized], tabControllers: [controller1!, controller2!])
        
    }
    
    func customizeTab() {
        indicatorColor = UIColor.white
        tabsViewBackgroundColor = UIColor.DPtableViewBgColor
        contentViewBackgroundColor = UIColor.DPtableViewBgColor
        
        startFromSecondTab = false
        centerCurrentTab = true
        tabLocation = PagerTabLocation.top
        tabHeight = 49
        tabOffset = 36
        tabWidth = self.view.frame.size.width / 2
        fixFormerTabsPositions = false
        fixLaterTabsPosition = false
        animation = PagerAnimation.during
        selectedTabTextColor = UIColor.DPmainTextColor
        tabsTextFont = UIFont.boldSystemFont(ofSize: 15)
        tabsTextColor = UIColor.DPsubTextColor
    }
    
    func changeTab() {
        self.selectTabAtIndex(2)
    }
   
}
