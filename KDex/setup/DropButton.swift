//
//  DropButton.swift
//  KDex
//
//  Created by park heewon on 2018. 7. 31..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation

protocol DropDownProtocol {
    func dropDownPressed(str : String)
}

class DropButton : UIButton, DropDownProtocol {
    
    var dropVi = DropDownView()
    
    var height = NSLayoutConstraint()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.backgroundColor = UIColor.white
        
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor(rgb: 0xeaeaea).cgColor
        
        dropVi = DropDownView.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        dropVi.delegate = self
        dropVi.translatesAutoresizingMaskIntoConstraints = false
        
    }
    
    override func didMoveToSuperview() {
        
        print("dropdownbutton didMovewtosuperview")
        self.superview?.addSubview(dropVi)
        self.superview?.bringSubviewToFront(dropVi)
        
        dropVi.topAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        dropVi.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        dropVi.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
        height = dropVi.heightAnchor.constraint(equalToConstant: 0)
        
    }
    
    var isOpen = false
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if isOpen == false {
            
            isOpen = true
            
            NSLayoutConstraint.deactivate([self.height])            
//            if self.dropVi.tableVi.contentSize.height > 150 {
//                self.height.constant = 150
//            }else {
//
//            }
            self.height.constant = self.dropVi.tableVi.contentSize.height
            
            NSLayoutConstraint.activate([self.height])
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
                
                self.dropVi.layoutIfNeeded()
                self.dropVi.center.y += self.dropVi.frame.height / 2
                
            }, completion: nil)
            
        }else {
            isOpen = false
            
            NSLayoutConstraint.deactivate([self.height])
            self.height.constant = 0
            NSLayoutConstraint.activate([self.height])
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
                
                self.dropVi.center.y -= self.dropVi.frame.height / 2
                self.dropVi.layoutIfNeeded()
                
            }, completion: nil)
            
            
        }
    }
    
    func dropDownPressed(str: String) {
        self.setTitle(str, for: .normal)
        
        isOpen = false
        
        NSLayoutConstraint.deactivate([self.height])
        self.height.constant = 0
        NSLayoutConstraint.activate([self.height])
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
            
            self.dropVi.center.y -= self.dropVi.frame.height / 2
            self.dropVi.layoutIfNeeded()
            
        }, completion: nil)
        
    }
    
}

class DropDownView : UIView, UITableViewDataSource, UITableViewDelegate {
    
    var dropDownOption = [String]()
    
    var delegate : DropDownProtocol!
    
    var tableVi = UITableView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        tableVi.dataSource = self
        tableVi.delegate = self
        
        tableVi.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(tableVi)
        
        tableVi.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        tableVi.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        tableVi.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        tableVi.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("error")
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return dropDownOption.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        
        cell.textLabel?.text = dropDownOption[indexPath.row]
        cell.textLabel?.textColor = UIColor(rgb: 0x666666)
        cell.textLabel?.textAlignment = .center
        cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
        cell.backgroundColor = UIColor.white
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor(rgb: 0xeaeaea).cgColor
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        delegate.dropDownPressed(str: dropDownOption[indexPath.row])
    }

}
