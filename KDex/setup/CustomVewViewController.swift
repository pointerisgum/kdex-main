//
//  CustomVewViewController.swift
//  KDex
//
//  Created by park heewon on 2018. 5. 2..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation
import WebKit

class CustomVewViewController: UIViewController, WKUIDelegate, WKNavigationDelegate, WKScriptMessageHandler{
    
    @IBOutlet var totalView: UIView!
    
    var url = ""
    var mainTitle = ""
    var webView : WKWebView!
    var activityIndicator : UIActivityIndicatorView = UIActivityIndicatorView()
    
    override func loadView() {
        super.loadView()
        
        totalView.translatesAutoresizingMaskIntoConstraints = false
        totalView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        totalView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        totalView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        totalView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
        totalView.backgroundColor = UIColor.red
        
        
        webView = WKWebView(frame: self.view.frame)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        
        print("notice :\(self.totalView.frame)")
        print("notice :\(self.webView.frame)")
        
        self.totalView = self.webView!
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initTitle()
        
        self.view.backgroundColor = UIColor.black
        
        self.view.addSubview(totalView)
        
        let urlvalue = URL(string: self.url)
        
        let request = URLRequest(url: urlvalue!)
        
        webView.load(request)
        
    }
    
    func initTitle(){
        
        let nTitle = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 40))
        
        nTitle.textAlignment = .center
        nTitle.font = UIFont.boldSystemFont(ofSize: 15)
        nTitle.textColor = UIColor.white
        nTitle.text = mainTitle
        nTitle.textAlignment = .center
        
        self.navigationItem.titleView = nTitle
        
        self.navigationItem.titleView?.frame.size = CGSize(width: 300, height: 40)
        
        self.navigationController?.navigationBar.barTintColor = UIColor.DPNaviBarTintColor
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print(error)
    }
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        //script에서 ios zhem
    }
    
    @available(iOS 8.0, *)
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping () -> Void) {
        
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .default, handler: { (action) in
            completionHandler()
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @available(iOS 8.0, *)
    func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping (Bool) -> Void) {
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .default, handler: { (action) in
            completionHandler(true)
        }))
        alertController.addAction(UIAlertAction(title: "common_no_btn".localized, style: .default, handler: { (action) in
            completionHandler(false)
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @available(iOS 8.0, *)
    func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping (String?) -> Void) {
        let alertController = UIAlertController(title: "", message: prompt, preferredStyle: .alert)
        alertController.addTextField { (textField) in
            textField.text = defaultText
        }
        
        alertController.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .default, handler: { (action) in
            if let text = alertController.textFields?.first?.text {
                completionHandler(text)
            } else {
                completionHandler(defaultText)
            }
        }))
        
        alertController.addAction(UIAlertAction(title: "common_no_btn".localized, style: .default, handler: { (action) in
            completionHandler(nil)
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    @available(iOS 8.0, *)
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
        activityIndicator.frame = CGRect(x: view.frame.midX-50, y: view.frame.midY-50, width: 100, height: 100)
        activityIndicator.style = .whiteLarge
        activityIndicator.color = UIColor.DPCompanyColor
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
    }
    
    @available(iOS 8.0, *)
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.activityIndicator.removeFromSuperview()
    }
    
    public func webViewWebContentProcessDidTerminate(_ webView: WKWebView) {
        // 중복적으로 리로드가 일어나지 않도록 처리 필요.
        self.webView.reload()
    }

}
