//
//  CustomController.swift
//  KDex
//
//  Created by park heewon on 2018. 5. 2..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation

class CustomController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let titleValue : [[String : String]] = [["main" : "customer_center_man_to_man".localized, "sub" : "customer_center_man_to_man_detail".localized, "url" : "\(AppDelegate.url)/mobile/oneonone?uid=\(AppDelegate.uid)&sessionid=\(AppDelegate.sessionid)"
        ], ["main" : "customer_center_cert_txt".localized, "sub" : "customer_center_cert_detail".localized, "url" : "\(AppDelegate.url)/mobile/authCenter"], ["main" : "customer_center_cert_alliance".localized, "sub" : "customer_center_cert_alliance_detail".localized, "url" : "\(AppDelegate.url)/mobile/alliance?uid=\(AppDelegate.uid)&sessionid=\(AppDelegate.sessionid)"], ["main" : "customer_center_cert_log_out".localized, "sub" : "customer_center_cert_log_out_detail".localized, "url" : "\(AppDelegate.url)/mobile/out"]]
    
    @IBOutlet var customTb: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customTb.delegate = self
        customTb.dataSource = self
        
        customTb.layoutMargins = UIEdgeInsets.zero
        customTb.separatorInset = UIEdgeInsets.zero
        
        initTitle()
    }
    @IBAction func backBtnAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func initTitle(){
        
        let nTitle = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 40))
        
        nTitle.textAlignment = .center
        nTitle.font = UIFont.boldSystemFont(ofSize: 15)
        nTitle.textColor = UIColor.white
        nTitle.text = "customer_center_title".localized
        nTitle.textAlignment = .center
        
        self.navigationItem.titleView = nTitle
        
        self.navigationItem.titleView?.frame.size = CGSize(width: 300, height: 40)
        
        self.navigationController?.navigationBar.topItem?.title = ""
        
        self.navigationController?.navigationBar.barTintColor = UIColor.DPNaviBarTintColor
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleValue.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "customCell") as? CustomCell
        
        guard let value = self.titleValue[indexPath.row] as? [String: String] else{ return cell!}
        
        cell?.mainLabel.text = value["main"]
        cell?.mainLabel.textColor = UIColor.DPmainTextColor
        cell?.subLabel.text = value["sub"]
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        guard let value = self.titleValue[indexPath.row] as? [String: String] else{ return }
        
        guard let controller = storyboard.instantiateViewController(withIdentifier: "customWebView") as? CustomVewViewController else { return }
        
        controller.url = value["url"]!
        controller.mainTitle = value["main"]!
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}
