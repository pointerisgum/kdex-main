//
//  MembershipController.swift
//  KDex
//
//  Created by park heewon on 2018. 5. 2..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation

class InfomationController: PagerController, PagerDataSource {
    
    var revealDelegate : RevealViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.black
        
        self.dataSource = self
        
        registerStoryboardControllers()
        
        customizeTab()
        
        initTitle()
        
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func initTitle(){
        
        let nTitle = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 40))
        
        nTitle.textAlignment = .center
        nTitle.font = UIFont.boldSystemFont(ofSize: 15)
        nTitle.textColor = UIColor.white
        nTitle.text = "action_bar_user_guide".localized
        nTitle.textAlignment = .center
        
        self.navigationItem.titleView = nTitle
        
        self.navigationItem.titleView?.frame.size = CGSize(width: 300, height: 40)
        
        self.navigationController?.navigationBar.topItem?.title = ""
        
        self.navigationController?.navigationBar.barTintColor = UIColor.DPNaviBarTintColor
        
    }
    
    func registerStoryboardControllers() {
        
        let storyboard = UIStoryboard(name: "Invest", bundle: nil)
        
        let controller1 = storyboard.instantiateViewController(withIdentifier: "infoWebView") as? InfoWebviewController
        controller1?.url = "\(AppDelegate.url)mobile/infomation"
        
        let controller2 = storyboard.instantiateViewController(withIdentifier: "infoWebView") as? InfoWebviewController
        controller2?.url = "\(AppDelegate.url)mobile/dealinfo"
        
        let controller3 = storyboard.instantiateViewController(withIdentifier: "infoWebView") as? InfoWebviewController
        controller3?.url = "\(AppDelegate.url)mobile/walletinfo"
        
        self.setupPager(tabNames: ["guide_fragment1_title".localized, "guide_fragment2_title".localized, "guide_fragment3_title".localized], tabControllers: [controller1!, controller2!, controller3!])
        
    }
    
    func customizeTab() {
        indicatorColor = UIColor.white
        tabsViewBackgroundColor = UIColor.DPtableViewBgColor
        contentViewBackgroundColor = UIColor.DPtableViewBgColor
        
        startFromSecondTab = false
        centerCurrentTab = true
        tabLocation = PagerTabLocation.top
        tabHeight = 49
        tabOffset = 36
        tabWidth = self.view.frame.size.width / 3
        fixFormerTabsPositions = false
        fixLaterTabsPosition = false
        animation = PagerAnimation.during
        selectedTabTextColor = UIColor.DPmainTextColor
        tabsTextFont = UIFont.boldSystemFont(ofSize: 15)
        tabsTextColor = UIColor.DPsubTextColor
    }
    
    func changeTab() {
        self.selectTabAtIndex(1)
    }
    
    
}
