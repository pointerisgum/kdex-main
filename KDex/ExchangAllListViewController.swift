//
//  ExchangAllListViewController.swift
//  crevolc
//
//  Created by crovolc on 2018. 1. 11..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//
// 매칭서비스 뷰 컨트롤러

import UIKit
import Foundation
import Alamofire
import WebKit

class ExchangAllListViewController : UIViewController, UITableViewDelegate, UITableViewDataSource, WKUIDelegate, WKNavigationDelegate, WKScriptMessageHandler, RealTimeDelegate {
   
    var webView : WKWebView!
    
    var exchangListMager = ExchangCoinListManager()
    
    var activityIndicator : UIActivityIndicatorView = UIActivityIndicatorView()
    
    var coinSimbol : String? //코인 아이디
    
    var coinName : String? //코인 네임
    
    var market : String?
    
    var selectedFavorites = false
    
    var dataJsondic : [String : Any]? // 데이타시세 값
    
    var jsonArray : [[String : Any]]? //매칭 데이터 json array 값

    var jsondic : NSMutableDictionary? //매칭 데이터 json 값 파싱
    
    @IBOutlet var sellBtn: UIButton!
    
    @IBOutlet var sendBtn: UIButton!
    
    @IBOutlet var listTable: UITableView! //매칭 테이블
    
    @IBOutlet var contentView: UIView! //차트, 현재가, 최고가, 변동률, 최저가 뷰
    
    @IBOutlet var openPrice: UILabel! // 현재가
    
    @IBOutlet var opnePriceKRW: UILabel!
    
    @IBOutlet var updnRate: UILabel! //변동율
    
    @IBOutlet var updnPrice: UILabel! // 변동가
    
    @IBOutlet var totalVol: UILabel! //거래량
    
    @IBOutlet var volSimbole: UILabel!
    
    @IBOutlet var highestPrice: UILabel! //최고가
    
    @IBOutlet var lowestPrice: UILabel! //최저가
    
    //@IBOutlet var btnSetView: UIView! //매수, 매도 버튼 셋 뷰
    
    @IBOutlet var cadleBar: UIView!  //차트 버튼 선택 바(cadleBar)
    
    @IBOutlet var depthBar: UIView!  //차트 버튼 선택 바(DepthBar)
    
    
  
    // ----- 테이블 헤더 뷰 -----
   var headerView : UIView = {
        let uv = UIView()
        return uv
    }()
    
    let tb_dateLabel : UILabel = {
        let lb = UILabel()
        lb.text = "trade_graph_match_time".localized
        lb.textColor = UIColor.DPmainTextColor
        lb.textAlignment = .center
        lb.baselineAdjustment = .alignCenters
        lb.font = UIFont.systemFont(ofSize: 14)
        return lb
    }()
    
    let tb_priceLabel : UILabel = {
        let lb = UILabel()
        lb.text = "trade_graph_match_price".localized
        lb.textColor = UIColor.DPmainTextColor
        lb.textAlignment = .center
        lb.baselineAdjustment = .alignCenters
        lb.font = UIFont.systemFont(ofSize: 14)
        return lb
    }()
    
    let tb_countLabel : UILabel = {
        let lb = UILabel()
        lb.text = "trade_graph_qty".localized
        lb.textColor = UIColor.DPmainTextColor
        lb.textAlignment = .center
        lb.baselineAdjustment = .alignCenters
        lb.font = UIFont.systemFont(ofSize: 14)
        
        return lb
    }()
    
    // ----- 테이블 헤더뷰 작업 완료 ------
    
    let favoriBtnSelectImg = UIImage(named: "btn_star_on")
    
    let favoriBtnNoSelectedImg = UIImage(named: "btn_star_off")
    
    let favoriteBtn : UIButton = {
        let btn = UIButton(type: .system)
        //btn.frame = CGRect(x: 0, y: 5, width: 30, height: 30)
        btn.frame = CGRect(x: 85, y: 5, width: 20, height: 20)
        return btn
    }()
    
    @IBAction func depthChatAction(_ sender: Any) {
        
        self.depthBar.backgroundColor = UIColor.DPLineBgColor
        self.cadleBar.backgroundColor = UIColor.DPStatusBgColor
        
        webViewSetup(chatType: false)
    }
    
    @IBAction func candleChatAction(_ sender: Any) {
        
        self.cadleBar.backgroundColor = UIColor.DPLineBgColor
        self.depthBar.backgroundColor = UIColor.DPStatusBgColor
        
        webViewSetup(chatType: true)
    }
    
    override func loadView() {
        super.loadView()
        
        webView = WKWebView()
        webView.backgroundColor = UIColor.DPViewBackgroundColor
        webView.translatesAutoresizingMaskIntoConstraints = false
        
        webView.uiDelegate = self
        webView.navigationDelegate = self
        
        self.view.addSubview( self.webView!)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        selectFavority(simbol: coinSimbol!)
        
        self.view.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        
//        let height = (self.navigationController?.navigationBar.frame.height)!
//        let tabbarHeight = (self.tabBarController?.tabBar.frame.height)!
        
//        self.view.bounds.origin.y = -height
        
//        self.listTable.frame.origin.y = self.listTable.frame.origin.y - (height)
//        self.sellBtn.frame.origin.y = self.sellBtn.frame.origin.y - (height)
//        self.sendBtn.frame.origin.y = self.sendBtn.frame.origin.y - (height)
        
        listTable.delegate = self
        listTable.dataSource = self
        listTable.backgroundColor = UIColor.DPtableViewBgColor
        listTable.layoutMargins = UIEdgeInsets.zero
        listTable.separatorInset = UIEdgeInsets.zero

        if #available(iOS 11.0, *){
            let window = UIApplication.shared.keyWindow
            let bottompadding = window?.safeAreaInsets.bottom ?? 0.0
            
            self.listTable.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -(54 + bottompadding)).isActive = true
        }
        
        initNextbtn()
        setupheaderView()
        quoteData()
        chatMoving()
        TcpSocket.sharedInstance.receiverThread?.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        chatMoving()
        TcpSocket.sharedInstance.receiverThread?.delegate = self
        
        self.depthBar.backgroundColor = UIColor.DPLineBgColor
        self.cadleBar.backgroundColor = UIColor.DPStatusBgColor
        webViewSetup(chatType: false)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
       TcpSocket.sharedInstance.receiverThread?.delegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        exchangListMager.favList()
    }
    
    func webViewSetup(chatType: Bool){
        
        var tempSimbole = ""
        
        if (coinSimbol?.contains("_"))! {
            tempSimbole = coinSimbol ?? ""
        }else {
            tempSimbole = "\(coinSimbol ?? "")_KRW"
        }
    
        var myUrl = ""
        
        if chatType {
            myUrl = "\(AppDelegate.url)mobile/chartDepth"
            
        }else {
           myUrl = "\(AppDelegate.url)mobile/chart?simbol=\(tempSimbole)"
        }
        
        print("myurl : \(myUrl)")
        
        let url = URL(string: myUrl)
        
        let request = URLRequest(url: url!)
        
        webView.load(request)
        
        webView.topAnchor.constraint(equalTo: self.cadleBar.bottomAnchor).isActive = true
        webView.bottomAnchor.constraint(equalTo: self.listTable.topAnchor).isActive = true
        webView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        webView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        
    }
    
    //매칭 테이블(listTable)헤더 설정
    func setupheaderView(){
        
        headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 40))
        headerView.backgroundColor = UIColor.DPtableHeaderBgColor
        
        let headerStackView = UIStackView(arrangedSubviews: [self.tb_dateLabel, self.tb_priceLabel, self.tb_countLabel])
        headerStackView.frame = CGRect(x: 0, y: 0, width: self.headerView.frame.width, height: self.headerView.frame.height)
        headerStackView.distribution = .fillEqually
        headerStackView.backgroundColor = UIColor.white
        self.headerView.addSubview(headerStackView)
        
    }
    
    //네비게이션바 알림및 톡 버튼 설정
    func initNextbtn(){
        let vi = UIView()
        
        let nTitle = UILabel(frame: CGRect(x: 0, y: 0, width: 400, height: 37))
        
        nTitle.textAlignment = .center
        nTitle.font = UIFont.boldSystemFont(ofSize: 15)
        nTitle.textColor = UIColor.DPTitleTextColor
        nTitle.text = "\(coinName!)(\(coinSimbol!.replacingOccurrences(of: "_", with: "/")))"
        nTitle.textAlignment = .left
        
        vi.addSubview(nTitle)
        
        self.navigationItem.titleView = vi
        
        self.navigationItem.titleView?.frame.size = CGSize(width: 400, height: 37)
        
        self.navigationController?.addShadowToBar(vi: self)
        
        self.navigationController?.navigationBar.barTintColor =  UIColor.DPNaviBarTintColor
        
        
        let totalVI = UIView()
        //totalVI.backgroundColor = UIColor.red
        totalVI.frame = CGRect(x: 0, y: 0, width: 120, height: 37)
        totalVI.tintColor = UIColor.white
        
//        let icon0 = UIImage(named: "favorites_no_selected")
        let icon1 = UIImage(named: "btn_graph")
        let icon2 = UIImage(named: "icn_appbar_talk")
        
        let btnLabel1 = UILabel()
        btnLabel1.text = "차트"
        btnLabel1.font = UIFont.systemFont(ofSize: 10)
        btnLabel1.textColor = UIColor.white
        btnLabel1.textAlignment = .center
        btnLabel1.frame = CGRect(x: 45, y: 24, width: 20, height: 15)
        
        let rightbtn1 = UIButton(type: .system)
        rightbtn1.frame = CGRect(x: 45, y: 2, width: 20, height: 20)
        rightbtn1.setImage(icon1, for: .normal)
        rightbtn1.addTarget(self, action: #selector(goChart), for: .touchUpInside)
        
        let favoriteLabel = UILabel()
        favoriteLabel.text = "즐겨찾기"
        favoriteLabel.font = UIFont.systemFont(ofSize: 10)
        favoriteLabel.textColor = UIColor.white
        favoriteLabel.textAlignment = .center
        favoriteLabel.frame = CGRect(x: 75, y: 24, width: 35, height: 15)
        
        let rightbtn2 = UIButton(type: .system)
        rightbtn2.frame = CGRect(x: 85, y: 5, width: 30, height: 30)
        rightbtn2.setImage(icon2, for: .normal)
        rightbtn2.addTarget(self, action: #selector(alertAction), for: .touchUpInside)
        
        if selectedFavorites {
            favoriteBtn.setImage(self.favoriBtnSelectImg, for: .normal)
        }else {
            favoriteBtn.setImage(self.favoriBtnNoSelectedImg, for: .normal)
        }
        
        favoriteBtn.addTarget(self, action: #selector(setupfavorit), for: .touchUpInside)
        
        totalVI.addSubview(favoriteBtn)
        totalVI.addSubview(rightbtn1)
        totalVI.addSubview(btnLabel1)
        totalVI.addSubview(favoriteLabel)
//        totalVI.addSubview(rightbtn2)
        
        let item = UIBarButtonItem(customView: totalVI)
        
        self.navigationItem.rightBarButtonItem = item
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationController?.navigationBar.topItem?.title = ""

//        self.navigationController?.navigationBar.topItem?.title = "\(coinName!)(\(coinSimbol!.replacingOccurrences(of: "_", with: "/")))"
    }
    
    //매칭 테이블 행 갯수
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.jsonArray?.count ?? 0
    }
    
    //매칭 테이블 셀 설정
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "exchangAllListCell") as? ExchangAllListCell
        
        cell?.layoutMargins = UIEdgeInsets.zero
        
        guard let jsondic = self.jsonArray?[indexPath.row] else {
            return cell!
        }
        
        let cnt = self.jsonArray?.count
        let indexpathRow = cnt! - (indexPath.row + 1)
        
        let dateStr = jsondic["mtchTime"] as? String ?? ""
        let mtcQty = jsondic["mtchQty"] as? String ?? ""
        let mtcprc = jsondic["mtchPrc"] as? String ?? ""
        let updnSign = jsondic["updnSign"] as? String ?? ""
        
        var upndSing = false
        
        if indexpathRow == 0 {
            let doubleMtcprc = stringtoDouble(str: mtcprc) ?? 0
            let doubleOpenPrc = stringtoDouble(str: self.openPrice.text ?? "0" ) ?? 0
            
            if doubleMtcprc > doubleOpenPrc {
                upndSing = true
            }else {
                upndSing = false
            }
            
        }else if indexpathRow > 0 && indexPath.row + 1 < cnt!{
            let jsonPre = self.jsonArray?[indexPath.row + 1]
            
            let mtcpreprc = jsonPre?["mtchPrc"] as? String ?? ""
            
            let doubleMtcprc = stringtoDouble(str: mtcprc) ?? 0
            let doublePrePrc = stringtoDouble(str: mtcpreprc) ?? 0
            
            print("matchlist : mtc : \(doubleMtcprc), pre : \(doublePrePrc), indexpath : \(indexPath.row)")
            
            if doubleMtcprc > doublePrePrc {
                upndSing = true
            }else if doubleMtcprc == doublePrePrc {
                upndSing = jsonPre?["upndSing"] as? Bool ?? false
            }else {
                upndSing = false
            }
            
        }
        
        self.jsonArray![indexPath.row].updateValue(upndSing, forKey: "upndSing")
        
        if updnSign == "-1" {
            cell?.priceValueLabel.textColor = UIColor.DPMinTextColor
            
        }else if updnSign == "1" {
            cell?.priceValueLabel.textColor = UIColor.DPPlusTextColor

        }else {
            cell?.priceValueLabel.textColor = UIColor.DPmainTextColor
            
        }
        
        if upndSing {
            
            cell?.countValueLabel.textColor = UIColor.DPPlusTextColor
        }else {
            cell?.countValueLabel.textColor = UIColor.DPMinTextColor
        }
        
        print("maching list count : \(self.jsonArray?.count)")
        
        cell?.AllListDateLabel.adjustsFontSizeToFitWidth = true
        cell?.AllListDateLabel.text = self.makeTime(date: dateStr)
        cell?.countValueLabel.text = mtcQty.insertComma
        cell?.priceValueLabel.text = mtcprc.insertComma
        cell?.selectionStyle = .none
        
        return cell!
    }
    
    //매칭 테이블 헤더 뷰 높이 설정
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }

    //매칭 테이블 헤더 뷰 설정
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return self.headerView
    }

    //차트 cadle 메소드
    @IBAction func chatCadleBtnAction(_ sender: UIButton) {
        
    }
    
    //차트 depth 메소드
    @IBAction func chatDepthBtnAction(_ sender: Any) {
        
    }
    
    //매도 버튼 메소드(해당 페이지로 이동)
    @IBAction func sellBtnAction(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let detail = storyboard.instantiateViewController(withIdentifier: "exchangInOut") as? ExchangInOutDetail {
            detail.currentTab = 2
            detail.coinSimbol = coinSimbol ?? ""
            detail.coinName = coinName ?? ""
            self.navigationController?.pushViewController(detail, animated: true)
        }
    }
    
    //매수 버튼 메소드(해당 페이지로 이동)
    @IBAction func buyBtnAction(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let detail = storyboard.instantiateViewController(withIdentifier: "exchangInOut") as? ExchangInOutDetail {
            detail.currentTab = 1
            detail.coinSimbol = coinSimbol ?? ""
            detail.coinName = coinName ?? ""
            self.navigationController?.pushViewController(detail, animated: true)
        }
    }

    //매칭 서비스 데이터 수신
    func chatMoving(){
      
        let plist = UserDefaults.standard
        let uid = plist.string(forKey: "uid")
        let sessionid = plist.string(forKey: "sessionId")
        
        //매칭리스트 값 호출
        //auth (로그인 사용자)
        //app (일반사용자)
        var urlComponents = URLComponents(string:"\(AppDelegate.url)/app/tickList?simbol=\(self.coinSimbol!)&uid=\(uid!)&sessionId=\(sessionid!)")!
        print("url : \(urlComponents)")
        
        let amo = Alamofire.request(urlComponents)
        amo.responseJSON(completionHandler:
            { response in
                
                //json 파싱
                guard let bool = response.result.value as? [[String : Any]] else {
                    return
                }
                
                self.jsonArray = response.result.value as! [[String : Any]]
                
                let arr = self.jsonArray?.sorted(by: {
                    
                    guard let num1 = $0["mtchTime"] as? String else {
                        return true
                    }
                    
                    guard let num2 = $1["mtchTime"] as? String else {
                        return true
                    }
                    
                    return num1 > num2
                    
                })
                
                self.jsonArray = arr
                
                let cnt = self.jsonArray?.count ?? 0
                
                if cnt != 0{
                    
                    for row in 1...cnt {
                        
                        guard let jsondic = self.jsonArray?[cnt - row] else {
                            return
                        }
                        
                        let indexpathRow = cnt - (row - 1)
                        
                        let mtcprc = jsondic["mtchPrc"] as? String ?? ""
                        
                        var upndSing = false
                        
                        if indexpathRow == cnt {
                            let doubleMtcprc = self.stringtoDouble(str: mtcprc) ?? 0
                            let doubleOpenPrc = self.stringtoDouble(str: self.openPrice.text ?? "0" ) ?? 0
                            
                            if doubleMtcprc > doubleOpenPrc {
                                upndSing = true
                            }else {
                                upndSing = false
                            }
                            
                        }else {
                            
                            guard let jsonNext = self.jsonArray?[indexpathRow] else {
                                return
                            }
                            
                            let mtcpreprc = jsonNext["mtchPrc"] as? String ?? ""
                            
                            let doubleMtcprc = self.stringtoDouble(str: mtcprc) ?? 0
                            let doublePrePrc = self.stringtoDouble(str: mtcpreprc) ?? 0
                            
                            if doubleMtcprc > doublePrePrc {
                                upndSing = true
                            }else if doubleMtcprc == doublePrePrc {
                                upndSing = jsonNext["upndSing"] as? Bool ?? false
                            }else {
                                upndSing = false
                            }
                            
                        }
                        
                        self.jsonArray![cnt - row].updateValue(upndSing, forKey: "upndSing")
                        
                    }
                    
                }
 
                //테이블 리로드
                self.listTable.reloadData()
        })
        
    }
    
    //날짜 형식 포멧
    func makeTime(date : String) -> String {
        
        let df : DateFormatter = DateFormatter()
        df.dateFormat = "yyyyMMddHHmmss"
        
        guard let formData = df.date(from: date) as? NSDate else {
            return "0.0.0 00:00:00"
        }
        
        let date_time : NSDate = formData
        
        df.dateFormat = "yy.MM.dd HH:mm:ss"
        
        let newDate : String = df.string(from: date_time as Date) as String
        
        return newDate
    }
    
    //시세 데이타
    
    
    fileprivate func setupQuDate() {
        print("setupQudate start image")
        
        var coinNameSimbole = ""
        var doubleLastKrwValue = 0.0
        let openprice = self.dataJsondic?["lastPrice"] as? String
        let highestprice = self.dataJsondic?["highPrice"] as? String
        let lowestprice = self.dataJsondic?["lowPrice"] as? String
        let updnrate = self.dataJsondic?["updnRate"] as? String
        let updnSign = self.dataJsondic?["updnSign"] as? String
        let totalVol = self.dataJsondic?["totalVol"] as? String
        let updnPrice = self.dataJsondic?["updnPrice"] as? String
        
        
        if self.market ?? "" == "ETH"{
            doubleLastKrwValue = AppDelegate.ETH
        }else if self.market ?? "" == "BTC" {
            doubleLastKrwValue = AppDelegate.BTC
        }else {
            doubleLastKrwValue = 1.0
        }
        
        let doubleHightestPrice = self.stringtoDouble(str: highestprice ?? "0") ?? 0
        
        DispatchQueue.main.async {
            if (highestprice?.contains("."))! {
                self.highestPrice.text = String(format: "%.8f", doubleHightestPrice)
            }else {
                self.highestPrice.text = highestprice?.insertComma
            }
            
            let doubleLowPrice = self.stringtoDouble(str: lowestprice ?? "0") ?? 0
            
            if (lowestprice?.contains("."))! {
                self.lowestPrice.text = String(format: "%.8f", doubleLowPrice)
            }else {
                //lowestprice?.insertComma
                self.lowestPrice.text = lowestprice?.insertComma
            }
            
            let doubleOpenPrice = self.stringtoDouble(str: openprice ?? "0") ?? 0
            
            if (openprice?.contains("."))! {
                self.openPrice.text = String(format: "%.8f", doubleOpenPrice)
            }else {
                //openprice?.insertComma
                self.openPrice.text = openprice?.insertComma
            }
            
            if let indexvalue = self.coinSimbol?.index(of: "_") {
                
                coinNameSimbole = String((self.coinSimbol?.prefix(upTo: indexvalue))!)
            }else {
                coinNameSimbole = self.coinSimbol ?? ""
            }
            
            var krwValue = doubleLastKrwValue * doubleOpenPrice
            
            self.opnePriceKRW.text = "\(String(format: "%.0f", krwValue.roundToPlaces(places: 0)))".insertComma + "KRW"
            
            if self.market ?? "" == "ETH" || self.market ?? "" == "BTC" {
                self.updnPrice.text = String(format: "%0.8f", self.stringtoDouble(str: updnPrice!) ?? 0.0)
            }else {
                self.updnPrice.text = String(format: "%0.2f", self.stringtoDouble(str: updnPrice!) ?? 0.0).insertComma
            }
            
            self.totalVol.text = totalVol
            self.volSimbole.text = coinNameSimbole
            self.updnRate.text = self.stringChage(value1: nil, value2: "%", origin: updnrate!)
            
            if updnSign == "-1" {
                self.openPrice.text = (self.openPrice.text ?? "") + " ▼"
                self.openPrice.textColor = UIColor.DPMinTextColor
                self.updnRate.textColor = UIColor.DPMinTextColor
                self.updnPrice.textColor = UIColor.DPMinTextColor
            }else if updnSign == "1" {
                self.openPrice.text = (self.openPrice.text ?? "") + " ▲"
                self.openPrice.textColor = UIColor.DPPlusTextColor
                self.updnRate.textColor = UIColor.DPPlusTextColor
                self.updnPrice.textColor = UIColor.DPPlusTextColor
            }else {
                self.openPrice.textColor = UIColor.DPmainTextColor
                self.updnRate.textColor = UIColor.DPmainTextColor
                self.updnPrice.textColor = UIColor.DPmainTextColor
            }
            
        }
        
    }
    
    func quoteData(){
        
        let coinSim = self.coinSimbol
        let plist = UserDefaults.standard
        let uid = plist.string(forKey: "uid")
        let sessionid = plist.string(forKey: "sessionId")
        
        //매칭리스트 값 호출
        var urlComponents = URLComponents(string:"\(AppDelegate.url)app/coinDetail?simbol=\(coinSim!)&uid=\(uid!)&sessionId=\(sessionid!)")!
        
        print("urlComponents : \(urlComponents)")
        
        let amo = Alamofire.request(urlComponents)
        amo.responseJSON(completionHandler:
            { response in

                //json 파싱
                guard let value = response.result.value as? [String: Any] else { return }
                
                self.dataJsondic = value
                
                self.setupQuDate()
                
        })
        
    }

    func stringChage(value1: String?, value2: String, origin: String) -> String {
        
        var str : String = origin
        
        if value1 == nil {
            str = "\(str) \(value2)"
            
        }else {
            str = "\(value1!) \(str) \(value2)"
        }
        
        return str
    }
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        //script에서 ios zhem
    }
    
    @available(iOS 8.0, *)
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping () -> Void) {
        
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .default, handler: { (action) in
            completionHandler()
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @available(iOS 8.0, *)
    func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping (Bool) -> Void) {
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .default, handler: { (action) in
            completionHandler(true)
        }))
        alertController.addAction(UIAlertAction(title: "common_no_btn".localized, style: .default, handler: { (action) in
            completionHandler(false)
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @available(iOS 8.0, *)
    func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping (String?) -> Void) {
        let alertController = UIAlertController(title: "", message: prompt, preferredStyle: .alert)
        alertController.addTextField { (textField) in
            textField.text = defaultText
        }
        alertController.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .default, handler: { (action) in
            if let text = alertController.textFields?.first?.text {
                completionHandler(text)
            } else {
                completionHandler(defaultText)
            }
        }))
        
        alertController.addAction(UIAlertAction(title: "common_no_btn".localized, style: .default, handler: { (action) in
            completionHandler(nil)
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @available(iOS 8.0, *)
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
        activityIndicator.frame = CGRect(x: self.webView.frame.midX-50, y: self.webView.frame.midY-50, width: 100, height: 100)
        activityIndicator.style = .whiteLarge
        activityIndicator.color = UIColor(rgb: 0x00A6ED)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        
    }
    
    @available(iOS 8.0, *)
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.activityIndicator.removeFromSuperview()
    }
    
    public func webViewWebContentProcessDidTerminate(_ webView: WKWebView) {
        // 중복적으로 리로드가 일어나지 않도록 처리 필요.
        self.webView.reload()
    }
    
    @objc func goChart(){
//        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "AlectVC") as? AlertViewController else {
//            return
//        }
//        vc.coinSimbol = self.coinSimbol!
//        self.navigationController?.pushViewController(vc, animated: true)
    
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChartView") as? ExchangChartView else {
            return
        }
        vc.coinSimbol = self.coinSimbol ?? ""
        vc.coinName = self.coinName ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @objc func alertAction(){
        let alertView = self.storyboard?.instantiateViewController(withIdentifier: "alertDetail") as? AlertDetailViewController
        self.navigationController?.pushViewController(alertView!, animated: true)
    }
    
    @objc func setupfavorit(){
        
        print("current page simbol : \(coinSimbol!)")
        
        let plist = UserDefaults.standard
        let uid = plist.string(forKey: "uid")
        let sessionid = plist.string(forKey: "sessionId")
        
        if selectedFavorites == false {
            print("favorites add")
            favorityAction(type: "N", simbol: self.coinSimbol!, uid: uid!, sessionid: sessionid!)
            favoriteBtn.setImage(self.favoriBtnSelectImg, for: .normal)
            selectedFavorites = true
            
        }else {
            print("favorites delete")
            favorityAction(type: "D", simbol: self.coinSimbol!, uid: uid!, sessionid: sessionid!)
            favoriteBtn.setImage(self.favoriBtnNoSelectedImg, for: .normal)
            selectedFavorites = false
            
        }
        
    }
    
    func favorityAction(type : String, simbol : String, uid : String, sessionid : String){
        
        print("uid : \(uid), sessionid: \(sessionid), simbole: \(simbol)")
        
        let url = "\(AppDelegate.url)ajax/favCoinProc?uid=\(uid)&sessionId=\(sessionid)&simbol=\(simbol)&type=\(type)"
        print(url)
        let amo = Alamofire.request(url)
        amo.responseString { response in
            if response.result.isSuccess == true {
                let result = response.result.value
                if result != nil {
                    print("\(result) 즐겨찾기 전송 완료")
                }else{
                    print("전송 실패")
                }
            }
        }
    }
    
    func selectFavority(simbol : String){
        print("selectFavority")
        for row in AppDelegate.favoritesList {
            print(row)
            if row == simbol {
                print("same \(simbol)")
               self.selectedFavorites = true
            }
        }
    }
    
    func exchangListRT(bt: NPriceTick) {
        
        print("RT : exchangAllLisView")
        
        let tempSymbole = self.coinSimbol ?? ""
        let symbol = bt.symbol.toString().components(separatedBy: [" "]).joined()
        let open_price = bt.open_price.toString()
        let high_price = bt.high_price.toString()
        let low_price = bt.low_price.toString()
        let delta_rate = bt.delta_rate.toString()
        let timestamp = bt.timestamp.toString()
//        let intopen_price = Int(open_price)
//        let inthigh_price = Int(high_price)
//        let intlow_price = Int(low_price)
        let floatdelta_rate = Float(delta_rate)
        
        let last_volume = bt.last_volume.toString()
        let last_price = bt.last_price.toString()
        
        let total_volume = bt.total_volume.toString()
        let updn_price = bt.delta_price.toString()
        let updn_Sign = bt.delta_sign.toString()
        
        let intLast_volume = Float(last_volume)
//        let intLast_price = Int(last_price)
        
        print("RT symbole : \(symbol)")
        let str = timestamp.subStr(num: 14, str: timestamp)
        
       print("RT data : \(str), \(last_price), \(high_price), \(low_price), \(delta_rate)")
        
//        var index = 0
//        var num = 0
        var dic = [String : Any]()
        
        if tempSymbole == symbol {
            print("RT start checked")
            
            print("RT start checked : \(last_volume)")
            
            let intlastVol = stringtoDouble(str: last_volume) ?? 0
            
            if intlastVol != 0 {
                
                dic.updateValue(str, forKey: "mtchTime" )
                dic.updateValue(String(last_volume.insertComma), forKey: "mtchQty")
                dic.updateValue(String(last_price.insertComma), forKey: "mtchPrc")
                
                let uprateChangeStr = self.stringChage(value1: nil, value2: "%", origin: String(floatdelta_rate!))
                
                self.jsonArray?.insert(dic, at: 0)
                print("RT jsonCount : \(self.jsonArray?.count)")
                
                self.dataJsondic?.updateValue(last_price.insertComma, forKey: "lastPrice")
                self.dataJsondic?.updateValue(high_price.insertComma, forKey: "highPrice")
                self.dataJsondic?.updateValue(low_price.insertComma, forKey: "lowPrice")
                self.dataJsondic?.updateValue(total_volume, forKey: "totalVol")
                self.dataJsondic?.updateValue(updn_price, forKey: "updnPrice")
                self.dataJsondic?.updateValue(uprateChangeStr, forKey: "updnRate")
                self.dataJsondic?.updateValue(updn_Sign, forKey: "updnSign")
                
                self.setupQuDate()
            
                DispatchQueue.main.async {
                    print("RT main async")
                    
//                    self.openPrice.text = last_price.insertComma
//                    self.highestPrice.text = high_price.insertComma
//                    self.lowestPrice.text = low_price.insertComma
//                    self.totalVol.text = total_volume
//                    self.updnPrice.text = updn_price
//
//                    self.updnRate.text = self.stringChage(value1: nil, value2: "%", origin: String(floatdelta_rate!))
                
                    
                    self.listTable.reloadData()
                }
                
            }
        }else {
            self.setupQuDate()
        }
        
    }
    
    func exchangDetailRT(bt: NPriceTick) {
        
    }
    
    func trandeListRT(bt: NPriceInfo) {
        
    }
    
    func openListRT(bt: NOpenInfo) {
        
    }
    
    func krwChangeRT(value: Double) {

    }
    
    public func stringtoDouble(str : String) -> Double?{
        let temp = str.components(separatedBy: [","]).joined()
        let double = Double(temp)
        return double ?? 0.0
    }
    
}
