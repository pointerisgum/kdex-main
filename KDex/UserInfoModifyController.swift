//
//  UserInfoModifyController.swift
//  DEXnPAY
//
//  Created by park heewon on 2018. 3. 28..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation
import Alamofire

class UserInfoModifyController: UIViewController {
    
    var isCheck = false
    
    let tfVi : UIView = {
        let vi = UIView()
        vi.backgroundColor = UIColor.clear
        return vi
    }()
   
    @IBOutlet var testfeilVi: UITextField!
    
    @IBOutlet var overlapBtn: UIButton!
    
    override func viewDidLoad() {
     
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.black
        
        let img = UIImage(named: "ico_inp4")
        
        let imgVi = UIImageView(image: img)
        
        imgVi.frame = CGRect(x: 8, y: 8, width: 15, height: 15)
        tfVi.frame = CGRect(x: 0, y: 0, width: 25, height: testfeilVi.frame.height)
        tfVi.addSubview(imgVi)
        
        testfeilVi.leftViewMode = .always
        testfeilVi.leftView = tfVi
        testfeilVi.text = AppDelegate.userName
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = "내정보"
        
    }
    
    @IBAction func overlapAction(_ sender: Any) {
        
        if self.testfeilVi.text ?? "" != "" {
            
            let str = self.testfeilVi.text ?? ""
            
            
            let urlStr = "\(AppDelegate.url)/auth/nickCheck?nick=\(str)"
            let encoded = urlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            
            guard let urlComponents = URLComponents(string: encoded) else {
                print("usl nil error")
                return
            }
            
            let amo = Alamofire.request(urlComponents)
            amo.responseString(completionHandler: { response in
                
                guard let check = response.result.value else {
                    self.showToast(message: "잠시후 다시 시도 부탁드립니다.", textcolor: UIColor.white, frame:CGRect(x: self.view.frame.size.width/2 - 100, y: self.view.frame.size.height-100, width: 200, height: 35), time: 0.2, animation: true)
                    return
                }
                
                if check == "N" {
                    self.isCheck = true
                    self.showToast(message: "사용 가능한 닉네임 입니다.", textcolor: UIColor.white, frame:CGRect(x: self.view.frame.size.width/2 - 100, y: self.view.frame.size.height-100, width: 200, height: 35), time: 0.2, animation: true)
                    
                }else {
                    self.isCheck = false
                    self.showToast(message: "중복된 닉네임 입니다.", textcolor: UIColor.white, frame:CGRect(x: self.view.frame.size.width/2 - 100, y: self.view.frame.size.height-100, width: 200, height: 35), time: 0.2, animation: true)
                    
                }
            })
            
        }else {
            self.showToast(message: "닉네임을 입력 해주세요.", textcolor: UIColor.white, frame:CGRect(x: self.view.frame.size.width/2 - 100, y: self.view.frame.size.height-100, width: 200, height: 35), time: 0.2, animation: true)
        }
        
    }
    @IBAction func nickNameSetupAction(_ sender: Any) {
        if self.testfeilVi.text == "" {
            self.showToast(message: "닉네임을 입력해 주세요.", textcolor: UIColor.white, frame:CGRect(x: self.view.frame.size.width/2 - 100, y: self.view.frame.size.height-100, width: 200, height: 35), time: 0.2, animation: true)
        }else if self.isCheck == false {
            self.showToast(message: "닉네임 중복을 확인 해 주세요.", textcolor: UIColor.white, frame:CGRect(x: self.view.frame.size.width/2 - 100, y: self.view.frame.size.height-100, width: 200, height: 35), time: 0.2, animation: true)
        }else {
            save()
        }
         
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func save(){
        
        let activity = UIActivityIndicatorView()
        activity.style = .whiteLarge
        activity.color = UIColor.DPActivityColor
        activity.hidesWhenStopped = true
        activity.startAnimating()
        
        let nickname = self.testfeilVi.text!
        
        let param = "uid=\(AppDelegate.uid)&userName=\(nickname)"
        
        print(param)
        
        let paramData = param.data(using: .utf8)
        
        let url = URL(string: "\(AppDelegate.url)/auth/modiProc")
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        request.httpBody = paramData
        
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        request.setValue(String(describing: paramData!.count), forHTTPHeaderField: "Content-Length")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if let err = error  {
                
                print("login error is failed")
                
                DispatchQueue.main.async {
                    let msg = "통신 장애로 수정에 실패하였습니다."
                    let alert = UIAlertController(title: "닉네임", message: msg, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "확인", style: .cancel, handler: nil))
                    self.present(alert, animated: false, completion: nil)
                    
                    activity.stopAnimating()
                }
                
                return
                
            }else{
                DispatchQueue.main.async {
                let msg = "정상적으로 수정되었습니다."
                let alert = UIAlertController(title: "닉네임", message: msg, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "확인", style: .cancel, handler: { (_) in
                        self.dismiss(animated: true, completion: nil)
                    }))
                self.present(alert, animated: false, completion: nil)
                
                activity.stopAnimating()
                
                }
              
            }
            
        }
        task.resume()
    }
    
}
