//
//  AlertViewController.swift
//  DEXnPAY
//
//  Created by park heewon on 2018. 3. 16..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//  알림내역

import UIKit
import Foundation
import Alamofire

class AlertDetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
   
    var alertArray : [[String : Any]]?
    
    var uid = ""
    
    var sessionid = ""
    
    var alertSearchOpenBool = false
    
    var alertTitleStr = "alarm_list_item1".localized
    
    let tableCellId = "alertDetailCell"
    
    var alertSordBool = false
    
    var sortType : alerType = .all
    
    var stheight : NSLayoutConstraint?
    
    var tableVi : UITableView = {
        let ti = UITableView()
        ti.translatesAutoresizingMaskIntoConstraints = false
        ti.backgroundColor = UIColor.white
        return ti
    }()
    
    let barVi : UIView = {
        let vi = UIView()
        vi.backgroundColor = UIColor(rgb: 0xcacacd)
        vi.translatesAutoresizingMaskIntoConstraints = false
        return vi
    }()
    
    let alertLb : UIButton = {
        let bt = UIButton(type: .system)
        bt.setTitle("alarm_list_item1_down".localized, for: .normal)
        bt.setTitleColor(UIColor.DPmainTextColor, for: .normal)
        bt.addTarget(self, action: #selector(alertBtnAction), for: .touchUpInside)
        return bt
    }()
    
    let alertLbAll : UIButton = {
        let bt = UIButton(type: .system)
        bt.setTitle("alarm_list_item1".localized, for: .normal)
        bt.setTitleColor(UIColor.DPMinTextColor, for: .normal)
        bt.addTarget(self, action: #selector(alertBtnAction), for: .touchUpInside)
        return bt
    }()
    
    let alertLbOne : UIButton = {
        let bt = UIButton(type: .system)
        bt.setTitle("alarm_list_item2".localized, for: .normal)
        bt.setTitleColor(UIColor.DPmainTextColor, for: .normal)
        bt.addTarget(self, action: #selector(alertBtnAction), for: .touchUpInside)
        return bt
    }()
    
    let alertLbTwo : UIButton = {
        let bt = UIButton(type: .system)
        bt.setTitle("alarm_list_item3".localized, for: .normal)
        bt.setTitleColor(UIColor.DPmainTextColor, for: .normal)
        bt.addTarget(self, action: #selector(alertBtnAction), for: .touchUpInside)
        return bt
    }()
    
    let alertLbThree : UIButton = {
        let bt = UIButton(type: .system)
        bt.setTitle("alarm_list_item4".localized, for: .normal)
        bt.setTitleColor(UIColor.DPmainTextColor, for: .normal)
        bt.addTarget(self, action: #selector(alertBtnAction), for: .touchUpInside)
        return bt
    }()
    
    let stackview : UIView = {
        let st = UIView()
        st.backgroundColor = UIColor.white
        st.translatesAutoresizingMaskIntoConstraints = false
        return st
    }()
    
//    let stackview : UIStackView = {
//        let st = UIStackView()
//        st.distribution = .fillEqually
//        st.axis = .vertical
//        st.backgroundColor = UIColor.red
//        st.translatesAutoresizingMaskIntoConstraints = false
//        return st
//    }()
    
    let actactivityIndicator = UIActivityIndicatorView()
    
    let tableFootVi : UIView = {
        let vi = UIView()
        vi.backgroundColor = UIColor.DPtableFooterBgColor
        return vi
    }()
 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor(red:0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        
        actactivityIndicator.frame = CGRect(x: self.view.frame.width/2 - 50, y: self.view.frame.height/2 - 100, width: 100, height: 100)
        actactivityIndicator.style = .whiteLarge
        actactivityIndicator.color = UIColor.DPActivityColor
        actactivityIndicator.hidesWhenStopped = true
        tableVi.addSubview(actactivityIndicator)
        
        self.initTitle()
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.tintColor = UIColor.DPNaviBartextTintColor
        self.navigationController?.navigationBar.barTintColor = UIColor.DPNaviBarTintColor
    
        
        tableVi.delegate = self
        tableVi.dataSource = self
        tableVi.separatorStyle = .none
        tableVi.layoutMargins = UIEdgeInsets.zero
        tableVi.separatorInset = UIEdgeInsets.zero
        
        view.addSubview(stackview)
        view.addSubview(tableVi)
        
        alertStackView()
        
        tableVi.register(AlertDetailCell.self, forCellReuseIdentifier: tableCellId)
        
        tableVi.backgroundColor = UIColor.DPtableViewBgColor
        
        //tableVi.register(AlertDetailCell.self, forCellReuseIdentifier: tableCellId)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let plist = UserDefaults.standard
        plist.set(false, forKey: "alertReadBool")
        plist.synchronize()
        
        alertList(uid: AppDelegate.uid, sessionid: AppDelegate.sessionid, type: .all)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
//        alertList(uid: AppDelegate.uid, sessionid: AppDelegate.sessionid, type: .all)
        
        buySellAlertAction()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        var num = 0
        
        guard let tempJson = self.alertArray else {
            return
        }
        
        for row in tempJson {
            
            let alertRead = row["readYn"] as? String ?? ""
            print("alert Read : \(alertRead)")
            
            if alertRead == "N" {
                num += 1
            }
        }
        
        UIApplication.shared.applicationIconBadgeNumber = num
        
        if UIApplication.shared.applicationIconBadgeNumber == 0 {
            AppDelegate.alertShow = false
        }
    }
    
    
    func initTitle(){
        
        let nTitle = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 40))
        
        nTitle.textAlignment = .center
        nTitle.font = UIFont.boldSystemFont(ofSize: 15)
        nTitle.textColor = UIColor.white
        nTitle.text = "notice_list_title".localized
        nTitle.textAlignment = .center
        
        self.navigationItem.titleView = nTitle
        
        self.navigationItem.titleView?.frame.size = CGSize(width: 300, height: 40)
        //print("size : \(self.navigationItem.titleView?.frame.size)")
        let color = UIColor(red:0.05, green: 0.05, blue: 0.05, alpha: 1.0)
        self.navigationController?.navigationBar.barTintColor = color
    
    }
    
    @objc func alertBtnAction(sender : UIButton) {
        
        if sender == alertLbAll {
            print("알림 전체 내역")
            self.sortType = .all
            [alertLb, alertLbOne, alertLbTwo, alertLbThree].forEach {
                $0.setTitleColor(UIColor.DPmainTextColor, for: .normal)
            }
            
            self.alertTitleStr = "alarm_list_item1".localized
            //            self.alertLb.setTitle("\(self.alertTitleStr) ▼", for: .normal)
            self.alertLbAll.setTitleColor(UIColor.DPMinTextColor, for: .normal)
            
            self.alertList(uid: AppDelegate.uid, sessionid: AppDelegate.sessionid, type: .all)
        }else if sender == alertLbOne {
            self.sortType = .login
            print("알림 로그인 내역")
            [alertLb, alertLbAll, alertLbTwo, alertLbThree].forEach {
                $0.setTitleColor(UIColor.DPmainTextColor, for: .normal)
            }
            self.alertTitleStr = "alarm_list_item2".localized
            //            self.alertLb.setTitle("로그인 알림", for: .normal)
            self.alertLbOne.setTitleColor(UIColor.DPMinTextColor, for: .normal)
            self.alertList(uid: AppDelegate.uid, sessionid: AppDelegate.sessionid, type: .login)
        }else if sender == alertLbTwo {
            print("알림 거래 내역")
            self.sortType = .trande
            [alertLb, alertLbAll, alertLbOne, alertLbThree].forEach {
                $0.setTitleColor(UIColor.DPmainTextColor, for: .normal)
            }
            self.alertTitleStr = "alarm_list_item3".localized
            //            self.alertLb.setTitle("거래 알림", for: .normal)
            self.alertLbTwo.setTitleColor(UIColor.DPMinTextColor, for: .normal)
            self.alertList(uid: AppDelegate.uid, sessionid: AppDelegate.sessionid, type: .trande)
        }else if sender == alertLbThree {
            print("알림 지갑 내역")
            self.sortType = .wallet
            [alertLb, alertLbAll, alertLbOne, alertLbTwo].forEach {
                $0.setTitleColor(UIColor.DPmainTextColor, for: .normal)
            }
            self.alertTitleStr = "alarm_list_item4".localized
            //            self.alertLb.setTitle("지갑 알림", for: .normal)
            self.alertLbThree.setTitleColor(UIColor.DPMinTextColor, for: .normal)
            self.alertList(uid: AppDelegate.uid, sessionid: AppDelegate.sessionid, type: .wallet)
        }
        
        if alertSordBool {
            print("click alert hidden")
            alertSordBool = false
            self.alertLb.setTitle("\(self.alertTitleStr) ▼", for: .normal)
            stheight?.constant -= 200
            UIView.animate(withDuration: 0.35) {
                self.view.layoutIfNeeded()
            }
            
        }else {
            print("click alert view")
            alertSordBool = true
            self.alertLb.setTitle("\(self.alertTitleStr) ▲", for: .normal)
            stheight?.constant += 200
            UIView.animate(withDuration: 0.35) {
                self.view.layoutIfNeeded()
                
            }
            
        }
        
    }
    
    fileprivate func alertStackView() {
        
        stackview.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        stackview.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        stackview.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        stheight = stackview.heightAnchor.constraint(equalToConstant: 50)
        stheight?.isActive = true
        
        
        let contentStackview = UIStackView(arrangedSubviews: [alertLb, alertLbAll, alertLbOne, alertLbTwo, alertLbThree])
        contentStackview.distribution = .fillEqually
        contentStackview.axis = .vertical
        contentStackview.translatesAutoresizingMaskIntoConstraints = false
        
        stackview.addSubview(contentStackview)
        stackview.addSubview(barVi)
        
        contentStackview.topAnchor.constraint(equalTo: stackview.topAnchor).isActive = true
        contentStackview.leadingAnchor.constraint(equalTo: stackview.leadingAnchor).isActive = true
        contentStackview.rightAnchor.constraint(equalTo: stackview.rightAnchor).isActive = true
        contentStackview.heightAnchor.constraint(equalToConstant: 250).isActive = true
        
        barVi.leadingAnchor.constraint(equalTo: stackview.leadingAnchor).isActive = true
        barVi.rightAnchor.constraint(equalTo: stackview.rightAnchor).isActive = true
        barVi.bottomAnchor.constraint(equalTo: stackview.bottomAnchor).isActive = true
        barVi.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        setupTable()
    }
    
    func setupTable(){
       
//        let headerHeight = (self.navigationController?.navigationBar.frame.height)! + UIApplication.shared.statusBarFrame.height
        
        tableVi.topAnchor.constraint(equalTo: self.stackview.bottomAnchor, constant: 0).isActive = true
        tableVi.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        tableVi.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
//        tableVi.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -(self.tabBarController?.tabBar.frame.height)!).isActive = true
        tableVi.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
        tableVi.add(border: .top, color: UIColor.black, width: 2)
        
    }
    
    func pricePostAction(vo: AlertVO, save: String, uid: String, sessionid: String) {
        
        let simbol = AlertVO.shared.simbol ?? ""
        let priceType = (AlertVO.shared.priceType ?? .B) == .B ? "2" : "1"
        
        if simbol.contains("_") {
            let simMaket = simbol.components(separatedBy: "_")
            DBManager.sharedInstance.selecttWalletSimbol(simbol: simMaket[0])
            DBManager.sharedInstance.selecttWalletMarket(market: simMaket[1])
        }else {
            DBManager.sharedInstance.selecttWalletSimbol(simbol: simbol)
        }
        
        let url = NSURL(string: "\(AppDelegate.url)/auth/orderAdd")
        
        //request to this file
        let request = NSMutableURLRequest(url: url as! URL)
        
        //method to pass data to this file
        request.httpMethod = "POST"
        
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        //body to be appended to url
        
        guard let orderType = AlertVO.shared.orderType else {
            return
        }
        
        let param = "uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&simbol=\(AlertVO.shared.simbol ?? "")&orderType=\(orderType)&priceType=\(priceType)&orderQty=\(AlertVO.shared.orderQty ?? "")&orderPrice=\(AlertVO.shared.orderPrice ?? "")&waysType=M&privKey1=\(AlertVO.shared.privKey1 ?? "")&privKey2=\(AlertVO.shared.privKey2 ?? "")&saveKeyType=\(save)&date=\(AlertVO.shared.date!)&time=\(AlertVO.shared.time!)"
        
        print("alert maching param : \(param)")
        
        let paramData = param.data(using: .utf8)
        
        request.httpBody = paramData
        
        request.setValue(String(describing: paramData!.count), forHTTPHeaderField: "Content-Length")
        
        //request.httpBody = body.data(using: String.Encoding.utf8, allowLossyConversion: true )
        
        //laucnhing
        let session = URLSession.shared
        let task = session.dataTask(with:request as URLRequest) {data, response, error in
            
//            let result = String(data: data!, encoding: String.Encoding.utf8) as? String (결과 문자열로 확인)
            
            if error == nil {
                DispatchQueue.main.async {
                    do {
                        
                        let object = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        
                        guard let jsonObject = object else { return }
                        
                        print("login id : \(object)")
                        
                        let result = jsonObject["returnYn"] as? String ?? "N"
                        let msg = jsonObject["returnMsg"] as? String ?? ""
                        
                        print("alert result : \(result)")
                        let alert = UIAlertController(title: "alarm_wallet_send_is".localized, message: msg, preferredStyle: .alert)
                        
                        let act = UIAlertAction(title: "common_yes_btn".localized, style: .default) { (action) in
//                            self.navigationController?.popViewController(animated: true)
                            self.alertList(uid: AppDelegate.uid, sessionid: AppDelegate.sessionid, type: .all)
                        }
                        
                        alert.addAction(act)
                        
                        self.present(alert, animated: true, completion: nil)
                        
                    }catch{
                        print("cCaught an error:\(error)")
                    }
                }
            }else{
//                let alert = UIAlertController(title: "alarm_send".localized, message: "alert_send_failed".localized, preferredStyle: .alert)
                let alert = UIAlertController(title: "alarm_wallet_send_is".localized, message: "alert_send_failed".localized, preferredStyle: .alert)
                
                let act = UIAlertAction(title: "common_yes_btn".localized, style: .default) { (action) in
//                    self.navigationController?.popViewController(animated: true)
                    self.alertList(uid: AppDelegate.uid, sessionid: AppDelegate.sessionid, type: .all)
                }
                alert.addAction(act)
                
                self.present(alert, animated: true, completion: nil)
                
                print("error:\(error)")
            }
            
            //launch prepared session
        }
        task.resume()
        
    }
    
    func alertList(uid: String, sessionid: String, type: alerType){
        print("favorite is start")
        
        self.actactivityIndicator.startAnimating()
        
        let urlComponents = URLComponents(string:"\(AppDelegate.url)/ajax/getPushLog?uid=\(uid)&sessionId=\(sessionid)&language=\(AppDelegate.appLang)")!
        
        print("urlcomponents : \(urlComponents)")
        
        let amo = Alamofire.request(urlComponents)
        amo.responseJSON { response in
            
            print("favorite result value : \(response.result.value)")
            
            if let value = response.result.value as? [[String : Any]] {
                
                var tempValue = [[String : Any]]()
                
                switch type {
                    case .login :
                        print("login")

                        for raw in value {
                            let tempType = raw["msgType"] as? String ?? ""
                            if tempType == "1"{
                                tempValue.append(raw)
                            }
                        }
                    
                        self.alertArray = tempValue
                    
                    case .trande:
                        print("trande")
                    
                        for raw in value {
                            let tempType = raw["msgType"] as? String ?? ""
                            if tempType == "2"{
                                tempValue.append(raw)
                            }
                        }
                        
                        self.alertArray = tempValue
            
                    case .wallet:
                        print("wallet")
                    
                        for raw in value {
                            let tempType = raw["msgType"] as? String ?? ""
                            if tempType == "4" || tempType == "5"{
                                tempValue.append(raw)
                            }
                        }
                        
                        self.alertArray = tempValue
                    
                    case .all:
                        print("favorite all : \(value)")
                        self.alertArray = value
                    
                }
                
                self.sortAction()
                
                self.actactivityIndicator.stopAnimating()
            }
        }
    }
    
    func tempAlertList() -> Void {
        self.alertList(uid: AppDelegate.uid, sessionid: AppDelegate.sessionid, type: .all)
    }
    
    func sortAction(){
        
        let arr =  self.alertArray?.sorted(by:{
            
            
            guard let date1 = $0["sendDate"] as? String else {
                return true
            }
            
            guard let time1 = $0["sendTime"] as? String else {
                return true
            }
            
            guard let date2 = $1["sendDate"] as? String else {
                return true
            }
            
            guard let time2 = $1["sendTime"] as? String else {
                return true
            }
            
            let num1 = "\(date1)\(time1)"
            let num2 = "\(date2)\(time2)"
            
            return num1 > num2
            
        })
        
        self.alertArray = arr
        
        tableVi.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return alertArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: tableCellId, for: indexPath) as? AlertDetailCell
        
        guard let tempJson = self.alertArray![indexPath.row] as? [String : Any] else {
            return cell!
        }
        
        cell?.layoutMargins = UIEdgeInsets.zero
        cell?.backgroundColor = UIColor.clear
        cell?.setupcontrant()
        cell?.sendDate.text = self.makeDate(date: tempJson["sendDate"] as? String ?? "")
        cell?.sendTime.text = self.makeTime(date: tempJson["sendTime"] as? String ?? "")
        cell?.msgText.text = tempJson["msgText"] as? String ?? ""
        
        let alertRead = tempJson["readYn"] as? String ?? "Y"
        
        if alertRead == "Y" {
//            cell?.more.text = "읽음"
//            cell?.more.textColor = UIColor.DPmainTextColor
            cell?.imgVi.image = UIImage(named: "icon_read.png")
            
            
        }else {
//            cell?.more.text = "안읽음"
//            cell?.more.textColor = UIColor.DPBuyDefaultColor
            cell?.imgVi.image = UIImage(named: "icon_unread.png")
        }
 
        let alertType = tempJson["msgType"] as? String ?? ""
        
        if alertType == "1"{
            cell?.typeLabel.text = "alert_login".localized
            cell?.msgType.text = "alarm_list_item2".localized
            
        }else if alertType == "2" {
            cell?.typeLabel.text = "alert_trance".localized
            cell?.msgType.text = "alarm_list_item3".localized
            
        }else if alertType == "3" {
            cell?.typeLabel.text = "지정가" //대기
            cell?.msgType.text = "코인 지정가 알림" //대기
            
        }else if alertType == "4" {
            cell?.typeLabel.text = "main_wallet_btn".localized
            cell?.msgType.text = "alarm_list_item4".localized
            
        }else if alertType == "5" {
            cell?.typeLabel.text = "main_wallet_btn".localized
            cell?.msgType.text = "exchange_exchange_inform_title".localized
            
        }else {
            cell?.typeLabel.text = "alarm_so".localized
            cell?.msgType.text = "alarm_list_item5".localized
    
        }
        
        return cell!
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let tempJson = self.alertArray![indexPath.row] as? [String : Any] else {
            return
        }
        
        let alert = UIAlertController(title: "pop_alert".localized, message: "", preferredStyle: .alert)
        
        let vicontroller = UIViewController()
        
        var vi = UIView()
        
        let types = tempJson["msgType"] as? String ?? ""
        let alertRead = tempJson["readYn"] as? String ?? "Y"
        let alertDate = tempJson["sendDate"] as? String ?? "Y"
        let alertTime = tempJson["sendTime"] as? String ?? "Y"
        let priceType = tempJson["priceType"] as? String ?? "2"
        
        let val1 = tempJson["val1"] as? String ?? ""
        let val2 = tempJson["val2"] as? String ?? ""
        let val3 = tempJson["val3"] as? String ?? ""
        let val4 = tempJson["val4"] as? String ?? ""
        let val5 = tempJson["val5"] as? String ?? ""
        let val6 = tempJson["val6"] as? String ?? ""
        
        
        print("alert index : \(indexPath.row)")
        print("alert var5 : \(val5)")
        print("alert var6 : \(val6)")
        
        
        if alertRead == "Y" {
            
            if types == "1" {
                vi = self.alertContentView(strArr: tempJson, msg: "pop_loing".localized, type: "pop_loing".localized, width: vicontroller.view.frame.width, height: 220 )
            }else if types == "2" {

                 vi = self.alertContentView(strArr: tempJson, msg: "main_investment_btn".localized, type: "alert_trance".localized, width: vicontroller.view.frame.width, height: 250 )
                
            }else if types == "3" {
                
                vi = self.alertContentView(strArr: tempJson, msg: "지정가", type: "지정가", width: vicontroller.view.frame.width, height: 250 )
                
            }else if types == "4" {
                
                vi = self.alertContentView(strArr: tempJson, msg: "pop_walletSend".localized, type: "main_wallet_btn".localized, width: vicontroller.view.frame.width, height: 250 )
                
            }else if types == "5" {
                
                vi = self.alertContentView(strArr: tempJson, msg: "pop_wallet_exchange".localized, type: "main_wallet_btn".localized, width: vicontroller.view.frame.width, height: 250 )
                
            }else {
                
                vi = self.alertContentView(strArr: tempJson, msg: "alarm_so".localized, type: "alarm_so".localized, width: vicontroller.view.frame.width, height: 220 )
            }
            
            
            let cancel = UIAlertAction(title: "common_yes_btn".localized, style: .default, handler: nil)
            
            let delete = UIAlertAction(title: "alarm_delete".localized, style: .default) { (_) in
                AppDelegate.alertReadLogAction(uid: AppDelegate.uid, sessionId: AppDelegate.sessionid, date: alertDate, time: alertTime, code: "" ,delete: true, complete: {
                    print("alert delete callback function")
                    self.alertList(uid: AppDelegate.uid, sessionid: AppDelegate.sessionid, type: .all)
                })
            }
            
            
            alert.addAction(delete)
            alert.addAction(cancel)
            
        }else {
            
            var code = ""
            
            if types == "1" {
                vi = self.alertContentView(strArr: tempJson, msg: "alarm_list_item2".localized, type: "alert_login".localized, width: vicontroller.view.frame.width, height: 220 )
                
                let okbtn = UIAlertAction(title: "common_yes_btn".localized, style: .default) { (_) in
                    AppDelegate.alertReadLogAction(uid: AppDelegate.uid, sessionId: AppDelegate.sessionid, date: alertDate, time: alertTime, code:"", delete: false, complete: {
                        print("alert delete callback function")
                        self.alertList(uid: AppDelegate.uid, sessionid: AppDelegate.sessionid, type: .all)
                    })
                }
                
                alert.addAction(okbtn)
                
                
            }else if types == "2" {
                
                code = "50001"
                
                let orderType = tempJson["val2"] as? String ?? "" == "B" ? OrderType.B : OrderType.S
                
                AlertVO.shared.orderPrice = val3
                AlertVO.shared.orderQty = val4
                AlertVO.shared.simbol = val1
                AlertVO.shared.date = alertDate
                AlertVO.shared.time = alertTime
                AlertVO.shared.orderType = orderType
                AlertVO.shared.WaysType = "M"
//                AlertVO.shared.privKey2 = ""
                AlertVO.shared.priceType = priceType == "2" ? PriceType.B : PriceType.A
                
                vi = self.alertContentView(strArr: tempJson, msg: "bid_ask_trade_inform_title".localized, type: "alert_trance".localized, width: vicontroller.view.frame.width, height: 250 )
                
                let btn1 = UIAlertAction(title: "alarm_ok".localized, style: .default, handler: { (action) in
                    AlertVO.shared.saveKeyType = .A
                    self.pricePostAction(vo: AlertVO.shared, save: "1", uid: self.uid , sessionid: self.sessionid)
                })
                let btn2 = UIAlertAction(title: "auto_app".localized, style: .default, handler:{(action) in
                    AlertVO.shared.saveKeyType = .B
                    self.pricePostAction(vo: AlertVO.shared, save: "2", uid: self.uid , sessionid: self.sessionid)
                })
                
                alert.addAction(btn1)
                alert.addAction(btn2)
          
            }else if types == "3" {
                
            }else if types == "4" {
                
                code = "50002"
                
                vi = AppDelegate.alertContentView(strArr: tempJson as! [String : String], msg: "alarm_wallet_send_is".localized, width: vicontroller.view.frame.width, height: 250 )
                let okbtn = UIAlertAction(title: "common_yes_btn".localized, style: .default) { (_) in
                    
                    let passPopVc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "emailAuthID") as! EmailAuthController
                    
                    //                    passPopVc.authdelegate = self
                    passPopVc.alertBool = false
                    passPopVc.sendBool = true
                    passPopVc.simbol = val1
                    passPopVc.address = val2
                    passPopVc.qty = val3
                    passPopVc.fee = val4
                    passPopVc.sendGasLimit = val5
                    passPopVc.sendGwei = val6
                    passPopVc.date = alertDate
                    passPopVc.time = alertTime
                    passPopVc.delegate = self
                    
                    let alertWindow = UIWindow(frame: UIScreen.main.bounds)
                    alertWindow.rootViewController = UIViewController()
                    alertWindow.windowLevel = UIWindow.Level.alert + 1;
                    alertWindow.makeKeyAndVisible()
                    alertWindow.rootViewController?.present(passPopVc, animated: true, completion: nil)
                    
                    
//                    AppDelegate.sendAlertAction(uid: AppDelegate.uid, sessionid: AppDelegate.sessionid, simbol: val1, address: val2, qty: val3, fee: val4, sendGasLimit: val5, sendGwei: val6, date:alertDate , time: alertTime, complete:{
//                        self.alertList(uid: AppDelegate.uid, sessionid: AppDelegate.sessionid, type: .all)
//                    } )
                   
                }
                
                alert.addAction(okbtn)
                
            }else if types == "5" {
                
                code = "50003"
                
                vi = AppDelegate.alertContentView(strArr: tempJson as! [String : String], msg: "alarm_wallet_exchange_is".localized, width: vicontroller.view.frame.width, height: 250 )
                let okbtn = UIAlertAction(title: "common_yes_btn".localized, style: .default) { (_) in
                    
                    let passPopVc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "emailAuthID") as! EmailAuthController
                    
                    //                    passPopVc.authdelegate = self
                    passPopVc.alertBool = false
                    passPopVc.sendBool = false
                    passPopVc.fromsimbol = val1
                    passPopVc.tosimbole = val2
                    passPopVc.qty = val3
                    passPopVc.fee = val4
                    passPopVc.date = alertDate
                    passPopVc.time = alertTime
                    passPopVc.delegate = self
                    
                    let alertWindow = UIWindow(frame: UIScreen.main.bounds)
                    alertWindow.rootViewController = UIViewController()
                    alertWindow.windowLevel = UIWindow.Level.alert + 1;
                    alertWindow.makeKeyAndVisible()
                    alertWindow.rootViewController?.present(passPopVc, animated: true, completion: nil)
                    
                    
//                    AppDelegate.exchangeAlertAction(uid: AppDelegate.uid, sessionid: AppDelegate.sessionid, fromsimbol: val1, tosimbole: val2, qty: val3, sendfee: val4, date: alertDate, time: alertTime, complete: {
//                        self.alertList(uid: AppDelegate.uid, sessionid: AppDelegate.sessionid, type: .all)
//                        })
                }
                
                alert.addAction(okbtn)
                
            }else if types == "6"  {
                vi = self.alertContentView(strArr: tempJson, msg: "alarm_list_item5".localized, type: "alarm_so".localized, width: vicontroller.view.frame.width, height: 220 )
                let okbtn = UIAlertAction(title: "common_yes_btn".localized, style: .default) { (_) in
                    
                    if AppDelegate.mainDeviceBool {
                        
                        let requestFunctionType = tempJson["val1"] as? String ?? ""
                        
                        if requestFunctionType == "delete" {
                            
                            AppDelegate.deviceStopAction()
                            
                        }else if requestFunctionType == "copy"{
                            
                            print("copy 요청 완료 ")
                            AppDelegate.requestCopyPrvateKeyAction()
                            
                        }else if requestFunctionType == "change" {
                            
                            AppDelegate.deviceMainChangeAction()
                            
                        }
                        
                    }
                    
                    
                    
                    //콜백 추가
                    self.alertList(uid: AppDelegate.uid, sessionid: AppDelegate.sessionid, type: .all)
                }
                
                alert.addAction(okbtn)
            }else {
                
                vi = self.alertContentView(strArr: tempJson, msg: "alarm_list_item5".localized, type: "alarm_so".localized, width: vicontroller.view.frame.width, height: 220 )
                
            }
            
            let cancel = UIAlertAction(title: "common_no_btn".localized, style: .cancel, handler: { (_) in
                AppDelegate.alertReadLogAction(uid: AppDelegate.uid, sessionId: AppDelegate.sessionid, date: alertDate, time: alertTime, code:code  ,delete: false, complete: {
                    self.alertList(uid: AppDelegate.uid, sessionid: AppDelegate.sessionid, type: .all)
                })
            })
            
            alert.addAction(cancel)
            
            
        }
        
        vicontroller.view = vi
        
        if types == "1" {
            vicontroller.preferredContentSize.height = 220
        }else {
            vicontroller.preferredContentSize.height = 250
        }
        
        alert.setValue(vicontroller, forKeyPath: "contentViewController")
        
        self.present(alert, animated: false, completion: nil)
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return tableFootVi
    }
    
    //날짜 형식 포멧
    func makeDate(date : String) -> String {
        
        let df : DateFormatter = DateFormatter()
        
        df.dateFormat = "yyyyMMdd"
        
        let date_time : NSDate = (df.date(from: date) as? NSDate)!
        
        df.dateFormat = "yy.MM.dd"
        
        let newDate : String = df.string(from: date_time as Date) as String
        
        return newDate
    }
    
    //날짜 형식 포멧
    func makeTime(date : String) -> String {
        
        let str = String(date.prefix(6))
    
        let df : DateFormatter = DateFormatter()
        
        df.dateFormat = "HHmmss"
        
        let date_time : NSDate = (df.date(from: str) as? NSDate)!
        
        df.dateFormat = "HH:mm:ss"
        
        let newDate : String = df.string(from: date_time as Date) as String
        
        return newDate
    }
    
    
    //리스트 알림 종류
    func alertContentView(strArr : [String : Any], msg : String, type: String, width: CGFloat, height: CGFloat) -> UIView{
        
        let alertDate = self.makeDate(date: strArr["sendDate"] as? String ?? "")
        let alertTime = self.makeTime(date: strArr["sendTime"] as? String ?? "")
        
        let alertMsg = strArr["msgText"] as? String ?? ""
        let alertStatus = strArr["resultMsg"] as? String ?? ""
        
        let attrStr = NSMutableAttributedString(string: "\(alertDate)", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.DPmainTextColor])
        
        attrStr.append(NSAttributedString(string: " \(alertTime)", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12), NSAttributedString.Key.foregroundColor : UIColor.DPsubTextColor]))
        
        
        let vi = UIView()
        vi.frame = CGRect(x: 0 , y: 0, width: width, height: height)
        vi.backgroundColor = UIColor.DPAlertContentBgColor
        
        let topVi = UIView()
        topVi.frame = CGRect(x: 0, y: 0, width: width, height: 1)
        topVi.backgroundColor = UIColor.DPCompanyColor
        
        let contentVi = UIView()
        contentVi.translatesAutoresizingMaskIntoConstraints = false
        contentVi.backgroundColor = UIColor.white
        
        vi.addSubview(contentVi)
        vi.addSubview(topVi)
        
        let lineVi = UIView()
        lineVi.translatesAutoresizingMaskIntoConstraints = false
        lineVi.backgroundColor = UIColor.DPLineBgColor
        
        let frontMessage = UILabel()
        frontMessage.translatesAutoresizingMaskIntoConstraints = false
        frontMessage.text = msg
        frontMessage.textAlignment = .center
        
        
        let statusValue = UILabel()
        statusValue.translatesAutoresizingMaskIntoConstraints = false
        statusValue.font = UIFont.systemFont(ofSize: 14)
        statusValue.textColor = UIColor.DPPlusTextColor
        
        if type == "alert_login".localized {
            statusValue.text = ""
        }else {
            statusValue.text = "\(alertStatus)"
        }
        
        let dateAlertlb = UILabel()
        dateAlertlb.translatesAutoresizingMaskIntoConstraints = false
        dateAlertlb.attributedText = attrStr
        
        let contentText = UITextView()
        contentText.translatesAutoresizingMaskIntoConstraints = false
        contentText.backgroundColor = UIColor.white

        contentText.font = UIFont.systemFont(ofSize: 14)
        contentText.textAlignment = .center
//        contentText.contentInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
//        contentText.contentInset = UIEdgeInsetsMake(0, 5, 0, 5)
        contentText.adjustsFontForContentSizeCategory = true
        contentText.text = "\(alertMsg)"

        
        

        let bottomlineVi = UIView()
        bottomlineVi.translatesAutoresizingMaskIntoConstraints = false
        bottomlineVi.backgroundColor = UIColor.DPLineBgColor
        
        contentVi.addSubview(frontMessage)
        contentVi.addSubview(lineVi)
        contentVi.addSubview(statusValue)
        contentVi.addSubview(dateAlertlb)
        contentVi.addSubview(contentText)
        contentVi.addSubview(bottomlineVi)
        
        contentVi.leadingAnchor.constraint(equalTo: vi.leadingAnchor, constant: 20).isActive = true
        contentVi.topAnchor.constraint(equalTo: vi.topAnchor, constant: 20).isActive = true
        contentVi.trailingAnchor.constraint(equalTo: vi.trailingAnchor, constant: -20).isActive = true
        contentVi.bottomAnchor.constraint(equalTo: vi.bottomAnchor, constant: -20).isActive = true
        
        frontMessage.centerXAnchor.constraint(equalTo: contentVi.centerXAnchor).isActive = true
        frontMessage.topAnchor.constraint(equalTo: contentVi.topAnchor, constant: 10).isActive = true
        
        lineVi.leadingAnchor.constraint(equalTo: contentVi.leadingAnchor).isActive = true
        lineVi.trailingAnchor.constraint(equalTo: contentVi.trailingAnchor).isActive = true
        lineVi.heightAnchor.constraint(equalToConstant: 1).isActive = true
        lineVi.topAnchor.constraint(equalTo: frontMessage.bottomAnchor, constant: 10).isActive = true
        
        statusValue.topAnchor.constraint(equalTo: lineVi.bottomAnchor, constant: 20).isActive = true
        statusValue.centerXAnchor.constraint(equalTo: contentVi.centerXAnchor).isActive = true
        
        dateAlertlb.topAnchor.constraint(equalTo: statusValue.bottomAnchor, constant: 3).isActive = true
        dateAlertlb.centerXAnchor.constraint(equalTo: contentVi.centerXAnchor).isActive = true
        
        contentText.topAnchor.constraint(equalTo: dateAlertlb.bottomAnchor, constant: 15).isActive = true
        contentText.leadingAnchor.constraint(equalTo: contentVi.leadingAnchor, constant : 10).isActive = true
        contentText.trailingAnchor.constraint(equalTo: contentVi.trailingAnchor, constant : -10).isActive = true
        contentText.bottomAnchor.constraint(equalTo: contentVi.bottomAnchor, constant: -10).isActive = true
        
        bottomlineVi.leadingAnchor.constraint(equalTo: contentVi.leadingAnchor).isActive = true
        bottomlineVi.trailingAnchor.constraint(equalTo: contentVi.trailingAnchor).isActive = true
        bottomlineVi.heightAnchor.constraint(equalToConstant: 1).isActive = true
        bottomlineVi.bottomAnchor.constraint(equalTo: contentVi.bottomAnchor).isActive = true
        
        /*
        
        let alertDate = self.makeDate(date: strArr["sendDate"] as? String ?? "")
        let alertTime = self.makeTime(date: strArr["sendTime"] as? String ?? "")
        
        let alertMsg = strArr["msgText"] as? String ?? ""
        let alertStatus = strArr["resultMsg"] as? String ?? ""
        
        let dateAlertlb = UILabel()
        dateAlertlb.translatesAutoresizingMaskIntoConstraints = false
//        dateAlertlb.text = "\(alertDate)"
        
        let attrStr = NSMutableAttributedString(string: "\(alertDate)", attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 14), NSAttributedStringKey.foregroundColor : UIColor.DPmainTextColor])
        
        attrStr.append(NSAttributedString(string: " \(alertTime)", attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 12), NSAttributedStringKey.foregroundColor : UIColor.DPsubTextColor]))
        
//        let timeAlertlb = UILabel()
//        timeAlertlb.translatesAutoresizingMaskIntoConstraints = false
//        timeAlertlb.text = "\(alertTime)"
        
//        let titleValue = UILabel()
//        titleValue.translatesAutoresizingMaskIntoConstraints = false
//        titleValue.text = "알림종류 : \(type)"
//
        let statusValue = UILabel()
//        let contentType = UILabel()
        
        [dateAlertlb, statusValue].forEach {
//            $0.font = UIFon t.systemFont(ofSize: 14)
            contentVi.addSubview($0)
        }
        
        dateAlertlb.attributedText = attrStr
        
        contentVi.addSubview(contentText)
        
        if type == "로그인" {

            statusValue.isHidden = true
//            statusValue.text = "\(alertStatus)"
//            contentType.frame = CGRect(x: 20, y: 105, width: vi.frame.width, height: 20)
//            contentType.text = "알림내용"

//            if width < 400   {
//                contentText.frame = CGRect(x: 20, y: 125, width: width - 150, height: 100)
//            }else {
//                contentText.frame = CGRect(x: 20, y: 125, width: width - 180, height: 100)
//            }

        }else {
            statusValue.isHidden = false
//            statusValue.frame = CGRect(x: 20, y: 105, width: vi.frame.width, height: 20)
//            statusValue.text = "알림결과 : \(alertStatus)"

//            contentType.frame = CGRect(x: 20, y: 135, width: vi.frame.width, height: 20)
//            contentType.text = "알림내용"

//            if width < 400   {
//                contentText.frame = CGRect(x: 20, y: 155, width: width - 150, height: 100)
//            }else {
//                contentText.frame = CGRect(x: 20, y: 155, width: width - 180, height: 100)
//            }
        }
        statusValue.text = "\(alertStatus)"
        
        
        
        
        
//        contentText.topAnchor.constraint(equalTo: dateAlertlb.bottomAnchor, constant: 15).isActive = true
//        contentText.leadingAnchor.constraint(equalTo: contentVi.leadingAnchor).isActive = true
//        contentText.trailingAnchor.constraint(equalTo: contentVi.trailingAnchor).isActive = true
//        contentText.bottomAnchor.constraint(equalTo: contentVi.bottomAnchor, constant: 15).isActive = true
        */
        return vi
    }
    
    func buySellAlertAction(){
        if AppDelegate.payBool {
            AppDelegate.payBool = false
            
            let alert = UIAlertController(title: "pop_alert".localized, message: "pop_aram_trance".localized, preferredStyle: .alert)
            
            let vicontroller = UIViewController()
            let vi = self.alertContentView(width: vicontroller.view.frame.width, height: 140, msg: "alarm_request_order".localized)
            vicontroller.view = vi
            vicontroller.preferredContentSize.height = 140
            alert.setValue(vicontroller, forKeyPath: "contentViewController")
            
            let btn1 = UIAlertAction(title: "alarm_ok".localized, style: .default, handler: { (action) in
                AlertVO.shared.saveKeyType = .A
                self.pricePostAction(vo: AlertVO.shared, save: "1", uid: AppDelegate.uid , sessionid: AppDelegate.sessionid)
                
            })
            let btn2 = UIAlertAction(title: "auto_app".localized, style: .default, handler:{(action) in
                AlertVO.shared.saveKeyType = .B
                self.pricePostAction(vo: AlertVO.shared, save: "2", uid: AppDelegate.uid , sessionid: AppDelegate.sessionid)

            })
            let btn3 = UIAlertAction(title: "common_no_btn".localized, style: .cancel, handler:{(action) in
                AppDelegate.alertReadLogAction(uid: AppDelegate.uid, sessionId: AppDelegate.sessionid, date: AlertVO.shared.date!, time: AlertVO.shared.time!, code:"50001"  ,delete: false, complete: {
                    self.alertList(uid: AppDelegate.uid, sessionid: AppDelegate.sessionid, type: .all)
                })
            })
            
            alert.addAction(btn1)
            alert.addAction(btn2)
            alert.addAction(btn3)
            
            DispatchQueue.main.async {
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    //매수매도 주문
    func alertContentView(width: CGFloat, height: CGFloat, msg: String) -> UIView {
        
        let vi = UIView()
        vi.frame = CGRect(x: 0 , y: 0, width: width, height: height)
        vi.backgroundColor = UIColor.DPAlertContentBgColor
        
        let frontMessage = UILabel()
//        frontMessage.frame = CGRect(x: 100, y: 200, width: vi.frame.width, height: 20)
//        frontMessage.text = msg
//        frontMessage.textAlignment = .left
//        
        let fromWalletlb = UILabel()
        fromWalletlb.frame = CGRect(x: 20, y: 15, width: vi.frame.width, height: 20)
        fromWalletlb.text = "코인 : \(AlertVO.shared.simbol ?? "")"
        
        let toWalletlb = UILabel()
        toWalletlb.frame = CGRect(x: 20, y: 45, width: vi.frame.width, height: 20)
        toWalletlb.text = "주문방법  : \((AlertVO.shared.orderType ?? .S) == .S ? "fragment_title_ask".localized : "fragment_title_bid".localized)"
        
        let coinValue = UILabel()
        coinValue.frame = CGRect(x: 20, y: 75, width: vi.frame.width, height: 20)
        coinValue.text = "trade_graph_qty".localized + " : \(AlertVO.shared.orderQty ?? "")"
        
        let walletSendType = UILabel()
        walletSendType.frame = CGRect(x: 20, y: 105, width: vi.frame.width, height: 20)
        walletSendType.text = "bid_ask_price".localized + " : \(AlertVO.shared.orderPrice ?? "")"
        
        [frontMessage, fromWalletlb, toWalletlb, coinValue, walletSendType].forEach {
            $0.font = UIFont.systemFont(ofSize: 14)
            print("addsubview")
            vi.addSubview($0)
        }
        
        return vi
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        if targetContentOffset.pointee.y == 0.0 && targetContentOffset.pointee.y > scrollView.contentOffset.y {
            alertList(uid: AppDelegate.uid, sessionid: AppDelegate.sessionid, type: sortType)
        }
    }
    
}

enum alerType {
    
    case all
    
    case login
    
    case trande
    
    case wallet
    
}




