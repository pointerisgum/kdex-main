//
//  AppDelegate.swift
//  crevolc
//
//  Created by park heewon on 2017. 12. 11..
//  Copyright © 2017년 hanbiteni. All rights reserved.
//
//
import FBSDKCoreKit
import FBSDKLoginKit
import UIKit
import Alamofire
import CoreData
import Firebase
import UserNotifications
import JWTDecode
import Kingfisher
import Reachability
import AuthenticationServices
//import IgaworksCore

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, FIRMessagingDelegate {
    
    var RTdelegate : RealTimeDelegate? //실시간 델리게이션
    var window: UIWindow? //윈도우 객체
    var configureError : NSError?
    var pushtoken = "" //FCM 토큰 값
    var walletTapBool = false
    let gcmMessageIDKey = "gcm.message_id"
    
    
    static let teamurl = "http://59.6.190.26:5500/" //팀장님 서버
    //    static let url = "http://115.68.52.29/" //개발 서버
    static let url = "https://kdex.io/" //운영 서버
//    static let realUrl = "115.68.190.24" //실시간 서버
    static let realUrl = "115.68.52.86" //실시간 서버
    static let realPort = 50301
    static var localSearch = "" //검색어 저장 변수
    static var didlunchLock = false //잠금 기능 활성화 여부
    static var ownCoinList = false // 보유코인 활성 여부
    //    static var alertReadBool = false
    
    static var devicevo = DeviceVO(pushky: "") // 디바이스 정보 도메인
    static var navibarHeight = 0
    static var alertBool = false
    static var payBool = false
    static var priceBool = false
    static var walletBool = false
    static var mainDeviceBool = false
    static var uid = ""
    static var sessionid = ""
    static var userName = ""
    static var userEmail = ""
    static var userPhoneNumber = ""
    static var userSecretLevel = 0
    static var userBankName = ""
    static var bankAccountNo = ""
    static var coinAllListJson = [[String : Any]]()
    static var favoritesList = [String]()
    static var defaulPrice : Float = 10000000
    static var defaulDoublePrice : Double = 10000000
    static var BTC = 0.0
    static var ETH = 0.0
    static var socialResult = ""
    static var simbole = ""
    static var market = ""
    static var walletListArray = [[String : Any]]()
    static var walletOtehrListArray = [[String : Any]]()
    static var tokenWalletListArray = [[String : Any]]()
    static var favoriesTokenWalletListArray = [[String : Any]]()
    static var myWalletInfo = [String : Any]()
    static var tesetWalletaddr = ""
    static var walletQrcode = ""
    static var alertUserInfo = [String: String]() //알림 정보 저장
    static var totalCoinValue = "" //임시 보유 코인 작업
    static var alertShow = false
    static var ipAddr = ""
    static var copyPkCount = 0
    static var WifiBool = false //와이파이 접속 유무
    static var notiAlertBool = false // 공지 알림 열림 유무
    static var changeAlertBool = false // 환전 알림 열람 유무
    static var newPkBool = false //새로운 주소 확인
    static var deviceAllList = [[String : Any]]() //프라이빗키 체크 디바이스 리스트
    static var subDevicePk = false // 서브 프라이빗키 변경 사항
    static var removeCoin = ""
    static var sideBarInfoBool = false
    static var mcvmBool = false //홀딩 기능 
    
    //    static var appLang = Bundle.main.object(forInfoDictionaryKey: "AppLanguage") as? String ?? ""
    static var appLang = "AppLanguage".localized // 언어 저장 변수
    
    var ethwalletAddr = "" //임시 이더 코인 변수
    
    let reachability = try! Reachability()
    
    static var socekEofError = false {
        didSet{
            if socekEofError {
                print("socket disconet observer")
            }
        }
    }
    
    //google delegate
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!){
        if (error == nil) {
            
            print("google delegate is good")
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            //let accestoken = user.authentication.accessToken
            let fullName = user.profile.name
            //            let givenName = user.profile.givenName
            //            let familyName = user.profile.familyName
            let email = user.profile.email
            
            do{
                let jwt = try decode(jwt: idToken!)
                
                guard let subToken = jwt.body["sub"] else {
                    return
                }
                
                let dvicevo = AppDelegate.devicevo
                
                if email != nil && userId != nil {
                    
                    let socialVo = SocialLoginVO(userid: email!, sType: .G, waysType: "M", socialId: subToken as! String, nickName: fullName!, MoblieNumber: "1111", deviceId:dvicevo.deviceId , pushKey: dvicevo.pushKey, version: dvicevo.version, model: dvicevo.model, os: dvicevo.os)
                    
                    AppDelegate.login(vo: socialVo, vc: nil)
                }
                
            }catch{
                print("error")
            }
            
        } else {
            print("\(error.localizedDescription)")
        }
    }
    
    // 구글 앱 연결 끊을 때 작업 수행
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        
    }
    
    // 앱 실행 최초 호출 메소드
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        print("didFinishLaunchingWithOptions1")
        //        elf.getIFAddresses()
        //        print("AppLanguage : \(AppDelegate.appLang)")
        
        //와이파이 연결 알림 설정
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged), name: Notification.Name.reachabilityChanged, object: reachability)
        
        do{
            try reachability.startNotifier()
        }
        catch {
            print("could not start reachability notifier")
        }
        
        //앱 버전 체크
        //        self.versionCheck()
        
        let plist = UserDefaults.standard
        let uid = plist.string(forKey: "uid")
        let sessionid = plist.string(forKey: "sessionId")
        
        AppDelegate.uid = uid ?? ""
        AppDelegate.sessionid = sessionid ?? ""
        //        AppDelegate.ipAddr = getIPAddress() ?? ""
        
//        print("uid : \(uid), sessionid : \(sessionid), ip : \(AppDelegate.ipAddr) ")
        
//        let applock = plist.bool(forKey: "appLock") // 잠김 확인
//        let applogin = plist.bool(forKey: "loginIs") // 자동 로그인 확인
        
        //앱 바 스타일 지정
//        UIApplication.shared.statusBarStyle = UIStatusBarStyle(rawValue: 1)!
//        UIApplication.shared.statusBarView?.backgroundColor = UIColor.DPStatusBgColor
        
        //        UIApplication.shared.value(forKey: "statusBar")
        
        //파베초기화 설정
        FIRApp.configure()
        
        //알림 벳지 설정
        //        UIApplication.shared.applicationIconBadgeNumber = 0
        
        //DB check
        DBManager.sharedInstance.checkAndCopyDB()
        
        //#sideKick 2021.06.05일 주석 해제 작업 start
        /*
        if applock && applogin {
            //자동 로그인
            AppDelegate.didlunchLock = true
            let storyboard = UIStoryboard(name: "Main", bundle: nil)

            if let detail = storyboard.instantiateViewController(withIdentifier: "lockPad") as? LockPadController {
                detail.lockis = true
                detail.backandfore = true

                window = UIWindow()
                window?.makeKeyAndVisible()
                window?.rootViewController = detail
            }
        //#sideKick 2021.06.28일 주석 처리
//            inglogin()
        }else if !applock && applogin{

            //자동 로그인

            let storyboard = UIStoryboard(name: "Main", bundle: nil)

            if let detail = storyboard.instantiateViewController(withIdentifier: "RevealViewCT") as? RevealViewController {
                window = UIWindow()

                window?.makeKeyAndVisible()
                window?.rootViewController = detail

            }
        //#sideKick 2021.06.28일 주석 처리
//            inglogin()
        }else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)

            if let detail = storyboard.instantiateViewController(withIdentifier: "loginPage") as? EmailLoginViewController {
                window = UIWindow()
                window?.makeKeyAndVisible()
                window?.rootViewController = detail

            }
        }
 */
        //#sideKick 2021.06.05일 주석 해제 end
    
        //google (initialize sign in)
        GGLContext.sharedInstance().configureWithError(&configureError)
        // assert(configureError == nil, "Error configuring Google services: \(configureError)")
        // GIDSignIn.sharedInstance().delegate = self
        
        //네이버 상수 셋팅(해당 값들 셋팅)
        NaverThirdPartyLoginConnection.getSharedInstance().isInAppOauthEnable = true
        NaverThirdPartyLoginConnection.getSharedInstance().isOnlyPortraitSupportedInIphone()
        NaverThirdPartyLoginConnection.getSharedInstance().isNaverAppOauthEnable = true
        
        let naverLoginKit = NaverThirdPartyLoginConnection.getSharedInstance()
        
        print("naver scheme : \(kServiceAppUrlScheme)")
        
        naverLoginKit?.isNaverAppOauthEnable = true
        naverLoginKit?.serviceUrlScheme = kServiceAppUrlScheme
        naverLoginKit?.consumerKey = kConsumerKey
        naverLoginKit?.consumerSecret = kConsumerSecret
        naverLoginKit?.appName = kServiceAppName
        
        naverLoginKit?.setOnlyPortraitSupportInIphone(true)
        
        // MARK: Init Notification
        // 알림 허용 확인
        registerForPushNotifications(application: application)
        
        FIRMessaging.messaging().remoteMessageDelegate = self
        
        // Add observer for InstanceID token refresh callback.
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.tokenRefreshNotification),
                                               name: .firInstanceIDTokenRefresh,
                                               object: nil)
        
        //FCM 토큰 값
        if let token = FIRInstanceID.instanceID().token() {
            
            print("FCM TOKEN : \(token)")
            self.pushtoken = token
            AppDelegate.devicevo.pushKey = token
            connectToFcm()
            
        }
        
        if UIApplication.shared.applicationIconBadgeNumber > 0 {
            print("tester good aler show")
            AppDelegate.alertShow = true
            
            AlertShowController.sharedInstance.alertDelegate?.alerShowAction()
        }
        //애니메이션 스플래쉬
        //        dismissSplashController()
        
//        let appleIDProvider = ASAuthorizationAppleIDProvider()
//            appleIDProvider.getCredentialState(forUserID: /* 로그인에 사용한 User Identifier */) { (credentialState, error) in
//                switch credentialState {
//                case .authorized:
//                    // The Apple ID credential is valid.
//                    print("해당 ID는 연동되어있습니다.")
//                case .revoked:
//                    // The Apple ID credential is either revoked or was not found, so show the sign-in UI.
//                    print("해당 ID는 연동되어있지않습니다.")
//                case .notFound:
//                    // The Apple ID credential is either was not found, so show the sign-in UI.
//                    print("해당 ID를 찾을 수 없습니다.")
//                default:
//                    break
//                }
//      }
        
        //apple 로그인 끊었을 경우
        if #available(iOS 13.0, *) {
            NotificationCenter.default.addObserver(forName: ASAuthorizationAppleIDProvider.credentialRevokedNotification, object: nil, queue: nil) { (Notification) in
                // 로그인 페이지로 이동
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                
                if let detail = storyboard.instantiateViewController(withIdentifier: "loginPage") as? EmailLoginViewController {
                    self.window = UIWindow()
                    self.window?.makeKeyAndVisible()
                    self.window?.rootViewController = detail
                    
                }
                
            }
        }
        
        self.initViController()
        
        return true
    }
    
    //와이파이 연결 확인 메소드
    @objc func reachabilityChanged(note: NSNotification) {
        
        let reachability = note.object as! Reachability
        
        if reachability.isReachable {
            if reachability.isReachableViaWiFi {
                if let addr = self.getIPAddress() {
                    print("ipAddress : \(addr)")
                    AppDelegate.ipAddr = addr
                    AppDelegate.WifiBool = true
                }
            } else {
                //                AppDelegate.ipAddr = tempIp
                AppDelegate.WifiBool = false
                print("Reachable via Cellular")
            }
            
            //            setIPAddress()
        } else {
            AppDelegate.ipAddr = "" // No IP captures
            AppDelegate.WifiBool = false
            print("Network not reachable")
        }
    }
    
    func setIPAddress() {
        if let addr = self.getWiFiAddress() {
            print("ipAddress : \(addr)")
            AppDelegate.ipAddr = addr
        } else {
            AppDelegate.ipAddr = self.getIFAddresses()
            print("No WiFi address")
        }
    }
    
    // Return IP address of WiFi interface (en0) as a String, or `nil`
    func getWiFiAddress() -> String? {
        var address : String?
        
        // Get list of all interfaces on the local machine:
        var ifaddr : UnsafeMutablePointer<ifaddrs>?
        guard getifaddrs(&ifaddr) == 0 else { return nil }
        guard let firstAddr = ifaddr else { return nil }
        
        // For each interface ...
        for ifptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
            let interface = ifptr.pointee
            
            // Check for IPv4 or IPv6 interface:
            let addrFamily = interface.ifa_addr.pointee.sa_family
            if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                
                // Check interface name:
                let name = String(cString: interface.ifa_name)
                
                print("check interface Name : \(name)")
                if  name == "en0" {
                    
                    // Convert interface address to a human readable string:
                    var addr = interface.ifa_addr.pointee
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                    getnameinfo(&addr, socklen_t(interface.ifa_addr.pointee.sa_len),
                                &hostname, socklen_t(hostname.count),
                                nil, socklen_t(0), NI_NUMERICHOST)
                    address = String(cString: hostname)
                }
            }
        }
        freeifaddrs(ifaddr)
        
        print("ip address : \(address)")
        
        return address
    }
    
    func getIFAddresses() -> String {
        
        var address : String = ""
        
        let url = "https://api.ip.pe.kr/"
        
        let amo = Alamofire.request(url)
        amo.responseString { response in
            if response.result.isSuccess == true {
                guard let result = response.result.value else {
                    return
                }
                
                address = result
                print("real address result : \(result)")
            }
            
        }
        
        return address
        
    }
    
    //    func getIFAddresses() -> [String] {
    //        var addresses = [String]()
    //
    //        // Get list of all interfaces on the local machine:
    //        var ifaddr : UnsafeMutablePointer<ifaddrs>?
    //        guard getifaddrs(&ifaddr) == 0 else { return [] }
    //        guard let firstAddr = ifaddr else { return [] }
    //
    //        // For each interface ...
    //        for ptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
    //            let flags = Int32(ptr.pointee.ifa_flags)
    //            let addr = ptr.pointee.ifa_addr.pointee
    //
    //            // Check for running IPv4, IPv6 interfaces. Skip the loopback interface.
    //            if (flags & (IFF_UP|IFF_RUNNING|IFF_LOOPBACK)) == (IFF_UP|IFF_RUNNING) {
    //                if addr.sa_family == UInt8(AF_INET) || addr.sa_family == UInt8(AF_INET6) {
    //
    //                    // Convert interface address to a human readable string:
    //                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
    //                    if (getnameinfo(ptr.pointee.ifa_addr, socklen_t(addr.sa_len), &hostname, socklen_t(hostname.count),
    //                                    nil, socklen_t(0), NI_NUMERICHOST) == 0) {
    //                        let address = String(cString: hostname)
    //                        addresses.append(address)
    //                    }
    //                }
    //            }
    //        }
    //
    //        print("ip address \(addresses)")
    //
    //        freeifaddrs(ifaddr)
    //        return addresses
    //    }
    
    
    //////////////////////////////////////////////////////////////////////////////////////
    
    private func splachScreen() {
        
        //        let launchScreenVC = UIStoryboard.init(name: "LaunchScreen", bundle: nil)
        //
        //        let rootVC = launchScreenVC.instantiateViewController(withIdentifier: "splashController")
        //        rootVC.view.backgroundColor = UIColor.black
        //
        //        self.window?.rootViewController = rootVC
        //        self.window?.makeKeyAndVisible()
        
        //        Timer.scheduledTimer(timeInterval:0, target: self, selector: #selector(dismissSplashController), userInfo: nil, repeats: false)
        
        
    }
    
    //사용중지
    func dismissSplashController() {
        
        let loaddVC = UIStoryboard.init(name: "Main", bundle: nil)
        guard let rootVc = loaddVC.instantiateViewController(withIdentifier: "loaddContrller") as? LanchController else {
            return
        }
        
        self.window?.rootViewController = rootVc
        self.window?.makeKeyAndVisible()
        
        //        Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(initViController), userInfo: nil, repeats: false)
        
    }
    
    func initViController() {
        
        let plist = UserDefaults.standard
        
        let applock = plist.bool(forKey: "appLock")
        let applogin = plist.bool(forKey: "loginIs")
        
        if applock && applogin {
            //자동 로그인
            AppDelegate.didlunchLock = true
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            if let detail = storyboard.instantiateViewController(withIdentifier: "lockPad") as? LockPadController {
                detail.lockis = true
                detail.backandfore = true
                
                window = UIWindow()
                window?.makeKeyAndVisible()
                window?.rootViewController = detail
            }
            
            inglogin()
        }else if !applock && applogin{
            
            //자동 로그인
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            if let detail = storyboard.instantiateViewController(withIdentifier: "RevealViewCT") as? RevealViewController {
                window = UIWindow()
                window?.makeKeyAndVisible()
                window?.rootViewController = detail
                
            }
            
            inglogin()
        }else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            if let detail = storyboard.instantiateViewController(withIdentifier: "loginPage") as? EmailLoginViewController {
                window = UIWindow()
                window?.makeKeyAndVisible()
                window?.rootViewController = detail
                
            }
        }
        
    }
    
    func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        print("appdelegation applicationDidEnterBackground")
        TcpSocket.sharedInstance.disconnect()
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        print("tester badge : \(UIApplication.shared.applicationIconBadgeNumber)")
        
        if UIApplication.shared.applicationIconBadgeNumber > 0 {
            print("tester good aler show")
            AppDelegate.alertShow = true
            
            AlertShowController.sharedInstance.alertDelegate?.alerShowAction()
        }
        
        let plist = UserDefaults.standard
        
        let applock = plist.bool(forKey: "appLock")
        let applogin = plist.bool(forKey: "loginIs")
        
        if applock && applogin {
            
            print("app lock")
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            if let detail = storyboard.instantiateViewController(withIdentifier: "lockPad") as? LockPadController {
                detail.lockis = true
                detail.backandfore = true
                application.topViewController?.present(detail, animated: true, completion: nil)
                
            }
            
        }
        
        //        UIApplication.shared.applicationIconBadgeNumber = 0
        
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        
        reachability.stopNotifier()
        
        NotificationCenter.default.removeObserver(self,name: Notification.Name.reachabilityChanged,object: reachability)
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    //url 콜백 되었을 경우 처리 하는 부분
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        print("naver url OpenURLOptionsKey")
        
        //kakako
        if KOSession.isKakaoAccountLoginCallback(url) {
            return KOSession.handleOpen(url)
        }
        
        //Google
        if GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplication.OpenURLOptionsKey.annotation]) {
            return true
        }
        
        
        print("naver url : \(kServiceAppUrlScheme)")
        //naver
        if url.scheme == kServiceAppUrlScheme {
            if url.host == kCheckResultPage {
                let thirdConnection = NaverThirdPartyLoginConnection.getSharedInstance()
                let resultType : THIRDPARTYLOGIN_RECEIVE_TYPE = (thirdConnection?.receiveAccessToken(url))!
                
                if SUCCESS == resultType {
                    print("success")
                }
                return true
            }
        }
        
        //facebook
        if let handled = ApplicationDelegate.shared.application(app, open: url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as! String, annotation: options[UIApplication.OpenURLOptionsKey.annotation]) as? Bool {
            return handled
        }
        
        return true
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        //kakao
        if KOSession.isKakaoAccountLoginCallback(url){
            return KOSession.handleOpen(url)
        }
        
        //Google
        var options: [String: AnyObject] = [UIApplication.OpenURLOptionsKey.sourceApplication.rawValue: sourceApplication as AnyObject, UIApplication.OpenURLOptionsKey.annotation.rawValue: annotation as AnyObject]
        if GIDSignIn.sharedInstance().handle(url, sourceApplication: sourceApplication, annotation: annotation) {
            return true
        }
        
        //facebook
        if let handled = ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation) as? Bool {
            return handled
        }
        
        return true
    }
    
    //푸쉬 알림 설정
    static func alertContentView(strArr : [String : String], msg : String, width: CGFloat, height: CGFloat) -> UIView{
        let vi = UIView()
        vi.frame = CGRect(x: 0 , y: 0, width: width, height: height)
        vi.backgroundColor = UIColor.DPAlertContentBgColor
        
        //        let frontMessage = UILabel()
        //        frontMessage.frame = CGRect(x: 100, y: 200, width: vi.frame.width, height: 20)
        //        frontMessage.text = msg
        //        frontMessage.textAlignment = .left
        
        var type = ""
        
        let requestFunctionType = strArr["function"] ?? ""
        
        if strArr["pushType"] == nil {
            guard let msgType = strArr["msgType"] else {
                return vi
            }
            type = msgType
            
            let fromWalletlb = UILabel()
            fromWalletlb.frame = CGRect(x: 20, y: 15, width: vi.frame.width, height: 20)
            let formstr = type == "4" ? strArr["val1"] ?? "" : strArr["val1"] ?? ""
            fromWalletlb.text = "exchange_send_wallet".localized + " : \(formstr)"
            
            let toWalletlb = UILabel()
            toWalletlb.frame = CGRect(x: 20, y: 45, width: vi.frame.width, height: 20)
            let tostr = type == "4" ? "\("address_titl".localized) : \(strArr["val2"] ?? "" )" : "exchange_recv_wallet".localized + ": \(strArr["val2"] ?? "")"
            toWalletlb.text = "\(tostr)"
            
            let coinValue = UILabel()
            coinValue.frame = CGRect(x: 20, y: 75, width: vi.frame.width, height: 20)
            let coinstr = type == "4" ? strArr["val3"] ?? "" : strArr["val3"] ?? ""
            coinValue.text = "\("coin_titl".localized) : \(coinstr)"
            
            let walletSendType = UILabel()
            walletSendType.frame = CGRect(x: 20, y: 105, width: vi.frame.width, height: 20)
            let wallettype = type == "4" ? "notify_kdex_wallet_send_is".localized : "notify_kdex_wallet_send_is".localized
            walletSendType.text = "\(wallettype)"
            
            
            [fromWalletlb, toWalletlb, coinValue, walletSendType].forEach {
                $0.font = UIFont.systemFont(ofSize: 14)
                print("addsubview")
                vi.addSubview($0)
            }
            
        }else {
            
            type = strArr["pushType"]!
            
            if type == "6" {
                
                let walletSendType = UILabel()
                walletSendType.frame = CGRect(x: 10, y: 5, width: vi.frame.width, height: 120)
                walletSendType.numberOfLines = 2
                walletSendType.textAlignment = .center
                //                walletSendType.adjustsFontSizeToFitWidth = true
                walletSendType.backgroundColor = UIColor(rgb: 0xf2f2f2)
                
                if requestFunctionType == "delete" {
                    
                    walletSendType.text = "notify_other_wallet_delete_rqu".localized
                    
                }else if requestFunctionType == "copy"{
                    
                    walletSendType.text = "notify_other_wallet_private_copy_rqu".localized
                    
                }else if requestFunctionType == "change" {
                    
                    walletSendType.text = "notify_other_wallet_maindevice_change_rqu".localized
                    
                }
                
                return walletSendType
                
                //                vi.addSubview(walletSendType)
                
            }else {
                
                let fromWalletlb = UILabel()
                fromWalletlb.frame = CGRect(x: 20, y: 15, width: vi.frame.width, height: 20)
                let formstr = type == "4" ? strArr["simbol"] ?? "" : strArr["fromSimbol"] ?? ""
                fromWalletlb.text = "exchange_send_wallet".localized + " : \(formstr)"
                
                let toWalletlb = UILabel()
                toWalletlb.frame = CGRect(x: 20, y: 45, width: vi.frame.width, height: 20)
                let tostr = type == "4" ? "address_titl".localized + " : \(strArr["sendAddr"] ?? "" )" : "exchange_recv_wallet".localized + ": \(strArr["toSimbol"] ?? "")"
                toWalletlb.text = "\(tostr)"
                
                let coinValue = UILabel()
                coinValue.frame = CGRect(x: 20, y: 75, width: vi.frame.width, height: 20)
                let coinstr = type == "4" ? strArr["sendQty"] ?? "" : strArr["exQty"] ?? ""
                coinValue.text = "coin_titl".localized + " : \(coinstr)"
                
                let walletSendType = UILabel()
                walletSendType.frame = CGRect(x: 20, y: 105, width: vi.frame.width, height: 20)
                let wallettype = type == "4" ? "notify_kdex_wallet_send_is".localized : "notify_kdex_wallet_exch_is".localized
                walletSendType.text = "\(wallettype)"
                
                [fromWalletlb, toWalletlb, coinValue, walletSendType].forEach {
                    $0.font = UIFont.systemFont(ofSize: 14)
                    vi.addSubview($0)
                }
                
            }
            
        }
        
        //        guard let type = strArr["pushType"] else {
        //            return vi
        //        }
        
        print("alert type : \(type)")
        
        return vi
    }
}

//알림 처리 기능
extension AppDelegate {
    /**
     Register for push notification.
     
     Parameter application: Application instance.
     */
    
    func registerForPushNotifications(application: UIApplication) {
        
        // iOS 10 support
        // 싱글톤으로 알림타입을 가져와서 아이디를 호출한다.
        // 알림 https://cocoapods.org/pods/arek 오픈소스
        
        // UIDevice.current.systemVersion.hasPrefix("10")
        if #available(iOS 10, *) {
            print("iso10")
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            application.registerForRemoteNotifications()
            
        }
            // iOS 9 support
        else if #available(iOS 9, *) {
            
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            application.registerForRemoteNotifications()
        }
            // iOS 8 support
        else if #available(iOS 8, *) {
            
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            application.registerForRemoteNotifications()
        }
            // iOS 7 support
        else {
            
            application.registerForRemoteNotifications(matching: [.badge, .sound, .alert])
        }
        
    }
    
    @objc func tokenRefreshNotification(_ notification: Notification) {
        
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            print("Notification: refresh token from FCM -> \(refreshedToken)")
            
            self.pushtoken = refreshedToken
            AppDelegate.devicevo.pushKey = refreshedToken
        }
        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm()
    }
    
    //FCM 접속 연결
    func connectToFcm() {
        // Won't connect since there is no token
        guard FIRInstanceID.instanceID().token() != nil else {
            print("FCM: Token does not exist.")
            return
        }
        
        // Disconnect previous FCM connection if it exists.
        FIRMessaging.messaging().disconnect()
        
        FIRMessaging.messaging().connect { (error) in
            if error != nil {
                print("FCM: Unable to connect with FCM. \(error.debugDescription)")
            } else {
                print("Connected to FCM.")
            }
        }
    }
    
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Notification: Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        var token = ""
        for i in 0..<deviceToken.count {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        
        print("Notification: APNs token: \((deviceToken as! NSData))")
        print("Notification: APNs token retrieved: \(token)")
        
    }
    
    
    //알림수신 처리
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        switch application.applicationState {
            
        case .active:  //실행중 알림 수신 상태
            print("active")
            noticationAction(userInfo: userInfo, activeType:  "active")
            break
        case .inactive:
            AppDelegate.alertShow = true
            noticationAction(userInfo: userInfo, activeType: "inactive")
            break
        case .background:
            AppDelegate.alertShow = true
            UIApplication.shared.applicationIconBadgeNumber += 1
            break
        default:
            break
            
        }
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    fileprivate func noticationAction(userInfo: [AnyHashable: Any], activeType : String){
        
        print("userinfo : \(userInfo)")
        
        var message = ""
        var strArr = [String : String]()
        
        let plist = UserDefaults.standard
        let uid = plist.string(forKey: "uid")
        let sessionid = plist.string(forKey: "sessionId")
        print("alert plist uid : \(uid), sessiionid : \(sessionid)")
        print("appdelegate udi : \(AppDelegate.uid), sessionid : \(AppDelegate.sessionid)")
        
        let pushType = userInfo[AnyHashable("pushType")] as? String ?? ""
        strArr.updateValue(pushType, forKey: "pushType")
        let orderPrice = userInfo[AnyHashable("orderPrice")] as? String ?? ""
        let orderQty = userInfo[AnyHashable("orderQty")] as? String ?? ""
        let simbol = userInfo[AnyHashable("simbol")] as? String ?? ""
        strArr.updateValue(simbol, forKey:"simbol")
        let orderType = (userInfo[AnyHashable("orderType")] as? String ?? "") == "B" ? OrderType.B : OrderType.S
        let sendAddr = userInfo[AnyHashable("sendAddr")] as? String ?? ""
        strArr.updateValue(sendAddr, forKey: "sendAddr")
        let sendQty = userInfo[AnyHashable("sendQty")] as? String ?? ""
        strArr.updateValue(sendQty, forKey: "sendQty")
        
        let sendGasLimit = userInfo[AnyHashable("sendGasLimit")] as? String ?? ""
        strArr.updateValue(sendGasLimit, forKey: "sendGasLimit")
        let sendGwei = userInfo[AnyHashable("sendGwei")] as? String ?? ""
        strArr.updateValue(sendGwei, forKey: "sendGwei")
        
        let sendFee = userInfo[AnyHashable("sendFee")] as? String ?? ""
        strArr.updateValue(sendFee, forKey: "sendFee")
        let fromSimbol = userInfo[AnyHashable("fromSimbol")] as? String ?? ""
        strArr.updateValue(fromSimbol, forKey: "fromSimbol")
        let toSimbol = userInfo[AnyHashable("toSimbol")] as? String ?? ""
        strArr.updateValue(toSimbol, forKey: "toSimbol")
        let exQty = userInfo[AnyHashable("exQty")] as? String ?? ""
        strArr.updateValue(exQty, forKey: "exQty")
        let exType = userInfo[AnyHashable("exType")] as? String ?? ""
        strArr.updateValue(exQty, forKey: "exType")
        let exRate = userInfo[AnyHashable("exRate")] as? String ?? ""
        strArr.updateValue(exRate, forKey: "exRate")
        let sendDate = userInfo[AnyHashable("date")] as? String ?? ""
        strArr.updateValue(sendDate, forKey: "sendDate")
        let sendTime = userInfo[AnyHashable("time")] as? String ?? ""
        strArr.updateValue(sendTime, forKey: "sendTime")
        
        let priceType = userInfo[AnyHashable("priceType")] as? String ?? ""
        strArr.updateValue(priceType, forKey: "priceType")
        
        let requestDeviceId = userInfo[AnyHashable("deviceId")] as? String ?? ""
        strArr.updateValue(requestDeviceId, forKey: "deviceId")
        
        let requestFunctionType = userInfo[AnyHashable("function")] as? String ?? ""
        strArr.updateValue(requestFunctionType, forKey: "function")
        
        let tempLan = userInfo[AnyHashable("lang")] as? String ?? "" //긴급 공지 알림 언어 사용
        strArr.updateValue(tempLan, forKey: "lang")
        
        
        
        let aps = userInfo[AnyHashable("aps")] as? NSDictionary
        
        let alert = aps!["alert"] as? NSDictionary
        let title = alert!["title"] as? String ?? ""
        let body = alert!["body"] as? String ?? ""
        
        
        AlertVO.shared.orderPrice = orderPrice
        AlertVO.shared.orderQty = orderQty
        AlertVO.shared.simbol = simbol
        AlertVO.shared.date = sendDate
        AlertVO.shared.time = sendTime
        AlertVO.shared.orderType = orderType
        AlertVO.shared.WaysType = "M"
        
        print("type : \(pushType)")
        var code = ""
        
        if pushType == "1" {
            print("일반 알림 입니다.")
            message = body
            AppDelegate.alertBool = true
        }else if pushType == "2" {
            print("매수 매도 알림 입니다.")
            
            code = "50001"
            
            if orderType == .S {
                message = "notify_trance_alert_coin".localized + "\(simbol)" + "notify_trance_alert_ask_request".localized
            }else {
                message = "notify_trance_alert_coin".localized + "\(simbol)" + "notify_trance_alert_bid_request".localized
            }
            
            //message = "거래 알림 - 코인: \(simbol), 가격: \(orderPrice), 수량: \(orderQty)"
            
            print("DB simbol : \(simbol)")
            
            if simbol.contains("_") {
                let simMaket = simbol.components(separatedBy: "_")
                DBManager.sharedInstance.selecttWalletSimbol(simbol: simMaket[0])
                DBManager.sharedInstance.selecttWalletMarket(market: simMaket[1])
            }else {
                DBManager.sharedInstance.selecttWalletSimbol(simbol: simbol)
            }
            
            AlertVO.shared.priceType = priceType == "2" ? PriceType.B : PriceType.A
            
            AppDelegate.alertBool = true
            AppDelegate.payBool = true
            
        }else if pushType == "3"{
            print("지정가 알림 입니다.")
            message = "notify_alert_fixed_title".localized
            AppDelegate.priceBool = true
        }else if pushType == "4" {
            print("지갑 전송 알림")
            code = "50002"
            message = "notify_alert_wallet_send_title".localized
            AppDelegate.alertUserInfo = strArr
            AppDelegate.walletBool = true
        }else if pushType == "5" {
            print("지갑 교환 알림")
            code = "50003"
            message = "notify_alert_wallet_exchange_title".localized
            AppDelegate.alertUserInfo = strArr
            AppDelegate.walletBool = true
        }else if pushType == "6" {
            print("privatekey copy request")
            code = "50004"
            
            message = "notify_alert_private_copy_title".localized
            AppDelegate.alertUserInfo = strArr
            AppDelegate.walletBool = true
        }else if pushType == "7" {
            print("alert 7번 푸쉬")
            
            //            code = "5000"
            AppDelegate.changeAlertBool = true
            message = "KDA 토큰 환전신청을 승인하시겠습니까?"
            
        }else if pushType == "99"{
            
            message = body
            AppDelegate.alertUserInfo = strArr
            AppDelegate.notiAlertBool = true
        }else {
            print("기본 알림 입니다.")
            message = body
            AppDelegate.alertBool = true
        }
        
        if activeType != "active" {
            
            print("alert no active is good ")
            
            window = UIWindow()
            window?.makeKeyAndVisible()
            let str = UIStoryboard(name: "Main", bundle: nil)
            
            let revealView = str.instantiateViewController(withIdentifier: "RevealViewCT") as? RevealViewController
            //let frontview = str.instantiateViewController(withIdentifier: "sw_front") as? UITabBarController
            
            window?.rootViewController = revealView
            
        }else {
            
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            
            if pushType == "4" || pushType == "5" {
                
                let vicontroller = UIViewController()
                let vi = AppDelegate.alertContentView(strArr: strArr, msg: message, width: vicontroller.view.frame.width, height: 140 )
                vicontroller.view = vi
                vicontroller.preferredContentSize.height = 140
                alert.setValue(vicontroller, forKeyPath: "contentViewController")
                
            }else if pushType == "6" {
                
                let vicontroller = UIViewController()
                
                let vi = AppDelegate.alertContentView(strArr: strArr, msg: message, width: vicontroller.view.frame.width, height: 120 )
                vicontroller.view = vi
                vicontroller.preferredContentSize.height = 120
                alert.setValue(vicontroller, forKeyPath: "contentViewController")
                
            }
                //            else if pushType == "7" {
                //
                //                let vicontroller = UIViewController()
                //
                //                let vi = AppDelegate.alertContentView(strArr: strArr, msg: message, width: vicontroller.view.frame.width, height: 120 )
                //                vicontroller.view = vi
                //                vicontroller.preferredContentSize.height = 120
                //                alert.setValue(vicontroller, forKeyPath: "contentViewController")
                //
                //            }
                
            else if pushType == "99" || pushType == "7" {
                
                AppDelegate.changeAlertBool = false
                
                //                let vicontroller = UIViewController()
                //
                //                let vi = AppDelegate.alertContentView(strArr: strArr, msg: message, width: vicontroller.view.frame.width, height: 120 )
                //                vicontroller.view = vi
                //                vicontroller.preferredContentSize.height = 120
                //
                //                alert.setValue(vicontroller, forKeyPath: "contentViewController")
                
                print("push type 99 : \(strArr)")
                
            }
            
            //이곳에 내용 넣기
            let okbtn = UIAlertAction(title: "common_yes_btn".localized, style: .default) { (_) in
                
                AppDelegate.alertReadLogAction(uid: uid!, sessionId: sessionid!, date: sendDate, time: sendTime, code:code, delete: false)
                
                if pushType == "4" {
                    
                    let passPopVc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "emailAuthID") as! EmailAuthController
                    
                    //                    passPopVc.authdelegate = self
                    passPopVc.alertBool = false
                    passPopVc.sendBool = true
                    passPopVc.simbol = simbol
                    passPopVc.address = sendAddr
                    passPopVc.qty = sendQty
                    passPopVc.fee = sendFee
                    passPopVc.sendGasLimit = sendGasLimit
                    passPopVc.sendGwei = sendGwei
                    passPopVc.date = sendDate
                    passPopVc.time = sendTime
                    
                    self.window?.rootViewController?.present(passPopVc, animated: true, completion: nil)
                    
//                    let alertWindow = UIWindow(frame: UIScreen.main.bounds)
//                    alertWindow.rootViewController = UIViewController()
//                    alertWindow.windowLevel = UIWindow.Level.alert + 1;
//                    alertWindow.makeKeyAndVisible()
//                    alertWindow.rootViewController?.present(passPopVc, animated: true, completion: nil)
                    
                    
                    //                    AppDelegate.sendAlertAction(uid: uid!, sessionid: sessionid!, simbol: simbol, address: sendAddr, qty: sendQty, fee: sendFee, sendGasLimit: sendGasLimit , sendGwei: sendGwei, date: sendDate, time: sendTime, complete:  nil)
                    
                } else if pushType == "5" {
                    
                    let passPopVc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "emailAuthID") as! EmailAuthController
                    
                    //                    passPopVc.authdelegate = self
                    passPopVc.alertBool = false
                    passPopVc.sendBool = false
                    passPopVc.fromsimbol = fromSimbol
                    passPopVc.tosimbole = toSimbol
                    passPopVc.qty = exQty
                    passPopVc.fee = exRate
                    passPopVc.date = sendDate
                    passPopVc.time = sendTime
                    passPopVc.exType = exType
                    
                    self.window?.rootViewController?.present(passPopVc, animated: true, completion: nil)
                    
//                    let alertWindow = UIWindow(frame: UIScreen.main.bounds)
//                    alertWindow.rootViewController = UIViewController()
//                    alertWindow.windowLevel = UIWindow.Level.alert + 1;
//                    alertWindow.makeKeyAndVisible()
//                    alertWindow.rootViewController?.present(passPopVc, animated: true, completion: nil)
                    
                    //                    AppDelegate.exchangeAlertAction(uid: uid!, sessionid: sessionid!, fromsimbol: fromSimbol, tosimbole: toSimbol, qty: exQty, sendfee: exRate, date: sendDate, time: sendTime, complete: nil)
                    
                } else if pushType == "6"{
                    
                    if requestFunctionType == "delete" {
                        
                        AppDelegate.deviceStopAction()
                        
                    }else if requestFunctionType == "copy"{
                        
                        AppDelegate.requestCopyPrvateKeyAction()
                        
                    }else if requestFunctionType == "change" {
                        
                        AppDelegate.deviceMainChangeAction()
                        
                    }
                    
                } else if pushType == "" {
                    print("default alert")
                    AppDelegate.payBool = false
                    AppDelegate.alertReadLogAction(uid: uid!, sessionId: sessionid!, date: sendDate, time: sendTime, code:code, delete: false)
                    
                }else if pushType == "99"{
                    
                    self.window = UIWindow()
                    
                    let str = UIStoryboard(name: "Main", bundle: nil)
                    
                    let alertview = str.instantiateViewController(withIdentifier: "RevealViewCT") as? RevealViewController
                    
                    self.window?.makeKeyAndVisible()
                    
                    self.window?.rootViewController = alertview
                    
                }else if pushType == "7"{
                    
                    self.ethwalletInfo()
                    
                }else {
                    
                    AppDelegate.alertReadLogAction(uid: uid!, sessionId: sessionid!, date: sendDate, time: sendTime, code: "", delete: false, complete: nil)
                    
                    self.window = UIWindow()
                    
                    let str = UIStoryboard(name: "Main", bundle: nil)
                    
                    //                  let alertview = str.instantiateViewController(withIdentifier: "mainView") as? ViewController
                    let alertview = str.instantiateViewController(withIdentifier: "RevealViewCT") as? RevealViewController
                    //                    let frontview = str.instantiateViewController(withIdentifier: "sw_front") as? UITabBarController // 지정가 알림 사용
                    self.window?.makeKeyAndVisible()
                    
                    self.window?.rootViewController = alertview
                    
                }
                
            }
            
            let cancel = UIAlertAction(title: "common_no_btn".localized, style: .cancel, handler: { (_) in
                AppDelegate.payBool = false
                AppDelegate.alertReadLogAction(uid: uid!, sessionId: sessionid!, date: sendDate, time: sendTime, code:code, delete: false)
            })
            
            if pushType != "" {
                alert.addAction(cancel)
            }
            
            alert.addAction(okbtn)
            
            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
            
//            let alertWindow = UIWindow(frame: UIScreen.main.bounds)
//            alertWindow.rootViewController = UIViewController()
//            alertWindow.windowLevel = UIWindow.Level.alert + 1;
//            alertWindow.makeKeyAndVisible()
//            alertWindow.rootViewController?.present(alert, animated: true, completion: nil)
            
        }
    }
}

@available(iOS 10, *)
extension AppDelegate: UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler:
        @escaping (UNNotificationPresentationOptions) -> Void) {
        print("Notification: iOS 10 delegate(willPresent notification)")
        let userInfo = notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        completionHandler([])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Notification: iOS 10 delegate(didReceive response)")
        
        let userInfo = response.notification.request.content.userInfo
        
        print(userInfo)
        
        let aps = userInfo[AnyHashable("pushType")] as! String
        print("pushType: \(aps)")
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        completionHandler()
    }
    
}

// 소셜 로그인 로그 아웃 처리 및 지갑 생성 처리
extension AppDelegate {
    
    static func login(vo : SocialLoginVO, vc: UIViewController?){
        
        let plist = UserDefaults.standard
        plist.set(true, forKey: "emailLogin")
        
        let applogin = plist.bool(forKey: "loginIs")
        plist.synchronize()
        
        let userEmail = plist.string(forKey: "userEmail") ?? ""
        
        let param = "userId=\(vo.userid)&sType=\(vo.sType)&waysType=\(vo.waysType)&socialId=\(vo.socialId)&nickName=\(vo.nickName)&mobileNumber=\(vo.MoblieNumber)&deviceId=\(vo.deviceId)&pushKey=\(vo.pushKey)&version=\(vo.version)&model=\(vo.model)&os=\(vo.os)&ip=\(AppDelegate.ipAddr)"
        
        print("social login : \(param)")
        
        if userEmail != vo.userid && userEmail != "" {
            
            DispatchQueue.main.async {
                
                let alert = UIAlertController(title: "alert_login".localized, message: "alert_login_current_not_msg".localized, preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
                
                guard let appdel = UIApplication.shared.delegate as? AppDelegate else {
                    return
                }
                
                appdel.window?.rootViewController?.present(alert, animated: true, completion: nil)
                
//                let alertWindow = UIWindow()
//                alertWindow.rootViewController = UIViewController()
//                alertWindow.windowLevel = UIWindow.Level.alert + 1
//                alertWindow.makeKeyAndVisible()
//                alertWindow.rootViewController?.present(alert, animated: true, completion: nil)
//
                logout(stype: vo.sType.rawValue)
                
            }
            
            return
        }
        
        let paramData = param.data(using: .utf8)
        
        let url = URL(string: "\(AppDelegate.url)auth/socialLoginProc")
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        request.httpBody = paramData
        
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        request.setValue(String(describing: paramData!.count), forHTTPHeaderField: "Content-Length")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if applogin == false {
                
                if let e = error  {
                    
                    NSLog("login error has occurred : \(e)")
                    
                    DispatchQueue.main.async {
                        
                        let alert = UIAlertController(title: "alert_login".localized, message: "alert_login_current_not_msg".localized, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
                        
                        guard let appdel = UIApplication.shared.delegate as? AppDelegate else {
                            return
                        }
                        
                        appdel.window?.rootViewController?.present(alert, animated: true, completion: nil)
                        
//                        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
//                        alertWindow.rootViewController = UIViewController()
//                        alertWindow.windowLevel = UIWindow.Level.alert + 1
//                        alertWindow.makeKeyAndVisible()
//                        alertWindow.rootViewController?.present(alert, animated: true, completion: nil)
                        
                    }
                    
                    return
                }
                
                DispatchQueue.main.async {
                    
                    do{
                        let object = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        
                        guard let jsonObject = object else { return }
                        
                        let result = jsonObject["result"] as? String
                        let msg = jsonObject["resultMsg"] as? String
                        let uid = jsonObject["uid"] as? String
                        let sessionid = jsonObject["sessionId"] as? String
                        
                        print("real address result \(result)")
                        
                        if result == "OK" {
                            
                            print("login success")
                            
                            //                            if userEmail == "" {
                            ////                                plist.set(vo.socialId, forKey: "userEmail")
                            //
                            plist.set(vo.userid, forKey: "userEmail")
                            plist.set(vo.socialId , forKey: "userPasswd")
                            //                            }
                            
                            plist.set(uid ?? "", forKey: "uid")
                            
                            plist.set(sessionid ?? "", forKey: "sessionId")
                            
                            AppDelegate.uid = uid ?? ""
                            AppDelegate.sessionid = sessionid ?? ""
                            
                            print("login uid : \(uid ?? "")")
                            print("login sessionid : \(sessionid ?? "")")
                            print("login stype : \(vo.sType)")
                            
                            plist.set(vo.sType.rawValue, forKey: "sType")
                            
                            plist.set(true, forKey: "loginIs")
                            
                            plist.synchronize()
                            
                            //                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            
                            if let presentvc = vc as? EmailLoginViewController {
                                presentvc.performSegue(withIdentifier: "crevolcLogin", sender: self)
                            }
                            
                            //                            AppDelegate.walletCheck(uid: uid!, sessionid: sessionid!)
                            
                            guard let appdel = UIApplication.shared.delegate as? AppDelegate else {
                                return
                            }
                            
                            appdel.inglogin()
                            
                        }else if result == "JOIN"{
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            
                            guard let presentvc = storyboard.instantiateViewController(withIdentifier: "joinVi") as? JoinViewController else{
                                return
                            }
                            
                            AppDelegate.socialResult = "JOIN"
                            
                            presentvc.socialVo = vo
                            
                            guard let appdel = UIApplication.shared.delegate as? AppDelegate else {
                                return
                            }
                            
                            appdel.window?.rootViewController?.present(presentvc, animated: true, completion: nil)
                            
//                            let win = UIWindow(frame: UIScreen.main.bounds)
//                            win.rootViewController = UIViewController()
//                            win.windowLevel = UIWindow.Level.alert + 1;
//                            win.makeKeyAndVisible()
//                            win.rootViewController?.present(presentvc, animated: true, completion: nil)
                            
                            plist.removeObject(forKey: "uid")
                            plist.removeObject(forKey: "sessionId")
                            
                            logout(stype: vo.sType.rawValue)
                            
                        }else {
                            
                            print("login fail \(msg)")
                            
                            let alert = UIAlertController(title: "alert_login".localized, message: msg ?? "alert_login_fail_msg".localized, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
                            
                            guard let appdel = UIApplication.shared.delegate as? AppDelegate else {
                                return
                            }
                            
                            appdel.window?.rootViewController?.present(alert, animated: true, completion: nil)
                            
//                            let alertWindow = UIWindow(frame: UIScreen.main.bounds)
//                            alertWindow.rootViewController = UIViewController()
//                            alertWindow.windowLevel = UIWindow.Level.alert + 1;
//                            alertWindow.makeKeyAndVisible()
//                            alertWindow.rootViewController?.present(alert, animated: true, completion: nil)
                            
                            plist.removeObject(forKey: "uid")
                            plist.removeObject(forKey: "sessionId")
                            
                            logout(stype: vo.sType.rawValue)
                            
                        }
                        
                    }catch{
                        
                        print("json pasing error : \(error)")
                        
                        plist.removeObject(forKey: "uid")
                        
                        plist.removeObject(forKey: "sessionId")
                        
                    }
                    
                }
                
            }else {
                print("자동 로그인")
            }
            
        }
        task.resume()
    }
    
    //소셜 로그아웃
    static func logout(stype: String){
        print("logout succeess")
        switch stype {
        case "K":
            print("kakao logout")
            KOSession.shared()?.logoutAndClose(completionHandler: { (sucess , error) -> Void in
                if(sucess){
                    KOSession.shared()?.close()
                }
            })
        case "F":
            print("facebook logout")
        case "G":
            print("google logout")
            GIDSignIn.sharedInstance().signOut()
        case "N":
            print("naver logout")
            //NaverThirdPartyLoginConnection.getSharedInstance().resetToken()
            NaverThirdPartyLoginConnection.getSharedInstance().requestDeleteToken()
            
        default:
            print("other log out")
        }
        
        TcpSocket.sharedInstance.disconnect()
        
    }
    
    
    //중지
    //지갑리스트 조회
    static func walletCheck(uid: String, sessionid: String){
        
        let walletList = DBManager.sharedInstance.selecttWalletKey()
        
        //        let url = "\(AppDelegate.url)/mobile/checkWallet?uid=\(uid)&sessionid=\(sessionid)"
        let url = "\(AppDelegate.url)/mobile/getWalletSymbolList?uid=\(uid)&sessionid=\(sessionid)"
        
        let amo = Alamofire.request(url)
        amo.responseString { response in
            if response.result.isSuccess == true {
                guard let result = response.result.value as? [[String: Any]] else {
                    return
                }
                
                for row in result {
                    for temp in walletList {
                        
                        guard let value = temp as? [String : Any] else {
                            return
                        }
                        
                        if (row["wletNo"] as? String ?? "") == (value["wletNo"] as? String ?? ""){
                            break;
                        }else {
                            
                        }
                        
                    }
                    
                }
                
                
                //                if result == "N" {
                //                    print("지갑 생성 되어 있음")
                //                }else{
                //                    print("저장 하지 않은 지갑이 있음")
                //                    AppDelegate.walletCheckPK(uid: uid, sessionid: sessionid)
                //                }
                
            }
            
        }
        
    }
    
    //사용 중지
    static func walletCheckPK(uid: String, sessionid: String){
        print("walletCheckPK start")
        
        let url = "\(AppDelegate.url)/mobile/checkWalletPK?uid=\(uid)&sessionid=\(sessionid)"
        
        let amo = Alamofire.request(url)
        
        amo.responseJSON { response in
            print(response.result.isSuccess)
            
            if response.result.isSuccess == true {
                print("almofire success")
                let result = response.result.value as? [[String : Any]]
                
                for rs in result! {
                    
                    if rs["wletPrikey"] == nil {
                        print("저장 프라이빗 키가 없음")
                        //                        AppDelegate.walletCheckResult(uid: uid, sessionid: sessionid, simbol: rs["simbol"] as! String, procYn: "N")
                        
                    }else {
                        print("저장 성공 ")
                        let Upresult = DBManager.sharedInstance.selecttWalletSimbol(simbol: rs["simbol"] as! String)
                        print(Upresult.count)
                        
                        if Upresult.count != 0 {
                            print("update : \(rs["simbol"] as! String), privateKey: \(rs["wletPrikey"] as! String)  ")
                            //for row in Upresult
                            //                                let _ = DBManager.sharedInstance.UpdateWalletKey(simbol: rs["simbol"] as! String, privateKey: rs["wletPrikey"] as! String)
                            //                                AppDelegate.walletCheckResult(uid: uid, sessionid: sessionid, simbol: rs["simbol"] as! String, procYn: "Y")
                            //  }
                            
                        }else{
                            print("insert : \(rs["simbol"] as! String), privateKey: \(rs["wletPrikey"] as! String)  ")
                            //                            let _ = DBManager.sharedInstance.insertWalletKey(simbol: rs["simbol"] as! String, privateKey: rs["wletPrikey"] as! String)
                            //                            AppDelegate.walletCheckResult(uid: uid, sessionid: sessionid, simbol: rs["simbol"] as! String, procYn: "Y")
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    //사용 중지
    static func walletCheckResult(uid: String, sessionid: String, simbol: String, procYn: String){
        
        let url = "\(AppDelegate.url)/mobile/checkWalletResult?uid=\(uid)&sessionid=\(sessionid)&simbol=\(simbol)&procYn=\(procYn)"
        
        let amo = Alamofire.request(url)
        amo.responseString { response in
            if response.result.isSuccess == true {
                let result = response.result.value
                if result == uid {
                    print("전송 성공")
                }else{
                    print("전송 실패")
                }
            }
        }
        
    }
    
    static func requestCopyPrvateKeyAction() {
        
        var copyBool = false
        
        let walletList = DBManager.sharedInstance.selecttWalletKey()
        
        let plist = UserDefaults.standard
        guard let uid = plist.string(forKey: "uid") else {
            return
        }
        guard let sessionid = plist.string(forKey: "sessionId") else {
            return
        }
        
        print("walletlist count : \(walletList.count)")
        
        if walletList.count != 0 {
            
            for row in walletList {
                
                print("wallet copy")
                
                guard let value = row as? [String : Any] else {
                    return
                }
                
                let url = "\(AppDelegate.url)mobile/sendPrivateKey?uid=\(uid)&sessionId=\(sessionid)&wletNo=\(value["wletNo"] as? String ?? "")&symbol=\(value["symbol"] as? String ?? "")&wletKey=\(value["wletKey"] as? String ?? "")&wletTp=\(value["wletTp"] as? String ?? "")"
                
                print("wallet request copy Url : \(url)")
                
                let amo = Alamofire.request(url)
                amo.responseString { response in
                    
                    if response.result.isSuccess == true {
                        
                        guard let value = response.result.value else {
                            return
                        }
                        
                        print("copy result : \(value)")
                        
                        //                        if value == "OK" {
                        //                            copyBool = true
                        //
                        //                        }else {
                        //                            copyBool = false
                        //
                        //                        }
                        
                    }
                    
                }
                
                //                if copyBool == false {
                //                    break
                //                }
                
            }
            
            AppDelegate.pricateKeyAthNumber()
            print("wallet copy tester")
            
            //            if copyBool {
            //
            //                AppDelegate.pricateKeyAthNumber()
            //
            //            }else {
            //                print("request copy fail")
            //            }
            
        }else {
            print("db count no")
        }
    }
    
    
    static func pricateKeyAthNumber(){
        
        let plist = UserDefaults.standard
        guard let uid = plist.string(forKey: "uid") else {
            return
        }
        guard let sessionid = plist.string(forKey: "sessionId") else {
            return
        }
        
        let url = "\(AppDelegate.url)mobile/createPrivateKeyCopyCert?&uid=\(uid)&sessionId=\(sessionid)"
        
        print("wallet request auth Url : \(url)")
        
        let amo = Alamofire.request(url)
        amo.responseString { response in
            
            if response.result.isSuccess == true {
                
                var msg = ""
                
                guard let result = response.result.value as? String else {
                    return
                }
                
                if result == "" {
                    msg = "privateKew_not_use_number".localized
                }else {
                    msg = result
                }
                
                let alert = UIAlertController(title: "privateKew_confirm_number".localized, message: "\(msg)", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
                
                guard let appdel = UIApplication.shared.delegate as? AppDelegate else {
                    return
                }
                
                appdel.window?.rootViewController?.present(alert, animated: true, completion: nil)
                
//                let alertWindow = UIWindow(frame: UIScreen.main.bounds)
//                alertWindow.rootViewController = UIViewController()
//                alertWindow.windowLevel = UIWindow.Level.alert + 1;
//                alertWindow.makeKeyAndVisible()
//                alertWindow.rootViewController?.present(alert, animated: true, completion: nil)
                
            }
            
        }
        
    }
    
    
    
    static func sendAlertAction(uid: String, sessionid: String, simbol: String, address: String, qty: String, fee: String, sendGasLimit: String , sendGwei: String, date: String, time: String, complete: (() -> Void)? = nil) {
        
        print("alert sendalert action")
        
        print("DB simbol : \(simbol)")
        
        if simbol.contains("_") {
            let simMaket = simbol.components(separatedBy: "_")
            DBManager.sharedInstance.selecttWalletSimbol(simbol: simMaket[0])
            DBManager.sharedInstance.selecttWalletMarket(market: simMaket[1])
        }else {
            DBManager.sharedInstance.selecttWalletSimbol(simbol: simbol)
        }
        
        let walletPrivatekey = AlertVO.shared.privKey1
        
        let url = URL(string: "\(AppDelegate.url)/auth/walletSend")
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let param = "uid=\(uid)&sessionId=\(sessionid)&simbol=\(simbol)&type=2&qty=\(qty)&fee=\(fee)&waysType=M&addr=\(address)&privKey=\(walletPrivatekey!)&gasLimit=\(sendGasLimit)&gwei=\(sendGwei)&date=\(date)&time=\(time)"
        
        print("alert send param : \(param)")
        
        let paramData = param.data(using: .utf8)
        
        request.httpBody = paramData
        
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        request.setValue(String(describing: paramData!.count), forHTTPHeaderField: "Content-Length")
        
        //laucnhing
        let session = URLSession.shared
        
        let task = session.dataTask(with:request as URLRequest) {data, response, error in
            
            let result = String(data: data!, encoding: String.Encoding.utf8) as? String
            
            print("alert result : \(result)")
            
            if error == nil {
                DispatchQueue.main.async {
                    do {
                        
                        let object = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        
                        guard let jsonObject = object else { return }
                        
                        print("login id : \(object)")
                        
                        let result = jsonObject["returnYn"] as? String ?? "N"
                        let msg = jsonObject["returnMsg"] as? String ?? ""
                        
                        let alert = UIAlertController(title: "pop_walletSend".localized, message: msg, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
                        
                        guard let appdel = UIApplication.shared.delegate as? AppDelegate else {
                            return
                        }
                        
                        appdel.window?.rootViewController?.present(alert, animated: true, completion: nil)
                        
//                        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
//                        alertWindow.rootViewController = UIViewController()
//                        alertWindow.windowLevel = UIWindow.Level.alert + 1;
//                        alertWindow.makeKeyAndVisible()
//                        alertWindow.rootViewController?.present(alert, animated: true, completion: nil)
                        
                        if complete != nil {
                            complete!()
                        }
                        
                    }catch{
                        print("cCaught an error:\(error)")
                    }
                }
            }else{
                let alert = UIAlertController(title: "pop_walletSend".localized, message: "alert_send_failed".localized, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
                
                guard let appdel = UIApplication.shared.delegate as? AppDelegate else {
                    return
                }
                
                appdel.window?.rootViewController?.present(alert, animated: true, completion: nil)
                
//                let alertWindow = UIWindow(frame: UIScreen.main.bounds)
//                alertWindow.rootViewController = UIViewController()
//                alertWindow.windowLevel = UIWindow.Level.alert + 1;
//                alertWindow.makeKeyAndVisible()
//                alertWindow.rootViewController?.present(alert, animated: true, completion: nil)
                
                print("error:\(error)")
            }
            
        }
        task.resume()
    }
    
    static func exchangeAlertAction(uid: String, sessionid: String, fromsimbol: String, tosimbole: String, qty: String, sendfee: String, date: String, time: String, exType: String, complete: (() -> Void)? = nil) {
        
        DBManager.sharedInstance.selecttWalletSimbol(simbol: fromsimbol)
        
        let walletPrivatekey = AlertVO.shared.privKey1
        
        let url = URL(string: "\(AppDelegate.url)auth/walletExchange")
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let param = "uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&fromSimbol=\(fromsimbol)&toSimbol=\(tosimbole)&qty=\(qty)&rate=\(sendfee)&waysType=M&privKey=\(walletPrivatekey ?? "")&date=\(date)&time=\(time)&exType=\(exType)"
        
        print("param : \(param)")
        let paramData = param.data(using: .utf8)
        
        request.httpBody = paramData
        
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        request.setValue(String(describing: paramData!.count), forHTTPHeaderField: "Content-Length")
        
        //laucnhing
        let session = URLSession.shared
        let task = session.dataTask(with:request as URLRequest) {data, response, error in
            
            if error == nil {
                DispatchQueue.main.async {
                    do {
                        
                        let object = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        
                        guard let jsonObject = object else { return }
                        
                        print("login id : \(object)")
                        
                        let result = jsonObject["returnYn"] as? String ?? "N"
                        let msg = jsonObject["returnMsg"] as? String ?? ""
                        
                        let alert = UIAlertController(title: "pop_wallet_exchange".localized, message: msg, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
                        
                        guard let appdel = UIApplication.shared.delegate as? AppDelegate else {
                            return
                        }
                        
                        appdel.window?.rootViewController?.present(alert, animated: true, completion: nil)
                        
//                        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
//                        alertWindow.rootViewController = UIViewController()
//                        alertWindow.windowLevel = UIWindow.Level.alert + 1;
//                        alertWindow.makeKeyAndVisible()
//                        alertWindow.rootViewController?.present(alert, animated: true, completion: nil)
                        
                        if complete != nil {
                            complete!()
                        }
                        
                    }catch{
                        print("cCaught an error:\(error)")
                    }
                }
            }else{
                let alert = UIAlertController(title: "pop_wallet_exchange".localized, message: "alert_send_failed".localized, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
                
                guard let appdel = UIApplication.shared.delegate as? AppDelegate else {
                    return
                }
                
                appdel.window?.rootViewController?.present(alert, animated: true, completion: nil)
                
//                let alertWindow = UIWindow(frame: UIScreen.main.bounds)
//                alertWindow.rootViewController = UIViewController()
//                alertWindow.windowLevel = UIWindow.Level.alert + 1;
//                alertWindow.makeKeyAndVisible()
//                alertWindow.rootViewController?.present(alert, animated: true, completion: nil)
                
                print("error:\(error)")
            }
            
        }
        task.resume()
        
    }
    
    static func alertReadLogAction(uid: String, sessionId: String, date:String, time: String, code: String, delete: Bool, complete : (()-> Void)? = nil){
        
        var url = ""
        
        if delete {
            url = "\(AppDelegate.url)/ajax/deleteLog?uid=\(uid)&sessionid=\(sessionId)&date=\(date)&time=\(time)&code=\(code)"
        }else {
            url = "\(AppDelegate.url)/ajax/setReadLog?uid=\(uid)&sessionid=\(sessionId)&date=\(date)&time=\(time)&code=\(code)"
        }
        
        print("alert url : \(url)")
        let amo = Alamofire.request(url)
        amo.responseString { response in
            if response.result.isSuccess == true {
                let result = response.result.value
                if result == "N" {
                    print("알림을 읽음 및 삭제 전송 실패")
                }else{
                    if complete != nil {
                        complete!()
                    }
                    print("알림을 읽음 및 삭제 전송 완료")
                }
            }
        }
    }
    
    static func dataImage(str: String) -> UIImage{
        
        let url: URL! = URL(string: "\(AppDelegate.url)/assets/img/coin/\(str).png")
        
        do {
            
            let imageDate = try Data(contentsOf: url)
            
            guard let image = UIImage(data: imageDate) else {
                return UIImage(named: "KDP")!
            }
            
            return image
            
        }catch{
            return UIImage(named: "KDP")!
        }
        
    }
    
    static func dataImageUrl(str: String) -> URL {
        guard let url = URL(string: "\(AppDelegate.url)/assets/img/coin/\(str).png") else {
            return URL(string: "\(AppDelegate.url)/assets/img/coin/KDP.png")!
        }
        return url
    }
    
    //현재 앱 버전 체크 메소드
    func versionCheck() {
        
        let version = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
        
        if let version = version {
            print("version : \(version)")
        }
        
    }
    
    func getIPAddress() -> String? {
        var address: String?
        var ifaddr: UnsafeMutablePointer<ifaddrs>? = nil
        if getifaddrs(&ifaddr) == 0 {
            var ptr = ifaddr
            while ptr != nil {
                defer { ptr = ptr?.pointee.ifa_next }
                
                let interface = ptr?.pointee
                let addrFamily = interface?.ifa_addr.pointee.sa_family
                if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                    let name:String = String(cString: (interface?.ifa_name)!)
                    if name == "en0" {
                        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                        getnameinfo(interface?.ifa_addr, socklen_t((interface?.ifa_addr.pointee.sa_len)!), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
                        address = String(cString: hostname)
                    }
                }
            }
            freeifaddrs(ifaddr)
        }
        
        print("3ip addr : \(address)")
        return address
    }
    
    
    func inglogin(){
        
        let plist = UserDefaults.standard
        
        let userEmail = plist.string(forKey: "userEmail") ?? ""
        let userPwd = plist.string(forKey: "userPasswd") ?? ""
        
        print("login useremail : \(userEmail)")
        
        let param = "userId=\(userEmail)&userPw=\(userPwd)&deviceId=\(AppDelegate.devicevo.deviceId)&pushKey=\(AppDelegate.devicevo.pushKey)&version=\(AppDelegate.devicevo.version)&model=\(AppDelegate.devicevo.model)&os=\(AppDelegate.devicevo.os)&ip=\(AppDelegate.ipAddr)"
        
        print("ing login para : \(param)")
        
        let paramData = param.data(using: .utf8)
        
//        let url = URL(string: "\(AppDelegate.url)/auth/loginAppProc")
        let url = URL(string: "\(AppDelegate.url)/auth/autoLoginProc")
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        request.httpBody = paramData
        
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        request.setValue(String(describing: paramData!.count), forHTTPHeaderField: "Content-Length")
        
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            
            if let err = error  {
                
                print("login error is failed")
                
                return
                
            }
            
            DispatchQueue.main.async {
                do{
                    let object = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                    
                    guard let jsonObject = object else { return }
                    
                    let result = jsonObject["result"] as? String
                    let uid = jsonObject["uid"] as? String
                    let sessionid = jsonObject["sessionId"] as? String
                    
                    print("udi: \(uid),sessionid: \(sessionid)")
                    
                    if result == "OK" {
                        
                        print("auto login is good")
                        
                        self.initAction()
                        
                    }
                    
                }catch{
                    
                    print("json pasing error : \(error)")
                    
                    
                }
                plist.synchronize()
            }
        }
        task.resume()
    }
    
    //초기작업(메인디바이스 확인)
    func initAction(){
        let plist = UserDefaults.standard
        guard let uid = plist.string(forKey: "uid") else {
            return
        }
        guard let sessionid = plist.string(forKey: "sessionId") else {
            return
        }
        
        let url = "\(AppDelegate.url)mobile/checkMainDevice?deviceId=\(AppDelegate.devicevo.deviceId)&uid=\(uid)&sessionId=\(sessionid)"
        
        print("main deviec check Url : \(url)")
        
        let amo = Alamofire.request(url)
        amo.responseString { response in
            
            if let value = response.result.value as? String {
                
                print("main value : \(value)")
                
                if value == "Y" {
                    AppDelegate.mainDeviceBool = true
                    
                    self.deviceAllList(mainBool: "Y")
                    
                    //                    self.getWalletSimbolPkList()
                    
                }else {
                    AppDelegate.mainDeviceBool = false
                    
                    //삭제디바이스 체크
                    self.deleteDeviceAction()
                    
                }
                
            }
            
        }
        
    }
    
    
    //삭제 디바이스 체크
    func deleteDeviceAction(){
        let plist = UserDefaults.standard
        guard let uid = plist.string(forKey: "uid") else {
            return
        }
        guard let sessionid = plist.string(forKey: "sessionId") else {
            return
        }
        
        let url = "\(AppDelegate.url)mobile/checkDeleteDevice?deviceId=\(AppDelegate.devicevo.deviceId)&uid=\(uid)&sessionId=\(sessionid)"
        
        print("delete deviec check Url : \(url)")
        
        let amo = Alamofire.request(url)
        amo.responseString { response in
            
            if let value = response.result.value {
                
                print("delete value : \(value)")
                
                if value == "Y" {
                    
                    let alert = UIAlertController(title: "device_title_is".localized, message: "device_device_stop_app_exit".localized, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .default, handler: { (_) in
                        
                        print("delete alert button push")
                        
                        let type = plist.object(forKey: "sType") as? String ?? ""
                        
                        plist.set(false, forKey: "loginIs")
                        plist.set(false, forKey: "appLock")
                        plist.set(false, forKey: "touchIdIs")
                        
                        plist.synchronize()
                        
                        let _ = DBManager.sharedInstance.deleteWalletKey()
                        
                        if type != "" {
                            AppDelegate.logout(stype: type)
                        }
                        
                        defer {
                            exit(0)
                        }
                    }))
                    
                    self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                    
//                    let alertWindow = UIWindow(frame: UIScreen.main.bounds)
//                    alertWindow.rootViewController = UIViewController()
//                    alertWindow.windowLevel = UIWindow.Level.alert + 1;
//                    alertWindow.makeKeyAndVisible()
//                    alertWindow.rootViewController?.present(alert, animated: true, completion: nil)
                    
                    
                    
                    
                    
                }else {
                    
                    self.deviceAllList(mainBool: "N")
                    //                    self.getWalletSimbolPkList()
                    
                }
                
            }
            
        }
        
    }
    
    //변경 프라이빗 키 확인
    func updateCheckWalletPk(){
        //        let walletList = DBManager.sharedInstance.selecttWalletKey()
        
        let plist = UserDefaults.standard
        guard let uid = plist.string(forKey: "uid") else {
            return
        }
        
        guard let sessionid = plist.string(forKey: "sessionId") else {
            return
        }
        
        let url = "\(AppDelegate.url)mobile/checkWalletPK?deviceId=\(AppDelegate.devicevo.deviceId)&uid=\(uid)&sessionId=\(sessionid)"
        
        print("wallet list new pk Url : \(url)")
        
        let amo = Alamofire.request(url)
        amo.responseJSON { response in
            
            if response.result.isSuccess == true {
                guard let result = response.result.value as? [[String: Any]] else {
                    return
                }
                
                for row in result {
                    
                    let pkBool = row["wletPrikey"] as? String ?? ""
                    
                    if pkBool != "" {
                        //프라이빗 키 저장
                        DBManager.sharedInstance.UpdateWalletKey(simbol: row["simbol"] as? String ?? "", privateKey: row["wletPrikey"] as? String ?? "", wletNo: row["wletNo"] as? String ?? "")
                        
                        self.privateSaveDone(simbole: row["simbol"] as? String ?? "", walletNo: row["wletNo"] as? String ?? "", walletTp: row["wletTp"] as? String ?? "")
                        
                    }
                    
                }
                
                self.privateSaveDone()
                
            }
            
        }
        
    }
    
    //지갑 프라이빗 키 확인(중지)
    func getWalletSimbolPkList() {
        
        var newPkBool = false
        
        let savePKwalletList = DBManager.sharedInstance.selecttWalletKey()
        
        let plist = UserDefaults.standard
        guard let uid = plist.string(forKey: "uid") else {
            return
        }
        
        guard let sessionid = plist.string(forKey: "sessionId") else {
            return
        }
        
        let url = "\(AppDelegate.url)mobile/getWalletSymbolList?uid=\(uid)&sessionId=\(sessionid)"
        
        print("wallet list pk check Url : \(url)")
        
        print("device walletlist count : \(savePKwalletList.count)")
        
        let amo = Alamofire.request(url)
        amo.responseJSON { response in
            
            if response.result.isSuccess == true {
                guard let result = response.result.value as? [[String: Any]] else {
                    return
                }
                print("wallet list count : \(savePKwalletList)")
                
                if savePKwalletList.count != 0 {
                    
                    var num = 0
                    print("wallet list count value  true")
                    
                    for row in result {
                        
                        for temp in savePKwalletList {
                            
                            guard let value = temp as? [String : Any] else {
                                return
                            }
                            
                            if (row["wletNo"] as? String ?? "") == (value["wletNo"] as? String ?? "") && (row["simbol"] as? String ?? "") == (value["symbol"] as? String ?? "")   {
                                
                                print("temp wallet false")
                                AppDelegate.newPkBool = false
                                break;
                            }else {
                                num += 1
                                print("temp wallet private key : \(num)")
                                AppDelegate.newPkBool = true
                            }
                            
                        }
                        
                    }
                    
                    
                    //12월31일 프라이빗키 저장 방법 변경으로 정지
                    
                    if newPkBool {
                        
                        if AppDelegate.mainDeviceBool{
                            
                            self.newCheckWalletPk()
                            
                        }else {
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            
                            if let detail = storyboard.instantiateViewController(withIdentifier: "privateRequestPop") as? PrivateKeyController {
//                            if let detail = storyboard.instantiateViewController(withIdentifier: "homeViPop") as? HomeViewController {
                                self.window?.rootViewController?.present(detail, animated: true, completion: nil)
////
//                                let alertWindow = UIWindow(frame: UIScreen.main.bounds)
//                                alertWindow.rootViewController = UIViewController()
//                                alertWindow.windowLevel = UIWindow.Level.alert + 1;
//                                alertWindow.makeKeyAndVisible()
//                                alertWindow.rootViewController?.present(detail, animated: true, completion: nil)
                            }
//                                                        self.requstPkCopyAction()
                        }
                        
                    }else {
                        self.privateSaveDone()
                        print("new privatekey is not ")
                    }
                    
                    //                }else {
                    //                    AppDelegate.newPkBool = true
                    //                }
                    
                }else {
                    if AppDelegate.mainDeviceBool {
                        self.newCheckWalletPk()
                    }else {
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        
                        if let detail = storyboard.instantiateViewController(withIdentifier: "privateRequestPop") as? PrivateKeyController {
//                        if let detail = storyboard.instantiateViewController(withIdentifier: "homeViPop") as? HomeViewController {
//
                            self.window?.rootViewController?.present(detail, animated: true, completion: nil)
//
//                            let alertWindow = UIWindow(frame: UIScreen.main.bounds)
//                            alertWindow.rootViewController = UIViewController()
//                            alertWindow.windowLevel = UIWindow.Level.alert + 1;
//                            alertWindow.makeKeyAndVisible()
//                            alertWindow.rootViewController?.present(detail, animated: true, completion: nil)
                        }
                        
                        //                        self.requstPkCopyAction()
                    }
                    
                }
                
                
            }
            
        }
        
    }
    
    //새로운 지갑 프라이빗 키 요청(사용중지)
    func newCheckWalletPk(){
        //        let walletList = DBManager.sharedInstance.selecttWalletKey()
        
        let plist = UserDefaults.standard
        guard let uid = plist.string(forKey: "uid") else {
            return
        }
        
        guard let sessionid = plist.string(forKey: "sessionId") else {
            return
        }
        
        let url = "\(AppDelegate.url)mobile/checkWalletPK?deviceId=\(AppDelegate.devicevo.deviceId)&uid=\(uid)&sessionId=\(sessionid)"
        
        print("wallet list new pk Url : \(url)")
        
        let amo = Alamofire.request(url)
        amo.responseJSON { response in
            
            if response.result.isSuccess == true {
                guard let result = response.result.value as? [[String: Any]] else {
                    return
                }
                
                for row in result {
                    
                    let pkBool = row["wletPrikey"] as? String ?? ""
                    
                    if pkBool != "" {
                        //프라이빗 키 저장
                        DBManager.sharedInstance.insertWalletKey(simbol: row["simbol"] as? String ?? "", privateKey: row["wletPrikey"] as? String ?? "", wletNo: row["wletNo"] as? String ?? "", wletTp: row["wletTp"] as? String ?? "")
                    }
                    
                }
                
                self.privateSaveDone()
                
            }
            
        }
        
    }
    
    //프라이빗 키 저장 확인(새로작업)
    func privateSaveDone(simbole: String, walletNo: String, walletTp: String){
        
        //        let walletList = DBManager.sharedInstance.selecttWalletKey()
        
        let plist = UserDefaults.standard
        guard let uid = plist.string(forKey: "uid") else {
            return
        }
        guard let sessionid = plist.string(forKey: "sessionId") else {
            return
        }
        
        let url = "\(AppDelegate.url)mobile/checkWalletResult?uid=\(uid)&sessionId=\(sessionid)&symbol=\(simbole)&wletNo=\(walletNo)&wletTp=\(walletTp)"
        
        let amo = Alamofire.request(url)
        amo.responseString { response in
            
            if response.result.isSuccess == true {
                guard let result = response.result.value as? String else {
                    return
                }
                
                
                
            }
            
        }
        
    }
    
    
    
    func privateSaveDone(){
        
        //        let walletList = DBManager.sharedInstance.selecttWalletKey()
        
        let plist = UserDefaults.standard
        guard let uid = plist.string(forKey: "uid") else {
            return
        }
        guard let sessionid = plist.string(forKey: "sessionId") else {
            return
        }
        
        let url = "\(AppDelegate.url)mobile/setDevicePK?deviceId=\(AppDelegate.devicevo.deviceId)&walletYn=Y&uid=\(uid)&sessionId=\(sessionid)"
        
        print("wallet list save Done pk Url : \(url)")
        
        let amo = Alamofire.request(url)
        amo.responseString { response in
            
            if response.result.isSuccess == true {
                guard let result = response.result.value as? String else {
                    return
                }
                
                print("wallet save result Done : \(result)")
                
            }
            
        }
        
    }
    
    
    //지갑 주소 복사 요청
    func requstPkCopyAction() {
        
        let plist = UserDefaults.standard
        guard let uid = plist.string(forKey: "uid") else {
            return
        }
        guard let sessionid = plist.string(forKey: "sessionId") else {
            return
        }
        
        //        let alert = UIAlertController(title: "지갑전송", message: "저장되지 않은 지갑 PRIVATE KEY 있습니다.", preferredStyle: .alert)
        //        alert.addAction(UIAlertAction(title: "확인", style: .cancel, handler: { (_) in
        //
        let url = "\(AppDelegate.url)mobile/copyPrivateKeyConfirm?deviceId=\(AppDelegate.devicevo.deviceId)&uid=\(uid)&sessionId=\(sessionid)"
        
        print("wallet request copy Url : \(url)")
        
        let amo = Alamofire.request(url)
        amo.responseString { response in
            
            if response.result.isSuccess == true {
                guard let result = response.result.value as? String else {
                    return
                }
                
                if result == "OK" {
                    
                    print("private key request success")
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    
                    if let detail = storyboard.instantiateViewController(withIdentifier: "PrivatePop") as? PrivateKeyActionController {
                        
                        self.window?.rootViewController?.present(detail, animated: true, completion: nil)
                        
//                        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
//                        alertWindow.rootViewController = UIViewController()
//                        alertWindow.windowLevel = UIWindow.Level.alert + 1;
//                        alertWindow.makeKeyAndVisible()
//                        alertWindow.rootViewController?.present(detail, animated: true, completion: nil)
                    }
                    
                }else {
                    
                    print("private key request fail")
                }
                
            }
            
        }
        
        //        }))
        //
        //        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        //        alertWindow.rootViewController = UIViewController()
        //        alertWindow.windowLevel = UIWindowLevelAlert + 1;
        //        alertWindow.makeKeyAndVisible()
        //        alertWindow.rootViewController?.present(alert, animated: true, completion: nil)
        
    }
    
    func getPrivateKeyList(){
        
        let plist = UserDefaults.standard
        guard let uid = plist.string(forKey: "uid") else {
            return
        }
        guard let sessionid = plist.string(forKey: "sessionId") else {
            return
        }
        
        let url = "\(AppDelegate.url)mobile/getPrivateKeyList?uid=\(uid)&sessionId=\(sessionid)"
        
        print("wallet list new pk Url : \(url)")
        
        let amo = Alamofire.request(url)
        amo.responseJSON { response in
            
            if response.result.isSuccess == true {
                guard let result = response.result.value as? [[String: Any]] else {
                    return
                }
                
                AppDelegate.copyPkCount = result.count
                
                
                
                for row in result {
                    
                    let pkBool = row["wletPrikey"] as? String ?? ""
                    
                    
                    
                    if pkBool != "" {
                        DBManager.sharedInstance.insertWalletKey(simbol: row["simbol"] as? String ?? "", privateKey: row["wletPrikey"] as? String ?? "", wletNo: row["wletNo"] as? String ?? "", wletTp: row["wletTp"] as? String ?? "")
                    }
                    
                }
                
                self.deleteTempPrivateKeyAction(message: "wallet_copy_done".localized, cancel: false)
                
            }
            
        }
        
    }
    
    
    //임시 저장 프라이빗키 삭제
    func deleteTempPrivateKeyAction(message: String,  cancel: Bool ) {
        
        let plist = UserDefaults.standard
        guard let uid = plist.string(forKey: "uid") else {
            return
        }
        guard let sessionid = plist.string(forKey: "sessionId") else {
            return
        }
        
        let url = "\(AppDelegate.url)mobile/deleteTempPrivateKey?uid=\(uid)&sessionId=\(sessionid)"
        
        print("wallet list save Done pk Url : \(url)")
        
        let amo = Alamofire.request(url)
        amo.responseString { response in
            
            if response.result.isSuccess == true {
                guard let result = response.result.value as? String else {
                    return
                }
                
                if result == "OK" {
                    
                    if message != "" {
                        
                        let walletList = DBManager.sharedInstance.selecttWalletKey()
                        
                        if walletList.count == AppDelegate.copyPkCount {
                            
                            self.privateSaveDone()
                            
                        }
                        
                        //                        let alert = UIAlertController(title: "pop_walletSend".localized, message: message, preferredStyle: .alert)
                        //                        alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: {(_) in
                        //                            if cancel {
                        ////                                exit(0)
                        //                            }
                        //                        }))
                        
                        //                        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
                        //                        alertWindow.rootViewController = UIViewController()
                        //                        alertWindow.windowLevel = UIWindowLevelAlert + 1;
                        //                        alertWindow.makeKeyAndVisible()
                        //                        alertWindow.rootViewController?.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                    
                }else {
                    print("delete temp privateKey fail ")
                }
                
            }
            
        }
        
    }
    
    //디바이스 정지 요청
    static func deviceStopAction(){
        
        let url = "\(AppDelegate.url)ajax/deleteDevice?deviceId=\(AppDelegate.alertUserInfo["deviceId"] ?? "")&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)"
        
        print("stop device url \(url)")
        
        let amo = Alamofire.request(url)
        amo.responseString(completionHandler: { response in
            
            guard let rs = response.result.value else {
                
                return
            }
            
            if rs == "OK" {
                print("stop device is good")
            }
            
        })
        
    }
    
    
    //디바이스 메인 변경 요청
    static func deviceMainChangeAction(){
        
        let url = "\(AppDelegate.url)ajax/changeMainDevice?deviceId=\(AppDelegate.alertUserInfo["deviceId"] ?? "")&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)"
        
        print("stop device url \(url)")
        
        let amo = Alamofire.request(url)
        amo.responseString(completionHandler: { response in
            
            guard let rs = response.result.value else {
                return
            }
            
            if rs == "OK" {
                print("change device is good")
                exit(0)
            }
            
        })
        
    }
    
    
    //KDA 환전신청
    func sendTakeAction() {
        
        let url = URL(string: "\(AppDelegate.url)/auth/walletSend")
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let param = "uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&type=3&simbol=KDA&addr=\(self.ethwalletAddr)&waysType=M&privKey=\(AlertVO.shared.privKey1!)"
        
        print("wallet send param : \(param)")
        let paramData = param.data(using: .utf8)
        
        request.httpBody = paramData
        
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        request.setValue(String(describing: paramData!.count), forHTTPHeaderField: "Content-Length")
        
        //laucnhing
        let session = URLSession.shared
        
        let task = session.dataTask(with:request as URLRequest) {data, response, error in
            
            if error == nil {
                
                print("buy sueccess")
                DispatchQueue.main.async {
                    
                    //activity.stopAnimating()
                    do {
                        let object = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        
                        guard let jsonObject = object else { return }
                        
                        //self.sendAddress.text = ""
                        //                        self.sendQty.text = ""
                        //                        self.gasLimitField.text = ""
                        //                        self.sendSpeedfield.text = ""
                        //                        self.sendAddress.text = ""
                        //                        self.isAddress = false
                        //                        self.isChecked = false
                        //                        self.sendfee = ""
                        //                        self.speedBtntap = 0
                        //                        self.manuBtn.titleLabel?.text = "선택"
                        //                        self.sendFees.text = "\(String(format: "%.8f", 0.0))"
                        //                        self.totalAmount.text = "\(String(format: "%.8f", 0.0))"
                        //                        self.isAddress = false
                        //
                        print("jsonobject : \(jsonObject)")
                        
                        let sendResult = jsonObject["returnYn"] as? String ?? "N"
                        
                        print("send result : \(sendResult)")
                        
                        if sendResult == "N" {
                            
                            let msg = jsonObject["returnMsg"] as? String ?? ""
                            let alert = UIAlertController(title: "pop_wallet".localized, message: msg, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: { (_) in
                            }))
                            
                            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                            
//                            let alertWindow = UIWindow(frame: UIScreen.main.bounds)
//                            alertWindow.rootViewController = UIViewController()
//                            alertWindow.windowLevel = UIWindow.Level.alert + 1;
//                            alertWindow.makeKeyAndVisible()
//                            alertWindow.rootViewController?.present(alert, animated: true, completion: nil)
                            
                        }else {
                            
                            let msg = jsonObject["returnMsg"] as? String ?? ""
                            let alert = UIAlertController(title: "pop_wallet".localized, message: msg, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: { (_) in
                            }))
                            
                            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                            
//                            let alertWindow = UIWindow(frame: UIScreen.main.bounds)
//                            alertWindow.rootViewController = UIViewController()
//                            alertWindow.windowLevel = UIWindow.Level.alert + 1;
//                            alertWindow.makeKeyAndVisible()
//                            alertWindow.rootViewController?.present(alert, animated: true, completion: nil)
                            
                        }
                        
                        //                        self.userAmount()
                        
                    }catch{
                        print("cCaught an error:\(error)")
                        //
                        let msg = "pop_wallet_send_not_reply".localized
                        let alert = UIAlertController(title: "pop_wallet".localized, message: msg, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
                        
                        self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                        
//                        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
//                        alertWindow.rootViewController = UIViewController()
//                        alertWindow.windowLevel = UIWindow.Level.alert + 1;
//                        alertWindow.makeKeyAndVisible()
//                        alertWindow.rootViewController?.present(alert, animated: true, completion: nil)
                        
                    }
                }
            }else{
                print("error:\(error)")
                
                let msg = "pop_wallet_send_not_reply".localized
                let alert = UIAlertController(title: "pop_wallet".localized, message: msg, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
                
                self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                
//                let alertWindow = UIWindow(frame: UIScreen.main.bounds)
//                alertWindow.rootViewController = UIViewController()
//                alertWindow.windowLevel = UIWindow.Level.alert + 1;
//                alertWindow.makeKeyAndVisible()
//                alertWindow.rootViewController?.present(alert, animated: true, completion: nil)
            }
            
        }
        
        task.resume()
        
    }
    
    func ethwalletInfo() {
        DBManager.sharedInstance.selecttWalletSimbol(simbol: "ETH")
        
        let activity = UIActivityIndicatorView()
        activity.style = .whiteLarge
        activity.color = UIColor.DPActivityColor
        activity.hidesWhenStopped = true
        activity.startAnimating()
        
        let url = "\(AppDelegate.url)/auth/walletInfo?simbol=ETH&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)"
        
        print(url)
        let amo = Alamofire.request(url)
        amo.responseJSON(completionHandler: { response in
            
            print("buy result :\(response.result.value)")
            
            if response.result.isSuccess == true {
                
                var result = response.result.value as? [String : Any]
                
                self.ethwalletAddr = (result!["wletAddr"] as? String) ?? ""
                
                self.sendTakeAction()
                
            }
            activity.stopAnimating()
        })
    }
    
    
    //프라이빗 체크를 위한 디바이스 리스트 메소드
    func deviceAllList(mainBool : String) {
        
        AppDelegate.deviceAllList.removeAll()
        
        let url = "\(AppDelegate.url)/ajax/getDeviceList?simbol&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)"
        
        print("device url \(url)")
        
        let amo = Alamofire.request(url)
        amo.responseJSON(completionHandler: { response in
            
            guard let tempArray = response.result.value as? [[String : Any]] else {
                return
            }
            
            var pkStatusValue = ""
            
            for row in tempArray {
                
                guard let deviceId = row["deviceId"] as? String else {
                    return
                }
                
                if AppDelegate.devicevo.deviceId == deviceId {
                    pkStatusValue = row["privateKeyYn"] as? String ?? ""
                    
                    print("check device id : \(row["deviceId"] as? String)")
                    break
                }
            }
            
            if mainBool == "Y" {
                if pkStatusValue == "N"{
                    
                    self.updateCheckWalletPk()
                }
            }else {
                
                if pkStatusValue == "N"{
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    
                    if let detail = storyboard.instantiateViewController(withIdentifier: "privateRequestPop") as? PrivateKeyController {
//                    if let detail = storyboard.instantiateViewController(withIdentifier: "homeViPop") as? HomeViewController {
//                        self.window?.rootViewController?.present(detail, animated: true, completion: nil)
                        
//                        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
//                        alertWindow.rootViewController = UIViewController()
//                        alertWindow.windowLevel = UIWindow.Level.alert + 1;
//                        alertWindow.makeKeyAndVisible()
//                        alertWindow.rootViewController?.present(detail, animated: true, completion: nil)
                    }
                }
                
            }
            
        })
    }
}
