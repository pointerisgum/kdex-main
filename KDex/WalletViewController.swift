//
//  WalletViewController.swift
//  crevolc
//
//  Created by crovolc on 2018. 1. 9..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//
// 지갑 뷰 컨트롤러

import UIKit
import Foundation
import Alamofire
import Kingfisher

class WalletViewController : UIViewController, UITableViewDelegate, UITableViewDataSource, RealTimeDelegate {
    
    let activity = UIActivityIndicatorView()
    
    var pointValue = 0.0
    
    var pointCatch = false
    
    var sendAT: (() -> ())?
    
    var reciveAT: (() -> ())?
   
    var coinSimbol : String?
    
    var coinType = ""
    
    var doubleAllAmount = 0.0
    
    var doubleAllPaddingAmoutn = 0.0
    
    var krwAmount = "" //lastprice
    
    var ethwalletAddr = "" //ETH 지갑 주소
    
    var walletPayListJson = [[String : Any]]()
    
    var headerView : UIView = {
        let uv = UIView()
        return uv
    }()
    
    let footerView: UIView = {
        let uv = UIView()
        uv.backgroundColor = UIColor.DPtableFooterBgColor
        return uv
    }()
    
    let tb_dateLabel : UILabel = {
        let lb = UILabel()
        lb.text = "wallet_manage_no_trade_inform".localized
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.textColor = UIColor.DPmainTextColor
        lb.textAlignment = .center
        lb.baselineAdjustment = .alignCenters
        lb.font = UIFont.systemFont(ofSize: 14)
        return lb
    }()
    
    @IBOutlet var sendAbleTitleLabel: UILabel!
    
    @IBOutlet var conTitleLabel: UILabel!
    
    @IBOutlet var sendAbleQtyLabel: UILabel!
    
    @IBOutlet var conculLabel: UILabel!
    
    @IBOutlet var simbolImage: UIImageView!
    
    @IBOutlet var finacalAmount: UILabel!
    
    @IBOutlet var finacalAmountLabel: UILabel!
    
    @IBOutlet var krwFinacalAmount: UILabel!
    
    @IBOutlet var walletPayList: UITableView!
    
    @IBOutlet var sendView: UIView!
    
    @IBOutlet var sendAbleCountVi: UIView!
    
    @IBOutlet var btnSendImg: UIImageView!
    
    @IBOutlet var btnGetImg: UIImageView!
    
    @IBOutlet var conculCountVi: UIView!
    
    @IBOutlet var contentVi: UIView!
    
    @IBOutlet var centerLineVi: UIView!
    
    @IBOutlet var reciveBtn: UIButton!
    
    @IBOutlet var mcvTokenLabel: UILabel!
    
    @IBOutlet var mcvValueLb: UILabel!
    
    @IBOutlet var sendBtn: UIButton!
    
    @IBOutlet var customtockenBtn: UIButton!
    
    @IBOutlet var krwFinaceLabel: UILabel!
    
    @IBOutlet var panndingFinacalAmount: UILabel!
    
    @IBOutlet var panndingKrwAmount: UILabel!
    
    @IBAction func customtockenAction(_ sender: Any) {
        
//        let alert = UIAlertController(title: "Custom Token 추가", message: nil, preferredStyle: .alert)
//
//        alert.addTextField { (textfield) in
//            textfield.placeholder = "Token Contract 주소를 입력해주세요"
//        }
//
//        alert.addTextField{ (textfield) in
//            textfield.placeholder = "토큰 기호"
//        }
//
//        alert.addTextField{ (textfield) in
//            textfield.placeholder = "소수 자릿수"
//            textfield.keyboardType = .numberPad
//        }
//
//        let action = UIAlertAction(title: "취소", style: .cancel, handler: nil)
//        let findAction = UIAlertAction(title: "토큰추가", style: .default, handler: nil)
//
//        alert.view.tintColor = UIColor.black
//        alert.addAction(action)
//        alert.addAction(findAction)
//
//        self.present(alert, animated: false, completion: nil)
        
        print("wallet coin value2 : \(coinSimbol)")
        
        if coinSimbol ?? "" == "KDA" {
            
            sendTakeAction()
            
        }else if coinSimbol ?? "" == "KDP" ||  coinSimbol ?? "" == "KDMP"  {
            
            let alert = UIAlertController(title: "결제", message: "서비스 준비중 입니다.", preferredStyle: .alert)
            
            let action = UIAlertAction(title: "OK", style: .default) { (_) in
                print("wallet click ")
            }
            
            alert.addAction(action)
            
            self.present(alert, animated: false, completion: nil)
            
        }else if coinSimbol ?? "" == "MCVW" {
            
            AppDelegate.mcvmBool = true
            self.sendAT!()
            
        }else {
            
            let tockenListVc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "customTockenId") as! CustomTockenAddController
            //        passPopVc.delegate = self
            
            self.addChild(tockenListVc)
            
            tockenListVc.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            self.view.addSubview(tockenListVc.view)
            tockenListVc.didMove(toParent: self)
            
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("wallet coin value : \(self.coinSimbol)")
        activity.frame = CGRect(x: self.view.frame.width/2 - 50, y: self.view.frame.height/2 - 100, width: 100, height: 100)
        activity.style = .whiteLarge
        activity.color = UIColor.DPActivityColor
        activity.hidesWhenStopped = true
        
        view.addSubview(activity)
        
        customtockenBtn.layer.borderColor = UIColor(rgb: 0x00b9b2).cgColor
        customtockenBtn.layer.borderWidth = 1
        
        simbolImage.kf.setImage(with: AppDelegate.dataImageUrl(str: "\(coinSimbol ?? "")"))

        //self.view.backgroundColor = UIColor.DPViewBackgroundColor
        self.view.backgroundColor = UIColor(red:0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        
        simbolImage.isUserInteractionEnabled = true
        
        let tab = UITapGestureRecognizer(target: self, action: #selector(simbolAnimation))
        simbolImage.addGestureRecognizer(tab)
        
        walletPayList.dataSource = self
        walletPayList.delegate = self
        
//        sendView.layer.borderColor = UIColor.DPLineBgColor.cgColor
//        sendView.layer.borderWidth = 1
        
//        reciveBtn.layer.borderColor = UIColor.DPLineBgColor.cgColor
//        reciveBtn.layer.borderWidth = 1
     
        setupheaderView()
        setupInit()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("wallet send viewwillappear")
        self.pointCatch = false
        userAmount()
        pointAmout()
        payList()
        self.simbolAnimation()
        
        TcpSocket.sharedInstance.receiverThread?.delegate = self //실시간 알림 델리게이트
    }
    
    func setupInit(){
        
        let value = "AppLanguage".localized
        
        if value == "en_US" {
          
            self.btnGetImg.image = UIImage(named: "btn_receive_en")
            self.btnSendImg.image = UIImage(named: "btn_send_en")
            
        }else {
            
            self.btnGetImg.image = UIImage(named: "btn_get")
            self.btnSendImg.image = UIImage(named: "btn_send")
        }
        
        if coinSimbol ?? "" == "ETH"{
            
            self.krwFinacalAmount.isHidden = false
            self.sendAbleQtyLabel.isHidden = false
            self.sendAbleTitleLabel.isHidden = false
            customtockenBtn.isHidden = false
            customtockenBtn.setTitle("커스텀 토큰 추가", for: .normal)
            
            let conculValue = stringtoDouble(str: self.conculLabel.text ?? "") ?? 0
            
//            if conculValue == 0 {
//
//                self.conculLabel.isHidden = true
//                self.conTitleLabel.isHidden = true
//
//                self.sendBtn.translatesAutoresizingMaskIntoConstraints = false
//                self.sendBtn.topAnchor.constraint(equalTo: krwFinacalAmount.bottomAnchor, constant: 47).isActive = true
//                self.reciveBtn.translatesAutoresizingMaskIntoConstraints = false
//                self.reciveBtn.topAnchor.constraint(equalTo: krwFinacalAmount.bottomAnchor, constant: 47).isActive = true
//
//                sendView.heightAnchor.constraint(equalToConstant: 270).isActive = true
//
//            }else {
            
                self.conculLabel.isHidden = false
                self.conTitleLabel.isHidden = false
            
                self.finacalAmountLabel.centerYAnchor.constraint(equalTo: self.simbolImage.centerYAnchor, constant: -28).isActive = true
            
                sendView.heightAnchor.constraint(equalToConstant: 320).isActive = true
                
//            }
            
        }else if coinSimbol ?? "" == "KRW"{
            
            self.krwFinacalAmount.isHidden = true
            self.conculLabel.isHidden = true
            self.conTitleLabel.isHidden = true
            self.sendAbleQtyLabel.isHidden = true
            self.sendAbleTitleLabel.isHidden = true
            self.panndingFinacalAmount.isHidden = true
            self.panndingKrwAmount.isHidden = true
            self.krwFinaceLabel.isHidden = true
            
            if value == "en_US" {
                self.btnGetImg.image = UIImage(named: "btn_deposit_en")
                self.btnSendImg.image = UIImage(named: "btn_withdraw_en")
            }else {
                self.btnGetImg.image = UIImage(named: "btn_deposit")
                self.btnSendImg.image = UIImage(named: "btn_withdraw")
            }
            
            customtockenBtn.isHidden = true
            
            sendAbleCountVi.topAnchor.constraint(equalTo: contentVi.bottomAnchor, constant: 0).isActive = true
        
            conculCountVi.topAnchor.constraint(equalTo: contentVi.bottomAnchor, constant: 0).isActive = true
            
            sendAbleCountVi.heightAnchor.constraint(equalToConstant: 0).isActive = true
            
            conculCountVi.heightAnchor.constraint(equalToConstant: 0).isActive = true
            
            sendView.heightAnchor.constraint(equalToConstant: 180).isActive = true
        
        }else if coinSimbol ?? "" == "KDMP" {
            
            self.conculLabel.isHidden = true
            self.conTitleLabel.isHidden = true
            self.sendAbleQtyLabel.isHidden = true
            self.sendAbleTitleLabel.isHidden = true
            self.reciveBtn.isHidden = true
            self.panndingFinacalAmount.isHidden = true
            self.panndingKrwAmount.isHidden = true
            self.btnGetImg.isHidden = true
            self.centerLineVi.isHidden = true
            self.customtockenBtn.isHidden = true
            self.krwFinaceLabel.text = "보유 KRW"
            
            if value == "en_US" {
                self.btnSendImg.image = UIImage(named: "btn_charge_en")
            }else {
                self.btnSendImg.image = UIImage(named: "btn_charge")
            }
            
//            self.customtockenBtn.setTitle("KDP QR Code 결제", for: .normal)
            
            self.krwFinacalAmount.isHidden = false
            
            self.sendBtn.rightAnchor.constraint(equalTo: sendView.rightAnchor, constant: -100).isActive = true
            self.sendBtn.leftAnchor.constraint(equalTo: sendView.leftAnchor, constant: 100).isActive = true
            
            sendAbleCountVi.topAnchor.constraint(equalTo: contentVi.bottomAnchor, constant: 0).isActive = true
            
            conculCountVi.topAnchor.constraint(equalTo: contentVi.bottomAnchor, constant: 0).isActive = true
            
            sendAbleCountVi.heightAnchor.constraint(equalToConstant: 0).isActive = true
            conculCountVi.heightAnchor.constraint(equalToConstant: 0).isActive = true
           
            
            
            self.finacalAmountLabel.centerYAnchor.constraint(equalTo: self.simbolImage.centerYAnchor, constant: -28).isActive = true
            
            sendView.heightAnchor.constraint(equalToConstant: 180
                ).isActive = true
            
        }else if coinSimbol ?? "" == "KDP" {
            
            self.conculLabel.isHidden = true
            self.conTitleLabel.isHidden = true
            self.sendAbleQtyLabel.isHidden = true
            self.sendAbleTitleLabel.isHidden = true
            self.reciveBtn.isHidden = true
            self.panndingFinacalAmount.isHidden = true
            self.panndingKrwAmount.isHidden = true
            self.btnGetImg.isHidden = true
            self.centerLineVi.isHidden = true
            self.krwFinaceLabel.text = "보유 KRW"
            
            if value == "en_US" {
                self.btnSendImg.image = UIImage(named: "btn_charge_en")
            }else {
                self.btnSendImg.image = UIImage(named: "btn_charge")
            }
            
            self.customtockenBtn.setTitle("KDP QR Code 결제", for: .normal)
            
            self.krwFinacalAmount.isHidden = false
            
            self.sendBtn.rightAnchor.constraint(equalTo: sendView.rightAnchor, constant: -100).isActive = true
            self.sendBtn.leftAnchor.constraint(equalTo: sendView.leftAnchor, constant: 100).isActive = true
            
            sendAbleCountVi.topAnchor.constraint(equalTo: contentVi.bottomAnchor, constant: 0).isActive = true
            
            conculCountVi.topAnchor.constraint(equalTo: contentVi.bottomAnchor, constant: 0).isActive = true
            
            sendAbleCountVi.heightAnchor.constraint(equalToConstant: 0).isActive = true
            conculCountVi.heightAnchor.constraint(equalToConstant: 0).isActive = true
            customtockenBtn.isHidden = false
            
            
            self.finacalAmountLabel.centerYAnchor.constraint(equalTo: self.simbolImage.centerYAnchor, constant: -28).isActive = true
            
            sendView.heightAnchor.constraint(equalToConstant: 240
                ).isActive = true
            
        }else if coinSimbol ?? "" == "KDA"{
            
            DBManager.sharedInstance.selecttWalletSimbol(simbol: "ETH") //이더 주소 저장
            ethwalletInfo()
            
            self.krwFinacalAmount.isHidden = false
            self.conculLabel.isHidden = false
            self.conTitleLabel.isHidden = false
            self.sendAbleQtyLabel.isHidden = false
            self.sendAbleTitleLabel.isHidden = false
            
//            self.conculLabel.isHidden = true
//            self.conTitleLabel.isHidden = true
//            self.sendAbleQtyLabel.isHidden = true
//            self.sendAbleTitleLabel.isHidden = true
//            self.reciveBtn.isHidden = true
//            self.panndingFinacalAmount.isHidden = true
//            self.panndingKrwAmount.isHidden = true
//            self.btnGetImg.isHidden = true
//            self.centerLineVi.isHidden = true
            self.krwFinaceLabel.text = "환전가능수량"
            self.customtockenBtn.setTitle("환전신청", for: .normal)
            
            if value == "en_US" {
                self.btnSendImg.image = UIImage(named: "btn_send_en")
//                self.btnSendImg.image = UIImage(named: "btn_Exchange_en")
            }else {
                self.btnSendImg.image = UIImage(named: "btn_send")
//                self.btnSendImg.image = UIImage(named: "btn_Exchange")
            }
            
            self.finacalAmountLabel.centerYAnchor.constraint(equalTo: self.simbolImage.centerYAnchor, constant: -28).isActive = true
            
            sendView.heightAnchor.constraint(equalToConstant: 320).isActive = true
            
            
//            self.sendBtn.rightAnchor.constraint(equalTo: sendView.rightAnchor, constant: -100).isActive = true
//            self.sendBtn.leftAnchor.constraint(equalTo: sendView.leftAnchor, constant: 100).isActive = true
//
//            sendAbleCountVi.topAnchor.constraint(equalTo: contentVi.bottomAnchor, constant: 0).isActive = true
//
//            conculCountVi.topAnchor.constraint(equalTo: contentVi.bottomAnchor, constant: 0).isActive = true
//
//            sendAbleCountVi.heightAnchor.constraint(equalToConstant: 0).isActive = true
//            conculCountVi.heightAnchor.constraint(equalToConstant: 0).isActive = true
//            customtockenBtn.isHidden = true
            
//            self.finacalAmountLabel.centerYAnchor.constraint(equalTo: self.simbolImage.centerYAnchor, constant: -28).isActive = true
//
//            sendView.heightAnchor.constraint(equalToConstant: 180
//                ).isActive = true
            
        }else if coinSimbol ?? "" == "MCVW" {
            let conculValue = stringtoDouble(str: self.conculLabel.text ?? "") ?? 0
            
            self.panndingFinacalAmount.isHidden = true
            self.panndingKrwAmount.isHidden = true
            
            self.krwFinacalAmount.isHidden = false
            self.conculLabel.isHidden = false
            self.conTitleLabel.isHidden = false
            self.sendAbleQtyLabel.isHidden = false
            self.sendAbleTitleLabel.isHidden = false
            self.mcvValueLb.isHidden = false
            self.mcvTokenLabel.isHidden = false
            
            self.krwFinaceLabel.centerYAnchor.constraint(equalTo: self.simbolImage.centerYAnchor).isActive = true
            
            
            self.customtockenBtn.setTitle("홀딩", for: .normal)
            
            if value == "en_US" {
                self.btnSendImg.image = UIImage(named: "btn_send_en")
                //                self.btnSendImg.image = UIImage(named: "btn_Exchange_en")
            }else {
                self.btnSendImg.image = UIImage(named: "btn_send")
                //                self.btnSendImg.image = UIImage(named: "btn_Exchange")
            }
            
            self.finacalAmountLabel.centerYAnchor.constraint(equalTo: self.simbolImage.centerYAnchor, constant: -28).isActive = true
            
            sendView.heightAnchor.constraint(equalToConstant: 320).isActive = true
//
//            self.finacalAmountLabel.centerYAnchor.constraint(equalTo: self.simbolImage.centerYAnchor, constant: -28).isActive = true
//
//
//
//            sendView.heightAnchor.constraint(equalToConstant: 240).isActive = true
            
            //            }
            
        }else  {
            
            let conculValue = stringtoDouble(str: self.conculLabel.text ?? "") ?? 0
            
//            if conculValue == 0 {
//                self.krwFinacalAmount.isHidden = false
//                self.conculLabel.isHidden = true
//                self.conTitleLabel.isHidden = true
//                self.sendAbleQtyLabel.isHidden = false
//                self.sendAbleTitleLabel.isHidden = false
//
//                customtockenBtn.isHidden = true
//
//                self.sendBtn.translatesAutoresizingMaskIntoConstraints = false
//                self.sendBtn.topAnchor.constraint(equalTo: krwFinacalAmount.bottomAnchor, constant: 47).isActive = true
//                self.reciveBtn.translatesAutoresizingMaskIntoConstraints = false
//                self.reciveBtn.topAnchor.constraint(equalTo: krwFinacalAmount.bottomAnchor, constant: 47).isActive = true
//
//                sendView.heightAnchor.constraint(equalToConstant: 220).isActive = true
//
//            }else {
                self.krwFinacalAmount.isHidden = false
                self.conculLabel.isHidden = false
                self.conTitleLabel.isHidden = false
                self.sendAbleQtyLabel.isHidden = false
                self.sendAbleTitleLabel.isHidden = false
                
                customtockenBtn.isHidden = true
            
                self.finacalAmountLabel.centerYAnchor.constraint(equalTo: self.simbolImage.centerYAnchor, constant: -28).isActive = true
            
                sendView.heightAnchor.constraint(equalToConstant: 240).isActive = true
                
//            }
            
        }
    }
    
    func setupheaderView(){
        headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 1))
        headerView.backgroundColor = UIColor.DPtableCellBgColor
        footerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 100)
        footerView.addSubview(tb_dateLabel)
        setupconstrains()
    }
    
    func setupconstrains(){
        tb_dateLabel.topAnchor.constraint(equalTo: footerView.topAnchor).isActive = true
        tb_dateLabel.leadingAnchor.constraint(equalTo: footerView.leadingAnchor).isActive = true
        tb_dateLabel.rightAnchor.constraint(equalTo: footerView.rightAnchor).isActive = true
        tb_dateLabel.bottomAnchor.constraint(equalTo: footerView.bottomAnchor).isActive = true
    }
    
    @IBAction func receiveAction(_ sender: Any) {
        
        print("click")
        
        if (coinSimbol ?? "") == "KRW" {
            
            if AppDelegate.userSecretLevel > 2 {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                
                if let detail = storyboard.instantiateViewController(withIdentifier: "depositPop") as? DepositController {
                    
                    let alertWindow = UIWindow(frame: UIScreen.main.bounds)
                    alertWindow.rootViewController = UIViewController()
                    alertWindow.windowLevel = UIWindow.Level.alert + 1;
                    alertWindow.makeKeyAndVisible()
                    alertWindow.rootViewController?.present(detail, animated: true, completion: nil)
                    
                }
            }else {
                
                let alert = UIAlertController(title: "입금", message: "인증레벨이 3이상만 가능합니다", preferredStyle: .alert)
                
                let action = UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil)
                
                alert.addAction(action)
                
                self.present(alert, animated: false, completion: nil)
                
            }
            
            
//            self.seviceAlert(vi: self)
            
            
        }else {
            
            reciveAT!()
            
        }
        
    }
    
    @IBAction func sendAction(_ sender: Any) {
        print("click")
        
        if pointValue >= 0.0 && pointCatch {
            if (coinSimbol ?? "") == "KRW" {
                
//                self.seviceAlert(vi: self)
                
                print("appdele : \(AppDelegate.userSecretLevel)")
                
                if AppDelegate.userSecretLevel > 2 {
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    
                    if let detail = storyboard.instantiateViewController(withIdentifier: "withdrawPop") as? WithdrawController {
                        
                        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
                        alertWindow.rootViewController = UIViewController()
                        alertWindow.windowLevel = UIWindow.Level.alert + 1;
                        alertWindow.makeKeyAndVisible()
                        alertWindow.rootViewController?.present(detail, animated: true, completion: nil)
                        
                    }
                    
                }else {
                    
                    let alert = UIAlertController(title: "출금", message: "인증레벨이 3이상만 가능합니다", preferredStyle: .alert)
                    
                    let action = UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil)
                    
                    alert.addAction(action)
                    
                    self.present(alert, animated: false, completion: nil)
                    
                }
                
            }
//            else if (coinSimbol ?? "") == "KDA" {
//
//                sendTakeAction()
//
//            }
            else {
                sendAT!()
            }
        }else{
            if (coinSimbol ?? "") == "KDP" || (coinSimbol ?? "") == "KDMP" {
                
                sendAT!()
                
            }
//            else if (coinSimbol ?? "") == "KDA" {
//                
//                sendTakeAction()
//            
//            }
            else {
                
                let alert = UIAlertController(title: "pop_wallet".localized, message: "pop_kdexPoint".localized + " \(pointValue)" + "pop_kdexPoint_out".localized, preferredStyle: .alert)
                
                let action = UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil)
                
                alert.addAction(action)
                
                self.present(alert, animated: false, completion: nil)
                
            }
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return walletPayListJson.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "walletpayCell", for: indexPath) as? WalletPayListCell
        
        let payJaon = walletPayListJson[indexPath.row]
        let date = payJaon["time"] as? String ?? ""
        let commentType = payJaon["comment"] as? String ?? "" // 1.입출고 2.교환 3.매매 9.기타
        let qty = payJaon["setAmount"] as? String ?? ""
        let sellType = payJaon["buho"] as? String ?? ""

        let txStatus = payJaon["txStatus"] as? String ?? "" // 1.Confirm 2.Pending  9.전송Fail
        let doubleQty = stringtoDouble(str:qty) ?? 0.0
        let simbole = payJaon["symbol"] as? String ?? ""
        
        print("wallet pay List : \(payJaon)")
        cell?.dateLabel.text = self.makeTime(date: date)
        
        if coinSimbol ?? "" == "KDP" || coinSimbol ?? "" == "KRW" || coinSimbol ?? "" == "KDMP"{
            cell?.payPriceLabel.text = "\(String(format: "%.0f", doubleQty).insertComma)"
        }else {
            cell?.payPriceLabel.text = "\(String(format: "%.8f", doubleQty))".insertComma
        }
        cell?.dateLabel.textColor = UIColor.DPsubTextColor
        
        
        var commentTypeValue = ""
        if commentType == "1" {
            commentTypeValue = "[" + "wallet_manage_deposit_withdraw".localized + "]"
        }else if commentType == "2"{
            commentTypeValue = "[" + "wallet_manage_trade".localized + "]"
        }else if commentType == "3"{
            commentTypeValue = "[" + "wallet_manage_buy_sell".localized + "]"
        }else if commentType == "4"{
            commentTypeValue = "[" + "wallet_manage_fee".localized + "]"
        }else if commentType == "5"{
            commentTypeValue = "[" + "wallet_manage_reward".localized + "]"
        }else if commentType == "6"{
            commentTypeValue = "[" + "wallet_manage_token".localized + "]"
        }else  {
            commentTypeValue = "[" + "wallet_manage_etc".localized + "]"
        }
        
        var statusTypeValue = ""
        if txStatus == "1" {
            statusTypeValue = "Confirm"
            cell?.txStatusLabel.backgroundColor = UIColor(rgb: 0x5cb85c)
        }else if txStatus == "2"{
            statusTypeValue = "Pending"
            cell?.txStatusLabel.backgroundColor = UIColor(rgb: 0x204d74)
        }else if txStatus == "9" {
            statusTypeValue = "Fail"
            cell?.txStatusLabel.backgroundColor = UIColor(rgb: 0xFF0000)
        }else {
            statusTypeValue = ""
            cell?.txStatusLabel.backgroundColor = UIColor(rgb: 0x000000)
        }
        cell?.txStatusLabel.textColor = UIColor.white
        cell?.txStatusLabel.text = statusTypeValue
        
        
        if sellType == "1" {
            cell?.contentLabel.text = "\(commentTypeValue) \(simbole) " + "wallet_deposit".localized
            cell?.payPriceLabel.textColor = UIColor.DPBuyDefaultColor
        }else if sellType == "2" {
            cell?.contentLabel.text = "\(commentTypeValue) \(simbole) " + "wallet_withdraw".localized
            cell?.payPriceLabel.textColor = UIColor.DPSellDefaultColor
        }else {
            cell?.contentLabel.text = "\(commentTypeValue) \(simbole)"
            cell?.payPriceLabel.textColor = UIColor.DPSellDefaultColor
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let txjson = walletPayListJson[indexPath.row]
        
        let txAddr = txjson["txAddr"] as? String ?? ""
        
        print("wallet receive : \(coinSimbol), address : \(txAddr)")
        
        if coinSimbol! == "BTC" {
            if let url = URL(string: "\(CoinTxNatwork.BTC.rawValue)\(txAddr)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else if coinSimbol! == "ETH" {
            if let url = URL(string: "\(CoinTxNatwork.ETH.rawValue)\(txAddr)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else if coinSimbol! == "BCH" {
            if let url = URL(string: "\(CoinTxNatwork.BCH.rawValue)\(txAddr)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else if coinSimbol! == "LTC" {
            if let url = URL(string: "\(CoinTxNatwork.LTC.rawValue)\(txAddr)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else if coinSimbol! == "ETC" {
            if let url = URL(string: "\(CoinTxNatwork.ETC.rawValue)\(txAddr)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else if coinSimbol! == "TRX" {
            if let url = URL(string: "\(CoinTxNatwork.TRX.rawValue)\(txAddr)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else if coinSimbol! == "KDA" {
            if let url = URL(string: "\(CoinTxNatwork.ETH.rawValue)\(txAddr)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else if coinSimbol! == "XRP" {
            if let url = URL(string: "\(CoinTxNatwork.XRP.rawValue)\(txAddr)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else if coinSimbol! == "QTUM" {
            if let url = URL(string: "\(CoinTxNatwork.QTUM.rawValue)\(txAddr)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else if coinSimbol == "KDA" || coinSimbol == "DPN" || coinSimbol == "FNK" || coinSimbol == "MCVW" || coinSimbol == "BTR" {
            if let url = URL(string: "\(CoinTxNatwork.ETH.rawValue)\(txAddr)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else {
            if let url = URL(string: "\(CoinTxNatwork.ETH.rawValue)\(txAddr)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }
        
    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        return self.headerView
//    }
//    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 1
//    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 100
    }
    
    func payList() {
        
        activity.startAnimating()
        
        let url = "\(AppDelegate.url)/auth/walletRcpyList?simbol=\(coinSimbol!)&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&beginDate&endDate"
        
        print(url)
        let amo = Alamofire.request(url)
        amo.responseJSON(completionHandler: { response in
    
            if response.result.isSuccess {
                
                guard let tempArray = response.result.value as? [[String : Any]] else {
                    return
                }
                
                self.walletPayListJson = self.dateSort(arr: tempArray)
                
                print("\(self.walletPayListJson)")
                
                
                if self.walletPayListJson.count == 0 {
                    self.tb_dateLabel.isHidden = false
                    self.footerView.isHidden = false
                    
                }else {
                    self.tb_dateLabel.isHidden = true
                    self.footerView.isHidden = true
                }
                
                
                
                self.walletPayList.reloadData()
            }
            
            self.activity.stopAnimating()
            
        })
    }
    
    //날짜 형식 포멧
    func makeTime(date : String) -> String {
        
        let df : DateFormatter = DateFormatter()
        df.dateFormat = "yyyyMMddHHmmss "
        
        guard let formData = df.date(from: date) as? NSDate else {
            return "0.0.0 00:00"
        }
        
        let date_time : NSDate = formData
        
        df.dateFormat = "yy.MM.dd HH:mm:ss"
        
        let newDate : String = df.string(from: date_time as Date) as String
        
        return newDate
    }
    
    func userAmount() {
        let activity = UIActivityIndicatorView()
        activity.style = .whiteLarge
        activity.color = UIColor.DPActivityColor
        activity.hidesWhenStopped = true
        activity.startAnimating()
        
        if coinSimbol ?? "" == "KRW" || coinSimbol ?? "" == "KDP" || coinSimbol ?? "" == "KDMP" {
            
            let url = "\(AppDelegate.url)/auth/userAmount?market=\(coinSimbol!)&simbol=\(coinSimbol!)&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)"
            
            print("url : \(url)")
            
            let amo = Alamofire.request(url)
            amo.responseJSON(completionHandler: { response in
                
                print("wallet userAbleAmount")
                var buyjson = response.result.value as? [String : Any]
                
                let tempstr = buyjson?["amount"] as? String ?? ""
                
                let doubleTempStr = self.stringtoDouble(str: tempstr) ?? 0.0
                
                let attrTempStr = NSMutableAttributedString(string: "\(String(format: "%.0f", doubleTempStr).insertComma)", attributes: [NSAttributedString.Key.foregroundColor : UIColor.DPmainTextColor])
                
                self.finacalAmount.attributedText = attrTempStr
                
                let attrPandingStr = NSMutableAttributedString(string: "\(String(format: "%.0f", doubleTempStr).insertComma) KRW", attributes: [NSAttributedString.Key.foregroundColor : UIColor.DPmainTextColor])
                
                self.krwFinacalAmount.attributedText = attrPandingStr
                
                activity.stopAnimating()
                
            })
            
            
        }else {
            
            let url = "\(AppDelegate.url)/auth/userAbleAmount?simbol=\(coinSimbol!)&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)"
            
            print("url : \(url)")
            
            let amo = Alamofire.request(url)
            amo.responseJSON(completionHandler: { response in
                
                print("wallet userAbleAmount")
                var buyjson = response.result.value as? [String : Any]
                
                let tempstr = buyjson?["dpoQty"] as? String ?? ""
                
                let doubleTempStr = self.stringtoDouble(str: tempstr) ?? 0.0
                
                let panddingStr = buyjson?["dpoPdngQty"] as? String ?? ""
                
                let doubletempKrwvalue = self.stringtoDouble(str: self.krwAmount) ?? 0.0
                
                let doubletempstr = self.stringtoDouble(str: tempstr) ?? 0.0
                
                var resultKreValue = doubletempstr * doubletempKrwvalue
                
                let intResultKrw = Int(resultKreValue.roundToPlaces(places: 0))
                
                let tempKrwStr = "\(intResultKrw)".insertComma
                
                let doubletempPanding = self.stringtoDouble(str: panddingStr) ?? 0.0
                
                var resultPandingKrw = doubletempPanding * doubletempKrwvalue
                
                let intResultPandingKrw = Int(resultPandingKrw.roundToPlaces(places: 0))
                
                let attrTempStr = NSMutableAttributedString(string: "\(String(format: "%.8f", doubleTempStr).insertComma)", attributes: [NSAttributedString.Key.foregroundColor : UIColor.DPmainTextColor])
                
                
                //19.01.11 수정 확인
                var attrTempPanndingStr = NSAttributedString()
                
                if doubletempPanding > 0.0 {
                    attrTempPanndingStr = NSAttributedString(string: "(\(String(format: "%.8f", doubletempPanding).insertComma))", attributes: [NSAttributedString.Key.foregroundColor : UIColor.DPBuyDefaultColor])
                }else if doubletempPanding == 0.0{
                    attrTempPanndingStr = NSAttributedString(string: "(\(String(format: "%.8f", doubletempPanding).insertComma))", attributes: [NSAttributedString.Key.foregroundColor : UIColor.DPmainTextColor])
                }else {
                    attrTempPanndingStr = NSAttributedString(string: "(\(String(format: "%.8f", doubletempPanding).insertComma))", attributes: [NSAttributedString.Key.foregroundColor : UIColor.DPSellDefaultColor])
                }
                
                self.doubleAllAmount = doubletempstr
                
//                if panddingStr != "" && panddingStr != "0" {
//                    print("wallet padding view : \(panddingStr)")
//                    print("wallet padding view : \(String(format: "%0.8f", doubletempPanding))")
//                    attrTempStr.append(NSAttributedString(string: "(\(String(format: "%.8f", doubletempPanding)))", attributes: [NSAttributedStringKey.foregroundColor : UIColor.DPBuyDefaultColor]))
//                }
                
                self.doubleAllPaddingAmoutn = doubletempPanding
                
                self.finacalAmount.attributedText = attrTempStr
                self.panndingFinacalAmount.attributedText = attrTempPanndingStr
                
                let tempString = "\(intResultPandingKrw)".insertComma
                
                let attrPandingStr = NSMutableAttributedString(string: "\(tempKrwStr) KRW", attributes: [NSAttributedString.Key.foregroundColor : UIColor.DPmainTextColor])
                
                var attrPandingKrwStr = NSAttributedString()
                
                if doubletempPanding > 0.0 {
                    attrPandingKrwStr = NSAttributedString(string: "(\(tempString) KRW)", attributes: [NSAttributedString.Key.foregroundColor : UIColor.DPBuyDefaultColor])
                }else if doubletempPanding == 0.0 {
                    attrPandingKrwStr = NSAttributedString(string: "(\(tempString) KRW)", attributes: [NSAttributedString.Key.foregroundColor : UIColor.DPmainTextColor])
                }else {
                    attrPandingKrwStr = NSAttributedString(string: "(\(tempString) KRW)", attributes: [NSAttributedString.Key.foregroundColor : UIColor.DPSellDefaultColor])
                }
                
//                let attrPandingKrwStr = NSAttributedString(string: "(\(tempString) KRW)", attributes: [NSAttributedStringKey.foregroundColor : UIColor.DPBuyDefaultColor])
                
//                if doubletempPanding != 0.0 {
//                    attrPandingStr.append(NSAttributedString(string: "(\(tempString))", attributes: [NSAttributedStringKey.foregroundColor : UIColor.DPBuyDefaultColor]))
//                }
                
                self.krwFinacalAmount.attributedText = attrPandingStr
                self.panndingKrwAmount.attributedText = attrPandingKrwStr
                
                let ableQty = buyjson?["wthrAbleQty"] as? String ?? ""
                
                print("able : \(ableQty)")
                
                let doubleQtyStr = self.stringtoDouble(str: ableQty) ?? 0.0
                
                self.sendAbleQtyLabel.text = "\(String(format: "%.8f", doubleQtyStr).insertComma)"
                
                let shortMnthQty = buyjson?["shortMnthQty"] as? String ?? ""
                
                let kdaCount = buyjson?["kdaDBamount"] as? String ?? ""
                
                let doubleshortMnthQtyStr = self.stringtoDouble(str: shortMnthQty) ?? 0.0
                let doubleshortKDAQtyStr = self.stringtoDouble(str: kdaCount) ?? 0.0
                
                 self.conculLabel.text = "\(String(format: "%.8f", doubleshortMnthQtyStr).insertComma)"
                
//                if self.coinSimbol ?? "" == "KDA" || self.coinSimbol ?? "" == "DPN" || self.coinSimbol ?? "" == "FNK" {
                if self.coinSimbol ?? "" == "KDA" {
                
                    self.krwFinacalAmount.text = "\(String(format: "%.8f", doubleshortKDAQtyStr).insertComma)"
                }
                
                activity.stopAnimating()
                
            })
            
        }
 
    }
    
    func pointAmout(){
        
        let url = "\(AppDelegate.url)/auth/userAmount?market=KDP&simbol=\(coinSimbol!)&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)"
        
        print("url : \(url)")
        
        let amo = Alamofire.request(url)
        amo.responseJSON(completionHandler: { response in
            
            print("wallet userAbleAmount")
            var buyjson = response.result.value as? [String : Any]
            
            let tempstr = buyjson?["amount"] as? String ?? ""
            
            let doubleTempStr = self.stringtoDouble(str: tempstr) ?? 0.0
            
            self.pointCatch = true
            
            self.pointValue = doubleTempStr
            
        })
        
    }
    
    func stringtoDouble(str : String) -> Double?{
        let temp = str.components(separatedBy: [","]).joined()
        let double = Double(temp)
        return double ?? 0.0
    }
    
    func dateSort(arr : [[String : Any]]) -> [[String : Any]] {
        
        let temparr = try arr.sorted(by:{
            guard let num1 = $0["time"] as? String else {
                return true
            }
            
            guard let num2 = $1["time"] as? String else {
                return true
            }
            
            return num1 > num2
        })
        
        return temparr
        
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        if targetContentOffset.pointee.y == 0.0 && targetContentOffset.pointee.y > scrollView.contentOffset.y {
            payList()
        }
        
    }
    
    func exchangListRT(bt: NPriceTick) {
        
        print("RT : wallet main view config")
        
        let symbol = bt.symbol.toString().components(separatedBy: [" "]).joined()
        let delta_rate = bt.delta_rate.toString()
        let last_volume = bt.last_volume.toString()
        let last_price = bt.last_price.toString()
        let intLast_volume = stringtoDouble(str: last_volume)
        let intLast_price = stringtoDouble(str: last_price) ?? 0
        
        print("RT : wallet main view simbol : \(symbol)")
        print("RT : wallet main view coin simbol : \(self.coinSimbol!)")
        print("RT : wallet main view last_price : \(last_price)")
        print("RT : wallet main view doubleAllAmount : \(doubleAllAmount)")
        print("RT : wallet main view doubleAllPaddingAmoutn : \(doubleAllPaddingAmoutn)")
        
        if coinSimbol! == symbol {
            
            var resultAmountKrw = doubleAllAmount * intLast_price
            
            let tempKrwStr = "\(Int(resultAmountKrw.roundToPlaces(places: 0)))"
            
            print("RT : wallet main view tempKrwStr : \(tempKrwStr)")
            
            let attrPandingStr = NSMutableAttributedString(string: "\(tempKrwStr.insertComma) KRW", attributes: [NSAttributedString.Key.foregroundColor : UIColor.DPmainTextColor])
            
            if self.doubleAllPaddingAmoutn != 0 {
                
                var resultPaddingKrw = doubleAllPaddingAmoutn * intLast_price
                
                let tempString = "\(Int(resultPaddingKrw.roundToPlaces(places: 0)))"
                 print("RT : wallet main view tempString : \(tempString)")
                
                attrPandingStr.append(NSAttributedString(string: "(\(tempString.insertComma))", attributes: [NSAttributedString.Key.foregroundColor : UIColor.DPBuyDefaultColor]))
                
            }
            
            DispatchQueue.main.async {
                self.krwFinacalAmount.attributedText = attrPandingStr
            }
            
        }
        
    }
    
    func exchangDetailRT(bt: NPriceTick) {
        
    }
    
    func trandeListRT(bt: NPriceInfo) {
        
    }
    
    func openListRT(bt: NOpenInfo) {
        
    }
    
    func krwChangeRT(value: Double) {
        
    }
    
    @objc func simbolAnimation(){
        print("animation tap guesture")
        
        UIView.animate(withDuration: 0.3, delay: 0, options: [UIView.AnimationOptions.curveEaseInOut, UIView.AnimationOptions.repeat], animations: {
            
            UIView.setAnimationRepeatCount(2)
            
            self.simbolImage.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.simbolImage.transform = CGAffineTransform(scaleX: 1, y: 1)
            
        }, completion: { (_) in self.userAmount() })
        
    }
    
    //KDA 환전신청
    func sendTakeAction() {
        
        self.activity.startAnimating()
        
        let url = URL(string: "\(AppDelegate.url)/auth/walletSend")
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let param = "uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&type=3&simbol=KDA&addr=\(self.ethwalletAddr)&waysType=M&privKey=\(AlertVO.shared.privKey1!)"
        
        print("wallet send param : \(param)")
        let paramData = param.data(using: .utf8)
        
        request.httpBody = paramData
        
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        request.setValue(String(describing: paramData!.count), forHTTPHeaderField: "Content-Length")
        
        //laucnhing
        let session = URLSession.shared
        
        let task = session.dataTask(with:request as URLRequest) {data, response, error in
            
            if error == nil {
                
                print("buy sueccess")
                DispatchQueue.main.async {
                    
                    //activity.stopAnimating()
                    do {
                        let object = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        
                        guard let jsonObject = object else { return }
                        
                        //self.sendAddress.text = ""
                        //                        self.sendQty.text = ""
                        //                        self.gasLimitField.text = ""
                        //                        self.sendSpeedfield.text = ""
                        //                        self.sendAddress.text = ""
                        //                        self.isAddress = false
                        //                        self.isChecked = false
                        //                        self.sendfee = ""
                        //                        self.speedBtntap = 0
                        //                        self.manuBtn.titleLabel?.text = "선택"
                        //                        self.sendFees.text = "\(String(format: "%.8f", 0.0))"
                        //                        self.totalAmount.text = "\(String(format: "%.8f", 0.0))"
                        //                        self.isAddress = false
                        //
                        print("jsonobject : \(jsonObject)")
                        
                        let sendResult = jsonObject["returnYn"] as? String ?? "N"
                        
                        print("send result : \(sendResult)")
                        
                        if sendResult == "N" {
                            
                            let msg = jsonObject["returnMsg"] as? String ?? ""
                            
                            let alert = UIAlertController(title: "pop_wallet".localized, message: msg, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: { (_) in
                            }))
                            
                            self.present(alert, animated: false, completion: nil)
                            
                        }else {
                            
                            let msg = jsonObject["returnMsg"] as? String ?? ""
                            
                            let alert = UIAlertController(title: "pop_wallet".localized, message: msg, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: { (_) in
                            }))
                            
                            self.present(alert, animated: false, completion: nil)
                            
                        }
                        
                        self.userAmount()
                        
                        self.activity.stopAnimating()
                        
                    }catch{
                        print("cCaught an error:\(error)")
                        
                        let msg = "pop_wallet_send_not_reply".localized
                        let alert = UIAlertController(title: "pop_wallet".localized, message: msg, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
                        self.present(alert, animated: false, completion: nil)
                        
                        self.activity.stopAnimating()
                        
                    }
                }
            }else{
                print("error:\(error)")
                let msg = "pop_wallet_send_not_reply".localized
                let alert = UIAlertController(title: "pop_wallet".localized, message: msg, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
                
                self.present(alert, animated: false, completion: nil)
                
                self.activity.stopAnimating()
                
            }
            
        }
        
        task.resume()
        
    }
    
    func ethwalletInfo() {
        let activity = UIActivityIndicatorView()
        activity.style = .whiteLarge
        activity.color = UIColor.DPActivityColor
        activity.hidesWhenStopped = true
        activity.startAnimating()
        
        let url = "\(AppDelegate.url)/auth/walletInfo?simbol=ETH&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)"
        
        print(url)
        let amo = Alamofire.request(url)
        amo.responseJSON(completionHandler: { response in
            
            print("buy result :\(response.result.value)")
            
            if response.result.isSuccess == true {
                
                var result = response.result.value as? [String : Any]
                
                self.ethwalletAddr = (result!["wletAddr"] as? String) ?? ""
                
            }
            activity.stopAnimating()
        })
    }

}
