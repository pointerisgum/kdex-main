//
//  sendLockController.swift
//  KDex
//
//  Created by park heewon on 2018. 6. 14..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import UIKit
import Foundation

class SendLockController: UIViewController {
    
    var delegate : WalletSendViewController?
    
    let plist = UserDefaults.standard
    
    @IBOutlet var contentLabel: UILabel!
    
    @IBOutlet var contentVi: UIView!
    
    @IBOutlet var textfieldVi: UIView!
    
    @IBOutlet var passwdTx: UITextField!
    
    @IBOutlet var repasswdTx: UITextField!
    
    @IBOutlet var conformBtn: UIButton!
    
    @IBOutlet var cancelBtn: UIButton!
    
    @IBAction func cancelAction(_ sender: Any) {
        
        self.closeAnimate()
        
    }
    
    @IBAction func conformAction(_ sender: Any) {
        
        let savePasswd = plist.string(forKey: "sendLock")
        
        let passwd = passwdTx.text ?? ""
        let repasswd = repasswdTx.text ?? ""
        
        self.view.endEditing(true)
        
        if savePasswd == nil {
            
            if passwd == "" {
                
                self.showToast(message: "send_pw_input_title".localized, textcolor: UIColor.white, frame: CGRect(x: self.view.frame.size.width/2 - 100, y: self.view.frame.size.height - 100  , width: 200, height: 35), time: 1, animation: true)
                
            }else if repasswd == "" {
                
                self.showToast(message: "send_pw_confirm_inform".localized, textcolor: UIColor.white, frame: CGRect(x: self.view.frame.size.width/2 - 100, y: self.view.frame.size.height - 100  , width: 200, height: 35), time: 1, animation: true)
                
            }else if passwd != repasswd {
                
                self.showToast(message: "send_pw_no_match_inform".localized, textcolor: UIColor.white, frame: CGRect(x: self.view.frame.size.width/2 - 100, y: self.view.frame.size.height - 100  , width: 200, height: 35), time: 1, animation: true)
                
            }else if passwd == repasswd {
                
                plist.set(passwd, forKey: "sendLock")
                plist.synchronize()
                
                self.showToast(message: "send_pw_complete_inform".localized, textcolor: UIColor.white, frame: CGRect(x: self.view.frame.size.width/2 - 100, y: self.view.frame.size.height - 100  , width: 200, height: 35), time: 1, animation: true)
                
                self.closeAnimate()
                
            }else {
                
                self.showToast(message: "send_pw_not_setup".localized, textcolor: UIColor.white, frame: CGRect(x: self.view.frame.size.width/2 - 100, y: self.view.frame.size.height - 100  , width: 200, height: 35), time: 1, animation: true)
                self.closeAnimate()
            }
            
        }else {
            if savePasswd == passwd {
                
                print("send passwd is equal")
                
                delegate?.sendTakeAction()
                
                self.closeAnimate()
                
            }else {
                
                self.showToast(message: "send_pw_no_match_inform".localized, textcolor: UIColor.white, frame: CGRect(x: self.view.frame.size.width/2 - 100, y: self.view.frame.size.height - 100  , width: 200, height: 35), time: 1, animation: true)
                
                passwdTx.text = ""
                
            }
    
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
        
        contentVi.backgroundColor = UIColor.DPPopupBgColor
        
        contentVi.layer.cornerRadius = 10
        contentVi.clipsToBounds = true
        
//        textfieldVi.add(border: .top, color: UIColor.DPLineBgColor, width: 1)
//        textfieldVi.add(border: .bottom, color: UIColor.DPLineBgColor, width: 1)
        
        conformBtn.layer.cornerRadius = 2
        conformBtn.clipsToBounds = true
        conformBtn.layer.borderColor = UIColor.DPGreenColor.cgColor
        conformBtn.setTitleColor(UIColor.DPGreenColor, for: .normal)
        conformBtn.layer.borderWidth = 1
        
        cancelBtn.layer.cornerRadius = 2
        cancelBtn.clipsToBounds = true
        cancelBtn.layer.borderWidth = 1
        cancelBtn.layer.borderColor = UIColor.DPPlusTextColor.cgColor
        cancelBtn.setTitleColor(UIColor.DPPlusTextColor, for: .normal)
        
        if plist.string(forKey: "sendLock") == nil {
            
            contentLabel.text = "send_pw_title".localized
            repasswdTx.isHidden = false
            contentVi.heightAnchor.constraint(equalToConstant: 222).isActive = true

        }else {
            
            contentLabel.text = "send_pw_input_title".localized
            repasswdTx.isHidden = true
            contentVi.heightAnchor.constraint(equalToConstant: 190).isActive = true

        }
        self.textfieldsAddtarget()
        
        self.showAnimate()
        
    }
    
    func showAnimate() {
        
        self.view.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.25) {
            
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform.init(scaleX: 1.0, y: 1.0)
            
        }
        
    }
    
    func closeAnimate() {
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0
        }) { (finished: Bool) in
            if(finished)
            {
                self.view.removeFromSuperview()
            }
        }
        
    }
    
    
    func textfieldsAddtarget(){
        
        self.passwdTx.addTarget(self, action: #selector(textfieldChageEvent), for: .editingChanged)
        self.repasswdTx.addTarget(self, action: #selector(textfieldChageEvent), for: .editingChanged)
        
    }
    
    @objc func textfieldChageEvent(textfield : UITextField) {
        
        let cnt = textfield.text?.count ?? 0
        
        if cnt > 4 {
            let str = textfield.text
            
            textfield.text = str?.substring(from: 0, to: cnt - 1)
            
        }
        
    }
    
}

