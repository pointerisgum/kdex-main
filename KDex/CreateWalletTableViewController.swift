//
//  CreateWalletTableViewController.swift
//  crevolc
//
//  Created by crovolc on 2018. 1. 4..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import UIKit

class CreateWalletTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initTitle()
        self.tableView.backgroundColor = UIColor.DPtableViewBgColor
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = ""
        
    }
    
    func initTitle(){
        
        let nTitle = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 40))
        
        nTitle.textAlignment = .center
        nTitle.font = UIFont.boldSystemFont(ofSize: 15)
        nTitle.textColor = UIColor.white
        nTitle.text = "지갑생성"
        nTitle.textAlignment = .left
        
        self.navigationItem.titleView = nTitle
        self.navigationItem.titleView?.frame.size = CGSize(width: 300, height: 40)
        
        self.navigationController?.navigationBar.barTintColor = UIColor.DPNaviBarTintColor
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "createWalletCell", for: indexPath) as! CreateWalletCell
        cell.backgroundColor = UIColor.DPtableCellBgColor
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
        
    }
    
}
