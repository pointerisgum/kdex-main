//
//  LanchController.swift
//  KDex
//
//  Created by park heewon on 2018. 10. 17..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation
import Alamofire


class LanchController: UIViewController {
    
    @IBOutlet var lchImgVi: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let value = "AppLanguage".localized
        
        if value == "en_US" {
            lchImgVi.loadGif(name: "loadding_en")
        }else {
            lchImgVi.loadGif(name: "loadding")
        }
        
        Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(initViController), userInfo: nil, repeats: false)
        
    }
    
@objc func initViController() {
    
    let plist = UserDefaults.standard
    
    let applock = plist.bool(forKey: "appLock")
    let applogin = plist.bool(forKey: "loginIs")
    
    guard let appdel = UIApplication.shared.delegate as? AppDelegate else {
        return
    }
    
    if applock && applogin {
        //자동 로그인
        AppDelegate.didlunchLock = true
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        if let detail = storyboard.instantiateViewController(withIdentifier: "lockPad") as? LockPadController {
            detail.lockis = true
            detail.backandfore = true
            
           self.present(detail, animated: true, completion: nil)
        }
       
        if AppDelegate.WifiBool {
            appdel.inglogin()
        }else {
            
            let url = "https://api.ip.pe.kr/"
            
            let amo = Alamofire.request(url)
            amo.responseString { response in
                if response.result.isSuccess == true {
                    guard let result = response.result.value else {
                        return
                    }
                    
                    AppDelegate.ipAddr = result
                    print("real address result : \(result)")
                    appdel.inglogin()
                    
                }
                
            }
            
        }
        
    }else if !applock && applogin{
        
        //자동 로그인
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        if let detail = storyboard.instantiateViewController(withIdentifier: "RevealViewCT") as? RevealViewController {
           self.present(detail, animated: true, completion: nil)
            
        }
        
        if AppDelegate.WifiBool {
            appdel.inglogin()
        }else {
            
            let url = "https://api.ip.pe.kr/"
            
            let amo = Alamofire.request(url)
            amo.responseString { response in
                if response.result.isSuccess == true {
                    guard let result = response.result.value else {
                        return
                    }
                    
                    AppDelegate.ipAddr = result
                    print("real address result : \(result)")
                    appdel.inglogin()
                    
                }
                
            }
            
        }
    }else {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        if let detail = storyboard.instantiateViewController(withIdentifier: "loginPage") as? EmailLoginViewController {
            self.present(detail, animated: true, completion: nil)
            
        }
    }
    
}
    
    
}
