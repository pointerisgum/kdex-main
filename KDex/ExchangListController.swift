//
// ExchangListController.swift
// crevolc
//
// Created by crovolc on 2017. 12. 28..
// Copyright © 2017년 hanbiteni. All rights reserved.
//
// 즐겨찾기 거래소 리스트 페이지

import Foundation
import UIKit
import Alamofire

class ExchangListController: UIViewController, UITableViewDelegate, UITableViewDataSource, RealTimeDelegate {
    
    var subscribeRS : NTcsReal = NTcsReal()
    
    var viewDelegate : ViewController?
    
    var searchValue = "" {
        didSet{
            exchangListMager.favoritesList(uid: AppDelegate.uid ,tableview: self.exahangTableView, activity: self.activity,  sessionid: AppDelegate.sessionid, search: searchValue, first: false)
            exchangListMager.tempArray.removeAll()
        }
    }
    
    var torchEvent : UITapGestureRecognizer?
    
    @IBOutlet var activity: UIActivityIndicatorView!
    
    var exchangListMager = ExchangCoinListManager()

    private var realTimeSelect = false
    private var titleSortBool = true
    private var volSortBool = true
    private var priceSortBool = true
    private var rateSortBool = true

    private let maket = ""

    @IBOutlet var exahangTableView: UITableView!

    var didSelectRow: ((String, String, String) -> ())?
    
    var searchViewContainer : (()->(UIView))?
    
    //table header start -------------------
    
    let headerView : UIView = {
        let uv = UIView()
        uv.backgroundColor = UIColor.white
        return uv
    }()
    
    let bottombarVi : UIView = {
        let vi = UIView()
        vi.backgroundColor = UIColor(rgb: 0xcacacd)
        vi.translatesAutoresizingMaskIntoConstraints = false
        return vi
    }()
    
    let favorImg : UIImageView = {
        let image = UIImage(named: "star_on")
        let img = UIImageView(image: image)
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    let AsortBtn : UIButton = {
        let bt = UIButton()
        let iamge = UIImage(named: "icn_list_sort_deactive-1")
        bt.backgroundColor =  UIColor.clear
        bt.translatesAutoresizingMaskIntoConstraints = false
        bt.setAttributedTitle(NSAttributedString(string: "trade_market_coin_name".localized, attributes:[NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.DPmainTextColor]), for: .normal)
        if AppDelegate.appLang == "en_US"{
            bt.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -55)
        }else {
            bt.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -75)
        }
        bt.titleEdgeInsets = UIEdgeInsets(top: 0, left: -40, bottom: 0, right: 0)
        bt.setImage(iamge, for: .normal)
        bt.addTarget(self, action: #selector(titleSort), for: .touchUpInside)
        return bt
    }()
    
    let BsortBtn : UIButton = {
        let bt = UIButton()
        let iamge = UIImage(named: "icn_list_sort_deactive-1")
        bt.translatesAutoresizingMaskIntoConstraints = false
        bt.setAttributedTitle(NSAttributedString(string: "trade_market_rate".localized, attributes:[NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.DPmainTextColor]), for: .normal)
        if AppDelegate.appLang == "en_US"{
            bt.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -65)
        }else {
            bt.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -70)
        }
        bt.titleEdgeInsets = UIEdgeInsets(top: 0, left: -45, bottom: 0, right: 0)
        bt.setImage(iamge, for: .normal)
        bt.addTarget(self, action: #selector(volSort), for: .touchUpInside)
        bt.backgroundColor =  UIColor.clear
        
        return bt
    }()
    
    let CsortBtn : UIButton = {
        let bt = UIButton()
        let iamge = UIImage(named: "icn_list_sort_deactive-1")
        bt.translatesAutoresizingMaskIntoConstraints = false
        bt.setAttributedTitle(NSAttributedString(string: "trade_market_last_price".localized, attributes:[NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.DPmainTextColor]), for: .normal)
        bt.setImage(iamge, for: .normal)
        if AppDelegate.appLang == "en_US"{
            bt.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -125)
        }else {
            bt.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -70)
        }
        bt.titleEdgeInsets = UIEdgeInsets(top: 0, left: -45, bottom: 0, right: 0)
        bt.addTarget(self, action: #selector(priceSort), for: .touchUpInside)
        bt.backgroundColor = UIColor.clear
        return bt
    }()
    
    let DsortBtn : UIButton = {
        let bt = UIButton()
        let iamge = UIImage(named: "icn_list_sort_deactive-1")
        bt.translatesAutoresizingMaskIntoConstraints = false
        bt.setAttributedTitle(NSAttributedString(string: "trade_market_yesterday".localized, attributes:[NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.DPmainTextColor]), for: .normal)
        bt.setImage(iamge, for: .normal)
        if AppDelegate.appLang == "en_US"{
            bt.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -90)
        }else {
            bt.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -110)
        }
        bt.titleEdgeInsets = UIEdgeInsets(top: 0, left: -30, bottom: 0, right: 0)
        bt.addTarget(self, action: #selector(rateSort), for: .touchUpInside)
        bt.backgroundColor =  UIColor.clear
        return bt
    }()
    
    //table header end -------------------
    
    fileprivate func headerSetup() {
        headerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 40)
        
        //        headerView.addSubview(favorImg)
        headerView.addSubview(bottombarVi)
        headerView.addSubview(AsortBtn)
        headerView.addSubview(BsortBtn)
        headerView.addSubview(CsortBtn)
        headerView.addSubview(DsortBtn)
        
        bottombarVi.bottomAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
        bottombarVi.leadingAnchor.constraint(equalTo: headerView.leadingAnchor).isActive = true
        bottombarVi.trailingAnchor.constraint(equalTo: headerView.trailingAnchor).isActive = true
        bottombarVi.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        //        favorImg.leftAnchor.constraint(equalTo: headerView.leftAnchor, constant: 10).isActive = true
        //        favorImg.widthAnchor.constraint(equalToConstant: 20).isActive = true
        //        favorImg.heightAnchor.constraint(equalToConstant: 20).isActive = true
        //        favorImg.centerYAnchor.constraint(lessThanOrEqualTo: headerView.centerYAnchor).isActive = true
        //        AsortBtn.leftAnchor.constraint(equalTo: headerView.leftAnchor, constant: 33).isActive = true
        
        AsortBtn.widthAnchor.constraint(equalToConstant: 60).isActive = true
        AsortBtn.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 5).isActive = true
        AsortBtn.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
        AsortBtn.centerXAnchor.constraint(equalTo: headerView.centerXAnchor, constant: -(self.view.frame.width/2 - 20)).isActive = true
        
        //        BsortBtn.leftAnchor.constraint(equalTo: AsortBtn.rightAnchor, constant: 3).isActive = true
        BsortBtn.widthAnchor.constraint(equalToConstant: 40).isActive = true
        //        BsortBtn.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 5).isActive = true
        BsortBtn.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
        BsortBtn.centerXAnchor.constraint(equalTo: headerView.centerXAnchor, constant: (self.view.frame.width/2 - 60)).isActive = true
        
        CsortBtn.widthAnchor.constraint(equalToConstant: 60).isActive = true
        CsortBtn.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
        CsortBtn.centerXAnchor.constraint(equalTo: headerView.centerXAnchor, constant: -25).isActive = true
        
        DsortBtn.widthAnchor.constraint(equalToConstant: 70).isActive = true
        DsortBtn.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
        DsortBtn.centerXAnchor.constraint(equalTo: headerView.centerXAnchor, constant: (self.view.frame.width/2 - 140)).isActive = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        headerSetup()
        
        //self.exahangTableView.tableHeaderView = headerView
        
        self.exahangTableView.backgroundColor = UIColor(rgb: 0xf3f4f5)
        self.exahangTableView.layoutMargins = UIEdgeInsets.zero
        self.exahangTableView.separatorInset = UIEdgeInsets.zero
        self.exahangTableView.separatorStyle = .none
        
        let nib = UINib(nibName: "ExchangListNib", bundle: nil)
        exahangTableView.register(nib, forCellReuseIdentifier: "newExchangCell")
        
    }
    
    override func viewDidAppear(_ animated: Bool) {

        let plist = UserDefaults.standard
        plist.set("", forKey: "marketSimbol")
        plist.synchronize()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        exchangListMager.favoritesList(uid: AppDelegate.uid, tableview: exahangTableView, activity: activity, sessionid: AppDelegate.sessionid, search: AppDelegate.localSearch, first: true)
        
        
        if torchEvent != nil {
            print("touch event")
            exahangTableView.removeGestureRecognizer(torchEvent!)
        }
        
        setupKeyboardObservers()
        
        TcpSocket.sharedInstance.receiverThread?.delegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("favority coutn : \(exchangListMager.favoriCoinListJson.count)")
        
        return exchangListMager.favoriCoinListJson.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let dataTemp = exchangListMager.favoriCoinListJson[indexPath.row] as? NSDictionary
        
        //let cell = tableView.dequeueReusableCell(withIdentifier: "exchangeCell") as? ExchangListCell
        let cell = tableView.dequeueReusableCell(withIdentifier: "newExchangCell", for: indexPath) as? ExchangListCell
        cell?.selectionStyle = .none
        cell?.layoutMargins = UIEdgeInsets.zero
        
        //"coinsim":"BTC", "coinName":"비트코인", "coinVol":"80000", "coinValue":"239402392", "coinPer":"12
        
        let coinsim = dataTemp!["simbol"] as? String ?? ""
        let coinname = dataTemp!["coinName"] as? String ?? ""
        let coinper = dataTemp!["updnRate"] as? String ?? ""
        let coinvol = dataTemp!["totalVol"] as? String ?? ""
        let coinvalue = dataTemp!["lastPrice"] as? String ?? ""
        let market = dataTemp!["market"] as? String ?? ""
        let updnSign = dataTemp!["updnSign"] as? String ?? "0"
        var updnPrice = dataTemp!["updnPrice"] as? String ?? "0"
        let doublecoinvalue = stringtoDouble(str: coinvalue)
        var convalueText = ""
//        var lastcoinsim = coinsim.replacingOccurrences(of: "_", with: "/")
        
        let attrConValueText = NSMutableAttributedString()
        
        if updnPrice.contains("-") {
            updnPrice.replace(originalString: "-", withString: "")
        }
        
        if market == "BTC" {
            
            var krwvalue = doublecoinvalue! * AppDelegate.BTC
            
            let intKrw = Int(krwvalue.roundToPlaces(places: 0))
            
            let strKrw = String(intKrw)
            
            let str = String(format: "%0.8f", doublecoinvalue!)
//            let str = coinvalue.insertComma
            
            convalueText = "\(str) \n \(strKrw.insertComma)KRW"
            
            if updnSign == "-1" {
                convalueText = "\(str) \n \(strKrw.insertComma)KRW"
                attrConValueText.append(NSAttributedString(string: "\(str) \n", attributes: [NSAttributedString.Key.foregroundColor: UIColor.DPMinTextColor]))
                cell?.coinPer.textColor = UIColor.DPMinTextColor
                cell?.perVsValue.textColor = UIColor.DPMinTextColor

            }else if updnSign == "1" {
                convalueText = "\(str) \n \(strKrw.insertComma)KRW"
                attrConValueText.append(NSAttributedString(string: "\(str) \n", attributes: [NSAttributedString.Key.foregroundColor: UIColor.DPPlusTextColor]))
                cell?.coinPer.textColor = UIColor.DPPlusTextColor
                cell?.perVsValue.textColor = UIColor.DPPlusTextColor

            }else {
                convalueText = "\(str) \n \(strKrw.insertComma)KRW"
                attrConValueText.append(NSAttributedString(string: "\(str) \n", attributes: [NSAttributedString.Key.foregroundColor: UIColor.DPmainTextColor]))
                cell?.coinPer.textColor = UIColor.DPmainTextColor
                cell?.perVsValue.textColor = UIColor.DPmainTextColor

            }
            
            attrConValueText.append(NSAttributedString(string: "\(strKrw.insertComma) KRW", attributes: [NSAttributedString.Key.foregroundColor: UIColor.DPsubTextColor, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12)]))
            
        }else if market == "ETH" {
            
            var krwvalue = doublecoinvalue! * AppDelegate.ETH
            
            let intKrw = Int(krwvalue.roundToPlaces(places: 0))
            
            let strKrw = String(intKrw)
            
//            let str = coinvalue.insertComma
            
            let str = String(format: "%0.8f", doublecoinvalue!)
            
            convalueText = "\(str) \n \(strKrw.insertComma)KRW"
            
            if updnSign == "-1" {
                convalueText = "\(str) \n \(strKrw.insertComma)KRW"
                attrConValueText.append(NSAttributedString(string: "\(str) \n", attributes: [NSAttributedString.Key.foregroundColor: UIColor.DPMinTextColor]))
                cell?.coinPer.textColor = UIColor.DPMinTextColor
                cell?.perVsValue.textColor = UIColor.DPMinTextColor
            }else if updnSign == "1" {
                convalueText = "\(str) \n \(strKrw.insertComma)KRW"
                attrConValueText.append(NSAttributedString(string: "\(str) \n", attributes: [NSAttributedString.Key.foregroundColor: UIColor.DPPlusTextColor]))
                cell?.coinPer.textColor = UIColor.DPPlusTextColor
                cell?.perVsValue.textColor = UIColor.DPPlusTextColor
            }else {
                convalueText = "\(str) \n \(strKrw.insertComma)KRW"
                attrConValueText.append(NSAttributedString(string: "\(str) \n", attributes: [NSAttributedString.Key.foregroundColor: UIColor.DPmainTextColor]))
                cell?.coinPer.textColor = UIColor.DPmainTextColor
                cell?.perVsValue.textColor = UIColor.DPmainTextColor
            }
            
            attrConValueText.append(NSAttributedString(string: "\(strKrw.insertComma) KRW", attributes: [NSAttributedString.Key.foregroundColor: UIColor.DPsubTextColor, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12)]))
            
        }else {
            
            let str = coinvalue.insertComma
            
            convalueText = "\(str)"
            
            if updnSign == "-1" {
                attrConValueText.append(NSAttributedString(string: "\(str)", attributes: [NSAttributedString.Key.foregroundColor: UIColor.DPMinTextColor]))
                cell?.coinPer.textColor = UIColor.DPMinTextColor
                cell?.perVsValue.textColor = UIColor.DPMinTextColor
            }else if updnSign == "1"{
                attrConValueText.append(NSAttributedString(string: "\(str)", attributes: [NSAttributedString.Key.foregroundColor: UIColor.DPPlusTextColor]))
                cell?.coinPer.textColor = UIColor.DPPlusTextColor
                cell?.perVsValue.textColor = UIColor.DPPlusTextColor
            }else {
                attrConValueText.append(NSAttributedString(string: "\(str)", attributes: [NSAttributedString.Key.foregroundColor: UIColor.DPmainTextColor]))
                cell?.coinPer.textColor = UIColor.DPmainTextColor
                cell?.perVsValue.textColor = UIColor.DPmainTextColor
            }
            
//            lastcoinsim = "\(lastcoinsim)/KRW"
        }
        
        var simboleStr = ""
        var marketStr = ""
        
        if coinsim.contains("_") {
            
            let indexStr = coinsim.index(of: "_")
            
            simboleStr = String(coinsim.prefix(upTo: indexStr!))
            marketStr = String(coinsim.suffix(from: indexStr!))
            marketStr.replace(originalString: "_", withString: "/")
            
        }else {
            simboleStr = coinsim
            marketStr = "/KRW"
        }
        
        //        let lastcoinsim = coinsim.replacingOccurrences(of: "_", with: "/")
        
        print("simbostr : \(simboleStr), makerStr : \(marketStr)")
        
        
        let attr = NSMutableAttributedString(string: simboleStr, attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 13), NSAttributedString.Key.foregroundColor : UIColor(rgb: 0x666666)])
        
        attr.append(NSAttributedString(string: marketStr, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12), NSAttributedString.Key.foregroundColor : UIColor(rgb: 0x999999)]))
        
        
        let widht = convalueText.calcWidth(forHeight: 20, font: UIFont.systemFont(ofSize: 14))
        
        cell?.coinValue.frame.size = CGSize(width: widht, height: (cell?.frame.height)! - 5)
        
        cell?.coinSymbol = coinsim
        cell?.simbol.attributedText = attr
        cell?.coinName.text = coinname
        cell?.coinName.font = UIFont.boldSystemFont(ofSize: 14)
        cell?.coinPer.adjustsFontSizeToFitWidth = true
        cell?.coinPer.text = self.stringChage(value1: nil, value2: "%", origin: "\(String(format: "%0.2f", stringtoDouble(str: coinper.insertComma) ?? 0.0))")
        cell?.coinVol.text = self.stringChage(value1: "", value2: "", origin: (coinvol.insertComma))
        cell?.coinValue.numberOfLines = 2
//        cell?.coinValue.text = convalueText
        cell?.coinValue.attributedText = attrConValueText
        cell?.coinValue.adjustsFontSizeToFitWidth = true
        cell?.coinValue.sizeToFit()
        
        let dbUpdnPrice = stringtoDouble(str: updnPrice) ?? 0.0
        
        cell?.perVsValue.text = String(format: "%0.8f", dbUpdnPrice)
        
//        if (indexPath.row % 2) == 0  {
//            
//            cell?.backgroundColor = UIColor.DPViewBackgroundColor
//            
//        }else {
//            cell?.backgroundColor = UIColor.DPViewGrayBackgroundColor
//        }
        
//        if (coinper?.contains("-"))! {
//            cell?.coinValue.textColor = UIColor.DPMinTextColor
//            cell?.coinPer.textColor = UIColor.DPMinTextColor
//            //cell?.coinPerBackground.backgroundColor = UIColor.DPSellDefaultColor
//        }else{
//            cell?.coinValue.textColor = UIColor.DPPlusTextColor
//            cell?.coinPer.textColor = UIColor.DPPlusTextColor
//            //cell?.coinPerBackground.backgroundColor = UIColor.DPBuyDefaultColor
//        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dataTemp = exchangListMager.favoriCoinListJson[indexPath.row] as? NSDictionary
        let coinSimbol = dataTemp?["simbol"] as? String ?? ""
        let coinName = dataTemp?["coinName"] as? String ?? ""
        let market = dataTemp?["market"] as? String
        let plist = UserDefaults.standard
        plist.set("\(market!)", forKey: "marketSimbol")
        plist.synchronize()
        
        didSelectRow?(coinSimbol, coinName, maket)
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return self.headerView
    }
    
    func stringChage(value1: String?, value2: String, origin: String) -> String {

        var str : String = origin

        if value1 == nil {
            str = "\(str) \(value2)"

        }else {
            str = "\(value1!) \(str) \(value2)"
        }
        return str
    }
    
    func stringtoDouble(str : String) -> Double?{
        let temp = str.components(separatedBy: [","]).joined()
        let double = Double(temp)
        return double ?? 0.0
    }
    
    //real time delegate
    func exchangListRT(bt: NPriceTick) {
        print("RT : exchangListKRWRT")
        
        var index = 0
        var num = 0
        var dic = [String : Any]()
        
        let symbol = bt.symbol.toString().components(separatedBy: [" "]).joined()
        let delta_rate = bt.delta_rate.toString()
        let last_volume = bt.last_volume.toString()
        let last_price = bt.last_price.toString()
        let intLast_volume = stringtoDouble(str: last_volume)
        
        
        if let intLast_price = Int(last_price){
            dic.updateValue(String(intLast_price), forKey: "lastPrice")
        }else {
            let intLast_price = stringtoDouble(str: last_price)
            dic.updateValue(String(intLast_price!), forKey: "lastPrice")
        }
        
        dic.updateValue(symbol, forKey: "simbol")
        dic.updateValue(delta_rate, forKey: "updnRate" )
        dic.updateValue(String(intLast_volume!), forKey: "totalVol")
        
        print("RT symbole : \(symbol)")
        
        let intlastVol = Int(last_volume) ?? 0
        
        for row in exchangListMager.favoriCoinListJson {
            
            let sym = row as [String : Any]
            
            let tempSymbole = sym["simbol"] as? String ?? ""
            
            num = index
            
            let coinname = sym["coinName"] as? String ?? ""
            
            print("RT coinname : \(coinname)")
            
            dic.updateValue(coinname, forKey: "coinName" )
            
            if symbol == tempSymbole && intLast_volume != 0{
                
                exchangListMager.favoriCoinListJson[index] = dic
                self.realTimeSelect = true
                
            }else if symbol == tempSymbole && intLast_volume == 0 {
                
                if !symbol.contains("_") {
                    
                    exchangListMager.favoriCoinListJson[index] = dic
                    self.realTimeSelect = true
                    
                }
                
            }
            
            
            index = index + 1
        }
        
        if symbol == "ETH" || symbol == "BTC" {
            DispatchQueue.main.async {
                self.exahangTableView.reloadData()
            }
        }else {
            
            let indexpath = IndexPath(row: num, section: 0)
            if self.realTimeSelect {
                DispatchQueue.main.async {
                    self.exahangTableView.reloadRows(at: [indexpath], with: .none)
                    self.realTimeSelect = false
                }
            }

        }
        
    }
    
    func exchangDetailRT(bt: NPriceTick) {
        
    }
    
    func trandeListRT(bt: NPriceInfo) {
        
    }
    
    func openListRT(bt: NOpenInfo) {
        
    }
    
    func krwChangeRT(value: Double) {
        DispatchQueue.main.async {
            self.exahangTableView.reloadData()
        }
    }
    
    @objc func titleSort(){
        
        if titleSortBool {
            titleSortBool = false
            
            let arr = try exchangListMager.favoriCoinListJson.sorted(by:{
                guard let num1 = $0["coinName"] as? String else {
                    return true
                }
                
                guard let num2 = $1["coinName"] as? String else {
                    return true
                }
                
                return num1 > num2
                
            })
            
            exchangListMager.favoriCoinListJson = arr
            exahangTableView.reloadData();
        }else {
            titleSortBool = true
            
            let arr = try exchangListMager.favoriCoinListJson.sorted(by:{
                guard let num1 = $0["coinName"] as? String else {
                    return true
                }
                
                guard let num2 = $1["coinName"] as? String else {
                    return true
                }
                
                return num1 < num2
                
            })
            
            exchangListMager.favoriCoinListJson = arr
            exahangTableView.reloadData();
        }
        
    }
    
    @objc func volSort(){
        
        if volSortBool {
            volSortBool = false
            
            let arr = try exchangListMager.favoriCoinListJson.sorted(by:{
                guard let num1 = $0["totalVol"] as? String else {
                    return true
                }
                
                guard let num2 = $1["totalVol"] as? String else {
                    return true
                }
                
                return num1 > num2
                
            })
            
            exchangListMager.favoriCoinListJson = arr
            exahangTableView.reloadData();
        }else {
            volSortBool = true
            
            let arr = try exchangListMager.favoriCoinListJson.sorted(by:{
                guard let num1 = $0["totalVol"] as? String else {
                    return true
                }
                
                guard let num2 = $1["totalVol"] as? String else {
                    return true
                }
                
                return num1 < num2
                
            })
            
            exchangListMager.favoriCoinListJson = arr
            exahangTableView.reloadData();
        }
        
    }
    
    @objc func priceSort(){
        
        if priceSortBool {
            priceSortBool = false
            
            let arr = try exchangListMager.favoriCoinListJson.sorted(by:{
                
                var krwNum1 = 0.0
                var krwNum2 = 0.0
                
                let market1 = $0["market"] as? String ?? ""
                
                let market2 = $1["market"] as? String ?? ""
                
                guard let num1 = self.stringtoDouble(str: ($0["lastPrice"] as? String)!) else {
                    return true
                }
                
                guard let num2 = self.stringtoDouble(str: ($1["lastPrice"] as? String)!) else {
                    return true
                }
                
                if market1 == "BTC" {
                    krwNum1 = num1 * AppDelegate.BTC
                }else if market1 == "ETC"{
                    krwNum1 = num1 * AppDelegate.ETH
                }else {
                    krwNum1 = num1
                }
                
                if market2 == "BTC" {
                    krwNum2 = num2 * AppDelegate.BTC
                }else if market2 == "ETC"{
                    krwNum2 = num2 * AppDelegate.ETH
                }else {
                    krwNum2 = num2
                }
 
                return krwNum1 > krwNum2
                
            })
            
            exchangListMager.favoriCoinListJson = arr
            exahangTableView.reloadData();
        }else {
            priceSortBool = true
            
            let arr = try exchangListMager.favoriCoinListJson.sorted(by:{
                
                var krwNum1 = 0.0
                var krwNum2 = 0.0
                
                let market1 = $0["market"] as? String ?? ""
                
                let market2 = $1["market"] as? String ?? ""
                
                guard let num1 = self.stringtoDouble(str: ($0["lastPrice"] as? String)!) else {
                    return true
                }
                
                guard let num2 = self.stringtoDouble(str: ($1["lastPrice"] as? String)!) else {
                    return true
                }
                
                if market1 == "BTC" {
                    krwNum1 = num1 * AppDelegate.BTC
                }else if market1 == "ETC"{
                    krwNum1 = num1 * AppDelegate.ETH
                }else {
                    krwNum1 = num1
                }
                
                if market2 == "BTC" {
                    krwNum2 = num2 * AppDelegate.BTC
                }else if market2 == "ETC"{
                    krwNum2 = num2 * AppDelegate.ETH
                }else {
                    krwNum2 = num2
                }
                
                return krwNum1 < krwNum2
                
            })
            
            exchangListMager.favoriCoinListJson = arr
            exahangTableView.reloadData();
        }
        
    }
    
    @objc func rateSort(){
        
        if rateSortBool {
            rateSortBool = false
            
            let arr = try exchangListMager.favoriCoinListJson.sorted(by:{
                guard let num1 = $0["updnRate"] as? String else {
                    return true
                }
                
                guard let num2 = $1["updnRate"] as? String else {
                    return true
                }
                
                return num1 > num2
                
            })
            
            exchangListMager.favoriCoinListJson = arr
            exahangTableView.reloadData();
        }else {
            rateSortBool = true
            
            let arr = try exchangListMager.favoriCoinListJson.sorted(by:{
                guard let num1 = $0["updnRate"] as? String else {
                    return true
                }
                
                guard let num2 = $1["updnRate"] as? String else {
                    return true
                }
                
                return num1 < num2
                
            })
            
            exchangListMager.favoriCoinListJson = arr
            exahangTableView.reloadData();
        }
        
    }
    
    @objc func hideKeyboard() {
        print("touchevent ")
        viewDelegate?.hideKeyboard()
    }
    
    func setupKeyboardObservers(){
        print("setupKeyboardObservers")
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    @objc func handleKeyboardWillShow(_ notification: Notification) {
        print("show keyborad")
        
        torchEvent = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        exahangTableView.addGestureRecognizer(torchEvent!)
    }
    
    @objc func handleKeyboardWillHide(_ notification: Notification)  {
        print("hide keyborad")
        if torchEvent != nil {
            print("touch event")
            exahangTableView.removeGestureRecognizer(torchEvent!)
        }
    }
    
}

