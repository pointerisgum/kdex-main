//
//  ShoppingViewController.swift
//  KDex
//
//  Created by park heewon on 17/04/2019.
//  Copyright © 2019 hanbiteni. All rights reserved.
//

import UIKit
import WebKit

class ShoppingViewController: UIViewController, WKUIDelegate, WKNavigationDelegate, WKScriptMessageHandler {
    
    let url = "http://www.kdamall.co.kr/kwallet/login.php?loginId=\(AppDelegate.uid)"
    var webView : WKWebView!
    var activityIndicator : UIActivityIndicatorView = UIActivityIndicatorView()
    
    var rightbtn1 = UIButton(type: .system)
    var rightbtn2 = UIButton(type: .system)
    var rightbtn3 = UIButton(type: .system)
    var rightbtn4 = UIButton(type: .system)
    
    override func loadView() {
        super.loadView()
        
        webView = WKWebView(frame: self.view.frame)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        
        self.view = self.webView!
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.DPViewBackgroundColor
        
        let urlvalue = URL(string: self.url)
        
        let request = URLRequest(url: urlvalue!)
        
        webView.load(request)
        
        initTitle()
        
    }
    
    
    func initTitle(){
        
        let vi = UIView()
        
//        let nTitle = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 37))
//
//        //        nTitle.textAlignment = .center
//        nTitle.font = UIFont.boldSystemFont(ofSize: 15)
//        nTitle.textColor = UIColor.DPTitleTextColor
//        nTitle.text = "쇼핑몰"
//        //        nTitle.sizeToFit()
//        nTitle.textAlignment = .left
//
//        vi.addSubview(nTitle)
        
        let icon1 = UIImage(named: "prev")
        

//        let rightbtn1 = UIButton(type: .system)
        rightbtn1.frame = CGRect(x: 35, y: 5, width: 25, height: 25)
        rightbtn1.setImage(icon1, for: .normal)
        rightbtn1.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        
        vi.addSubview(rightbtn1)
        
        let icon2 = UIImage(named: "next")
        
        
        rightbtn2.frame = CGRect(x: 80, y: 5, width: 25, height: 25)
        rightbtn2.setImage(icon2, for: .normal)
        rightbtn2.addTarget(self, action: #selector(forwardButtonAction), for: .touchUpInside)
        
        vi.addSubview(rightbtn2)
        
        let icon3 = UIImage(named: "refresh")
        
        
        rightbtn3.frame = CGRect(x: 120, y: 5, width: 25, height: 25)
        rightbtn3.setImage(icon3, for: .normal)
        rightbtn3.addTarget(self, action: #selector(refreshButtonAction), for: .touchUpInside)
        
        vi.addSubview(rightbtn3)
        
        let icon4 = UIImage(named: "homt")
        
        
        rightbtn4.frame = CGRect(x: 160, y: 5, width: 25, height: 25)
        rightbtn4.setImage(icon4, for: .normal)
        rightbtn4.addTarget(self, action: #selector(goHomeButtonAction), for: .touchUpInside)
        
        vi.addSubview(rightbtn4)

        
        self.navigationItem.titleView = vi
        
        self.navigationItem.titleView?.frame.size = CGSize(width: 400, height: 37)
        
        self.navigationController?.navigationBar.barTintColor =  UIColor.DPNaviBarTintColor
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationController?.addShadowToBar(vi: self)
        
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print(error)
    }
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        //script에서 ios zhem
    }
    
    
    
    @available(iOS 8.0, *)
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping () -> Void) {
        
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .default, handler: { (action) in
            completionHandler()
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @available(iOS 8.0, *)
    func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping (Bool) -> Void) {
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .default, handler: { (action) in
            completionHandler(true)
        }))
        alertController.addAction(UIAlertAction(title: "common_no_btn".localized, style: .default, handler: { (action) in
            completionHandler(false)
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @available(iOS 8.0, *)
    func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping (String?) -> Void) {
        let alertController = UIAlertController(title: "", message: prompt, preferredStyle: .alert)
        alertController.addTextField { (textField) in
            textField.text = defaultText
        }
        alertController.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .default, handler: { (action) in
            if let text = alertController.textFields?.first?.text {
                completionHandler(text)
            } else {
                completionHandler(defaultText)
            }
        }))
        
        alertController.addAction(UIAlertAction(title: "common_no_btn".localized, style: .default, handler: { (action) in
            completionHandler(nil)
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @available(iOS 8.0, *)
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
        activityIndicator.frame = CGRect(x: view.frame.midX-50, y: view.frame.midY-50, width: 100, height: 100)
        activityIndicator.style = .whiteLarge
        activityIndicator.color = UIColor.DPCompanyColor
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        
    }
    
    @available(iOS 8.0, *)
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        rightbtn1.isEnabled = webView.canGoBack
        rightbtn2.isEnabled = webView.canGoForward
        
        self.activityIndicator.removeFromSuperview()
    }
    
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
    }
    
    
    public func webViewWebContentProcessDidTerminate(_ webView: WKWebView) {
        // 중복적으로 리로드가 일어나지 않도록 처리 필요.
        print("webViewWebContentProcessDidTerminate")
        self.webView.reload()
    }
    
    @objc func backButtonAction() {
        
        webView.goBack()
        
//        webView.reload()
        
    }
    
    
    
    @objc func forwardButtonAction() {
        
        webView.goForward()
        
//        webView.reload()
        
    }
    
    @objc func refreshButtonAction() {
        
        webView.reload()
        
    }
    
    @objc func goHomeButtonAction() {
        
        let urlvalue = URL(string: self.url)
        
        let request = URLRequest(url: urlvalue!)
        
        webView.load(request)
        
    }
    
}
