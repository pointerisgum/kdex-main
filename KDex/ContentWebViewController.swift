//
//  ContentWebViewController.swift
//  KDex
//
//  Created by park heewon on 2018. 4. 18..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//
// 사용하지 않는 페이지

import Foundation
import WebKit

class ContentWebViewController : UIViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBOutlet var backbtn: UIButton!
    
    @IBAction func backbtnAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
