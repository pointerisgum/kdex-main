//
//  AlertViewController.swift
//  crevolc
//
//  Created by park heewon on 2018. 2. 23..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Alamofire
import UIKit
import Foundation

class AlertViewController : UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    var coinSimbol : String = "" //코인 아이디
    
    var selectedFavorites = true
    
    var tickSice : String?
    
    var dataJsondic : [String : Any]? // 데이타시세 값
    
    var jsonArray : [[String : Any]]? //매칭 데이터 json array 값
    
    var jsondic : NSMutableDictionary? //매칭 데이터 json 값 파싱
    
    @IBOutlet var openPrice: UILabel! // 현재가
    
    @IBOutlet var updnRate: UILabel! //변동율
    
    @IBOutlet var highestPrice: UILabel! //최고가
    
    @IBOutlet var lowestPrice: UILabel! //최저가
    
    @IBOutlet weak var selectPrice: UITextField!
    
    @IBOutlet weak var currentPrice: UITextField!
    
    @IBOutlet weak var alertListTb: UITableView!
    
    let headerVi : UIView = {
        
        let vi = UIView()
        
        vi.backgroundColor = UIColor.DPtableHeaderBgColor
        return vi
        
    }()
    
    let footerVi : UIView = {
        
        let vi = UIView()
        vi.backgroundColor = UIColor.DPtableFooterBgColor
        return vi

    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        selectPrice.addTarget(self, action: #selector(textfieldChageEvent), for: .editingChanged)
        currentPrice.addTarget(self, action: #selector(textfieldChageEvent), for: .editingChanged)
        
        selectPrice.delegate = self
        currentPrice.delegate = self
        
        let lb = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 37))
        lb.text = "alarm_setup".localized
        lb.textColor = UIColor.white
        lb.textAlignment = .center
        lb.font = UIFont.systemFont(ofSize: 14)
        lb.sizeToFit()
        
        self.navigationItem.titleView = lb
        
        //back 버튼 글 없애기
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.addShadowToBar(vi: self)
        
        alertListTb.backgroundColor = UIColor.DPtableViewBgColor
        
        alertListTb.delegate = self
        alertListTb.dataSource = self
        
        alertlistAction()
        
        setupheaderVeiw()
        
        selectPrice.attributedPlaceholder = NSAttributedString(string: "지정가격", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14)])
        
        currentPrice.attributedPlaceholder = NSAttributedString(string: "현재가 대비", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14)])
        
        quoteData()
        
    }
    
    @IBAction func addPrice(_ sender: Any) {
        
        if self.selectPrice.text == "" {
            self.selectPrice.text = "0"
        }
        
        let priceSize = Int(self.selectPrice.text!)
        
        
        if let size = self.tickSice {
            
            self.selectPrice.text = String(priceSize! + Int(size)!)
        }
        
    }
    
    @IBAction func minPrice(_ sender: Any) {
        
        if self.selectPrice.text == "" {
            self.selectPrice.text = "0"
        }
        
        let priceSize = Int(self.selectPrice.text!)
        
        
        if let size = self.tickSice {
            
            self.selectPrice.text = String(priceSize! - Int(size)!)
        }
        
    }
    
    @IBAction func addCurrent(_ sender: Any) {
        
    }
    
    @IBAction func minCurrent(_ sender: Any) {
        
    }
    
    @IBAction func register(_ sender: Any) {
        let coinSim = self.coinSimbol
        let uid = AppDelegate.uid
        let sessionid = AppDelegate.sessionid
        
        var urlComponents = URLComponents(string:"\(AppDelegate.url)/ajax/alramProc?simbol=\(coinSim)&uid=\(uid)&sessionId=\(sessionid)&price=\(self.selectPrice.text!)&type=N")!
        let amo = Alamofire.request(urlComponents)
        amo.responseString { (respons) in
            
            if respons.result.isSuccess {
                
                let alert = UIAlertController(title: "알림추가", message: "지정가 알림을 추가하였습니다.", preferredStyle: .alert)
                
                let conAtion = UIAlertAction(title: "확인", style: .cancel, handler: nil)
                
                alert.addAction(conAtion)
                
                self.present(alert, animated: true, completion: nil)
                
                print("\(respons.result.value)")
            }
        }
        
    }
    
    func setupheaderVeiw(){
        headerVi.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 40)
        
        let headerLb = UILabel()
        headerLb.frame = CGRect(x: 20, y: 0, width: 200, height: 40)
        headerLb.text = "지정가 알림 목록"
        headerLb.textColor = UIColor.DPmainTextColor
        
        headerVi.addSubview(headerLb)
        
    }
    
    //시세 데이타
    func quoteData(){
        
        let coinSim = self.coinSimbol
        let uid = AppDelegate.uid
        let sessionid = AppDelegate.sessionid
        
        //매칭리스트 값 호출
        var urlComponents = URLComponents(string:"\(AppDelegate.url)/app/coinDetail?simbol=\(coinSim)&uid=\(uid)&sessionId=\(sessionid)")!
        let amo = Alamofire.request(urlComponents)
        amo.responseJSON(completionHandler:
            { response in
                
                //json 파싱
                self.dataJsondic = response.result.value as! [String: Any]
                //print(self.dataJsondic)
                
                let openprice = self.dataJsondic?["lastPrice"] as? String
                let highestprice = self.dataJsondic?["highPrice"] as? String
                let lowestprice = self.dataJsondic?["lowPrice"] as? String
                let updnrate = self.dataJsondic?["updnRate"] as? String
                
                self.openPrice.text = openprice?.insertComma
                self.highestPrice.text = highestprice?.insertComma
                self.lowestPrice.text = lowestprice?.insertComma
                self.updnRate.text = self.stringChage(value1: nil, value2: "%", origin: updnrate!)
                
        })
        
    }
    
    func alertlistAction(){
        
        //let coinSim = self.coinSimbol
        let uid = AppDelegate.uid
        let sessionid = AppDelegate.sessionid
        
        //매칭리스트 값 호출
        var urlComponents = URLComponents(string:"\(AppDelegate.url)/ajax/alramList?uid=\(uid)&sessionId=\(sessionid)")!
        let amo = Alamofire.request(urlComponents)
        amo.responseJSON(completionHandler:
            { response in
                
                //json 파싱
                self.jsonArray = response.result.value as? [[String: Any]]
                
                print("json count = \(self.jsonArray?.count)")
                print("json data value = \(self.jsonArray?.count)")
                
                if self.jsonArray?.count == 0 {
                    
                    self.alertListTb.isHidden = true
                    
                }else {
                    self.alertListTb.reloadData()
                }
                
                //print(self.dataJsondic)
//
//                let openprice = self.dataJsondic?["lastPrice"] as? String
//                let highestprice = self.dataJsondic?["highPrice"] as? String
//                let lowestprice = self.dataJsondic?["lowPrice"] as? String
//                let updnrate = self.dataJsondic?["updnRate"] as? String
//
//                self.openPrice.text = openprice?.insertComma
//                self.highestPrice.text = highestprice?.insertComma
//                self.lowestPrice.text = lowestprice?.insertComma
//                self.updnRate.text = self.stringChage(value1: nil, value2: "%", origin: updnrate!)
                
        })
        
    }
    
    func stringChage(value1: String?, value2: String, origin: String) -> String {
        
        var str : String = origin
        
        if value1 == nil {
            str = "\(str) \(value2)"
            
        }else {
            str = "\(value1!) \(str) \(value2)"
        }
        
        return str
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.jsonArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "alertViewCell", for: indexPath) as? AlertViewCell else {
            print("cell null value ")
            return UITableViewCell()
        }
        
        cell.backgroundColor = UIColor.clear
        
        let value = jsonArray![indexPath.row] as? [String : Any]
        
        //value!["price"] as! String
        var attText = NSMutableAttributedString(string: "12342532134234", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.gray])
        // value!["simbol"] as! String
        attText.append(NSAttributedString(string: "   BTC", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.white]))
        
        cell.simbolLabel.attributedText = attText
        
        cell.deleteBtn.addTarget(self, action: #selector(delbtnAction), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return self.headerVi
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return self.footerVi
    }
    
    @objc func delbtnAction(){
        print("action coin alert delete")
        
        let coinSim = self.coinSimbol
        let uid = AppDelegate.uid
        let sessionid = AppDelegate.sessionid
        
        var urlComponents = URLComponents(string:"\(AppDelegate.url)/ajax/alramProc?simbol=\(coinSim)&uid=\(uid)&sessionId=\(sessionid)&price=\(self.selectPrice.text!)&type=D")!
        let amo = Alamofire.request(urlComponents)
        amo.responseString { (respons) in
            
            if respons.result.isSuccess {
                
                let alert = UIAlertController(title: "알림추가", message: "지정가 알림을 삭제하였습니다.", preferredStyle: .alert)
                
                let conAtion = UIAlertAction(title: "확인", style: .cancel, handler: nil)
                
                alert.addAction(conAtion)
                
                self.present(alert, animated: true, completion: nil)
                
                self.alertListTb.reloadData()
                
                print("\(respons.result.value)")
            }
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var allowCharatorSet = CharacterSet.decimalDigits
        let charaterset = CharacterSet(charactersIn: string)
        allowCharatorSet.insert(charactersIn: ".")
        return allowCharatorSet.isSuperset(of: charaterset)
    }
    
    @objc func textfieldChageEvent() {
        
        if self.selectPrice.text == "." {
            self.selectPrice.text = ""
        }else if self.currentPrice.text == "." {
            self.currentPrice.text = ""
        }
        
    }
    
}
