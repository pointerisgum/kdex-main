//
//  ExchangListKRWcontroller.swift
//  crevolc
//
//  Created by crovolc on 2017. 12. 28..
//  Copyright © 2017년 hanbiteni. All rights reserved.
//
// KRW 거래소 리스트 페이지

import UIKit
import SystemConfiguration
import Alamofire

class ExchangListKRWcontroller: UIViewController, UITableViewDelegate, UITableViewDataSource, RealTimeDelegate {
    
    let appID = Bundle.bundleIdentifier
    let appIDNum = "1387360646"
    
    var subscribeRS : NTcsReal = NTcsReal()

    var viewDelegate : ViewController?
    
    //검색 키워드 옵저버
    var searchValue = "" {
        didSet{
            exchangListMager.searchcoinList(market: self.maket, tableview: self.exahangTableView, activity: self.activity, search: searchValue, first: false)
            exchangListMager.tempArray.removeAll()
        }
    }
    
    var torchEvent : UITapGestureRecognizer?
    
    private var realTimeSelect = false
    private var titleSortBool = true
    private var volSortBool = true
    private var priceSortBool = true
    private var rateSortBool = true
    
    private let maket = "KRW"
    
    var ethwalletAddr = "" //ETH 지갑 주소
    
    @IBOutlet var activity: UIActivityIndicatorView!
    
    var exchangListMager = ExchangCoinListManager()
    
    @IBOutlet var exahangTableView: UITableView!
    
    var didSelectRow: ((String, String, String) -> ())?
    
    //table header start -------------------
    
    let headerView : UIView = {
        let uv = UIView()
        uv.backgroundColor = UIColor.white
        return uv
    }()
    
    let bottombarVi : UIView = {
        let vi = UIView()
        vi.backgroundColor = UIColor(rgb: 0xcacacd)
        vi.translatesAutoresizingMaskIntoConstraints = false
        return vi
    }()
    
    let favorImg : UIImageView = {
        let image = UIImage(named: "star_on")
        let img = UIImageView(image: image)
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    let AsortBtn : UIButton = {
        let bt = UIButton()
        let iamge = UIImage(named: "icn_list_sort_deactive-1")
        bt.backgroundColor =  UIColor.clear
        bt.translatesAutoresizingMaskIntoConstraints = false
        bt.setAttributedTitle(NSAttributedString(string: "trade_market_coin_name".localized, attributes:[NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.DPmainTextColor]), for: .normal)
        if AppDelegate.appLang == "en_US"{
            bt.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -55)
        }else {
            bt.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -75)
        }
        bt.titleEdgeInsets = UIEdgeInsets(top: 0, left: -40, bottom: 0, right: 0)
        bt.setImage(iamge, for: .normal)
        bt.addTarget(self, action: #selector(titleSort), for: .touchUpInside)
        return bt
    }()
    
    let BsortBtn : UIButton = {
        let bt = UIButton()
        let iamge = UIImage(named: "icn_list_sort_deactive-1")
        bt.translatesAutoresizingMaskIntoConstraints = false
        bt.setAttributedTitle(NSAttributedString(string: "trade_market_rate".localized, attributes:[NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.DPmainTextColor]), for: .normal)
        if AppDelegate.appLang == "en_US"{
            bt.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -65)
        }else {
            bt.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -70)
        }
        bt.titleEdgeInsets = UIEdgeInsets(top: 0, left: -45, bottom: 0, right: 0)
        bt.setImage(iamge, for: .normal)
        bt.addTarget(self, action: #selector(volSort), for: .touchUpInside)
        bt.backgroundColor =  UIColor.clear
        
        return bt
    }()
    
    let CsortBtn : UIButton = {
        let bt = UIButton()
        let iamge = UIImage(named: "icn_list_sort_deactive-1")
        bt.translatesAutoresizingMaskIntoConstraints = false
        bt.setAttributedTitle(NSAttributedString(string: "trade_market_last_price".localized, attributes:[NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.DPmainTextColor]), for: .normal)
        bt.setImage(iamge, for: .normal)
        if AppDelegate.appLang == "en_US"{
            bt.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -125)
        }else {
            bt.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -70)
        }
        bt.titleEdgeInsets = UIEdgeInsets(top: 0, left: -45, bottom: 0, right: 0)
        bt.addTarget(self, action: #selector(priceSort), for: .touchUpInside)
        bt.backgroundColor = UIColor.clear
        return bt
    }()
    
    let DsortBtn : UIButton = {
        let bt = UIButton()
        let iamge = UIImage(named: "icn_list_sort_deactive-1")
        bt.translatesAutoresizingMaskIntoConstraints = false
        bt.setAttributedTitle(NSAttributedString(string: "trade_market_yesterday".localized, attributes:[NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.DPmainTextColor]), for: .normal)
        bt.setImage(iamge, for: .normal)
        if AppDelegate.appLang == "en_US"{
            bt.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -90)
        }else {
            bt.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -110)
        }
        bt.titleEdgeInsets = UIEdgeInsets(top: 0, left: -30, bottom: 0, right: 0)
        bt.addTarget(self, action: #selector(rateSort), for: .touchUpInside)
        bt.backgroundColor =  UIColor.clear
        return bt
    }()
    
    //table header end -------------------
    
    fileprivate func headerSetup() {
        headerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 40)
        
        //        headerView.addSubview(favorImg)
        headerView.addSubview(bottombarVi)
        headerView.addSubview(AsortBtn)
        headerView.addSubview(BsortBtn)
        headerView.addSubview(CsortBtn)
        headerView.addSubview(DsortBtn)
        
        bottombarVi.bottomAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
        bottombarVi.leadingAnchor.constraint(equalTo: headerView.leadingAnchor).isActive = true
        bottombarVi.trailingAnchor.constraint(equalTo: headerView.trailingAnchor).isActive = true
        bottombarVi.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        //        favorImg.leftAnchor.constraint(equalTo: headerView.leftAnchor, constant: 10).isActive = true
        //        favorImg.widthAnchor.constraint(equalToConstant: 20).isActive = true
        //        favorImg.heightAnchor.constraint(equalToConstant: 20).isActive = true
        //        favorImg.centerYAnchor.constraint(lessThanOrEqualTo: headerView.centerYAnchor).isActive = true
        //        AsortBtn.leftAnchor.constraint(equalTo: headerView.leftAnchor, constant: 33).isActive = true
        
        AsortBtn.widthAnchor.constraint(equalToConstant: 60).isActive = true
        AsortBtn.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 5).isActive = true
        AsortBtn.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
        AsortBtn.centerXAnchor.constraint(equalTo: headerView.centerXAnchor, constant: -(self.view.frame.width/2 - 20)).isActive = true
//        AsortBtn.leadingAnchor.constraint(equalTo: <#T##NSLayoutAnchor<NSLayoutXAxisAnchor>#>, constant: <#T##CGFloat#>)
        
        //        BsortBtn.leftAnchor.constraint(equalTo: AsortBtn.rightAnchor, constant: 3).isActive = true
        BsortBtn.widthAnchor.constraint(equalToConstant: 40).isActive = true
        //        BsortBtn.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 5).isActive = true
        BsortBtn.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
        BsortBtn.centerXAnchor.constraint(equalTo: headerView.centerXAnchor, constant: (self.view.frame.width/2 - 60)).isActive = true
        
        CsortBtn.widthAnchor.constraint(equalToConstant: 60).isActive = true
        CsortBtn.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
        CsortBtn.centerXAnchor.constraint(equalTo: headerView.centerXAnchor, constant: -20).isActive = true
        
        DsortBtn.widthAnchor.constraint(equalToConstant: 70).isActive = true
        DsortBtn.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
        DsortBtn.centerXAnchor.constraint(equalTo: headerView.centerXAnchor, constant: (self.view.frame.width/2 - 140)).isActive = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerSetup()
        
        self.exahangTableView.backgroundColor = UIColor(rgb: 0xf3f4f5)
        
        self.exahangTableView.layoutMargins = UIEdgeInsets.zero
        self.exahangTableView.separatorInset = UIEdgeInsets.zero
        self.exahangTableView.separatorStyle = .none
        
        self.subscribeRS.subscribeRealSise(uid: AppDelegate.uid, symbol: " ", outputStream: TcpSocket.sharedInstance.outputStream!)
        
        let nib = UINib(nibName: "ExchangListNib", bundle: nil)
        exahangTableView.register(nib, forCellReuseIdentifier: "newExchangCell")
        
        titleSort()
        startPage()
        updateCheck()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        print("list krW viewdid Appear")
        
        let plist = UserDefaults.standard
        plist.set("KRW", forKey: "marketSimbol")
        plist.synchronize()
        
        exchangListMager.searchcoinList(market: self.maket, tableview: self.exahangTableView, activity: self.activity, search: AppDelegate.localSearch, first: true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        if AppDelegate.notiAlertBool {
            
            AppDelegate.notiAlertBool = false
            
//            let str = UIStoryboard(name: "Main", bundle: nil)
//
//            //                  let alertview = str.instantiateViewController(withIdentifier: "mainView") as? ViewController
//            let notivi = str.instantiateViewController(withIdentifier: "RevealViewCT") as? RevealViewController
            //                    let frontview = str.instantiateViewController(withIdentifier: "sw_front") as? UITabBarController // 지정가 알림 사용
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let detail = storyboard.instantiateViewController(withIdentifier: "NoticeNav") as? UINavigationController {
                self.present(detail, animated: true, completion: {
                })
            }
        }
        
        
        if AppDelegate.changeAlertBool {
             self.ethwalletInfo()
            
            AppDelegate.changeAlertBool = false
            
            let alert = UIAlertController(title: "KDA 토큰 환전신청", message: "KDA 토큰 환전신청을 승인하시겠습니까?", preferredStyle: .alert)
            
            let cancel = UIAlertAction(title: "취소", style: .cancel, handler: nil)
            let ok = UIAlertAction(title: "확인", style: .default) { (_) in
                self.ethwalletInfo()
            }
            alert.addAction(cancel)
            alert.addAction(ok)
            
            self.present(alert, animated: false, completion: nil)
            
        }
        
        
        if torchEvent != nil {
            print("touch event")
            exahangTableView.removeGestureRecognizer(torchEvent!)
        }
        
        setupKeyboardObservers()
        
        print("RT KRWviewWillAppear")
       
        TcpSocket.sharedInstance.receiverThread?.delegate = self
        
        if AppDelegate.walletBool {
            print("wallet type 4 start")
            walletAlertAction()
            AppDelegate.walletBool = false
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    func startPage() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)

        if let detail = storyboard.instantiateViewController(withIdentifier: "homeViPop") as? HomeViewController  {

            guard let appdel = UIApplication.shared.delegate as? AppDelegate else {
                return
            }
            
            appdel.window?.rootViewController?.present(detail, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard  let tempjson = exchangListMager.jsonArray else {
            return 0
        }
        
        return exchangListMager.jsonArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = tableView.dequeueReusableCell(withIdentifier: "exchangeCell") as? ExchangListCell
        let cell = tableView.dequeueReusableCell(withIdentifier: "newExchangCell", for: indexPath) as? ExchangListCell
        
        cell?.selectionStyle = .none
        cell?.layoutMargins = UIEdgeInsets.zero
        
        
        let dataTemp = exchangListMager.jsonArray![indexPath.row] as [String : Any]
        //print(dataTemp)
        
        let coinsim = dataTemp["simbol"] as? String ?? ""
        let coinname = dataTemp["coinName"] as? String ?? ""
        let coinper = dataTemp["updnRate"] as? String ?? ""
        var updnPrice = dataTemp["updnPrice"] as? String ?? "0"
        let coinvol = dataTemp["totalVol"] as? String ?? ""
        let coinvalue = dataTemp["lastPrice"] as? String ?? ""
        let updnSign = dataTemp["updnSign"] as? String ?? "0"
        
        print("trandeupdnsign : \(updnSign)")
        
        let str = coinvalue.insertComma
        
        var simboleStr = ""
        var marketStr = ""
        
        if coinsim.contains("_") {
            
            let indexStr = coinsim.index(of: "_")
        
            simboleStr = String(coinsim.prefix(upTo: indexStr!))
            marketStr = String(coinsim.suffix(from: indexStr!))
            
        }else {
            simboleStr = coinsim
            marketStr = "/KRW"
        }
        
//        let lastcoinsim = coinsim.replacingOccurrences(of: "_", with: "/")
        
        print("simbostr : \(simboleStr), makerStr : \(marketStr)")
    
        
        let attr = NSMutableAttributedString(string: simboleStr, attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 13), NSAttributedString.Key.foregroundColor : UIColor(rgb: 0x666666)])
        
        attr.append(NSAttributedString(string: marketStr, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12), NSAttributedString.Key.foregroundColor : UIColor(rgb: 0x999999)]))
        
        
        let perStr = self.stringChage(value1: "", value2: "%", origin: "\(String(format: "%0.2f", stringtoDouble(str: coinper.insertComma) ?? 0.0))")
        
        let widht = str.calcWidth(forHeight: 30, font: UIFont.systemFont(ofSize: 16))
        
        cell?.coinSymbol = coinsim
        
        cell?.simbol.attributedText = attr
        
        cell?.coinName.text = coinname
        cell?.coinName.font = UIFont.boldSystemFont(ofSize: 14)
        
        if coinsim == "BTC" {
            AppDelegate.BTC = stringtoDouble(str: coinvalue)!
        }else if coinsim == "ETH" {
            AppDelegate.ETH = stringtoDouble(str: coinvalue)!
        }
        
        if updnSign == "1" {
            cell?.coinValue.textColor = UIColor.DPPlusTextColor
            cell?.coinPer.textColor = UIColor.DPPlusTextColor
            cell?.perVsValue.textColor = UIColor.DPPlusTextColor
            //cell?.coinPerBackground.backgroundColor = UIColor.DPSellDefaultColor
        }else if updnSign == "-1"{
            cell?.coinValue.textColor = UIColor.DPMinTextColor
            cell?.coinPer.textColor = UIColor.DPMinTextColor
            cell?.perVsValue.textColor = UIColor.DPMinTextColor
            //cell?.coinPerBackground.backgroundColor = UIColor.DPBuyDefaultColor
        }else{
            cell?.coinValue.textColor = UIColor.DPmainTextColor
            cell?.coinPer.textColor = UIColor.DPmainTextColor
            cell?.perVsValue.textColor = UIColor.DPmainTextColor
        }
        
        print("krw coinper : \(coinper)")
//        cell?.coinPer.drawText(in: CGRect(x: 0.5, y: 0.5, width: 0.5, height: 0.5))
        cell?.coinPer.adjustsFontSizeToFitWidth = true
        cell?.coinPer.text = perStr
        
        cell?.coinVol.text = self.stringChage(value1: "", value2: "", origin: (coinvol.insertComma))
        cell?.coinVol.font = UIFont.systemFont(ofSize: 12)
        cell?.coinValue.numberOfLines = 2
        cell?.coinValue.text = str
        cell?.coinValue.adjustsFontSizeToFitWidth = true
        cell?.coinValue.sizeToFit()
        
        if updnPrice.contains("-") {
            updnPrice.replace(originalString: "-", withString: "")
        }
        
        let dbUpdnPrice = stringtoDouble(str: updnPrice) ?? 0.0
        
        if dbUpdnPrice == 0.0 {
           cell?.perVsValue.text = String(format: "%0.0f", dbUpdnPrice).insertComma
        }else if dbUpdnPrice < 100.0 && dbUpdnPrice > 10.0 {
            cell?.perVsValue.text = String(format: "%0.1f", dbUpdnPrice).insertComma
        }else if dbUpdnPrice < 10.0 {
            cell?.perVsValue.text = String(format: "%0.2f", dbUpdnPrice).insertComma
        }else{
            cell?.perVsValue.text = String(format: "%0.0f", dbUpdnPrice).insertComma
        }
        
        
        
//        let widht = .calcWidth(forHeight: 20, font: UIFont.systemFont(ofSize: 14))
//        
//        cell?.coinValue.frame.size = CGSize(width: widht, height: (cell?.frame.height)! - 5)
        
        
        cell?.coinValue.frame.size = CGSize(width: widht, height: 30)
        
//        if (indexPath.row % 2) == 0  {
//            
//            cell?.backgroundColor = UIColor.DPViewBackgroundColor
//            
//        }else {
//            cell?.backgroundColor = UIColor.DPViewGrayBackgroundColor
//        }
        
        if realTimeSelect {
            print("RT animation start verty")
//                UIView.animate(withDuration: 10, animations: {
//                    print("RT animation")
//
//                    cell?.coinValue.layer.borderColor = UIColor.DPBuyDefaultColor.cgColor
//
//                }, completion: { (_) in
//                    print("RT animation completion")
//                    cell?.coinValue.layer.borderColor = UIColor.white.cgColor
//
//                })
            
            realTimeSelect = false
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        let alert = UIAlertController(title: "trade_market_title".localized, message: "pop_wonMark_ready".localized, preferredStyle: .alert)
//
//        let action = UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil)
//
//        alert.addAction(action)
//
//        self.present(alert, animated: false, completion: nil)
//
//        view.endEditing(true)
        
//         주후 작업 필요 절대 지우지 말것
        let dataTemp = exchangListMager.jsonArray![indexPath.row] as [String : Any]
        let coinSimbol = dataTemp["simbol"] as? String
        let coinName = dataTemp["coinName"] as? String
        didSelectRow?(coinSimbol!, coinName!, self.maket)
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return self.headerView
    }
    
//    func stringChage(value1: String?, value2: String, origin: String) -> String {
//
//        var str : String = origin
//
//        if value1 == nil {
//            str = "\(str) \(value2)"
//
//        }else {
//            str = "\(value1!) \(str) \(value2)"
//        }
//
//        return str
//    }
    
    func stringChage(value1: String, value2: String, origin: String) -> String {
        
        var str : String = origin
        
        if value1 == "" {
            str = "\(str) \(value2)"
            print("krw coinper value 1 : \(str)")
        }else if value2 == ""{
            str = "\(value1) \(str)"
            print("krw coinper value 2 : \(str)")
        }else {
            str = "\(value1) \(str) \(value2)"
            print("krw coinper value 3 : \(str)")
        }
        
        
         print("krw coinper value result : \(str)")
        return str
    }

    @objc func titleSort(){
        
        if titleSortBool {
            titleSortBool = false
            
            let arr = try exchangListMager.jsonArray?.sorted(by:{
                guard let num1 = $0["coinName"] as? String else {
                    return true
                }
                
                guard let num2 = $1["coinName"] as? String else {
                    return true
                }
                
                return num1 > num2
                
            })
            
            exchangListMager.jsonArray = arr
            exahangTableView.reloadData();
        }else {
            titleSortBool = true
            
            let arr = try exchangListMager.jsonArray?.sorted(by:{
                guard let num1 = $0["coinName"] as? String else {
                    return true
                }
                
                guard let num2 = $1["coinName"] as? String else {
                    return true
                }
                
                return num1 < num2
                
            })
            
            exchangListMager.jsonArray = arr
            exahangTableView.reloadData();
        }
        
    }
    
    @objc func volSort(){
        
        if volSortBool {
            volSortBool = false
            
            let arr = try exchangListMager.jsonArray?.sorted(by:{
                guard let num1 = $0["totalVol"] as? String else {
                    return true
                }
                
                guard let num2 = $1["totalVol"] as? String else {
                    return true
                }
                
                return num1 > num2
                
            })
            
            exchangListMager.jsonArray = arr
            exahangTableView.reloadData();
        }else {
            volSortBool = true
            
            let arr = try exchangListMager.jsonArray?.sorted(by:{
                guard let num1 = $0["totalVol"] as? String else {
                    return true
                }
                
                guard let num2 = $1["totalVol"] as? String else {
                    return true
                }
                
                return num1 < num2
                
            })
            
            exchangListMager.jsonArray = arr
            exahangTableView.reloadData();
        }
        
    }
    
    @objc func priceSort(){
        
        if priceSortBool {
            priceSortBool = false
            
            let arr = try exchangListMager.jsonArray?.sorted(by:{
                guard let num1 = self.stringtoDouble(str: ($0["lastPrice"] as? String)!) else {
                    return true
                }
                
                guard let num2 = self.stringtoDouble(str: ($1["lastPrice"] as? String)!) else {
                    return true
                }
                
                return num1 > num2
                
            })
            
            exchangListMager.jsonArray = arr
            exahangTableView.reloadData();
        }else {
            priceSortBool = true
            
            let arr = try exchangListMager.jsonArray?.sorted(by:{
                guard let num1 = self.stringtoDouble(str: ($0["lastPrice"] as? String)!) else {
                    return true
                }
                
                guard let num2 = self.stringtoDouble(str: ($1["lastPrice"] as? String)!) else {
                    return true
                }
                
                return num1 < num2
    
            })
            
            exchangListMager.jsonArray = arr
            exahangTableView.reloadData();
        }
        
    }
    
    @objc func rateSort(){
        
        if rateSortBool {
            rateSortBool = false
            
            let arr = try exchangListMager.jsonArray?.sorted(by:{
                guard let num1 = $0["updnRate"] as? String else {
                    return true
                }
                
                guard let num2 = $1["updnRate"] as? String else {
                    return true
                }
                
                return num1 > num2
                
            })
            
            exchangListMager.jsonArray = arr
            exahangTableView.reloadData();
        }else {
            rateSortBool = true
            
            let arr = try exchangListMager.jsonArray?.sorted(by:{
                guard let num1 = $0["updnRate"] as? String else {
                    return true
                }
                
                guard let num2 = $1["updnRate"] as? String else {
                    return true
                }
                
                return num1 < num2
                
            })
            
            exchangListMager.jsonArray = arr
            exahangTableView.reloadData();
        }
        
    }
    
    func stringtoDouble(str : String) -> Double?{
        let temp = str.components(separatedBy: [","]).joined()
        let double = Double(temp)
        return double ?? 0.0
    }
    
    //real time delegate
    
    func exchangListRT(bt: NPriceTick) {
        print("RT : exchangListKRWRT")
        
        var index = 0
        var num = 0
        var dic = [String : Any]()
        
        let symbol = bt.symbol.toString().components(separatedBy: [" "]).joined()
        let delta_rate = bt.delta_rate.toString()
        let last_volume = bt.last_volume.toString()
        let last_price = bt.last_price.toString()
        let intLast_volume = stringtoDouble(str: last_volume)
        
        if let intLast_price = Int(last_price){
            dic.updateValue(String(intLast_price), forKey: "lastPrice")
        }else {
            let intLast_price = stringtoDouble(str: last_price)
            dic.updateValue(String(intLast_price!), forKey: "lastPrice")
        }
        
        dic.updateValue(symbol, forKey: "simbol")
        dic.updateValue(delta_rate, forKey: "updnRate" )
        dic.updateValue(String(intLast_volume!), forKey: "totalVol")
        
        
        print("RT symbole : \(symbol)")
        
        let intlastVol = Int(last_volume) ?? 0
        
        for row in exchangListMager.jsonArray ?? [[String : Any]]() {
            
            let sym = row as [String : Any]
            
            let tempSymbole = sym["simbol"] as? String ?? ""
            
            print("RT symbol : \(tempSymbole)")
            
            print("RT symbols : \(symbol)")
            
            print("RT test : \(symbol == tempSymbole)")
            
            if symbol == tempSymbole {
                
                num = index
                
                let coinname = sym["coinName"] as? String ?? ""
                
                print("RT coinname : \(coinname)")
                
                dic.updateValue(coinname, forKey: "coinName" )
                
                exchangListMager.jsonArray![index] = dic
                
//                if intlastVol != 0 {
//                    exchangListMager.jsonArray![index] = dic
//                }else {
//                    if let intLast_price = Int(last_price){
//                        exchangListMager.jsonArray![index].updateValue(String(intLast_price), forKey: "lastPrice")
//                    }else {
//                        let intLast_price = stringtoDouble(str: last_price)
//                        exchangListMager.jsonArray![index].updateValue(String(intLast_price!), forKey: "lastPrice")
//                    }
//                }
                
                self.realTimeSelect = true
                print("RT coinname : \(coinname)")
                print("RT index : \(index)")
                
            }
            
            index = index + 1
        }
 
        print("RT Tick : \(dic)")
        let indexpath = IndexPath(row: num, section: 0)
        
        DispatchQueue.main.async {
            self.exahangTableView.reloadRows(at: [indexpath], with: .none)
        }
        
    }
    
    func exchangDetailRT(bt: NPriceTick) {
       
    }
    
    func openListRT(bt: NOpenInfo) {
        
    }
    
    func trandeListRT(bt: NPriceInfo) {
        
    }
    
    func krwChangeRT(value: Double) {
        
    }
    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        print("screen touchbegan")
//    }
    
    @objc func hideKeyboard() {
        print("touchevent ")
        viewDelegate?.hideKeyboard()
    }
    
    func setupKeyboardObservers(){
        print("setupKeyboardObservers")
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    @objc func handleKeyboardWillShow(_ notification: Notification) {
        print("show keyborad")
        
        torchEvent = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        exahangTableView.addGestureRecognizer(torchEvent!)
    }
    
    @objc func handleKeyboardWillHide(_ notification: Notification)  {
        print("hide keyborad")
        if torchEvent != nil {
            print("touch event")
            exahangTableView.removeGestureRecognizer(torchEvent!)
        }
    }
    
    func walletAlertAction() {
        
        let alert = UIAlertController(title: "pop_alert".localized, message: nil, preferredStyle: UIAlertController.Style.alert)
        
        let vicontroller = UIViewController()
        let vi = AppDelegate.alertContentView(strArr: AppDelegate.alertUserInfo, msg: "", width: vicontroller.view.frame.width, height: 140 )
        vicontroller.view = vi
        vicontroller.preferredContentSize.height = 140
        alert.setValue(vicontroller, forKeyPath: "contentViewController")
        
        //이곳에 내용 넣기
        var code = ""
        let pushType = AppDelegate.alertUserInfo["pushType"] ?? ""
        let sendDate = AppDelegate.alertUserInfo["sendDate"] ?? ""
        let sendTime = AppDelegate.alertUserInfo["sendTime"] ?? ""
        
        if pushType == "4" {
            code = "50002"
        }else if pushType == "5" {
            code = "50003"
        }
        
        //수정
        let okbtn = UIAlertAction(title: "common_yes_btn".localized, style: .default) { (_) in
            
            if pushType == "4" {
                
                let simbol = AppDelegate.alertUserInfo["simbol"] ?? ""
                let sendAddr = AppDelegate.alertUserInfo["sendAddr"] ?? ""
                let sendQty = AppDelegate.alertUserInfo["sendQty"] ?? ""
                let sendFee = AppDelegate.alertUserInfo["sendFee"] ?? ""
                let sendGasLimit = AppDelegate.alertUserInfo["sendGasLimit"] ?? ""
                let sendGwei = AppDelegate.alertUserInfo["sendGwei"] ?? ""
                
                let passPopVc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "emailAuthID") as! EmailAuthController
                
                //                    passPopVc.authdelegate = self
                passPopVc.alertBool = false
                passPopVc.sendBool = true
                passPopVc.simbol = simbol
                passPopVc.address = sendAddr
                passPopVc.qty = sendQty
                passPopVc.fee = sendFee
                passPopVc.sendGasLimit = sendGasLimit
                passPopVc.sendGwei = sendGwei
                passPopVc.date = sendDate
                passPopVc.time = sendTime
                
                let alertWindow = UIWindow(frame: UIScreen.main.bounds)
                alertWindow.rootViewController = UIViewController()
                alertWindow.windowLevel = UIWindow.Level.alert + 1;
                alertWindow.makeKeyAndVisible()
                alertWindow.rootViewController?.present(passPopVc, animated: true, completion: nil)
                
//                AppDelegate.sendAlertAction(uid: AppDelegate.uid, sessionid: AppDelegate.sessionid, simbol: simbol, address: sendAddr, qty: sendQty, fee: sendFee, sendGasLimit: sendGasLimit, sendGwei: sendGwei, date: sendDate, time: sendTime, complete: nil)
                
            } else if pushType == "5" {
                
                let fromSimbol = AppDelegate.alertUserInfo["fromSimbol"] ?? ""
                let toSimbol = AppDelegate.alertUserInfo["toSimbol"] ?? ""
                let exQty = AppDelegate.alertUserInfo["exQty"] ?? ""
                let exRate = AppDelegate.alertUserInfo["exRate"] ?? ""
                
//                AppDelegate.exchangeAlertAction(uid:  AppDelegate.uid, sessionid: AppDelegate.sessionid, fromsimbol: fromSimbol, tosimbole: toSimbol, qty: exQty, sendfee: exRate, date: sendDate, time: sendTime)
                
                let passPopVc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "emailAuthID") as! EmailAuthController
                
                //                    passPopVc.authdelegate = self
                passPopVc.alertBool = false
                passPopVc.sendBool = false
                passPopVc.fromsimbol = fromSimbol
                passPopVc.tosimbole = toSimbol
                passPopVc.qty = exQty
                passPopVc.fee = exRate
                passPopVc.date = sendDate
                passPopVc.time = sendTime
                
                let alertWindow = UIWindow(frame: UIScreen.main.bounds)
                alertWindow.rootViewController = UIViewController()
                alertWindow.windowLevel = UIWindow.Level.alert + 1;
                alertWindow.makeKeyAndVisible()
                alertWindow.rootViewController?.present(passPopVc, animated: true, completion: nil)
                
            } else if pushType == "6" {
                
                let requestFunctionType = AppDelegate.alertUserInfo["function"]
                
                if requestFunctionType == "delete" {
                    
                    AppDelegate.deviceStopAction()
                    
                }else if requestFunctionType == "copy"{
                    
                    AppDelegate.requestCopyPrvateKeyAction()
                    
                }else if requestFunctionType == "change" {
                    
                    AppDelegate.deviceMainChangeAction()
                    
                }
                
            }
            
        }
        
        let cancel = UIAlertAction(title: "common_no_btn".localized, style: .cancel, handler: { (_) in
            AppDelegate.alertReadLogAction(uid: AppDelegate.uid, sessionId: AppDelegate.sessionid, date: sendDate, time: sendTime, code:code, delete: false)
        })
        
        alert.addAction(cancel)
        alert.addAction(okbtn)
        
        self.present(alert, animated: false, completion: nil)
        
    }
    
    func updateCheck() {
        
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//
//        if let detail = storyboard.instantiateViewController(withIdentifier: "homeViPop") as? HomeViewController  {
//
//            let alertWindow = UIWindow(frame: UIScreen.main.bounds)
//            alertWindow.rootViewController = UIViewController()
//            alertWindow.windowLevel = UIWindow.Level.alert + 1;
//            alertWindow.makeKeyAndVisible()
//            alertWindow.rootViewController?.present(detail, animated: true, completion: nil)
//        }
        
        if ( isConnectedToNetwork() ) {
            print("start ???")

            let checkVersion = needUpdate()

            let ckappVersion = checkVersion["ckappVersion"] as! String

            print("ckappversion: \(ckappVersion)")

            if checkVersion["storeVersion"] != nil {

                let storeVersion = checkVersion["storeVersion"] as! String

                print("storeVersion: \(storeVersion)")

                print("needupdate : \(checkVersion["needUpdate"])")

                if checkVersion["needUpdate"] as! Bool {

                    let storyboard = UIStoryboard(name: "Main", bundle: nil)

                    if let detail = storyboard.instantiateViewController(withIdentifier: "updatePop") as? UpdateController  {

                        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
                        alertWindow.rootViewController = UIViewController()
                        alertWindow.windowLevel = UIWindow.Level.alert + 1;
                        alertWindow.makeKeyAndVisible()
                        alertWindow.rootViewController?.present(detail, animated: true, completion: nil)
                    }

//                    self.appVersionLabel.text = "현재 버전 정보 \(ckappVersion)"
//                    self.appLastDownloadbtn.setTitle("최신 버전은 \(storeVersion) 입니다.", for: .normal)
//                    self.appLastDownloadbtn.addTarget(self, action: #selector(versionUpdate), for: .touchUpInside)
                } else {
//                    self.appVersionLabel.text = "현재 버전 정보 \(ckappVersion)"
//                    self.appLastDownloadbtn.setTitle("현재 최신 버전입니다.", for: .normal)
                }

            }else{
//                self.appVersionLabel.text = "현재 버전 정보 \(Bundle.appVersion).\(Bundle.appBuildVersion)"
//                self.appLastDownloadbtn.setTitle("최신버전을 확인 할 수 없는 상태입니다", for: .normal)
            }

        } else {
//            let versionCode = Bundle.appVersion
//            let buildNumber = Bundle.appBuildVersion
//            self.appVersionLabel.text = "현재 버전 정보 \(versionCode).\(buildNumber)"
//            self.appLastDownloadbtn.setTitle("네트워크 비활성화로 최신버전을 알 수가 없습니다.", for: .normal)
        }

        
    }
    
    
    func needUpdate() -> NSDictionary {
        
        let ret = NSMutableDictionary()
        
//        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        
        
        
        let jsonData = getJSON(urlToRequest: "http://itunes.apple.com/lookup?bundleId="+self.appID)
        
        print("checkappid : \(self.appID)")
        let lookup = try? JSONSerialization.jsonObject(with: jsonData as Data, options:  JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
        
        //
        let ckappVersion = Bundle.appVersion
        ret.setValue(ckappVersion, forKey: "ckappVersion")
        
        //
        if ( (lookup!["resultCount"] as AnyObject).integerValue == 1 ) {
            let resultsArray = lookup!["results"] as! NSArray // MDLMaterialProperty to Array
            let resultsDic = resultsArray[0] as! NSDictionary
            let storeVersion = resultsDic["version"] as! String
            NSLog("need storversion : \(storeVersion)")
            NSLog("need ckappversion : \(ckappVersion)")
            
            //compare메소드는 comparisonResult객체를 반환
            if (storeVersion as NSString).compare(ckappVersion, options:.caseInsensitive) == ComparisonResult.orderedDescending {
                ret.setValue(true, forKey: "needUpdate")
            }else {
                ret.setValue(false, forKey: "needUpdate")
            }
            ret.setValue(storeVersion, forKey: "storeVersion")
        }
        return ret
    }
    
    func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1){
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        })
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    func getJSON(urlToRequest: String) -> NSData {
        var data = NSData()
        do {
            data = try NSData(contentsOf: NSURL(string: urlToRequest)! as URL, options: NSData.ReadingOptions() )
        } catch let error as NSError {
            print( error.description )
        } catch {}
        return data
    }
    
    //KDA 환전신청
    func sendTakeAction() {
        
        self.activity.startAnimating()
        
        let url = URL(string: "\(AppDelegate.url)/auth/walletSend")
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let param = "uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&type=3&simbol=KDA&addr=\(self.ethwalletAddr)&waysType=M&privKey=\(AlertVO.shared.privKey1!)"
        
        print("wallet send param : \(param)")
        let paramData = param.data(using: .utf8)
        
        request.httpBody = paramData
        
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        request.setValue(String(describing: paramData!.count), forHTTPHeaderField: "Content-Length")
        
        //laucnhing
        let session = URLSession.shared
        
        let task = session.dataTask(with:request as URLRequest) {data, response, error in
            
            if error == nil {
                
                print("buy sueccess")
                DispatchQueue.main.async {
                    
                    //activity.stopAnimating()
                    do {
                        let object = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        
                        guard let jsonObject = object else { return }
                        
                        //self.sendAddress.text = ""
                        //                        self.sendQty.text = ""
                        //                        self.gasLimitField.text = ""
                        //                        self.sendSpeedfield.text = ""
                        //                        self.sendAddress.text = ""
                        //                        self.isAddress = false
                        //                        self.isChecked = false
                        //                        self.sendfee = ""
                        //                        self.speedBtntap = 0
                        //                        self.manuBtn.titleLabel?.text = "선택"
                        //                        self.sendFees.text = "\(String(format: "%.8f", 0.0))"
                        //                        self.totalAmount.text = "\(String(format: "%.8f", 0.0))"
                        //                        self.isAddress = false
                        //
                        print("jsonobject : \(jsonObject)")
                        
                        let sendResult = jsonObject["returnYn"] as? String ?? "N"
                        
                        print("send result : \(sendResult)")
                        
                        if sendResult == "N" {
                            
                            let msg = jsonObject["returnMsg"] as? String ?? ""
                            let alert = UIAlertController(title: "pop_wallet".localized, message: msg, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: { (_) in
                            }))
                            
                            self.present(alert, animated: false, completion: nil)
                            
                        }else {
                            
                            let msg = jsonObject["returnMsg"] as? String ?? ""
                            let alert = UIAlertController(title: "pop_wallet".localized, message: msg, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: { (_) in
                            }))
                            
                            self.present(alert, animated: false, completion: nil)
                            
                        }
                        
//                        self.userAmount()
                        
                        self.activity.stopAnimating()
                        
                    }catch{
                        print("cCaught an error:\(error)")
                        
                        let msg = "pop_wallet_send_not_reply".localized
                        let alert = UIAlertController(title: "pop_wallet".localized, message: msg, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
                        self.present(alert, animated: false, completion: nil)
                        
                        self.activity.stopAnimating()
                        
                    }
                }
            }else{
                print("error:\(error)")
                let msg = "pop_wallet_send_not_reply".localized
                let alert = UIAlertController(title: "pop_wallet".localized, message: msg, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
                
                self.present(alert, animated: false, completion: nil)
                
                self.activity.stopAnimating()
                
            }
            
        }
        
        task.resume()
        
    }
    
    func ethwalletInfo() {
        DBManager.sharedInstance.selecttWalletSimbol(simbol: "ETH")
        
        let activity = UIActivityIndicatorView()
        activity.style = .whiteLarge
        activity.color = UIColor.DPActivityColor
        activity.hidesWhenStopped = true
        activity.startAnimating()
        
        let url = "\(AppDelegate.url)/auth/walletInfo?simbol=ETH&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)"
        
        print(url)
        let amo = Alamofire.request(url)
        amo.responseJSON(completionHandler: { response in
            
            print("buy result :\(response.result.value)")
            
            if response.result.isSuccess == true {
                
                var result = response.result.value as? [String : Any]
                
                self.ethwalletAddr = (result!["wletAddr"] as? String) ?? ""
                self.sendTakeAction()
                
            }
            activity.stopAnimating()
        })
    }
}
