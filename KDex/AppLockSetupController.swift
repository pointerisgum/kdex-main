//
//  AppLockSetupController.swift
//  crevolc
//
//  Created by crovolc on 2018. 2. 6..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation
import UIKit

class AppLockSetupController: UIViewController {
    
    @IBOutlet var headertext: UITextView!
    
    @IBOutlet var lockSW: UISwitch!
    
    @IBOutlet var touchSW: UISwitch!
    
    @IBOutlet var changeBtn: UIButton!
    
    @IBOutlet var touchLabel: UILabel!
    
    @IBOutlet var touchcontent: UILabel!
    
    @IBOutlet var changeLabel: UILabel!
    
    @IBOutlet var changecontent: UILabel!
    
    @IBOutlet var bottomLine: UIView!
    
    @IBOutlet var topLine: UIView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.black
        
        lockSW.onTintColor = UIColor.DPbdidBgColor
        
        touchSW.onTintColor = UIColor.DPbdidBgColor
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationController?.navigationBar.topItem?.title = ""
    }
    
    override func viewDidAppear(_ animated: Bool) {
       
      self.intistart()
    }
    
    @IBAction func applockAction(_ sender: Any) {
        
        let plist = UserDefaults.standard
        
        if lockSW.isOn {
            //새로설정
            print("새로설정")
            plist.set(true, forKey: "appLock")
            self.touchSW.isEnabled = true
            
            changeBtn.isHidden = false
            touchSW.isHidden = false
            touchLabel.isHidden = false
            touchcontent.isHidden = false
            bottomLine.isHidden = false
            topLine.isHidden = false
            changeLabel.isHidden = false
            changecontent.isHidden = false
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let detail = storyboard.instantiateViewController(withIdentifier: "lockPad") as? LockPadController {
                self.present(detail, animated: true, completion: nil)
            }
        }else{
            //설정해지
            print("설정해지")
            plist.set(false, forKey: "appLock")
            plist.set(false, forKey: "touchIdIs")
            self.touchSW.isEnabled = false
            changeBtn.isHidden = true
            touchSW.isHidden = true
            touchLabel.isHidden = true
            touchcontent.isHidden = true
            bottomLine.isHidden = true
            topLine.isHidden = true
            changeLabel.isHidden = true
            changecontent.isHidden = true
            
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let detail = storyboard.instantiateViewController(withIdentifier: "lockPad") as? LockPadController {
                detail.lockis = true
                self.present(detail, animated: true, completion: nil)
            }
        }
        
        plist.synchronize()
        
    }
    
    @IBAction func touchAction(_ sender: Any) {
        let plist = UserDefaults.standard
        
        if touchSW.isOn {
            //새로설정
            print("새로설정")
            plist.set(true, forKey: "touchIdIs")
            
        }else{
            //설정해지
            print("설정해지")
            plist.set(false, forKey: "touchIdIs")
            
        }
        
        plist.synchronize()
        
    }
    
    @IBAction func passwdchangeAction(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let detail = storyboard.instantiateViewController(withIdentifier: "lockPad") as? LockPadController{
            detail.passwdchange = true
            self.present(detail, animated: true, completion: nil)
        }
        
    }

    func intistart(){
        let plist = UserDefaults.standard
        
        if plist.bool(forKey: "appLock"){
            self.lockSW.isOn = true
            self.touchSW.isEnabled = true
        }else {
            self.lockSW.isOn = false
            self.touchSW.isEnabled = false
        }
        
        if plist.bool(forKey: "touchIdIs") && plist.bool(forKey: "appLock") {
            self.touchSW.isOn = true
        }else {
            self.touchSW.isOn = false
        }
        
        if self.lockSW.isOn {
            changeBtn.isHidden = false
            touchSW.isHidden = false
            touchLabel.isHidden = false
            touchcontent.isHidden = false
            bottomLine.isHidden = false
            topLine.isHidden = false
            changeLabel.isHidden = false
            changecontent.isHidden = false
            
        }else {
            changeBtn.isHidden = true
            touchSW.isHidden = true
            touchLabel.isHidden = true
            touchcontent.isHidden = true
            bottomLine.isHidden = true
            topLine.isHidden = true
            changeLabel.isHidden = true
            changecontent.isHidden = true
            
            
        }
    }
    
}
