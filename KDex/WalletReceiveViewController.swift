//
//  WalletReceiveViewController.swift
//  crevolc
//
//  Created by crovolc on 2018. 1. 9..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import LocalAuthentication
import Kingfisher

class WalletReceiveViewController : UIViewController {
    
    var coinsimbol : String?
    
    var coinName = ""
    
    var coinType = ""
    
    var walletPrivatekey : String?
    
    var walletAddress : String?
    
    @IBOutlet var toptitleText: UITextView!
    
    @IBOutlet var walletImg: UIImageView!
    
    @IBOutlet var myaddress: UILabel!
    
    @IBOutlet var QRImage: UIImageView!
    
    @IBOutlet var walletAddr: UILabel!
    
    @IBAction func addressCopyPasteAction(_ sender: Any) {
        
        UIPasteboard.general.string = walletAddr.text
        
        if walletAddr.text != "" {
            showToast(message: "주소가 복사되었습니다", textcolor: UIColor.white, frame: CGRect(x: self.view.frame.size.width/2 - 100, y: self.view.frame.size.height-100, width: 200, height: 35), time: 0.2, animation: true)
        }
        
    }
    
    @IBAction func blockChainNateWorkAct(_ sender: Any) {
        
        print("wallet receive : \(coinsimbol), address : \(walletAddress)")
        
        if coinsimbol! == "BTC" {
            if let url = URL(string: "\(CoinWalletNatwork.BTC.rawValue)\(walletAddress!)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else if coinsimbol! == "ETH" {
            if let url = URL(string: "\(CoinWalletNatwork.ETH.rawValue)\(walletAddress!)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else if coinsimbol! == "BCH" {
            if let url = URL(string: "\(CoinWalletNatwork.BCH.rawValue)\(walletAddress!)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else if coinsimbol! == "LTC" {
            if let url = URL(string: "\(CoinWalletNatwork.LTC.rawValue)\(walletAddress!)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else if coinsimbol! == "ETC" {
            if let url = URL(string: "\(CoinWalletNatwork.ETC.rawValue)\(walletAddress!)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else if coinType == "2" || coinsimbol! == "KDA" || coinsimbol! == "DPN" || coinsimbol! == "FNK" || coinsimbol == "MCVW" || coinsimbol == "BTR" || coinsimbol == "520" || coinsimbol == "EYEAL" || coinsimbol == "METAC" || coinsimbol == "AGO"{
            if let url = URL(string: "https://etherscan.io/address/\(walletAddress!)#tokentxns" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else if coinsimbol! == "TRX" {
            if let url = URL(string: "\(CoinWalletNatwork.TRX.rawValue)\(walletAddress!)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else if coinsimbol! == "XRP" {
            if let url = URL(string: "\(CoinWalletNatwork.XRP.rawValue)\(walletAddress!)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else if coinsimbol! == "QTUM" {
            if let url = URL(string: "\(CoinWalletNatwork.QTUM.rawValue)\(walletAddress!)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else {
            if let url = URL(string: "https://etherscan.io/address/\(walletAddress!)#tokentxns" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNaviTitle()
        
        self.view.backgroundColor = UIColor.DPViewBackgroundColor
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.addShadowToBar(vi: self)
        
        walletInfo()
        
        walletImg.kf.setImage(with: AppDelegate.dataImageUrl(str: "\(coinsimbol ?? "")"))
        
        toptitleText.text = "wallet_member".localized + "\(self.coinsimbol ?? "")" + "wallet_addres_able_deposite".localized
        
    }
    
    func walletInfo() {
        let activity = UIActivityIndicatorView()
        activity.style = .whiteLarge
        activity.color = UIColor.DPActivityColor
        activity.hidesWhenStopped = true
        activity.startAnimating()
        
        var url = ""
        
        if coinType == "2" || coinsimbol == "KDA" || coinsimbol == "DPN" || coinsimbol == "FNK" || coinsimbol == "MCVW" || coinsimbol == "BTR" || coinsimbol == "520" || coinsimbol == "EYEAL" || coinsimbol == "METAC" || coinsimbol == "AGO"{
            url = "\(AppDelegate.url)/auth/walletInfo?simbol=ETH&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)"
        }else {
            url = "\(AppDelegate.url)/auth/walletInfo?simbol=\(coinsimbol!)&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)"
        }
        
        print(url)
        let amo = Alamofire.request(url)
        amo.responseJSON(completionHandler: { response in
            
            print("buy result :\(response.result.value)")
            
            if response.result.isSuccess == true {
                
                var result = response.result.value as? [String : Any]
                
                self.walletAddr.text = (result!["wletAddr"] as? String) ?? ""
                
                AppDelegate.tesetWalletaddr = (result!["wletAddr"] as? String) ?? ""
                
                self.myaddress.text = "wallet_myAddress".localized + " (\(self.coinsimbol!))"
                
                self.QRImage.image = self.generateQRCode(from: self.walletAddr.text!)
                
            }
            activity.stopAnimating()
        })
    }
    
    func generateQRCode(from string: String) -> UIImage? {
        
       // authenticateUser()
        
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
            
        }
        
        return nil
    }
    
    func authenticateUser() {
        print("auth call")
        
        let context = LAContext()
        var error: NSError?
        
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            let reason = "Identify yourself!"
            
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) {
                [unowned self] success, authenticationError in
                
                DispatchQueue.main.async {
                    if success {
                        self.runSecretCode()
                    } else {
                        let ac = UIAlertController(title: "Authentication failed", message: "Sorry!", preferredStyle: .alert)
                        ac.addAction(UIAlertAction(title: "OK", style: .default))
                        self.present(ac, animated: true)
                    }
                }
            }
        } else {
            
            let ac = UIAlertController(title: "app_secure_finger_print", message: "finger_print_no_support_inform".localized, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .default))
            present(ac, animated: true)
        }
    }
    
    func runSecretCode(){
        
    }
    
    @IBAction func addrShareAction(_ sender: Any) {
        let ac = UIAlertController(title: "wallet_address_sharing".localized, message: "pop_Ready".localized, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .default))
        present(ac, animated: true)
        
    }
    
    func setupNaviTitle(){
        let vi = UIView()
        
        let nTitle = UILabel(frame: CGRect(x: 0, y: 0, width: 400, height: 37))
        
        nTitle.textAlignment = .center
        nTitle.font = UIFont.boldSystemFont(ofSize: 15)
        nTitle.textColor = UIColor.DPTitleTextColor
        nTitle.text = "\(self.coinName)"
        nTitle.textAlignment = .left
        
        vi.addSubview(nTitle)
        
        self.navigationItem.titleView = vi
        
        self.navigationItem.titleView?.frame.size = CGSize(width: 400, height: 37)
        
        self.navigationController?.navigationBar.barTintColor =  UIColor.DPNaviBarTintColor
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = ""
        
    }
}
