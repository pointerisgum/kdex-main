//
//  NTradInfo.swift
//  socketText
//
//  Created by park heewon on 2018. 3. 23..
//  Copyright © 2018년 park heewon. All rights reserved.
//

import Foundation

class NTradInfo: NSObject {
    
    public var data_tp           = NString(size: 6)    // µ¥ÀÌÅÍ±¸ºÐ
    public var user_id           = NString(size: 8)    // »ç¿ëÀÚID
    public var acct_no           = NString(size:15)    // °èÁÂ¹øÈ£
    public var ordr_dt           = NString(size: 8)    // ÁÖ¹®ÀÏÀÚ
    public var ordr_no           = NString(size:20)    // ÁÖ¹®¹øÈ£
    public var trad_dt           = NString(size: 8)    // Ã¼°áÀÏÀÚ
    public var trad_no           = NString(size:20)    // Ã¼°á¹øÈ£
    public var inst_cd           = NString(size:32)    // Á¾¸ñÄÚµå
    public var bysl_tp           = NString(size: 1)    // ¸Å¼ö¸Åµµ
    public var ordr_qty          = NString(size:15)    // ÁÖ¹®¼ö·®
    public var trad_qty          = NString(size:15)    // Ã¼°á¼ö·®
    public var trad_prc          = NString(size:15)    // Ã¼°á°¡°Ý
    public var lqdn_prc          = NString(size:15)    // ÁøÀÔ°¡°Ý
    public var commission        = NString(size:15)    // ¼ö¼ö·á
    public var lqdn_pl_amt       = NString(size:15)    // ¼ÕÀÍ
    public var exra              = NString(size:15)    // È¯À²
    public var ordr_dtm          = NString(size:20)    // ÁÖ¹®½Ã°¢
    public var trad_dtm          = NString(size:20)    // Ã¼°á½Ã°¢
    public var stat_tp           = NString(size: 1)    // »óÅÂ±¸ºÐ                 1.Pending 2.Confirmed 3.Reject 4.Cancel
    public var crc_cd            = NString(size:10)    // ÅëÈ­ÄÚµå
    public var ordr_tp           = NString(size: 1)    // ÁÖ¹®±¸ºÐ
    public var prce_tp           = NString(size: 1)    // °¡°ÝÁ¶°Ç
    public var prc_prsn          = NString(size: 5)    // °¡°Ý
    public var qty_prsn          = NString(size: 5)    // ¼ö·®
    public var tx_status         = NString(size: 1)    // TX_STATUS                1.¿Ï·á  2.ÁøÇà  3.´ë±â  9.Àü¼Û½ÇÆÐ
    public var tx_hash           = NString(size:120)    // TX_HASH
    
    public var strBuffer = ""
    
    override init() {
        super.init()
        
    }
    
    public func reset() {
        data_tp           .reset()
        user_id           .reset()
        acct_no           .reset()
        ordr_dt           .reset()
        ordr_no           .reset()
        trad_dt           .reset()
        trad_no           .reset()
        inst_cd           .reset()
        bysl_tp           .reset()
        ordr_qty          .reset()
        trad_qty          .reset()
        trad_prc          .reset()
        lqdn_prc          .reset()
        commission        .reset()
        lqdn_pl_amt       .reset()
        exra              .reset()
        ordr_dtm          .reset()
        trad_dtm          .reset()
        stat_tp           .reset()
        crc_cd            .reset()
        ordr_tp           .reset()
        prce_tp           .reset()
        prc_prsn          .reset()
        qty_prsn          .reset()
        tx_status         .reset()
        tx_hash           .reset()
    }
    
    public func parseData(bt: [UInt8]){
        
        var msgLen = 0
        
        var tempArr = [UInt8](repeating: 0x00, count: data_tp.getSize())
        var temp = [UInt8]()
        
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: data_tp.getSize())
        msgLen = msgLen + data_tp.getSize()
        data_tp.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: user_id.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: user_id.getSize())
        msgLen = msgLen + user_id.getSize()
        user_id.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: acct_no.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: acct_no.getSize())
        msgLen = msgLen + acct_no.getSize()
        acct_no.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ordr_dt.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ordr_dt.getSize())
        msgLen = msgLen + ordr_dt.getSize()
        ordr_dt.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ordr_no.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ordr_no.getSize())
        msgLen = msgLen + ordr_no.getSize()
        ordr_no.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: trad_dt.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: trad_dt.getSize())
        msgLen = msgLen + trad_dt.getSize()
        trad_dt.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: trad_no.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: trad_no.getSize())
        msgLen = msgLen + trad_no.getSize()
        trad_no.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: inst_cd.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: inst_cd.getSize())
        msgLen = msgLen + inst_cd.getSize()
        inst_cd.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: bysl_tp.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: bysl_tp.getSize())
        msgLen = msgLen + bysl_tp.getSize()
        bysl_tp.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ordr_qty.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ordr_qty.getSize())
        msgLen = msgLen + ordr_qty.getSize()
        ordr_qty.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: trad_qty.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: trad_qty.getSize())
        msgLen = msgLen + trad_qty.getSize()
        trad_qty.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: trad_prc.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: trad_prc.getSize())
        msgLen = msgLen + trad_prc.getSize()
        trad_prc.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: lqdn_prc.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: lqdn_prc.getSize())
        msgLen = msgLen + lqdn_prc.getSize()
        lqdn_prc.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: commission.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: commission.getSize())
        msgLen = msgLen + commission.getSize()
        commission.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: lqdn_pl_amt.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: lqdn_pl_amt.getSize())
        msgLen = msgLen + lqdn_pl_amt.getSize()
        lqdn_pl_amt.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: exra.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: exra.getSize())
        msgLen = msgLen + exra.getSize()
        exra.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ordr_dtm.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ordr_dtm.getSize())
        msgLen = msgLen + ordr_dtm.getSize()
        ordr_dtm.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: trad_dtm.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: trad_dtm.getSize())
        msgLen = msgLen + trad_dtm.getSize()
        trad_dtm.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: stat_tp.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: stat_tp.getSize())
        msgLen = msgLen + stat_tp.getSize()
        stat_tp.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: crc_cd.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: crc_cd.getSize())
        msgLen = msgLen + crc_cd.getSize()
        crc_cd.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ordr_tp.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ordr_tp.getSize())
        msgLen = msgLen + ordr_tp.getSize()
        ordr_tp.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: prce_tp.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: prce_tp.getSize())
        msgLen = msgLen + prce_tp.getSize()
        ordr_tp.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: prc_prsn.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: prc_prsn.getSize())
        msgLen = msgLen + prc_prsn.getSize()
        prc_prsn.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: qty_prsn.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: qty_prsn.getSize())
        msgLen = msgLen + qty_prsn.getSize()
        qty_prsn.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: tx_status.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: tx_status.getSize())
        msgLen = msgLen + tx_status.getSize()
        tx_status.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: tx_hash.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: tx_hash.getSize())
        msgLen = msgLen + tx_hash.getSize()
        tx_hash.setValue(byteArray: temp)
    }
    
    public func toString() -> String {
        return ( data_tp     .toString()  +
            user_id     .toString()  +
            acct_no     .toString()  +
            ordr_dt     .toString()  +
            ordr_no     .toString()  +
            trad_dt     .toString()  +
            trad_no     .toString()  +
            inst_cd     .toString()  +
            bysl_tp     .toString()  +
            ordr_qty    .toString()  +
            trad_qty    .toString()  +
            trad_prc    .toString()  +
            lqdn_prc    .toString()  +
            commission  .toString()  +
            lqdn_pl_amt .toString()  +
            exra        .toString()  +
            ordr_dtm    .toString()  +
            trad_dtm    .toString()  +
            stat_tp     .toString()  +
            crc_cd      .toString()  +
            ordr_tp     .toString()  +
            prce_tp     .toString()  +
            prc_prsn    .toString()  +
            qty_prsn    .toString()  +
            tx_status   .toString()  +
            tx_hash     .toString()  )
    }
    
    
    private func writeObject(out: OutputStream){
        // out.write(<#T##buffer: UnsafePointer<UInt8>##UnsafePointer<UInt8>#>, maxLength: <#T##Int#>)
        
    }
    
    
    public func ToBytes() -> [UInt8] {
        
        strBuffer = data_tp     .toString()  +
            user_id     .toString()  +
            acct_no     .toString()  +
            ordr_dt     .toString()  +
            ordr_no     .toString()  +
            trad_dt     .toString()  +
            trad_no     .toString()  +
            inst_cd     .toString()  +
            bysl_tp     .toString()  +
            ordr_qty    .toString()  +
            trad_qty    .toString()  +
            trad_prc    .toString()  +
            lqdn_prc    .toString()  +
            commission  .toString()  +
            lqdn_pl_amt .toString()  +
            exra        .toString()  +
            ordr_dtm    .toString()  +
            trad_dtm    .toString()  +
            stat_tp     .toString()  +
            crc_cd      .toString()  +
            ordr_tp     .toString()  +
            prce_tp     .toString()  +
            prc_prsn    .toString()  +
            qty_prsn    .toString()  +
            tx_status   .toString()  +
            tx_hash     .toString()
        
        let buf: [UInt8] = Array(strBuffer.utf8)
        
        return buf
    }
    
    
    public func toJSON() -> String {
        
        return ( data_tp     .toString()  +
            user_id     .toString()  +
            acct_no     .toString()  +
            ordr_dt     .toString()  +
            ordr_no     .toString()  +
            trad_dt     .toString()  +
            trad_no     .toString()  +
            inst_cd     .toString()  +
            bysl_tp     .toString()  +
            ordr_qty    .toString()  +
            trad_qty    .toString()  +
            trad_prc    .toString()  +
            lqdn_prc    .toString()  +
            commission  .toString()  +
            lqdn_pl_amt .toString()  +
            exra        .toString()  +
            ordr_dtm    .toString()  +
            trad_dtm    .toString()  +
            stat_tp     .toString()  +
            crc_cd      .toString()  +
            ordr_tp     .toString()  +
            prce_tp     .toString()  +
            prc_prsn    .toString()  +
            qty_prsn    .toString()  +
            tx_status   .toString()  +
            tx_hash     .toString()  )
        
    }
    
    func arrayCopy(arr: [UInt8], start: Int, arr2: [UInt8], index: Int, len: Int) -> [UInt8]{
        var i = index
        var temp = arr2
        let tempLen = start + len
        for row in start..<tempLen {
            temp[i] = arr[row]
            i = i + 1
        }
        print(temp)
        return temp
    }
}
