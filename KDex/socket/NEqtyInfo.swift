//
//  NEqtyInfo.swift
//  DEXnPAY
//
//  Created by park heewon on 2018. 3. 26..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation

class NEqtyInfo: NSObject {
    
    public var data_tp           = NString(size:  6 )     // 데이터구분
    public var user_id           = NString(size:  8 )     // 사용자ID
    public var acct_no           = NString(size: 15 )     // Account
    public var crc_cd            = NString(size: 10 )     // Currency
    public var crc_tp            = NString(size:  1 )     // 1.Base 2.Currency
    public var receivables       = NString(size: 15 )     // Receivables
    public var multiplier        = NString(size: 15 )     // Multiplier
    public var balance           = NString(size: 15 )     // Balance
    public var deposit           = NString(size: 15 )     // Deposit
    public var withdraw          = NString(size: 15 )     // Withdraw
    public var profitloss        = NString(size: 15 )     // P/L
    public var commistion        = NString(size: 15 )     // Fee
    public var others            = NString(size: 15 )     // Others
    public var realized          = NString(size: 15 )     // Realized P/L
    public var yield             = NString(size: 15 )     // Yield
    public var buy_amount        = NString(size: 15 )     // Buyed amount
    public var market_value      = NString(size: 15 )     // Market Value
    public var unrealized        = NString(size: 15 )     // UnRealized P/L
    public var total_equity      = NString(size: 15 )     // TE
    public var today_deposit     = NString(size: 15 )     // Today deposit
    public var today_withdraw    = NString(size: 15 )     // Today withdraw
    public var accum_deposit     = NString(size: 15 )     // Accumulated deposit
    public var accum_withdraw    = NString(size: 15 )     // Accumulated withdraw
    public var today_profitloss  = NString(size: 15 )     // Today P/L
    public var today_commistion  = NString(size: 15 )     // Today Fee
    public var today_others      = NString(size: 15 )     // Today Others
    public var accum_profitloss  = NString(size: 15 )     // Accumulated P/L
    public var accum_commistion  = NString(size: 15 )     // Accumulated Fee
    public var accum_others      = NString(size: 15 )     // Accumulated Others
    public var crc_dp            = NString(size:  5 )     // Currency Decimal point
    public var used_margin       = NString(size: 15 )     // 사용증거금
    public var free_margin       = NString(size: 15 )     // 여유증거금
    public var ordr_able_amt     = NString(size: 15 )     // 주문가능금액
    public var wthr_able_amt     = NString(size: 15 )     // 출금가능금액
    
    
    public var strBuffer = ""
    
    public func reset(){
        data_tp           .reset() ;
        user_id           .reset() ;
        acct_no           .reset() ;
        crc_cd            .reset() ;
        crc_tp            .reset() ;
        receivables       .reset() ;
        multiplier        .reset() ;
        balance           .reset() ;
        deposit           .reset() ;
        withdraw          .reset() ;
        profitloss        .reset() ;
        commistion        .reset() ;
        others            .reset() ;
        realized          .reset() ;
        yield             .reset() ;
        buy_amount        .reset() ;
        market_value      .reset() ;
        unrealized        .reset() ;
        total_equity      .reset() ;
        today_deposit     .reset() ;
        today_withdraw    .reset() ;
        accum_deposit     .reset() ;
        accum_withdraw    .reset() ;
        today_profitloss  .reset() ;
        today_commistion  .reset() ;
        today_others      .reset() ;
        accum_profitloss  .reset() ;
        accum_commistion  .reset() ;
        accum_others      .reset() ;
        crc_dp            .reset() ;
        used_margin       .reset() ;
        free_margin       .reset() ;
        ordr_able_amt     .reset() ;
        wthr_able_amt     .reset() ;
    }
    
    public func parseData(bt: [UInt8]){
        var msgLen = 0
        
        var tempArr = [UInt8](repeating: 0x00, count: data_tp.getSize())
        var temp = [UInt8]()
        
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: data_tp.getSize())
        msgLen = msgLen + data_tp.getSize()
        data_tp.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: user_id.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: user_id.getSize())
        msgLen = msgLen + user_id.getSize()
        user_id.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: acct_no.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: acct_no.getSize())
        msgLen = msgLen + acct_no.getSize()
        acct_no.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: crc_cd.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: crc_cd.getSize())
        msgLen = msgLen + crc_cd.getSize()
        crc_cd.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: crc_tp.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: crc_tp.getSize())
        msgLen = msgLen + crc_tp.getSize()
        crc_tp.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: receivables.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: receivables.getSize())
        msgLen = msgLen + receivables.getSize()
        receivables.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: multiplier.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: multiplier.getSize())
        msgLen = msgLen + multiplier.getSize()
        multiplier.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: balance.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: balance.getSize())
        msgLen = msgLen + balance.getSize()
        balance.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: deposit.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: deposit.getSize())
        msgLen = msgLen + deposit.getSize()
        deposit.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: withdraw.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: withdraw.getSize())
        msgLen = msgLen + withdraw.getSize()
        withdraw.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: profitloss.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: profitloss.getSize())
        msgLen = msgLen + profitloss.getSize()
        profitloss.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: commistion.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: commistion.getSize())
        msgLen = msgLen + commistion.getSize()
        commistion.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: others.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: others.getSize())
        msgLen = msgLen + others.getSize()
        others.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: realized.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: realized.getSize())
        msgLen = msgLen + realized.getSize()
        realized.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: yield.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: yield.getSize())
        msgLen = msgLen + yield.getSize()
        yield.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: buy_amount.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: buy_amount.getSize())
        msgLen = msgLen + buy_amount.getSize()
        buy_amount.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: market_value.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: market_value.getSize())
        msgLen = msgLen + market_value.getSize()
        market_value.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: unrealized.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: unrealized.getSize())
        msgLen = msgLen + unrealized.getSize()
        unrealized.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: total_equity.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: total_equity.getSize())
        msgLen = msgLen + total_equity.getSize()
        total_equity.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: today_deposit.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: today_deposit.getSize())
        msgLen = msgLen + today_deposit.getSize()
        today_deposit.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: today_withdraw.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: today_withdraw.getSize())
        msgLen = msgLen + today_withdraw.getSize()
        today_withdraw.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: accum_deposit.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: accum_deposit.getSize())
        msgLen = msgLen + accum_deposit.getSize()
        accum_deposit.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: accum_withdraw.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: accum_withdraw.getSize())
        msgLen = msgLen + accum_withdraw.getSize()
        accum_withdraw.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: today_profitloss.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: today_profitloss.getSize())
        msgLen = msgLen + today_profitloss.getSize()
        today_profitloss.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: today_commistion.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: today_commistion.getSize())
        msgLen = msgLen + today_commistion.getSize()
        today_commistion.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: today_others.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: today_others.getSize())
        msgLen = msgLen + today_others.getSize()
        today_others.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: accum_profitloss.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: accum_profitloss.getSize())
        msgLen = msgLen + accum_profitloss.getSize()
        accum_profitloss.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: accum_commistion.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: accum_commistion.getSize())
        msgLen = msgLen + accum_commistion.getSize()
        accum_commistion.setValue(byteArray: temp)
  
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: accum_others.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: accum_others.getSize())
        msgLen = msgLen + accum_others.getSize()
        accum_others.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: crc_dp.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: crc_dp.getSize())
        msgLen = msgLen + crc_dp.getSize()
        crc_dp.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: used_margin.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: used_margin.getSize())
        msgLen = msgLen + used_margin.getSize()
        used_margin.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: free_margin.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: free_margin.getSize())
        msgLen = msgLen + free_margin.getSize()
        free_margin.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ordr_able_amt.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ordr_able_amt.getSize())
        msgLen = msgLen + ordr_able_amt.getSize()
        ordr_able_amt.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: wthr_able_amt.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: wthr_able_amt.getSize())
        msgLen = msgLen + wthr_able_amt.getSize()
        wthr_able_amt.setValue(byteArray: temp)

    }
    
    func arrayCopy(arr: [UInt8], start: Int, arr2: [UInt8], index: Int, len: Int) -> [UInt8]{
            var i = index
            var temp = arr2
            let tempLen = start + len
            for row in start..<tempLen {
                temp[i] = arr[row]
                i = i + 1
            }
            print(temp)
            return temp
    }
    
    public func toString() -> String {
        return ( data_tp          .toString() +
        user_id          .toString() +
        acct_no          .toString() +
        crc_cd           .toString() +
        crc_tp           .toString() +
        receivables      .toString() +
        multiplier       .toString() +
        balance          .toString() +
        deposit          .toString() +
        withdraw         .toString() +
        profitloss       .toString() +
        commistion       .toString() +
        others           .toString() +
        realized         .toString() +
        yield            .toString() +
        buy_amount       .toString() +
        market_value     .toString() +
        unrealized       .toString() +
        total_equity     .toString() +
        today_deposit    .toString() +
        today_withdraw   .toString() +
        accum_deposit    .toString() +
        accum_withdraw   .toString() +
        today_profitloss .toString() +
        today_commistion .toString() +
        today_others     .toString() +
        accum_profitloss .toString() +
        accum_commistion .toString() +
        accum_others     .toString() +
        crc_dp           .toString() +
        used_margin      .toString() +
        free_margin      .toString() +
        ordr_able_amt    .toString() +
        wthr_able_amt    .toString() )
    
    }
    
    public func ToBytes() -> [UInt8] {
        strBuffer = data_tp          .toString() +
        user_id          .toString() +
        acct_no          .toString() +
        crc_cd           .toString() +
        crc_tp           .toString() +
        receivables      .toString() +
        multiplier       .toString() +
        balance          .toString() +
        deposit          .toString() +
        withdraw         .toString() +
        profitloss       .toString() +
        commistion       .toString() +
        others           .toString() +
        realized         .toString() +
        yield            .toString() +
        buy_amount       .toString() +
        market_value     .toString() +
        unrealized       .toString() +
        total_equity     .toString() +
        today_deposit    .toString() +
        today_withdraw   .toString() +
        accum_deposit    .toString() +
        accum_withdraw   .toString() +
        today_profitloss .toString() +
        today_commistion .toString() +
        today_others     .toString() +
        accum_profitloss .toString() +
        accum_commistion .toString() +
        accum_others     .toString() +
        crc_dp           .toString() +
        used_margin      .toString() +
        free_margin      .toString() +
        ordr_able_amt    .toString() +
        wthr_able_amt    .toString() ;
        
        let buf: [UInt8] = Array(strBuffer.utf8)
        
        return buf
    }
    
    private func writeObject(out: OutputStream){
        // out.write(<#T##buffer: UnsafePointer<UInt8>##UnsafePointer<UInt8>#>, maxLength: <#T##Int#>)
        
    }
    
    public func toJSON() -> String {
        return ( data_tp          .toString() +
        user_id          .toString() +
        acct_no          .toString() +
        crc_cd           .toString() +
        crc_tp           .toString() +
        receivables      .toString() +
        multiplier       .toString() +
        balance          .toString() +
        deposit          .toString() +
        withdraw         .toString() +
        profitloss       .toString() +
        commistion       .toString() +
        others           .toString() +
        realized         .toString() +
        yield            .toString() +
        buy_amount       .toString() +
        market_value     .toString() +
        unrealized       .toString() +
        total_equity     .toString() +
        today_deposit    .toString() +
        today_withdraw   .toString() +
        accum_deposit    .toString() +
        accum_withdraw   .toString() +
        today_profitloss .toString() +
        today_commistion .toString() +
        today_others     .toString() +
        accum_profitloss .toString() +
        accum_commistion .toString() +
        accum_others     .toString() +
        crc_dp           .toString() +
        used_margin      .toString() +
        free_margin      .toString() +
        ordr_able_amt    .toString() +
        wthr_able_amt    .toString() )
    }
    
    
}
