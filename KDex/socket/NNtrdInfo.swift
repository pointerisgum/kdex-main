//
//  NNtrdInfo.swift
//  socketText
//
//  Created by park heewon on 2018. 3. 23..
//  Copyright © 2018년 park heewon. All rights reserved.
//

import Foundation

class NNtrdInfo: NSObject {
    
    public var data_tp           = NString(size: 6)    // µ¥ÀÌÅÍ±¸ºÐ
   public var user_id           = NString(size: 8)    // »ç¿ëÀÚID
   public var acct_no           = NString(size: 15)    // °èÁÂ¹øÈ£
   public var ordr_dt           = NString(size: 8)    // ÁÖ¹®ÀÏÀÚ
   public var ordr_no           = NString(size: 20)    // ÁÖ¹®¹øÈ£
   public var orig_ordr_no      = NString(size: 20)    // ¿øÁÖ¹®¹øÈ£
   public var mthr_ordr_no      = NString(size: 20)    // ¸ðÁÖ¹®¹øÈ£
   public var inst_cd           = NString(size: 32)    // Á¾¸ñÄÚµå
   public var mrkt_cd           = NString(size: 10)    // °Å·¡¼ÒÄÚµå
   public var bysl_tp           = NString(size: 1)    // ¸Å¼ö¸Åµµ
   public var ordr_qty          = NString(size: 15)    // ÁÖ¹®¼ö·®
   public var mdfy_qty          = NString(size: 15)    // Á¤Á¤¼ö·®
   public var cncl_qty          = NString(size: 15)    // Ãë¼Ò¼ö·®
   public var trad_qty          = NString(size: 15)    // Ã¼°á¼ö·®
   public var remn_qty          = NString(size: 15)    // ÁÖ¹®ÀÜ·®
   public var ordr_prc          = NString(size: 15)    // ÁÖ¹®°¡°Ý
   public var tick_cnt          = NString(size: 5)    // Æ½¼ö
   public var cond_prc          = NString(size: 15)    // Á¶°Ç°¡°Ý
   public var akprc_tp          = NString(size: 1)    // È£°¡±¸ºÐ
   public var ordr_tp           = NString(size: 1)    // ÁÖ¹®±¸ºÐ
   public var trad_cond         = NString(size: 1)    // Ã¼°áÁ¶°Ç
   public var prce_tp           = NString(size: 1)    // °¡°ÝÁ¶°Ç(size:1.½ÃÀå°¡ 2.ÁöÁ¤°¡ 3.STOP 4.STOP LIMIT)
   public var aval_tp           = NString(size: 1)    // GTC/DAY
   public var acpt_tp           = NString(size: 1)    // Á¢¼ö±¸ºÐ
   public var ordr_dtm          = NString(size: 20)    // ÁÖ¹®½Ã°£
   public var crc_cd            = NString(size: 10)    // ÅëÈ­ÄÚµå
   public var prc_prsn          = NString(size: 5)    // °¡°Ý
   public var qty_prsn          = NString(size: 5)    // ¼ö·®
    
    public var strBuffer = ""
    
    override init() {
        super.init()
    }
    
    public func Reset() {
    data_tp.reset()
    user_id.reset()
    acct_no.reset()
    ordr_dt.reset()
    ordr_no.reset()
    orig_ordr_no.reset()
    mthr_ordr_no.reset()
    inst_cd.reset()
    mrkt_cd.reset()
    bysl_tp.reset()
    ordr_qty.reset()
    mdfy_qty.reset()
    cncl_qty.reset()
    trad_qty.reset()
    remn_qty.reset()
    ordr_prc.reset()
    tick_cnt.reset()
    cond_prc.reset()
    akprc_tp.reset()
    ordr_tp.reset()
    trad_cond.reset()
    prce_tp.reset()
    aval_tp.reset()
    acpt_tp.reset()
    ordr_dtm.reset()
    crc_cd.reset()
    prc_prsn.reset()
    qty_prsn.reset()
        
    }
    
    
    public func parseData(bt: [UInt8]){
        var msgLen = 0
        
        var tempArr = [UInt8](repeating: 0x00, count: data_tp.getSize())
        var temp = [UInt8]()
        
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: data_tp.getSize())
        msgLen = msgLen + data_tp.getSize()
        data_tp.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: user_id.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: user_id.getSize())
        msgLen = msgLen + user_id.getSize()
        user_id.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: acct_no.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: acct_no.getSize())
        msgLen = msgLen + acct_no.getSize()
        acct_no.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ordr_no.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ordr_no.getSize())
        msgLen = msgLen + ordr_no.getSize()
        ordr_no.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: orig_ordr_no.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: orig_ordr_no.getSize())
        msgLen = msgLen + orig_ordr_no.getSize()
        orig_ordr_no.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: mthr_ordr_no.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: mthr_ordr_no.getSize())
        msgLen = msgLen + mthr_ordr_no.getSize()
        mthr_ordr_no.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: inst_cd.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: inst_cd.getSize())
        msgLen = msgLen + inst_cd.getSize()
        inst_cd.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: mrkt_cd.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: mrkt_cd.getSize())
        msgLen = msgLen + mrkt_cd.getSize()
        mrkt_cd.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: bysl_tp.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: bysl_tp.getSize())
        msgLen = msgLen + bysl_tp.getSize()
        bysl_tp.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ordr_qty.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ordr_qty.getSize())
        msgLen = msgLen + ordr_qty.getSize()
        ordr_qty.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: mdfy_qty.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: mdfy_qty.getSize())
        msgLen = msgLen + mdfy_qty.getSize()
        mdfy_qty.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: cncl_qty.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: cncl_qty.getSize())
        msgLen = msgLen + cncl_qty.getSize()
        cncl_qty.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: trad_qty.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: trad_qty.getSize())
        msgLen = msgLen + trad_qty.getSize()
        trad_qty.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ordr_prc.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ordr_prc.getSize())
        msgLen = msgLen + ordr_prc.getSize()
        ordr_prc.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: tick_cnt.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: tick_cnt.getSize())
        msgLen = msgLen + tick_cnt.getSize()
        tick_cnt.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: cond_prc.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: cond_prc.getSize())
        msgLen = msgLen + cond_prc.getSize()
        cond_prc.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: akprc_tp.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: akprc_tp.getSize())
        msgLen = msgLen + akprc_tp.getSize()
        akprc_tp.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ordr_tp.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ordr_tp.getSize())
        msgLen = msgLen + ordr_tp.getSize()
        ordr_tp.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: trad_cond.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: trad_cond.getSize())
        msgLen = msgLen + trad_cond.getSize()
        trad_cond.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: prce_tp.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: prce_tp.getSize())
        msgLen = msgLen + prce_tp.getSize()
        prce_tp.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: aval_tp.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: aval_tp.getSize())
        msgLen = msgLen + aval_tp.getSize()
        aval_tp.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: acpt_tp.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: acpt_tp.getSize())
        msgLen = msgLen + acpt_tp.getSize()
        acpt_tp.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ordr_dtm.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ordr_dtm.getSize())
        msgLen = msgLen + ordr_dtm.getSize()
        ordr_dtm.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: crc_cd.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: crc_cd.getSize())
        msgLen = msgLen + crc_cd.getSize()
        crc_cd.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: prc_prsn.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: prc_prsn.getSize())
        msgLen = msgLen + prc_prsn.getSize()
        prc_prsn.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: qty_prsn.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: qty_prsn.getSize())
        msgLen = msgLen + qty_prsn.getSize()
        qty_prsn.setValue(byteArray: temp)
        
    }
    
    public func toString() -> String {
        return (data_tp.toString()  +
        user_id.toString()  +
        acct_no.toString()  +
        ordr_dt.toString()  +
        ordr_no.toString()  +
        orig_ordr_no.toString()  +
        mthr_ordr_no.toString()  +
        inst_cd.toString()  +
        mrkt_cd.toString()  +
        bysl_tp.toString()  +
        ordr_qty.toString()  +
        mdfy_qty.toString()  +
        cncl_qty.toString()  +
        trad_qty.toString()  +
        remn_qty.toString()  +
        ordr_prc.toString()  +
        tick_cnt.toString()  +
        cond_prc.toString()  +
        akprc_tp.toString()  +
        ordr_tp.toString()  +
        trad_cond.toString()  +
        prce_tp.toString()  +
        aval_tp.toString()  +
        acpt_tp.toString()  +
        ordr_dtm.toString()  +
        crc_cd .toString()  +
        prc_prsn.toString()  +
        qty_prsn.toString())
    }
    
    public func ToBytes() -> [UInt8] {
        strBuffer = data_tp       .toString()  +
        user_id       .toString()  +
        acct_no       .toString()  +
        ordr_dt       .toString()  +
        ordr_no       .toString()  +
        orig_ordr_no  .toString()  +
        mthr_ordr_no  .toString()  +
        inst_cd       .toString()  +
        mrkt_cd       .toString()  +
        bysl_tp       .toString()  +
        ordr_qty      .toString()  +
        mdfy_qty      .toString()  +
        cncl_qty      .toString()  +
        trad_qty      .toString()  +
        remn_qty      .toString()  +
        ordr_prc      .toString()  +
        tick_cnt      .toString()  +
        cond_prc      .toString()  +
        akprc_tp      .toString()  +
        ordr_tp       .toString()  +
        trad_cond     .toString()  +
        prce_tp       .toString()  +
        aval_tp       .toString()  +
        acpt_tp       .toString()  +
        ordr_dtm      .toString()  +
        crc_cd        .toString()  +
        prc_prsn      .toString()  +
        qty_prsn      .toString()
        
    let buf: [UInt8] = Array(strBuffer.utf8)
    
    return buf
    }
    
    private func writeObject(out: OutputStream){
        // out.write(<#T##buffer: UnsafePointer<UInt8>##UnsafePointer<UInt8>#>, maxLength: <#T##Int#>)
        
    }
    
    public func toJSON() -> String {
        
        return ( data_tp       .toString()  +
        user_id       .toString()  +
        acct_no       .toString()  +
        ordr_dt       .toString()  +
        ordr_no       .toString()  +
        orig_ordr_no  .toString()  +
        mthr_ordr_no  .toString()  +
        inst_cd       .toString()  +
        mrkt_cd       .toString()  +
        bysl_tp       .toString()  +
        ordr_qty      .toString()  +
        mdfy_qty      .toString()  +
        cncl_qty      .toString()  +
        trad_qty      .toString()  +
        remn_qty      .toString()  +
        ordr_prc      .toString()  +
        tick_cnt      .toString()  +
        cond_prc      .toString()  +
        akprc_tp      .toString()  +
        ordr_tp       .toString()  +
        trad_cond     .toString()  +
        prce_tp       .toString()  +
        aval_tp       .toString()  +
        acpt_tp       .toString()  +
        ordr_dtm      .toString()  +
        crc_cd        .toString()  +
        prc_prsn      .toString()  +
        qty_prsn      .toString())
        
    }
    
    func arrayCopy(arr: [UInt8], start: Int, arr2: [UInt8], index: Int, len: Int) -> [UInt8]{
        var i = index
        var temp = arr2
        let tempLen = start + len
        for row in start..<tempLen {
            temp[i] = arr[row]
            i = i + 1
        }
       // print(temp)
        return temp
    }
    
}
