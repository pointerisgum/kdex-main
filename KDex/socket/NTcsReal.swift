//
//  NTcsReal.swift
//  DEXnPAY
//
//  Created by park heewon on 2018. 3. 26..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation

class NTcsReal: NSObject {
    
    var socket : TcpSocket?
    var pkt : NPacket = NPacket()
    
    
    public func subscribeRealSise(uid : String, symbol: String, outputStream: OutputStream){
        
        print("subscribeRealSise")
        
        let tr_tp = NString(size: 1)
        let tr_cd = NString(size: 6)
        let srs_cd = NString(size: 32)
        
        tr_tp.setValue(str: "1")
        tr_cd.setValue(str: "R_TICK")
        srs_cd.setValue(str: "\(symbol)")
        
        let str = tr_tp.toString() + tr_cd.toString() + srs_cd.toString()
        print("str : \(str)")
        
        pkt.makePacket(service: "MI0020", sessid: "00000000000000000000", uid: uid, input: str, segnbr: 0, segkey: 0)
        
        let strMsg = pkt.toString()
        print("strMsg : \(strMsg)")
        
        guard let data = strMsg.data(using: String.Encoding.utf8, allowLossyConversion: true) else{
            return
        }
        
        let datacount = data.withUnsafeBytes { outputStream.write($0, maxLength: data.count) }
        
        if datacount != 0 {
            print("\(symbol) success~~!!")
        }else{
            print("fail~~~!")
        }
    }
    
    public func unsubscribeRealSise(uid : String, symbol: String, outputStream: OutputStream){
        
        let tr_tp = NString(size: 1)
        let tr_cd = NString(size: 6)
        let srs_cd = NString(size: 32)
        
        tr_tp.setValue(str: "0")
        tr_cd.setValue(str: "R_TICK")
        srs_cd.setValue(str: symbol)
        
        let str = tr_tp.toString() + tr_cd.toString() + srs_cd.toString()
        
        pkt.makePacket(service: "MI0020", sessid: "00000000000000000000", uid: uid, input: str, segnbr: 0, segkey: 0)
        
        let strMsg = pkt.toString()
        
        guard let data = strMsg.data(using: String.Encoding.utf8, allowLossyConversion: true) else{
            return
        }
        
        let datacount = data.withUnsafeBytes { outputStream.write($0, maxLength: data.count) }
        
        if datacount != 0 {
            print("\(symbol) unsubscribeRealSise success~~!!")
        }else{
            print("unsubscribeRealSise fail~~~!")
        }
        
    }
    
    public func subscribeRealAcct(uid : String, account: String, outputStream: OutputStream){
        
        let tr_tp = NString(size: 1)
        let acct_no = NString(size: 15)
        
        
        tr_tp.setValue(str: "1")
        acct_no.setValue(str: account)
        
        let str = tr_tp.toString() + acct_no.toString()
        
        pkt.makePacket(service: "MI0030", sessid: "00000000000000000000", uid: uid, input: str, segnbr: 0, segkey: 0)
        
        let strMsg = pkt.toString()
        
        guard let data = strMsg.data(using: String.Encoding.utf8, allowLossyConversion: true) else{
            return
        }
        
        let datacount = data.withUnsafeBytes { outputStream.write($0, maxLength: data.count) }
        
        if datacount != 0 {
            print("success~~!!")
        }else{
            print("fail~~~!")
        }
        
    }
    
    public func unsubscribeRealAcct(uid : String, account: String, outputStream: OutputStream){
        
        let tr_tp = NString(size: 1)
        let acct_no = NString(size: 15)
        
        
        tr_tp.setValue(str: "0")
        acct_no.setValue(str: account)
        
        let str = tr_tp.toString() + acct_no.toString()
        
        pkt.makePacket(service: "MI0030", sessid: "00000000000000000000", uid: uid, input: str, segnbr: 0, segkey: 0)
        
        let strMsg = pkt.toString()
        
        guard let data = strMsg.data(using: String.Encoding.utf8, allowLossyConversion: true) else{
            return
        }
        
        let datacount = data.withUnsafeBytes { outputStream.write($0, maxLength: data.count) }
        
        if datacount != 0 {
            print("unsubscribeRealAcct success~~!!")
        }else{
            print("fail~~~!")
        }
        
    }
    
}
