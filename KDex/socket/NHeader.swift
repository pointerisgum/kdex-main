//
//  NHeader.swift
//  DEXnPAY
//
//  Created by park heewon on 2018. 3. 21..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation

class NHeader: NSObject {
    
    public var tmslen = ""
    public var tmstr = ""
    public var tmsservice = ""
    public var tmsclid = ""
    public var tmscd = ""
    public var tmserrcd = ""
    public var tmssreserved = ""
    public var tmscreserved = ""
    public var len = ""
    public var zip = ""
    public var uid = ""
    public var clid = ""
    public var tr = ""
    public var ctype = ""
    public var dtype = ""
    public var srvtp = ""
    public var errtp = ""
    public var errcd = ""
    public var ctinfo = ""
    public var svrinfo = ""
    public var seg_nbr = ""
    public var rec_cnt = ""
    public var seg_key = ""
    public var dat_len = ""
    public var zip_len = ""
    public var memb_id = ""
    public var chnl_cd = ""
    public var scrn_no = ""
    public var team_cd = ""
    public var sid = ""
    public var reserved2 = ""

}
