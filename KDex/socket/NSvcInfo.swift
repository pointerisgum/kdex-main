//
//  NSvcInfo.swift
//  DEXnPAY
//
//  Created by park heewon on 2018. 3. 21..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation

class NSvcInfo: NSObject {
    
    public var len = ""
    public var tr = ""
    public var service = ""
    public var clid = ""
    public var cd = ""
    public var errcd = ""
    public var sreserved = ""
    public var creserved = ""
    
}
