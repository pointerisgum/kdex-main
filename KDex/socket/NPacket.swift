//
//  NPacket.swift
//  DEXnPAY
//
//  Created by park heewon on 2018. 3. 21..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation

class NPacket: NSObject {
    
    public static let TMSHDR_SIZE = 100
    public static let TMSHDR_SZ_LEN = 10
    
    public static let PKT_LEN_SIZE    =  10
    public static let PKT_HEADER_TLEN = 210
    public static let PKT_HEADER_DLEN = 200
    public static let PKT_MAX_BUF     = 4096 * 64 // 256 K
    public static let PKT_DATA_LEN    = (PKT_MAX_BUF - PKT_HEADER_TLEN)
    public static let SND_MAX_LEN     = 4096 * 64 // 256 K
    
    public var m_tmslen         = NString(size: 10)
    public var m_tmstr          = NString(size: 5)   // transaction code
    public var m_tmsservice     = NString(size: 6)   // º≠∫ÒΩ∫∏Ì
    public var m_tmsclid        = NString(size:10)   // client Ωƒ∫∞¿⁄
    public var m_tmscd          = NString(size:10)   // ≈¨∂Û¿Ãæ∆Æø°º≠ ªÁøÎµ
    public var m_tmserrcd       = NString(size: 5)   // "00000" : complete, 90001" : unknown AP, "99999" : transaction failed
    public var m_tmssreserved   = NString(size:44)   // reserved 5
    public var m_tmscreserved   = NString(size:10)   // client reserved
    
    public var m_len            = NString(size:10)   // packet length without len field 10 byte
    public var m_zip            = NString(size: 1)   // 'N': no zip
    public var m_uid            = NString(size: 8)   // user id
    public var m_clid           = NString(size:10)   // tms clid
    public var m_tr             = NString(size: 6)   // transation code i.e. service name
    public var m_ctype          = NString(size: 1)   // '1': syncronus '2': asyncronus
    public var m_dtype          = NString(size: 1)   // '1': text '2': binary '3': html '4' xtml
    public var m_srvtp          = NString(size: 1)   // '1': text '2': binary '3': html '4' xtml
    public var m_errtp          = NString(size: 1)   // '0': OK else error '9': system error
    public var m_errcd          = NString(size: 5)   // error code
    public var m_ctinfo         = NString(size:16)   // client information area
    public var m_svrinfo        = NString(size: 9)   // server information area
    public var m_seg_nbr        = NString(size: 5)   // Segment Number
    public var m_rec_cnt        = NString(size:10)   // record count
    public var m_seg_key        = NString(size:12)   // Segment Key
    public var m_dat_len        = NString(size:10)   // uncompress data size
    public var m_zip_len        = NString(size:10)   // compress data size
    public var m_memb_id        = NString(size: 7)   // member id
    public var m_chnl_cd        = NString(size: 2)   // channel cd
    public var m_scrn_no        = NString(size: 4)   // screen no
    public var m_team_cd        = NString(size: 5)   // team cd
    public var m_sid            = NString(size:20)   // session id
    public var m_reserved2      = NString(size:56)   // reserved area : client space padding
    public var m_data           = NString(size: 0)   // data area(variable size)
    
    public var m_str = ""
    
    
//    override init() {
//        super.init()
//    }
    
    public func reset(){
        
        m_tmslen.reset()
        m_tmstr.reset()
        m_tmsservice.reset()
        m_tmsclid.reset()
        m_tmscd.reset()
        m_tmserrcd.reset()
        m_tmssreserved.reset()
        m_tmscreserved.reset()
        m_len.reset()
        m_zip.reset()
        m_uid.reset()
        m_clid.reset()
        m_tr.reset()
        m_ctype.reset()
        m_dtype.reset()
        m_srvtp.reset()
        m_errtp.reset()
        m_errcd.reset()
        m_ctinfo.reset()
        m_svrinfo.reset()
        m_seg_nbr.reset()
        m_rec_cnt.reset()
        m_seg_key.reset()
        m_dat_len.reset()
        m_zip_len.reset()
        m_memb_id.reset()
        m_chnl_cd.reset()
        m_scrn_no.reset()
        m_team_cd.reset()
        m_sid.reset()
        m_reserved2.reset()
        m_data.clear()
    }
    
    public func makePacket(service: String, sessid: String, uid: String, input: String, segnbr: Int, segkey: Int ){
        
        reset()
        
        // size of message
        let msglen = input.count
        
        
        // TMS HEADER
        m_tmslen.setValue(value: NPacket.TMSHDR_SIZE - NPacket.TMSHDR_SZ_LEN + NPacket.PKT_HEADER_TLEN + msglen)
        m_tmstr.setValue(str: "SVC_I")
        m_tmsservice.setValue(str: service)
        m_tmsclid.setValue(str: " ")
        m_tmscd.setValue(str: " ")
        m_tmserrcd.setValue(str: " ")
        m_tmssreserved.setValue(str: " ")
        m_tmscreserved.setValue(str: " ")
        
        // USR HEADER
        m_len.setValue(value: NPacket.PKT_HEADER_TLEN - NPacket.PKT_LEN_SIZE +  msglen)
        m_zip.setValue(str: "N")
        m_uid.setValue(str: uid)
        m_clid.setValue(str: " ")
        m_tr.setValue(str: service)
        m_ctype.setValue(str: " ")
        m_dtype.setValue(str: " ")
        m_srvtp.setValue(str: " ")
        m_errtp.setValue(str: "0")
        m_errcd.setValue(str: "00000")
        m_ctinfo.setValue(str: " ")
        m_svrinfo.setValue(str: " ")
        m_seg_nbr.setValue(value:segnbr)
        m_rec_cnt.setValue(value:0)
        m_seg_key.setValue(value:segkey)
        m_dat_len.setValue(value:msglen)
        m_zip_len.setValue(value:msglen)
        m_memb_id.setValue(str: " ")
        m_chnl_cd.setValue(str: " ")
        m_scrn_no.setValue(str: " ")
        m_team_cd.setValue(str: " ")
        m_sid.setValue(str:sessid)
        m_reserved2.setValue(str:" ")
        
        m_data = NString(size: msglen)
        m_data.setValue(str:input)
        
    }
  
    public func parseSvcInfo(bt : [UInt8]) -> NSvcInfo{
        
        let hdr = NSvcInfo();
        var hdrLen = 0;
        
        var tempArr = [UInt8](repeating: 0x00, count: m_tmslen.getSize())
        var temp = [UInt8]()
        
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_tmslen.getSize())
        hdrLen = hdrLen + m_tmslen.getSize()
        hdr.len = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_tmstr.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_tmstr.getSize())
        hdrLen = hdrLen + m_tmstr.getSize()
        hdr.tr = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_tmsservice.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_tmsservice.getSize())
        hdrLen = hdrLen + m_tmsservice.getSize()
        hdr.service = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_tmsclid.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_tmsclid.getSize())
        hdrLen = hdrLen + m_tmsclid.getSize()
        hdr.clid = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_tmscd.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_tmscd.getSize())
        hdrLen = hdrLen + m_tmscd.getSize()
        hdr.cd = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_tmserrcd.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_tmserrcd.getSize())
        hdrLen = hdrLen + m_tmserrcd.getSize()
        hdr.errcd = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_tmssreserved.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_tmssreserved.getSize())
        hdrLen = hdrLen + m_tmssreserved.getSize()
        hdr.sreserved = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_tmscreserved.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_tmscreserved.getSize())
        hdrLen = hdrLen + m_tmscreserved.getSize()
        hdr.creserved = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        return hdr
        
    }
    
    public func parseHeader(bt : [UInt8]) -> NHeader {
        let hdr = NHeader()
        var hdrLen = 0;
        
        var tempArr = [UInt8](repeating: 0, count: m_tmslen.getSize())
        var temp = [UInt8]()
        
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_tmslen.getSize())
        hdrLen = hdrLen + m_tmslen.getSize()
        hdr.tmslen = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_tmstr.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_tmstr.getSize())
        hdrLen = hdrLen + m_tmstr.getSize()
        hdr.tmstr = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_tmsservice.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_tmsservice.getSize())
        hdrLen = hdrLen + m_tmsservice.getSize()
        hdr.tmsservice = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_tmsclid.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_tmsclid.getSize())
        hdrLen = hdrLen + m_tmsclid.getSize()
        hdr.tmsclid = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_tmscd.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_tmscd.getSize())
        hdrLen = hdrLen + m_tmscd.getSize()
        hdr.tmscd = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_tmserrcd.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_tmserrcd.getSize())
        hdrLen = hdrLen + m_tmserrcd.getSize()
        hdr.tmserrcd = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_tmssreserved.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_tmssreserved.getSize())
        hdrLen = hdrLen + m_tmssreserved.getSize()
        hdr.tmssreserved = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_tmscreserved.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_tmscreserved.getSize())
        hdrLen = hdrLen + m_tmscreserved.getSize()
        hdr.tmscreserved = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_len.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_len.getSize())
        hdrLen = hdrLen + m_len.getSize()
        hdr.len = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_zip.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_zip.getSize())
        hdrLen = hdrLen + m_zip.getSize()
        hdr.zip = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_uid.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_uid.getSize())
        hdrLen = hdrLen + m_uid.getSize()
        hdr.uid = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_clid.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_clid.getSize())
        hdrLen = hdrLen + m_clid.getSize()
        hdr.clid = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_tr.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_tr.getSize())
        hdrLen = hdrLen + m_tr.getSize()
        hdr.tr = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_ctype.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_ctype.getSize())
        hdrLen = hdrLen + m_ctype.getSize()
        hdr.ctype = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_dtype.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_dtype.getSize())
        hdrLen = hdrLen + m_dtype.getSize()
        hdr.dtype = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_srvtp.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_srvtp.getSize())
        hdrLen = hdrLen + m_srvtp.getSize()
        hdr.srvtp = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_errtp.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_errtp.getSize())
        hdrLen = hdrLen + m_errtp.getSize()
        hdr.errtp = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_errcd.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_errcd.getSize())
        hdrLen = hdrLen + m_errcd.getSize()
        hdr.errcd = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_ctinfo.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_ctinfo.getSize())
        hdrLen = hdrLen + m_ctinfo.getSize()
        hdr.ctinfo = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_svrinfo.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_svrinfo.getSize())
        hdrLen = hdrLen + m_svrinfo.getSize()
        hdr.svrinfo = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_seg_nbr.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_seg_nbr.getSize())
        hdrLen = hdrLen + m_seg_nbr.getSize()
        hdr.seg_nbr = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_rec_cnt.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_rec_cnt.getSize())
        hdrLen = hdrLen + m_rec_cnt.getSize()
        hdr.rec_cnt = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_seg_key.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_seg_key.getSize())
        hdrLen = hdrLen + m_seg_key.getSize()
        hdr.seg_key = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_dat_len.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_dat_len.getSize())
        hdrLen = hdrLen + m_dat_len.getSize()
        hdr.dat_len = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_zip_len.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_zip_len.getSize())
        hdrLen = hdrLen + m_zip_len.getSize()
        hdr.zip_len = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_memb_id.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_memb_id.getSize())
        hdrLen = hdrLen + m_memb_id.getSize()
        hdr.memb_id = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_chnl_cd.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_chnl_cd.getSize())
        hdrLen = hdrLen + m_chnl_cd.getSize()
        hdr.chnl_cd = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_scrn_no.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_scrn_no.getSize())
        hdrLen = hdrLen + m_scrn_no.getSize()
        hdr.scrn_no = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_team_cd.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_team_cd.getSize())
        hdrLen = hdrLen + m_team_cd.getSize()
        hdr.team_cd = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_sid.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_sid.getSize())
        hdrLen = hdrLen + m_sid.getSize()
        hdr.sid = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: m_reserved2.getSize())
        temp = arrayCopy(arr: bt, start:hdrLen , arr2: tempArr, index: 0, len: m_reserved2.getSize())
        hdrLen = hdrLen + m_reserved2.getSize()
        hdr.reserved2 = String(bytes: temp, encoding: String.Encoding.utf8)!
        
        return hdr
    }
  
    //사이즈 확인 요청 부탁 드립니다.
    func arrayCopy(arr: [UInt8], start: Int, arr2: [UInt8], index: Int, len: Int) -> [UInt8]{
        
        var i = index
        var temp = arr2
        let tempLen = start + len
        
        print("NPack index : \(index)")
        print("NPack temp : \(temp.count)")
        
        for row in start..<tempLen {
            if arr.count == 0 {
                print("packet error break")
                break
            }else {
                //테스트 확인 에러사이즈 확인
//                print("RT index : \(i)")
                temp[i] = arr[row]
                i = i + 1
            }
            
        }
        
        return temp
    }
    
    func toString() -> String {
        
        return (m_tmslen.toString() + m_tmstr.toString() +
            m_tmsservice.toString() + m_tmsclid.toString() +
            m_tmscd.toString() + m_tmserrcd.toString() +
            m_tmssreserved.toString() + m_tmscreserved.toString() +
            m_len.toString() + m_zip.toString() +
            m_uid.toString() + m_clid.toString() +
            m_tr.toString() + m_ctype.toString() +
            m_dtype.toString() + m_srvtp.toString() +
            m_errtp.toString() + m_errcd.toString() +
            m_ctinfo.toString() + m_svrinfo.toString() +
            m_seg_nbr.toString() + m_rec_cnt.toString() +
            m_seg_key.toString() + m_dat_len.toString() +
            m_zip_len.toString() + m_memb_id.toString() +
            m_chnl_cd.toString() + m_scrn_no.toString() +
            m_team_cd.toString() + m_sid.toString() +
            m_reserved2.toString() + m_data.toString())
        
    }
    
    private func writeObject(out: OutputStream){
       
    }
    
    private func readObject(in: InputStream){
        
    }
    
    public func ToBytes() -> [UInt8] {
        m_str = m_tmslen.toString() + m_tmstr.toString() +
            m_tmsservice.toString() + m_tmsclid.toString() +
            m_tmscd.toString() + m_tmserrcd.toString() +
            m_tmssreserved.toString() + m_tmscreserved.toString() +
            m_len.toString() + m_zip.toString() +
            m_uid.toString() + m_clid.toString() +
            m_tr.toString() + m_ctype.toString() +
            m_dtype.toString() + m_srvtp.toString() +
            m_errtp.toString() + m_errcd.toString() +
            m_ctinfo.toString() + m_svrinfo.toString() +
            m_seg_nbr.toString() + m_rec_cnt.toString() +
            m_seg_key.toString() + m_dat_len.toString() +
            m_zip_len.toString() + m_memb_id.toString() +
            m_chnl_cd.toString() + m_scrn_no.toString() +
            m_team_cd.toString() + m_sid.toString() +
            m_reserved2.toString() + m_data.toString()
        
        let buf: [UInt8] = Array(m_str.utf8)
        
        return buf
        
    }
    
 
}
