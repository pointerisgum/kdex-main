//
//  ReceiverThread.swift
//  DEXnPAY
//
//  Created by park heewon on 2018. 3. 21..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation

class ReceiverThread: NSObject {
    
    public static let INTPUTSTREAM_READ_RETRY_COUNT = 10//호출 카운트
    
    var socket : TcpSocket?
    var delegate : RealTimeDelegate?
   
    let TMSHDR_SIZE = 100 // 미들 헤더
    let USRHDR_SIZE = 210 //유저 헤더
    
    let TPMSG_SUCCESS_TR =  "00000"   // Complete
    let TPMSG_UNKNOWN_TR =  "90001"    // UNKNOWN AP
    let TPMSG_TRXFAIL_TR =  "99999"    // transaction failed
    
    let RTS_TICK         =  "R_TICK"   // Price Tick
    let RTS_QUOT         =  "R_QUOT"   // Price Info
    let RTS_ORDR         =  "R_ORDR"   // ¡÷πÆ π◊ √º∞·≈Î∫∏
    let RTS_NTRD         =  "R_NTRD"   // πÃ√º∞·¡÷πÆ
    let RTS_TRAD         =  "R_TRAD"   // √º∞·≥ªø™
    let RTS_OPEN         =  "R_OPEN"   // πÃ∞·¡¶≥ªø™
    let RTS_EQTY         =  "R_ASET"   // Account Balance
    let RTS_EARN         =  "R_EARN"   // Profit & loss
    let RTS_NOTI         =  "R_NOTI"   // Notice Info
    
    init(socket : TcpSocket) {
        super.init()
        self.socket = socket
    }
    
    public static func streamNRead(input : InputStream, len : Int) -> String {
    
        print("recive")
        var bcount = 0
        var chunkString = ""
//        let buffersize = 4096
        let buffersize = 2048
        var read_retry_count = 0
        var buffer = [UInt8](repeating :0, count : buffersize)
        
        while ( bcount < len){
            print("sc : streamNRead")
            
            let maxlen = len-bcount < buffersize ? len-bcount : buffersize
            
            let bytesRead = input.read(&buffer, maxLength: maxlen)
            
            print("sc : bytesRead : \(bytesRead)")
            
            if bytesRead == -1 {
                print("inputstream has returned an unexpected EOF")
//                ReceiverThread.alertAction();
                return ""
            }
        
            if bytesRead != 0 {
                bcount = bcount + bytesRead
            }
            
            if bytesRead == 0 && (read_retry_count == INTPUTSTREAM_READ_RETRY_COUNT) {
                print(" inputstream-read-retry-count exceed socket reader error ")
//                ReceiverThread.alertAction();
                return ""
                //서버 소켓이 끊겼을 경우
            }
            read_retry_count = read_retry_count + 1
            
            var dropCount = buffersize - bytesRead
            
            if dropCount < 0 {
                dropCount = 0
            }
            
            let chunk = buffer.dropLast(dropCount)
            
            chunkString = String(bytes: chunk, encoding: String.Encoding.utf8)!
        }
   
         return chunkString
    }
    
    public func run(input : InputStream, output: OutputStream) {
        
        var pktlen = 0
        var msglen = 0
        
        var strLen = ""
        var strMsg = ""
        var strPkt = ""
        
        var tms = NSvcInfo()
        var hdr = NHeader()
        var pkt = NPacket()
        var bd2 = NPriceTick()  // 체결추이
        var bo3 = NPriceInfo()  // 호가
        var bd3 = NNtrdInfo ()  // working order
        var bd4 = NTradInfo ()  // trade
        var bd5 = NOpenInfo ()  // position
        
        while true {
            print("sc : reading.........")
            
            strLen = ReceiverThread.streamNRead(input: input, len: 10)
            
            print("sc : reading strLen..... \(strLen)")
            
            if strLen == "0" || strLen == "" {
                print("sc : socket lenght 0 eror")
                socket?.disconnect()
                
                SocketManger.sharedInstance.client?.close()
                
                SocketManger.sharedInstance.client?.connect(timeout: 10)
            }
            
            pktlen = Int(strLen.trimmingCharacters(in: CharacterSet.whitespaces)) ?? 0
            
            print("sc : reading pktlen..... \(pktlen)")
            
            strMsg = ReceiverThread.streamNRead(input: input, len: pktlen)
            
            strPkt = strLen + strMsg
            
            //print("sc : output : \(strPkt)")
            
            let byteBuffer: [UInt8] = Array(strPkt.utf8)
            
            tms = pkt.parseSvcInfo(bt: byteBuffer)
            
            print("sc : tmslen : \(tms.len), tmstr : \(tms.tr), tmsservice : \(tms.service), tmserrcd : \(tms.errcd)")
            
            if tms.tr == "HBPKT" {
                tms.errcd = "00000"
                
            }
            
            if tms.errcd == TPMSG_SUCCESS_TR {
                print("sc :DEBUG 1")
                
                if tms.tr == "HBPKT" {
                    print("sc : heat beat received!!")
                    
//                    let random = Int(arc4random_uniform(UInt32(100000)))
//                    print("random : \(random)")
//                    AppDelegate.BTC = Double(random)
//                    print("appDelegate BTC : \(AppDelegate.BTC)")
//                    delegate?.krwChangeRT(value: Double(random))
//
                    let byte = self.sendHeatBeat(output: output)
                    if byte == 0 {
                        print("sc : no send data")
                    }else {
                        print("sc : HBPKT send data")
                    }
                }else if tms.tr == "BROAD" {
                    print("sc : Broadcasting msg received!!")
                }else if tms.tr == "SVC_O" {//조회 결과
                   print("sc : SVC_O")
                    hdr = pkt.parseHeader(bt: byteBuffer)
                    print("len : \(hdr.len)")
                    print("zip : \(hdr.zip)")
                    print("uid : \(hdr.uid)")
                    print("tr : \(hdr.tr)")
                    print("errtp : \(hdr.errtp)")
                    print("errcd : \(hdr.errcd)")
                    print("ctinfo : \(hdr.ctinfo)")
                    print("seg_nbr : \(hdr.seg_nbr)")
                    print("seg_key : \(hdr.seg_key)")
                    print("dat_len : \(hdr.dat_len)")
                    print("zip_len : \(hdr.zip_len)")
                    
                }else if tms.tr == "RDPKT" {//실시간
                    hdr = pkt.parseHeader(bt: byteBuffer)
                    
                    msglen = Int(tms.len.trimmingCharacters(in: CharacterSet.whitespaces))!
                   
                    var msgArr = [UInt8](repeating :0x00, count : msglen - 300)
                    msgArr = arrayCopy(arr: byteBuffer, start: (TMSHDR_SIZE + USRHDR_SIZE) , arr2: msgArr, index: 0, len: msglen - 300)
                    
                    if hdr.tr == RTS_TICK {
                        print("sc : RTS_TICK")//현재가
                        bd2.parseData(bt: msgArr)
                        delegate?.exchangListRT(bt: bd2)
                        
                        let symbol = bd2.symbol.toString().components(separatedBy: [" "]).joined() //실시간 심볼
                        let last_price = bd2.last_price.toString()
                        
                        print("RT exchang all symbole : \(symbol)")
                        print("RT exchang all last_price : \(last_price)")
                        
                        if symbol == "ETH" {
                            AppDelegate.ETH = stringtoDouble(str: last_price) ?? 0.0
                        }else if symbol == "BTC" {
                            AppDelegate.BTC = stringtoDouble(str: last_price) ?? 0.0
                        }
                        
                        //delegate?.exchangDetailRT(bt: bd2)
                        
                    }else if hdr.tr == RTS_QUOT {
                        print("sc : RTS_QUOT") //호가
                        bo3.parseData(bt: msgArr)
                        delegate?.trandeListRT(bt: bo3)
                        
                    }else if hdr.tr == RTS_NTRD {
                        print("sc : RTS_NTRD")
                        bd3.parseData(bt: msgArr)
                        
                    }else if hdr.tr == RTS_TRAD {
                        print("sc : RTS_TRAD")
                        bd4.parseData(bt: msgArr)
                        
                    }else if hdr.tr == RTS_OPEN {
                        print("sc : RTS_OPEN")
                        bd5.parseData(bt: msgArr)
                        delegate?.openListRT(bt: bd5)
                        
                    }
                }
                
            }else {
                print("sc :DEBUG2")
                //19.01.11 error
                if tms.errcd == self.TPMSG_UNKNOWN_TR {
                    print("sc :unknown tr!!")
                }else if tms.errcd == self.TPMSG_TRXFAIL_TR {
                    print("sc :transaction failed!!")
                }else {
                    print("sc :other case error")
//                    ReceiverThread.alertAction();
                    return
                }
            }
        }
    }
    
    func arrayCopy(arr: [UInt8], start: Int, arr2: [UInt8], index: Int, len: Int) -> [UInt8]{
        var i = index
        var temp = arr2
        let tempLen = start + len
        for row in start..<tempLen {
            temp[i] = arr[row]
            i = i + 1
        }
        
        return temp
    }
    
//    public func sendHeatBeat(output: OutputStream) -> Int{
//        let TMSHDR_SIZE = 100
//        let TMSHDR_SZ_LEN = 10
//
//        //let TMS_HEADER_FORMAT = "%010d%-5s%-6s%-10s%-10s%-5s%-44s%-10s"
//
//        let TMS_HEADER_FORMAT = "%010d"
//
//        let len = TMSHDR_SIZE - TMSHDR_SZ_LEN
//        let tr = "HBPKT"
//        let service_name = " "
//        let clid = " "
//        let cd = " "
//        let errcd = " "
//        let sreserved = " "
//        let creserved = " "
//        //let userid = uid
//
//        let str = "\(String(format: TMS_HEADER_FORMAT, len))\(tr.streamStr(num: 5, str: tr))\(service_name.streamStr(num: 6, str: service_name))\(clid.streamStr(num: 10, str: clid))\(cd.streamStr(num: 10, str: cd))\(errcd.streamStr(num: 5, str: errcd))\(sreserved.streamStr(num: 44, str: sreserved))\(creserved.streamStr(num: 10, str: creserved))"
//
//        guard let data = str.data(using: String.Encoding.utf8, allowLossyConversion: true) else{
//            return 0
//        }
//
//        let bytesWritten = data.withUnsafeBytes { output.write($0, maxLength: data.count) }
//        return bytesWritten
//
//    }
    
    public func sendHeatBeat(output: OutputStream) -> Int{
        
        print("RS sendheatBeat send event")
        
        let pkt = NPacket()
        
        let user_id = NString(size: 8)
        
        user_id.setValue(str: [AppDelegate.uid])
        
        let str = user_id.toString()
        
        pkt.makePacket(service: "TR0099", sessid: "00000000000000000000", uid: AppDelegate.uid, input: str, segnbr: 0, segkey: 0)
        
        let strMsg = pkt.toString()
        
        guard let byteBuffer = strMsg.data(using: String.Encoding.utf8, allowLossyConversion: true) else {
            return 0
        }

        let bytesWritten = byteBuffer.withUnsafeBytes { output.write($0, maxLength: byteBuffer.count) }
        
        return bytesWritten
        
    }
    
    public func stringtoDouble(str : String) -> Double?{
        let temp = str.components(separatedBy: [","]).joined()
        let double = Double(temp)
        return double ?? 0.0
    }
    
    public static func alertAction() {
        
        let alert = UIAlertController(title: "통신", message: "통신이 끊겼습니다. 잠시후 앱을 다시 실행해 주세요", preferredStyle: UIAlertController.Style.alert)
        
        let okbtn = UIAlertAction(title: "확인", style: .default) { (_) in
        }
        
        alert.addAction(okbtn)
        
        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        alertWindow.rootViewController = UIViewController()
        alertWindow.windowLevel = UIWindow.Level.alert + 1;
        alertWindow.makeKeyAndVisible()
        alertWindow.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
}
