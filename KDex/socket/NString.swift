//
//  NString.swift
//  DEXnPAY
//
//  Created by park heewon on 2018. 3. 21..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation

class NString: NSObject {
    
    var m_size: Int?
    var m_buf : [String]?
    
    override init() {
        super.init()
        m_size = 1
    }
    
    init(size : Int){
        m_size = size
        m_buf = [String](repeating: " ", count: size)
    }
    
    public func toString() -> String{
        guard let str = m_buf?.joined(separator: "") else {
            return ""
        }
        
        return str
    }
    
    public func setValue(str: String, ch: Character){
        
        var i = 0
        var strLen = 0
        var spaceSize = 0
        
        strLen = str.count
        
        if m_size == 0 {
            m_size = strLen
            m_buf = [String](repeating: " ", count: m_size!)
        }
        
        spaceSize = m_size! - strLen
        
        for row in 0..<spaceSize {
            m_buf![row] = String(ch) 
        }
        
        if m_size! > spaceSize {
            
            
            for  row in spaceSize..<m_size! {
                
                let tempstr = str[str.index(str.startIndex, offsetBy: i)]
               // print(tempstr)
                
                m_buf![row] = "\(tempstr)"
                
                i = i + 1

            }
            
        }
        
    }
    
    public func setValue(str : String) {
        self.setValue(str: str, ch: " ")
    }
    
    
    public func setValue(str: [String]){
        self.setValue(str : str.joined(separator: ""))
    }
    
    public func setValue(value : Int){
        
        let str = String(value)
        
        self.setValue(str: str, ch: "0")
    }
    
    public func setValue(value : Double){
        let str = String(value)
        
        self.setValue(str: str, ch: "0")
        
    }
    
    public func setValue(byteArray:[UInt8]){
        let str = String(bytes: byteArray, encoding: String.Encoding.utf8)
        
        self.setValue(str: str!, ch: " ")
    }
    
    
    public func reset() {
        m_buf = [String](repeating: " ", count:  m_size!)
    }
    
    public func clear() {
        if m_size == 0 {
            return
        }else{
            m_buf?.removeAll()
        }
        
    }
    
    public func getSize() -> Int{
        return m_size!
    }
    
}
