//
//  NOpenInfo.swift
//  socketText
//
//  Created by park heewon on 2018. 3. 23..
//  Copyright © 2018년 park heewon. All rights reserved.
//

import Foundation

class NOpenInfo: NSObject {
    
    public var data_tp           = NString(size:  6)     // µ¥ÀÌÅÍ±¸ºÐ
    public var user_id           = NString(size:  8)     // »ç¿ëÀÚID
    public var acct_no           = NString(size: 15)     // °èÁÂ¹øÈ£
    public var inst_cd           = NString(size: 32)     // Á¾¸ñÄÚµå
    public var bysl_tp           = NString(size:  1)     // ¸Å¼ö¸Åµµ
    public var open_qty          = NString(size: 15)     // ÀÜ°í¼ö·®
    public var lqab_qty          = NString(size: 15)     // Ã»»ê°¡´É¼ö·®
    public var nmth_qty          = NString(size: 15)     // ¹ÌÃ¼°á¼ö·®
    public var over_qty          = NString(size: 15)     // ¿À¹ö³ªÀÕ
    public var new_qty           = NString(size: 15)     // ½Å±Ô
    public var avg_prc           = NString(size: 15)     // Æò±Õ´Ü°¡
    public var last_prc          = NString(size: 15)     // ÇöÀç°¡
    public var open_pl           = NString(size: 15)     // Æò°¡¼ÕÀÍ
    public var commission        = NString(size: 15)     // commsion
    public var exra              = NString(size: 15)     // È¯À²
    public var crc_cd            = NString(size: 10)     // ÅëÈ­ÄÚµå
    public var prc_prsn          = NString(size:  5)     // °¡°Ý
    public var qty_prsn          = NString(size:  5)     // ¼ö·®
    
    public var strBuffer = ""
    
    override init() {
        super.init()
        
    }
    
    public func reset() {
        data_tp        .reset();
        user_id        .reset();
        acct_no        .reset();
        inst_cd        .reset();
        bysl_tp        .reset();
        open_qty       .reset();
        lqab_qty       .reset();
        nmth_qty       .reset();
        over_qty       .reset();
        new_qty        .reset();
        avg_prc        .reset();
        last_prc       .reset();
        open_pl        .reset();
        commission     .reset();
        exra           .reset();
        crc_cd         .reset();
        prc_prsn       .reset();
        qty_prsn       .reset();
    }
    
    public func parseData(bt: [UInt8]){
        
        var msgLen = 0
        
        var tempArr = [UInt8](repeating: 0x00, count: data_tp.getSize())
        var temp = [UInt8]()
    
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: data_tp.getSize())
        msgLen = msgLen + data_tp.getSize()
        data_tp.setValue(byteArray: temp)
    
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: user_id.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: user_id.getSize())
        msgLen = msgLen + user_id.getSize()
        user_id.setValue(byteArray: temp)
    
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: acct_no.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: acct_no.getSize())
        msgLen = msgLen + acct_no.getSize()
        acct_no.setValue(byteArray: temp)
    
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: inst_cd.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: inst_cd.getSize())
        msgLen = msgLen + inst_cd.getSize()
        inst_cd.setValue(byteArray: temp)
    
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: bysl_tp.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: bysl_tp.getSize())
        msgLen = msgLen + bysl_tp.getSize()
        bysl_tp.setValue(byteArray: temp)
    
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: open_qty.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: open_qty.getSize())
        msgLen = msgLen + open_qty.getSize()
        open_qty.setValue(byteArray: temp)
    
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: lqab_qty.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: lqab_qty.getSize())
        msgLen = msgLen + lqab_qty.getSize()
        lqab_qty.setValue(byteArray: temp)
    
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: nmth_qty.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: nmth_qty.getSize())
        msgLen = msgLen + nmth_qty.getSize()
        nmth_qty.setValue(byteArray: temp)
    
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: over_qty.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: over_qty.getSize())
        msgLen = msgLen + over_qty.getSize()
        over_qty.setValue(byteArray: temp)
    
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: new_qty.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: new_qty.getSize())
        msgLen = msgLen + new_qty.getSize()
        new_qty.setValue(byteArray: temp)
    
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: avg_prc.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: avg_prc.getSize())
        msgLen = msgLen + avg_prc.getSize()
        avg_prc.setValue(byteArray: temp)
    
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: last_prc.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: last_prc.getSize())
        msgLen = msgLen + last_prc.getSize()
        last_prc.setValue(byteArray: temp)
    
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: open_pl.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: open_pl.getSize())
        msgLen = msgLen + open_pl.getSize()
        open_pl.setValue(byteArray: temp)
    
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: commission.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: commission.getSize())
        msgLen = msgLen + commission.getSize()
        commission.setValue(byteArray: temp)
    
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: exra.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: exra.getSize())
        msgLen = msgLen + exra.getSize()
        exra.setValue(byteArray: temp)
    
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: crc_cd.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: crc_cd.getSize())
        msgLen = msgLen + crc_cd.getSize()
        crc_cd.setValue(byteArray: temp)
    
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: prc_prsn.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: prc_prsn.getSize())
        msgLen = msgLen + prc_prsn.getSize()
        prc_prsn.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: qty_prsn.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: qty_prsn.getSize())
        msgLen = msgLen + qty_prsn.getSize()
        qty_prsn.setValue(byteArray: temp)
    
    }
    
    public func toString() -> String {
        return ( user_id    .toString()  +
        acct_no    .toString()  +
        inst_cd    .toString()  +
        bysl_tp    .toString()  +
        open_qty   .toString()  +
        lqab_qty   .toString()  +
        nmth_qty   .toString()  +
        over_qty   .toString()  +
        new_qty    .toString()  +
        avg_prc    .toString()  +
        last_prc   .toString()  +
        open_pl    .toString()  +
        commission .toString()  +
        exra       .toString()  +
        crc_cd     .toString()  +
        prc_prsn   .toString()  +
        qty_prsn   .toString())
    }
    
    
    private func writeObject(out: OutputStream){
            // out.write(<#T##buffer: UnsafePointer<UInt8>##UnsafePointer<UInt8>#>, maxLength: <#T##Int#>)
            
    }
    
    
    public func ToBytes() -> [UInt8] {
        
        strBuffer = user_id     .toString()  +
        acct_no     .toString()  +
        inst_cd     .toString()  +
        bysl_tp     .toString()  +
        open_qty    .toString()  +
        lqab_qty    .toString()  +
        nmth_qty    .toString()  +
        over_qty    .toString()  +
        new_qty     .toString()  +
        avg_prc     .toString()  +
        last_prc    .toString()  +
        open_pl     .toString()  +
        commission  .toString()  +
        exra        .toString()  +
        crc_cd      .toString()  +
        prc_prsn    .toString()  +
        qty_prsn    .toString()
        
        let buf: [UInt8] = Array(strBuffer.utf8)
        
        return buf
    }
    
    
    public func toJSON() -> String {

        return ( user_id    .toString()  +
        acct_no    .toString()  +
        inst_cd    .toString()  +
        bysl_tp    .toString()  +
        open_qty   .toString()  +
        lqab_qty   .toString()  +
        nmth_qty   .toString()  +
        over_qty   .toString()  +
        new_qty    .toString()  +
        avg_prc    .toString()  +
        last_prc   .toString()  +
        open_pl    .toString()  +
        commission .toString()  +
        exra       .toString()  +
        crc_cd     .toString()  +
        prc_prsn   .toString()  +
        qty_prsn   .toString()  )
        
    }
    
    func arrayCopy(arr: [UInt8], start: Int, arr2: [UInt8], index: Int, len: Int) -> [UInt8]{
        var i = index
        var temp = arr2
        let tempLen = start + len
        for row in start..<tempLen {
            temp[i] = arr[row]
            i = i + 1
        }
        //print(temp)
        return temp
    }
    
}
