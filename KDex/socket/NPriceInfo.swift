//
//  NPriceInfo.swift
//  socketText
//
//  Created by park heewon on 2018. 3. 23..
//  Copyright © 2018년 park heewon. All rights reserved.
//

import Foundation

class NPriceInfo : NSObject{
    public var timestamp         = NString(size: 20)     // È£°¡½Ã°£
    public var symbol            = NString(size: 32)     // Á¾¸ñÄÚµå
    public var prc_precision     = NString(size: 10)     // °¡°ÝÁ¤¹Ðµµ
    public var qty_precision     = NString(size: 10)     // ¼ö·®Á¤¹Ðµµ
    public var ask_total_vol     = NString(size: 15)     // ¸Åµµ¼ö·®
    public var ask_total_cnt     = NString(size: 15)     // ¸Åµµ°Ç¼ö
    public var bid_total_vol     = NString(size: 15)     // ¸Å¼ö¼ö·®
    public var bid_total_cnt     = NString(size: 15)     // ¸Å¼ö°Ç¼ö
    
    public var ask_prc1          = NString(size: 15)     // ¸Åµµ°¡
    public var ask_vol1          = NString(size: 15)     // ¸Åµµ¼ö·®
    public var ask_cnt1          = NString(size: 15)     // ¸Åµµ°Ç¼ö
    public var bid_prc1          = NString(size: 15)     // ¸Å¼ö°¡
    public var bid_vol1          = NString(size: 15)     // ¸Å¼ö¼ö·®
    public var bid_cnt1          = NString(size: 15)     // ¸Å¼ö°Ç¼ö
    
    public var ask_prc2          = NString(size: 15)     // ¸Åµµ°¡
    public var ask_vol2          = NString(size: 15)     // ¸Åµµ¼ö·®
    public var ask_cnt2          = NString(size: 15)     // ¸Åµµ°Ç¼ö
    public var bid_prc2          = NString(size: 15)     // ¸Å¼ö°¡
    public var bid_vol2          = NString(size: 15)     // ¸Å¼ö¼ö·®
    public var bid_cnt2          = NString(size: 15)     // ¸Å¼ö°Ç¼ö
    
    public var ask_prc3          = NString(size: 15)     // ¸Åµµ°¡
    public var ask_vol3          = NString(size: 15)     // ¸Åµµ¼ö·®
    public var ask_cnt3          = NString(size: 15)     // ¸Åµµ°Ç¼ö
    public var bid_prc3          = NString(size: 15)     // ¸Å¼ö°¡
    public var bid_vol3          = NString(size: 15)     // ¸Å¼ö¼ö·®
    public var bid_cnt3          = NString(size: 15)     // ¸Å¼ö°Ç¼ö
    
    public var ask_prc4          = NString(size: 15)     // ¸Åµµ°¡
    public var ask_vol4          = NString(size: 15)     // ¸Åµµ¼ö·®
    public var ask_cnt4          = NString(size: 15)     // ¸Åµµ°Ç¼ö
    public var bid_prc4          = NString(size: 15)     // ¸Å¼ö°¡
    public var bid_vol4          = NString(size: 15)     // ¸Å¼ö¼ö·®
    public var bid_cnt4          = NString(size: 15)     // ¸Å¼ö°Ç¼ö
    
    public var ask_prc5          = NString(size: 15)     // ¸Åµµ°¡
    public var ask_vol5          = NString(size: 15)     // ¸Åµµ¼ö·®
    public var ask_cnt5          = NString(size: 15)     // ¸Åµµ°Ç¼ö
    public var bid_prc5          = NString(size: 15)     // ¸Å¼ö°¡
    public var bid_vol5          = NString(size: 15)     // ¸Å¼ö¼ö·®
    public var bid_cnt5          = NString(size: 15)     // ¸Å¼ö°Ç¼ö
    
    public var ask_prc6          = NString(size: 15)     // ¸Åµµ°¡
    public var ask_vol6          = NString(size: 15)     // ¸Åµµ¼ö·®
    public var ask_cnt6          = NString(size: 15)     // ¸Åµµ°Ç¼ö
    public var bid_prc6          = NString(size: 15)     // ¸Å¼ö°¡
    public var bid_vol6          = NString(size: 15)     // ¸Å¼ö¼ö·®
    public var bid_cnt6          = NString(size: 15)     // ¸Å¼ö°Ç¼ö
    
    public var ask_prc7          = NString(size: 15)     // ¸Åµµ°¡
    public var ask_vol7          = NString(size: 15)     // ¸Åµµ¼ö·®
    public var ask_cnt7          = NString(size: 15)     // ¸Åµµ°Ç¼ö
    public var bid_prc7          = NString(size: 15)     // ¸Å¼ö°¡
    public var bid_vol7          = NString(size: 15)     // ¸Å¼ö¼ö·®
    public var bid_cnt7          = NString(size: 15)     // ¸Å¼ö°Ç¼ö
    
    public var ask_prc8          = NString(size: 15)     // ¸Åµµ°¡
    public var ask_vol8          = NString(size: 15)     // ¸Åµµ¼ö·®
    public var ask_cnt8          = NString(size: 15)     // ¸Åµµ°Ç¼ö
    public var bid_prc8          = NString(size: 15)     // ¸Å¼ö°¡
    public var bid_vol8          = NString(size: 15)     // ¸Å¼ö¼ö·®
    public var bid_cnt8          = NString(size: 15)     // ¸Å¼ö°Ç¼ö
    
    public var ask_prc9          = NString(size: 15)     // ¸Åµµ°¡
    public var ask_vol9          = NString(size: 15)     // ¸Åµµ¼ö·®
    public var ask_cnt9          = NString(size: 15)     // ¸Åµµ°Ç¼ö
    public var bid_prc9          = NString(size: 15)     // ¸Å¼ö°¡
    public var bid_vol9          = NString(size: 15)     // ¸Å¼ö¼ö·®
    public var bid_cnt9          = NString(size: 15)     // ¸Å¼ö°Ç¼ö
    
    public var ask_prc10         = NString(size: 15)     // ¸Åµµ°¡
    public var ask_vol10         = NString(size: 15)     // ¸Åµµ¼ö·®
    public var ask_cnt10         = NString(size: 15)     // ¸Åµµ°Ç¼ö
    public var bid_prc10         = NString(size: 15)     // ¸Å¼ö°¡
    public var bid_vol10         = NString(size: 15)     // ¸Å¼ö¼ö·®
    public var bid_cnt10         = NString(size: 15)     // ¸Å¼ö°Ç¼ö
    
    public var strBuffer = ""
    public var krwPrice = 1
    
    override init() {
        super.init()
    }
    
    public func reset() {
        timestamp         .reset();
        symbol            .reset();
        prc_precision     .reset();
        qty_precision     .reset();
        ask_total_vol     .reset();
        ask_total_cnt     .reset();
        bid_total_vol     .reset();
        bid_total_cnt     .reset();
        ask_prc1          .reset();
        ask_vol1          .reset();
        ask_cnt1          .reset();
        bid_prc1          .reset();
        bid_vol1          .reset();
        bid_cnt1          .reset();
        ask_prc2          .reset();
        ask_vol2          .reset();
        ask_cnt2          .reset();
        bid_prc2          .reset();
        bid_vol2          .reset();
        bid_cnt2          .reset();
        ask_prc3          .reset();
        ask_vol3          .reset();
        ask_cnt3          .reset();
        bid_prc3          .reset();
        bid_vol3          .reset();
        bid_cnt3          .reset();
        ask_prc4          .reset();
        ask_vol4          .reset();
        ask_cnt4          .reset();
        bid_prc4          .reset();
        bid_vol4          .reset();
        bid_cnt4          .reset();
        ask_prc5          .reset();
        ask_vol5          .reset();
        ask_cnt5          .reset();
        bid_prc5          .reset();
        bid_vol5          .reset();
        bid_cnt5          .reset();
        ask_prc6          .reset();
        ask_vol6          .reset();
        ask_cnt6          .reset();
        bid_prc6          .reset();
        bid_vol6          .reset();
        bid_cnt6          .reset();
        ask_prc7          .reset();
        ask_vol7          .reset();
        ask_cnt7          .reset();
        bid_prc7          .reset();
        bid_vol7          .reset();
        bid_cnt7          .reset();
        ask_prc8          .reset();
        ask_vol8          .reset();
        ask_cnt8          .reset();
        bid_prc8          .reset();
        bid_vol8          .reset();
        bid_cnt8          .reset();
        ask_prc9          .reset();
        ask_vol9          .reset();
        ask_cnt9          .reset();
        bid_prc9          .reset();
        bid_vol9          .reset();
        bid_cnt9          .reset();
        ask_prc10         .reset();
        ask_vol10         .reset();
        ask_cnt10         .reset();
        bid_prc10         .reset();
        bid_vol10         .reset();
        bid_cnt10         .reset();
        
    }

    public func parseData(bt: [UInt8]){
        
        print("RT price real Price : \(bt)")
        
        var msgLen = 0
        
        var tempArr = [UInt8](repeating: 0x00, count: timestamp.getSize())
        var temp = [UInt8]()
        
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: timestamp.getSize())
        msgLen = msgLen + timestamp.getSize()
        timestamp.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: symbol.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: symbol.getSize())
        msgLen = msgLen + symbol.getSize()
        symbol.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: prc_precision.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: prc_precision.getSize())
        msgLen = msgLen + prc_precision.getSize()
        prc_precision.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: qty_precision.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: qty_precision.getSize())
        msgLen = msgLen + qty_precision.getSize()
        qty_precision.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ask_total_vol.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ask_total_vol.getSize())
        msgLen = msgLen + ask_total_vol.getSize()
        ask_total_vol.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ask_total_cnt.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ask_total_cnt.getSize())
        msgLen = msgLen + ask_total_cnt.getSize()
        ask_total_cnt.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: bid_total_vol.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: bid_total_vol.getSize())
        msgLen = msgLen + bid_total_vol.getSize()
        bid_total_vol.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: bid_total_cnt.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: bid_total_cnt.getSize())
        msgLen = msgLen + bid_total_cnt.getSize()
        bid_total_cnt.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ask_prc1.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ask_prc1.getSize())
        msgLen = msgLen + ask_prc1.getSize()
        ask_prc1.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ask_vol1.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ask_vol1.getSize())
        msgLen = msgLen + ask_vol1.getSize()
        ask_vol1.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ask_cnt1.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ask_cnt1.getSize())
        msgLen = msgLen + ask_cnt1.getSize()
        ask_cnt1.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: bid_prc1.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: bid_prc1.getSize())
        msgLen = msgLen + bid_prc1.getSize()
        bid_prc1.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: bid_vol1.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: bid_vol1.getSize())
        msgLen = msgLen + bid_vol1.getSize()
        bid_vol1.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: bid_cnt1.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: bid_cnt1.getSize())
        msgLen = msgLen + bid_cnt1.getSize()
        bid_cnt1.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ask_prc2.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ask_prc2.getSize())
        msgLen = msgLen + ask_prc2.getSize()
        ask_prc2.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ask_vol2.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ask_vol2.getSize())
        msgLen = msgLen + ask_vol2.getSize()
        ask_vol2.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ask_vol2.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ask_cnt2.getSize())
        msgLen = msgLen + ask_cnt2.getSize()
        ask_cnt2.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: bid_prc2.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: bid_prc2.getSize())
        msgLen = msgLen + bid_prc2.getSize()
        bid_prc2.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: bid_vol2.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: bid_vol2.getSize())
        msgLen = msgLen + bid_vol2.getSize()
        bid_vol2.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: bid_cnt2.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: bid_cnt2.getSize())
        msgLen = msgLen + bid_cnt2.getSize()
        bid_cnt2.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ask_prc3.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ask_prc3.getSize())
        msgLen = msgLen + ask_prc3.getSize()
        ask_prc3.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ask_vol3.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ask_vol3.getSize())
        msgLen = msgLen + ask_vol3.getSize()
        ask_vol3.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ask_cnt3.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ask_cnt3.getSize())
        msgLen = msgLen + ask_cnt3.getSize()
        ask_cnt3.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: bid_prc3.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: bid_prc3.getSize())
        msgLen = msgLen + bid_prc3.getSize()
        bid_prc3.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: bid_vol3.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: bid_vol3.getSize())
        msgLen = msgLen + bid_vol3.getSize()
        bid_vol3.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: bid_cnt3.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: bid_cnt3.getSize())
        msgLen = msgLen + bid_cnt3.getSize()
        bid_cnt3.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ask_prc4.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ask_prc4.getSize())
        msgLen = msgLen + ask_prc4.getSize()
        ask_prc4.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ask_vol4.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ask_vol4.getSize())
        msgLen = msgLen + ask_vol4.getSize()
        ask_vol4.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ask_cnt4.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ask_cnt4.getSize())
        msgLen = msgLen + ask_cnt4.getSize()
        ask_cnt4.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: bid_prc4.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: bid_prc4.getSize())
        msgLen = msgLen + bid_prc4.getSize()
        bid_prc4.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: bid_vol4.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: bid_vol4.getSize())
        msgLen = msgLen + bid_vol4.getSize()
        bid_vol4.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: bid_cnt4.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: bid_cnt4.getSize())
        msgLen = msgLen + bid_cnt4.getSize()
        bid_cnt4.setValue(byteArray: temp)
        
        //
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ask_prc5.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ask_prc5.getSize())
        msgLen = msgLen + ask_prc5.getSize()
        ask_prc5.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ask_vol5.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ask_vol5.getSize())
        msgLen = msgLen + ask_vol5.getSize()
        ask_vol5.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ask_cnt5.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ask_cnt5.getSize())
        msgLen = msgLen + ask_cnt5.getSize()
        ask_cnt5.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: bid_prc5.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: bid_prc5.getSize())
        msgLen = msgLen + bid_prc5.getSize()
        bid_prc5.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: bid_vol5.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: bid_vol5.getSize())
        msgLen = msgLen + bid_vol5.getSize()
        bid_vol5.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: bid_cnt5.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: bid_cnt5.getSize())
        msgLen = msgLen + bid_cnt5.getSize()
        bid_cnt5.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ask_prc6.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ask_prc6.getSize())
        msgLen = msgLen + ask_prc6.getSize()
        ask_prc6.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ask_vol6.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ask_vol6.getSize())
        msgLen = msgLen + ask_vol6.getSize()
        ask_vol6.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ask_cnt6.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ask_cnt6.getSize())
        msgLen = msgLen + ask_cnt6.getSize()
        ask_cnt6.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: bid_prc6.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: bid_prc6.getSize())
        msgLen = msgLen + bid_prc6.getSize()
        bid_prc6.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: bid_vol6.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: bid_vol6.getSize())
        msgLen = msgLen + bid_vol6.getSize()
        bid_vol6.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: bid_cnt6.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: bid_cnt6.getSize())
        msgLen = msgLen + bid_cnt6.getSize()
        bid_cnt6.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ask_prc7.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ask_prc7.getSize())
        msgLen = msgLen + ask_prc7.getSize()
        ask_prc7.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ask_vol7.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ask_vol7.getSize())
        msgLen = msgLen + ask_vol7.getSize()
        ask_vol7.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ask_cnt7.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ask_cnt7.getSize())
        msgLen = msgLen + ask_cnt7.getSize()
        ask_cnt7.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: bid_prc7.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: bid_prc7.getSize())
        msgLen = msgLen + bid_prc7.getSize()
        bid_prc7.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: bid_vol7.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: bid_vol7.getSize())
        msgLen = msgLen + bid_vol7.getSize()
        bid_vol7.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: bid_cnt7.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: bid_cnt7.getSize())
        msgLen = msgLen + bid_cnt7.getSize()
        bid_cnt7.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ask_prc8.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ask_prc8.getSize())
        msgLen = msgLen + ask_prc8.getSize()
        ask_prc8.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ask_vol8.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ask_vol8.getSize())
        msgLen = msgLen + ask_vol8.getSize()
        ask_vol8.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ask_cnt8.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ask_cnt8.getSize())
        msgLen = msgLen + ask_cnt8.getSize()
        ask_cnt8.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: bid_prc8.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: bid_prc8.getSize())
        msgLen = msgLen + bid_prc8.getSize()
        bid_prc8.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: bid_vol8.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: bid_vol8.getSize())
        msgLen = msgLen + bid_vol8.getSize()
        bid_vol8.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: bid_cnt8.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: bid_cnt8.getSize())
        msgLen = msgLen + bid_cnt8.getSize()
        bid_cnt8.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ask_prc9.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ask_prc9.getSize())
        msgLen = msgLen + ask_prc9.getSize()
        ask_prc9.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ask_vol9.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ask_vol9.getSize())
        msgLen = msgLen + ask_vol9.getSize()
        ask_vol9.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ask_cnt9.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ask_cnt9.getSize())
        msgLen = msgLen + ask_cnt9.getSize()
        ask_cnt9.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: bid_prc9.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: bid_prc9.getSize())
        msgLen = msgLen + bid_prc9.getSize()
        bid_prc9.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: bid_vol9.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: bid_vol9.getSize())
        msgLen = msgLen + bid_vol9.getSize()
        bid_vol9.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: bid_cnt9.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: bid_cnt9.getSize())
        msgLen = msgLen + bid_cnt9.getSize()
        bid_cnt9.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ask_prc10.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ask_prc10.getSize())
        msgLen = msgLen + ask_prc10.getSize()
        ask_prc10.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ask_vol10.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ask_vol10.getSize())
        msgLen = msgLen + ask_vol10.getSize()
        ask_vol10.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ask_cnt10.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ask_cnt10.getSize())
        msgLen = msgLen + ask_cnt10.getSize()
        ask_cnt10.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: bid_prc10.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: bid_prc10.getSize())
        msgLen = msgLen + bid_prc10.getSize()
        bid_prc10.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: bid_vol10.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: bid_vol10.getSize())
        msgLen = msgLen + bid_vol10.getSize()
        bid_vol10.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: bid_cnt10.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: bid_cnt10.getSize())
        msgLen = msgLen + bid_cnt10.getSize()
        bid_cnt10.setValue(byteArray: temp)
        
        
    }
    
    public func toString() -> String {
        return (timestamp         .toString()  +
            symbol            .toString()  +
            prc_precision     .toString()  +
            qty_precision     .toString()  +
            ask_total_vol     .toString()  +
            ask_total_cnt     .toString()  +
            bid_total_vol     .toString()  +
            bid_total_cnt     .toString()  +
            ask_prc1          .toString()  +
            ask_vol1          .toString()  +
            ask_cnt1          .toString()  +
            bid_prc1          .toString()  +
            bid_vol1          .toString()  +
            bid_cnt1          .toString()  +
            ask_prc2          .toString()  +
            ask_vol2          .toString()  +
            ask_cnt2          .toString()  +
            bid_prc2          .toString()  +
            bid_vol2          .toString()  +
            bid_cnt2          .toString()  +
            ask_prc3          .toString()  +
            ask_vol3          .toString()  +
            ask_cnt3          .toString()  +
            bid_prc3          .toString()  +
            bid_vol3          .toString()  +
            bid_cnt3          .toString()  +
            ask_prc4          .toString()  +
            ask_vol4          .toString()  +
            ask_cnt4          .toString()  +
            bid_prc4          .toString()  +
            bid_vol4          .toString()  +
            bid_cnt4          .toString()  +
            ask_prc5          .toString()  +
            ask_vol5          .toString()  +
            ask_cnt5          .toString()  +
            bid_prc5          .toString()  +
            bid_vol5          .toString()  +
            bid_cnt5          .toString()  +
            ask_prc6          .toString()  +
            ask_vol6          .toString()  +
            ask_cnt6          .toString()  +
            bid_prc6          .toString()  +
            bid_vol6          .toString()  +
            bid_cnt6          .toString()  +
            ask_prc7          .toString()  +
            ask_vol7          .toString()  +
            ask_cnt7          .toString()  +
            bid_prc7          .toString()  +
            bid_vol7          .toString()  +
            bid_cnt7          .toString()  +
            ask_prc8          .toString()  +
            ask_vol8          .toString()  +
            ask_cnt8          .toString()  +
            bid_prc8          .toString()  +
            bid_vol8          .toString()  +
            bid_cnt8          .toString()  +
            ask_prc9          .toString()  +
            ask_vol9          .toString()  +
            ask_cnt9          .toString()  +
            bid_prc9          .toString()  +
            bid_vol9          .toString()  +
            bid_cnt9          .toString()  +
            ask_prc10         .toString()  +
            ask_vol10         .toString()  +
            ask_cnt10         .toString()  +
            bid_prc10         .toString()  +
            bid_vol10         .toString()  +
            bid_cnt10         .toString()  )
    }
    
    
    private func writeObject(out: OutputStream){
        // out.write(<#T##buffer: UnsafePointer<UInt8>##UnsafePointer<UInt8>#>, maxLength: <#T##Int#>)
        
    }
    
    
    public func ToBytes() -> [UInt8] {
        
        strBuffer =  timestamp         .toString()  +
            symbol            .toString()  +
            prc_precision     .toString()  +
            qty_precision     .toString()  +
            ask_total_vol     .toString()  +
            ask_total_cnt     .toString()  +
            bid_total_vol     .toString()  +
            bid_total_cnt     .toString()  +
            ask_prc1          .toString()  +
            ask_vol1          .toString()  +
            ask_cnt1          .toString()  +
            bid_prc1          .toString()  +
            bid_vol1          .toString()  +
            bid_cnt1          .toString()  +
            ask_prc2          .toString()  +
            ask_vol2          .toString()  +
            ask_cnt2          .toString()  +
            bid_prc2          .toString()  +
            bid_vol2          .toString()  +
            bid_cnt2          .toString()  +
            ask_prc3          .toString()  +
            ask_vol3          .toString()  +
            ask_cnt3          .toString()  +
            bid_prc3          .toString()  +
            bid_vol3          .toString()  +
            bid_cnt3          .toString()  +
            ask_prc4          .toString()  +
            ask_vol4          .toString()  +
            ask_cnt4          .toString()  +
            bid_prc4          .toString()  +
            bid_vol4          .toString()  +
            bid_cnt4          .toString()  +
            ask_prc5          .toString()  +
            ask_vol5          .toString()  +
            ask_cnt5          .toString()  +
            bid_prc5          .toString()  +
            bid_vol5          .toString()  +
            bid_cnt5          .toString()  +
            ask_prc6          .toString()  +
            ask_vol6          .toString()  +
            ask_cnt6          .toString()  +
            bid_prc6          .toString()  +
            bid_vol6          .toString()  +
            bid_cnt6          .toString()  +
            ask_prc7          .toString()  +
            ask_vol7          .toString()  +
            ask_cnt7          .toString()  +
            bid_prc7          .toString()  +
            bid_vol7          .toString()  +
            bid_cnt7          .toString()  +
            ask_prc8          .toString()  +
            ask_vol8          .toString()  +
            ask_cnt8          .toString()  +
            bid_prc8          .toString()  +
            bid_vol8          .toString()  +
            bid_cnt8          .toString()  +
            ask_prc9          .toString()  +
            ask_vol9          .toString()  +
            ask_cnt9          .toString()  +
            bid_prc9          .toString()  +
            bid_vol9          .toString()  +
            bid_cnt9          .toString()  +
            ask_prc10         .toString()  +
            ask_vol10         .toString()  +
            ask_cnt10         .toString()  +
            bid_prc10         .toString()  +
            bid_vol10         .toString()  +
            bid_cnt10         .toString()
        
        let buf: [UInt8] = Array(strBuffer.utf8)
        
        return buf
    }
    
    
    public func toJSON() -> (NSArray, NSArray) {
        
        var tempAskPrcArray = [String]()
        var tempAskVolArray = [String]()
        
        var tempDidPrcArray = [String]()
        var tempDidVolArray = [String]()
        
        tempAskPrcArray.append(ask_prc1.toString())
        tempAskPrcArray.append(ask_prc2.toString())
        tempAskPrcArray.append(ask_prc3.toString())
        tempAskPrcArray.append(ask_prc4.toString())
        tempAskPrcArray.append(ask_prc5.toString())
        tempAskPrcArray.append(ask_prc6.toString())
        tempAskPrcArray.append(ask_prc7.toString())
        tempAskPrcArray.append(ask_prc8.toString())
        tempAskPrcArray.append(ask_prc9.toString())
        tempAskPrcArray.append(ask_prc10.toString())
        
        tempAskVolArray.append(ask_vol1.toString())
        tempAskVolArray.append(ask_vol2.toString())
        tempAskVolArray.append(ask_vol3.toString())
        tempAskVolArray.append(ask_vol4.toString())
        tempAskVolArray.append(ask_vol5.toString())
        tempAskVolArray.append(ask_vol6.toString())
        tempAskVolArray.append(ask_vol7.toString())
        tempAskVolArray.append(ask_vol8.toString())
        tempAskVolArray.append(ask_vol9.toString())
        tempAskVolArray.append(ask_vol10.toString())
        
        tempDidPrcArray.append(bid_prc1.toString())
        tempDidPrcArray.append(bid_prc2.toString())
        tempDidPrcArray.append(bid_prc3.toString())
        tempDidPrcArray.append(bid_prc4.toString())
        tempDidPrcArray.append(bid_prc5.toString())
        tempDidPrcArray.append(bid_prc6.toString())
        tempDidPrcArray.append(bid_prc7.toString())
        tempDidPrcArray.append(bid_prc8.toString())
        tempDidPrcArray.append(bid_prc9.toString())
        tempDidPrcArray.append(bid_prc10.toString())
        
        tempDidVolArray.append(bid_vol1.toString())
        tempDidVolArray.append(bid_vol2.toString())
        tempDidVolArray.append(bid_vol3.toString())
        tempDidVolArray.append(bid_vol4.toString())
        tempDidVolArray.append(bid_vol5.toString())
        tempDidVolArray.append(bid_vol6.toString())
        tempDidVolArray.append(bid_vol7.toString())
        tempDidVolArray.append(bid_vol8.toString())
        tempDidVolArray.append(bid_vol9.toString())
        tempDidVolArray.append(bid_vol10.toString())
        
        var selljsonvalueArray : NSArray?
        var buyjsonvalueArray : NSArray?
        
        buyjsonvalueArray = createDic(prcArr: tempDidPrcArray, volArr: tempDidVolArray, value: "bid")
        selljsonvalueArray = createDic(prcArr: tempAskPrcArray, volArr: tempAskVolArray, value: "ask")
        
        return (buyjsonvalueArray!, selljsonvalueArray!)
    }
    
    func arrayCopy(arr: [UInt8], start: Int, arr2: [UInt8], index: Int, len: Int) -> [UInt8]{
        var i = index
        var temp = arr2
        let tempLen = start + len
        for row in start..<tempLen {
            temp[i] = arr[row]
            i = i + 1
        }
       // print(temp)
        return temp
    }
    
    func createDic(prcArr : [String], volArr: [String], value: String) -> NSArray {
        print("RT price real")
        
        var tempArray = NSMutableArray()
        
        for row in 0...9 {
            
            let sPrice = prcArr[row]
            let vol = volArr[row]
            print("RT price real vol : \(vol)")
            print("RT price real vol : \(vol)")
            
            let plist = UserDefaults.standard
            let market = plist.string(forKey: "marketSimbol")
            
            if market == "BTC" {
                print("RT : BTC market")
                krwPrice = Int(AppDelegate.BTC)
                print("RT krwPrice : \(krwPrice)")
            }else if market == "ETH" {
                print("RT : ETH market")
                krwPrice = Int(AppDelegate.ETH)
            }
            
            if let price = Int(sPrice) {
                let perPrice = Float(price)
                let perVol = Float(vol)
                
                var percet = ((perPrice * Float(krwPrice) * perVol!) / AppDelegate.defaulPrice ) * 100.0
                
                if perPrice * perVol! > AppDelegate.defaulPrice {
                    percet = 100.0
                }
                
                print("RT percet : \(Int(percet))")
                var dic = [String : String]()
                
                dic.updateValue(value, forKey: "dealType")
                dic.updateValue("\(row + 1)", forKey: "order")
                dic.updateValue(String(describing: price), forKey: "price")
                dic.updateValue(String(describing: perVol!), forKey: "qty")
                
                print("RT price real Price : \(row+1), \(perVol!)")
                dic.updateValue(String(Int(percet)), forKey: "percent")
                
                tempArray[row] = dic
                
            }else {
                
                let perPrice = Double(sPrice)
                let perVol = Double(vol)
                print("RT price real double : \(row+1), \(perVol!)")
                
                
                var percet = ((perPrice! * Double(krwPrice) * perVol! ) / AppDelegate.defaulDoublePrice ) * 100.0
                
                if perPrice! * perVol! > AppDelegate.defaulDoublePrice {
                    percet = 100.0
                }
                
                print("RT percet : \(Int(percet))")
                var dic = [String : String]()
                
                dic.updateValue(value, forKey: "dealType")
                dic.updateValue("\(row + 1)", forKey: "order")
                dic.updateValue(String(describing: perPrice!), forKey: "price")
                dic.updateValue(String(describing: perVol!), forKey: "qty")
                dic.updateValue(String(Int(percet)), forKey: "percent")
                
                tempArray[row] = dic
                
            }
            
        }
        
        return tempArray
        
    }
    
    
}
