//
//  SocketManger.swift
//  crevolc
//
//  Created by crovolc on 2018. 2. 5..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import UIKit
import Foundation
import SwiftSocket

class SocketManger {
    
    static let sharedInstance = SocketManger()

    var client : TCPClient?

    func connect(host : String, port : String ) {

        client = TCPClient(address: host, port: Int32(port)!)

        switch client?.connect(timeout: 10) {
        case .success?:
            
            break
        default:

            break
        }
    }
    
}
