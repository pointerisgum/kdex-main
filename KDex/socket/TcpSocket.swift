//
//  TcpSocket.swift
//  crevolc
//
//  Created by crovolc on 2018. 2. 5..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation

class TcpSocket: NSObject, StreamDelegate {
    
    static let sharedInstance = TcpSocket()
    
    var host:String?
    var port:Int?
    var inputStream: InputStream?
    var outputStream: OutputStream?
    var receiverThread : ReceiverThread?
    
    func connect(host: String, port: Int, uid: String) {
        print("tcpsocket connect")
        
        self.host = host
        self.port = port
        
        Stream.getStreamsToHost(withName: host, port : port, inputStream: &inputStream, outputStream: &outputStream)
        
        if inputStream != nil && outputStream != nil {
            
            // Set delegate
            inputStream!.delegate = self
            outputStream!.delegate = self
            
            // Schedule
            inputStream!.schedule(in: RunLoop.current, forMode: RunLoop.Mode.default)
            outputStream!.schedule(in: RunLoop.current, forMode: RunLoop.Mode.default)
            
            print("Start open()")
            
            // Open!
            inputStream!.open()
            outputStream!.open()
            
            print(initsend(uid: uid))
            
            print("recive start")
            DispatchQueue.global(qos: .utility).async {
                self.receiverThread = ReceiverThread(socket: TcpSocket.sharedInstance)
                self.receiverThread?.run(input: self.inputStream!, output: self.outputStream!)

            }
        }else {
        // connet failed 
            
            
        }
    }
    
    func send(data: Data) -> Int {
        let bytesWritten = data.withUnsafeBytes { outputStream?.write($0, maxLength: data.count) }
        return bytesWritten!
    }
    
    func initsend(uid: String) -> Int {
        let TMSHDR_SIZE = 100
        let TMSHDR_SZ_LEN = 10
        
        let TMS_HEADER_FORMAT = "%010d%-5s%-6s%-10s%-10s%-5s%-44s%-10s"
        //let TMS_SIGNON_FORMAT = "%010d%-5s%-6s%-10s%-10s%-5s%-44s%-10s%-8s%-8s%-8s%-8s"
        let TMS_SIGNON_FORMAT = "%010d"
        
        let len = TMSHDR_SIZE - TMSHDR_SZ_LEN + 32
        let tr = "LOGIN"
        let service_name = " "
        let clid = " "
        let cd = " "
        let errcd = " "
        let sreserved = " "
        let creserved = " "
        let userid = uid
        let cltname = uid
        let dompwd = " "
        let usrpwd = " "
        
//        let str1 =
//        let str2 = tr.streamStr(num: 5, str: tr)
//
        let str = "\(String(format: TMS_SIGNON_FORMAT, len))\(tr.streamStr(num: 5, str: tr))\(service_name.streamStr(num: 6, str: service_name))\(clid.streamStr(num: 10, str: clid))\(cd.streamStr(num: 10, str: cd))\(errcd.streamStr(num: 5, str: errcd))\(sreserved.streamStr(num: 44, str: sreserved))\(creserved.streamStr(num: 10, str: creserved))\(userid.streamStr(num: 8, str: userid))\(cltname.streamStr(num: 8, str: cltname))\(dompwd.streamStr(num: 8, str: dompwd))\(usrpwd.streamStr(num: 8, str: usrpwd))"
    
//        let str = String(format: TMS_SIGNON_FORMAT, len, tr, service_name, clid, cd,errcd, sreserved, creserved,userid, cltname, dompwd, usrpwd)

      //  print(str)
        guard let data = str.data(using: String.Encoding.utf8, allowLossyConversion: true) else{
            return 0
        }
        
        let bytesWritten = data.withUnsafeBytes { outputStream?.write($0, maxLength: data.count) }
        return bytesWritten!
    }
    
    //    func send(data: String) -> Int {
    //        let bytesWritten = outputStream?.write(data, maxLength:data.characters.count)
    //        return bytesWritten!
    //    }
    
    func recv(buffersize: Int) -> Data {
        print("recive")
        var buffer = [UInt8](repeating :0, count : buffersize)
        
        let bytesRead = inputStream?.read(&buffer, maxLength: buffersize)
        var dropCount = buffersize - bytesRead!
        if dropCount < 0 {
            dropCount = 0
        }
        let chunk = buffer.dropLast(dropCount)
        return Data(chunk)
    }
    
    func customrecv(buffersize: Int, len : Int) -> Data {
        print("recive")
        let bount = 0
        
        //while ( bcount < len){
        var buffer = [UInt8](repeating :0, count : buffersize)
        
        let bytesRead = inputStream?.read(&buffer, maxLength: buffersize)
        
        var dropCount = buffersize - bytesRead!
        if dropCount < 0 {
            dropCount = 0
        }
        let chunk = buffer.dropLast(dropCount)
        return Data(chunk)
    }
    
    func disconnect() {
        inputStream?.close()
        outputStream?.close()
        
        
    }
    
    func stream(_ stream: Stream, handle eventCode: Stream.Event) {
        
        print("event:\(eventCode)")
        
        if stream === inputStream {
            switch eventCode {
            case Stream.Event.errorOccurred:
                print("inputStream:ErrorOccurred")
            case Stream.Event.openCompleted:
                print("inputStream:OpenCompleted")
            case Stream.Event.hasBytesAvailable:
                print("inputStream:HasBytesAvailable")
            default:
                break
            }
        }
            
        else if stream === outputStream {
            switch eventCode {
                case Stream.Event.errorOccurred:
                    print("outputStream:ErrorOccurred")
                case Stream.Event.openCompleted:
                    print("outputStream:OpenCompleted")
                case Stream.Event.hasSpaceAvailable:
                    print("outputStream:HasSpaceAvailable")
            default:
                break
            }
        }
    }
    
    /*
     사용법
     let socket = TcpSocket()
    
     ....
     
     socket.connect(host: "118.34.167.155", port: 5552)
     
     
     let query = "HELLO SWIFT SOCKET!"
     let dataQuery = query.data(using: String.Encoding.utf8, allowLossyConversion: true)
     let sentCount = socket.send(data: dataQuery!)
     //let sentCount = socket.send(data: query)
     print("sentCount : \(sentCount)")
     
     let buffersize = 1024
     let chunk = socket.recv(buffersize: buffersize)
     
     var getString : String?
     
     if(chunk.count > 0){
     getString = String(bytes: chunk, encoding: String.Encoding.utf8)!
     print("received : \(getString!)")
     }
     
     connect.disconnect()
 
     */
    
}
