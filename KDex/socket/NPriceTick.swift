//
//  NPriceTick.swift
//  DEXnPAY
//
//  Created by park heewon on 2018. 3. 21..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation

class NPriceTick: NSObject {
    
    public var timestamp         = NString(size: 20)
    public var symbol            = NString(size: 32)
    public var last_price        = NString(size: 15)
    public var last_volume       = NString(size: 15)
    public var last_amount       = NString(size: 15)
    public var ask_price         = NString(size: 15)
    public var bid_price         = NString(size: 15)
    public var open_price        = NString(size: 15)
    public var high_price        = NString(size: 15)
    public var low_price         = NString(size: 15)
    public var close_price       = NString(size: 15)
    public var total_volume      = NString(size: 15)
    public var total_amount      = NString(size: 20)
    public var delta_sign        = NString(size:  2)
    public var delta_price       = NString(size: 15)
    public var delta_rate        = NString(size: 15)
    public var prc_precision     = NString(size: 10)
    public var qty_precision     = NString(size: 10)
    public var highest_price     = NString(size: 15)
    public var lowest_price      = NString(size: 15)
    public var highest_timestamp = NString(size: 20)
    public var lowest_timestamp  = NString(size: 20)
    
    public var strBuffer = ""
    
    override init() {
        super.init()
    }
    
    public func reset(){
        timestamp         .reset()
        symbol            .reset()
        last_price        .reset()
        last_volume       .reset()
        last_amount       .reset()
        ask_price         .reset()
        bid_price         .reset()
        open_price        .reset()
        high_price        .reset()
        low_price         .reset()
        close_price       .reset()
        total_volume      .reset()
        total_amount      .reset()
        delta_sign        .reset()
        delta_price       .reset()
        delta_rate        .reset()
        prc_precision     .reset()
        qty_precision     .reset()
        highest_price     .reset()
        lowest_price      .reset()
        highest_timestamp .reset()
        lowest_timestamp  .reset()
    }
    
    public func parseData(bt: [UInt8]){
        var msgLen = 0
        
        var tempArr = [UInt8](repeating: 0x00, count: timestamp.getSize())
        var temp = [UInt8]()
        
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: timestamp.getSize())
        msgLen = msgLen + timestamp.getSize()
        timestamp.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: symbol.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: symbol.getSize())
        msgLen = msgLen + symbol.getSize()
        symbol.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: last_price.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: last_price.getSize())
        msgLen = msgLen + last_price.getSize()
        last_price.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: last_volume.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: last_volume.getSize())
        msgLen = msgLen + last_volume.getSize()
        last_volume.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: last_amount.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: last_amount.getSize())
        msgLen = msgLen + last_amount.getSize()
        last_amount.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: ask_price.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: ask_price.getSize())
        msgLen = msgLen + ask_price.getSize()
        ask_price.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: bid_price.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: bid_price.getSize())
        msgLen = msgLen + bid_price.getSize()
        bid_price.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: open_price.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: open_price.getSize())
        msgLen = msgLen + open_price.getSize()
        open_price.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: high_price.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: high_price.getSize())
        msgLen = msgLen + high_price.getSize()
        high_price.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: low_price.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: low_price.getSize())
        msgLen = msgLen + low_price.getSize()
        low_price.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: close_price.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: close_price.getSize())
        msgLen = msgLen + close_price.getSize()
        close_price.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: total_volume.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: total_volume.getSize())
        msgLen = msgLen + total_volume.getSize()
        total_volume.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: total_amount.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: total_amount.getSize())
        msgLen = msgLen + total_amount.getSize()
        total_amount.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: delta_sign.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: delta_sign.getSize())
        msgLen = msgLen + delta_sign.getSize()
        delta_sign.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: delta_price.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: delta_price.getSize())
        msgLen = msgLen + delta_price.getSize()
        delta_price.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: delta_rate.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: delta_rate.getSize())
        msgLen = msgLen + delta_rate.getSize()
        delta_rate.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: prc_precision.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: prc_precision.getSize())
        msgLen = msgLen + prc_precision.getSize()
        prc_precision.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: qty_precision.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: qty_precision.getSize())
        msgLen = msgLen + qty_precision.getSize()
        qty_precision.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: highest_price.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: highest_price.getSize())
        msgLen = msgLen + highest_price.getSize()
        highest_price.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: lowest_price.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: lowest_price.getSize())
        msgLen = msgLen + lowest_price.getSize()
        lowest_price.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: highest_timestamp.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: highest_timestamp.getSize())
        msgLen = msgLen + highest_timestamp.getSize()
        highest_timestamp.setValue(byteArray: temp)
        
        temp.removeAll()
        tempArr = [UInt8](repeating: 0x00, count: lowest_timestamp.getSize())
        temp = arrayCopy(arr: bt, start:msgLen , arr2: tempArr, index: 0, len: lowest_timestamp.getSize())
        msgLen = msgLen + lowest_timestamp.getSize()
        lowest_timestamp.setValue(byteArray: temp)
        
        
    }
   
    func arrayCopy(arr: [UInt8], start: Int, arr2: [UInt8], index: Int, len: Int) -> [UInt8]{
        var i = index
        var temp = arr2
        let tempLen = start + len
        for row in start..<tempLen {
            temp[i] = arr[row]
            i = i + 1
        }
      //  print(temp)
        return temp
    }
    
    public func toString() -> String {
        return (timestamp         .toString()  +
            symbol            .toString()  +
            last_price        .toString()  +
            last_volume       .toString()  +
            last_amount       .toString()  +
            ask_price         .toString()  +
            bid_price         .toString()  +
            open_price        .toString()  +
            high_price        .toString()  +
            low_price         .toString()  +
            close_price       .toString()  +
            total_volume      .toString()  +
            total_amount      .toString()  +
            delta_sign        .toString()  +
            delta_price       .toString()  +
            delta_rate        .toString()  +
            prc_precision     .toString()  +
            qty_precision     .toString()  +
            highest_price     .toString()  +
            lowest_price      .toString()  +
            highest_timestamp .toString()  +
            lowest_timestamp  .toString())
        
    }
    
    public func toBytes() -> [UInt8]{
        
        strBuffer = timestamp         .toString()  +
            symbol            .toString()  +
            last_price        .toString()  +
            last_volume       .toString()  +
            last_amount       .toString()  +
            ask_price         .toString()  +
            bid_price         .toString()  +
            open_price        .toString()  +
            high_price        .toString()  +
            low_price         .toString()  +
            close_price       .toString()  +
            total_volume      .toString()  +
            total_amount      .toString()  +
            delta_sign        .toString()  +
            delta_price       .toString()  +
            delta_rate        .toString()  +
            prc_precision     .toString()  +
            qty_precision     .toString()  +
            highest_price     .toString()  +
            lowest_price      .toString()  +
            highest_timestamp .toString()  +
            lowest_timestamp  .toString()
        
        let buf: [UInt8] = Array(strBuffer.utf8)
        
        return buf
    }
    
    private func writeObject(out: OutputStream){
        
    }
    
    public func toJSON() -> String {
        
       return (timestamp         .toString()  +
            symbol            .toString()  +
            last_price        .toString()  +
            last_volume       .toString()  +
            last_amount       .toString()  +
            ask_price         .toString()  +
            bid_price         .toString()  +
            open_price        .toString()  +
            high_price        .toString()  +
            low_price         .toString()  +
            close_price       .toString()  +
            total_volume      .toString()  +
            total_amount      .toString()  +
            delta_sign        .toString()  +
            delta_price       .toString()  +
            delta_rate        .toString()  +
            prc_precision     .toString()  +
            qty_precision     .toString()  +
            highest_price     .toString()  +
            lowest_price      .toString()  +
            highest_timestamp .toString()  +
            lowest_timestamp  .toString()  )
     
    }
   
}
