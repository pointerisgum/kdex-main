//
//  CustomTockenAddController.swift
//  KDex
//
//  Created by park heewon on 2018. 7. 5..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import Kingfisher

class CustomTockenAddController: UIViewController, UITableViewDelegate, UITableViewDataSource, TokenOpenAble, UITextFieldDelegate {
    
    let activity = UIActivityIndicatorView()
    
    var tokenArray = [[String: Any]]()
    
    private var titleSortBool = true
    private var volSortBool = true
    private var selectSortBool = true
    
    @IBOutlet var customTkFd: UITextField!
    
    @IBOutlet var customTokenCnt: UILabel!
    
    @IBOutlet var closeBtn: UIButton!
    
//    @IBOutlet var selectBtn: UIButton!
    
    let imgVi : UIImageView = {
        let vi = UIImageView()
        let img = UIImage(named: "zoom")
        vi.image = img
        return vi
    }()
    
    let rtVi : UIView = {
        let vi = UIView()
        return vi
    }()
    
    let lineTop : UIView = {
        let vi = UIView()
        vi.backgroundColor = UIColor(rgb: 0xd1d1d1)
        vi.translatesAutoresizingMaskIntoConstraints = false
        return vi
    }()
    
    let lineBotton : UIView = {
        let vi = UIView()
        vi.backgroundColor = UIColor(rgb: 0xd1d1d1)
        vi.translatesAutoresizingMaskIntoConstraints = false
        return vi
    }()
    
    let headerVi : UIView = {
        let uv = UIView()
        uv.backgroundColor = UIColor.white
        return uv
    }()
    
    let AsortBtn : UIButton = {
        let bt = UIButton()
        let image = UIImage(named: "icn_list_sort_deactive-1")
        bt.backgroundColor = UIColor.clear
        bt.translatesAutoresizingMaskIntoConstraints = false
        bt.setAttributedTitle(NSAttributedString(string: "wallet_manage_token".localized, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor: UIColor.DPmainTextColor]), for: .normal)
        bt.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -95)
        bt.titleEdgeInsets = UIEdgeInsets(top: 0, left: -45, bottom: 0, right: 0)
        bt.addTarget(self, action:#selector(titleSort) , for: .touchUpInside)
        bt.setImage(image, for: .normal)
        
        return bt
    }()
    
    let BsortBtn : UIButton = {
        let bt = UIButton()
        let image = UIImage(named: "icn_list_sort_deactive-1")
        bt.backgroundColor = UIColor.clear
        bt.translatesAutoresizingMaskIntoConstraints = false
        bt.setAttributedTitle(NSAttributedString(string: "bid_ask_total_qty".localized, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor: UIColor.DPmainTextColor]), for: .normal)
        bt.addTarget(self, action: #selector(volSort), for: .touchUpInside)
        bt.setImage(image, for: .normal)
        bt.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -95)
        bt.titleEdgeInsets = UIEdgeInsets(top: 0, left: -45, bottom: 0, right: 0)
        
        return bt
    }()
    
    let CsortBtn : UIButton = {
        let bt = UIButton()
        let image = UIImage(named: "icn_list_sort_deactive-1")
        bt.backgroundColor = UIColor.clear
        bt.translatesAutoresizingMaskIntoConstraints = false
        bt.setAttributedTitle(NSAttributedString(string: "send_add".localized, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor: UIColor.DPmainTextColor]), for: .normal)
        bt.addTarget(self, action: #selector(selectSort), for: .touchUpInside)
        bt.setImage(image, for: .normal)
        bt.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -45)
        bt.titleEdgeInsets = UIEdgeInsets(top: 0, left: -45, bottom: 0, right: 0)
        
        return bt
    }()
    
    
    @IBOutlet var tokenListTb: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
        
        tokenListTb.delegate = self
        tokenListTb.dataSource = self
        tokenListTb.register(UINib(nibName: "tokenListCell", bundle: nil), forCellReuseIdentifier: "tokenListCell")
        tokenListTb.separatorStyle = .none
        
        headerVi.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 40)
        
        
        headerVi.addSubview(AsortBtn)
        headerVi.addSubview(BsortBtn)
        headerVi.addSubview(CsortBtn)
        headerVi.addSubview(lineTop)
        headerVi.addSubview(lineBotton)
        
        setupLayout()
        
        showAnimate()
        
        textfieldViewSetup()
        
        
        activity.frame = CGRect(x: self.view.frame.width/2 - 50, y: self.tokenListTb.frame.height/2 - 30 , width: 100, height: 100)
        activity.style = .whiteLarge
        activity.color = UIColor.DPActivityColor
        activity.hidesWhenStopped = true
        tokenListTb.addSubview(activity)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        walletTokenList()
        customTkFd.delegate = self
        customTkFd.addTarget(self, action: #selector(textChangeAction), for: .editingChanged)
        
    }
    
    func setupLayout() {
        
        lineTop.leadingAnchor.constraint(equalTo: headerVi.leadingAnchor).isActive = true
        lineTop.topAnchor.constraint(equalTo: headerVi.topAnchor).isActive = true
        lineTop.trailingAnchor.constraint(equalTo: headerVi.trailingAnchor).isActive = true
        lineTop.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        AsortBtn.leftAnchor.constraint(equalTo: headerVi.leftAnchor, constant: 35).isActive = true
        AsortBtn.widthAnchor.constraint(equalToConstant: 60).isActive = true
        AsortBtn.topAnchor.constraint(equalTo: headerVi.topAnchor, constant: 5).isActive = true
        
        CsortBtn.rightAnchor.constraint(equalTo: headerVi.rightAnchor, constant: 5).isActive = true
        CsortBtn.widthAnchor.constraint(equalToConstant: 70).isActive = true
        CsortBtn.topAnchor.constraint(equalTo: headerVi.topAnchor, constant: 5).isActive = true
        
        BsortBtn.rightAnchor.constraint(equalTo: CsortBtn.leftAnchor, constant: -20).isActive = true
        BsortBtn.widthAnchor.constraint(equalToConstant: 40).isActive = true
        BsortBtn.topAnchor.constraint(equalTo: headerVi.topAnchor, constant: 5).isActive = true
        
        lineBotton.leadingAnchor.constraint(equalTo: headerVi.leadingAnchor).isActive = true
        lineBotton.bottomAnchor.constraint(equalTo: headerVi.bottomAnchor).isActive = true
        lineBotton.trailingAnchor.constraint(equalTo: headerVi.trailingAnchor).isActive = true
        lineBotton.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
    }
    
    func showAnimate() {
        
        self.view.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.25) {
            
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform.init(scaleX: 1.0, y: 1.0)
            
        }
        
    }
    
    @IBAction func closeBtnAction(_ sender: Any) {
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0
        }) { (finished: Bool) in
            if(finished)
            {
                self.view.removeFromSuperview()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tokenArray .count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let walletJson = self.tokenArray [indexPath.row]
        
        let simbole = walletJson["symbol"] as? String ?? ""
        
        let status = walletJson["status"] as? String ?? ""
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "tokenListCell", for: indexPath) as? tokenListCell else {
            return UITableViewCell()
        }
        
        if status == "Y" {
            cell.coinSwitch.setOn(true, animated: false)
        }else {
            cell.coinSwitch.setOn(false, animated: false)
        }
        cell.coinSwitch.tag = indexPath.row
        cell.tokenDelegate = self
        cell.coinNameLabel.text = "\(walletJson["symbolName"]!)"
        cell.coinSimbol.text = "\(simbole)"
        cell.coinNameLabel.adjustsFontSizeToFitWidth = true
        cell.coinNameLabel.sizeToFit()
        
//        DispatchQueue.main.async {
//            cell.coinImage.image = AppDelegate.dataImage(str: )
//        }
        cell.coinImage.kf.setImage(with: AppDelegate.dataImageUrl(str: "\(walletJson["symbol"]!)" ))
        
        
//        cell.coinImage.image = UIImage(named: "\(walletJson["symbol"]!)")
        cell.coinValueLabel.text = "\(walletJson["dpoQty"]!)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 70
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return self.headerVi
    }
    
//    @IBAction func selectBtnAction(_ sender: Any) {
//
//        walletTokenList()
//
//        self.showToast(message: "토큰이 적용되었습니다.", textcolor: UIColor.white, frame: CGRect(x: self.view.frame.size.width/2 - 100, y: self.view.frame.size.height - 100  , width: 200, height: 35), time: 1, animation: true)
//    }
//
    func openToken() {
        
    }
    
    func selectTokenSwAction(sender: UISwitch) {
        
        let selectBool = sender.isOn
        
        print("selectToken tag : \(sender.tag)")
        
        let walletJson = self.tokenArray[sender.tag]
        
        print("selecttoken walletjson : \(walletJson)")
        
        let simbole = walletJson["symbol"] as? String ?? ""
        
        var url = ""
        
        if selectBool {
            url = "\(AppDelegate.url)/auth/procToken?symbol=\(simbole)&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&status=N"
            self.favoitesGetHttpAction(url: url)
        }else {
            url = "\(AppDelegate.url)/auth/procToken?symbol=\(simbole)&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&status=D"
            self.favoitesGetHttpAction(url: url)
        }
        
    }
    
    func favoitesGetHttpAction(url : String){
        
        print("favorite select url : \(url)")
        
        let tempurl = url as NSString
        
        let safeURL = tempurl.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
        
        let amo = Alamofire.request("\(safeURL)")
        amo.responseJSON(completionHandler: { response in
            
            guard let tempArray = response.result.value as? [String : Any] else {
                return
            }
            
            print("result = \(tempArray["returnMsg"] as? String)")
            
        })
        
    }
    
    func walletTokenList() {
        
        activity.startAnimating()
        
        let url = "\(AppDelegate.url)/auth/getTokenList?simbol&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&"
        
        print("gettoken walleturl \(url)")
        let amo = Alamofire.request(url)
        amo.responseJSON(completionHandler: { response in
            
            guard let tempArray = response.result.value as? [[String : Any]] else {
                return
            }
            
            AppDelegate.tokenWalletListArray = tempArray
            
            self.tokenArray = tempArray
            
            self.tokenCntAction()
            
            self.tokenListTb.reloadData()
            
            self.activity.stopAnimating()
            
        })
    }
    
    func sendWalletViAction(value: [String : Any]) {
        
    }
    
    @objc func titleSort(){
        
        if titleSortBool {
            titleSortBool = false
            
            let arr = try self.tokenArray.sorted(by:{
                guard let num1 = $0["symbolName"] as? String else {
                    return true
                }
                
                guard let num2 = $1["symbolName"] as? String else {
                    return true
                }
                
                return num1 > num2
                
            })
            
            self.tokenArray = arr
            tokenListTb.reloadData();
        }else {
            titleSortBool = true
            
            let arr = try self.tokenArray.sorted(by:{
                guard let num1 = $0["symbolName"] as? String else {
                    return true
                }
                
                guard let num2 = $1["symbolName"] as? String else {
                    return true
                }
                
                return num1 < num2
                
            })
            
            self.tokenArray = arr
            tokenListTb.reloadData();
        }
        
    }
    
    @objc func volSort(){
        
        if volSortBool {
            volSortBool = false
            
            let arr = try self.tokenArray.sorted(by:{
                guard let num1 = $0["dpoQty"] as? String else {
                    return true
                }
                
                guard let num2 = $1["dpoQty"] as? String else {
                    return true
                }
                
                return num1 > num2
                
            })
            
            self.tokenArray  = arr
            tokenListTb.reloadData();
        }else {
            volSortBool = true
            
            let arr = self.tokenArray.sorted(by:{
                guard let num1 = $0["dpoQty"] as? String else {
                    return true
                }
                
                guard let num2 = $1["dpoQty"] as? String else {
                    return true
                }
                
                return num1 < num2
                
            })
            
            self.tokenArray  = arr
            tokenListTb.reloadData();
        }
        
    }
    
    @objc func selectSort(){
        
        if selectSortBool {
            selectSortBool = false
            
            let arr = try self.tokenArray.sorted(by:{
                guard let num1 = $0["status"] as? String else {
                    return true
                }
                
                guard let num2 = $1["status"] as? String else {
                    return true
                }
                
                return num1 > num2
                
            })
            
            self.tokenArray = arr
            tokenListTb.reloadData();
        }else {
            selectSortBool = true
            
            let arr = try self.tokenArray.sorted(by:{
                guard let num1 = $0["status"] as? String else {
                    return true
                }
                
                guard let num2 = $1["status"] as? String else {
                    return true
                }
                
                return num1 < num2
                
            })
            
            self.tokenArray = arr
            tokenListTb.reloadData();
        }
        
    }
    
    private func tokenCntAction() {
        let cnt = tokenArray.count
        
        let attrstr = NSMutableAttributedString(string: "total".localized, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.DPmainTextColor])
        
        attrstr.append(NSAttributedString(string: "\(cnt)", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.DPCompanyColor ]))
        
        attrstr.append(NSAttributedString(string: "wallet_count".localized, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.DPmainTextColor]))
        
        customTokenCnt.attributedText = attrstr
    }
    
    private func textfieldViewSetup(){
        
        rtVi.addSubview(imgVi)
        rtVi.frame = CGRect(x: 0, y: 0, width: self.customTkFd.frame.height, height: self.customTkFd.frame.height)
        imgVi.frame.size = CGSize(width: self.customTkFd.frame.height - 10, height: self.customTkFd.frame.height - 10)
        imgVi.center = rtVi.center
        
        customTkFd.rightView = rtVi
        customTkFd.rightViewMode = .always
        
        
//        self.priceTf.leftView = tfView
//        self.priceTf.leftViewMode = .always
//        self.priceTf.attributedPlaceholder = NSAttributedString(string: "가격", attributes: [NSAttributedString.Key.foregroundColor : UIColor.DPmainTextColor])
//        self.priceTf.backgroundColor = UIColor.DPTextFieldBgColor
//        self.priceTf.addBorderBottom(height: 1.0, width: self.priceTf.frame.width , color: UIColor.DPLineBgColor)
//        textfiledView(simName: self.marksimbol!, textFi: self.priceTf, rigthMode: true)
        
    }
    
    @objc func textChangeAction(){
        let txt = self.customTkFd.text ?? ""
        seachTokenAction(str: txt)
    }
    
    
    private func seachTokenAction(str : String){
        tokenArray.removeAll()
        
        if str == "" {
            tokenArray = AppDelegate.tokenWalletListArray
        }else {
            
            print("texter :\(str)")
            for row in AppDelegate.tokenWalletListArray {
                let simboleName = row["symbolName"] as? String ?? ""
                
                if simboleName.containsIgnoringCase(find: str){
                    tokenArray.append(row)
                }
            }
            
        }
        
        tokenCntAction()
        tokenListTb.reloadData();
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        customTkFd.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        print("tocken return ")
        self.view.endEditing(true)
        return true
    }

}

