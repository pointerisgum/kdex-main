//
//  LockPadController.swift
//  crevolc
//
//  Created by crovolc on 2018. 2. 6..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation
import UIKit
import LocalAuthentication

class LockPadController: UIViewController {
    
    var passwdConut = 0 {
        willSet{
            switch newValue {
            case 1:
                self.firstNum.backgroundColor = UIColor.DPbdidBgColor
                [secondNum, thridNum, fouthNum].forEach({ $0?.backgroundColor = UIColor.white })
            case 2:
                self.secondNum.backgroundColor = UIColor.DPbdidBgColor
                [thridNum, fouthNum].forEach({ $0?.backgroundColor = UIColor.white })
            case 3:
                self.thridNum.backgroundColor = UIColor.DPbdidBgColor
                self.fouthNum.backgroundColor = UIColor.white
            case 4:
                self.fouthNum.backgroundColor = UIColor.DPbdidBgColor
                fallthrough
            default:
                delay(0.2, completion: {
                    [self.firstNum, self.secondNum, self.thridNum, self.fouthNum].forEach({ $0?.backgroundColor = UIColor.white })
                })
              
            }
           
        }
    }
    
    var backandfore : Bool = false
    
    var passwdfine : Bool = true
    
    var lockis : Bool = false
    
    var repeatePasswd : Bool = false
    
    var passwdchange : Bool = false
    
    var passwdDone : Bool = false
    
    var passwd = [String]()
    
    @IBOutlet var firstNum: UIView!
    
    @IBOutlet var secondNum: UIView!
    
    @IBOutlet var thridNum: UIView!
    
    @IBOutlet var fouthNum: UIView!
    
    @IBOutlet var passwdLabel: UILabel!
    
    @IBOutlet var cancel: UIButton!
    
    @IBOutlet var touchIdBtn: UIButton!
    
    @IBAction func cancelBtn(_ sender: Any) {
        
        let plist = UserDefaults.standard
        plist.set(lockis, forKey: "appLock")
        plist.synchronize()
        self.dismiss(animated: true, completion: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let plist = UserDefaults.standard
        if plist.bool(forKey: "touchIdIs") {
            self.authenticateUser()
        }
        self.initstart()
    }
    
    @IBAction func touchIdAction(_ sender: Any) {
        self.authenticateUser()
    }
    
    
    @IBAction func oneAction(_ sender: UIButton) {
        
        passwdConut += 1
        print(passwdConut)
        
        if passwd.count < 5 {
            passwd.append((sender.titleLabel?.text!)!)
        }
        print(passwd)
        if passwdConut == 4 {
            let plist = UserDefaults.standard
            
            if lockis {
                self.passwordDone(passwd: passwd)
                if passwdfine {
                    //plist.removeObject(forKey: "lockPasswd")
                    self.dismiss(animated: true, completion: nil)
                    
                    if AppDelegate.didlunchLock {
                        AppDelegate.didlunchLock = false
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        
                        if let detail = storyboard.instantiateViewController(withIdentifier: "RevealViewCT") as? RevealViewController {
                            
                                self.present(detail, animated: true, completion: nil)
                        }
                        
                    }
                    
                } else {
                    passwdLabel.text = "password_mismatch".localized
                    [self.firstNum, self.secondNum, self.thridNum, self.fouthNum].forEach({ $0?.backgroundColor = UIColor.red })
                    delay(0.2, completion: {
                        print("delay")
                    })
                }
            }else if passwdchange {
                self.passwordDone(passwd: passwd)
                if passwdfine {
                    plist.removeObject(forKey: "lockPasswd")
                    passwdLabel.text = "password_pw_confirm_detail".localized
                    passwdchange = false
                    repeatePasswd = true
                } else {
                    passwdLabel.text = "password_mismatch".localized
                    [self.firstNum, self.secondNum, self.thridNum, self.fouthNum].forEach({ $0?.backgroundColor = UIColor.red })
                    delay(0.2, completion: {
                        print("delay")
                    })
                }
                
            }else if repeatePasswd  {
                plist.set(passwd, forKey: "lockPasswd")
                passwdLabel.text = "password_pw_re_confirm".localized
                repeatePasswd = false
                passwdDone = true
            }else if passwdDone {
                self.passwordDone(passwd: passwd)
                print("change Done1")
                if passwdfine  {
                    self.dismiss(animated: true, completion: nil)
                }else {
                    plist.removeObject(forKey: "lockPasswd")
                    passwdLabel.text = "password_pw_confirm_detail".localized
                    repeatePasswd = true
                    [firstNum, secondNum, thridNum, fouthNum].forEach({ $0?.backgroundColor = UIColor.DPBuyButtonColor })
                }
            }
            
            plist.synchronize()
            passwd.removeAll()
            passwdConut = 0
            print("change Done2")
        }

    }
    
    @IBAction func minusAction(_ sender: UIButton) {
        if passwdConut >= 0 {
         passwdConut -= 1
        }
        
        if passwd.count < 5 && passwd.count > 0 {
            passwd.remove(at: passwdConut)
        }
        
        print(passwd)
        
        if passwdConut == 0 {
            passwdConut = 0
        }

    }
    
    
    func authenticateUser() {

        let context = LAContext()
        var error: NSError?
        
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            
            
            let reason = "Identify yourself!"
            
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) {
                [unowned self] success, authenticationError in
                
                DispatchQueue.main.async {
                    if success {
                        print("success is good")
                        
                        self.dismiss(animated: true, completion: nil)
                        
                    } else {
                        let ac = UIAlertController(title: "Authentication failed", message: "Sorry!", preferredStyle: .alert)
                        ac.addAction(UIAlertAction(title: "OK", style: .default))
                        self.present(ac, animated: true)
                    }
                }
            }
        } else {
            print("error")
            let ac = UIAlertController(title: "Touch ID not available", message: "Your device is not configured for Touch ID.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
    
    func initstart(){
        
        let plist = UserDefaults.standard
        let touchis = plist.bool(forKey: "touchIdIs")
        
        if touchis {
            touchIdBtn.isHidden = false
        }else{
            touchIdBtn.isHidden = true
        }
        
        if backandfore {
            cancel.isHidden = true
        }else {
            cancel.isHidden = false
        }
        
        if lockis {
            print("사용 중인 앱 잠금번호를 입력해 주세요")
            passwdLabel.text = "password_pw_confirm_detail".localized
            
        }else if passwdchange{
            print("사용 중인 앱 잠금번호를 입력해 주세요")
            passwdLabel.text = "password_pw_confirm_detail".localized
            
        }else{
            print("앱 잠금 번호를 입력 해 주세요")
            passwdLabel.text = "password_pw_confirm_detail".localized
            repeatePasswd = true
        }
    }
    
    func passwordDone(passwd : Array<String> ){
        var num = 0
        let plist = UserDefaults.standard
        
        guard let repasswd = plist.array(forKey: "lockPasswd") as? Array<String> else {
            print("plist null")
            return
        }
        
        if repasswd != passwd {
            self.passwdfine = false
        }else {
            self.passwdfine = true
        }
        
    }
    
    func delay(_ seconds: Double, completion: @escaping () -> ()){
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            completion()
        }
    }
    
}
