//
//  UpdateController.swift
//  KDex
//
//  Created by park heewon on 2018. 8. 17..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation
import StoreKit

class UpdateController: UIViewController, SKStoreProductViewControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
        
    }
    
    private func updateAction() {
        
//        openStoreProductWithiTunesItemIdentifier(identifier: "id1387360646")
        
        guard let url = URL(string: "https://itunes.apple.com/app/id1387360646") else { return }
        
        UIApplication.shared.open(url, options: [:]) { (_) in
            exit(0)
        }
        
//
//        print("update Action guard")
//
//        UIApplication.shared.open(url, options: [:], completionHandler: nil)
        
//
//        if #available(iOS 10.0, *) {
//            UIApplication.shared.open(url, options: [:], completionHandler: nil)
//        } else {
//            UIApplication.shared.openURL(url)
//        }
        
//        exit(0)
        
//
        
//        if let url = URL(string: "itms-apps://itunes.apple.com/app/id1387360646"),
//            UIApplication.shared.canOpenURL(url)
//        {
//            if #available(iOS 10.0, *) {
//                UIApplication.shared.open(url, options: [:], completionHandler: nil)
//            } else {
//                UIApplication.shared.openURL(url)
//            }
//        }
        
    }
    
    @IBAction func updateBtnAction(_ sender: Any) {
        print("update action click")
        self.updateAction()
        
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        print("cancel")
        
        let alert = UIAlertController(title: "app_update".localized, message: "app_stop".localized, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .default, handler: { (_) in
            exit(0)
        }))
        
        self.present(alert, animated: false, completion: nil)
        
    }
    
//    func openStoreProductWithiTunesItemIdentifier(identifier: String) {
//        let storeViewController = SKStoreProductViewController()
//        storeViewController.delegate = self
//
//        let parameters = [ SKStoreProductParameterITunesItemIdentifier : identifier]
//        storeViewController.loadProduct(withParameters: parameters) { [weak self] (loaded, error) -> Void in
//            if loaded {
//                // Parent class of self is UIViewContorller
//                self?.present(storeViewController, animated: true, completion: nil)
//            }
//        }
//    }
//
//    func productViewControllerDidFinish(_ viewController: SKStoreProductViewController) {
//        viewController.dismiss(animated: true, completion: nil)
//    }
    
    
}
