//
//  PrivateKeyActionController.swift
//  KDex
//
//  Created by park heewon on 2018. 9. 27..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation

class PrivateKeyActionController: UIViewController {
    
    @IBOutlet var authField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
        
    }
    
    @IBAction func corformAction(_ sender: Any) {
        
        DBManager.sharedInstance.deleteWalletKey()
        
        guard let ad = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        ad.getPrivateKeyList()
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        print("cancel action")
            
        guard let ad = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        ad.deleteTempPrivateKeyAction(message: "pop_wallet_private_copy_cancel".localized, cancel: true)
        
        let alert = UIAlertController(title: "pop_walletSend".localized, message: "app_stop".localized, preferredStyle: .alert)
//        let alert = UIAlertController(title: "pop_walletSend".localized, message: "pop_wallet_private_copy_cancel".localized, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .default, handler: { (_) in
//            self.dismiss(animated: true, completion: nil)
            exit(0)
        }))
        
        self.present(alert, animated: false, completion: nil)
        
    }
}
