//
//  EmailAuthController.swift
//  KDex
//
//  Created by park heewon on 24/12/2018.
//  Copyright © 2018 hanbiteni. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class EmailAuthController: UIViewController {
    
    @IBOutlet var inputNumber: UITextField!
    
    var delegate : AlertDetailViewController?
    
    var tempDelegate : TempAlertViewController?
    
    var alertBool = false // true : 알림 아님
    
    var sendBool = false // true : send 알림
    
    var simbol = ""
    
    var address = ""
    
    var qty = ""
    
    var fee = ""
    
    var sendGasLimit = ""
    
    var sendGwei = ""
    
    var date = ""
    
    var time = ""
    
    var fromsimbol = ""
    
    var tosimbole = ""
    
    var sendfee = ""
    
    var exType = ""
    
    
    //    var delegate : WalletSendViewController?
    var authdelegate : EmailAuthDelegate?
    
    var activity : UIActivityIndicatorView = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activity.color = UIColor.DPActivityColor
        
        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
        
        
        if alertBool {
            requestEmailAuth()
        }
        
        textfieldsAddtarget()
        
        showAnimate()
        
    }
    
    @IBAction func requestAction(_ sender: Any) {
        
        //        self.authdelegate?.requstAction()
        
        
        self.activity.startAnimating()
        
        guard let tempnumber = inputNumber.text else {
            return
        }
        
        let url = URL(string: "\(AppDelegate.url)/auth/checkCertCode")
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let param = "uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&cetno=\(tempnumber)"
        
        print("email param : \(param)")
        let paramData = param.data(using: .utf8)
        
        request.httpBody = paramData
        
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        request.setValue(String(describing: paramData!.count), forHTTPHeaderField: "Content-Length")
        
        //laucnhing
        let session = URLSession.shared
        
        let task = session.dataTask(with:request as URLRequest) {data, response, error in
            
            if error == nil {
                
                DispatchQueue.main.async {
                    
                    //activity.stopAnimating()
                    do {
                        
                        let result = String(data: data!, encoding: String.Encoding.utf8)
                        
                        print("email result : \(result!)")
                        
                        if result == "인증번호를 확인하세요." {
                            
                            let alert = UIAlertController(title: "메일인증", message: "인증 번호가 잘못 되었습니다. ", preferredStyle: .alert)
                            
                            alert.addAction(UIAlertAction(title: "확인", style: .cancel, handler: { (_) in
                                
                            }))
                            
                            self.present(alert, animated: false, completion: nil)
                            
                        }else {
                            
                            if self.alertBool {
                                
                                self.authdelegate?.requstAction()
                                
                            }else {
                                
                                if self.fromsimbol == "" && self.sendBool {
                                    
                                    AppDelegate.sendAlertAction(uid: AppDelegate.uid, sessionid: AppDelegate.sessionid, simbol: self.simbol, address: self.address, qty: self.qty, fee: self.fee, sendGasLimit: self.sendGasLimit , sendGwei: self.sendGwei, date: self.date, time: self.time, complete: {
                                        
                                        if self.delegate != nil {
                                            self.delegate?.alertList(uid: AppDelegate.uid, sessionid: AppDelegate.sessionid, type: .all)
                                        }
                                        
                                    })
                                    
                                }else {
                                    
                                    AppDelegate.exchangeAlertAction(uid: AppDelegate.uid, sessionid: AppDelegate.sessionid, fromsimbol: self.fromsimbol, tosimbole: self.tosimbole, qty: self.qty, sendfee: self.sendfee, date: self.date, time: self.time, exType: self.exType, complete: {
                                        
                                        if self.delegate != nil {
                                            self.delegate?.alertList(uid: AppDelegate.uid, sessionid: AppDelegate.sessionid, type: .all)
                                        }
                                        
                                    })
                                    
                                }
                                
                            }
                            
                        }
                        
                        //                        self.dismiss(animated: false, completion: nil)
                        
                        self.closeAnimate()
                        
                        self.activity.stopAnimating()
                        
                    }catch{
                        
                        //                        print("cCaught an error:\(error)")
                        //
                        //                        let msg = "pop_wallet_send_not_reply".localized
                        //                        let alert = UIAlertController(title: "pop_wallet".localized, message: msg, preferredStyle: .alert)
                        //                        alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
                        //                        self.present(alert, animated: false, completion: nil)
                        
                        self.activity.stopAnimating()
                        
                    }
                }
                
            }else{
                
                //                let msg = "pop_wallet_send_not_reply".localized
                //                let alert = UIAlertController(title: "pop_wallet".localized, message: msg, preferredStyle: .alert)
                //                alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
                //
                //                self.present(alert, animated: false, completion: nil)
                
                self.activity.stopAnimating()
                
            }
            
        }
        
        task.resume()
        
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        print("email cancel")
        self.dismiss(animated: false, completion: nil)
        self.view.removeFromSuperview()
        
    }
    
    
    func textfieldsAddtarget(){
        
        self.inputNumber.addTarget(self, action: #selector(textfieldChageEvent), for: .editingChanged)
        
    }
    
    @objc func textfieldChageEvent(textfield : UITextField) {
        
        let cnt = textfield.text?.count ?? 0
        
        if cnt > 6{
            let str = textfield.text
            
            textfield.text = str?.substring(from: 0, to: cnt - 1)
            
        }
        
    }
    
    func showAnimate() {
        
        self.view.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.25) {
            
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform.init(scaleX: 1.0, y: 1.0)
            
        }
        
    }
    
    func closeAnimate() {
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0
        }) { (finished: Bool) in
            if(finished)
            {
                self.view.removeFromSuperview()
            }
        }
        
    }
    
    func requestEmailAuth() {
        
        let url = "\(AppDelegate.url)/auth/sendMailCert?uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)"
        
        print("wallet url : \(url)")
        let amo = Alamofire.request(url)
        amo.responseJSON(completionHandler: { response in
            
            guard let result = response.result.value as? String else {
                return
            }
            
            //            print("request email auth : \(result)")
            //
            //            if result == "OK" {
            //
            //                let alert = UIAlertController(title: "메일인증", message: "등록된 이메일로 인증번호를 발송하였습니다.", preferredStyle: .alert)
            //
            //                alert.addAction(UIAlertAction(title: "확인", style: .cancel, handler: { (_) in
            //
            //                }))
            //
            //                self.present(alert, animated: false, completion: nil)
            //
            //            }else {
            //
            //                let alert = UIAlertController(title: "메일인증", message: "이메일 인증발송이 실패되었습니다. 재시도 부탁드립니다.", preferredStyle: .alert)
            //
            //                alert.addAction(UIAlertAction(title: "확인", style: .cancel, handler: nil))
            //
            //                self.present(alert, animated: false, completion: nil)
            //
            //            }
            
        })
    }
    
}
