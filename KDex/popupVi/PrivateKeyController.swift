//
//  PrivateKeyController.swift
//  KDex
//
//  Created by park heewon on 2018. 9. 27..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation

class PrivateKeyController: UIViewController{
    
    @IBOutlet var maindeviceLb: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
    }
    
    @IBAction func requestAuth(_ sender: Any) {
        
        guard let ad  = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        ad.deleteTempPrivateKeyAction(message: "", cancel: false)
        
        ad.requstPkCopyAction()
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func cancelBtnAction(_ sender: Any) {
        
//        self.dismiss(animated: true, completion: nil)
        
//        let alert = UIAlertController(title: "pop_walletSend".localized, message: "pop_wallet_private_copy_cancel".localized, preferredStyle: .alert)
        
        let alert = UIAlertController(title: "pop_walletSend".localized, message: "app_stop".localized, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .default, handler: { (_) in
            exit(0)
        }))

        self.present(alert, animated: false, completion: nil)

    }
    
}
