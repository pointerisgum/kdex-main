//
//  WithdrawController.swift
//  KDex
//
//  Created by park heewon on 10/12/2018.
//  Copyright © 2018 hanbiteni. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class WithdrawController : UIViewController, EmailAuthDelegate, UITextFieldDelegate {
    
    //등록계좌
    @IBOutlet var registerCountLb: UILabel!
    
    //출금가능금액
    @IBOutlet var withAblePriceLb: UILabel!
    
    //최소출금금액
    @IBOutlet var minWithdrawPriceLb: UILabel!
    
    //출금액 입력
    @IBOutlet var totalAmount: UITextField!
    
    //수수료
    @IBOutlet var rateLb: UILabel!
    
    //최종출금액
    @IBOutlet var finalAmount: UILabel!
    
    var activity : UIActivityIndicatorView = UIActivityIndicatorView()
    
    var ableCount = 0.0
    
    var withPrice = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activity.color = UIColor.DPActivityColor
        
        totalAmount.delegate = self
        totalAmount.addTarget(self, action: #selector(textfieldChageEvent), for: .editingChanged)
        
        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
        
        registerCountLb.text = "\(AppDelegate.bankAccountNo)(\(AppDelegate.userBankName))"
        
        userAmount()
        
    }
    
    @IBAction func withdrawBtnAction(_ sender: Any) {
        
        self.view.endEditing(true)
        
        let price = totalAmount.text ?? "0"
        
        let tempPrice = stringtoDouble(str: price) ?? 0.0
            
        print("tempPrice : \(tempPrice) , \(self.ableCount)")
        
        if price == "" || price == "0" {
            
            let alertVi = UIAlertController(title: "출금", message: "출금 금액을 입력해주세요", preferredStyle: .alert)
            
            alertVi.addAction(UIAlertAction(title: "확인", style: .cancel, handler: nil))
            
            self.present(alertVi, animated: false, completion: nil)
            
        }else if self.ableCount < tempPrice {
            
            let alertVi = UIAlertController(title: "출금", message: "출금 가능 금액을 확인해주세요", preferredStyle: .alert)
            
            alertVi.addAction(UIAlertAction(title: "확인", style: .cancel, handler: nil))
            
            self.present(alertVi, animated: false, completion: nil)
            
        }else if tempPrice < 10000.0 {
            
            let alertVi = UIAlertController(title: "출금", message: "출금 금액을 10,000원 이상으로 신청해주세요", preferredStyle: .alert)
            
            alertVi.addAction(UIAlertAction(title: "확인", style: .cancel, handler: nil))
            
            self.present(alertVi, animated: false, completion: nil)
        
        }else {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//
//            if let detail = storyboard.instantiateViewController(withIdentifier: "emailAuthID") as? EmailAuthController {
//
//                let alertWindow = UIWindow(frame: UIScreen.main.bounds)
//                alertWindow.rootViewController = UIViewController()
//                alertWindow.windowLevel = UIWindowLevelAlert + 1;
//                alertWindow.makeKeyAndVisible()
//                alertWindow.rootViewController?.present(detail, animated: true, completion: nil)
//
//            }
            
            let passPopVc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "emailAuthID") as! EmailAuthController
            
            passPopVc.authdelegate = self
            passPopVc.alertBool = true
            
            self.addChild(passPopVc)
            
            passPopVc.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            self.view.addSubview(passPopVc.view)
            passPopVc.didMove(toParent: self)
            
//            requstAction(: price)
        }
        
    }
    
    @IBAction func cancelBtnAction(_ sender: Any) {
        
        self.dismiss(animated: false, completion: nil)
        
    }
    
    
    //수정완료후
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        print("textFieldDidEndEditing")
        
        let price = totalAmount.text ?? "0"
        
        let tempPrice = stringtoDouble(str: price) ?? 0.0
        
        print("tempPrice : \(tempPrice) , \(self.ableCount)")
        
        
        if tempPrice == self.ableCount {
            
//            self.withPrice = self.ableCount - 1000
            self.withPrice = self.ableCount
            self.finalAmount.text = String(format: "%.0f", self.withPrice).insertComma
            
            
        }else if tempPrice > (self.ableCount - 1000) && tempPrice < self.ableCount {
            
//            self.withPrice = self.ableCount - 1000
            self.withPrice = self.ableCount - 1000
            self.finalAmount.text = String(format: "%.0f", self.withPrice).insertComma
            
        }else if  self.ableCount == 0.0 {
            
            self.withPrice = 0.0
            self.finalAmount.text = String(format: "%.0f", self.withPrice).insertComma
            
        }else if tempPrice < self.ableCount {
            
            self.withPrice = tempPrice + 1000
            self.finalAmount.text = String(format: "%.0f", self.withPrice).insertComma
        }
        
//        let number = Int.random(in: 0 ..< 20)
//        if inputPrice.text != "" {
//            let price = Int(inputPrice.text!)
//            self.tempprice = price! + number
//        }
//        valuePrice.text = "\(self.tempprice)".insertComma + " 원"
    }
    
    func requstAction() {
        
        self.activity.startAnimating()
        
        let url = URL(string: "\(AppDelegate.url)/auth/walletSendKRW")
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let param = "uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&price=\(self.withPrice - 1000)"
        
        print("wallet send param : \(param)")
        let paramData = param.data(using: .utf8)
        
        request.httpBody = paramData
        
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        request.setValue(String(describing: paramData!.count), forHTTPHeaderField: "Content-Length")
        
        //laucnhing
        let session = URLSession.shared
        
        let task = session.dataTask(with:request as URLRequest) {data, response, error in
            
            if error == nil {
                
                DispatchQueue.main.async {
                    
                    //activity.stopAnimating()
                    do {
                        let object = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        
                        guard let jsonObject = object else { return }
                     
                        print("jsonobject : \(jsonObject)")
                        
                        let sendResult = jsonObject["returnYn"] as? String ?? "N"
                        
                        print("send result : \(sendResult)")
                        
                        self.activity.stopAnimating()
                        
                        self.totalAmount.text = ""
                        
                        if sendResult == "Y" {
                            
                            self.dismiss(animated: false, completion: nil)
                           
                        }else {
                            
                            let msg = "현재 서비스 준비중입니다."
                            let alert = UIAlertController(title: "출금", message: msg, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: { (_) in
                                self.dismiss(animated: false, completion: nil)
                            }))
                            self.present(alert, animated: false, completion: nil)
                            
                        }
                        
                        
//                        self.dismiss(animated: false, completion: nil)
                        
                        
                        
                    }catch{
                        print("cCaught an error:\(error)")
                        
                        let msg = "pop_wallet_send_not_reply".localized
                        let alert = UIAlertController(title: "pop_wallet".localized, message: msg, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
                        self.present(alert, animated: false, completion: nil)
                        
                        self.activity.stopAnimating()
                        
                    }
                }
            }else{
                print("error:\(error)")
                let msg = "pop_wallet_send_not_reply".localized
                let alert = UIAlertController(title: "pop_wallet".localized, message: msg, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
                
                self.present(alert, animated: false, completion: nil)
                
                self.activity.stopAnimating()
                
            }
            
        }
        
        task.resume()
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
        
        self.view.endEditing(true)
        
    }
    
    func userAmount() {
        
        let activity = UIActivityIndicatorView()
        activity.color = UIColor.DPActivityColor
        activity.hidesWhenStopped = true
        activity.startAnimating()
        
        let url = "\(AppDelegate.url)auth/userAmount?market=KRW&simbol=KRW&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)"
        
        print("wallet url : \(url)")
        let amo = Alamofire.request(url)
        amo.responseJSON(completionHandler: { response in
            
            print("wallet userAbleAmount")
            var buyjson = response.result.value as? [String : Any]
            
            self.withAblePriceLb.text = buyjson?["wthrAbleAmount"] as? String ?? ""
            
            self.ableCount = self.stringtoDouble(str: (buyjson?["wthrAbleAmount"] as? String ?? "")) ?? 0.0
            
            activity.stopAnimating()
        })
    }
    
    public func stringtoDouble(str : String) -> Double?{
        let temp = str.components(separatedBy: [","]).joined()
        let double = Double(temp)
        return double ?? 0.0
    }
    
    @objc func textfieldChageEvent() {
        
        let tempStr = self.totalAmount.text
        
        var doubleTemp = stringtoDouble(str: tempStr!) ?? 0.0
        
        if self.ableCount == 0.0 {
            
//            doubleTemp = 0.0
            
        }else if doubleTemp > self.ableCount {
            
            doubleTemp = self.ableCount - 1000
            
        }
        
        self.totalAmount.text = String(format: "%.0f", doubleTemp).insertComma
     
    }
    
}
