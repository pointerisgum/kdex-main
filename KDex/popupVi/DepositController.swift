//
//  WithdrawController.swift
//  KDex
//
//  Created by park heewon on 10/12/2018.
//  Copyright © 2018 hanbiteni. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class DepositController : UIViewController, UITextFieldDelegate {
    
    @IBOutlet var inputPrice: UITextField! // 입력한 금액
    
    @IBOutlet var valuePrice: UILabel! // 입금금액
    
    @IBOutlet var bankName: UILabel! // 은행이름
    
    @IBOutlet var bankCount: UILabel! // 은행계좌번호
    
    @IBOutlet var contentVi: UITextView! // 주의사항
    
    @IBOutlet var depoTitleLabel: UILabel! // 입력 신청후 타이틀
    
    @IBOutlet var requestPriceValue: UILabel! //신청금액
    
    @IBOutlet var requestDate: UILabel! //신청시간
    
    @IBOutlet var footerContentVi: UITextView! //신청시 주의사항
    
    @IBOutlet var listTbVi: UIView!
    
    @IBOutlet var requestBtn: UIButton! // 입금 버튼
    
    var tempprice = 0
    
    var tempKrwInfo = [String : String]()
    
    var activity : UIActivityIndicatorView = UIActivityIndicatorView()
    
    @IBAction func requestCancelAction(_ sender: Any) {
        getRemoveKRW()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        activity.color = UIColor.DPActivityColor
        
        footerContentVi.isEditable = false
        contentVi.isEditable = false
        depoTitleLabel.isHidden = true
        listTbVi.isHidden = true
        footerContentVi.isHidden = true
        contentVi.isHidden = false
        inputPrice.isEnabled = false
        inputPrice.isEnabled = true
        requestBtn.isEnabled = true
    
        getReceiveList()
        
        bankAmount()
        
        valuePrice.text = "0 원"
        
        inputPrice.delegate = self
        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
        
    }
    
    @IBAction func cancelBtnAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        let number = Int.random(in: 0 ..< 20)
        if inputPrice.text != "" {
            let price = Int(inputPrice.text!)
            self.tempprice = price! + number
        }
        valuePrice.text = "\(self.tempprice)".insertComma + " 원"
    }
    
    @IBAction func depositeBtnActioin(_ sender: Any) {
        
        self.view.endEditing(true)
        
        let price = valuePrice.text ?? ""
        
        
        let doublePrice = stringtoDouble(str: "\(tempprice)") ?? 0.0
        
        if price == "" || price == "0" {
            
            let alertVi = UIAlertController(title: "입금", message: "입금 금액을 입력해주세요", preferredStyle: .alert)
            
            alertVi.addAction(UIAlertAction(title: "확인", style: .cancel, handler: nil))
            
            
            self.present(alertVi, animated: false, completion: nil)
            
        } else if doublePrice < 10000.0  {
            
            let alertVi = UIAlertController(title: "입금", message: "입금 금액이 10,000원 이상으로 신청해주세요", preferredStyle: .alert)
            
            alertVi.addAction(UIAlertAction(title: "확인", style: .cancel, handler: nil))
            
            
            self.present(alertVi, animated: false, completion: nil)
            
        }else {
            
            self.requstAction()
            
//            let passPopVc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "emailAuthID") as! EmailAuthController
//
//            passPopVc.authdelegate = self
//
//            self.addChildViewController(passPopVc)
//
//            passPopVc.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
//            self.view.addSubview(passPopVc.view)
//            passPopVc.didMove(toParentViewController: self)
            
        }
        
    }

//    func requstAction(price : String) {
//
//
//
//    }
    
    func requstAction() {
        self.activity.startAnimating()
        
        let url = URL(string: "\(AppDelegate.url)/auth/walletReceiveKRW")
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let param = "uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&&price=\(self.tempprice)"
        
        print("wallet send param : \(param)")
        let paramData = param.data(using: .utf8)
        
        request.httpBody = paramData
        
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        request.setValue(String(describing: paramData!.count), forHTTPHeaderField: "Content-Length")
        
        //laucnhing
        let session = URLSession.shared
        
        let task = session.dataTask(with:request as URLRequest) {data, response, error in
            
            if error == nil {
                
                DispatchQueue.main.async {
                    
                    //activity.stopAnimating()
                    do {
                        let object = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        
                        guard let jsonObject = object else { return }
                        
                        let sendResult = jsonObject["returnYn"] as? String ?? "N"
                        
                        print("email send result : \(sendResult)")
                        
                        self.activity.stopAnimating()
                        
                        self.inputPrice.text = ""
                        
                        if sendResult == "Y" {
                            
                            self.dismiss(animated: false, completion: nil)

                        }else {
                            
                            self.inputPrice.text = ""

                            let msg = "현재 서비스 준비중입니다."
                            let alert = UIAlertController(title: "입금", message: msg, preferredStyle: .alert)

                            alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: { (_) in
                                self.dismiss(animated: false, completion: nil)
                            }))
//                            alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
                            self.present(alert, animated: false, completion: nil)

                        }
                        
                        
//                        self.getReceiveList()
                        
                    }catch{
                        print("cCaught an error:\(error)")
                        
                        let msg = "pop_wallet_send_not_reply".localized
                        let alert = UIAlertController(title: "pop_wallet".localized, message: msg, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
                        self.present(alert, animated: false, completion: nil)
                        
                        self.activity.stopAnimating()
                        
                    }
                }
            }else{
                print("error:\(error)")
                let msg = "pop_wallet_send_not_reply".localized
                let alert = UIAlertController(title: "pop_wallet".localized, message: msg, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
                
                self.present(alert, animated: false, completion: nil)
                
                self.activity.stopAnimating()
                
            }
            
        }
        
        task.resume()
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
        
        self.view.endEditing(true)
        
    }
    
    func bankAmount() {
        
        let activity = UIActivityIndicatorView()
        activity.style = .whiteLarge
        activity.color = UIColor.DPActivityColor
        activity.hidesWhenStopped = true
        activity.startAnimating()
        
        let url = "\(AppDelegate.url)/wallet/bankInfo"
        
        print("wallet url : \(url)")
        let amo = Alamofire.request(url)
        amo.responseJSON(completionHandler: { response in
            
            print("wallet userAbleAmount")
            var buyjson = response.result.value as? [String : Any]
            
            let name = buyjson?["bankName"] as? String ?? ""
            
            let count = buyjson?["bankAccount"] as? String ?? ""
            
            self.bankCount.text = "\(count)(\(name))"
            
            self.bankName.text = buyjson?["bankAccountName"] as? String ?? ""
            
            activity.stopAnimating()
        })
    }
    
    public func stringtoDouble(str : String) -> Double?{
        let temp = str.components(separatedBy: [","]).joined()
        let double = Double(temp)
        return double ?? 0.0
    }
    
    func getReceiveList() {
        
        let url = "\(AppDelegate.url)/auth/getReceiveKRWList?uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)"
        
        print("wallet url : \(url)")
        let amo = Alamofire.request(url)
        amo.responseJSON(completionHandler: { response in
            
            guard let result = response.result.value as? [[String : String]] else {
                return
            }
            
            if result.count == 0 {
                
                self.depoTitleLabel.isHidden = true
                self.listTbVi.isHidden = true
                self.footerContentVi.isHidden = true
                self.contentVi.isHidden = false
                self.inputPrice.isEnabled = false
                self.inputPrice.isEnabled = true
                self.requestBtn.isEnabled = true
                
                
            }else {
                
                let tempValue = result[0]
                
                self.tempKrwInfo = tempValue
                
                self.depoTitleLabel.isHidden = false
                self.listTbVi.isHidden = false
                self.footerContentVi.isHidden = false
                self.contentVi.isHidden = true
                self.inputPrice.isEnabled = false
                self.requestBtn.isEnabled = false
                
                guard let date = tempValue["updateDatetime"] else {
                    return
                }
                
                self.requestDate.text = self.makeTime(date: date)
                self.requestPriceValue.text = tempValue["amount"]?.insertComma
            
            }
            
        })
        
    }
    
    func getRemoveKRW() {
        
        let url = "\(AppDelegate.url)/auth/cancleReceiveKRW?uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&date=\(self.tempKrwInfo["applyDate"] ?? "")&no=\(self.tempKrwInfo["applyNo"] ?? "")&account=\(self.tempKrwInfo["accountNo"] ?? "")"
        
        print("wallet cancleReceiveKRW url : \(url)")
        let amo = Alamofire.request(url)
        amo.responseJSON(completionHandler: { response in
            
            guard let result = response.result.value as? [String : Any] else {
                
                return
                
            }
            
            let tempresult = result["returnYn"] as? String ?? "N"
            
            if tempresult == "Y" {
                self.getReceiveList()
            }
            
//            리턴 JSON
//            CheckInfo
//            private String returnYn; //
//            private String returnNo;
//            private String returnMsg;
//
            
        })
    }
    
    //날짜 형식 포멧
    func makeTime(date : String) -> String {
        
        let df : DateFormatter = DateFormatter()
        df.dateFormat = "yyyyMMddHHmmss "
        
        guard let formData = df.date(from: date) as? NSDate else {
            return "0.0.0 00:00"
        }
        
        let date_time : NSDate = formData
        
        df.dateFormat = "yy.MM.dd HH:mm:ss"
        
        let newDate : String = df.string(from: date_time as Date) as String
        
        return newDate
    }
    
}
