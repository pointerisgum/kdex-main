//
//  ViewController.swift
//  crevolc
//
//  Created by park heewon on 2017. 12. 11..
//  Copyright © 2017년 hanbiteni. All rights reserved.
//
//  거래소 첫번째 탭바 컨트롤러

import Alamofire
import Foundation
import UIKit

class ViewController: PagerController, PagerDataSource, UITextFieldDelegate, AlertShowDelegate{
    
    var revealDelegate : RevealViewController?
    
    var pageCT = [UIViewController]()
    
    var showSearch = true
    
    let circle : UIView = {
        let vi = UIView()
        vi.backgroundColor = UIColor(rgb: 0xe9a01b)
        vi.frame = CGRect(x: 40, y: 5, width: 5, height: 5)
        vi.layer.cornerRadius = vi.frame.size.width / 2
        vi.clipsToBounds = true
        vi.isHidden = true
        return vi
    }()
    
    let searchvi : UIView = {
        let vi = UIView()
        vi.translatesAutoresizingMaskIntoConstraints = false
        vi.backgroundColor = UIColor.DPViewBackgroundColor
        return vi
    }()

    let testfield : UITextField = {
        let tf = UITextField()
        tf.backgroundColor = UIColor.clear
        tf.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        return tf
    }()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        UIApplication.shared.statusBarStyle = UIStatusBarStyle(rawValue: 1)!
//        UIApplication.shared.statusBarView?.backgroundColor = UIColor.DPStatusBgColor
        
        print("size : \(self.view.frame.size.width) \(self.view.frame.height)")
        
        self.view.backgroundColor = UIColor(red:0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        
        //뷰 페이저 델리게이트
        self.dataSource = self
        testfield.delegate = self
        
        print("navibar height : \(self.navigationController?.navigationBar.frame.height)")
        
        initTitle()
        initbarbtn()
//        initNextbtn()
        
        let dragLeft = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(moveSide(_:)))
        dragLeft.edges = UIRectEdge.left
        self.view.addGestureRecognizer(dragLeft)
        
        let dragRight = UISwipeGestureRecognizer(target: self, action: #selector(moveSide(_:)))
        
        dragRight.direction = .left
        self.view.addGestureRecognizer(dragRight)
        
        registerStoryboardControllers()
        customizeTab()
        
        if AppDelegate.alertBool {
            alertAction()
            AppDelegate.alertBool = false
        }
        
        if AppDelegate.priceBool {
            priceAction(maket: "", simbol: "")
            AppDelegate.priceBool = false
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("view controller viewdidappear")
        initNextbtn()
    }
    
    func registerStoryboardControllers() {
        print("registerStoryboardControllers")
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let controller1 = storyboard.instantiateViewController(withIdentifier: "ex_first")
        let controller2 = storyboard.instantiateViewController(withIdentifier: "ex_second")
        let controller3 = storyboard.instantiateViewController(withIdentifier: "ex_third")
        let controller4 = storyboard.instantiateViewController(withIdentifier: "ex_third")
        
        self.setupPager(tabNames: ["trade_market_favorite_txt".localized, "KRW", "BTC", "ETH"], tabControllers: [controller1, controller2, controller3, controller4])
        
        if let controller = controller1 as? ExchangListController {
            pageCT.append(controller)
            controller.viewDelegate = self
            controller.didSelectRow = { [weak self] (text: String, name: String, market: String) in
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                if let detail = storyboard.instantiateViewController(withIdentifier: "exchangInOut") as? ExchangInOutDetail {
                    detail.currentTab = 1
                    detail.coinSimbol = text
                    detail.coinName = name
                    detail.market = market
                    self?.navigationController?.pushViewController(detail, animated: true)
                }
//                if let detail = storyboard.instantiateViewController(withIdentifier: "ExchangAll") as? ExchangAllListViewController {
//                    detail.coinSimbol = text
//                    detail.coinName = name
//                    detail.market = market
//                    self?.navigationController?.pushViewController(detail, animated: true)
//                }
            }
        }
        
        if let controller = controller2 as? ExchangListKRWcontroller {
            pageCT.append(controller)
            controller.viewDelegate = self
            controller.didSelectRow = { [weak self] (text: String, name: String, market: String) in
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                if let detail = storyboard.instantiateViewController(withIdentifier: "exchangInOut") as? ExchangInOutDetail {
                    detail.currentTab = 1
                    detail.coinSimbol = text
                    detail.coinName = name
                    detail.market = market
                    self?.navigationController?.pushViewController(detail, animated: true)
                }
//                if let detail = storyboard.instantiateViewController(withIdentifier: "ExchangAll") as? ExchangAllListViewController {
//                    detail.coinSimbol = text
//                    detail.coinName = name
//                    detail.market = market
//                    self?.navigationController?.pushViewController(detail, animated: true)
//                }
            }
        }
        
        if let controller = controller3 as? ExchangListCoinController {
            pageCT.append(controller)
            controller.viewDelegate = self
            controller.maket = "BTC"
            controller.didSelectRow = { [weak self] (text: String, name : String, market: String) in
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                if let detail = storyboard.instantiateViewController(withIdentifier: "exchangInOut") as? ExchangInOutDetail {
                    detail.currentTab = 1
                    detail.coinSimbol = text
                    detail.coinName = name
                    detail.market = market
                    self?.navigationController?.pushViewController(detail, animated: true)
                }
//                if let detail = storyboard.instantiateViewController(withIdentifier: "ExchangAll") as? ExchangAllListViewController {
//                    detail.coinSimbol = text
//                    detail.coinName = name
//                    detail.market = market
//                    self?.navigationController?.pushViewController(detail, animated: true)
//                }
            }
        }
        
        if let controller = controller4 as? ExchangListCoinController {
            pageCT.append(controller)
            controller.viewDelegate = self
             controller.maket = "ETH"
            controller.didSelectRow = { [weak self] (text: String, name : String, market: String) in
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                if let detail = storyboard.instantiateViewController(withIdentifier: "exchangInOut") as? ExchangInOutDetail {
                    detail.currentTab = 1
                    detail.coinSimbol = text
                    detail.coinName = name
                    detail.market = market
                    self?.navigationController?.pushViewController(detail, animated: true)
                }
//                if let detail = storyboard.instantiateViewController(withIdentifier: "ExchangAll") as? ExchangAllListViewController {
//                    detail.coinSimbol = text
//                    detail.coinName = name
//                    detail.market = market
//                    self?.navigationController?.pushViewController(detail, animated: true)
//                }
            }
        }
    }
    
    /*
    //UIRespoder의 의하여 화면 터치시 호출
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("screen touch")
        /*
        //탭바를 숨기는 기능
        let tabBar = self.tabBarController?.tabBar
         tabBar?.isHidden@objc  = (tabBar?.isHidden == true) ? false : true
        
        //탭바 숨기는 기능 애니메이션 설정
        UIView.animate(withDuration: TimeInterval(1)){
            tabBar?.alpha = (tabBar?.alpha == 0 ) ? 1 : 0
        }
        */
    }
    */
    
    func initTitle(){

        let vi = UIView()
        
        let nTitle = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 37))
        
//        nTitle.textAlignment = .center
        nTitle.font = UIFont.boldSystemFont(ofSize: 15)
        nTitle.textColor = UIColor.DPTitleTextColor
        nTitle.text = "main_investment_btn".localized
//        nTitle.sizeToFit()
        nTitle.textAlignment = .left
        
        vi.addSubview(nTitle)
        
        self.navigationItem.titleView = vi
        
        self.navigationItem.titleView?.frame.size = CGSize(width: 400, height: 37)
        
        self.navigationController?.navigationBar.barTintColor =  UIColor.DPNaviBarTintColor
        
        self.navigationController?.addShadowToBar(vi: self)
        
        let searchimg = UIImage(named: "zoom")
        
        //search 버튼
        let searchImage = UIImageView(image: searchimg)
        searchImage.frame = CGRect(x: 10, y: 10 , width: 20, height: 20)
        
        let attr = NSMutableAttributedString(string: "trade_market_search_bar_hint".localized, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14)])
        
        testfield.frame = CGRect(x: 40, y: 0, width:(self.view.frame.width - 40), height: 40)
        testfield.attributedPlaceholder = attr
        
        searchvi.addSubview(searchImage)
        searchvi.addSubview(testfield)
        
        searchvi.frame = CGRect(x: 0, y: 2, width: view.frame.width, height: 40)
        
        searchvi.add(border: .bottom, color: UIColor.DPLineBgColor, width: 1)
        
        self.view.addSubview(searchvi)
        
    }
    
    func initbarbtn(){
        
            let icon = UIImage(named: "icn_appbar_menu")
        
            let item = UIBarButtonItem(image:icon , style: .plain, target: self, action: nil)
            
            item.tintColor = UIColor.DPWhiteBtnColor
            
            item.target = self
        
            item.action = #selector(moveSide)
            
            self.navigationItem.leftBarButtonItem = item
        
    }
    
    @objc func moveSide(_ sender: Any) {
        
        let torchEvent = UITapGestureRecognizer(target: self, action: #selector(moveSide(_:)))
        
        if sender is UIScreenEdgePanGestureRecognizer{
            self.revealDelegate?.openSideBar(nil)
        }else if sender is UISwipeGestureRecognizer{
            self.revealDelegate?.closeSideBar(nil)
        }else if sender is UITapGestureRecognizer{
            self.revealDelegate?.closeSideBar(nil)
            self.view.removeGestureRecognizer((sender as? UITapGestureRecognizer)!)
        }else if sender is UIBarButtonItem{
            if self.revealDelegate?.isSideBarShowing == false{
                self.view.addGestureRecognizer(torchEvent)
                self.revealDelegate?.openSideBar(nil)
            } else {
                self.revealDelegate?.closeSideBar(nil)
            }
        }
    }
    
    //네비게이션 바 버튼
    func initNextbtn(){
    
        let totalVI = UIView()
        totalVI.backgroundColor = UIColor.clear
        totalVI.frame = CGRect(x: 0, y: 0, width: 70, height: 37)
        totalVI.tintColor = UIColor.DPNaviBartextTintColor
        
        let icon3 = UIImage(named: "alarm")
        
        //알림 리스트 버튼
        let rightbtn3 = UIButton(type: .system)
        rightbtn3.frame = CGRect(x: 35, y: 5, width: 30, height: 30)
        rightbtn3.setImage(icon3, for: .normal)
        rightbtn3.addTarget(self, action: #selector(alertAction), for: .touchUpInside)
        
        totalVI.addSubview(rightbtn3)
        totalVI.addSubview(self.circle)
        
        let icon4 = UIImage(named: "homt")
        
        let rightbtn4 = UIButton(type: .system)
        rightbtn4.frame = CGRect(x: 0, y: 8, width: 25, height: 25)
        rightbtn4.setImage(icon4, for: .normal)
        rightbtn4.addTarget(self, action: #selector(homeAction), for: .touchUpInside)
        
        totalVI.addSubview(rightbtn4)
        
        let item = UIBarButtonItem(customView: totalVI)
        self.navigationItem.rightBarButtonItem = item
    }
    

    func customizeTab() {
        print("customizeTab")
        
        indicatorColor = UIColor.white
        tabsViewBackgroundColor = UIColor.DPtableViewBgColor
        contentViewBackgroundColor = UIColor.DPtableViewBgColor
        
        startFromSecondTab = true
        centerCurrentTab = true
        tabLocation = PagerTabLocation.top
        tabHeight = 49
        tabTopOffset = 40
        tabOffset = 36
        
        tabWidth = self.view.frame.size.width / 4
        
        print("tabwidth : \(tabWidth)")
        fixFormerTabsPositions = false
        fixLaterTabsPosition = false
        animation = PagerAnimation.during
        selectedTabTextColor = UIColor.DPmainTextColor
        //tabsTextFont = UIFont(name: "HelveticaNeue-Bold", size: 20)!
        tabsTextFont = UIFont.boldSystemFont(ofSize: 15)
        // tabTopOffset = 10.0
        tabsTextColor = UIColor.DPsubTextColor
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.alertReadValue(uid: AppDelegate.uid, sessionid: AppDelegate.sessionid)
       
        AlertShowController.sharedInstance.alertDelegate = self
    }
    
    // Programatically selecting a tab. This function is getting called on AppDelegate
    func changeTab() {
        self.selectTabAtIndex(4)
    }
    
    //login text version
    func logintext(){
        
//        let loginId = ""
//        let loginpw = ""
        
        let jsonObject : NSMutableDictionary = [ "F01" : "A0000007", "F02":"B", "F03":"BTC", "F05":"1000", "F04":"T", "F06":"18000000"]
        
        let jsonArr : NSArray = [jsonObject]
        
        let param : NSMutableDictionary = ["data": jsonArr]
        
        let jsonparam = try! JSONSerialization.data(withJSONObject: param, options: [])
        
        let jsonString = String(data: jsonparam, encoding: String.Encoding.utf8)
        
        var urlComponents = URLComponents(string: "\(AppDelegate.url)/orderTest")!
        urlComponents.queryItems = [
            URLQueryItem(name: "data", value: jsonString),
        ]
        
        let amo = Alamofire.request(urlComponents)
        amo.responseJSON(completionHandler: { response in
            print("\(response.result.value)")
        })
    }
    
    @objc func searchAction() {
        print(pageCT.count)
        
        if showSearch == true {
            self.testfield.isHidden = false
            self.showSearch = false
        }else{
            self.testfield.isHidden = true
            self.showSearch = true
            
            AppDelegate.localSearch = ""
            
            self.searchCtValue(str: "")
            
        }
        
    }
    
    @objc func homeAction() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        if let detail = storyboard.instantiateViewController(withIdentifier: "homeViPop") as? HomeViewController  {
            
            guard let appdel = UIApplication.shared.delegate as? AppDelegate else {
                return
            }
            
            appdel.window?.rootViewController?.present(detail, animated: true, completion: nil)
        }
        
    }
    
    @objc func textFieldDidChange() {
        print("test change~~~~~~~~~~")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        guard let val = self.testfield.text else {
            return true
        }
        
        AppDelegate.localSearch = val
        print(pageCT.count)
        
        self.searchCtValue(str: val)
        
        self.view.endEditing(true)
        return true
        
    }
    
    func searchCtValue(str : String){
        
        let selectCT = pageCT[self.activeTabIndex]
        
        if let controller = selectCT as? ExchangListController {
            controller.searchValue = str
        }
        
        if let controller = selectCT as? ExchangListKRWcontroller {
            controller.searchValue = str
        }
        
        if let controller = selectCT as? ExchangListCoinController {
            controller.searchValue = str
        }
       
    }
    
    @objc func alertAction(){
        
        let alertView = self.storyboard?.instantiateViewController(withIdentifier: "alertDetail") as? AlertDetailViewController
//        self.navigationController?.addShadowToBar(vi: alertView!)
        self.navigationController?.pushViewController(alertView!, animated: true)
        
    }
    
    func priceAction(maket : String, simbol : String){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
         if let detail = storyboard.instantiateViewController(withIdentifier: "ExchangAll") as? ExchangAllListViewController {
            detail.coinSimbol = simbol
            
            self.navigationController?.pushViewController(detail, animated: true)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        testfield.resignFirstResponder()
    }
    
    func hideKeyboard(){
        self.view.endEditing(true)
    }
    
    func alerShowAction() {
        
        if AppDelegate.alertShow {
            circle.isHidden = false
        }else {
            circle.isHidden = true
        }
    }
    
    func alertReadValue(uid: String, sessionid: String) {
        
        let urlComponents = URLComponents(string:"\(AppDelegate.url)/ajax/getPushLog?uid=\(uid)&sessionId=\(sessionid)&language=\(AppDelegate.appLang)")!
        
        print("urlcomponents : \(urlComponents)")
        
        let amo = Alamofire.request(urlComponents)
        amo.responseJSON { response in
            
            guard let value = response.result.value as? [[String : Any]] else {
                return
            }
            
            var tempValue = [[String:Any]]()
            
            for raw in value {
                let tempType = raw["readYn"] as? String ?? ""
                if tempType == "N"{
                    tempValue.append(raw)
                }
            }
            
            UIApplication.shared.applicationIconBadgeNumber = tempValue.count
            
            print("BadgeNumber : \(UIApplication.shared.applicationIconBadgeNumber)")
            
            if tempValue.count != 0 {
                self.circle.isHidden = false
            }else {
                self.circle.isHidden = true
            }
        }
    }
}
