//
//  WalletListController.swift
//  DEXnPAY
//
//  Created by park heewon on 2018. 4. 6..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class WalletListController: UITableViewController {
    
    var cellSelectReActionBool = false
    
    var delegate : WalletExchageViewController?
    
    var cellId = "walletCellId"
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.preferredContentSize.height = 220
        tableView.register(WalletListCell.self, forCellReuseIdentifier: cellId)
        tableView.layoutMargins = UIEdgeInsets.zero
        tableView.separatorInset = UIEdgeInsets.zero
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AppDelegate.walletOtehrListArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? WalletListCell else {
            return UITableViewCell()
        }
        
        let tempJson = AppDelegate.walletOtehrListArray[indexPath.row]
        
        let resultLastprice = tempJson["lastPrice"] as? String ?? ""
        
        let doubleLastPrice = stringtoDouble(str: resultLastprice)
        
        let intResultKrw = Int(doubleLastPrice ?? 0.0)
        
        let tempKrwStr = "\(intResultKrw)".insertComma
        
        cell.layoutMargins = UIEdgeInsets.zero
        cell.walletQty.text = "\(tempKrwStr) KRW"
        cell.walletName.text = tempJson["simbol"] as? String ?? ""
        
        cell.walletImg.kf.setImage(with: AppDelegate.dataImageUrl(str: "\(tempJson["simbol"] as? String ?? "")"))
//        cell.walletImg.image = AppDelegate.dataImage(str: "\(tempJson["simbol"] as? String ?? "")")
//            UIImage(named: "\(tempJson["simbol"] as? String ?? "")")
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tempJson = AppDelegate.walletOtehrListArray[indexPath.row]
        
        let simbole = tempJson["simbol"] as? String ?? ""
        let resultLastprice = tempJson["lastPrice"] as? String ?? ""
        
        DBManager.sharedInstance.selecttWalletSimbol(simbol: simbole)
        
        print("wallet receive simbole : \(simbole), resulteLastprice :\(resultLastprice)")
        
        delegate?.testalert(index: indexPath)
        
        if cellSelectReActionBool {
            delegate?.predictionCoinQtyCount(simbol: simbole, lastprice: resultLastprice)
        }
        
    }
    
    func stringtoDouble(str : String) -> Double?{
        let temp = str.components(separatedBy: [","]).joined()
        let double = Double(temp)
        return double ?? 0.0
    }
    
}
