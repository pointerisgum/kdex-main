//
//  JoinViewController.swift
//  crevolc
//
//  Created by crovolc on 2018. 1. 9..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//
// 회원가입 컨트롤페이지

import UIKit
import Foundation
import Alamofire

class JoinViewController : UIViewController, UITextFieldDelegate {
    
    //닉네임
    @IBOutlet var nickNameLabel: UITextField!
    @IBOutlet var nickCheckLb: UILabel!
    
    //이메일
    @IBOutlet var emailIdLabel: UITextField!
    @IBOutlet var emailCheckLb: UILabel!
    
    //패스워드
    @IBOutlet var passwdLabel: UITextField!
    @IBOutlet var passwdCheckLb: UILabel!
    
    //팩스워드 확인
    @IBOutlet var repasswdLabel: UITextField!
    @IBOutlet var repassCheckLb: UILabel!
    
    //폰번호 확인
    @IBOutlet var phoneLabel: UITextField!
    @IBOutlet var phoneCheckLb: UILabel!
    
    //추천인
    
    @IBOutlet var recommender: UITextField!
    
    
    //정보 동의 체크
    @IBOutlet var checkBoxBtn: UIImageView!
    
    //회원가입 버튼
    @IBOutlet weak var joinBtn: UIButton!
    
    @IBOutlet var personCheckBtn: UIImageView!
    
    var socialVo : SocialLoginVO?
    
    var toatLb : UILabel?
    
    var passLabelShow = false
    
    var keyboardShow = false
    
    let checkImg = UIImage(named: "ico_ch_on")
    
    let unckeckImg = UIImage(named: "ico_ch_off")
    
    var isChecked : Bool?
    
    var isPersonChecked : Bool?
    
    var changeInheight : CGFloat? {
        willSet {
            if passLabelShow {
                toatLb?.frame.origin.y = self.view.frame.height - newValue!
                passLabelShow = false
            }
            
        }
    }
    
    var isEmailChecked : Bool = false
    
    var isNickChecked : Bool = false
    
    var isPhoneChecked : Bool = false
    
    @IBAction func cancelBtnAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //이미지에 탭제스처 적용할경우 필수 조건
        checkBoxBtn.isUserInteractionEnabled = true
        personCheckBtn.isUserInteractionEnabled = true
        
        let tab = UITapGestureRecognizer(target: self, action: #selector(clickCheckBox))
        checkBoxBtn.addGestureRecognizer(tab)
        let personTab = UITapGestureRecognizer(target: self, action: #selector(personClickCheckBox))
        personCheckBtn.addGestureRecognizer(personTab)
        
        
        [emailIdLabel, passwdLabel, repasswdLabel, phoneLabel, nickNameLabel].forEach {
            $0?.addTarget(self, action: #selector(textDidChange), for: UIControl.Event.editingChanged)
        }
        
        passwdLabel.addTarget(self, action: #selector(passwordShow), for: UIControl.Event.editingDidBegin)
        
        joinBtn.backgroundColor = UIColor.DPSellButtonColor
        
        self.checkBoxBtn.image = unckeckImg
        
        self.view.backgroundColor = UIColor.DPViewBackgroundColor
        self.nickNameLabel.attributedPlaceholder = NSAttributedString(string: "register_act_nickname_hint".localized, attributes: [NSAttributedString.Key.foregroundColor: UIColor.DPmainTextColor])
        self.emailIdLabel.attributedPlaceholder = NSAttributedString(string: "register_act_email_hint".localized, attributes: [NSAttributedString.Key.foregroundColor: UIColor.DPmainTextColor])
        self.passwdLabel.attributedPlaceholder = NSAttributedString(string: "register_act_password_hint".localized, attributes: [NSAttributedString.Key.foregroundColor: UIColor.DPmainTextColor])
        self.repasswdLabel.attributedPlaceholder = NSAttributedString(string: "register_act_re_password_hint".localized, attributes: [NSAttributedString.Key.foregroundColor: UIColor.DPmainTextColor])
        self.phoneLabel.attributedPlaceholder = NSAttributedString(string: "register_act_phone_hint".localized, attributes: [NSAttributedString.Key.foregroundColor: UIColor.DPmainTextColor])
        self.recommender.attributedPlaceholder = NSAttributedString(string: "register_act_recom_hint".localized, attributes: [NSAttributedString.Key.foregroundColor: UIColor.DPmainTextColor])
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupKeyboardObservers()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        isChecked = false
        isPersonChecked = false
        isEmailChecked = false
        self.checkBoxBtn.image = unckeckImg
        
        if AppDelegate.socialResult == "JOIN" && socialVo != nil{
            
            print("soical login join ")
            
            self.nickNameLabel.text = socialVo?.nickName
            self.emailIdLabel.text = socialVo?.userid
            self.passwdLabel.text = socialVo?.socialId
            self.repasswdLabel.text = socialVo?.socialId
            
            self.emailIdLabel.isEnabled = false
            self.passwdLabel.isEnabled = false
            self.repasswdLabel.isEnabled = false
            
            emailCheckAction()
            nickCheckAction()
            
            
        }
       
    }
    
    //이메일 검증 메소드
    func emailCheckAction() {
        
        let emailBool = validateEmail(enterEmail: self.emailIdLabel.text ?? "")
        
        if emailBool {
            
            
            var urlComponents = URLComponents(string: "\(AppDelegate.url)/auth/emailCheck?email=\(self.emailIdLabel.text!)")!
            
            let amo = Alamofire.request(urlComponents)
            amo.responseString(completionHandler: { response in
                guard let check = response.result.value else {
                    self.emailCheckLb.textColor = UIColor.DPBuyDefaultColor
                    self.emailCheckLb.text = "error".localized
                    return
                }
                
                if check == "N" {
                    self.isEmailChecked = true
                    self.emailCheckLb.textColor = UIColor.DPSellDefaultColor
                    self.emailCheckLb.text = "available".localized
                }else {
                    self.isEmailChecked = false
                    self.emailCheckLb.textColor = UIColor.DPBuyDefaultColor
                    self.emailCheckLb.text = "overlap".localized
                }
            })
            
        }else if self.emailIdLabel.text ?? "" == "" {
            self.emailCheckLb.text = ""
        }else {
            self.emailCheckLb.textColor = UIColor.DPBuyDefaultColor
            self.emailCheckLb.text = "impossible".localized
        }
        
    }
    
    func nickCheckAction(){
        
        let nickBool = validateEmail(enterEmail: self.emailIdLabel.text ?? "")
        
        if nickBool {
            
            let urlStr = "\(AppDelegate.url)/auth/nickCheck?nick=\(self.emailIdLabel.text ?? "")"
            let encoded = urlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            
            guard let urlComponents = URLComponents(string: encoded) else {
                print("usl nil error")
                return
            }
            
            let amo = Alamofire.request(urlComponents)
            amo.responseString(completionHandler: { response in
                guard let check = response.result.value else {
                    self.nickCheckLb.textColor = UIColor.DPBuyDefaultColor
                    self.nickCheckLb.text = "error".localized
                    return
                }
                
                if check == "N" {
                    self.isEmailChecked = true
                    self.nickCheckLb.textColor = UIColor.DPSellDefaultColor
                    self.nickCheckLb.text = "available".localized
                }else {
                    self.isEmailChecked = false
                    self.nickCheckLb.textColor = UIColor.DPBuyDefaultColor
                    self.nickCheckLb.text = "overlap".localized
                }
            })
            
        }else if self.nickCheckLb.text ?? "" == "" {
            self.nickCheckLb.text = ""
        }else {
            self.nickCheckLb.textColor = UIColor.DPBuyDefaultColor
            self.nickCheckLb.text = "impossible".localized
        }
    }
    
    func phoneCheckAction() {
        let phoneBool = validateEmail(enterEmail: self.emailIdLabel.text ?? "")
        
        if phoneBool {
            
            var urlComponents = URLComponents(string: "\(AppDelegate.url)/auth/emailCheck?email=\(self.emailIdLabel.text!)")!
            
            let amo = Alamofire.request(urlComponents)
            amo.responseString(completionHandler: { response in
                guard let check = response.result.value else {
                    self.phoneCheckLb.textColor = UIColor.DPBuyDefaultColor
                    self.phoneCheckLb.text = "error".localized
                    return
                }
                
                if check == "N" {
                    self.isPhoneChecked = true
                    self.phoneCheckLb.textColor = UIColor.DPSellDefaultColor
                    self.phoneCheckLb.text = "available".localized
                }else {
                    self.isPhoneChecked = false
                    self.phoneCheckLb.textColor = UIColor.DPBuyDefaultColor
                    self.phoneCheckLb.text = "overlap".localized
                }
            })
            
        }else if self.phoneCheckLb.text ?? "" == "" {
            self.phoneCheckLb.text = ""
        }else {
            self.phoneCheckLb.textColor = UIColor.DPBuyDefaultColor
            self.phoneCheckLb.text = "impossible".localized
        }
    }
    
    func passwdCheckAction(){
        
        let passwdBool = validatePassword(enterPwd: self.passwdLabel.text ?? "")
        
        if passwdBool {
            self.passwdCheckLb.textColor = UIColor.DPSellDefaultColor
            self.passwdCheckLb.text = "available".localized
        }else if self.passwdLabel.text ?? "" == "" {
            self.passwdCheckLb.text = ""
        }else {
            self.passwdCheckLb.textColor = UIColor.DPBuyDefaultColor
            self.passwdCheckLb.text = "impossible".localized
        }
    }
    
    func repasswdCheckAction(){

        if self.passwdLabel.text ?? "" == ""{
            self.repassCheckLb.textColor = UIColor.DPBuyDefaultColor
            self.repassCheckLb.text = "inconsistency".localized
        }else if self.passwdLabel.text == self.repasswdLabel.text {
            self.repassCheckLb.textColor = UIColor.DPSellDefaultColor
            self.repassCheckLb.text = "same".localized
        }else if self.repasswdLabel.text ?? "" == "" {
            self.repassCheckLb.text = ""
        }else {
            self.repassCheckLb.textColor = UIColor.DPBuyDefaultColor
            self.repassCheckLb.text = "inconsistency".localized
        }
        
    }
    
    @objc func passwordShow() {
        print("password show start")
        
            guard let height = self.changeInheight else {
                passLabelShow = true
                toatLb = showToast(message: "password_setting".localized, textcolor: UIColor.white, frame: CGRect(x: self.view.frame.size.width/2 - 150, y: self.view.frame.size.height - 100  , width: 300, height: 35), time: 5.0, animation: true)
                return
            }
            
            toatLb = showToast(message: "password_setting".localized, textcolor: UIColor.white, frame: CGRect(x: self.view.frame.size.width/2 - 150, y: self.view.frame.size.height - height  , width: 300, height: 35), time: 5.0, animation: true)
            
    }
    
    //동의 박스 클릭이벤트 메소드
    @objc func clickCheckBox() {
        
        print("tab click ")
        if self.isChecked! {
            self.isChecked = false
            self.checkBoxBtn.image = self.unckeckImg
            
        }else {
            self.isChecked = true
            self.checkBoxBtn.image = self.checkImg
        }
    }
    
    @objc func personClickCheckBox(){
        
        print("persiontab click ")
        if self.isPersonChecked! {
            self.isPersonChecked = false
            self.personCheckBtn.image = self.unckeckImg
            
        }else {
            self.isPersonChecked = true
            self.personCheckBtn.image = self.checkImg
        }
        
    }
    
    //회원가입 버튼 클릭이벤트 메소드
    @IBAction func joinBtnAction(_ sender: UIButton) {
       
        /*
        let alert = UIAlertController(title: "회원가입", message: "현재는 kdex.io 사이트를 통해 가입가능합니다.", preferredStyle: UIAlertControllerStyle.alert)
        
        let cancel = UIAlertAction(title: "확인", style: UIAlertActionStyle.cancel)
        
        alert.addAction(cancel)
        
        self.present(alert, animated: true)
       */
        
        //필드 값 체크
        if( self.isNickChecked ){
            let alert = UIAlertController(title: "screenName".localized, message: "plz_screenName".localized, preferredStyle: UIAlertController.Style.alert)
            
            let cancel = UIAlertAction(title: "common_yes_btn".localized, style: UIAlertAction.Style.cancel)
            
            alert.addAction(cancel)
            
            self.present(alert, animated: true)
            
        }else if(self.passwdLabel.text!.isEmpty) {
            let alert = UIAlertController(title: "register_act_password_hint".localized, message: "send_pw_input_title".localized, preferredStyle: UIAlertController.Style.alert)
            
            let cancel = UIAlertAction(title: "common_yes_btn".localized, style: UIAlertAction.Style.cancel)
            
            alert.addAction(cancel)
            
            self.present(alert, animated: true)
        }else if(self.repasswdLabel.text!.isEmpty){
            let alert = UIAlertController(title: "register_act_re_password_hint".localized, message: "send_pw_confirm_inform".localized, preferredStyle: UIAlertController.Style.alert)
            
            let cancel = UIAlertAction(title: "common_yes_btn".localized, style: UIAlertAction.Style.cancel)
            
            alert.addAction(cancel)
            
            self.present(alert, animated: true)
        }else if(self.passwdLabel.text != self.repasswdLabel.text){
            
            let alert = UIAlertController(title: "common_yes_btn".localized, message: "send_pw_no_match_inform".localized, preferredStyle: UIAlertController.Style.alert)
            
            let cancel = UIAlertAction(title: "common_yes_btn".localized, style: UIAlertAction.Style.cancel)
            
            alert.addAction(cancel)
            
            self.present(alert, animated: true)
            
        }else if( !isChecked! ){
            
            let alert = UIAlertController(title: "agreement".localized, message: "agreement_check".localized, preferredStyle: UIAlertController.Style.alert)
            
            let cancel = UIAlertAction(title: "common_yes_btn".localized, style: UIAlertAction.Style.cancel)
            
            alert.addAction(cancel)
            
            self.present(alert, animated: true)
        }else if( !isPersonChecked! ){
            
            let alert = UIAlertController(title: "agreement".localized, message: "person_agreement_check".localized, preferredStyle: UIAlertController.Style.alert)
            
            let cancel = UIAlertAction(title: "common_yes_btn".localized, style: UIAlertAction.Style.cancel)
            
            alert.addAction(cancel)
            
            self.present(alert, animated: true)
        }else if( !isEmailChecked ){
            
            let alert = UIAlertController(title: "my_info_email".localized, message: "email_overlap".localized, preferredStyle: UIAlertController.Style.alert)
            
            let cancel = UIAlertAction(title: "common_yes_btn".localized, style: UIAlertAction.Style.cancel)
            
            alert.addAction(cancel)
            
            self.present(alert, animated: true)
        }else if (self.phoneLabel.text?.isEmpty)! {
            let alert = UIAlertController(title: "my_info_phone".localized, message: "input_phoneNum".localized, preferredStyle: UIAlertController.Style.alert)
            
            let cancel = UIAlertAction(title: "common_yes_btn".localized, style: UIAlertAction.Style.cancel)
            
            alert.addAction(cancel)
            
            self.present(alert, animated: true)
        }
        
        else{
            
            //url to php file
            let url = NSURL(string: "\(AppDelegate.url)/auth/joinProc")
            
            //request to this file
            let request = NSMutableURLRequest(url: url as! URL)
            
            //method to pass data to this file
            request.httpMethod = "POST"
            
            request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            
            //body to be appended to url
            let param = "userId=\(self.emailIdLabel.text!)&userPw=\(self.passwdLabel.text!)&userName=\(self.nickNameLabel.text!)&userMobile=\(self.phoneLabel.text!)&device=M&recom=\(self.recommender.text!)"
            
            print("join param: \(param)")
            
            let paramData = param.data(using: .utf8)
            
            request.httpBody = paramData
            
            request.setValue(String(describing: paramData!.count), forHTTPHeaderField: "Content-Length")
            
            //request.httpBody = body.data(using: String.Encoding.utf8, allowLossyConversion: true )
            
            //laucnhing
            let session = URLSession.shared
            let task = session.dataTask(with:request as URLRequest) {data, response, error in
                                let result = String(data: data!, encoding: String.Encoding.utf8) as? String
                
                if error == nil {
                   
                    DispatchQueue.main.async {
                        do {
                            
                            let alert = UIAlertController(title: "singUp".localized, message: "join_summit".localized, preferredStyle: .alert)
                            
                            let act = UIAlertAction(title: "common_yes_btn".localized, style: .default) { (action) in
                                self.dismiss(animated: false, completion: nil)
                                
//                                self.navigationController?.popViewController(animated: true)
                            }
                            alert.addAction(act)
                            
                            self.present(alert, animated: true, completion: nil)
                            
                        }catch{
                            print("cCaught an error:\(error)")
                        }
                    }
                }else{
                    print("error:\(error)")
                }
                
                //launch prepared session
            }
            task.resume()
        }
        
    }
    
    //이메일 정규식
    func validateEmail(enterEmail:String) -> Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enterEmail)
    }
    
    //패스워드 정규식
    func validatePassword(enterPwd:String) -> Bool {
        let pwdFormat = "^(?=.*\\d)(?=.*[~`.!@#$%\\^&*()-])(?=.*[a-z])(?=.*[A-Z]).{8,16}$"
        let pwdPredicate = NSPredicate(format:"SELF MATCHES %@", pwdFormat)
        return pwdPredicate.evaluate(with: enterPwd)
    }
    
    //키보드 리턴키 클릭이벤트 메소드
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if (textField.isEqual(emailIdLabel)){
            if( !validateEmail(enterEmail: self.emailIdLabel.text!) ){
                let alert = UIAlertController(title: "email".localized, message: "not_email".localized, preferredStyle: UIAlertController.Style.alert)
                
                let cancel = UIAlertAction(title: "common_yes_btn".localized, style: UIAlertAction.Style.cancel)
                
                alert.addAction(cancel)
                
                self.present(alert, animated: true)
            }
        }else if(textField.isEqual(passwdLabel)){
            if( !validatePassword(enterPwd: self.passwdLabel.text!)){
                let alert = UIAlertController(title: "register_act_password_hint".localized, message: "input_seven".localized, preferredStyle: UIAlertController.Style.alert)
                
                let cancel = UIAlertAction(title: "common_yes_btn".localized, style: UIAlertAction.Style.cancel)
                
                alert.addAction(cancel)
                
                self.present(alert, animated: true)
            }
        }
        
        self.view.endEditing(true)
        return true
    }
    
    //뷰 터치 이벤트 메소드
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        [nickNameLabel, passwdLabel, repasswdLabel, emailIdLabel, phoneLabel].forEach { $0?.resignFirstResponder() }
    }
    
    @objc func textDidChange(_ textfield: UITextField){
        
        if textfield == passwdLabel {
            passwdCheckAction()
        }else if textfield == emailIdLabel {
            emailCheckAction()
        }else if textfield == repasswdLabel {
            repasswdCheckAction()
        }else if textfield == nickNameLabel {
            nickCheckAction()
        }else if textfield == phoneLabel {
           phoneCheckAction()
        }
        
    }
    
    func setupKeyboardObservers() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardWillHide), name: UIResponder.keyboardDidHideNotification, object: nil)
     
    }
    
    @objc func handleKeyboardWillShow(_ notification: Notification) {
        print("keyboard show")
        
        if !keyboardShow {
            adjustingheight(show: true, notification: notification as Notification)
        }
    }
    
    @objc func handleKeyboardWillHide(_ notification: Notification) {
        print("keyboard hidden")
        if toatLb != nil {
            toatLb?.removeFromSuperview()
        }
        
        adjustingheight(show: false, notification: notification as Notification)
        
    }
    
    func adjustingheight(show : Bool, notification: Notification) {
        self.keyboardShow ? (self.keyboardShow = false) : (self.keyboardShow = true)
        
        let userInfo = notification.userInfo
        
        let keyboardFrame = (userInfo![UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        
        changeInheight = keyboardFrame.height + 60
        
    }
    
}
