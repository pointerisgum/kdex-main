//
//  ShowProfitController.swift
//  KDex
//
//  Created by park heewon on 2018. 7. 9..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class ShowProfitController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var jsonArray : [[String : Any]]?
    
    let activity = UIActivityIndicatorView()
    
    @IBOutlet var headerLb: UILabel!
    @IBOutlet var headerLb2: UILabel!
    @IBOutlet var headerLb3: UILabel!
    @IBOutlet var headerLb4: UILabel!
    @IBOutlet var headerLb5: UILabel!
    @IBOutlet var headerLb6: UILabel!
    @IBOutlet var headerLb7: UILabel!
    @IBOutlet var headerLb8: UILabel!
    @IBOutlet var headerLb9: UILabel!
    @IBOutlet var headerLb10: UILabel!
    @IBOutlet var headerLb11: UILabel!
    @IBOutlet var stackVi: UIStackView!
    
    var baseDt : String?
    
    @IBOutlet var detailProfitCollectionVi: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        detailProfitCollectionVi.delegate = self
        detailProfitCollectionVi.dataSource = self
        
        detailProfitCollectionVi.layer.borderWidth = 1
        detailProfitCollectionVi.layer.borderColor = UIColor.DPLineBgColor.cgColor
        detailProfitCollectionVi.layoutMargins = UIEdgeInsets.zero
        
        
        [headerLb, headerLb2,headerLb3,headerLb4,headerLb5,headerLb6,headerLb7,headerLb8,headerLb9,headerLb10,headerLb11].forEach {
            $0?.layer.borderColor = UIColor.DPLineBgColor.cgColor
            $0?.layer.borderWidth = 0.5
            
        }
        
        headerLb11.add(border: .bottom, color: UIColor.DPLineBgColor, width: 1)
        
        
        self.view.backgroundColor = UIColor.DPPopupBgColor
        
        showAnimate()
        
        chatMoving()
        
    }
    
    
    
    func showAnimate() {
        
        self.view.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.25) {
            
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform.init(scaleX: 1.0, y: 1.0)
            
        }
        
    }
    
    @IBAction func closeBtnAction(_ sender: Any) {
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0
        }) { (finished: Bool) in
            if(finished)
            {
                self.view.removeFromSuperview()
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return jsonArray?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let temp = jsonArray?[indexPath.row] ?? [String: Any]()
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "showProfitCell", for: indexPath) as? ShowProfitCell

        let date = temp["date"] as? String ?? ""
        let rate = temp["rate"] as? String ?? ""
        let dbRate = stringtoDouble(str: rate) ?? 0.0
        
        cell?.date.text = self.makeTime(date: date)
        cell?.instCd.text = temp["instCd"] as? String ?? ""
        cell?.tradePl.text = self.intToStringComma(data: temp["tradePl"] as? String ?? "")
        cell?.tradeFee.text = self.intToStringComma(data: temp["tradeFee"] as? String ?? "")
        cell?.profit.text = self.intToStringComma(data: temp["profit"] as? String ?? "")
        cell?.balance.text = self.intToStringComma(data: temp["balance"] as? String ?? "")
        cell?.buyQty.text = temp["buyQty"] as? String ?? ""
        cell?.sellQty.text = temp["sellQty"] as? String ?? ""
        cell?.buyAvgPrc.text = temp["buyAvgPrc"] as? String ?? ""
        cell?.sellAvgPrc.text = temp["sellAvgPrc"] as? String ?? ""
        cell?.rate.text = String(format: "%0.2f", dbRate)
        
        cell?.date.add(border: .right, color: UIColor.DPLineBgColor, width: 1)
        cell?.instCd.add(border: .right, color: UIColor.DPLineBgColor, width: 1)
        cell?.tradePl.add(border: .right, color: UIColor.DPLineBgColor, width: 1)
        cell?.tradeFee.add(border: .right, color: UIColor.DPLineBgColor, width: 1)
        cell?.profit.add(border: .right, color: UIColor.DPLineBgColor, width: 1)
        cell?.balance.add(border: .right, color: UIColor.DPLineBgColor, width: 1)
        cell?.buyQty.add(border: .right, color: UIColor.DPLineBgColor, width: 1)
        cell?.sellQty.add(border: .right, color: UIColor.DPLineBgColor, width: 1)
        cell?.buyAvgPrc.add(border: .right, color: UIColor.DPLineBgColor, width: 1)
        cell?.sellAvgPrc.add(border: .right, color: UIColor.DPLineBgColor, width: 1)
        cell?.rate.add(border: .right, color: UIColor.DPLineBgColor, width: 1)
        
        cell?.date.add(border: .bottom, color: UIColor.DPLineBgColor, width: 1)
        cell?.instCd.add(border: .bottom, color: UIColor.DPLineBgColor, width: 1)
        cell?.tradePl.add(border: .bottom, color: UIColor.DPLineBgColor, width: 1)
        cell?.tradeFee.add(border: .bottom, color: UIColor.DPLineBgColor, width: 1)
        cell?.profit.add(border: .bottom, color: UIColor.DPLineBgColor, width: 1)
        cell?.balance.add(border: .bottom, color: UIColor.DPLineBgColor, width: 1)
        cell?.buyQty.add(border: .bottom, color: UIColor.DPLineBgColor, width: 1)
        cell?.sellQty.add(border: .bottom, color: UIColor.DPLineBgColor, width: 1)
        cell?.buyAvgPrc.add(border: .bottom, color: UIColor.DPLineBgColor, width: 1)
        cell?.sellAvgPrc.add(border: .bottom, color: UIColor.DPLineBgColor, width: 1)
        

//        if (indexPath.row % 2) == 0  {
//
//            cell?.date.add(border: .bottom, color: UIColor.DPLineBgColor, width: 1)
//            cell?.instCd.add(border: .bottom, color: UIColor.DPLineBgColor, width: 1)
//            cell?.tradePl.add(border: .bottom, color: UIColor.DPLineBgColor, width: 1)
//            cell?.tradeFee.add(border: .bottom, color: UIColor.DPLineBgColor, width: 1)
//            cell?.profit.add(border: .bottom, color: UIColor.DPLineBgColor, width: 1)
//            cell?.balance.add(border: .bottom, color: UIColor.DPLineBgColor, width: 1)
//            cell?.buyQty.add(border: .bottom, color: UIColor.DPLineBgColor, width: 1)
//            cell?.sellQty.add(border: .bottom, color: UIColor.DPLineBgColor, width: 1)
//            cell?.buyAvgPrc.add(border: .bottom, color: UIColor.DPLineBgColor, width: 1)
//            cell?.sellAvgPrc.add(border: .bottom, color: UIColor.DPLineBgColor, width: 1)
//
//            cell?.backgroundColor = UIColor.DPViewBackgroundColor
//
//        }else {
//            cell?.date.add(border: .bottom, color: UIColor.white, width: 1)
//            cell?.instCd.add(border: .bottom, color: UIColor.white, width: 1)
//            cell?.tradePl.add(border: .bottom, color: UIColor.white, width: 1)
//            cell?.tradeFee.add(border: .bottom, color: UIColor.white, width: 1)
//            cell?.profit.add(border: .bottom, color: UIColor.white, width: 1)
//            cell?.balance.add(border: .bottom, color: UIColor.white, width: 1)
//            cell?.buyQty.add(border: .bottom, color: UIColor.white, width: 1)
//            cell?.sellQty.add(border: .bottom, color: UIColor.white, width: 1)
//            cell?.buyAvgPrc.add(border: .bottom, color: UIColor.white, width: 1)
//            cell?.sellAvgPrc.add(border: .bottom, color: UIColor.white, width: 1)
//
//            cell?.backgroundColor = UIColor.DPViewGrayBackgroundColor
//        }
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
    func chatMoving(){
        
        activity.startAnimating()
        
        let urlComponents = URLComponents(string:"\(AppDelegate.url)auth/profitDetailList?uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&baseDt=\(self.baseDt ?? "")")!
        
        print("urlComponents : \(urlComponents)")
        
        let amo = Alamofire.request(urlComponents)
        amo.responseJSON(completionHandler:
            { response in
                
                guard let tempjson = response.result.value as? [[String : Any]] else {
                    return
                }
                
                self.jsonArray = tempjson
                
                let arr = self.jsonArray?.sorted(by: {
                    
                    guard let num1 = $0["date"] as? String else {
                        return true
                    }
                    
                    guard let num2 = $1["date"] as? String else {
                        return true
                    }
                    
                    return num1 > num2
                    
                })
                
                self.jsonArray = arr
                
                self.activity.stopAnimating()
                
                //테이블 리로드
                self.detailProfitCollectionVi.reloadData()
                
        })
        
    }
    
    func makeTime(date : String) -> String {
        
        let df : DateFormatter = DateFormatter()
        
        df.dateFormat = "yyyyMMdd"
        
        let date_time = df.date(from: date)
        
        df.dateFormat = "yy.MM.dd"
        
        let newDate : String = df.string(from: date_time!)
        
        return newDate
    }
    
    func intToStringComma(data : String) -> String {
        
        let doubleData = stringtoDouble(str: data) ?? 0.0
        let intData = Int(doubleData)
        
        let strData = "\(intData)"
        
        return strData.insertComma
        
    }
    
    func stringtoDouble(str : String) -> Double?{
        let temp = str.components(separatedBy: [","]).joined()
        let double = Double(temp)
        return double ?? 0.0
    }
    
}
