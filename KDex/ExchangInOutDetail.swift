//
//  ExchangInOutDetail.swift
//  crevolc
//
//  Created by crovolc on 2018. 1. 2..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//
//  거래소 즐겨찾기, KRW, BTC, ETH 페이지 분할 컨트롤러

import UIKit
import Foundation
import Alamofire

class ExchangInOutDetail : PagerController, PagerDataSource, RealTimeDelegate {
    
    var selectedFavorites = false
    
    var coinSimbol : String? //코인 아이디
    
    var coinName : String?
    
    var market : String?
    
    var currentTab : Int?
    
    var dataJsondic : [String : Any]? // 데이타시세 값
    
    @IBOutlet weak var openPrice: UILabel! // 현재가
    
    @IBOutlet weak var openPriceKRW: UILabel! //현재가KRW
    
    @IBOutlet weak var updnPrice: UILabel! // 변동가격
    
    @IBOutlet weak var updnRate: UILabel! //변동률
    
    @IBOutlet weak var totalVol: UILabel! //거래량
    
    @IBOutlet weak var volSimbole: UILabel! //심볼
    
    @IBOutlet weak var highestPrice: UILabel! //최고가
    
    @IBOutlet weak var lowestPrice: UILabel! //최저가
    
    let favoriBtnSelectImg = UIImage(named: "btn_star_on")
    
    let favoriBtnNoSelectedImg = UIImage(named: "btn_star_off")
    
    let favoriteBtn : UIButton = {
        let btn = UIButton(type: .system)
        //btn.frame = CGRect(x: 0, y: 5, width: 30, height: 30)
        btn.frame = CGRect(x: 45, y: 5, width: 20, height: 20)
        return btn
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        selectFavority(simbol: coinSimbol!)
        
        self.view.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        
        self.dataSource = self
        
        registerStoryboardControllers()
        customizeTab()
        initNextbtn()
        
        quoteData()
        TcpSocket.sharedInstance.receiverThread?.delegate = self
    }
    
    func initNextbtn(){
        
        let vi = UIView()
        
        let nTitle = UILabel(frame: CGRect(x: 0, y: 0, width: 400, height: 37))
        
        nTitle.textAlignment = .center
        nTitle.font = UIFont.boldSystemFont(ofSize: 15)
        nTitle.textColor = UIColor.DPTitleTextColor
        nTitle.text = "\(coinName!)(\(coinSimbol!.replacingOccurrences(of: "_", with: "/")))"
        nTitle.textAlignment = .left
        
        vi.addSubview(nTitle)
        
        self.navigationItem.titleView = vi
        
        self.navigationItem.titleView?.frame.size = CGSize(width: 400, height: 37)
        
        self.navigationController?.navigationBar.barTintColor =  UIColor.DPNaviBarTintColor
        
        let totalVI = UIView()
        //totalVI.backgroundColor = UIColor.red
        totalVI.frame = CGRect(x: 0, y: 0, width: 120, height: 37)
        totalVI.tintColor = UIColor.white
        
        let icon1 = UIImage(named: "btn_graph")
        let icon2 = UIImage(named: "icn_appbar_talk")
        let icon3 = UIImage(named: "next")
        
        let btnLabel1 = UILabel()
        btnLabel1.text = "차트"
        btnLabel1.font = UIFont.systemFont(ofSize: 10)
        btnLabel1.textColor = UIColor.white
        btnLabel1.textAlignment = .center
        btnLabel1.frame = CGRect(x: 5, y: 24, width: 20, height: 15)
        
        let rightbtn1 = UIButton(type: .system)
        rightbtn1.frame = CGRect(x: 5, y: 2, width: 20, height: 20)
        rightbtn1.setImage(icon1, for: .normal)
        rightbtn1.addTarget(self, action: #selector(goChart), for: .touchUpInside)
        
        let favoriteLabel = UILabel()
        favoriteLabel.text = "즐겨찾기"
        favoriteLabel.font = UIFont.systemFont(ofSize: 10)
        favoriteLabel.textColor = UIColor.white
        favoriteLabel.textAlignment = .center
        favoriteLabel.frame = CGRect(x: 35, y: 24, width: 35, height: 15)
        
        let rightbtn2 = UIButton(type: .system)
        rightbtn2.frame = CGRect(x: 85, y: 5, width: 20, height: 15)
//        btn.frame = CGRect(x: 85, y: 5, width: 20, height: 20)
        rightbtn2.setImage(icon3, for: .normal)
        rightbtn2.addTarget(self, action: #selector(alertAction), for: .touchUpInside)
        
        let resultLabel = UILabel()
        resultLabel.text = "체결내역"
        resultLabel.font = UIFont.systemFont(ofSize: 10)
        resultLabel.textColor = UIColor.white
        resultLabel.textAlignment = .center
        resultLabel.frame = CGRect(x: 75, y: 24, width: 35, height: 15)
        
        if selectedFavorites {
            favoriteBtn.setImage(self.favoriBtnSelectImg, for: .normal)
        }else {
            favoriteBtn.setImage(self.favoriBtnNoSelectedImg, for: .normal)
        }
        
        favoriteBtn.addTarget(self, action: #selector(setupfavorit), for: .touchUpInside)
        
        totalVI.addSubview(favoriteBtn)
        totalVI.addSubview(rightbtn1)
        totalVI.addSubview(btnLabel1)
        totalVI.addSubview(rightbtn2)
        totalVI.addSubview(resultLabel)
        totalVI.addSubview(favoriteLabel)
        
//        let icon1 = UIImage(named: "icn_appbar_alert")
//        let icon2 = UIImage(named: "icn_appbar_talk")
//
//        let rightbtn1 = UIButton(type: .system)
//        rightbtn1.frame = CGRect(x: 45, y: 5, width: 30, height: 30)
//        rightbtn1.setImage(icon1, for: .normal)
//        rightbtn1.addTarget(self, action: #selector(goAlert), for: .touchUpInside)
//
//        let rightbtn2 = UIButton(type: .system)
//        rightbtn2.frame = CGRect(x: 85, y: 5, width: 30, height: 30)
//        rightbtn2.setImage(icon2, for: .normal)
//        rightbtn2.addTarget(self, action: #selector(alertAction), for: .touchUpInside)
       
//        totalVI.addSubview(rightbtn1)
//        totalVI.addSubview(rightbtn2)
      
        let item = UIBarButtonItem(customView: totalVI)
        
        self.navigationItem.rightBarButtonItem = item
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationController?.navigationBar.topItem?.title = ""
        
        self.navigationController?.addShadowToBar(vi: self)
        
//        self.navigationController?.navigationBar.topItem?.title = "\(coinName!)(\(coinSimbol!.replacingOccurrences(of: "_", with: "/")))"
    }
    
    func registerStoryboardControllers() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let listStoryboard = UIStoryboard(name: "List", bundle: nil)
        
        let controller1 = storyboard.instantiateViewController(withIdentifier: "inout_first") as? ExchangInDetail
        let controller2 = storyboard.instantiateViewController(withIdentifier: "inout_first") as? ExchangInDetail
        //let controller2 = storyboard.instantiateViewController(withIdentifier: "inout_second") as? ExchangOutDetail
        let controller3 = listStoryboard.instantiateViewController(withIdentifier: "Li_conclu") as? ListConcluController
        
        var maketsimbole = ""
        var simbole = ""
        
        if (coinSimbol?.contains("_"))! {
            
            guard let strIndex = coinSimbol?.index(of: "_") else {
                return
            }
            
            simbole = String(coinSimbol!.prefix(upTo: strIndex))
            
            guard let tempStrIndex  =  coinSimbol?.index(after: strIndex) else {
                return
            }
            
            maketsimbole = String(coinSimbol![tempStrIndex...])
            
            print("simbole : \(simbole) , maket : \(maketsimbole)")
            
        }else {
            maketsimbole = "KRW"
            simbole = coinSimbol!
            
            print("simbole : \(simbole) , maket : \(maketsimbole)")
            
        }
        
        controller1?.coinSimbol = self.coinSimbol
        controller1?.marksimbol = maketsimbole
        controller1?.simbole = simbole
        controller1?.coinName = coinName ?? ""
        controller1?.buyAndSell = ExchageInOutSimbol.In
        controller2?.coinSimbol = self.coinSimbol
        controller2?.marksimbol = maketsimbole
        controller2?.simbole = simbole
        controller2?.coinName = coinName ?? ""
        controller2?.buyAndSell = ExchageInOutSimbol.Out
        controller3?.coinSimbol = self.coinSimbol
        controller3?.coinName = coinName ?? ""
        
        self.setupPager(tabNames: ["fragment_title_bid".localized, "fragment_title_ask".localized, "fragment_title_pending".localized], tabControllers: [controller1!, controller2!, controller3!])
    }
    
    func customizeTab() {
        indicatorColor = UIColor.white
        tabsViewBackgroundColor = UIColor.DPtableViewBgColor
        contentViewBackgroundColor = UIColor.DPtableViewBgColor
        
        if currentTab == 1 {
            startFromSecondTab = false
        }else{
            startFromSecondTab = true
        }
        
        centerCurrentTab = true
        tabLocation = PagerTabLocation.top
        tabHeight = 49
        tabOffset = 36
        tabTopOffset = 80.0
        tabWidth = self.view.frame.size.width / 3
        fixFormerTabsPositions = false
        fixLaterTabsPosition = false
        animation = PagerAnimation.during
        selectedTabTextColor = .DPmainTextColor
        tabBarHidden = true
        //tabsTextFont = UIFont(name: "HelveticaNeue-Bold", size: 20)!
        tabsTextFont = UIFont.boldSystemFont(ofSize: 15)
        
        tabsTextColor = .DPsubTextColor
    }
    
    // Programatically selecting a tab. This function is getting called on AppDelegate
    func changeTab() {
        self.selectTabAtIndex(3)
    }
    
    @objc func goAlert(){
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "AlectVC") as? AlertViewController else {
            return
        }
        
        vc.coinSimbol = self.coinSimbol!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func alertAction(){
//        let alertView = self.storyboard?.instantiateViewController(withIdentifier: "alertDetail") as? AlertDetailViewController
//        self.navigationController?.pushViewController(alertView!, animated: true)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let detail = storyboard.instantiateViewController(withIdentifier: "ExchangAll") as? ExchangAllListViewController {
            detail.coinSimbol = self.coinSimbol
            detail.coinName = self.coinName
            detail.market = self.market
            self.navigationController?.pushViewController(detail, animated: true)
        }
        
    }
    
    @objc func goChart(){
//        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "AlectVC") as? AlertViewController else {
//            return
//        }
//        vc.coinSimbol = self.coinSimbol!
//        self.navigationController?.pushViewController(vc, animated: true)
    
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChartView") as? ExchangChartView else {
            return
        }
        vc.coinSimbol = self.coinSimbol ?? ""
        vc.coinName = self.coinName ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @objc func setupfavorit(){
        
        print("current page simbol : \(coinSimbol!)")
        
        let plist = UserDefaults.standard
        let uid = plist.string(forKey: "uid")
        let sessionid = plist.string(forKey: "sessionId")
        
        if selectedFavorites == false {
            print("favorites add")
            favorityAction(type: "N", simbol: self.coinSimbol!, uid: uid!, sessionid: sessionid!)
            favoriteBtn.setImage(self.favoriBtnSelectImg, for: .normal)
            selectedFavorites = true
            
        }else {
            print("favorites delete")
            favorityAction(type: "D", simbol: self.coinSimbol!, uid: uid!, sessionid: sessionid!)
            favoriteBtn.setImage(self.favoriBtnNoSelectedImg, for: .normal)
            selectedFavorites = false
            
        }
        
    }
    
    func favorityAction(type : String, simbol : String, uid : String, sessionid : String){
        
        print("uid : \(uid), sessionid: \(sessionid), simbole: \(simbol)")
        
        let url = "\(AppDelegate.url)ajax/favCoinProc?uid=\(uid)&sessionId=\(sessionid)&simbol=\(simbol)&type=\(type)"
        print(url)
        let amo = Alamofire.request(url)
        amo.responseString { response in
            if response.result.isSuccess == true {
                let result = response.result.value
                if result != nil {
                    print("\(result) 즐겨찾기 전송 완료")
                }else{
                    print("전송 실패")
                }
            }
        }
    }
    
    func selectFavority(simbol : String){
        print("selectFavority")
        for row in AppDelegate.favoritesList {
            print(row)
            if row == simbol {
                print("same \(simbol)")
               self.selectedFavorites = true
            }
        }
    }
    
    func quoteData(){
        
        let coinSim = self.coinSimbol
        let plist = UserDefaults.standard
        let uid = plist.string(forKey: "uid")
        let sessionid = plist.string(forKey: "sessionId")
        
        //매칭리스트 값 호출
        var urlComponents = URLComponents(string:"\(AppDelegate.url)app/coinDetail?simbol=\(coinSim!)&uid=\(uid!)&sessionId=\(sessionid!)")!
        
        print("urlComponents : \(urlComponents)")
        
        let amo = Alamofire.request(urlComponents)
        amo.responseJSON(completionHandler:
            { response in

                //json 파싱
                guard let value = response.result.value as? [String: Any] else { return }
                
                self.dataJsondic = value
                
                self.setupQuDate()
                
        })
        
    }
    
    fileprivate func setupQuDate() {
        print("setupQudate start image")
        
        var coinNameSimbole = ""
        var doubleLastKrwValue = 0.0
        let openprice = self.dataJsondic?["lastPrice"] as? String
        let highestprice = self.dataJsondic?["highPrice"] as? String
        let lowestprice = self.dataJsondic?["lowPrice"] as? String
        let updnrate = self.dataJsondic?["updnRate"] as? String
        let updnSign = self.dataJsondic?["updnSign"] as? String
        let totalVol = self.dataJsondic?["totalVol"] as? String
        let updnPrice = self.dataJsondic?["updnPrice"] as? String
        
        //market simbole 수정
        if self.market ?? "" == "ETH"{
            doubleLastKrwValue = AppDelegate.ETH
        }else if self.market ?? "" == "BTC" {
            doubleLastKrwValue = AppDelegate.BTC
        }else {
            doubleLastKrwValue = 1.0
        }
        
        let doubleHightestPrice = self.stringtoDouble(str: highestprice ?? "0") ?? 0
        
        DispatchQueue.main.async {
            if (highestprice?.contains("."))! {
                self.highestPrice.text = String(format: "%.8f", doubleHightestPrice)
            }else {
                self.highestPrice.text = highestprice?.insertComma
            }
            
            let doubleLowPrice = self.stringtoDouble(str: lowestprice ?? "0") ?? 0
            
            if (lowestprice?.contains("."))! {
                self.lowestPrice.text = String(format: "%.8f", doubleLowPrice)
            }else {
                //lowestprice?.insertComma
                self.lowestPrice.text = lowestprice?.insertComma
            }
            
            let doubleOpenPrice = self.stringtoDouble(str: openprice ?? "0") ?? 0
            
            if (openprice?.contains("."))! {
                self.openPrice.text = String(format: "%.8f", doubleOpenPrice)
            }else {
                //openprice?.insertComma
                self.openPrice.text = openprice?.insertComma
            }
            
            if let indexvalue = self.coinSimbol?.index(of: "_") {
                
                coinNameSimbole = String((self.coinSimbol?.prefix(upTo: indexvalue))!)
            }else {
                coinNameSimbole = self.coinSimbol ?? ""
            }
            
            var krwValue = doubleLastKrwValue * doubleOpenPrice
            
            self.openPriceKRW.text = "\(String(format: "%.0f", krwValue.roundToPlaces(places: 0)))".insertComma + "KRW"
            
            if self.market ?? "" == "ETH" || self.market ?? "" == "BTC" {
                self.updnPrice.text = String(format: "%0.8f", self.stringtoDouble(str: updnPrice!) ?? 0.0)
            }else {
                self.updnPrice.text = String(format: "%0.2f", self.stringtoDouble(str: updnPrice!) ?? 0.0).insertComma
            }
            
            self.totalVol.text = totalVol
            self.volSimbole.text = coinNameSimbole
            self.updnRate.text = self.stringChage(value1: nil, value2: "%", origin: updnrate!)
            
            if updnSign == "-1" {
                self.openPrice.text = (self.openPrice.text ?? "") + " ▼"
                self.openPrice.textColor = UIColor.DPMinTextColor
                self.updnRate.textColor = UIColor.DPMinTextColor
                self.updnPrice.textColor = UIColor.DPMinTextColor
            }else if updnSign == "1" {
                self.openPrice.text = (self.openPrice.text ?? "") + " ▲"
                self.openPrice.textColor = UIColor.DPPlusTextColor
                self.updnRate.textColor = UIColor.DPPlusTextColor
                self.updnPrice.textColor = UIColor.DPPlusTextColor
            }else {
                self.openPrice.textColor = UIColor.DPmainTextColor
                self.updnRate.textColor = UIColor.DPmainTextColor
                self.updnPrice.textColor = UIColor.DPmainTextColor
            }
            
        }
        
    }
    
    func stringChage(value1: String?, value2: String, origin: String) -> String {
        
        var str : String = origin
        
        if value1 == nil {
            str = "\(str) \(value2)"
            
        }else {
            str = "\(value1!) \(str) \(value2)"
        }
        return str
    }

    
    func stringtoDouble(str : String) -> Double?{
        let temp = str.components(separatedBy: [","]).joined()
        let double = Double(temp)
        return double ?? 0.0
    }
    
    func exchangListRT(bt: NPriceTick) {
            
    }
    
    func exchangDetailRT(bt: NPriceTick) {
            
    }
    
    func trandeListRT(bt: NPriceInfo) {
            
    }
    
    func openListRT(bt: NOpenInfo) {
            
    }
    
    func krwChangeRT(value: Double) {
            
    }

    
}

enum ExchageInOutSimbol : String {
    case In = "매수"
    case Out = "매도"
}
