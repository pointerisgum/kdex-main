//
//  ExchangConcluCell.swift
//  crevolc
//
//  Created by crovolc on 2018. 1. 18..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation

class ExchangConcluCell : UITableViewCell {
    
    @IBOutlet var coinLabel: UILabel!
    
    @IBOutlet var sellBuyLabel: UILabel!
    
    @IBOutlet var dateLabel: UILabel!
    
    @IBOutlet var orderCountLabel: UILabel!
    
    @IBOutlet var orderPriceLabe: UILabel!
    
    @IBOutlet var confirmCountLabel: UILabel!
    
    @IBOutlet var concluCountLabel: UILabel!
    
    @IBOutlet var isSelectBtn: UIImageView!
    
    var selectBtn = false
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
}
