//
//  CreateWalletCell.swift
//  crevolc
//
//  Created by crovolc on 2018. 1. 4..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import UIKit

class CreateWalletCell: UITableViewCell {

    @IBOutlet var coinLoginImage: UIImageView!
    
    @IBOutlet var coinNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
