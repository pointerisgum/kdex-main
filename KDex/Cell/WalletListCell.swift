//
//  walletListCell.swift
//  KDex
//
//  Created by park heewon on 2018. 4. 11..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation
import UIKit

class WalletListCell: UITableViewCell {
    
    let walletName : UILabel = {
        let lb = UILabel()
        lb.text = "name BTC"
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.font = UIFont.systemFont(ofSize: 10)
        lb.textColor = UIColor.DPmainTextColor
        lb.textAlignment = .left
        return lb
    }()
    
    let walletQty : UILabel = {
        let lb = UILabel()
        lb.text = "0.00000013"
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.font = UIFont.systemFont(ofSize: 10)
        lb.textColor = UIColor.DPmainTextColor
        lb.textAlignment = .right
        return lb
    }()
    
    let walletImg : UIImageView = {
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.image = UIImage(named: "Bitcoin512.png")
        return img
    }()

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        
        setupcontrant()
    }
    
    func setupcontrant() {
        addSubview(walletName)
        addSubview(walletQty)
        addSubview(walletImg)
       
        
        walletImg.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        walletImg.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10).isActive = true
        walletImg.widthAnchor.constraint(equalToConstant: 15).isActive = true
        walletImg.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        walletName.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        walletName.leftAnchor.constraint(equalTo: walletImg.rightAnchor, constant: 5).isActive = true
        walletName.widthAnchor.constraint(equalToConstant: 100).isActive = true
        walletName.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        walletQty.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        walletQty.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        walletQty.leftAnchor.constraint(equalTo: walletName.rightAnchor, constant: 5).isActive = true
        walletQty.heightAnchor.constraint(equalToConstant: 20).isActive = true
    }
    
}

