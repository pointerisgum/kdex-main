//
//  tokenListCell.swift
//  KDex
//
//  Created by park heewon on 2018. 7. 5..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import UIKit

class tokenListCell: UITableViewCell {
    
    var tokenDelegate : TokenOpenAble?
    
    @IBOutlet var contentVi: UIView!
    //코인 이미지
    @IBOutlet var coinImage: UIImageView!
    
    //코인이름
    @IBOutlet var coinNameLabel: UILabel!
    
    //코인보유금액
    @IBOutlet var coinValueLabel: UILabel!
    
    //코인 심볼
    @IBOutlet var coinSimbol: UILabel!

    @IBOutlet var coinSwitch: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contentVi.frame = self.bounds
        contentVi.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    @IBAction func coinSwitchAction(_ sender: UISwitch) {
        tokenDelegate?.selectTokenSwAction(sender: sender)
    }
    
}
