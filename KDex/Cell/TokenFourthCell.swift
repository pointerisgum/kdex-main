//
//  TokenFourthCell.swift
//  KDex
//
//  Created by park heewon on 2018. 7. 4..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Kingfisher

class TokenFourthCell : UITableViewCell, UITableViewDelegate, UITableViewDataSource{
    
    var tokenDelegate : TokenOpenAble?
    
    let footerVi : UIView = {
        let vi = UIView()
        return vi
    }()
    
    @IBOutlet var openBtn: UIButton!
    
    @IBOutlet var contentVi: UIView!
    
    @IBAction func tonkenOpenBtn(_ sender: UIButton) {
        
        let lb = sender.titleLabel?.text ?? ""
        
        if lb == "▲" {
            sender.setTitle("▼", for: .normal)
        }else {
            sender.setTitle("▲", for: .normal)
        }
        
        tokenDelegate?.openToken()
        
    }
    
    //코인 이미지
    @IBOutlet var coinImage: UIImageView!
    
    //코인이름
    @IBOutlet var coinNameLabel: UILabel!
    
    //코인보유금액
    @IBOutlet var coinValueLabel: UILabel!
    
    //코인 심볼
    @IBOutlet var coinSimbol: UILabel!
    
    //코인 krw
    @IBOutlet var coinKRWLabel: UILabel!
    
    @IBOutlet var tableVi: UITableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        print("awarkeFromNib")
        
        contentVi.frame = self.bounds
        contentVi.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        tableVi.dataSource = self
        tableVi.delegate = self
        tableVi.isScrollEnabled = false
        
        tableVi.register(UINib(nibName: "SubFourthCell", bundle: nil), forCellReuseIdentifier: "subCellId")
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        print("wallet favorites count : \(AppDelegate.favoriesTokenWalletListArray.count)")
        
        return AppDelegate.favoriesTokenWalletListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
         print("wallet favorites indexpath : \(indexPath.row)")
        
        let walletJson = AppDelegate.favoriesTokenWalletListArray[indexPath.row]
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "subCellId", for: indexPath) as? SubFourthCell else {
            return UITableViewCell()
        }
        
        cell.coinNameLabel.text = "\(walletJson["simbolName"]!)"
        cell.coinSimbol.text = "\(walletJson["simbol"]!)"
        cell.coinNameLabel.adjustsFontSizeToFitWidth = true
        cell.coinNameLabel.sizeToFit()

        cell.coinImage.kf.setImage(with: AppDelegate.dataImageUrl(str: "\(walletJson["simbol"]!)"))

        cell.coinValueLabel.text = "\(walletJson["dpoQty"]!)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return footerVi
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let walletJson = AppDelegate.favoriesTokenWalletListArray[indexPath.row]
        
        tokenDelegate?.sendWalletViAction(value: walletJson)
        
    }
    
}
