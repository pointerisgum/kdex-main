//
//  FourthCell.swift
//  crevolc
//
//  Created by crovolc on 2017. 12. 27..
//  Copyright © 2017년 hanbiteni. All rights reserved.
//

import UIKit
import Foundation

class FourthCell : UITableViewCell{
    
    var delegate : FourthViewController?
    
    var tempWalletArray = [String : Any]()
    
    //코인 이미지
    @IBOutlet var coinImage: UIImageView!
    
    //코인이름
    @IBOutlet var coinNameLabel: UILabel!
    
    //코인보유금액
    @IBOutlet var coinValueLabel: UILabel!
    
    //코인 심볼
    @IBOutlet var coinSimbol: UILabel!
    
    //코인 krw 
    @IBOutlet var coinKRWLabel: UILabel!
    
    @IBOutlet var backVi: UIView!
    
    @IBOutlet var createBtn: UIButton!
    
    @IBAction func createBtnAction(_ sender: Any) {
        
        let bt = sender as? UIButton
        
        print("wallet create btn tag click : \(bt?.tag)")
        
        let simboleValue = tempWalletArray["simbol"] as? String ?? ""
        
//        let simboleValue = tempWalletArray["simbol"] as? String ?? ""
        
        print("temp simbole value : \(tempWalletArray.count)")
        
        print("wallet create btn simbole : \(simboleValue)")
        
        delegate?.privateKeyCreate(simbole: simboleValue)
        
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        coinValueLabel.adjustsFontSizeToFitWidth = true
    }
    
    
}
