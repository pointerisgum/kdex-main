//
//  AlertViewCell.swift
//  DEXnPAY
//
//  Created by park heewon on 2018. 3. 19..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation

class AlertViewCell : UITableViewCell {
    
    @IBOutlet var simbolLabel: UILabel!
    
    @IBOutlet var deleteBtn: UIButton!
    
}
