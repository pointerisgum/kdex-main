//
//  ExchangListCell.swift
//  crevolc
//
//  Created by crovolc on 2017. 12. 28..
//  Copyright © 2017년 hanbiteni. All rights reserved.
//
import UIKit
import Foundation

class ExchangListCell : UITableViewCell {
    
    var coinSymbol : String?
    
    @IBOutlet var backVi: UIView!
    
    @IBOutlet var coinName: UILabel!
    
    @IBOutlet var simbol: PaddingLabel!
    
    @IBOutlet var coinVol: UILabel!
    
    @IBOutlet var coinValue: PaddingLabel!
    
    @IBOutlet var coinPer: PaddingLabel!
    
    @IBOutlet var coinPerBackground: UIView!
  
    @IBOutlet var perVsValue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundColor = UIColor(rgb: 0xf3f4f5)
        
        self.coinPer.adjustsFontSizeToFitWidth = true
        self.coinVol.adjustsFontSizeToFitWidth = true
        self.perVsValue.adjustsFontSizeToFitWidth = true
        self.coinName.textColor = UIColor.DPmainTextColor
        self.coinValue.textColor = UIColor.DPSellDefaultColor
        self.coinVol.textColor = UIColor.DPmainTextColor
        self.coinPer.textColor = UIColor.DPSellDefaultColor
        self.coinPerBackground.backgroundColor = UIColor.clear
        
        backVi.layer.masksToBounds = true
        backVi.layer.cornerRadius = 5
        
        coinValue.font = UIFont.systemFont(ofSize: 14)
//        coinValue.layer.borderWidth = 1
//        coinValue.layer.borderColor = UIColor.clear.cgColor
//
//        coinPerBackground.layer.cornerRadius = 4
//        coinPerBackground.layer.masksToBounds = true
        
    }
    
}
