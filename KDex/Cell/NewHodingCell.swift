//
//  NewHodingCell.swift
//  KDex
//
//  Created by park heewon on 2018. 6. 25..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import UIKit

class NewHodingCell: UITableViewCell {
    
//    var allAccet = 0.0 //총보유자산
//    var allAmount = 0.0 //총매수
//    var allTest = 0.0 //총평가
//    var allTestProfit = 0.0 //평가손익
//    var profitRate = 0.0 //수익률
    var cellindex = 0
    var delegate : NewHodingController?
    
    @IBOutlet var editFlaglb: UILabel!
    
    @IBOutlet var backVi: UIView!
    @IBOutlet var coinSimbole: UILabel!
    @IBOutlet var coinName: UILabel!
    @IBOutlet var img: UIImageView!
    @IBOutlet var lineVi: UIView!
    @IBOutlet var profitRateCl: UILabel! //평가손익률
    @IBOutlet var allTestProfitCl: UILabel! //평가손익
    @IBOutlet var allAccetCl: UILabel! //보유수량
    @IBOutlet var allTestCl: UILabel! //평가금액
    @IBOutlet var buyEvalCl: UILabel! //매수평균가
    @IBOutlet var buyPriceCl: UILabel!  //매수금액
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        allTestProfitCl.adjustsFontSizeToFitWidth = true
        allTestProfitCl.sizeToFit()
        allAccetCl.adjustsFontSizeToFitWidth = true
        allAccetCl.sizeToFit()
        allTestCl.adjustsFontSizeToFitWidth = true
        allTestCl.sizeToFit()
        buyPriceCl.adjustsFontSizeToFitWidth = true
        buyPriceCl.sizeToFit()
        
        backgroundColor = UIColor(rgb: 0xf3f4f5)
        lineVi.backgroundColor = UIColor(rgb: 0xcacacd)
        backVi.layer.masksToBounds = true
        backVi.layer.cornerRadius = 5
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    @IBAction func evalBtnAction(_ sender: Any) {
        delegate?.evalBtnDelegateAction(index: cellindex)
    }
    
}
