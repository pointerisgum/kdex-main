//
//  LoginCell.swift
//  DEXnPAY
//
//  Created by park heewon on 2018. 3. 28..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation

class LoginCell: UITableViewCell {
    
    
    @IBOutlet var connectDate: UILabel!
    
    @IBOutlet var deviceType: UILabel!
    
    @IBOutlet var ipaddress: UILabel!
    
}
