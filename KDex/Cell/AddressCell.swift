//
//  AddressCell.swift
//  KDex
//
//  Created by park heewon on 2018. 6. 8..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

class AddressCell : UITableViewCell {
    
    var delete : AddressListController?
    
    @IBOutlet var addressName: UILabel!
    
    @IBOutlet var address: UILabel!
    
    @IBOutlet var deleteBtn: UIButton!
    
    @IBAction func deleteAction(_ sender: Any) {
        print("wallet address delete")
        
        let addr = address.text ?? ""
        addressDelete(addr: addr)
        
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        deleteBtn.setTitleColor(UIColor.DPPlusTextColor, for: .normal)
        deleteBtn.layer.cornerRadius = 2
        deleteBtn.clipsToBounds = true
        deleteBtn.layer.borderColor = UIColor.DPPlusTextColor.cgColor
        deleteBtn.layer.borderWidth = 1
    }
    
    private func addressDelete(addr: String) {
        
        let simbole = delete?.coinSimbol ?? ""
        
        let url = "\(AppDelegate.url)/auth/delAddr?simbol=\(simbole)&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&inputAddr=\(addr)"

        print("wallet inputAddr : \(url)")

        let amo = Alamofire.request(url)
        amo.responseJSON(completionHandler: { response in

            let result = response.result.value

            print("wallet list delete : \(result)")
            
            self.delete?.addressList()

        })
    }
    
}
