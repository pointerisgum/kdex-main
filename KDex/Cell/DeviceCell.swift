//
//  DeviceCell.swift
//  KDex
//
//  Created by park heewon on 2018. 9. 19..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import UIKit
import Alamofire

class DeviceCell: UITableViewCell {
    
    var delegate : DeviceController?
    
    var morbtnOpenBool = false
    
    var deviceId = ""
    
    var deleteBool = "N"
    
    var indexPathRow : IndexPath?
    
    @IBOutlet var bgView: UIView!
    
    @IBOutlet var mainCheckImg: UIImageView!
    
    @IBOutlet var deviceModelName: UILabel!
    
    @IBOutlet var deviceOs: UILabel!
    
    @IBOutlet var deviceRegsterDate: UILabel!
    
    @IBOutlet var privateKeyImg: UIImageView!
    
    @IBOutlet var privateKeyInitBtn: UIButton!
    
    @IBOutlet var popVi: UIView!
    
    @IBOutlet var mainChangeBtn: UIButton!
    
    @IBOutlet var deleteBtn: UIButton!
    
    @IBOutlet var moreBtn: UIButton!
    
    @IBOutlet var privateLb: UILabel!
    
    @IBOutlet var currentDviceImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        popVi.isHidden = true
        privateKeyInitBtn.isHidden = true
        
        popVi.layer.borderColor = UIColor(rgb: 0xdadada).cgColor
        popVi.layer.borderWidth = 1
        
        bgView.layer.masksToBounds = true
        bgView.layer.cornerRadius = 5
        bgView.layer.borderColor = UIColor(rgb: 0xdadada).cgColor
        bgView.layer.borderWidth = 1
        
        privateKeyInitBtn.layer.borderColor = UIColor(rgb: 0xdadada).cgColor
        privateKeyInitBtn.layer.borderWidth = 1
        privateKeyInitBtn.layer.masksToBounds = true
        privateKeyInitBtn.layer.cornerRadius = 5
        
    }
    
    @IBAction func morBtnAction(_ sender: Any) {
        
        delegate?.morebtnSelecteindexRow = self.indexPathRow
        
        var tempArray = [[String : Any]]()
        
        var temindex = 0
        
        for row in (delegate?.deviceArray)! {
            
            var value = row
            
            let bool = row["openBool"] as? String ?? "N"
            
            if temindex == self.indexPathRow?.row {
                
                if bool == "N" {
                    value.updateValue("Y", forKey: "openBool")
                }else {
                    value.updateValue("N", forKey: "openBool")
                }
                
            }else {
                value.updateValue("N", forKey: "openBool")
            }
            
            tempArray.append(value)
            
            temindex += 1
            
        }
        
        
        delegate?.deviceArray.removeAll()
        delegate?.deviceArray = tempArray
        
        
//        if morbtnOpenBool {
//
//            moreBtn.setImage(UIImage(named: "more_off"), for: .normal)
//            popVi.isHidden = true
//            morbtnOpenBool = false
//
//        }else {
//
//            moreBtn.setImage(UIImage(named: "more_on"), for: .normal)
//            popVi.isHidden = false
//            morbtnOpenBool = true
//
//        }
        
        print("cell more after open Bool (\(indexPathRow?.row) : \(morbtnOpenBool)")
        
        delegate?.deviceListTb.reloadData()
        
    }
    
    @IBAction func deleteBtnAction(_ sender: Any) {
        popVi.isHidden = true
        delegate?.stopWallet(dv: self.deviceId)
    }
    
    @IBAction func mainChangeBtnAction(_ sender: Any) {
        
        popVi.isHidden = true
        delegate?.mainDeviceChange(dv: self.deviceId)
        
    }
    
    @IBAction func initAndDeleteBtnAction(_ sender: UIButton) {
        
        delegate?.initDeleteDeviceAction(dv: self.deviceId, deleteBool: self.deleteBool)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
