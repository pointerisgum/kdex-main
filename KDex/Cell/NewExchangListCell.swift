//
//  NewExchangListCell.swift
//  KDex
//
//  Created by park heewon on 2018. 8. 10..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import UIKit

class NewExchangListCell: UITableViewCell {
    
    var coinSymbol : String?
    
    @IBOutlet var backVi: UIView!
    
    @IBOutlet var coinName: UILabel!
    
    @IBOutlet var simbol: PaddingLabel!
    
    @IBOutlet var coinVol: UILabel!
    
    @IBOutlet var coinValue: PaddingLabel!
    
    @IBOutlet var coinPer: PaddingLabel!
    
    @IBOutlet var coinPerBackground: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundColor = UIColor(rgb: 0xf3f4f5)
        
        self.coinPer.adjustsFontSizeToFitWidth = true
        self.coinVol.adjustsFontSizeToFitWidth = true
        self.coinName.textColor = UIColor.DPmainTextColor
        self.coinValue.textColor = UIColor.DPSellDefaultColor
        self.coinVol.textColor = UIColor.DPmainTextColor
        self.coinPer.textColor = UIColor.DPSellDefaultColor
        self.coinPerBackground.backgroundColor = UIColor.clear
        
        coinValue.font = UIFont.systemFont(ofSize: 14)
        coinValue.layer.borderWidth = 1
        coinValue.layer.borderColor = UIColor.clear.cgColor
        
        coinPerBackground.layer.cornerRadius = 4
        coinPerBackground.layer.masksToBounds = true
        
        backVi.layer.masksToBounds = true
        backVi.layer.cornerRadius = 5
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
