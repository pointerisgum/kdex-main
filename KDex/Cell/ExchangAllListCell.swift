//
//  ExchangAllListCell.swift
//  crevolc
//
//  Created by crovolc on 2018. 1. 11..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//
import UIKit
import Foundation

class ExchangAllListCell : UITableViewCell {

    @IBOutlet var AllListDateLabel: UILabel!
    
    @IBOutlet var priceValueLabel: UILabel!
    
    @IBOutlet var countValueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundColor = UIColor.DPtableCellBgColor
        
        self.AllListDateLabel.textColor = UIColor.DPmainTextColor
        self.priceValueLabel.textColor = UIColor.DPBuyDefaultColor
        self.countValueLabel.textColor = UIColor.DPmainTextColor
        
    }
}
