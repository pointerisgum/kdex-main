//
//  ListCell.swift
//  crevolc
//
//  Created by crovolc on 2018. 1. 19..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//
import UIKit
import Foundation

class ListCell: UITableViewCell {
    
    var delegate : ListTradeController?
    
    var tempDate : [String : Any]?
    
    @IBOutlet var backVi: UIView!
    
    @IBOutlet var simboleImg: UIImageView!
    
    @IBOutlet var backBarVi: UIView!
    
    @IBOutlet var coinLabel: UILabel!
    
    @IBOutlet var sellBuyLabel: UILabel!
    
    @IBOutlet var dateLabel: UILabel!
    
    @IBOutlet var orderCountLabel: UILabel!
    
    @IBOutlet var orderPriceLabe: UILabel!
    
    @IBOutlet var confirmCountLabel: UILabel!
    
    @IBOutlet var concluCountLabel: UILabel!
    
    @IBOutlet weak var isSelector: UIImageView!
    
    @IBOutlet var concluLabel: UILabel!
    
    @IBOutlet var orderLabel: UILabel!
    
    @IBOutlet var orderPriceLabel: UILabel!
    
    @IBOutlet var confirmLabel: UILabel!
    
    @IBOutlet var totalPriceLabel: UILabel!
    
    @IBOutlet var totalPriceLabeValue: UILabel!
    
    @IBOutlet var benefitLabel: UILabel!
    
    @IBOutlet var benefitLabelValue: UILabel!
    
    @IBOutlet var selectAddrBtn: UIButton!
    
    var selectBtn = false
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBAction func selectAddrBtnAction(_ sender: Any) {
        guard let date = tempDate  else {
            return
        }
        
        delegate?.walletAddressCallUrl(data: date)
        
        print("select btn botton")
    }
}
