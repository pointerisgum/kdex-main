//
//  AlertDetailCell.swift
//  DEXnPAY
//
//  Created by park heewon on 2018. 3. 19..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation

class AlertDetailCell: UITableViewCell {
    
    let imgVi : UIImageView = {
        
        let img = UIImageView()
        img.image = UIImage(named: "icon_read.png")
        img.contentMode = .scaleAspectFill
        img.translatesAutoresizingMaskIntoConstraints = false
        
        return img
    }()
    
    let sendDate : UILabel = {
        let lb = UILabel()
        lb.text = "2018.01.01"
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.font = UIFont.systemFont(ofSize: 14)
        lb.textColor = UIColor(rgb: 0x12335a)
        lb.textAlignment = .left
        return lb
    }()
    
    let typeLabel : UILabel = {
        let lb = UILabel()
        lb.text = "타입"
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.font = UIFont.systemFont(ofSize: 14)
        lb.textColor = UIColor.DPmainTextColor
        lb.textAlignment = .left
        return lb
    }()
    
    let msgType : UILabel = {
        let lb = UILabel()
        lb.text = "test type"
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.font = UIFont.systemFont(ofSize: 14)
        lb.textColor = UIColor.DPmainTextColor
        lb.textAlignment = .left
        return lb
    }()
    
    let sendTime : UILabel = {
        let lb = UILabel()
        lb.text = "PM 10:23"
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.font = UIFont.systemFont(ofSize: 12)
        lb.textColor = UIColor(rgb: 0x858484)
        lb.textAlignment = .left
        return lb
    }()
    
    let msgText : UILabel = {
        let lb = UILabel()
        lb.text = "test alert "
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.font = UIFont.systemFont(ofSize: 14)
        lb.textColor = UIColor.DPmainTextColor
        lb.textAlignment = .left
        return lb
    }()
    
    let more : UILabel = {
        let lb = UILabel()
        lb.text = "상세보기"
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.font = UIFont.systemFont(ofSize: 14)
        lb.textColor = UIColor.DPmainTextColor
        lb.textAlignment = .left
        return lb
    }()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        
        setupcontrant()
    }
    
    func setupcontrant() {
        addSubview(imgVi)
        addSubview(sendDate)
        addSubview(sendTime)
        addSubview(typeLabel)
        addSubview(msgType)
        addSubview(msgText)
//        addSubview(more)
        
        
        imgVi.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        imgVi.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        imgVi.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10).isActive = true
        imgVi.widthAnchor.constraint(equalToConstant: 20).isActive = true
        
        typeLabel.centerYAnchor.constraint(equalTo: imgVi.centerYAnchor).isActive = true
        typeLabel.leadingAnchor.constraint(equalTo: imgVi.trailingAnchor, constant: 10).isActive = true
        typeLabel.widthAnchor.constraint(equalToConstant: 60).isActive = true
        typeLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        msgType.topAnchor.constraint(equalTo: typeLabel.topAnchor).isActive = true
        msgType.leftAnchor.constraint(equalTo: typeLabel.rightAnchor, constant : 5).isActive = true
        msgType.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -5).isActive = true
        msgType.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        sendDate.bottomAnchor.constraint(equalTo: msgType.topAnchor, constant: 0).isActive = true
        sendDate.leadingAnchor.constraint(equalTo: msgType.leadingAnchor).isActive = true
        sendDate.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        sendTime.leadingAnchor.constraint(equalTo: sendDate.trailingAnchor, constant: 8).isActive = true
        sendTime.bottomAnchor.constraint(equalTo: sendDate.bottomAnchor).isActive = true
        sendTime.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        msgText.topAnchor.constraint(equalTo: msgType.bottomAnchor, constant: 0).isActive = true
        msgText.leadingAnchor.constraint(equalTo: msgType.leadingAnchor).isActive = true
        msgText.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -5).isActive = true
        msgText.heightAnchor.constraint(equalToConstant: 20).isActive = true

//        more.topAnchor.constraint(equalTo: msgText.bottomAnchor).isActive = true
//        more.leftAnchor.constraint(equalTo: sendDate.leftAnchor).isActive = true
//        more.widthAnchor.constraint(equalToConstant: 100).isActive = true
//        more.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
    }

}
