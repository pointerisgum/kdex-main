//
//  sideCell.swift
//  KDex
//
//  Created by park heewon on 2018. 10. 16..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import UIKit

class sideCell: UITableViewCell {

    @IBOutlet var textLb: UILabel!
    
    @IBOutlet var img: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        img.contentMode = .scaleAspectFit
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
