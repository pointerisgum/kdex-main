//
//  ListCell.swift
//  crevolc
//
//  Created by crovolc on 2018. 1. 19..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//
import UIKit
import Foundation

class ListConCluCell: UITableViewCell {
    
    var delegate : ListConcluController?
    
    var indexRow = 10
    
    @IBOutlet var backVi: UIView!
    
    @IBOutlet var simboleImg: UIImageView!
    
    @IBOutlet var deleteBtn: UIButton!
    
    @IBOutlet var coinLabel: UILabel!
    
    @IBOutlet var sellBuyLabel: UILabel!
    
    @IBOutlet var dateLabel: UILabel!
    
    @IBOutlet var orderCountLabel: UILabel!
    
    @IBOutlet var orderPriceLabe: UILabel!
    
    @IBOutlet var confirmCountLabel: UILabel!
    
    @IBOutlet var concluCountLabel: UILabel!
    
    @IBOutlet weak var isSelector: UIImageView!
    
    @IBOutlet var concluLabel: UILabel!
    
    @IBOutlet var orderLabel: UILabel!
    
    @IBOutlet var orderPriceLabel: UILabel!
    
    @IBOutlet var confirmLabel: UILabel!
    
    
    @IBAction func deleteBtnAction(_ sender: Any) {
        print("delete btn cell : \(indexRow)")
        delegate?.httppostAction(row: (delegate?.jsonArray![indexRow])!)
    }
    
    var selectBtn = false
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
}
