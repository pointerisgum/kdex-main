//
//  profitCell.swift
//  KDex
//
//  Created by park heewon on 2018. 7. 9..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import UIKit

class profitCell: UITableViewCell {
    
    
    @IBOutlet var contentVi: UIView!
    
    @IBOutlet var backVi: UIView!
    
    @IBOutlet var coinSimbol: UILabel!
    
    @IBOutlet var evalBuyPrice: UILabel!
    
    @IBOutlet var evalSellPrice: UILabel!
    
    @IBOutlet var profite: UILabel!
    
    @IBOutlet var rate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundColor = UIColor(rgb: 0xf3f4f5)
        
        backVi.layer.masksToBounds = true
        backVi.layer.cornerRadius = 5
        
        contentVi.frame = self.bounds
        contentVi.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)


    }
    
}
