//
//  ShowProfitCell.swift
//  KDex
//
//  Created by park heewon on 2018. 7. 10..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation


class ShowProfitCell: UICollectionViewCell {
    
    @IBOutlet var date : UILabel!   // 일자
    
    @IBOutlet var instCd : UILabel!   // 코인
    
    @IBOutlet var tradePl : UILabel!  // 거래손익(KRW)
    
    @IBOutlet var tradeFee : UILabel!  // 수수료(KRW)
    
    @IBOutlet var profit : UILabel!   // 순손익(KRW)
    
    @IBOutlet var balance : UILabel!  // 보유자산(KRW)
    
    @IBOutlet var rate : UILabel!   // 수익율(%)
    
    @IBOutlet var buyQty : UILabel!   // 매수수량
    
    @IBOutlet var sellQty : UILabel!  // 매도수량
    
    @IBOutlet var buyAvgPrc : UILabel! // 평균매수가
    
    @IBOutlet var sellAvgPrc : UILabel!  // 평균매도가
    
}
