//
//  WalletPayListCell.swift
//  KDex
//
//  Created by park heewon on 2018. 4. 10..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation
import UIKit

class WalletPayListCell: UITableViewCell {
    
    @IBOutlet var dateLabel: UILabel!
    
    @IBOutlet var contentLabel: UILabel!
    
    @IBOutlet var payPriceLabel: UILabel!
    
    @IBOutlet var txStatusLabel: UILabel!
    
}
