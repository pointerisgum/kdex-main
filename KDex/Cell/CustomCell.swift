//
//  customCell.swift
//  KDex
//
//  Created by park heewon on 2018. 5. 2..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation

class CustomCell: UITableViewCell {
    
    @IBOutlet var mainLabel: UILabel!
    
    @IBOutlet var subLabel: UILabel!
    
}
