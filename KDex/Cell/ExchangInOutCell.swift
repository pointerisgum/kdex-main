//
//  ExchangInOutCell.swift
//  crevolc
//
//  Created by crovolc on 2018. 1. 2..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import UIKit
import Foundation

class ExchangInOutCell : UITableViewCell {
    
    @IBOutlet var priceLabel: UILabel!
    
    @IBOutlet var quarityLabel: UILabel!
    
    @IBOutlet var grafBar: UIView!
    
    @IBOutlet var krwLabel: UILabel!
    
}
