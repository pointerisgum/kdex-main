
//
//  ExchangOutDetail.swift
//  crevolc
//
//  Created by crovolc on 2018. 1. 2..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//  사용하지 않는 컨트롤러

import Alamofire
import UIKit
import Foundation

class ExchangOutDetail : UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, RealTimeDelegate{
    
    var coinSimbol : String? //코인 아이디
    
    var lastprice : String? // 시가
    
     var marksimbol : String?
    
    var userId : String!
    
    var sessionId : String!
    
    var inputvalue = ExchangInput()
    
    var selljsonvalueArray : NSArray?
    
    var buyjsonvalueArray : NSArray?
    
    var textvaluedictionry : [String : Any]?
    
    var bid : Array<Any>?
    
    var ask : Array<Any>?
    
    var tickSice : String?
    
    @IBOutlet var priceTf: UITextField!
    
    @IBOutlet var quantityTf: UITextField!
    
    @IBOutlet var orderAbleAmountLabel: UILabel!
    
    @IBOutlet var feeRateLabel: UILabel!
    
    @IBOutlet var minPriceLabel: UILabel!
    
    @IBOutlet var orderAmountLabel: UILabel!

    @IBOutlet var exOutTable: UITableView!
    
    @IBOutlet var orderAmountKRWLabel: UILabel!
    
    @IBOutlet var oerderAmountKRWLabel: UILabel!
    
    @IBAction func segmentSelecterBtn(_ sender: UISegmentedControl) {
        
        if self.quantityTf.text == "" {
            self.quantityTf.text = "0"
        }
        
        guard let minprc = self.stringtoDouble(str: self.minPriceLabel.text!) else {
            return
        }
        guard let lastprc = self.stringtoDouble(str: self.lastprice!) else{
            return
        } 
        
        guard let orderableprc = self.stringtoDouble(str: self.orderAbleAmountLabel.text!) else {
            return
        }
        
        let index = sender.selectedSegmentIndex
        
        if lastprc == 0 {
            return
        }
        
        switch index {
        case 0:
            if orderableprc > minprc {
                let askQty = minprc / lastprc
                self.quantityTf.text = "\(askQty)"
            }
            self.quantityTf.text = "0.0"
        case 1:
            let askQty = (orderableprc * 0.5) / lastprc
            self.quantityTf.text = "\(askQty)"
        case 2:
            let askQty = orderableprc / lastprc
            self.quantityTf.text = "\(askQty)"
        default:
            print("error")
        }
        
        self.textfieldChageEvent()
    }
    
    @IBAction func pricePlusAction(_ sender: UIButton) {

        if self.priceTf.text == "" {
            self.priceTf.text = "0"
        }
        
        guard let priceSize = stringtoDouble(str: self.priceTf.text!) else {
            return
        }
        
        guard let size = stringtoDouble(str: self.tickSice!) else {
            return
        }
        
        if self.marksimbol! == "BTC"{
            self.priceTf.text = String(priceSize + size)
        }else if self.marksimbol == "ETH"{
            self.priceTf.text = String(priceSize + size)
        }else {
            self.priceTf.text = String(Int(priceSize + size))
        }
        
        self.textfieldChageEvent()
        
    }
    
    @IBAction func priceMinusAction(_ sender: Any) {
        
        if self.priceTf.text == "" {
            self.priceTf.text = "0"
        }
        
        guard let priceSize = stringtoDouble(str: self.priceTf.text!) else {
            return
        }
        
        guard let size = stringtoDouble(str: self.tickSice!) else {
            return
        }
        
        if priceSize <= 0 || priceSize < size{
            self.priceTf.text = "0"
        }else{
            if self.marksimbol! == "BTC"{
                self.priceTf.text = String(priceSize - size)
            }else if self.marksimbol == "ETH"{
                self.priceTf.text = String(priceSize - size)
            }else {
                self.priceTf.text = String(Int(priceSize - size))
            }
        }
        
        self.textfieldChageEvent()
        
    }
    
    @IBAction func coinInAction(_ sender: Any) {
        
        if priceTf.text == ""{
            let msg = "코인의 가격을 입력해주세요"
            let alert = UIAlertController(title: "매도", message: msg, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "확인", style: .cancel, handler: nil))
            self.present(alert, animated: false, completion: nil)
        }else if quantityTf.text == "" {
            let msg = "코인의 수량을 입력해주세요"
            let alert = UIAlertController(title: "매도", message: msg, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "확인", style: .cancel, handler: nil))
            self.present(alert, animated: false, completion: nil)
        }else{
            let msg = "해당 사항을 판매 하시겠습니까?"
            let alert = UIAlertController(title: "매도", message: msg, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "취소", style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "확인", style: .destructive, handler: { (_) in
                self.sellAction()
            }))
            self.present(alert, animated: false, completion: nil)
        }
    }
    
    let tfView : UIView = {
        let vi = UIView()
        vi.backgroundColor = UIColor.clear
        vi.frame = CGRect(x: 0, y: 0, width: 4, height: 35)
        return vi
    }()
    
    let quatfView : UIView = {
        let vi = UIView()
        vi.backgroundColor = UIColor.clear
        vi.frame = CGRect(x: 0, y: 0, width: 4, height: 35)
        return vi
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let plist = UserDefaults.standard
        self.marksimbol = plist.string(forKey: "marketSimbol")
        self.userId = plist.string(forKey: "uid")
        self.sessionId = plist.string(forKey: "sessionId")
        
        self.priceTf.leftView = tfView
        self.priceTf.leftViewMode = .always
        self.priceTf.attributedPlaceholder = NSAttributedString(string: "가격", attributes: [NSAttributedString.Key.foregroundColor : UIColor.DPmainTextColor])
        self.priceTf.addBorderBottom(height: 1.0, width: self.priceTf.frame.width,  color: UIColor.DPLineBgColor)
        
        self.quantityTf.leftView = quatfView
        self.quantityTf.leftViewMode = .always
        self.quantityTf.attributedPlaceholder = NSAttributedString(string: "수량", attributes: [NSAttributedString.Key.foregroundColor : UIColor.DPmainTextColor])
        self.quantityTf.addBorderBottom(height: 1.0, width: self.quantityTf.frame.width, color: UIColor.DPLineBgColor)
        
        exOutTable.delegate = self
        exOutTable.dataSource = self
        
        chatMoving()
        userAmount()
        
        exOutTable.backgroundColor = UIColor.clear
        self.view.backgroundColor = UIColor.DPViewBackgroundColor
        
        self.priceTf.addTarget(self, action: #selector(textfieldChageEvent), for: .editingChanged)
        self.quantityTf.addTarget(self, action: #selector(textfieldChageEvent), for: .editingChanged)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if self.marksimbol! == "BTC" {
            self.orderAmountLabel.text = "0 BTC"
            self.oerderAmountKRWLabel.text = "0 KRW"
        }else if self.marksimbol! == "ETH" {
            self.orderAmountLabel.text = "0 ETH"
            self.oerderAmountKRWLabel.text = "0 KRW"
        }else {
            self.orderAmountLabel.text = "0 KRW"
            self.oerderAmountKRWLabel.isHidden = true
        }
        
        TcpSocket.sharedInstance.receiverThread?.delegate = self
        chatMoving()
    }
    
    override func viewWillDisappear(_ animated: Bool) {

        self.priceTf.text = ""
        self.quantityTf.text = ""
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ex_InOutCell") as? ExchangInOutCell
        cell?.backgroundColor = UIColor.DPtableCellBgColor
        cell?.quarityLabel.textColor = UIColor.DPmainTextColor
        
        if self.buyjsonvalueArray != nil {
            
            if indexPath.row > 9 {
                
                // 매수
                cell?.backgroundColor = UIColor.DPaskBgColor.withAlphaComponent(0.05)
                
                cell?.grafBar.backgroundColor = UIColor.DPaskBgColor.withAlphaComponent(0.1)
                
                cell?.priceLabel.textColor = UIColor.DPBuyDefaultColor
                
                for temp in self.buyjsonvalueArray! {
                    
                    var bidjson = temp as? [String : String]
                    
                    if bidjson!["dealType"] == "bid" && bidjson!["order"] == "\(indexPath.row - 9)" {
                        
                        let doubleprice = stringtoDouble(str: (bidjson?["price"])!)
                        
                        if bidjson?["price"] == "0.0"{
                            cell?.priceLabel.text = "0"
                        }else{
                            
                            if self.marksimbol! == "BTC" || self.marksimbol! == "ETH" {
                                let price = stringtoDouble(str: (bidjson?["price"])!)
                                cell?.priceLabel.text = String(format: "%.8f", price!)
                            }else {
                                cell?.priceLabel.text = bidjson?["price"]?.insertComma
                            }
                            
                        }
                        
                        if bidjson?["qty"] == "0.0" {
                            cell?.quarityLabel.text = "0"
                        }else {
                            cell?.quarityLabel.text = bidjson?["qty"]
                            
                        }
                        
                        if self.marksimbol! == "BTC" {
                            cell?.krwLabel.text = "\(Int(doubleprice! * AppDelegate.BTC)) KRW"
                        }else if self.marksimbol! == "ETH" {
                            cell?.krwLabel.text = "\(Int(doubleprice! * AppDelegate.ETH)) KRW"
                        }else {
                            cell?.krwLabel.text = ""
                        }
                        
                        let per = CGFloat(((bidjson?["percent"] as? NSString)?.floatValue)!)
                        
                        if per.isEqual(to: 0){
                            cell?.grafBar.frame.origin.x = (cell?.frame.width)!
                        } else if per.isEqual(to: 100) {
                            cell?.grafBar.frame.origin.x = 0
                        } else {
                            let graper : CGFloat = (((cell?.frame.width)! / 100) * per)
                            cell?.grafBar.frame.origin.x = (cell?.frame.width)! - graper
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
        if self.selljsonvalueArray != nil {
            if indexPath.row < 10 {
                //매도
                var num = 0
                cell?.backgroundColor = UIColor.DPbdidBgColor.withAlphaComponent(0.05)
                
                cell?.grafBar.backgroundColor = UIColor.DPbdidBgColor.withAlphaComponent(0.1)
                
                cell?.priceLabel.textColor = UIColor.DPSellDefaultColor
                
                if indexPath.row == 0 {
                    num = 10 + indexPath.row
                }else{
                    num = 10 - indexPath.row
                }
                
                for temp in self.selljsonvalueArray! {
                    
                    var askjson = temp as? [String : String]
                    
                    if askjson!["dealType"] == "ask" && askjson!["order"] == "\(num)" {
                        
                        let doubleprice = stringtoDouble(str: (askjson?["price"])!)
                        
                        if askjson?["price"] == "0.0"{
                            cell?.priceLabel.text = "0"
                        }else{
                            if self.marksimbol! == "BTC" || self.marksimbol! == "ETH" {
                                let price = stringtoDouble(str: (askjson?["price"])!)
                                cell?.priceLabel.text = String(format: "%.8f", price!)
                            }else {
                                cell?.priceLabel.text = askjson?["price"]?.insertComma
                            }
                        }
                        
                        if askjson?["qty"] == "0.0" {
                            cell?.quarityLabel.text = "0"
                        }else {
                            cell?.quarityLabel.text = askjson?["qty"]
                        }
                        
                        if self.marksimbol! == "BTC" {
                            cell?.krwLabel.text = "\(Int(doubleprice! * AppDelegate.BTC)) KRW"
                        }else if self.marksimbol! == "ETH" {
                            cell?.krwLabel.text = "\(Int(doubleprice! * AppDelegate.ETH)) KRW"
                        }else {
                            cell?.krwLabel.text = ""
                        }
                        
                        let per = CGFloat(((askjson?["percent"] as? NSString)?.floatValue)!)
                        
                        if per.isEqual(to: 0){
                            cell?.grafBar.frame.origin.x = (cell?.frame.width)!
                        } else if per.isEqual(to: 100) {
                            cell?.grafBar.frame.origin.x = 0
                        } else {
                            let graper : CGFloat = (((cell?.frame.width)! / 100) * per)
                            cell?.grafBar.frame.origin.x = (cell?.frame.width)! - graper
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
        cell?.selectionStyle = .none
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 41
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        
        if indexPath.row < 10 {
            //매도
            var num = 0
            
            if indexPath.row == 0 {
                num = 10 + indexPath.row
            }else{
                num = 10 - indexPath.row
            }
            
            for temp in self.selljsonvalueArray! {
                
                var askjson = temp as? [String : String]
                
                if askjson!["dealType"] == "ask" && askjson!["order"] == "\(num)" {
                    
                    let doublePrice = stringtoDouble(str: (askjson?["price"])!)
                    let doubleQty = stringtoDouble(str: (askjson?["qty"])!)
                    
                    self.priceTf.text = askjson?["price"]
                    self.quantityTf.text = askjson?["qty"]
                    
                    let amount = doublePrice! * doubleQty!
                    
                    let intnum = Int(amount)
                    
                    if self.marksimbol! == "BTC" {
                        let krwAmount = amount * AppDelegate.BTC
                        let str = "\(Int(krwAmount))".insertComma
                        self.orderAmountLabel.text = "\(String(format: "%.8f", amount)) BTC"
                        self.oerderAmountKRWLabel.text = "\(str) KRW"
                    }else if self.marksimbol! == "ETH" {
                        let krwAmount = amount * AppDelegate.ETH
                        let str = "\(Int(krwAmount))".insertComma
                        self.orderAmountLabel.text = "\(String(format: "%.8f", amount)) ETH"
                        self.oerderAmountKRWLabel.text = "\(str) KRW"
                    }else {
                        let str = "\(intnum)".insertComma
                        self.orderAmountLabel.text = "\(str) KRW"
                        self.oerderAmountKRWLabel.isHidden = true
                    }
                }
            }
        }else{
            
            for temp in self.buyjsonvalueArray! {
                
                var bidjson = temp as? [String : String]
                
                if bidjson!["dealType"] == "bid" && bidjson!["order"] == "\(indexPath.row - 9)" {
                    
                    let doublePrice = stringtoDouble(str: (bidjson?["price"])!)
                    let doubleQty = stringtoDouble(str: (bidjson?["qty"])!)
                    
                    self.priceTf.text = bidjson?["price"]
                    self.quantityTf.text = bidjson?["qty"]
                    
                    let amount = doublePrice! * doubleQty!
                    
                    let intnum = Int(amount)
                    
                    if self.marksimbol! == "BTC" {
                        let krwAmount = amount * AppDelegate.BTC
                        let str = "\(Int(krwAmount))".insertComma
                        self.orderAmountLabel.text = "\(String(format: "%.8f", amount)) BTC"
                        self.oerderAmountKRWLabel.text = "\(str) KRW"
                    }else if self.marksimbol! == "ETH" {
                        let krwAmount = amount * AppDelegate.ETH
                        let str = "\(Int(krwAmount))".insertComma
                        self.orderAmountLabel.text = "\(String(format: "%.8f", amount)) ETH"
                        self.oerderAmountKRWLabel.text = "\(str) KRW"
                    }else {
                        let str = "\(intnum)".insertComma
                        self.orderAmountLabel.text = "\(str) KRW"
                        self.oerderAmountKRWLabel.isHidden = true
                    }
                    
                }
            }
        }
    }
    
    func chatMoving(){
        
        var urlComponents = URLComponents(string: "\(AppDelegate.url)/app/orderList?simbol=\(self.coinSimbol!)")!
        //        urlComponents.queryItems = [
        //            URLQueryItem(name: "data", value: jsonString),
        //        ]
        let amo = Alamofire.request(urlComponents)
        amo.responseJSON(completionHandler: { response in
            //            print("\(response.result.value)")
            if let tempJsonObj = response.result.value as? [String : Any]{
                
                self.lastprice = (tempJsonObj["lastPrice"] as? String) ?? "0"
                self.tickSice = (tempJsonObj["tickSize"] as? String) ?? "0"
                
                //self.minPriceLabel.text = (tempJsonObj["minPrice"] as? String) ?? "0"
                
                let tempstr = (tempJsonObj["minPrice"] as? String) ?? "0"
                
                if self.marksimbol! == "BTC" {
                    self.minPriceLabel.text = "\(tempstr) BTC"
                } else if self.marksimbol! == "ETH" {
                    self.minPriceLabel.text = "\(tempstr) ETH"
                } else {
                    self.minPriceLabel.text = "\(tempstr) KRW"
                }
                
                if let datatemp = tempJsonObj["bidInfoList"] as? NSArray{
                    self.buyjsonvalueArray = datatemp
                }
                
                if let datatemp = tempJsonObj["askInfoList"] as? NSArray{
                    self.selljsonvalueArray = datatemp
                }
                
            }
            self.exOutTable.reloadData()
            
        })
        
    }
    
    func sellAction(){
        
//        let activity = UIActivityIndicatorView()
//        activity.activityIndicatorViewStyle = .whiteLarge
//        activity.color = UIColor(rgb: 0x00A6ED)
//        activity.hidesWhenStopped = true
//        activity.startAnimating()
        
        let quanity = self.quantityTf.text?.components(separatedBy: [","]).joined()
        let price = self.priceTf.text?.components(separatedBy: [","]).joined()
        
//        let quanity = self.quantityTf.text
//        let price = self.priceTf.text
        
        let url = URL(string: "\(AppDelegate.url)/auth/orderAdd")
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let param = "uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&simbol=\(coinSimbol!)&priceType=2&orderType=S&waysType=M&orderQty=\(quanity!)&orderPrice=\(price!)&privKey1&privKey2&saveKeyType"
        
        let paramData = param.data(using: .utf8)
        
        request.httpBody = paramData
        
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        request.setValue(String(describing: paramData!.count), forHTTPHeaderField: "Content-Length")
        
        //laucnhing
        let session = URLSession.shared
        let task = session.dataTask(with:request as URLRequest) {data, response, error in
            
           // activity.stopAnimating()
            
            if error == nil {
                
                print("buy error sueccess")
                DispatchQueue.main.async {
                    do {
                        let object = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        
                        guard let jsonObject = object else { return }
                        
                        print("buy : \(object)")
                        
                        let msg = "판매 등록이 완료되었습니다."
                        let alert = UIAlertController(title: "매도", message: msg, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "확인", style: .cancel, handler: { (_) in
                           // self.chatMoving()
                        }))
                        self.present(alert, animated: false, completion: nil)
                        
                    }catch{
                        print("cCaught an error:\(error)")
                        let msg = "판매 등록이 실패하였습니다. 다시 등록 부탁드립니다.."
                        let alert = UIAlertController(title: "매도", message: msg, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "확인", style: .cancel, handler: nil))
                        self.present(alert, animated: false, completion: nil)
                    }
                }
            }else{
                print("error:\(error)")
            }
            
        }
        task.resume()
        
    }
    
    func userAmount() {
        let activity = UIActivityIndicatorView()
        activity.style = .whiteLarge
        activity.color = UIColor.DPActivityColor
        activity.hidesWhenStopped = true
        activity.startAnimating()
        
        let url = "\(AppDelegate.url)/auth/userAbleAmount?simbol=\(coinSimbol!)&uid=\(self.userId!)&sessionId=\(self.sessionId!)"
        
        print(url)
        let amo = Alamofire.request(url)
        amo.responseJSON(completionHandler: { response in
      
            if response.result.isSuccess == true {
                
                var buyjson = response.result.value as? [String : Any]
                
                let tempstr = buyjson?["openQty"] as? String ?? ""
                
                if self.marksimbol == "BTC" {
                    self.orderAbleAmountLabel.text = "\(tempstr) BTC"
                }else if self.marksimbol! == "ETH"{
                    self.orderAbleAmountLabel.text = "\(tempstr) ETH"
                }else {
                    self.orderAbleAmountLabel.text = "\(tempstr) KRW"
                }
                
                self.feeRateLabel.text = (buyjson?["feeRate"] as? String) ?? ""
            }
            activity.stopAnimating()
            
        })
        
    }
    
    func stringChage(value1: String?, value2: String, origin: String) -> String {
        
        var str : String = origin
        
        if value1 == nil {
            str = "\(str) \(value2)"
            
        }else {
            str = "\(value1!) \(str) \(value2)"
        }
        
        return str
    }
    
    func stringtoDouble(str : String) -> Double?{
        let temp = str.components(separatedBy: [","]).joined()
        let double = Double(temp)
        return double ?? 0.0
    }
    
    @objc func textfieldChageEvent() {
        
        let num1 = self.stringtoDouble(str: self.quantityTf.text!)!
        let num2 = self.stringtoDouble(str: self.priceTf.text!)!
        
        if self.quantityTf.text == "." {
            self.quantityTf.text = ""
        }else if self.priceTf.text == "." {
            self.priceTf.text = ""
        }
        
        let amount = num1 * num2
        
        let intnum = Int(amount)
        
        if self.marksimbol! == "BTC" {
            let krwAmount = amount * AppDelegate.BTC
            let str = "\(Int(krwAmount))".insertComma
            self.orderAmountLabel.text = "\(String(format: "%.8f", amount)) BTC"
            self.oerderAmountKRWLabel.text = "\(str) KRW"
        }else if self.marksimbol! == "ETH" {
            let krwAmount = amount * AppDelegate.ETH
            let str = "\(Int(krwAmount))".insertComma
            self.orderAmountLabel.text = "\(String(format: "%.8f", amount)) ETH"
            self.oerderAmountKRWLabel.text = "\(str) KRW"
        }else {
            let str = "\(intnum)".insertComma
            self.orderAmountLabel.text = "\(str) KRW"
            self.oerderAmountKRWLabel.isHidden = true
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        priceTf.resignFirstResponder()
        quantityTf.resignFirstResponder()
    }
    
    func exchangListRT(bt: NPriceTick) {
        
    }
    
    func exchangDetailRT(bt: NPriceTick) {
        
    }
    
    //호가 실시간 리스트
    func trandeListRT(bt: NPriceInfo) {
        print("RT : OuttrandeListRT")
        let symbol = bt.symbol.toString().components(separatedBy: [" "]).joined()
        
        if coinSimbol == symbol {
        
            let tempArray = bt.toJSON()
            self.buyjsonvalueArray = tempArray.0
            self.selljsonvalueArray = tempArray.1
            
            DispatchQueue.main.async {
                print("RT ReloadDate")
                self.exOutTable.reloadData()
            }
        }
    }
    
    func openListRT(bt: NOpenInfo) {
        
    }
    
    func krwChangeRT(value: Double) {
        DispatchQueue.main.async {
            self.exOutTable.reloadData()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var allowCharatorSet = CharacterSet.decimalDigits
        let charaterset = CharacterSet(charactersIn: string)
        allowCharatorSet.insert(charactersIn: ".")
        return allowCharatorSet.isSuperset(of: charaterset)
    }
    
    
}
