//
//  TokenOpenAble.swift
//  KDex
//
//  Created by park heewon on 2018. 7. 4..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation

protocol TokenOpenAble {
    
    func openToken()
    func selectTokenSwAction(sender: UISwitch)
    func sendWalletViAction(value : [String : Any])
    
}
