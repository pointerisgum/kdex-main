//
//  KdexEnumSet.swift
//  KDex
//
//  Created by park heewon on 2018. 4. 30..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation

enum CoinWalletNatwork : String {
    
    case BTC = "https://blockchain.info/address/"
    case ETH = "https://etherscan.io/address/"
    case BCH = "https://bch.btc.com/"
    case LTC = "https://chainz.cryptoid.info/ltc/address.dws?"
    case QTUM = " https://explorer.qtum.org/address/"
    case ETC = "http://gastracker.io/addr/"
    case TRX = "https://tronscan.org/#/address/"
    case XRP = "https://bithomp.com/explorer/"
    
}

enum CoinTxNatwork : String {
    
    case BTC = "https://blockchain.info/tx/"
    case ETH = "https://etherscan.io/tx/"
    case BCH = "https://bch.btc.com/"
    case LTC = "https://chainz.cryptoid.info/ltc/tx.dws?"
    case QTUM = "https://explorer.qtum.org/tx/"
    case ETC = "http://gastracker.io/tx/"
    case TRX = "https://tronscan.org/#/transaction/"
    case XRP = "https://bithomp.com/explorer/"
    
}
