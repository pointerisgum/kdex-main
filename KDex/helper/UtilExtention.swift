//
//  UtilExtention.swift
//  crevolc
//
//  Created by crovolc on 2018. 1. 24..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation

// String extension
extension UIApplication {
    
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
    
}

extension String {
    
    var insertComma: String {
        let numberFormatter = NumberFormatter();
        numberFormatter.numberStyle = .decimal
        // 소수점이 있는 경우 처리
        if let _ = self.range(of: ".") {
            var numberArray = self.components(separatedBy: ".")
            if numberArray.count == 1 {
                var numberString = numberArray[0]
                if numberString.isEmpty { numberString = "0" }
                guard let doubleValue = Double(numberString) else { return self }
                return numberFormatter.string(from: NSNumber(value: doubleValue)) ?? self }
            else if numberArray.count == 2 { var numberString = numberArray[0]
                if numberString.isEmpty { numberString = "0" }
                guard let doubleValue = Double(numberString) else { return self }
                return (numberFormatter.string(from: NSNumber(value: doubleValue)) ?? numberString) + ".\(numberArray[1])" } }
        else {
            guard let doubleValue = Double(self) else { return self }
            return numberFormatter.string(from: NSNumber(value: doubleValue)) ?? self }
        return self
    }
    
    func streamStr(num: Int, str: String) -> String {
        
        let temp = num - str.count
        var tempstr = str
        print(str.count)
        print(temp)
        
        if temp == 0 {
            return str
        }else if temp > 0{
            for row in 1...temp {
                let i = str.index(str.startIndex, offsetBy: 0)
                tempstr.insert(" ", at: i)
            }
            return tempstr
            
        }else {
            guard let sttr = str[..<str.index(str.startIndex, offsetBy: num)] as? String else{
                return ""
            }
            return sttr
        }
        
        return ""
    }
    
    func subStr(num: Int, str: String) -> String {
        
        let count = str.count
        let endnum = count - num
        
        if endnum <= 0 {
            return str
        }else{
            let startindex = str.startIndex
            let endindex = str.index(str.endIndex, offsetBy: endnum * -1 )
            let sttr = str[startindex..<endindex]
            
            return String(sttr)
            
        }
        
        return str
    }
    
    public func calcHeight(forWidth: CGFloat, font: UIFont?) -> CGFloat {
        guard let font = font else { return 0 }
        let rect = CGSize(width: forWidth, height: .greatestFiniteMagnitude)
        let boundingRect = self.boundingRect(with: rect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        return ceil(boundingRect.height)
    }
    
    public func calcWidth(forHeight: CGFloat, font: UIFont) -> CGFloat {
        let rect = CGSize(width: .greatestFiniteMagnitude, height: forHeight)
        let boundingRect = self.boundingRect(with: rect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        return ceil(boundingRect.width)
    }
    
    public func stringtoDouble(str : String) -> Double?{
        let temp = str.components(separatedBy: [","]).joined()
        let double = Double(temp)
        return double ?? 0.0
    }
    
    
    func containsIgnoringCase(find: String) -> Bool{
        return self.range(of: find, options: .caseInsensitive) != nil
    }
    
    
    mutating func replace(originalString:String, withString newString:String)
    {
        let replacedString = self.replacingOccurrences(of: originalString, with: newString,
                                                       options: NSString.CompareOptions.literal, range:nil)
        self = replacedString
    }
    
}

// textView extension
extension UITextView {
    
    func centerVertically() {
        let fittingSize = CGSize(width: bounds.width, height: CGFloat.greatestFiniteMagnitude)
        let size = sizeThatFits(fittingSize)
        let topOffset = (bounds.size.height - size.height * zoomScale) / 2
        let positiveTopOffset = max(1, topOffset)
        contentOffset.y = -positiveTopOffset
    }
    
}

// Application application
extension UIApplication{
    
    var topViewController: UIViewController?{
        if keyWindow?.rootViewController == nil{
            return keyWindow?.rootViewController
        }
        
        var pointedViewController = keyWindow?.rootViewController
        
        while  pointedViewController?.presentedViewController != nil {
            switch pointedViewController?.presentedViewController {
            case let navagationController as UINavigationController:
                pointedViewController = navagationController.viewControllers.last
            case let tabBarController as UITabBarController:
                pointedViewController = tabBarController.selectedViewController
            default:
                pointedViewController = pointedViewController?.presentedViewController
            }
        }
        return pointedViewController
        
    }
}

// color extentsion
extension UIColor {
    
    convenience init(rgb: UInt) {
        self.init(
            red: CGFloat((rgb & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgb & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgb & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    static var DPdefaultBackgroundColor = UIColor(rgb: 0x00b9b2) //기본배경 색
    
    static var DPStatusBgColor = UIColor(rgb: 0x00b9b2) //상태바 배경색
    static var DPNaviBarTintColor = UIColor(rgb: 0x1b2f42) //네비바 배경색
    static var DPDarkBlueBgColor = UIColor(rgb: 0x12335a) //다크블루 배경색
    static var DPKrwTextColor = UIColor(rgb: 0xf2a40e) //총 보유자산 krw 텍스트 색
    static var DPmainTextColor = UIColor(rgb: 0x444444) //주 텍스트색
    static var DPLineBgColor = UIColor(rgb: 0xCACACD) //라인 배경색 -
    
    static var DPViewGrayBackgroundColor = UIColor(rgb: 0xf5f5f7) //회색배경
    static var DPViewBackgroundColor = UIColor(rgb: 0xffffff) //흰배경
    static var DPTabBarIndicateTintColor = UIColor(rgb: 0x00b9b2) //탭아이템 선택시 -
    static var DPCompanyColor = UIColor(rgb: 0x00B9B2) //대표 텍스트색
    static var DPSubCompanyColor = UIColor(rgb: 0xe6fefe) //대표 서브 테스트색
    static var DPtableLineColor = UIColor(rgb: 0xcacacd) //테이블 라인색
    static var DPgrayBackColor = UIColor(rgb: 0xf5f5f5) //테이블 라인색
    static var DPActivityColor = UIColor(rgb: 0x00B9B2) //액티비티배경색
    static var DPAlertContentBgColor = UIColor(rgb: 0xf2f2f2) //알림 내용 배경
    
    static var DPMinTextColor = UIColor(rgb: 0x2c5ac1) //하향 파란 기본색 -
    static var DPPlusTextColor = UIColor(rgb: 0xe4292b) //상향 매수기본색 -
    static var DPNormalTextColor = UIColor(rgb: 0xe9a01b) //기본 기본색 -
    
//    color_array['KRW'] = "#000000";
//    color_array['KDP'] = "#00b9b2";
//    color_array['BTC'] = "#FF9B00";
//    color_array['ETH'] = "#719E71";
//    color_array['BCH'] = "#FFB914";
//    color_array['LTC'] = "#46B4B4";
//    color_array['ETC'] = "#006400";
//
    
    static var DPLoginBgColor = UIColor(rgb: 0xe8eefa) //테이블헤더배경색 -
    
    static var DPNaviBartextTintColor = UIColor(rgb: 0xffffff) //네비바 텍스트색 -
    static var DPTabBarTintColor = UIColor(rgb: 0x323240) //탭바 배경색 -
    
//    static var DPLineBgColor = UIColor(rgb: 0xeeedf5) //라인 배경색 -
//    static var DPLineBgColor = UIColor(rgb: 0xe2e2e4) //라인 배경색 -
    
    
    
    
    static var DPPopupBgColor = UIColor(rgb: 0xf7f7f7) //팝업 배경색 -
    
    static var DPtableViewBgColor = UIColor(rgb: 0xffffff) //테이블배경색 -
    static var DPtableHeaderBgColor = UIColor(rgb: 0xe8eefa) //테이블헤더배경색 -
    static var DPtableFooterBgColor = UIColor(rgb: 0xffffff) //테이블푸터배경색 -
    static var DPtableCellBgColor = UIColor(rgb: 0xffffff) //테이블푸터배경색 -
    static var DPTextFieldBgColor = UIColor(rgb: 0xffffff) //테스트필드배경색 -
    
    
    
    static var DPGreenColor = UIColor(rgb: 0x5cb85c) //녹색기본색 -
    
    
    
    static var DPTitleTextColor = UIColor(rgb: 0xffffff) //타이틀 텍스트 색상 -

    static var DPsubTextColor = UIColor(rgb: 0x8b90a6) //서브 텍스트색 -
    
    
    static var DPDefaultBtnColor = UIColor(rgb: 0x14233E) //기본버튼배경색 -
    static var DPButtonSelectedColor = UIColor(rgb: 0x3373f2) //버튼 클릭시 색상 -
    static var DPWhiteBtnColor = UIColor(rgb: 0xffffff) //하얀버튼, 네비 lefticon-
    static var DPOrengeBtnColor = UIColor(rgb: 0xf17c43) //오렌지버튼,
    
    static var DPBuyButtonColor = UIColor(rgb: 0xe4292b) //매수버튼 배경색
    static var DPSellButtonColor = UIColor(rgb: 0x14233e) //매도버튼 배경색 -
    static var DPExchangeButtonColor = UIColor(rgb: 0x376af4) //exchange 버튼 배경색
    
    static var DPBuyDefaultColor = UIColor(rgb: 0xe4292b) //매수글자기본색 -
    static var DPSellDefaultColor = UIColor(rgb: 0x0ca3f5) //매도글자기본색 -
    
    static var DPaskBgColor = UIColor(rgb: 0xe4292b) //매수호가 배경 배경색 -
    static var DPbdidBgColor = UIColor(rgb: 0x0ca3f5) //매도호가 배경 배경색 -
    
}

// navigation extension
extension UINavigationBar {
    
    func hideBottomHairline() {
        let navigationBarImageView = hairlineImageViewInNavigationBar(self)
        navigationBarImageView!.isHidden = true
    }
    
    func showBottomHairline() {
        let navigationBarImageView = hairlineImageViewInNavigationBar(self)
        navigationBarImageView!.isHidden = false
    }
    
    fileprivate func hairlineImageViewInNavigationBar(_ view: UIView) -> UIImageView? {
        if let view  = view as? UIImageView, view.bounds.size.height <= 1.0 {
            return view
        }
        
        let subviews = (view.subviews as [UIView])
        for subview: UIView in subviews {
            if let imageView: UIImageView = hairlineImageViewInNavigationBar(subview) {
                return imageView
            }
        }
        
        return nil
    }
}

// testfield estension
extension UITextField {
    
    func addBorderBottom(height: CGFloat, width: CGFloat, color: UIColor) {
        let border = CALayer()
        border.frame = CGRect(x: 0, y: self.frame.height-height, width: width, height: height)
        border.backgroundColor = color.cgColor
        self.layer.addSublayer(border)
    }
    
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

// double extension
extension Double {
    mutating func roundToPlaces(places:Int) -> Double {
        if self.isNaN {
            self = 0
        }
        let divisor = pow(10.0, Double(places))
        return Darwin.round(self * divisor) / divisor }
}

enum ViewBorder: String {
    case left, right, top, bottom
}

// uiview extension
extension UIView {
    
    func add(border: ViewBorder, color: UIColor, width: CGFloat) {
        let borderLayer = CALayer()
        borderLayer.backgroundColor = color.cgColor
        borderLayer.name = border.rawValue
        switch border {
        case .left:
            borderLayer.frame = CGRect(x: 0, y: 0, width: width, height: self.frame.size.height)
        case .right:
            borderLayer.frame = CGRect(x: self.frame.size.width - width, y: 0, width: width, height: self.frame.size.height)
        case .top:
            borderLayer.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: width)
        case .bottom:
            borderLayer.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: width)
        }
        self.layer.addSublayer(borderLayer)
    }
    
    func allBorder(color: UIColor, width: CGFloat){
        self.add(border: .bottom, color: color, width: width)
        self.add(border: .left, color: color, width: width)
        self.add(border: .right, color: color, width: width)
        self.add(border: .top, color: color, width: width)
    }
    
    func remove(border: ViewBorder) {
        guard let sublayers = self.layer.sublayers else { return }
        var layerForRemove: CALayer?
        for layer in sublayers {
            if layer.name == border.rawValue {
                layerForRemove = layer
            }
        }
        if let layer = layerForRemove {
            layer.removeFromSuperlayer()
        }
    }
    
    
    
}

@IBDesignable class PaddingLabel: UILabel {
    
    @IBInspectable var topInset: CGFloat = 5.0
    @IBInspectable var bottomInset: CGFloat = 5.0
    @IBInspectable var leftInset: CGFloat = 10.0
    @IBInspectable var rightInset: CGFloat = 10.0
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }
    
    override var intrinsicContentSize: CGSize {
        var intrinsicSuperViewContentSize = super.intrinsicContentSize
        intrinsicSuperViewContentSize.height += topInset + bottomInset
        intrinsicSuperViewContentSize.width += leftInset + rightInset
        return intrinsicSuperViewContentSize
    }
    
}

extension UIViewController {
    
    func showToast(message : String, textcolor : UIColor, frame : CGRect, time: Double, animation: Bool) -> UILabel {
        
        let toastLabel = UILabel(frame: frame)
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = textcolor
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont.systemFont(ofSize: 12)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        
        if animation {
            UIView.animate(withDuration: 2.0, delay: time, options: .curveEaseOut, animations: {
                toastLabel.alpha = 0.0
            }, completion: {(isCompleted) in
                toastLabel.removeFromSuperview()
            })
        }
        
        return toastLabel
    }
    
}

extension Bundle {
    // 앱 이름
    class var appName: String {
        if let value = Bundle.main.infoDictionary?["CFBundleDisplayName"] as? String {
            return value
        }
        return ""
    }
    // 앱 버전
    class var appVersion: String {
        if let value = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            return value
        }
        return ""
    }
    // 앱 빌드 버전
    class var appBuildVersion: String {
        if let value = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            return value
        }
        return ""
    }
    // 앱 번들 ID
    class var bundleIdentifier: String {
        if let value = Bundle.main.infoDictionary?["CFBundleIdentifier"] as? String {
            return value
        }
        return ""
    }
}

extension String {
    var localized: String {
        
        return NSLocalizedString(self, tableName: "Localiable", value: "**\(self)**", comment: "")
        
    }
    
    func substring(from: Int, to: Int) -> String {
        let start = index(startIndex, offsetBy: from)
        let end = index(start, offsetBy: to - from)
        return String(self[start ..< end])
    }
    
    func substring(range: NSRange) -> String {
        return substring(from: range.lowerBound, to: range.upperBound)
    }
    
}

extension UIViewController {
    
    func seviceAlert(vi: UIViewController){
        
        let alert = UIAlertController(title: "지갑", message: "서비스 준비중 입니다.", preferredStyle: .alert)
        
        let action = UIAlertAction(title: "OK", style: .default) { (_) in
            print("wallet click ")
        }
        
        alert.addAction(action)
        
        vi.present(alert, animated: false, completion: nil)
        
    }
    
}

extension UINavigationController {
    
    func addShadowToBar(vi : UIViewController) {
        
        let shadowView = UIView(frame: vi.navigationController!.navigationBar.frame)
        shadowView.frame.origin = CGPoint(x: 0, y: 0)
        shadowView.frame.size.height = 2
        shadowView.backgroundColor = UIColor.DPdefaultBackgroundColor
        shadowView.layer.masksToBounds = false
        shadowView.layer.shadowOpacity = 1 // your opacity
        //        shadowView.layer.shadowOffset = CGSize(width: 0, height: 2) // your offset
        shadowView.layer.shadowRadius =  0 //your radius
        vi.view.addSubview(shadowView)
    }
    
}

extension UITextView {
    
    func alignTextVerticallyIncontainer() {
        var topCorrection = (self.bounds.size.height - self.contentSize.height * self.zoomScale) / 2
        topCorrection = max(0, topCorrection)
        self.contentInset = UIEdgeInsets(top: topCorrection, left: 0, bottom: 0, right: 0)
        
    }
    
    
}

