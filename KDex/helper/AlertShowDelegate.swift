//
//  AlertShowDelegate.swift
//  KDex
//
//  Created by park heewon on 2018. 5. 24..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation

protocol AlertShowDelegate {
    
    func alerShowAction()
    
}
