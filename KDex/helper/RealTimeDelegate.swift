//
//  RealTimeDelegate.swift
//  DEXnPAY
//
//  Created by park heewon on 2018. 3. 23..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation

protocol RealTimeDelegate {

    func exchangListRT(bt: NPriceTick)
    func exchangDetailRT(bt: NPriceTick)
    func trandeListRT(bt: NPriceInfo)
    func openListRT(bt:NOpenInfo)
    func krwChangeRT(value: Double)
    
}
