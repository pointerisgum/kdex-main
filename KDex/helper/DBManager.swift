//
//  DBManager.swift
//  crevolc
//
//  Created by crovolc on 2018. 2. 5..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation

class DBManager : NSObject {
    
    static let sharedInstance = DBManager()
    
    var databasePath = String()
    
    //데이터베이스 체크
    func checkAndCopyDB(){
        
        let fileMgr = FileManager.default
        let dirPaths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let docsDir = dirPaths[0]
        databasePath = "\(docsDir)/KDexWallet.db"
        
        print("databasePath : \(databasePath)")
        
        if !fileMgr.fileExists(atPath: databasePath as String) {
            
            //데이터베이스 생성
            let db = FMDatabase(path: databasePath as String)
            
            if db.open() {
            
                let sql_walletstatement = "CREATE TABLE IF NOT EXISTS WALLETS (ID INTEGER PRIMARY KEY AUTOINCREMENT, wletNo TEXT, wletTp TEXT, walletSimbole TEXT, walletKey TEXT)"
                
                if !(db.executeStatements(sql_walletstatement)){
                    NSLog("user table create error")
                }
                
                db.close()
                
            }else {
                NSLog("dbcheck connect error")
            }
            
        }else {
            NSLog("DB file is exist")
        }
    }
    
    //새로운 지갑 키 삽입
    func insertWalletKey(simbol : String , privateKey : String, wletNo: String, wletTp: String) -> Bool{
        
        print("insert wallet DB")
        
        let db = FMDatabase(path: databasePath as String)
        
        if db.open() {
            let sql_statement = "INSERT INTO WALLETS (walletSimbole, walletKey, wletTp, wletNo) VALUES (:walletSimbole, :walletKey, :wletTp, :wletNo)"
            
            var paramDictionary = [String : String]()
            
            paramDictionary["walletSimbole"] = simbol
            paramDictionary["walletKey"] = privateKey
            paramDictionary["wletNo"] = wletNo
            paramDictionary["wletTp"] = wletTp
            
            let result = db.executeUpdate(sql_statement, withParameterDictionary: (paramDictionary as? [String: String])!)
            
            if !result {
                print("등록오류 \(String(describing: db.lastErrorMessage()))")
            }else{
                print("등록성공")
            }
            
            db.close()
            
        }else{
            
            print("deleteMember db connect error")
            
        }
        return true
    }
    
    //사용중지
    func UpdateWalletKey(simbol : String , privateKey : String, wletNo: String) -> Bool{
        
        let db = FMDatabase(path: databasePath as String)
        
        if db.open() {

            do {
                let sql_statement = "Update WALLETS SET walletKey = :walletKey where wletNo == :wletNo "
                
                var paramDictionary = [String : String]()
                
                //            paramDictionary["walletType"] = simbol
                paramDictionary["walletKey"] = privateKey
                paramDictionary["wletNo"] = wletNo
                //paramDictionary["id"] = id
                
                let result = db.executeUpdate(sql_statement, withParameterDictionary: (paramDictionary as? [String: String])!)
                
                if !result {
                    print("차단멤버 업데이트 오류 \(String(describing: db.lastErrorMessage()))")
                }else{
                    print("채팅멤버 업데이트 성공")
                }
                
            }
            
            db.close()
            
        }else{
            
            print("deleteMember db connect error")
            
        }
        return true
    }
    
    func deleteWalletKey() -> Bool{
        
        let db = FMDatabase(path: databasePath as String)
        
        if db.open() {
            
            let sql_statement = "delete from WALLETS"
            
            let result = db.executeStatements(sql_statement)
            
            if !result {
                print("차단멤버 업데이트 오류 \(String(describing: db.lastErrorMessage()))")
            }else{
                print("채팅멤버 업데이트 성공")
            }
            
            db.close()
            
        }else{
            print("deleteMember db connect error")
        }
        return true
    }
    
    func selecttWalletSimbol(simbol : String) -> NSMutableArray{
        print("db selecttWalletSimbol")
        
        let db = FMDatabase(path: databasePath as String)
        
        let returnArray = NSMutableArray()
        
        if db.open() {
            
            //var sql_statement : String = "select * from WALLET where walletType == walletType "
            let sql_statement : String = "select * from WALLETS where walletSimbole == :walletSimbole and wletTp == :wletTp "
            do{
                
                var paramDictionary = [String : String]()
                
                paramDictionary["walletSimbole"] = simbol
                paramDictionary["wletTp"] = "D"
               
                guard let rs = try db.executeQuery(sql_statement, withParameterDictionary: (paramDictionary as [String: String])) else {
                    return returnArray
                }
                
                print("db query done good ")
                while rs.next() {
                    
                    print("db query next")
                    let simbol = rs.string(forColumn: "walletSimbole")
                    let id = rs.string(forColumn: "id")
                    let key = rs.string(forColumn: "walletKey")
                    let wletNo = rs.string(forColumn: "wletNo")
                    let wletTp = rs.string(forColumn: "wletTp")
                    
                    print("simbol : \(simbol), key : \(id)")
                    AlertVO.shared.privKey1 = key!
                    
                    print("DB privateKey : \(key!)")
                    
                    returnArray.add(rs)
                }
                
            }catch{
                print("update Error : \(error.localizedDescription)")
                
            }
        }
        return returnArray
    }
    
    func selectPointAddSimbol(simbol : String) -> NSMutableArray{
        print("db selecttWalletSimbol")
        
        let db = FMDatabase(path: databasePath as String)
        
        let returnArray = NSMutableArray()
        
        if db.open() {
            
            //var sql_statement : String = "select * from WALLET where walletType == walletType "
            let sql_statement : String = "select * from WALLETS where walletSimbole == :walletSimbole "
            do{
                
                var paramDictionary = [String : String]()
                
                paramDictionary["walletSimbole"] = simbol
                
                guard let rs = try db.executeQuery(sql_statement, withParameterDictionary: (paramDictionary as [String: String])) else {
                    return returnArray
                }
                
                print("db query done good ")
                while rs.next() {
                    
                    print("db query next")
                    let simbol = rs.string(forColumn: "walletSimbole")
                    let id = rs.string(forColumn: "id")
                    let key = rs.string(forColumn: "walletKey")
                    
                    print("simbol : \(simbol), key : \(id)")
                    AlertVO.shared.pointAddprivKey = key!
                    print("DB privateKey : \(key!)")
                    
                    returnArray.add(rs)
                }
                
            }catch{
                print("update Error : \(error.localizedDescription)")
                
            }
        }
        return returnArray
    }
    
    func selecttWalletMarket(market : String) -> NSMutableArray{
        print("selecttWalletSimbol")
        
        let db = FMDatabase(path: databasePath as String)
        
        let returnArray = NSMutableArray()
        
        if db.open() {
            
            //var sql_statement : String = "select * from WALLET where walletType == walletType "
            let sql_statement : String = "select * from WALLET where walletSimbole == :walletSimbole and wletTp == :wletTp "
            do{
                
                var paramDictionary = [String : String]()
                
                paramDictionary["walletSimbole"] = market
                paramDictionary["wletTp"] = "D"
                
                guard let rs = try db.executeQuery(sql_statement, withParameterDictionary: (paramDictionary as [String: String])) else {
                    return returnArray
                }
                
                print("query done good ")
                while rs.next() {
                    
                    print("query next")
                    let simbol = rs.string(forColumn: "walletType")
                    let id = rs.string(forColumn: "id")
                    let key = rs.string(forColumn: "walletKey")
                    
                    print("simbol : \(market), key : \(id)")
                    AlertVO.shared.privKey2 = key!
                    print("DB privateKey : \(key!)")
                    
                    returnArray.add(rs)
                }
                
            }catch{
                print("update Error : \(error.localizedDescription)")
                
            }
        }
        return returnArray
    }
    
    func selecttWalletKey() -> NSMutableArray{
        
        let db = FMDatabase(path: databasePath as String)
        
        let returnArray = NSMutableArray()
        
        if db.open() {
            
            var sql_statement : String = "select * from WALLETS"
            
            do{
                let rs = try db.executeQuery(sql_statement, values: nil)
                
                while rs.next() {
                    let wletNo = rs.string(forColumn: "wletNo")
                    let symbol = rs.string(forColumn: "walletSimbole")
                    let wletKey = rs.string(forColumn: "walletKey")
                    let wletTp = rs.string(forColumn: "wletTp")
                    
//                    let key = rs.string(forColumn: "walletType")
//                    print("wallet simbol : \(wletNo), key : \(key)")
                    
                    print("wallet rs : \(rs)")
                    
                    returnArray.add(["wletNo": wletNo, "symbol": symbol , "wletKey": wletKey , "wletTp": wletTp ])
                }
                
            }catch{
                print("Inser Error : \(error.localizedDescription)")
                
            }
        }
        return returnArray
    }
    
}
