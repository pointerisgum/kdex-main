//
//  EmailLoginViewController.swift
//  crevolc
//
//  Created by crovolc on 2018. 1. 9..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//
// 이메일 로그인 컨트롤페이지

import UIKit
import Foundation
import Alamofire
import FBSDKLoginKit
import JWTDecode
import AuthenticationServices

class EmailLoginViewController : UIViewController, UITextFieldDelegate, GIDSignInDelegate, GIDSignInUIDelegate, NaverThirdPartyLoginConnectionDelegate, XMLParserDelegate, ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding {
    
    
    //kakao user info
    fileprivate var kakaouser : KOUserMe? = nil
    
    let activity = UIActivityIndicatorView()
    
    var tag = 0
    
    //로그인 텍스트 필드
    @IBOutlet var loginIDTX: UITextField!
    
    //패스워드 텍스트 필드
    @IBOutlet var loginPWTX: UITextField!
    
    //로그인 버튼
    @IBOutlet weak var loginBtn: UIButton!
    
    //하단 밑줄
    @IBOutlet weak var footerLine: UIView!
    
    //비밀번호찾기 버튼
    @IBOutlet weak var pwSearch: UIButton!
    
    //이메일 찾기 버튼
    @IBOutlet weak var emailSearch: UIButton!
    
    //apple login button
    @IBOutlet weak var appleLogin: UIButton!
    
    
    
    let emailImg : UIView = {
        let img = UIView()
        return img
    }()
    
    let passwordImg : UIView = {
        let img = UIView()
        return img
    }()
    
    //네이버 회원정보 파싱
    let recordKey = "response"
    let dictionaryKeys = ["id", "nickname", "email"]
     
    var naverResults = [[String: String]]()           // the whole array of dictionaries
    var currentDictionary: [String: String]!  // the current dictionary
    var currentValue: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let plist = UserDefaults.standard
        
        //자동로그인 확인 변수
        let isLogin = plist.bool(forKey: "loginIs")
        
        if isLogin == true {
            self.performSegue(withIdentifier: "crevolcLogin", sender: self)
        }
        
        print("email login view controller : \(plist.string(forKey: "userEmail"))")
        
        if plist.string(forKey: "saveUserEmail") != nil && plist.bool(forKey: "emailLogin") {
            loginIDTX.text = plist.string(forKey: "saveUserEmail")!
        }
        
        GIDSignIn.sharedInstance().delegate = self
        
        emailImg.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        passwordImg.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        
        let email = UIImage(named: "ico_inp1")
        let emailvi = UIImageView(image: email)
        emailImg.addSubview(emailvi)
        emailvi.frame.size = CGSize(width: 20, height: 20)
        emailvi.center = CGPoint(x: emailImg.frame.width/2, y: emailImg.frame.height/2)
        
        let pw = UIImage(named: "ico_inp2")
        let pwvi = UIImageView(image: pw)
        passwordImg.addSubview(pwvi)
        pwvi.frame.size = CGSize(width: 20, height: 20)
        pwvi.center = CGPoint(x: passwordImg.frame.width/2, y: passwordImg.frame.height/2)
        
        pwSearch.setTitleColor(UIColor.white, for: .normal)
        
        emailSearch.setTitleColor(UIColor.white, for: .normal)
        
        footerLine.backgroundColor = UIColor.DPLineBgColor
        
        loginBtn.backgroundColor = UIColor.DPDefaultBtnColor
        
        GIDSignIn.sharedInstance().uiDelegate = self
        
        self.navigationController?.navigationBar.barTintColor = UIColor.DPNaviBarTintColor
        self.navigationController?.navigationBar.tintColor = UIColor.DPNaviBartextTintColor
        self.navigationController?.navigationBar.topItem?.title = ""
        
        self.view.backgroundColor = UIColor.DPLoginBgColor
        
        
        self.loginIDTX.attributedPlaceholder = NSAttributedString(string: "register_act_email_hint".localized, attributes: [NSAttributedString.Key.foregroundColor: UIColor.DPmainTextColor])
        self.loginIDTX.leftView = emailImg
        self.loginIDTX.leftViewMode = .always
        self.loginPWTX.attributedPlaceholder = NSAttributedString(string: "register_act_password_hint".localized, attributes: [NSAttributedString.Key.foregroundColor: UIColor.DPmainTextColor])
        self.loginPWTX.leftViewMode = .always
        self.loginPWTX.leftView = passwordImg
        
    }
    
    @IBAction func joinAction(_ sender: UIButton) {
      
    }
    
    //로그인 버튼 클릭 메소드
    @IBAction func loginAction(_ sender: UIButton) {
        
        guard let appdel = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        if loginIDTX.text == "" {
            let msg = "pop_login_id_input".localized
            let alert = UIAlertController(title: "register_act_email_hint".localized, message: msg, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
            self.present(alert, animated: false, completion: nil)
            
        }else if loginPWTX.text == "" {
            let msg = "pop_login_passwd_input".localized
            let alert = UIAlertController(title: "register_act_password_hint".localized, message: msg, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
            self.present(alert, animated: false, completion: nil)
        }else {
            
            let plist = UserDefaults.standard
            plist.set(true, forKey: "emailLogin")
            plist.synchronize()
            
            if AppDelegate.WifiBool {
                self.login()
                
            }else {
                
                let url = "https://api.ip.pe.kr/"
                
                let amo = Alamofire.request(url)
                amo.responseString { response in
                    if response.result.isSuccess == true {
                        guard let result = response.result.value else {
                            return
                        }
                        
                        AppDelegate.ipAddr = result
                        print("real address result : \(result)")
                        self.login()
                        
                    }
                    
                }
                
            }
            
        }
    }
    
    
    // 이메일 로그인
    func login(){
        
        let plist = UserDefaults.standard
        
        let userEmail = plist.string(forKey: "userEmail") ?? ""
        
        print("login useremail : \(userEmail)")
        
        activity.style = .whiteLarge
        activity.color = UIColor.DPActivityColor
        activity.hidesWhenStopped = true
        activity.startAnimating()
        
        let id = self.loginIDTX.text as? String
        
        let pw = self.loginPWTX.text as? String
        
        if userEmail != self.loginIDTX.text! && userEmail != ""{
            
            let alert = UIAlertController(title: "pop_loing".localized, message: "pop_loing_not_id_text".localized, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
            
            self.present(alert, animated: false, completion: nil)
            
            return
        }
        
        let param = "userId=\(id!)&userPw=\(pw!)&deviceId=\(AppDelegate.devicevo.deviceId)&pushKey=\(AppDelegate.devicevo.pushKey)&version=\(AppDelegate.devicevo.version)&model=\(AppDelegate.devicevo.model)&os=\(AppDelegate.devicevo.os)&ip=\(AppDelegate.ipAddr)"
        
        print(param)
        
        let paramData = param.data(using: .utf8)
        
        let url = URL(string: "\(AppDelegate.url)/auth/loginAppProc")
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        request.httpBody = paramData
        
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        request.setValue(String(describing: paramData!.count), forHTTPHeaderField: "Content-Length")
        
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if let err = error  {
                
                print("login error is failed")
                
                DispatchQueue.main.async {
                    let msg = "pop_login_error_network".localized
                    let alert = UIAlertController(title: "pop_loing".localized, message: msg, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
                    self.present(alert, animated: false, completion: nil)
                    
                    self.activity.stopAnimating()
                }
                
                return
                
            }
            
            let plist = UserDefaults.standard
            
            DispatchQueue.main.async {
                do{
                    let object = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                    
                    guard let jsonObject = object else { return }
                    
                    print("login id : \(object)")
                    
                    let result = jsonObject["result"] as? String
                    let uid = jsonObject["uid"] as? String
                    let sessionid = jsonObject["sessionId"] as? String
                    
                    let resultMsg = jsonObject["resultMsg"] as? String ?? ""
                    
                    
                    print("udi: \(uid),sessionid: \(sessionid)")
                    
                    if result == "OK" {
                        
                        print("login success")
                        
                        plist.set(uid!, forKey: "uid")
                        
                        plist.set(sessionid!, forKey: "sessionId")
                        
                        plist.set(true, forKey: "loginIs")
                        
                        if userEmail == "" {
                            plist.set(self.loginIDTX.text! , forKey: "userEmail")
                            plist.set(self.loginPWTX.text! , forKey: "userPasswd")
                        }
                        
                        AppDelegate.uid = uid!
                        
                        AppDelegate.sessionid = sessionid!
                        
                        plist.removeObject(forKey: "sType")
                        
                      //self.test.start()
                        
                        self.loginIDTX.text = ""
                        self.loginPWTX.text = ""
                        
                        guard let appdel = UIApplication.shared.delegate as? AppDelegate else {
                            return
                        }
                        
                        appdel.inglogin()
                        
//                        AppDelegate.walletCheck(uid: uid!, sessionid: sessionid!)
                        
                        self.performSegue(withIdentifier: "crevolcLogin", sender: self)
                        
                    }else {
                        print("login false")
                        
                        plist.removeObject(forKey: "uid")
                        plist.removeObject(forKey: "sessionId")
                        
//                        let msg = "pop_login_failed".localized
                        let alert = UIAlertController(title: "pop_loing".localized, message: resultMsg, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
                        
                        self.present(alert, animated: false, completion: nil)
                    }
                    
                    self.activity.stopAnimating()
                    
                }catch{
                
                    print("json pasing error : \(error)")
                    
                    plist.removeObject(forKey: "uid")
                    
                    plist.removeObject(forKey: "sessionId")
                    
                    let msg = "pop_login_failed".localized
                    let alert = UIAlertController(title: "pop_loing".localized, message: msg, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
                    self.present(alert, animated: false, completion: nil)
                    
                    self.activity.stopAnimating()
                }
                plist.synchronize()
            }
        }
        task.resume()
    }
    
    //키보드 리턴시 키보드 닫힘
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        loginIDTX.resignFirstResponder()
        loginPWTX.resignFirstResponder()
    }
    
    @IBAction func kakaoLoginAction(_ sender: Any) {
        
        let session: KOSession = KOSession.shared()!;
        
        if session.isOpen() {
            session.close()
        }
        
        session.open(completionHandler: { (error) -> Void in
            print("click kakao login")
            if !session.isOpen() {
                print("end session close")
                switch ((error as NSError!).code) {
                case Int(KOErrorCancelled.rawValue):
                    break;
                default:
                    print(error)
                    break;
                }
            }else{
                    print("success")
                    var email = ""
                    var nickNames = ""
                    var id = ""
                    let dvicevo = AppDelegate.devicevo
                    //2021.06.11 수정
                
                
                    KOSessionTask.userMeTask(completion: { (error, user) in
                        if user != nil {
                            print("kakao login user info print")
                            guard let kouser = user else { return }

                            self.kakaouser = kouser as! KOUserMe
//
//                            if let properties = self.kakaouser?.properties {

                                if self.kakaouser?.account?.email != nil {
                                    email = (self.kakaouser?.account?.email)!
                                    print("kakao email : \(email)")
                                }
                                //
//                                if let nickName = properties[KOUserNicknamePropertyKey] as? String {
                                if self.kakaouser?.nickname != nil  {
                                    nickNames = self.kakaouser?.nickname ?? ""
                                    print("kakao nick : \(nickNames)")
                                }

                                id = (self.kakaouser?.id)!
                                print("kakao id: \(id)")

//                            }

                        }

                        let socialVo = SocialLoginVO(userid: email, sType: SType.K, waysType: "M", socialId: id, nickName: nickNames, MoblieNumber: "11", deviceId:dvicevo.deviceId , pushKey: dvicevo.pushKey, version: dvicevo.version, model: dvicevo.model, os: dvicevo.os)

                        if AppDelegate.WifiBool {

                            AppDelegate.login(vo: socialVo, vc: self)
                            

                        }else {

                            let url = "https://api.ip.pe.kr/"

                            let amo = Alamofire.request(url)
                            amo.responseString { response in
                                if response.result.isSuccess == true {
                                    guard let result = response.result.value else {
                                        return
                                    }

                                    AppDelegate.ipAddr = result
                                    print("real address result : \(result)")


                                    AppDelegate.login(vo: socialVo, vc: self)


                                }

                            }

                        }

                    })

            }
            
        })
        
    }
    
    @IBAction func googleLoginAction(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func facebookLoginAction(_ sender: Any) {
    
//        LoginManager().logIn(withReadPermissions: ["email", "public_profile"], from: self){ (result, error) in
//            if error != nil {
//                return
//            }
//
//            FBSDKGraphRequest(graphPath: "/me", parameters: ["fields": "id, name, email"]).start {
//                (connection, result, error ) in
//
//                if error != nil {
//                    print("Failed to start graph request", error)
//                    return
//
//                }
//                print(result)
//            }
//        }
    }
    
    
    @IBAction func naverLoginAction(_ sender: Any) {
        print("naverLoginAction")
        let naverConnection = NaverThirdPartyLoginConnection.getSharedInstance()
        naverConnection?.delegate = self
        naverConnection?.requestThirdPartyLogin()
    }
    
    
    @IBAction func emailFindAction(_ sender: Any) {
//        let alert = UIAlertController(title: "계정찾기", message: "현재 kdex.biz 홈페이지를 통해 확인 가능합니다", preferredStyle: .alert)
//
//        alert.addAction(UIAlertAction(title: "확인", style: .cancel, handler: nil))
//
//        self.present(alert, animated: false, completion: nil)
        if let btn = sender as? UIButton {
            self.tag = btn.tag
            print("tag number : \(btn.tag)")
        }
        
        self.performSegue(withIdentifier: "findAcount", sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let dest = segue.destination
        
        guard let rvc = dest as? FindAccountController else {
            return
        }
        
        if self.tag == 0 {
            rvc.url = "\(AppDelegate.url)/mobile/findEmail"
            rvc.titl = "login_email_find".localized
        }else {
            rvc.url = "\(AppDelegate.url)/mobile/findPw1"
            rvc.titl = "login_passwd_find".localized
        }
        
    }
    
    
    func oauth20ConnectionDidFinishRequestACTokenWithAuthCode() {
        print("oauth20ConnectionDidFinishRequestACTokenWithAuthCode")
        getNaverEmailFromURL()
    }
    
    func oauth20Connection(_ oauthConnection: NaverThirdPartyLoginConnection!, didFailWithError error: Error!) {
        print("error \(error)")
        /* 로그인 실패시에 호출되며 실패 이유와 메시지 확인 가능합니다. */
    }
    
    func oauth20ConnectionDidOpenInAppBrowser(forOAuth request: URLRequest!) {
        // 네이버 앱이 설치되어있지 않은 경우에 인앱 브라우저로 열리는데 이때 호출되는 함수
        print("naver brower")
        let naverInappBrower = NLoginThirdPartyOAuth20InAppBrowserViewController(request: request)
        naverInappBrower?.modalPresentationStyle = .overFullScreen
        self.present(naverInappBrower!, animated: true, completion: nil)
        
    }
    
    func oauth20ConnectionDidFinishRequestACTokenWithRefreshToken() {
 
    }
    
    func oauth20ConnectionDidFinishDeleteToken() {
        // 로그아웃이나 토큰이 삭제되는 경우
    }
    
    func getNaverEmailFromURL() {
        
        /*
        print("getNaverEmailFromURL")
        
        let naverConnection = NaverThirdPartyLoginConnection.getSharedInstance()
        
        guard let token = naverConnection!.accessToken else {
            return
        }
        guard let tokenType = naverConnection?.tokenType else {
            return
        }
        print("naver token type : \(tokenType) ")
        print("naver token : \(token) ")
        
        var urlComponents = URLComponents(string: "https://openapi.naver.com/v1/nid/me")!
        urlComponents.queryItems = [
            URLQueryItem(name: "Authorization", value: "\(tokenType) \(token)"),
        ]
        
        let amo = Alamofire.request(urlComponents)
        amo.responseJSON { response in
            
            print("naver result value : \(response.result.value)")
            
            
        }
        */
       
        // Naver SignIn Success
        let dvicevo = AppDelegate.devicevo
        
        let loginConn = NaverThirdPartyLoginConnection.getSharedInstance()
        
        let tokenType = loginConn?.tokenType
        
        let accessToken = loginConn?.accessToken
        
        if let url = URL(string: "https://apis.naver.com/nidlogin/nid/getUserProfile.xml") {
            
            if tokenType != nil && accessToken != nil {
                
                let authorization = "\(tokenType!) \(accessToken!)"
                
                var request = URLRequest(url: url)
                
                request.setValue(authorization, forHTTPHeaderField: "Authorization")
                
                let dataTask = URLSession.shared.dataTask(with: request) {(data, response, error) in
                    
                    if let str = String(data: data!, encoding: .utf8) {
                        
                        print("naver str \(str)")
                        
                        let passer = XMLParser(data: data!)
                        passer.delegate = self
                        
                        passer.parse()
                        
                        print("naver vale : \(self.naverResults)")
                        
                        let naverValue = self.naverResults[0]
                        
                        let socialVo = SocialLoginVO(userid:naverValue["email"]!, sType: .N, waysType: "M", socialId:naverValue["id"]!, nickName:naverValue["nickname"]!, MoblieNumber: "11", deviceId: dvicevo.deviceId , pushKey: dvicevo.pushKey, version: dvicevo.version, model: dvicevo.model, os: dvicevo.os)
                        
                        if AppDelegate.WifiBool {
                            AppDelegate.login(vo: socialVo, vc: self)
                        }else {
                            
                            let url = "https://api.ip.pe.kr/"
                            
                            let amo = Alamofire.request(url)
                            amo.responseString { response in
                                if response.result.isSuccess == true {
                                    guard let result = response.result.value else {
                                        return
                                    }
                                    
                                    AppDelegate.ipAddr = result
                                    print("real address result : \(result)")
                                    AppDelegate.login(vo: socialVo, vc: self)
                                    
                                }
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
                dataTask.resume()
                
            }
            
        }
        
    }
   
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        if elementName == recordKey {
            currentDictionary = [String : String]()
        } else if dictionaryKeys.contains(elementName) {
            currentValue = String()
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == recordKey {
            naverResults.append(currentDictionary)
            currentDictionary = nil
            
        } else if dictionaryKeys.contains(elementName) {
            
            currentDictionary[elementName] = currentValue
            currentValue = nil
            
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        currentValue? += string
    }
    
    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        print("failure error: ", parseError)
    }
    
    //google delegate
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!){
        if (error == nil) {
            
            print("google delegate is good")
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            //let accestoken = user.authentication.accessToken
            let fullName = user.profile.name
            //            let givenName = user.profile.givenName
            //            let familyName = user.profile.familyName
            let email = user.profile.email
            
            do{
                let jwt = try decode(jwt: idToken!)
                
                guard let subToken = jwt.body["sub"] else {
                    return
                }
                
                let dvicevo = AppDelegate.devicevo
                
                if email != nil && userId != nil {
                    
                    let socialVo = SocialLoginVO(userid: email!, sType: .G, waysType: "M", socialId: subToken as! String, nickName: fullName!, MoblieNumber: "1111", deviceId:dvicevo.deviceId , pushKey: dvicevo.pushKey, version: dvicevo.version, model: dvicevo.model, os: dvicevo.os)
                    
                    if AppDelegate.WifiBool {
                        AppDelegate.login(vo: socialVo, vc: self)
                    }else {
                        
                        let url = "https://api.ip.pe.kr/"
                        
                        let amo = Alamofire.request(url)
                        amo.responseString { response in
                            if response.result.isSuccess == true {
                                guard let result = response.result.value else {
                                    return
                                }
                                
                                AppDelegate.ipAddr = result
                                print("real address result : \(result)")
                                AppDelegate.login(vo: socialVo, vc: self)
                                
                            }
                            
                        }
                        
                    }
                }
                
            }catch{
                print("error")
            }
            
        } else {
            print("\(error.localizedDescription)")
        }
        
    }
    
    // 구글 앱 연결 끊을 때 작업 수행
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        
    }
    
    //apple 로그인 버튼 생성
    @available(iOS 13.0, *)
    @IBAction func appleLoginAction(_ sender: Any) {
        
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
            
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
    
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        print("apple authorizationController")
        
        switch authorization.credential {
            // Apple ID
            case let appleIDCredential as ASAuthorizationAppleIDCredential:
                
                // 계정 정보 가져오기
                let userIdentifier = appleIDCredential.user
                let fullName = appleIDCredential.fullName
                let email = appleIDCredential.email
                let dvicevo = AppDelegate.devicevo
                    
                print("apple User ID : \(userIdentifier)")
                print("apple User Email : \(email ?? "")")
                print("apple User Name : \((fullName?.givenName ?? "") + (fullName?.familyName ?? ""))")
                
                let socialVo = SocialLoginVO(userid: email ?? "", sType: SType.A, waysType: "M", socialId: userIdentifier, nickName: (fullName?.givenName ?? "") + (fullName?.familyName ?? ""), MoblieNumber: "11", deviceId:dvicevo.deviceId , pushKey: dvicevo.pushKey, version: dvicevo.version, model: dvicevo.model, os: dvicevo.os)
                
                
                if AppDelegate.WifiBool {
                    AppDelegate.login(vo: socialVo, vc: self)
                }else {

                    let url = "https://api.ip.pe.kr/"

                    let amo = Alamofire.request(url)
                    amo.responseString { response in
                        if response.result.isSuccess == true {
                            guard let result = response.result.value else {
                                return
                            }

                            AppDelegate.ipAddr = result
                            print("apple real address result : \(result)")

                            AppDelegate.login(vo: socialVo, vc: self)


                        }

                    }

                }

            default:
                break
        }
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print("apple login error")
    }
    
}
