//
//  SocialLoginVO.swift
//  DEXnPAY
//
//  Created by park heewon on 2018. 3. 14..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation

enum SType : String{
    case G
    case F
    case K
    case N
    case A
}

struct SocialLoginVO {
    
    var userid : String
    var sType : SType
    var waysType = "M"
    var socialId : String
    var nickName : String
    var MoblieNumber : String
    
    var deviceId : String
    var pushKey : String
    var version : String
    var model : String
    var os : String

}
