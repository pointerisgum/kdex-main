//
//  AlertVO.swift
//  DEXnPAY
//
//  Created by park heewon on 2018. 3. 17..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation

class AlertVO : NSObject{
    
    static let shared = AlertVO()
    
    var simbol : String?
    
    var orderType : OrderType?
    
    var priceType : PriceType?
    
    var orderQty : String?
    
    var orderPrice : String?
    
    var WaysType : String?
    
    var privKey1 : String?
    
    var privKey2 : String?
    
    var pointAddprivKey : String?
    
    var saveKeyType : PriceType?
    
    var date : String?
    
    var time : String?
    
}

enum PriceType : Int {
    case A
    case B
}

enum OrderType {
    case B
    case S
}


