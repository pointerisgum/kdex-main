//
//  DeviceVO.swift
//  DEXnPAY
//
//  Created by park heewon on 2018. 3. 14..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//
import UIKit
import Foundation
import TAKUUID


struct DeviceVO {
    
    var deviceId : String
    var pushKey : String
    var version : String
    var model : String
    var os : String
    
    init(pushky : String) {
        
        //디바이스 아이디를 유일한값으로 작업
        TAKUUIDStorage.sharedInstance().migrate()
        let uuid = String(describing: TAKUUIDStorage.sharedInstance().findOrCreate() ?? "")
        
        self.deviceId = uuid
        
        print("deviceId : \(self.deviceId)")
        self.version = UIDevice.current.systemVersion
        self.model = UIDevice.current.model
        self.os = UIDevice.current.systemName
        self.pushKey = pushky
        
    }
}
