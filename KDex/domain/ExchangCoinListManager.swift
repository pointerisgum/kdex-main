//
//  ExchangCoinListManager.swift
//  crevolc
//
//  Created by crovolc on 2018. 1. 16..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation
import Alamofire

class ExchangCoinListManager {
    
    var subscribeRS : NTcsReal = NTcsReal()
    var jsonArray : [[String : Any]]? //매칭 데이터 json array 값
    var tempArray = [[String : Any]]() //매칭 데이터 json array 값
    var favoriArray = [String]()
    var favoriCoinListJson = [[String : Any]]()

    func coinAllList(market : String){
        
        AppDelegate.coinAllListJson.removeAll()
        
        let plist = UserDefaults.standard
        guard let uid = plist.string(forKey: "uid") else{
            return
        }
        guard let sessionid = plist.string(forKey: "sessionId") else{
            return
        }
        
        //매칭리스트 값 호출
        
        
        let urlComponents = URLComponents(string:"\(AppDelegate.url)/app/coinInfoList?market=&uid=\(uid)&sessionId=\(sessionid)&language=\(AppDelegate.appLang)")!
        
        let amo = Alamofire.request(urlComponents)
        amo.responseJSON(completionHandler:
            { response in
                
                guard let value = response.result.value as? [[String : Any]] else {
                    return
                }
                
                AppDelegate.coinAllListJson = value
                AppDelegate.coinAllListJson.removeAll()
                
                for row in value {
                    let dataTemp = row as [String : Any]
                    let symbol = dataTemp["simbol"] as? String ?? ""
                    
                    AppDelegate.coinAllListJson.append(row)

                    if symbol != AppDelegate.removeCoin {
                      AppDelegate.coinAllListJson.append(row)
                    }

                }
                
        })
    }
    
    //매칭리스트 값 호출
    func searchcoinList(market : String, tableview : UITableView, activity : UIActivityIndicatorView, search : String, first : Bool){
        
        self.jsonArray?.removeAll()
        
        let urlComponents = URLComponents(string:"\(AppDelegate.url)/app/coinInfoList?market=\(market)&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&language=\(AppDelegate.appLang)")!
        
        print("url : \(urlComponents)")
        activity.hidesWhenStopped = true
        activity.color = UIColor.DPActivityColor
        activity.startAnimating()
        
        let amo = Alamofire.request(urlComponents)
        amo.responseJSON(completionHandler:
            { response in
                
                guard let tempArrJson = response.result.value as? [[String : Any]] else { return }
                
                self.jsonArray = tempArrJson
                self.jsonArray?.removeAll()
                
                for row in tempArrJson {
                    let dataTemp = row as [String : Any]
                    let symbol = dataTemp["simbol"] as? String ?? ""

//                    self.jsonArray?.append(row)
                    if symbol != AppDelegate.removeCoin {
                        self.jsonArray?.append(row)
                    }

                }
                
                
                if first {
                
                    for val in self.jsonArray! {
                        let dataTemp = val as [String : Any]
                        let symbol = dataTemp["simbol"] as? String ?? ""

//                        self.subscribeRS.subscribeRealSise(uid: uid!, symbol: symbol, outputStream: TcpSocket.sharedInstance.outputStream!)
                    }
                    
                    if search != "" && self.jsonArray?.count != 0 {
                        
                        for row in self.jsonArray! {
                            let str = row["coinName"] as? String ?? ""
                            
                            if str.contains(search) {
                                print("test good")
                                self.tempArray.append(row)
                            }
                        }
                        
                        self.jsonArray = self.tempArray
                        
                        self.tempArray.removeAll()
                    }
                    
                }else{
                 print("first end page")
                    if search != "" || self.tempArray.count != 0 {
                        for row in self.jsonArray! {
                            
                            let str = row["coinName"] as? String ?? ""
                            
                            if str.contains(search) {
                                
                                self.tempArray.append(row)
                            }
                        }
                        
                        self.jsonArray = self.tempArray
                        
                        self.tempArray.removeAll()
                        
                    }
                    
                }
                tableview.reloadData()
                
                activity.stopAnimating()
        })
        
    }
    
    func favoritesList(uid: String, tableview : UITableView, activity : UIActivityIndicatorView, sessionid: String, search : String, first: Bool){
        print("favorite is start")
        self.coinAllList(market: " ")
        
        let urlComponents = URLComponents(string:"\(AppDelegate.url)/ajax/favCoinList?uid=\(uid)&sessionId=\(sessionid)")!
        
        print("favorites urlComponents : \(urlComponents)")
        
        activity.hidesWhenStopped = true
        activity.color = UIColor.DPActivityColor
        activity.startAnimating()
     
        let amo = Alamofire.request(urlComponents)
        amo.responseJSON { response in
            
            if let value = response.result.value as? [String] {
                
                print("\(value)")
                
                self.favoriArray = value
               
                AppDelegate.favoritesList = value
                
                print("favorites : \(value)")
                
                if value.count != 0 {
                    
                    if first {
                        print("first start page")
                        self.favoriCoinListJson.removeAll()
                        
                        for row in self.favoriArray {
                            
                            for val in AppDelegate.coinAllListJson {
                                
                                guard let coinName = val["coinName"] as? String else {
                                    return
                                }
                                guard let symbole = val["simbol"] as? String else {
                                    return
                                }
                                guard let market = val["market"] as? String else {
                                    return
                                }
                                
                                if row == symbole {
                                    self.favoriCoinListJson.append(val)
//                                    self.subscribeRS.subscribeRealSise(uid: uid, symbol: symbole, outputStream: TcpSocket.sharedInstance.outputStream!)
                                }
                                
                            }
                            
                        }
                        
                        if search != "" && self.favoriCoinListJson.count != 0 {
                            print("search : \(search)")
                            
                            for row in self.favoriCoinListJson {
                                let str = row["coinName"] as? String ?? ""
                                
                                if str.contains(search) {
                                    self.tempArray.append(row)
                                }
                            }
                            self.favoriCoinListJson = self.tempArray
                            
                            print(self.tempArray.count)
                            print(self.favoriCoinListJson.count)
                            
                            self.tempArray.removeAll()
                        }
                    
                    }else {
                        print("first end page")
                        
                        if search != "" || self.tempArray.count != 0 {
                             print("search : \(search)")
                            
                            for row in self.favoriCoinListJson {
                                let str = row["coinName"] as? String ?? ""
                                
                                if str.contains(search) {
                                    self.tempArray.append(row)
                                }
                            }
                            
                            self.favoriCoinListJson = self.tempArray
                            
                            print(self.tempArray.count)
                            print(self.favoriCoinListJson.count)
                            
                            self.tempArray.removeAll()
                        }else{
                            self.favoriCoinListJson.removeAll()
                            
                            for row in self.favoriArray {
                                
                                for val in AppDelegate.coinAllListJson {
                                    
                                    guard let coinName = val["coinName"] as? String else {
                                        return
                                    }
                                    guard let symbole = val["simbol"] as? String else {
                                        return
                                    }
                                    guard let market = val["market"] as? String else {
                                        return
                                    }
                                    
                                    if row == symbole {
                                        self.favoriCoinListJson.append(val)
                                    }
                                    
                                }
                                
                            }
                        }
                        
                    }
                    
                }else {
                    print("favorites value 0")
                    self.favoriCoinListJson.removeAll()
                }
                
            }else{
                print("favorites null error")
                self.favoriCoinListJson.removeAll()
            }
            
            tableview.reloadData()
            
            activity.stopAnimating()
            
        }
    
    }
    
    func favList(){
        
        let plist = UserDefaults.standard
        guard let uid = plist.string(forKey: "uid") else {
            return
        }
        guard let sessionid = plist.string(forKey: "sessionId") else {
            return
        }

        let urlComponents = URLComponents(string:"\(AppDelegate.url)ajax/favCoinList?uid=\(uid)&sessionId=\(sessionid)")!
        
        let amo = Alamofire.request(urlComponents)
        amo.responseJSON { response in
            
            if let value = response.result.value as? [String] {
                
                print("favorites count : \(value.count)")
                print(value)
                
                self.favoriArray = value
                
                AppDelegate.favoritesList = value
                
                print("favorieList: \(value)")
                
            }else{
                print("favorites null")
                AppDelegate.favoritesList.removeAll()
            }
        }
    }

}
