//
//  LoginInfo.swift
//  crevolc
//
//  Created by crovolc on 2018. 1. 15..
//  Copyright © 2018년 hanbiteni. All rights reserved.
// 로그인 매니져객체

import Foundation

struct LoginInforKey {
    
    static let loginId = "LOGINID"
    static let account = "ACCOUNT"
    static let loginPath = "LOGINPATH"
    static let name = "NAME"
    static let email = "EMAIL"
    
}

class LoginInfoManager {
    
    var loginid : String {
        get {
            return UserDefaults.standard.string(forKey: LoginInforKey.loginId)!
        }
        set(v) {
            let ud = UserDefaults.standard
            ud.set(v, forKey: LoginInforKey.loginId)
            ud.synchronize()
        }
    }
    
    var account : String? {
        get {
            return UserDefaults.standard.string(forKey: LoginInforKey.account)
        }
        
        set(v) {
            let ud = UserDefaults.standard
            ud.set(v, forKey: LoginInforKey.account)
            ud.synchronize()
        }
    }
    
    var loginpath : String? {
        get{
            return UserDefaults.standard.string(forKey: LoginInforKey.loginPath)
        }
        set(v) {
            let ud = UserDefaults.standard
            ud.set(v, forKey: LoginInforKey.loginPath)
            ud.synchronize()
        }
    }
    
    var name : String? {
        get{
            return UserDefaults.standard.string(forKey: LoginInforKey.name)
        }
        set(v){
            let ud = UserDefaults.standard
            ud.set(v, forKey: LoginInforKey.name)
            ud.synchronize()
        }
        
    }
    
    var isLogin : Bool {
        if self.loginid == "0" || self.account == nil {
            return false
        } else {
            return true
        }
        
    }
    
    
}
