//
//  ExchangInput.swift
//  crevolc
//
//  Created by crovolc on 2018. 1. 5..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation

struct ExchangInput {
    
    var user_id = ""    // 사용자 아이디
    var inst_cd = ""    // 종목 코드
    var bysl_tp = ""    // 매수매도 구분
    var prce_tp = ""    // 주문유형
    var ordr_qty = ""   // 주문수량
    var ordr_prc = ""   // 주문가격
    var ordr_no = ""    // 주문번호

}
