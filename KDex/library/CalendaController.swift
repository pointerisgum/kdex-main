//
//  CalendaController.swift
//  KDex
//
//  Created by park heewon on 2018. 8. 6..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation
import FSCalendar

class CalendaController : UIViewController, FSCalendarDelegate, FSCalendarDataSource {
    
    var delegate : CalendarProtocol!
    
    var startDate : Date?
    
    var endDate : Date?
    
    var startCompanyDate : Date?
    
    var type : calendarType?
    
    @IBOutlet var calendar: FSCalendar!
    
    @IBAction func cancelBtnAction(_ sender: Any) {
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0
        }) { (finished: Bool) in
            if(finished)
            {
                self.view.removeFromSuperview()
            }
        }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.DPPopupBgColor
        calendar.allowsMultipleSelection = false
        calendar.today = nil
        
        if  self.type == .start {
            calendar.select(startDate ?? Date())
        }else {
            calendar.select(endDate ?? Date())
        }
        
        
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        delegate.dateLabelEdit(date: date, type: type ?? .start)
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0
        }) { (finished: Bool) in
            if(finished)
            {
                self.view.removeFromSuperview()
            }
        }
        
    }
    
    func minimumDate(for calendar: FSCalendar) -> Date {
        
        if  self.type == .start {
            return startCompanyDate ?? Date()
        }else {
            return startDate ?? Date()
        }
        
    }
    
    func maximumDate(for calendar: FSCalendar) -> Date {
        
        if  self.type == .start {
            return endDate ?? Date()
        }else {
            return Date()
        }
        
    }
    
}


enum calendarType {
    case start
    case end
}


protocol CalendarProtocol {
    func dateLabelEdit(date : Date, type: calendarType)
}
