//
//  WalletSendViewController.swift
//  crevolc
//
//  Created by crovolc on 2018. 1. 9..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import MobileCoreServices

class WalletSendViewController : UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, EmailAuthDelegate {
    
    var QRcode = "" {
        didSet{
            print("qrcode observer start : \(QRcode)")
            self.sendAddress.text = QRcode
            self.addressfind()
        }
    }
    
    let plist = UserDefaults.standard
    
    let activity = UIActivityIndicatorView()
    
    var walletPrivatekey : String?
    
    var toatLb : UILabel?
    
    var coinsimbol : String?
    
    var coinName = "" //코인 심볼 이름
    
    var coinType = ""
    
    var lastPrice = ""
    
    var ethLastPrice = ""
    
    var walletAddress : String?
    
    @IBOutlet var sendfeeSimoblsLb: UILabel!
    
    @IBOutlet var totalamountLabel: UILabel!
    
    var newMedia = false
    
    var walletJson = [String : Any]()
    
    var ethWallet = [String : Any]()
    
    var image : UIImage? //업로드 이미지 저장 변수
    
    var speedBtntap = 0 {
        didSet {
            if speedBtntap == 9 && coinsimbol == "ETH"{
                self.sendSpeedfield.isEnabled = true
                self.gasLimitField.isEnabled = true
//                self.sendSpeedfield.isUserInteractionEnabled = true
//                self.sendSpeedfield.backgroundColor = UIColor.DPViewBackgroundColor
            }
        }
    }
    
    var isChecked = false {
        didSet {
            if isChecked {
                self.checkBoxBtn.image = self.checkImg
            }else {
                self.checkBoxBtn.image = self.unckeckImg
            }
        }
    }
    
    var isAddress = false
    
    var sendfee = ""
    
    @IBOutlet var selectorLine: UIView!
    
    let checkImg = UIImage(named: "ico_ch_on")
    
    let unckeckImg = UIImage(named: "ico_ch_off")
    
    @IBOutlet var gasLimitLbel: UILabel!
    
    @IBOutlet var sendAddress: UITextField!
    
    @IBOutlet var sendQty: UITextField!
    
    @IBOutlet var totalAmount: UILabel!
    
    @IBOutlet var addressOkBtn: UIButton!
    
    @IBOutlet var selectorTopValue: NSLayoutConstraint!
    
    @IBOutlet var contentRow: UITextView!
    
    @IBOutlet var maxBtn: UIButton!
    
    @IBOutlet var sendFees: UILabel!
    
    @IBOutlet var manuBtn: UIButton!
    
    @IBOutlet var gasLimitField: UITextField!
    
    @IBOutlet var sendSpeedfield: UITextField!
    
    @IBOutlet var checkBoxBtn: UIImageView!
    
    @IBOutlet var sendSpeedBtns: [UIButton]!
    
//    @IBOutlet var vitopView: UIView!
    
    @IBOutlet var sendSppedWon: UILabel!
    
    @IBOutlet var totalAmountWon: UILabel!
    
    @IBAction func manuBtnAction(_ sender: Any) {
        sendSpeedBtns.forEach { (button) in
            
            UIView.animate(withDuration: 0.35, animations: {
                if (self.coinsimbol ?? "") != "ETH" && button.tag == 9 && self.coinType != "2" && (self.coinsimbol ?? "") != "KDA" && (self.coinsimbol ?? "") != "DPN" && (self.coinsimbol ?? "") != "FNK" && (self.coinsimbol ?? "") != "MCVW" && (self.coinsimbol ?? "") != "BTR" && (self.coinsimbol ?? "") != "520" && (self.coinsimbol ?? "") != "EYEAL" && (self.coinsimbol ?? "") != "METAC" && (self.coinsimbol ?? "") != "AGO" {
                    
                }else {
                    button.isHidden = !button.isHidden
                    self.view.layoutIfNeeded()
                }
                
            })
        }
        
    }
    
    @IBAction func sendSpeedBtnsAction(_ sender: UIButton) {
        
        manuBtn.titleLabel?.text = sender.titleLabel?.text
        speedBtntap = sender.tag
        sendCost()
        sendSpeedBtns.forEach { (button) in
            UIView.animate(withDuration: 0.35, animations: {
                if self.coinsimbol != "ETH" && button.tag == 9 && self.coinType != "2" && self.coinsimbol != "KDA" && self.coinsimbol != "DPN" && (self.coinsimbol ?? "") != "FNK" && (self.coinsimbol ?? "") != "MCVW" && (self.coinsimbol ?? "") != "BTR" && (self.coinsimbol ?? "") != "520" && (self.coinsimbol ?? "") != "EYEAL" && (self.coinsimbol ?? "") != "METAC" && (self.coinsimbol ?? "") != "AGO"{
                    
                }else {
                    button.isHidden = !button.isHidden
                    self.view.layoutIfNeeded()
                }
            })
        }
        
        if sender.tag == 9 && self.coinsimbol == "ETH"  {
            sendSpeedfield.isEnabled = true
            gasLimitField.isEnabled = true
        }else if sender.tag == 9 && self.coinType == "2" {
            
            sendSpeedfield.isEnabled = true
            gasLimitField.isEnabled = true
            
        }else if sender.tag == 9 && (self.coinsimbol == "KDA" || self.coinsimbol == "DPN" || self.coinsimbol == "FNK" || self.coinsimbol == "MCVW" || self.coinsimbol == "BTR" || self.coinsimbol == "520" || self.coinsimbol == "EYEAL" || self.coinsimbol == "METAC" || self.coinsimbol == "AGO") {
            
            sendSpeedfield.isEnabled = true
            gasLimitField.isEnabled = true
            
        }else {
            sendSpeedfield.isEnabled = false
            gasLimitField.isEnabled = false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        vitopView.translatesAutoresizingMaskIntoConstraints = false
        setupView()
        initNextbtn()
        setupNaviTitle()
        
        self.view.backgroundColor = UIColor.DPViewBackgroundColor
        self.navigationController?.addShadowToBar(vi: self)
        
        sendFees.text = "\(String(format: "%.8f", 0.0))"
        totalAmount.text = "\(String(format: "%.8f", 0.0))"
        
        if coinType == "2" || coinsimbol == "KDA" || coinsimbol == "DPN" || coinsimbol == "FNK" || coinsimbol == "MCVW" || coinsimbol == "BTR" || coinsimbol == "520" || coinsimbol == "EYEAL" || coinsimbol == "METAC" || coinsimbol == "AGO" {
            self.sendfeeSimoblsLb.text = "ETH"
        }else {
            self.sendfeeSimoblsLb.text = "\(coinsimbol!)"
        }
        
        totalamountLabel.text = "\(coinsimbol!)"
        
        walletPrivatekey = AlertVO.shared.privKey1
        
        manuBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        manuBtn.titleLabel?.sizeToFit()
        
        [maxBtn, addressOkBtn, manuBtn].forEach {
            $0?.layer.borderColor = UIColor.DPLineBgColor.cgColor
            $0?.layer.borderWidth = 1
        }
        
        sendSpeedBtns.forEach {
            $0.layer.borderColor = UIColor.DPLineBgColor.cgColor
            $0.layer.borderWidth = 1
        }
        
        checkBoxBtn.isUserInteractionEnabled = true
        
        let tab = UITapGestureRecognizer(target: self, action: #selector(clickCheckBox))
        checkBoxBtn.addGestureRecognizer(tab)
        
        if coinsimbol != "ETH" && coinType != "2" && coinsimbol != "KDA" && coinsimbol != "DPN" && coinsimbol != "FNK" && coinsimbol != "MCVW" && coinsimbol != "BTR" && coinsimbol != "520" && coinsimbol != "EYEAL" && coinsimbol != "METAC" && coinsimbol != "AGO"{
            
            gasLimitField.isHidden = true
            gasLimitLbel.isHidden = true
            selectorTopValue.constant = 0
            sendSpeedfield.isHidden = true
            
        }else {
            
            sendSpeedfield.isHidden = true
            gasLimitField.isHidden = true
            gasLimitLbel.isHidden = true
            sendSpeedfield.isEnabled = false
            gasLimitField.isEnabled = false
            selectorTopValue.constant = 0
            
        }
        
//        sendQty.addTarget(self, action: #selector(amount), for: .editingDidEndOnExit)
//        gasLimitField.addTarget(self, action: #selector(sendCost), for: .editingChanged)
//        sendSpeedfield.addTarget(self, action: #selector(sendCost), for: .editingChanged)
        
        sendQty.addTarget(self, action: #selector(textfieldChageEvent), for: .editingChanged)
        gasLimitField.addTarget(self, action: #selector(textfieldChageEvent), for: .editingChanged)
        sendSpeedfield.addTarget(self, action: #selector(textfieldChageEvent), for: .editingChanged)
        sendAddress.addTarget(self, action: #selector(textfieldsendChageEvent), for: .editingChanged)
        
        sendQty.delegate = self
        sendAddress.delegate = self
        gasLimitField.delegate = self
        sendSpeedfield.delegate = self
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(taptouchAction))
        contentRow.addGestureRecognizer(tap)
        
        contentRow.text = "send_terms_detail".localized
        
        textfiledView(simName: "GWEI", textFi: self.sendSpeedfield, rigthMode: true)
        
        sendCost()
        
        activity.frame = CGRect(x: self.view.frame.width/2 - 50, y: self.view.frame.height/2 - 100, width: 100, height: 100)
        activity.style = .whiteLarge
        activity.color = UIColor.DPActivityColor
        activity.hidesWhenStopped = true
        
        view.addSubview(activity)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        print("send viewwillappear")
        userWalletInfo()
        QRcode = AppDelegate.walletQrcode
        
        if coinType == "2" || coinsimbol == "KDA" || coinsimbol == "DPN" || coinsimbol == "FNK" || coinsimbol == "MCVW" || coinsimbol == "BTR" || coinsimbol == "520" || coinsimbol == "EYEAL" || coinsimbol == "METAC" || coinsimbol == "AGO" {
            self.ethWalletInfo()
        }
        
        if AppDelegate.mcvmBool {
            
            print("mcvm Bool select send holeding")
            
            self.sendAddress.text = "0xb038a3ff9b42d2cf09dce70f95a8ce8a1b37de83"
            self.sendAddress.textColor = UIColor.DPMinTextColor
            self.sendQty.placeholder = "최소 홀딩수량 2000개"
            
        }
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        print("send viewDidDisappear")
        AppDelegate.walletQrcode = ""
        
    }
    
    func setupView(){
        
//        let headerHeight = (self.navigationController?.navigationBar.frame.height)! + UIApplication.shared.statusBarFrame.height
//
//        vitopView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: headerHeight).isActive = true
//        vitopView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
//        vitopView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
//        vitopView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -(self.tabBarController?.tabBar.frame.height)!).isActive = true
        
    }
    
    
    @IBAction func sendBtnAction(_ sender: Any) {
        
        let ethValue = self.ethWallet["openQty"] as? String ?? "0"
        let dbEthValue = stringtoDouble(str: ethValue) ?? 0
        let sendFeeValue = self.sendFees.text ?? "0"
        let dbSendFee = stringtoDouble(str: sendFeeValue) ?? 0
        let sendValue = stringtoDouble(str: sendQty.text ?? "0.0") ?? 0
        
        if AppDelegate.mcvmBool {
            if sendValue < 2000.0 {
                self.alertMessage(msg: "최소 홀딩수량 2000개 이상 입력해주세요")
            }
        }
        
        if isChecked == false {
            self.alertMessage(msg: "send_checkbox_inform".localized)
            return
            
        }else if (sendSpeedfield.text ?? "") == "" {
            self.alertMessage(msg: "전송 속도를 체크해주세요")
            return
            
        }else if (sendQty.text ?? "") == "" {
            self.alertMessage(msg: "send_qty_blank_inform".localized)
            return
            
        } else if isAddress == false {
            self.alertMessage(msg: "send_address_blank_inform".localized)
            return
            
        } else if dbEthValue < dbSendFee  && coinType == "2"{
            self.alertMessage(msg: "send_transfer_not_enough_eth_inform".localized)//보유하신 이더리움이 전송료보다 적어 전송을 할수 없습니다.
            return
            
        }else if dbEthValue < dbSendFee  && (coinsimbol == "KDA" || coinsimbol == "DPN" || coinsimbol == "FNK" || coinsimbol == "MCVW" || coinsimbol == "BTR" || coinsimbol == "520" || coinsimbol == "EYEAL" || coinsimbol == "METAC" || coinsimbol == "AGO"){
            self.alertMessage(msg: "send_transfer_not_enough_eth_inform".localized)//보유하신 이더리움이 전송료보다 적어 전송을 할수 없습니다.
            return
            
        }else {
            
            let alert = UIAlertController(title: "pop_wallet".localized, message: "send_transfer_inform".localized, preferredStyle: .alert)
            
            let okBtn = UIAlertAction(title: "common_yes_btn".localized, style: .default) { (_) in
                
//                let passPopVc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "sendPassVi") as! SendLockController
                
                if AppDelegate.mcvmBool {
                    
                    self.requstAction()
                    
                }else {
                    let passPopVc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "emailAuthID") as! EmailAuthController
                    
                    passPopVc.authdelegate = self
                    passPopVc.alertBool = true
                    
                    self.addChild(passPopVc)
                    
                    passPopVc.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
                    self.view.addSubview(passPopVc.view)
                    passPopVc.didMove(toParent: self)
                    
                    self.view.endEditing(true)
                }
                
            }
            
            let cancelBtn = UIAlertAction(title: "common_no_btn".localized, style: .cancel, handler: nil)
            
            alert.addAction(okBtn)
            alert.addAction(cancelBtn)
            
            self.present(alert, animated: false, completion: nil)
            
        }
        
    }
    
    //동의 박스 클릭이벤트 메소드
    @objc func clickCheckBox() {
        
        print("tab click ")
        if self.isChecked {
            self.isChecked = false
            print(isChecked)
           // self.checkBoxBtn.image = self.unckeckImg
            
        }else {
            self.isChecked = true
            print(isChecked)
           // self.checkBoxBtn.image = self.checkImg
        }
        
    }
    
    @objc func sendCost(){
        
        var url = ""
        
        if speedBtntap != 9 {
            
            if coinsimbol != "ETH" && coinType != "2" && coinsimbol != "KDA" && coinsimbol != "DPN" && coinsimbol != "FNK" && coinsimbol != "MCVW" && coinsimbol != "BTR" && coinsimbol != "520" && coinsimbol != "EYEAL" && coinsimbol != "METAC" && coinsimbol != "AGO"{
                if self.speedBtntap == 0 {
                    url = "\(AppDelegate.url)/auth/getSendFee?uid=\(AppDelegate.uid)&sessionid=\(AppDelegate.sessionid)&simbol=\(coinsimbol!)&procType=1&calcType=1&gasLimit=0&gwei=0"
                }else{
                    url = "\(AppDelegate.url)/auth/getSendFee?uid=\(AppDelegate.uid)&sessionid=\(AppDelegate.sessionid)&simbol=\(coinsimbol!)&procType=1&calcType=\(speedBtntap)&gasLimit=0&gwei=0"
                }
            }else{
                //            if self.speedBtntap == 0 || (gasLimitField.text ?? "") == ""{
                //                print("no cost~~~~~~~~~~~~~~~~~")
                //                return
                //            } else if self.speedBtntap == 9 && (sendSpeedfield.text ?? "") == ""{
                //                print("no cost~~~~~~~~~~~~~~~~~")
                //
                if self.speedBtntap == 0 {
                    url = "\(AppDelegate.url)/auth/getSendFee?uid=\(AppDelegate.uid)&sessionid=\(AppDelegate.sessionid)&simbol=\(coinsimbol!)&procType=1&calcType=1&gasLimit=\(gasLimitField.text ?? "0")&gwei=\(sendSpeedfield.text ?? "0")"
                    
                }else{
                    url = "\(AppDelegate.url)/auth/getSendFee?uid=\(AppDelegate.uid)&sessionid=\(AppDelegate.sessionid)&simbol=\(coinsimbol!)&procType=1&calcType=\(speedBtntap)&gasLimit=\(gasLimitField.text ?? "0")&gwei=\(sendSpeedfield.text ?? "0")"
                }
                
            }
            
            self.gasLimitField.text = self.gasLimitField.text?.insertComma
            
            let amo = Alamofire.request(url)
            
            amo.responseJSON(completionHandler:
                { response in
                    
                    var amtWon = 0.0
                    //json 파싱
                    guard let val = response.result.value as? NSDictionary else {
                        return
                    }
                    
                    
                    let fee = self.stringtoDouble(str:(val["fee"] as? String) ?? "" ) ?? 0.0
                    
                    //        self.sendQty.text = "\(String(format: "%.8f", qty))"
                    
                    if self.coinType != "2" && self.coinsimbol != "KDA" && self.coinsimbol != "DPN" && self.coinsimbol != "FNK" && self.coinsimbol != "MCVW" && self.coinsimbol != "BTR" && self.coinsimbol != "520" || self.coinsimbol != "EYEAL" && self.coinsimbol != "METAC" && self.coinsimbol != "AGO"{
                        amtWon = fee * (self.stringtoDouble(str: self.lastPrice) ?? 0.0)
                    }else {
                        amtWon = fee * (self.stringtoDouble(str: self.ethLastPrice) ?? 0.0)
                    }
                    
                    let strAmtwon = "\(Int(amtWon.roundToPlaces(places: 0)))".insertComma
                    
                    print("send coin val : \(val)")
                    self.gasLimitField.text = (val["gasLimit"] as? String) ?? ""
                    self.sendSpeedfield.text = (val["gwei"] as? String) ?? ""
                    self.sendFees.text = (val["fee"] as? String) ?? ""
                    self.sendSppedWon.text = "₩ \(strAmtwon)"
                    
                    self.amount()
                    
            })
            
        }else {
            
            let sendSpeed = stringtoDouble(str: self.sendSpeedfield.text ?? "0") ?? 0.0
            let gas = stringtoDouble(str: self.gasLimitField.text ?? "0") ?? 0.0
            
            self.sendFees.text = "\(String(format: "%.8f", ((sendSpeed * gas) / 1000000000.0)))"
            
            self.amount()
            
        }
      
    }
    
    func stringtoDouble(str : String) -> Double?{
        
        let temp = str.components(separatedBy: [","]).joined()
        let double = Double(temp)
        return double ?? 0.0
    }
    
    @objc func amount(){
        
        print("textfil send Qty event")
        var amt = 0.0
        
        let fee = stringtoDouble(str: sendFees.text ?? "" ) ?? 0.0
        let qty = stringtoDouble(str: sendQty.text ?? "") ?? 0.0
        
//        self.sendQty.text = "\(String(format: "%.8f", qty))"
        
        if coinType == "2" || coinsimbol == "KDA" || coinsimbol == "DPN" || coinsimbol == "FNK" || coinsimbol == "MCVW" || coinsimbol == "BTR" || coinsimbol == "520" || coinsimbol == "EYEAL" || coinsimbol == "METAC" || coinsimbol == "AGO"{
            amt = qty
        }else {
            amt = fee + qty
        }
        
        var amtWon = amt * (stringtoDouble(str: self.lastPrice) ?? 0.0)
        let strAmtwon = "\(Int(amtWon.roundToPlaces(places: 0)))".insertComma
        
        self.totalAmount.text = "\(String(format: "%.8f", amt))"
        self.totalAmountWon.text = "₩ \(strAmtwon)"
        
    }
    
    @IBAction func maxCoinACT(_ sender: Any) {
        
        let qty = stringtoDouble(str: walletJson["wthrAbleQty"] as? String ?? "0") ?? 0.0
        let fee = sendFees.text ?? "0"
        let dbFee = stringtoDouble(str: fee) ?? 0.0
        let dbLastprice = stringtoDouble(str: self.lastPrice) ?? 0.0
        
        let amount = qty - dbFee
        
        var totalWon = qty * dbLastprice
        
        
        if coinType == "2" || coinsimbol == "KDA" || coinsimbol == "DPN" || coinsimbol == "FNK" || coinsimbol == "MCVW" || coinsimbol == "BTR" || coinsimbol == "520" || coinsimbol == "EYEAL" || coinsimbol == "METAC" || coinsimbol == "AGO"{
            self.sendQty.text = "\(String(format: "%.8f", qty))"
            self.totalAmount.text = "\(String(format: "%.8f", qty))"
        }else {
            self.sendQty.text = "\(String(format: "%.8f", amount))"
            self.totalAmount.text = "\(String(format: "%.8f", qty))"
            self.totalAmountWon.text = "\(Int(totalWon.roundToPlaces(places: 0)))".insertComma
            
        }
        
    }
    
    @IBAction func addFindAction(_ sender: Any) {
        
        let popOverVc = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "popOverView") as! AddressListController
        popOverVc.coinSimbol = self.coinsimbol ?? ""
        popOverVc.coinType = self.coinType
        popOverVc.delegate = self
        popOverVc.sendAddr = self.sendAddress.text ?? ""
        
        self.addChild(popOverVc)
        popOverVc.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height )
        self.view.addSubview(popOverVc.view)
        popOverVc.didMove(toParent: self)
        
        
//        let contentVC = AddressListController()
//
//        contentVC.preferredContentSize.height = 200
//
//        contentVC.coinSimbol = self.coinsimbol ?? ""
//
//        let alert = UIAlertController(title: "지갑", message: nil, preferredStyle: .alert)
//
//        alert.setValue(contentVC, forKeyPath: "contentViewController")
//
////        let action = UIAlertAction(title: "추가", style: .default) { (_) in
////
////            let name = alert.textFields?[0].text ?? ""
////            let addr = alert.textFields?[1].text ?? ""
////
////            self.addAddress(name: name, address: addr)
////
////        }
//
//        let cancel = UIAlertAction(title: "닫기", style: .cancel, handler: nil)
//
////        alert.addAction(action)
//        alert.addAction(cancel)
//
//        self.present(alert, animated: false, completion: nil)
        
    }
    
    func alertMessage(msg : String){
        let alert = UIAlertController(title: "pop_wallet".localized, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
        self.present(alert, animated: false, completion: nil)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == sendQty {
            
            var allowCharatorSet = CharacterSet.decimalDigits
            let charaterset = CharacterSet(charactersIn: string)
            allowCharatorSet.insert(charactersIn: ".")
            
            return allowCharatorSet.isSuperset(of: charaterset)
            
        }
      
        return true
    }

    
    func initNextbtn(){
        let totalVI = UIView()
        totalVI.backgroundColor = UIColor.clear
        totalVI.frame = CGRect(x: 0, y: 0, width: 70, height: 37)
        totalVI.tintColor = UIColor.DPNaviBartextTintColor
        
        let icon3 = UIImage(named: "qrcodever1")
        
        //알림 리스트 버튼
        let rightbtn3 = UIButton(type: .system)
        rightbtn3.frame = CGRect(x: 35, y: 5, width: 25, height: 25)
        rightbtn3.setBackgroundImage(icon3, for: .normal)
        //rightbtn3.setImage(icon3, for: .normal)
        rightbtn3.addTarget(self, action: #selector(addressCatch), for: .touchUpInside)
        
        // totalVI.addSubview(rightbtn2)
        totalVI.addSubview(rightbtn3)
        
        let item = UIBarButtonItem(customView: totalVI)
        self.navigationItem.rightBarButtonItem = item
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("address touchesBegan ")
        
        sendSpeedBtns.forEach { (button) in
            UIView.animate(withDuration: 0.35, animations: {
                if button.isHidden == false {
                    button.isHidden = true
                    self.view.layoutIfNeeded()
                    
                }
    
            })
            
        }
        
        self.view.endEditing(true)
        
    }
    
    @objc func textfieldChageEvent() {
        print("textfiled change event")
        
        let sendSpeed = self.sendSpeedfield.text ?? "0"
        let gas = self.gasLimitField.text ?? "0"
        
        if coinsimbol == "ETH" {
            if (Int(sendSpeed) ?? 0) > 100 {
                self.sendSpeedfield.text = "99"
            }
            
            if (Int(gas) ?? 0) > 100000 {
                self.gasLimitField.text = "100000"
            }
            
        }
        
        let tempstr = self.sendQty.text!
        let doubleTempStr = stringtoDouble(str: tempstr) ?? 0.0
        let maxDoubleAmount = stringtoDouble(str: walletJson["wthrAbleQty"] as? String ?? "0") ?? 0.0
        
        if doubleTempStr > maxDoubleAmount {
            self.sendQty.text = "\(String(format: "%.8f", maxDoubleAmount))"
        }
        
        let strIndex = self.sendQty.text?.index(of: ".")
        var strLen = ""
        
        if strIndex != nil {
            strLen = String(tempstr.suffix(from: strIndex!))
        }
        
        if strLen.count > 9 {
            let tempsttr = self.stringtoDouble(str: (tempstr.substring(from: 0, to: ((tempstr.count)) - 1))) ?? 0
            sendQty.text = "\(String(format: "%.8f", tempsttr))"
        }
        
        if self.sendQty.text == "." {
            self.sendQty.text = ""
        }
        
        if speedBtntap == 9 {
            self.sendCost()
        }
        
        self.amount()
    }
    
    @objc func textfieldsendChageEvent(){
        let addressCount = sendAddress.text?.count ?? 0
        
        print("wallet shouldChangeCharactersIn count : \(sendAddress.text)")
        
        if addressCount > 20 {
            self.addressfind()
        }else {
            self.sendAddress.textColor = UIColor.DPPlusTextColor
        }
        
    }
    
    @objc func addressCatch() {
       
        let scanner = WalletScanQRController()
        self.present(scanner, animated: false, completion: nil)
        
    }
    
    @objc func taptouchAction(){
        print("button taptouchAction")
        
        self.view.endEditing(true)
        
        sendSpeedBtns.forEach { (button) in
            
            if button.isHidden == false {
                
                UIView.animate(withDuration: 0.35, animations: {
                    button.isHidden = true
                    self.view.layoutIfNeeded()
                })
                
            }
        
        }
        
    }
    
    private func userWalletInfo() {
        
        let url = "\(AppDelegate.url)/auth/userAbleAmount?simbol=\(coinsimbol!)&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)"
        
        print("wallet userAbleAmont : \(url)")
        
        let amo = Alamofire.request(url)
        amo.responseJSON(completionHandler: { response in
            
            print("wallet userAbleAmount")
            self.walletJson = (response.result.value as? [String : Any])!

        })
        
    }
    
    private func ethWalletInfo() {
        print("ethwalletinfo enter")
        
        for row in AppDelegate.walletListArray {
            
            let simbole = row["simbol"] as? String ?? ""
            
            if simbole == "ETH" {
                self.ethWallet = row
            }
        }
        
        print("ethwallet : \(self.ethWallet)")
        
        self.ethLastPrice = self.ethWallet["lastPrice"] as? String ?? "0"
    }
    
    func textfiledView(simName: String, textFi: UITextField, rigthMode : Bool) {
        
        let tfvi = UIView()
        tfvi.backgroundColor = UIColor.clear
        
        let simLable = UILabel()
        simLable.text = simName
        simLable.textColor = UIColor.DPsubTextColor
        simLable.font = UIFont.systemFont(ofSize: 12)
        simLable.adjustsFontSizeToFitWidth = true
        simLable.sizeToFit()
        
        simLable.frame = CGRect(x: 0, y: 0, width: 40, height: textFi.frame.height)
        tfvi.frame = CGRect(x: 0, y: 0, width: 40, height: textFi.frame.height)
        
        tfvi.addSubview(simLable)
        
        
        if rigthMode {
            textFi.rightView = tfvi
            textFi.rightViewMode = .always
            
        }else {
            textFi.leftView = tfvi
            textFi.rightViewMode = .always
        }
    }
    
    func addressfind(){
        
        var url = ""
        
        if coinType == "2" || coinsimbol == "KDA" || coinsimbol == "DPN" || coinsimbol == "FNK" || coinsimbol == "MCVW" || coinsimbol == "BTR" || coinsimbol == "520" || coinsimbol == "EYEAL" || coinsimbol == "METAC" || coinsimbol == "AGO" {
            url = "\(AppDelegate.url)/auth/checkWalletAddr?uid=\(AppDelegate.uid)&sessionid=\(AppDelegate.sessionid)&simbol=ETH&addr=\(sendAddress.text ?? "")"
            print("url : \(url)")
            
        }else {
            url = "\(AppDelegate.url)/auth/checkWalletAddr?uid=\(AppDelegate.uid)&sessionid=\(AppDelegate.sessionid)&simbol=\(coinsimbol!)&addr=\(sendAddress.text ?? "")"
            print("url : \(url)")
        }
        
        let amo = Alamofire.request(url)
        amo.responseString { response in
            guard let result = response.result.value else{
                return
            }
            print("wallet result : \(result)")
                
            if result == "Y" {
                self.isAddress = true
                self.sendAddress.textColor = UIColor.DPMinTextColor
            }else {
                self.isAddress = false
                self.sendAddress.textColor = UIColor.DPPlusTextColor
            }
            
            if AppDelegate.mcvmBool {
                self.isAddress = true
                self.sendAddress.textColor = UIColor.DPMinTextColor
            }
        }
    }
    
    func setupNaviTitle(){
        let vi = UIView()
        
        let nTitle = UILabel(frame: CGRect(x: 0, y: 0, width: 400, height: 37))
        
        nTitle.textAlignment = .center
        nTitle.font = UIFont.boldSystemFont(ofSize: 15)
        nTitle.textColor = UIColor.DPTitleTextColor
        nTitle.text = "\(self.coinName)"
        nTitle.textAlignment = .left
        
        vi.addSubview(nTitle)
        
        self.navigationItem.titleView = vi
        
        self.navigationItem.titleView?.frame.size = CGSize(width: 400, height: 37)
        
        self.navigationController?.navigationBar.barTintColor =  UIColor.DPNaviBarTintColor
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = ""
        
    }
    
    private func addAddress(name: String, address: String) {
        
        let url = "\(AppDelegate.url)/auth/inputAddr?simbol=\(coinsimbol!)&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&inputAddrName=\(name)&inputAddr=\(address)"
        
        print("wallet inputAddr : \(url)")
        
        let amo = Alamofire.request(url)
        amo.responseJSON(completionHandler: { response in
            
            guard let result = response.result.value else{
                return
            }
            print("wallet result : \(result)")
            
        })
        
    }
    
    func sendTakeAction() {
        
        self.activity.startAnimating()
        
        let address = self.sendAddress.text
        let qty = self.sendQty.text
        let fee = self.sendFees.text
        
        let url = URL(string: "\(AppDelegate.url)/auth/walletSend")
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let param = "uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&simbol=\(self.coinsimbol!)&type=2&qty=\(qty ?? "0")&fee=\(fee ?? "0")&waysType=M&addr=\(address ?? "")&privKey=\(self.walletPrivatekey ?? "")&gasLimit=\(self.gasLimitField.text ?? "0")&gwei=\(self.sendSpeedfield.text ?? "0")"
        
        print("wallet send param : \(param)")
        let paramData = param.data(using: .utf8)
        
        request.httpBody = paramData
        
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        request.setValue(String(describing: paramData!.count), forHTTPHeaderField: "Content-Length")
        
        //laucnhing
        let session = URLSession.shared
        
        let task = session.dataTask(with:request as URLRequest) {data, response, error in
            
            if error == nil {
                
                print("buy sueccess")
                DispatchQueue.main.async {
                    
                    //activity.stopAnimating()
                    do {
                        let object = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        
                        guard let jsonObject = object else { return }
                        
                        if !AppDelegate.mcvmBool {
                            self.sendAddress.text = ""
                        }
                        
                        //self.sendAddress.text = ""
                        self.sendQty.text = ""
                        self.gasLimitField.text = ""
                        self.sendSpeedfield.text = ""
                        self.isAddress = false
                        self.isChecked = false
                        self.sendfee = ""
                        self.speedBtntap = 0
                        self.manuBtn.titleLabel?.text = "선택"
                        self.sendFees.text = "\(String(format: "%.8f", 0.0))"
                        self.totalAmount.text = "\(String(format: "%.8f", 0.0))"
                        self.isAddress = false
                        
                        print("jsonobject : \(jsonObject)")
                        
                        let sendResult = jsonObject["returnYn"] as? String ?? "N"
                        
                        
                        print("send result : \(sendResult)")
                        
                        if sendResult == "N" {
                            
                            let msg = jsonObject["returnMsg"] as? String ?? ""
                            let alert = UIAlertController(title: "pop_wallet".localized, message: msg, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: { (_) in
                            }))
                            
                            self.present(alert, animated: false, completion: nil)
                            
                        }else {
                            
                            let msg = jsonObject["returnMsg"] as? String ?? ""
                            let alert = UIAlertController(title: "pop_wallet".localized, message: msg, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: { (_) in
                            }))
                            
                            self.present(alert, animated: false, completion: nil)
                            
                        }
                        
                        self.activity.stopAnimating()
                        
                    }catch{
                        print("cCaught an error:\(error)")
                        
                        let msg = "pop_wallet_send_not_reply".localized
                        let alert = UIAlertController(title: "pop_wallet".localized, message: msg, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
                        self.present(alert, animated: false, completion: nil)
                        
                        self.activity.stopAnimating()
                        
                    }
                }
                
            }else{
                print("error:\(error)")
                let msg = "pop_wallet_send_not_reply".localized
                let alert = UIAlertController(title: "pop_wallet".localized, message: msg, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
                
                self.present(alert, animated: false, completion: nil)
                
                self.activity.stopAnimating()
                
            }
            
        }
        
        task.resume()
        
    }
    
    // 델리게이션 패턴
    func requstAction() {
        self.activity.startAnimating()
        
        let address = self.sendAddress.text
        let qty = self.sendQty.text
        let fee = self.sendFees.text
        var type = 2
        
        if AppDelegate.mcvmBool {
            type = 6
        }
        
        let url = URL(string: "\(AppDelegate.url)/auth/walletSend")
        
        var request = URLRequest(url: url!)
        
        print("wallet send url : \(url)")
        
        request.httpMethod = "POST"
        
        let param = "uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&simbol=\(self.coinsimbol!)&type=\(type)&qty=\(qty ?? "0")&fee=\(fee ?? "0")&waysType=M&addr=\(address ?? "")&privKey=\(self.walletPrivatekey ?? "")&gasLimit=\(self.gasLimitField.text ?? "0")&gwei=\(self.sendSpeedfield.text ?? "0")"
        
        print("wallet send param : \(param)")
        let paramData = param.data(using: .utf8)
        
        request.httpBody = paramData
        
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        request.setValue(String(describing: paramData!.count), forHTTPHeaderField: "Content-Length")
        
        //laucnhing
        let session = URLSession.shared
        
        let task = session.dataTask(with:request as URLRequest) {data, response, error in
            
            if error == nil {
                
                print("buy sueccess")
                DispatchQueue.main.async {
                    
                    //activity.stopAnimating()
                    do {
                        let object = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        
                        guard let jsonObject = object else { return }
                        
                        if !AppDelegate.mcvmBool {
                            self.sendAddress.text = ""
                        }
                        
                        //self.sendAddress.text = ""
                        self.sendQty.text = ""
                        self.gasLimitField.text = ""
                        self.sendSpeedfield.text = ""
                        self.isAddress = false
                        self.isChecked = false
                        self.sendfee = ""
                        self.speedBtntap = 0
                        self.manuBtn.titleLabel?.text = "선택"
                        self.sendFees.text = "\(String(format: "%.8f", 0.0))"
                        self.totalAmount.text = "\(String(format: "%.8f", 0.0))"
                        self.isAddress = false
                        
                        print("jsonobject : \(jsonObject)")
                        
                        let sendResult = jsonObject["returnYn"] as? String ?? "N"
                        
                        
                        print("send result : \(sendResult)")
                        
                        if sendResult == "N" {
                            
                            let msg = jsonObject["returnMsg"] as? String ?? ""
                            let alert = UIAlertController(title: "pop_wallet".localized, message: msg, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: { (_) in
                            }))
                            
                            self.present(alert, animated: false, completion: nil)
                            
                        }else {
                            
                            let msg = jsonObject["returnMsg"] as? String ?? ""
                            let alert = UIAlertController(title: "pop_wallet".localized, message: msg, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: { (_) in
                            }))
                            
                            self.present(alert, animated: false, completion: nil)
                            
                        }
                        
                        self.activity.stopAnimating()
                        
                    }catch{
                        print("cCaught an error:\(error)")
                        
                        let msg = "pop_wallet_send_not_reply".localized
                        let alert = UIAlertController(title: "pop_wallet".localized, message: msg, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
                        self.present(alert, animated: false, completion: nil)
                        
                        self.activity.stopAnimating()
                        
                    }
                }
            }else{
                print("error:\(error)")
                let msg = "pop_wallet_send_not_reply".localized
                let alert = UIAlertController(title: "pop_wallet".localized, message: msg, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
                
                self.present(alert, animated: false, completion: nil)
                
                self.activity.stopAnimating()
                
            }
            
        }
        
        task.resume()
    }
    
}
