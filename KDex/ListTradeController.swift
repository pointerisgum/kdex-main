//
//  ListTradeController.swift
//  crevolc
//
//  Created by crovolc on 2018. 1. 19..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import Kingfisher

class ListTradeController : UIViewController, UITableViewDelegate, UITableViewDataSource, CalendarProtocol, UIPickerViewDelegate, UIPickerViewDataSource {
    
    fileprivate let gregorian = Calendar(identifier: .gregorian)
    
    fileprivate let formatter : DateFormatter = {
       let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    @IBOutlet var pickerViewBackView: UIView!
    
    @IBOutlet var rangeTitleLb: UILabel!
    
    var tradetDropBtnType = ""
    
    var coinDropBtnType = ""
    
    var pickerType = false
    
    var openBool = false
    
    var selectBtnTag = 0 {
        didSet {
            print("selectBtn tag : \(selectBtnTag)")
        }
    }
    @IBOutlet var typePickerVi: UIPickerView!
   
    
    private let tradeType = ["invest_totla_list".localized, "trade_list_spinner_item2".localized, "trade_list_spinner_item3".localized, "trade_list_spinner_item4".localized, "trade_list_spinner_item5".localized]
    
    private let coinType = ["invest_totla_list".localized, "ETH/BTC", "BCH/BTC", "LTC/BTC", "ETC/BTC", "BCH/ETH", "LTC/ETH", "LTC/ETH", "ETC/ETH", "BTC", "ETH", "BCH", "LTC", "ETC", "KDP", "KRW" ]
    
    //검색 높이 조절 레이아웃
    var height = NSLayoutConstraint()
    
    @IBOutlet var searchRangLabel: UILabel!
    
    @IBOutlet var startDateLabel: UILabel!
    
    @IBOutlet var endDateLabel: UILabel!
    
    @IBOutlet var searchViContent: UIView!
    
    @IBOutlet var searchOpenBtn: UIButton!
    
    @IBOutlet var dropBtn: UIButton!
    
    @IBOutlet var rightDropBtn: UIButton!
    
    @IBOutlet var termBtnSt: UIStackView!
    
    @IBOutlet var startDateVi: UIView!
    
    @IBOutlet var endDateVi: UIView!
    
    @IBOutlet var searchBtn: UIButton!
    
    @IBOutlet var andLabel: UILabel!
    
    @IBOutlet var startVi: UIView!
    
    @IBOutlet var endVi: UIView!
    
    @IBOutlet var selectBtns: [UIButton]!
    
    @IBAction func selectBtnsAction(_ sender: UIButton) {
        print("sender tag : \(sender.tag)")
        
        selectBtnTag = sender.tag
        
        selectBtns.forEach { (btn) in
            if btn.tag == sender.tag {
                
                btn.setTitleColor(UIColor.white, for: .normal)
                btn.backgroundColor = UIColor(rgb: 0x00b9b2)
                
            }else {
                
                btn.setTitleColor(UIColor(rgb: 0x666666), for: .normal)
                btn.backgroundColor = UIColor(rgb: 0xeaeaea)
                
            }
            
         }
        
        let endDate = endDateLabel.text ?? ""
        let tempEndDate = calendarDate(str: endDate)
        
        switch sender.tag {
        case 0:
            endDateLabel.text = makeDate(date: Date())
            startDateLabel.text = makeDate(date: Date())
            searchRangLabel.text = "\(startDateLabel.text ?? "") ~ \(endDateLabel.text ?? "")"
            self.chatMoving()
        case 1:
            let tempDate = self.gregorian.date(byAdding: .day, value: -7, to:tempEndDate) ?? Date()
            startDateLabel.text = makeDate(date: tempDate)
            searchRangLabel.text = "\(startDateLabel.text ?? "") ~ \(endDateLabel.text ?? "")"
            self.chatMoving()
        case 2:
            let tempDate = self.gregorian.date(byAdding: .month, value: -1, to:tempEndDate) ?? Date()
            startDateLabel.text = makeDate(date: tempDate)
            searchRangLabel.text = "\(startDateLabel.text ?? "") ~ \(endDateLabel.text ?? "")"
            self.chatMoving()
        case 3:
            let tempDate = self.gregorian.date(byAdding: .month, value: -3, to:tempEndDate) ?? Date()
            startDateLabel.text = makeDate(date: tempDate)
            searchRangLabel.text = "\(startDateLabel.text ?? "") ~ \(endDateLabel.text ?? "")"
            self.chatMoving()
        case 4:
            let tempDate = self.gregorian.date(byAdding: .month, value: -6, to:tempEndDate) ?? Date()
            startDateLabel.text = makeDate(date: tempDate)
            searchRangLabel.text = "\(startDateLabel.text ?? "") ~ \(endDateLabel.text ?? "")"
            self.chatMoving()
        case 5:
            let tempDate = self.gregorian.date(byAdding: .year, value: -1, to:tempEndDate) ?? Date()
            startDateLabel.text = makeDate(date: tempDate)
            searchRangLabel.text = "\(startDateLabel.text ?? "") ~ \(endDateLabel.text ?? "")"
            self.chatMoving()
        default:
            print("default")
        }
        
    }
    
    @IBAction func selectCalendaAction(_ sender: UIButton) {
        
        let popOverVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "calendarPop") as! CalendaController
        popOverVc.delegate = self
        
        popOverVc.startDate = self.calendarDate(str: startDateLabel.text ?? "")
        popOverVc.endDate = self.calendarDate(str: endDateLabel.text ?? "")
        popOverVc.startCompanyDate = self.calendarDate(str: "2010.01.01")
        
        if sender.tag == 11 {
            popOverVc.type = .start
        }else {
            popOverVc.type = .end
        }
        
        self.addChild(popOverVc)
        popOverVc.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        
        self.view.addSubview(popOverVc.view)
        popOverVc.didMove(toParent: self)
        
    }
    
    @IBAction func dropBtnAction(_ sender: UIButton) {
        
        if sender == dropBtn {
            self.pickerType = true
        }else {
            self.pickerType = false
        }
        
        self.typePickerVi.reloadAllComponents()
        
        self.pickerViewBackView.isHidden = false
        self.pickerViewBackView.frame.size.height = 170
     
    }
    
    /////////////
    
    var jsonArray : [[String : Any]]?
    
    let activity = UIActivityIndicatorView()
    
    let footerView: UIView = {
        let uv = UIView()
        uv.backgroundColor = UIColor(rgb: 0xf3f4f5)
        return uv
    }()
    
    let footer_Label : UILabel = {
        let lb = UILabel()
        lb.text = "trade_list_no_list_txt".localized
        lb.textColor = UIColor.DPmainTextColor
        lb.textAlignment = .center
        lb.font = UIFont.systemFont(ofSize: 16)
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    @IBOutlet var listtradeTable: UITableView!
    
    @IBAction func searchOpenBtnAction(_ sender: Any) {
        
        if openBool {
            
            openBool = false
            
            NSLayoutConstraint.deactivate([self.height])
            self.height.constant = 1
            NSLayoutConstraint.activate([self.height])
            
            dropBtn.isHidden = true
            termBtnSt.isHidden = true
            startDateVi.isHidden = true
            endDateVi.isHidden = true
            searchBtn.isHidden = true
            andLabel.isHidden = true
            rightDropBtn.isHidden = true
            
        }else {
            
            openBool = true
            
            NSLayoutConstraint.deactivate([self.height])
            self.height.constant = 128
            NSLayoutConstraint.activate([self.height])
            
            dropBtn.isHidden = false
            termBtnSt.isHidden = false
            startDateVi.isHidden = false
            endDateVi.isHidden = false
            searchBtn.isHidden = false
            andLabel.isHidden = false
            rightDropBtn.isHidden = false
            
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        listtradeTable.allowsSelection = false
        
   
        
        typePickerVi.delegate = self
        typePickerVi.dataSource = self
        typePickerVi.backgroundColor = UIColor(rgb: 0xf3f4f5)
        
        pickerViewBackView.isHidden = true
        
        pickerViewBackView.frame.size.height = 0
        
        endDateLabel.text = makeDate(date: Date())
        startDateLabel.text = makeDate(date: Date())
        
        searchRangLabel.text = "\(startDateLabel.text ?? "") ~ \(endDateLabel.text ?? "")"
        
        selectBtns.forEach { (btn) in
            if btn.tag == 0 {
                
                btn.setTitleColor(UIColor.white, for: .normal)
                btn.backgroundColor = UIColor(rgb: 0x00b9b2)
                
            }else {
                
                btn.setTitleColor(UIColor(rgb: 0x666666), for: .normal)
                btn.backgroundColor = UIColor(rgb: 0xeaeaea)
                
            }
        }
        
        [startVi, endVi].forEach {
            $0?.layer.borderColor = UIColor(rgb: 0xeaeaea).cgColor
            $0?.layer.borderWidth = 1
        }
        
        searchViContent.translatesAutoresizingMaskIntoConstraints = false
        
        if openBool {
            height = searchViContent.heightAnchor.constraint(equalToConstant: 128)
            NSLayoutConstraint.activate([self.height])
            dropBtn.isHidden = false
            termBtnSt.isHidden = false
            startDateVi.isHidden = false
            endDateVi.isHidden = false
            searchBtn.isHidden = false
            andLabel.isHidden = false
            rightDropBtn.isHidden = false
            
            self.view.bringSubviewToFront(searchViContent)
            
        }else {
            
            height = searchViContent.heightAnchor.constraint(equalToConstant: 1)
            NSLayoutConstraint.activate([self.height])
            dropBtn.isHidden = true
            termBtnSt.isHidden = true
            startDateVi.isHidden = true
            endDateVi.isHidden = true
            searchBtn.isHidden = true
            andLabel.isHidden = true
            rightDropBtn.isHidden = true
            
        }
        
        [dropBtn, rightDropBtn].forEach {
            $0?.layer.borderColor = UIColor(rgb: 0xeaeaea).cgColor
            $0?.layer.borderWidth = 1
        }
        
        dropBtn.setTitle("trade_list_spinner_item1".localized + "▼", for: .normal)
        rightDropBtn.setTitle("trade_list_coin_spinner_item1".localized + "▼", for: .normal)
        
        activity.frame = CGRect(x: self.view.frame.width/2 - 50, y: self.listtradeTable.frame.height/2 - 100, width: 100, height: 100)
        activity.style = .whiteLarge
        activity.color = UIColor.DPActivityColor
        activity.hidesWhenStopped = true
        listtradeTable.addSubview(activity)
        
        listtradeTable.backgroundColor = UIColor(rgb: 0xf3f4f5)
        self.view.backgroundColor = UIColor.DPViewBackgroundColor
        
        let nib = UINib(nibName: "ListCellNib", bundle: nil)
        listtradeTable.register(nib, forCellReuseIdentifier: "listcell")
        listtradeTable.layoutMargins = UIEdgeInsets.zero
        listtradeTable.separatorInset = UIEdgeInsets.zero
        listtradeTable.separatorStyle = .none
        
        footerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 100)
        footerView.backgroundColor = UIColor(rgb: 0xf3f4f5)
        
        footerView.addSubview(footer_Label)
        
        setupconstrains()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        chatMoving()
    }
    
    @IBAction func searchBtnAction(_ sender: Any) {
        
        selectBtns.forEach { (btn) in
            
            btn.setTitleColor(UIColor(rgb: 0x666666), for: .normal)
            btn.backgroundColor = UIColor(rgb: 0xeaeaea)
            
        }
        
        self.chatMoving()
        
    }
    
    
    func setupconstrains(){
        self.footer_Label.topAnchor.constraint(equalTo: footerView.topAnchor).isActive = true
        self.footer_Label.leadingAnchor.constraint(equalTo: footerView.leadingAnchor).isActive = true
        self.footer_Label.rightAnchor.constraint(equalTo: footerView.rightAnchor).isActive = true
        self.footer_Label.bottomAnchor.constraint(equalTo: footerView.bottomAnchor).isActive = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return jsonArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listcell", for: indexPath) as? ListCell
        
        cell?.backgroundColor = UIColor(rgb: 0xf3f4f5)
        cell?.selectionStyle = .none
        cell?.layoutMargins = UIEdgeInsets.zero
//        cell?.backgroundColor = UIColor.DPtableCellBgColor
        cell?.isSelector.isHidden = true
        
        cell?.backVi.layer.masksToBounds = true
        cell?.backVi.layer.cornerRadius = 5
        cell?.backBarVi.backgroundColor = UIColor.red
        
        cell?.orderLabel.text = "invest_buy_volume".localized
        cell?.orderPriceLabel.text = "invest_buy_price".localized
        
        guard let tempData = self.jsonArray?[indexPath.row] else {
            return cell!
        }
        cell?.delegate = self
        cell?.tempDate = tempData
        
        let buyType = tempData["tranTp"] as? String ?? ""
        let butTypeTest = tempData["tranTpText"] as? String ?? ""
        let dateStr = tempData["dateTime"] as? String ?? ""
        var simbole = tempData["instCd"] as? String ?? ""
        let mtchPrc = tempData["tranPrc"] as? String ?? "0"
        
        var market = ""
        
        
//        cell?.coinLabel.text = simbole.replacingOccurrences(of: "_", with: "/")
        
        if simbole.contains("_") {
            cell?.selectAddrBtn.isHidden = true
            let simMaket = simbole.components(separatedBy: "_")
            simbole = simMaket[0]
            market = simMaket[1]

        }else {
            cell?.selectAddrBtn.isHidden = false
            market = ""
        }
        
        cell?.simboleImg.kf.setImage(with: AppDelegate.dataImageUrl(str: simbole))
        
        var attr = NSMutableAttributedString(attributedString: NSAttributedString(string: "\(simbole)", attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor(rgb: 0x1b2f42)]))
        
        if market != "" {
            attr.append(NSAttributedString(string: "/\(market)", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12), NSAttributedString.Key.foregroundColor : UIColor(rgb: 0x8c8c8d)]))
        }
        
        cell?.coinLabel.attributedText = attr
        
        let tmepdate = String(dateStr.prefix(14))
        
        
        
        cell?.dateLabel.text = self.makeTime(date: tmepdate)
//        cell?.dateLabel.text = dateStr

        
        var doubleMtcPrc = 0.0
        doubleMtcPrc = stringtoDouble(str: mtchPrc) ?? 0.0
        
        if market == "KRW" || market == "" {
            cell?.orderPriceLabe.text = "\(String(format: "%0.0f", doubleMtcPrc))".insertComma
        }else {
            
            cell?.orderPriceLabe.text = "\(String(format: "%0.8f", doubleMtcPrc))"
        }
        
        cell?.orderCountLabel.text = "\((tempData["tranQty"] as? String) ?? "-")"
        
        cell?.confirmLabel.text = "send_transfer_fee".localized
        cell?.confirmCountLabel.text = "\((tempData["sendFee"] as? String) ?? "-")    "
        
//        cell?.concluCountLabel.text = (tempData["remnQty"] as? String) ?? "0"
        
        cell?.concluLabel.text = "invest_trading_fees".localized
        
        
        if market == "KRW" || market == "" {
            cell?.concluCountLabel.text = "\(self.intToStringComma(data: (tempData["tranFee"] as? String) ?? "0")) KRW"
        }else {
            
            cell?.concluCountLabel.text = "\(self.intToStringComma(data: (tempData["tranFee"] as? String) ?? "0")) KDP"
        }
        
        
        
        let transAmtQty = stringtoDouble(str: tempData["tranAmt"] as? String ?? "0") ?? 0.0
        
        cell?.totalPriceLabel.text = "invest_trading_price".localized
        
        if transAmtQty == 0.0 {
            cell?.totalPriceLabeValue.text = "-"
        }else {
            if market == "KRW" || market == "" {
                cell?.totalPriceLabeValue.text = "\(String(format: "%0.0f", transAmtQty))".insertComma
            }else {
                cell?.totalPriceLabeValue.text = "\(String(format: "%0.8f", transAmtQty))"
            }
            
        }
        
        let tempPlamt = stringtoDouble(str: (tempData["plAmt"] as? String) ?? "0") ?? 0.0
            
        if tempPlamt == 0.0 {
            cell?.benefitLabelValue.text = "-"
        }else {
            cell?.benefitLabelValue.text = "\(self.intToStringComma(data: (tempData["plAmt"] as? String) ?? "-")) KRW"
        }
    
        
        
        cell?.sellBuyLabel.text = butTypeTest
        
        // TB : 매수체결
        // TS : 매도체결
        // TI : 매매입금
        // TO : 매매출금
        // SI : 전송입금
        // SO : 전송출금...
        
        if buyType == "TB" {
            cell?.sellBuyLabel.textColor = UIColor(rgb: 0xe4292b)
            cell?.backBarVi.backgroundColor = UIColor(rgb: 0xe4292b)
        }else if buyType == "TS" {
            cell?.sellBuyLabel.textColor = UIColor(rgb: 0x2c5ac1)
            cell?.backBarVi.backgroundColor = UIColor(rgb: 0x2c5ac1)
        }else if buyType == "TI" || buyType == "SI" {
            cell?.sellBuyLabel.textColor = UIColor(rgb: 0xe9a01b)
            cell?.backBarVi.backgroundColor = UIColor(rgb: 0xe9a01b)
        }else {
            cell?.sellBuyLabel.textColor = UIColor(rgb: 0x465866)
            cell?.backBarVi.backgroundColor = UIColor(rgb: 0x465866)
        }
        //셀간격 테스트
        cell?.frame = (cell?.frame)!.inset(by: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let tempData = self.jsonArray?[indexPath.row] else {
            return
        }
        
        let txAddr = tempData["txAddress"] as? String ?? ""
        let simbole = tempData["instCd"] as? String ?? ""
        
        if simbole.contains("_") {
            print("not wallet trade")
            return
        }
        
        if simbole == "BTC" {
            if let url = URL(string: "\(CoinTxNatwork.BTC.rawValue)\(txAddr)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else if simbole == "ETH" {
            if let url = URL(string: "\(CoinTxNatwork.ETH.rawValue)\(txAddr)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else if simbole == "BCH" {
            if let url = URL(string: "\(CoinTxNatwork.BCH.rawValue)\(txAddr)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else if simbole == "LTC" {
            if let url = URL(string: "\(CoinTxNatwork.LTC.rawValue)\(txAddr)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else if simbole == "ETC" {
            if let url = URL(string: "\(CoinTxNatwork.ETC.rawValue)\(txAddr)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else if simbole == "TRX" {
            if let url = URL(string: "\(CoinTxNatwork.TRX.rawValue)\(txAddr)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else if simbole == "XRP" {
            if let url = URL(string: "\(CoinTxNatwork.XRP.rawValue)\(txAddr)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else if simbole == "QTUM" {
            if let url = URL(string: "\(CoinTxNatwork.QTUM.rawValue)\(txAddr)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else if simbole == "KDA" || simbole == "DPN" || simbole == "FNK" || simbole == "MCVW" || simbole == "BTR"{
            if let url = URL(string: "\(CoinTxNatwork.ETH.rawValue)\(txAddr)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else {
            if let url = URL(string: "\(CoinTxNatwork.ETH.rawValue)\(txAddr)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 110
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 100
    }
    
    //매칭 서비스 데이터 수신
    func chatMoving(){
        //매칭리스트 값 호출
        //auth (로그인 사용자)
        //app (일반사용자)
        activity.startAnimating()
        
        let tempStartDate = formatter.date(from: self.startDateLabel.text ?? "") ?? Date()
        let tempEndDate = formatter.date(from: self.endDateLabel.text ?? "") ?? Date()
        // 데이터 포맷터
        let dateFormatter = DateFormatter()
        // 한국 Locale
        dateFormatter.locale = Locale(identifier: "ko_KR")
        dateFormatter.dateFormat = "yyyyMMdd"
        let startDateStr = dateFormatter.string(from: tempStartDate)
        let endDateStr = dateFormatter.string(from: tempEndDate)
        
        guard let urlComponents = URLComponents(string:"\(AppDelegate.url)auth/transactionHistory??simbol&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&startDt=\(startDateStr)&endDt=\(endDateStr)&tranTp=\(self.tradetDropBtnType)&instCd=\(self.coinDropBtnType)") else {
            return
        }
        
        print("urlComponents : \(urlComponents)")
        
        let amo = Alamofire.request(urlComponents)
        amo.responseJSON(completionHandler:
            { response in
                
  
                guard let tempjson = response.result.value as? [[String : Any]] else {
                    return
                }
                
                var tempValue = [[String : Any]]()
                
                for row in tempjson {
                    
                    var simbole = row["instCd"] as? String ?? ""
                    
                    if !simbole.contains("\(AppDelegate.removeCoin)") {
                        tempValue.append(row)
                    }
                    
                }
                
                self.jsonArray = tempValue
                
                let arr = self.jsonArray?.sorted(by: {
                    
                    guard let num1 = $0["dateTime"] as? String else {
                        return true
                    }
                    
                    guard let num2 = $1["dateTime"] as? String else {
                        return true
                    }
                    
                    return num1 > num2
                    
                })
                
                self.jsonArray = arr
                
                self.activity.stopAnimating()
                
                //테이블 리로드
                self.listtradeTable.reloadData()
                
                if self.jsonArray?.count == 0 {
                    self.footer_Label.isHidden = false
                    self.footerView.isHidden = false
                    
                }else {
                    self.footer_Label.isHidden = true
                    self.footerView.isHidden = true
                }
                
        })
        
    }
    
//    날짜 형식 포멧
    func makeTime(date : String) -> String {

        let df : DateFormatter = DateFormatter()

        df.dateFormat = "yyyyMMddHHmmss"

//        let date_time : NSDate = (df.date(from: date) as NSDate?)!
        let date_time = df.date(from: date)

        df.dateFormat = "yy.MM.dd HH:mm:ss"

//        let newDate : String = df.string(from: date_time as! Date) as String
        let newDate : String = df.string(from: date_time!)

        return newDate
    }

    func stringtoDouble(str : String) -> Double?{
        let temp = str.components(separatedBy: [","]).joined()
        let double = Double(temp)
        return double ?? 0.0
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        if targetContentOffset.pointee.y == 0.0 && targetContentOffset.pointee.y > scrollView.contentOffset.y {
            chatMoving()
        }
        
    }
    
    func intToStringComma(data : String) -> String {
        
        let doubleData = stringtoDouble(str: data) ?? 0.0
        let intData = Int(doubleData)
        
        let strData = "\(intData)"
        
        return strData.insertComma
        
    }
    
    func dateLabelEdit(date: Date, type: calendarType) {
        
        if type == .start {
            startDateLabel.text = makeDate(date: date)
            searchRangLabel.text = "\(makeDate(date: date)) ~ \(endDateLabel.text ?? "")"
        }else {
            endDateLabel.text = makeDate(date: date)
            searchRangLabel.text = "\(startDateLabel.text ?? "") ~ \(makeDate(date: date))"
        }
        
    }
    
    func makeDate(date : Date) -> String {
        
        let df : DateFormatter = DateFormatter()
        df.dateFormat = "yyyy.MM.dd"
        
        let strDate = df.string(from: date) as String
        
        return strDate
    }
    
    func calendarDate(str : String) -> Date {
        
        var strDate = str
        
        strDate.replace(originalString: ".", withString: "-")
        
        let tempdate = formatter.date(from: strDate) ?? Date()
        
        return tempdate
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerType {
            return tradeType.count
        }else {
            return coinType.count
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerType {
            return tradeType[row]
        }else {
            return coinType[row]
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerType {
            self.dropBtn.setTitle(tradeType[row], for: .normal)
            
            switch row {
            case 1 :
                self.tradetDropBtnType = "B"
            case 2 :
                self.tradetDropBtnType = "S"
            case 3 :
                self.tradetDropBtnType = "I"
            case 4 :
                self.tradetDropBtnType = "O"
            default :
                self.tradetDropBtnType = ""
            }
            
        }else {
            self.rightDropBtn.setTitle(coinType[row], for: .normal)
            
            var tempCoin = coinType[row]
            
            tempCoin.replace(originalString: "/", withString: "_")
            
            if tempCoin == "전체" {
                tempCoin = ""
            }
            
            self.coinDropBtnType = tempCoin
        }
        
    }
   
    @IBAction func pickerViewCloseAction(_ sender: Any) {
        pickerViewBackView.isHidden = true
        pickerViewBackView.frame.size.height = 0
    }
    
    func walletAddressCallUrl(data : [String : Any]){
        
        let txAddr = data["txAddress"] as? String ?? ""
        let simbole = data["instCd"] as? String ?? ""
        
        if simbole.contains("_") {
            print("not wallet trade")
            return
        }
        
        if simbole == "BTC" {
            if let url = URL(string: "\(CoinTxNatwork.BTC.rawValue)\(txAddr)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else if simbole == "ETH" {
            if let url = URL(string: "\(CoinTxNatwork.ETH.rawValue)\(txAddr)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else if simbole == "BCH" {
            if let url = URL(string: "\(CoinTxNatwork.BCH.rawValue)\(txAddr)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else if simbole == "LTC" {
            if let url = URL(string: "\(CoinTxNatwork.LTC.rawValue)\(txAddr)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else if simbole == "ETC" {
            if let url = URL(string: "\(CoinTxNatwork.ETC.rawValue)\(txAddr)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else if simbole == "TRX" {
            if let url = URL(string: "\(CoinTxNatwork.TRX.rawValue)\(txAddr)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else if simbole == "XRP" {
            if let url = URL(string: "\(CoinTxNatwork.XRP.rawValue)\(txAddr)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else if simbole == "QTUM" {
            if let url = URL(string: "\(CoinTxNatwork.QTUM.rawValue)\(txAddr)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else if simbole == "KDA" || simbole == "DPN" || simbole == "FNK" || simbole == "MCVW" || simbole == "BTR" {
            if let url = URL(string: "\(CoinTxNatwork.ETH.rawValue)\(txAddr)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }else {
            if let url = URL(string: "\(CoinTxNatwork.ETH.rawValue)\(txAddr)" ) {
                UIApplication.shared.open(url, options: [:])
            }
        }
    }
}

//enum TransType : String {
//
//    case TB = "매수체결"
//    case TS = "매도체결"
//    case TI = "매매입금"
//    case TO = "매매출금"
//    case SI = "전송입금"
//    case SO = "전송출금"
//    case EI = "교환입금"
//    case EO = "교환출금"
//    case PI = "포인트입금"
//    case PO = "포인트출금"
//    case CI = "현금입금"
//    case CO = "현금출금"
//    case SI = "대체입금"
//    case SO = "대체출금"
//    case XI = "기타입금"
//    case XO = "기타출금"
//
//}
