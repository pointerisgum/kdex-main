
//
//  WalletPageViewController.swift
//  crevolc
//
//  Created by crovolc on 2018. 1. 9..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//
import UIKit
import Foundation
import Alamofire

class WalletPageViewController : PagerController, PagerDataSource{
    
    var coinsimbol = ""
    var coinName = ""
    var walletaddr = ""
    var coinValue = ""
    var lastPrice = ""
    var coinType = ""

    override func viewDidLoad() {
        
        print("wallet selectede simbol : \(coinsimbol)")
        
        if coinType == "2" {
            DBManager.sharedInstance.selecttWalletSimbol(simbol: "ETH")
        }else {
            DBManager.sharedInstance.selecttWalletSimbol(simbol: "\(coinsimbol)")
        }
     
        print("wallet selectede simbol privatekey: \(AlertVO.shared.privKey1)")
        
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        
        self.dataSource = self
        
        registerStoryboardControllers()
        
        customizeTab()
        
        initNextbtn()
        
        walletInfo()
        
        print("wallet pageviewcontroller : \(coinsimbol), \(AlertVO.shared.privKey1), \(walletaddr), \(coinValue), \(lastPrice)")
    }
    
    func initNextbtn(){
        let vi = UIView()
        
        let nTitle = UILabel(frame: CGRect(x: 0, y: 0, width: 400, height: 37))
        
        nTitle.textAlignment = .center
        nTitle.font = UIFont.boldSystemFont(ofSize: 15)
        nTitle.textColor = UIColor.DPTitleTextColor
        nTitle.text = "\(self.coinName)"
        nTitle.textAlignment = .left
        
        vi.addSubview(nTitle)
        
        self.navigationItem.titleView = vi
        
        self.navigationItem.titleView?.frame.size = CGSize(width: 400, height: 37)
        
        self.navigationController?.navigationBar.barTintColor =  UIColor.DPNaviBarTintColor
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationController?.navigationBar.topItem?.title = ""
        
        self.navigationController?.addShadowToBar(vi: self)
        
    }
    
    //탭 컨트롤러 설정
    func registerStoryboardControllers() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let exchanetitle = "wallet_exchange_is".localized
        
//        if coinsimbol == "KDP" {
//            exchanetitle = "충전하기"
//        }
        
        let controller1 = storyboard.instantiateViewController(withIdentifier: "wallet")
        let controller2 = storyboard.instantiateViewController(withIdentifier: "walletExchage")
        
        self.setupPager(tabNames: ["wallet_manage_is".localized, exchanetitle], tabControllers: [controller1, controller2])
        
        //첫번째 탭위치 컨트롤러 설정
        if let controller = controller1 as? WalletViewController {
            controller.coinSimbol = coinsimbol
            controller.coinType = coinType
//            controller.walletAddr = walletaddr
             print("wallet pageviewcontroller : \(coinsimbol), \(coinValue), \(lastPrice), \(walletaddr)")
            
            //controller.finAmount = "\(coinsimbol) \(coinValue)"
            
            //let inttempKrwvalue = Int(stringtoDouble(str: lastPrice) ?? 0.0)
            
            //let tempKrwStr = "\(inttempKrwvalue)".insertComma
            
            controller.krwAmount = lastPrice
            
            //보내기 버튼 클릭시 이동 컨틀로러 설정
            controller.sendAT = { ()in
                
//                if self.coinsimbol == "KDP" || self.coinsimbol == "KDA"{
                if self.coinsimbol == "KDP" || self.coinsimbol == "KDMP"{
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    if let detail = storyboard.instantiateViewController(withIdentifier: "walletExchage") as? WalletExchageViewController {
                        
                        detail.exchangeIsBool = false
                        detail.coinsimbol = self.coinsimbol
                        detail.coinName = self.coinName
                        
                        self.navigationController?.pushViewController(detail, animated: true)
                    }
                    
                }else {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    if let detail = storyboard.instantiateViewController(withIdentifier: "Walletsend") as? WalletSendViewController {
                        detail.walletPrivatekey = AlertVO.shared.privKey1
                        detail.coinsimbol = self.coinsimbol
                        detail.walletAddress = self.walletaddr
                        detail.coinName = self.coinName
                        detail.coinType = self.coinType
                        detail.lastPrice = self.lastPrice
                        
                        self.navigationController?.pushViewController(detail, animated: true)
                    }
                    
                }
            }
            
            //받기 버튼 클릭시 이동 컨틀로러 설정
            controller.reciveAT = { () in
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                if let detail = storyboard.instantiateViewController(withIdentifier: "WalletReceive") as? WalletReceiveViewController {
                    detail.walletPrivatekey = AlertVO.shared.privKey1
                    detail.coinsimbol = self.coinsimbol
                    detail.walletAddress = self.walletaddr
                    detail.coinName = self.coinName
                    detail.coinType = self.coinType
                    
                    self.navigationController?.pushViewController(detail, animated: true)
                    
                }
            }
        }
        
        //두번째 탭위치 컨트롤러 설정
        if let controller = controller2 as? WalletExchageViewController {
            print("wallet exchange intro ")
            
            controller.coinsimbol = coinsimbol
            controller.exchangeIsBool = true
            
            if coinsimbol != "KDP" && coinsimbol != "KDMP"{
                controller.lastprice = lastPrice
            }
        }
    }
    
    func customizeTab() {
        
        indicatorColor = UIColor.white
        tabsViewBackgroundColor = UIColor.DPtableViewBgColor
        contentViewBackgroundColor = UIColor.DPtableViewBgColor
        
        startFromSecondTab = false
        centerCurrentTab = true
        tabLocation = PagerTabLocation.top
        tabHeight = 49
        tabOffset = 36
        tabWidth = self.view.frame.size.width / 2
        fixFormerTabsPositions = false
        fixLaterTabsPosition = false
        tabBarHidden = true
        animation = PagerAnimation.during
        selectedTabTextColor = UIColor.DPmainTextColor
        tabsTextFont = UIFont.boldSystemFont(ofSize: 15)
        tabsTextColor = UIColor.DPsubTextColor
        
    }
    
    func changeTab() {
        self.selectTabAtIndex(2)
    }
    
    func walletInfo() {
        let activity = UIActivityIndicatorView()
        activity.style = .whiteLarge
        activity.color = UIColor.DPActivityColor
        activity.hidesWhenStopped = true
        activity.startAnimating()
        
        var url = ""
        
        if coinType == "2" || coinsimbol == "KDA" || coinsimbol == "DPN" || coinsimbol == "FNK" || coinsimbol == "MCVW" || coinsimbol == "BTR" || coinsimbol == "520" || coinsimbol == "EYEAL" || coinsimbol == "METAC" || coinsimbol == "AGO" {
            url = "\(AppDelegate.url)/auth/walletInfo?simbol=ETH&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)"
        }else {
            url = "\(AppDelegate.url)/auth/walletInfo?simbol=\(coinsimbol)&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)"
        }
        
        print("wallet info : \(url)")
        
        let amo = Alamofire.request(url)
        
        amo.responseJSON(completionHandler: { response in
            
            print("buy result :\(response.result.value)")
            
            if response.result.isSuccess == true {
                
                guard var result = response.result.value as? [String : Any] else {
                    return
                }
                
                AppDelegate.myWalletInfo = result

                self.walletaddr = (result["wletAddr"] as? String) ?? ""
                
            }
            
            activity.stopAnimating()
            
        })
    }
    
    func stringtoDouble(str : String) -> Double?{
        let temp = str.components(separatedBy: [","]).joined()
        let double = Double(temp)
        return double ?? 0.0
    }
    
    
    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        if coinType == "2" || coinsimbol == "KDP" || coinsimbol == "KRW" || coinsimbol == "KDMP"{
            scrollView.isScrollEnabled = false
        }
        
    }
    
}


