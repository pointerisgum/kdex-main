//
//  FindAccountController.swift
//  KDex
//
//  Created by park heewon on 2018. 8. 29..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//
// 이메일 찾기 페이지 (웹뷰작업)

import Foundation
import WebKit

class FindAccountController: UIViewController, WKUIDelegate, WKNavigationDelegate, WKScriptMessageHandler{
   
    var url = ""
    var titl = ""
    
    @IBOutlet var webView: WKWebView!
    
    var activityIndicator : UIActivityIndicatorView = UIActivityIndicatorView()
    
    @IBOutlet var titlLabel: UILabel!
    @IBOutlet var topVi: UIView!
    
    override func loadView() {
        super.loadView()
        
//        webView = WKWebView()
//        webView.translatesAutoresizingMaskIntoConstraints = false
//
//        webView.topAnchor.constraint(equalTo: topVi.bottomAnchor).isActive = true
//        webView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
//        webView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
//        webView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
        webView.uiDelegate = self
        webView.navigationDelegate = self
        
        titlLabel.text = titl
        titlLabel.sizeToFit()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let urlValue = URL(string: self.url)
        
        let request = URLRequest(url: urlValue!)
        
        webView.load(request)
        
    }
    
    @IBAction func cancelBtnAction(_ sender: Any) {
        self.presentingViewController?.dismiss(animated: false, completion: nil)
    }
    
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        
        print("webView Error : \(error)")
    }
    
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
         //script에서 ios zhem
    }
    
    @available(iOS 8.0, *)
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping () -> Void) {
        
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "확인", style: .default, handler: { (action) in
            completionHandler()
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @available(iOS 8.0, *)
    func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping (Bool) -> Void) {
        
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "확인", style: .default, handler: { (action) in
            completionHandler(true)
        }))
        alertController.addAction(UIAlertAction(title: "취소", style: .default, handler: { (action) in
            completionHandler(false)
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @available(iOS 8.0, *)
    func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping (String?) -> Void) {
        let alertController = UIAlertController(title: "", message: prompt, preferredStyle: .alert)
        alertController.addTextField { (textField) in
            textField.text = defaultText
        }
        alertController.addAction(UIAlertAction(title: "확인", style: .default, handler: { (action) in
            if let text = alertController.textFields?.first?.text {
                completionHandler(text)
            } else {
                completionHandler(defaultText)
            }
        }))
        
        alertController.addAction(UIAlertAction(title: "취소", style: .default, handler: { (action) in
            completionHandler(nil)
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @available(iOS 8.0, *)
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
        activityIndicator.frame = CGRect(x: view.frame.midX-50, y: view.frame.midY-50, width: 100, height: 100)
        activityIndicator.style = .whiteLarge
        activityIndicator.color = UIColor.DPCompanyColor
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        
    }
    
    @available(iOS 8.0, *)
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.activityIndicator.removeFromSuperview()
    }
    
    public func webViewWebContentProcessDidTerminate(_ webView: WKWebView) {
        // 중복적으로 리로드가 일어나지 않도록 처리 필요.
        self.webView.reload()
    }
    
}
