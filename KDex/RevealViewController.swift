//
//  RevealViewController.swift
//  crevolc
//
//  Created by crovolc on 2017. 12. 26..
//  Copyright © 2017년 hanbiteni. All rights reserved.
//
// main 컨테이너 컨트롤러

import Foundation
import UIKit
import Alamofire

class RevealViewController: UIViewController {
    
    var exchangListMager = ExchangCoinListManager()
    
    var pageNum = 0
    
    var contentVC : UIViewController?
    
    var sideVC : UIViewController?
    
    var isSideBarShowing = false
    
    let SLIDE_TIME : TimeInterval = 0.3
    
    var SIDEBAR_WIDTH : CGFloat = 0
    
    override func viewDidLoad() {
        print("revealview controller viewdidload")
        super.viewDidLoad()
        exchangListMager.coinAllList(market: " ")
        exchangListMager.favList()
        setupUserInfor()
        self.setupView(num : pageNum)
       
        SIDEBAR_WIDTH = -(self.view.frame.width)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        TcpSocket.sharedInstance.connect(host: AppDelegate.realUrl, port: AppDelegate.realPort, uid: AppDelegate.uid)
    }
    
    func setupUserInfor(){
        let plist = UserDefaults.standard
        
        var urlComponents = URLComponents(string:"\(AppDelegate.url)app/getUserInfo?uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)")!
        print("url : \(urlComponents)")
        
        let amo = Alamofire.request(urlComponents)
        amo.responseJSON(completionHandler:
            { response in
                
                //json 파싱
                guard let val = response.result.value as? NSDictionary else {
                    return
                }
        
                AppDelegate.userName = val["userName"] as? String ?? ""
                AppDelegate.userEmail = val["userEmail"] as? String ?? ""
                AppDelegate.userPhoneNumber = val["userMobile"] as? String ?? ""
                AppDelegate.userSecretLevel = val["userLevel"] as? Int ?? 1
                AppDelegate.userBankName = val["bankName"] as? String ?? ""
                AppDelegate.bankAccountNo = val["bankAccountNo"] as? String ?? ""
                
                
                plist.set(val["userEmail"] as? String ?? "", forKey: "saveUserEmail")
                plist.synchronize()
                
        })
        
    }

    func setupView(num : Int){
        let storybd = UIStoryboard(name: "Main", bundle: nil)
        
        if let vc = storybd.instantiateViewController(withIdentifier: "sw_front") as? UITabBarController {
            
            //탭바 활성화된 아이콘 색깔 변경
            vc.tabBar.tintColor = UIColor.DPTabBarIndicateTintColor
            vc.tabBar.layer.masksToBounds = false
            vc.tabBar.layer.shadowRadius = 0
            vc.tabBar.layer.shadowColor = UIColor.DPdefaultBackgroundColor.cgColor
            vc.tabBar.layer.shadowOffset = CGSize(width: 0, height: -2)
            vc.tabBar.layer.shadowOpacity = 1
            vc.selectedIndex = num
              
            self.contentVC = vc
            self.addChild(vc)
            self.view.addSubview(vc.view)
            vc.didMove(toParent: self)
            
           var loopNum = 0
            
            for num in vc.viewControllers! {
                
                let nextNC = num as? UINavigationController
                
                switch loopNum {
                    case 0 :
                        print("0")
                        let firstVC = nextNC?.viewControllers[0] as? ViewController
                        firstVC?.revealDelegate = self
                    case 1 :
                        print("1")
                        let secondVC = nextNC?.viewControllers[0] as? SecondViewController
                        secondVC?.revealDelegate = self
                    case 2 :
                        print("2")
                        let thirdVC = nextNC?.viewControllers[0] as? ThirdViewController
                        thirdVC?.revealDelegate = self
                    case 3 :
                        print("3")
                        let fourthVC = nextNC?.viewControllers[0] as? FourthViewController
                        fourthVC?.revealDelegate = self
                    case 4 :
                        print("4")
                        let fifthVC = nextNC?.viewControllers[0] as? FifthViewController
                        fifthVC?.revealDelegate = self
                    default :
                        let otherVC = nextNC?.viewControllers[0]

                }
                loopNum += 1
            }
           
            print("setup end")
            
        }
    }
    
    func getSideView(){
        if self.sideVC == nil {
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "sw_rear") as? SideBarViewController{
                
                self.sideVC = vc
                self.addChild(vc)
                vc.revealDelegate = self
                vc.view.frame = CGRect(x: self.SIDEBAR_WIDTH, y: 0, width: 260, height: self.view.frame.height)
            
                self.view.addSubview(vc.view)
                vc.didMove(toParent: self)
            }
        }
    }
    
    func openSideBar(_ complete: ( () -> Void)? ){
        self.getSideView()
        //self.setShadowEffect(shadow: true, offset: 2)
        
        let options = UIView.AnimationOptions([.curveEaseOut, .beginFromCurrentState])
        
        UIView.animate(withDuration: TimeInterval(self.SLIDE_TIME), delay: TimeInterval(0), options: options, animations: {
            self.sideVC?.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        }, completion: {
            if $0 == true {
                self.isSideBarShowing = true
                complete?()
            }
        })
    }
    
    //사이드 바를 닫는다
    func closeSideBar(_ complete: ( () -> Void)? ){
        let options = UIView.AnimationOptions([.curveEaseIn, .beginFromCurrentState])
        
        UIView.animate(withDuration: TimeInterval(self.SLIDE_TIME), delay: TimeInterval(0), options: options, animations: {
            self.sideVC?.view.frame = CGRect(x: self.SIDEBAR_WIDTH, y: 0, width: 260, height: self.view.frame.height)
            
        }, completion: {
            if $0 == true {
                self.sideVC?.view.removeFromSuperview()
                self.sideVC = nil
                self.isSideBarShowing = false
               // self.setShadowEffect(shadow: false, offset: 0)
                complete?()
                
                if AppDelegate.sideBarInfoBool {
                    self.setupView(num: 4)
                    AppDelegate.sideBarInfoBool = false
            
                }
                
            }
        })
    }
    
    func closetest(){
        self.sideVC?.view.frame = CGRect(x: self.SIDEBAR_WIDTH, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        self.sideVC?.view.removeFromSuperview()
        self.sideVC = nil
        self.isSideBarShowing = false
    }
    
}
