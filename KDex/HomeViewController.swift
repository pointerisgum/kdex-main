//
//  ExchangListKRWcontroller.swift
//  crevolc
//
//  Created by crovolc on 2017. 12. 28..
//  Copyright © 2017년 hanbiteni. All rights reserved.
//
// KRW 거래소 리스트 페이지

import UIKit
import SystemConfiguration
import Alamofire

class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, RealTimeDelegate {
    
    private var realTimeSelect = false
    private var titleSortBool = true
    private var volSortBool = true
    private var priceSortBool = true
    private var rateSortBool = true
    
    private let maket = "KRW"
    
    var subscribeRS : NTcsReal = NTcsReal()
    
    var exchangListMager = ExchangCoinListManager()
    
    var timer : Timer? = Timer()
    
    let appID = Bundle.bundleIdentifier
    
    var didSelectRow: ((String, String, String) -> ())?
    
    var counter = 0
    
    var imgUrl = [
        "https://kdex.io/images/slide1_m.jpg",
        "https://kdex.io/images/slide2_m.jpg",
        "https://kdex.io/images/slide3_m.jpg"
    ]
    
    //table header start -------------------
    
    let headerView : UIView = {
        let uv = UIView()
        uv.backgroundColor = UIColor.white
        return uv
    }()
    
    let bottombarVi : UIView = {
        let vi = UIView()
        vi.backgroundColor = UIColor(rgb: 0xcacacd)
        vi.translatesAutoresizingMaskIntoConstraints = false
        return vi
    }()
    
    let favorImg : UIImageView = {
        let image = UIImage(named: "star_on")
        let img = UIImageView(image: image)
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    let AsortBtn : UIButton = {
        let bt = UIButton()
        let iamge = UIImage(named: "icn_list_sort_deactive-1")
        bt.backgroundColor =  UIColor.clear
        bt.translatesAutoresizingMaskIntoConstraints = false
        bt.setAttributedTitle(NSAttributedString(string: "trade_market_coin_name".localized, attributes:[NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.DPmainTextColor]), for: .normal)
        if AppDelegate.appLang == "en_US"{
            bt.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -55)
        }else {
            bt.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -75)
        }
        bt.titleEdgeInsets = UIEdgeInsets(top: 0, left: -40, bottom: 0, right: 0)
        bt.setImage(iamge, for: .normal)
        bt.addTarget(self, action: #selector(titleSort), for: .touchUpInside)
        return bt
    }()
    
    let BsortBtn : UIButton = {
        let bt = UIButton()
        let iamge = UIImage(named: "icn_list_sort_deactive-1")
        bt.translatesAutoresizingMaskIntoConstraints = false
        bt.setAttributedTitle(NSAttributedString(string: "trade_market_rate".localized, attributes:[NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.DPmainTextColor]), for: .normal)
        if AppDelegate.appLang == "en_US"{
            bt.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -65)
        }else {
            bt.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -70)
        }
        bt.titleEdgeInsets = UIEdgeInsets(top: 0, left: -45, bottom: 0, right: 0)
        bt.setImage(iamge, for: .normal)
        bt.addTarget(self, action: #selector(volSort), for: .touchUpInside)
        bt.backgroundColor =  UIColor.clear
        
        return bt
    }()
    
    let CsortBtn : UIButton = {
        let bt = UIButton()
        let iamge = UIImage(named: "icn_list_sort_deactive-1")
        bt.translatesAutoresizingMaskIntoConstraints = false
        bt.setAttributedTitle(NSAttributedString(string: "trade_market_last_price".localized, attributes:[NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.DPmainTextColor]), for: .normal)
        bt.setImage(iamge, for: .normal)
        if AppDelegate.appLang == "en_US"{
            bt.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -125)
        }else {
            bt.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -70)
        }
        bt.titleEdgeInsets = UIEdgeInsets(top: 0, left: -45, bottom: 0, right: 0)
        bt.addTarget(self, action: #selector(priceSort), for: .touchUpInside)
        bt.backgroundColor = UIColor.clear
        return bt
    }()
    
    let DsortBtn : UIButton = {
        let bt = UIButton()
        let iamge = UIImage(named: "icn_list_sort_deactive-1")
        bt.translatesAutoresizingMaskIntoConstraints = false
        bt.setAttributedTitle(NSAttributedString(string: "trade_market_yesterday".localized, attributes:[NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.DPmainTextColor]), for: .normal)
        bt.setImage(iamge, for: .normal)
        if AppDelegate.appLang == "en_US"{
            bt.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -90)
        }else {
            bt.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -110)
        }
        bt.titleEdgeInsets = UIEdgeInsets(top: 0, left: -30, bottom: 0, right: 0)
        bt.addTarget(self, action: #selector(rateSort), for: .touchUpInside)
        bt.backgroundColor =  UIColor.clear
        return bt
    }()
    
    //table header end -------------------
    
    @IBOutlet weak var notiBackVi: UIView!
    
    @IBOutlet weak var notiTextField: UILabel!
    
    @IBOutlet weak var closeBtn: UIButton!
    
    @IBOutlet var activity: UIActivityIndicatorView!
    
    @IBOutlet weak var pageView: UIPageControl!
    
    @IBOutlet weak var sliderCollectionView: UICollectionView!
    
    @IBOutlet weak var coinListTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.alertList(uid: AppDelegate.uid, sessionid: AppDelegate.sessionid, type: .all)
        
        [self.view, self.notiBackVi].forEach { $0?.backgroundColor = UIColor.DPNaviBarTintColor }
        
        
        let tapEvet = UITapGestureRecognizer(target: self, action: #selector(notiAction))
        
        self.notiTextField.isUserInteractionEnabled = true
        self.notiTextField.addGestureRecognizer(tapEvet)
        
        
        exchangListMager.searchcoinList(market: self.maket, tableview: self.coinListTableView, activity: self.activity, search: "", first: false)
        exchangListMager.tempArray.removeAll()
        
        headerSetup()
        
        self.coinListTableView.backgroundColor = UIColor(rgb: 0xf3f4f5)
        
        self.coinListTableView.layoutMargins = UIEdgeInsets.zero
        self.coinListTableView.separatorInset = UIEdgeInsets.zero
        self.coinListTableView.separatorStyle = .none
        
        self.subscribeRS.subscribeRealSise(uid: AppDelegate.uid, symbol: " ", outputStream: TcpSocket.sharedInstance.outputStream!)
        
        let nib = UINib(nibName: "ExchangListNib", bundle: nil)
        coinListTableView.register(nib, forCellReuseIdentifier: "newExchangCell")
        
        titleSort()

        
        pageView.numberOfPages = 3
        pageView.currentPage = 0
        
        DispatchQueue.main.async {
//            self.timer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.changeImage), userInfo: nil, repeats: true)
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.timer?.invalidate()
        self.timer = nil
        NotificationCenter.default.removeObserver(self)
    }
    
    fileprivate func headerSetup() {
        headerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 40)
        
        //        headerView.addSubview(favorImg)
        headerView.addSubview(bottombarVi)
        headerView.addSubview(AsortBtn)
        headerView.addSubview(BsortBtn)
        headerView.addSubview(CsortBtn)
        headerView.addSubview(DsortBtn)
        
        bottombarVi.bottomAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
        bottombarVi.leadingAnchor.constraint(equalTo: headerView.leadingAnchor).isActive = true
        bottombarVi.trailingAnchor.constraint(equalTo: headerView.trailingAnchor).isActive = true
        bottombarVi.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        //        favorImg.leftAnchor.constraint(equalTo: headerView.leftAnchor, constant: 10).isActive = true
        //        favorImg.widthAnchor.constraint(equalToConstant: 20).isActive = true
        //        favorImg.heightAnchor.constraint(equalToConstant: 20).isActive = true
        //        favorImg.centerYAnchor.constraint(lessThanOrEqualTo: headerView.centerYAnchor).isActive = true
        //        AsortBtn.leftAnchor.constraint(equalTo: headerView.leftAnchor, constant: 33).isActive = true
        
        AsortBtn.widthAnchor.constraint(equalToConstant: 60).isActive = true
        AsortBtn.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 5).isActive = true
        AsortBtn.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
        AsortBtn.centerXAnchor.constraint(equalTo: headerView.centerXAnchor, constant: -(self.view.frame.width/2 - 20)).isActive = true
//        AsortBtn.leadingAnchor.constraint(equalTo: <#T##NSLayoutAnchor<NSLayoutXAxisAnchor>#>, constant: <#T##CGFloat#>)
        
        //        BsortBtn.leftAnchor.constraint(equalTo: AsortBtn.rightAnchor, constant: 3).isActive = true
        BsortBtn.widthAnchor.constraint(equalToConstant: 40).isActive = true
        //        BsortBtn.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 5).isActive = true
        BsortBtn.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
        BsortBtn.centerXAnchor.constraint(equalTo: headerView.centerXAnchor, constant: (self.view.frame.width/2 - 60)).isActive = true
        
        CsortBtn.widthAnchor.constraint(equalToConstant: 60).isActive = true
        CsortBtn.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
        CsortBtn.centerXAnchor.constraint(equalTo: headerView.centerXAnchor, constant: -20).isActive = true
        
        DsortBtn.widthAnchor.constraint(equalToConstant: 70).isActive = true
        DsortBtn.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
        DsortBtn.centerXAnchor.constraint(equalTo: headerView.centerXAnchor, constant: (self.view.frame.width/2 - 140)).isActive = true
    }
    
    @IBAction func closeBtnAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func notiAction(){
        print("noti text field click event")
        self.performSegue(withIdentifier: "notiPopSeq", sender: self)
    }
    
    @objc func titleSort(){
        
        if titleSortBool {
            titleSortBool = false
            
            let arr = try exchangListMager.jsonArray?.sorted(by:{
                guard let num1 = $0["coinName"] as? String else {
                    return true
                }
                
                guard let num2 = $1["coinName"] as? String else {
                    return true
                }
                
                return num1 > num2
                
            })
            
            exchangListMager.jsonArray = arr
            coinListTableView.reloadData();
        }else {
            titleSortBool = true
            
            let arr = try exchangListMager.jsonArray?.sorted(by:{
                guard let num1 = $0["coinName"] as? String else {
                    return true
                }
                
                guard let num2 = $1["coinName"] as? String else {
                    return true
                }
                
                return num1 < num2
                
            })
            
            exchangListMager.jsonArray = arr
            coinListTableView.reloadData();
        }
        
    }
    
    @objc func volSort(){
        
        if volSortBool {
            volSortBool = false
            
            let arr = try exchangListMager.jsonArray?.sorted(by:{
                guard let num1 = $0["totalVol"] as? String else {
                    return true
                }
                
                guard let num2 = $1["totalVol"] as? String else {
                    return true
                }
                
                return num1 > num2
                
            })
            
            exchangListMager.jsonArray = arr
            coinListTableView.reloadData();
        }else {
            volSortBool = true
            
            let arr = try exchangListMager.jsonArray?.sorted(by:{
                guard let num1 = $0["totalVol"] as? String else {
                    return true
                }
                
                guard let num2 = $1["totalVol"] as? String else {
                    return true
                }
                
                return num1 < num2
                
            })
            
            exchangListMager.jsonArray = arr
            coinListTableView.reloadData();
        }
        
    }
    
    @objc func priceSort(){
        
        if priceSortBool {
            priceSortBool = false
            
            let arr = try exchangListMager.jsonArray?.sorted(by:{
                guard let num1 = self.stringtoDouble(str: ($0["lastPrice"] as? String)!) else {
                    return true
                }
                
                guard let num2 = self.stringtoDouble(str: ($1["lastPrice"] as? String)!) else {
                    return true
                }
                
                return num1 > num2
                
            })
            
            exchangListMager.jsonArray = arr
            coinListTableView.reloadData();
        }else {
            priceSortBool = true
            
            let arr = try exchangListMager.jsonArray?.sorted(by:{
                guard let num1 = self.stringtoDouble(str: ($0["lastPrice"] as? String)!) else {
                    return true
                }
                
                guard let num2 = self.stringtoDouble(str: ($1["lastPrice"] as? String)!) else {
                    return true
                }
                
                return num1 < num2
    
            })
            
            exchangListMager.jsonArray = arr
            coinListTableView.reloadData();
        }
        
    }
    
    @objc func rateSort(){
        
        if rateSortBool {
            rateSortBool = false
            
            let arr = try exchangListMager.jsonArray?.sorted(by:{
                guard let num1 = $0["updnRate"] as? String else {
                    return true
                }
                
                guard let num2 = $1["updnRate"] as? String else {
                    return true
                }
                
                return num1 > num2
                
            })
            
            exchangListMager.jsonArray = arr
            coinListTableView.reloadData();
        }else {
            rateSortBool = true
            
            let arr = try exchangListMager.jsonArray?.sorted(by:{
                guard let num1 = $0["updnRate"] as? String else {
                    return true
                }
                
                guard let num2 = $1["updnRate"] as? String else {
                    return true
                }
                
                return num1 < num2
                
            })
            
            exchangListMager.jsonArray = arr
            coinListTableView.reloadData();
        }
        
    }
    
    func stringtoDouble(str : String) -> Double?{
        let temp = str.components(separatedBy: [","]).joined()
        let double = Double(temp)
        return double ?? 0.0
    }
    
    //real time delegate
    
    func exchangListRT(bt: NPriceTick) {
        print("RT : exchangListKRWRT")
        
        var index = 0
        var num = 0
        var dic = [String : Any]()
        
        let symbol = bt.symbol.toString().components(separatedBy: [" "]).joined()
        let delta_rate = bt.delta_rate.toString()
        let last_volume = bt.last_volume.toString()
        let last_price = bt.last_price.toString()
        let intLast_volume = stringtoDouble(str: last_volume)
        
        if let intLast_price = Int(last_price){
            dic.updateValue(String(intLast_price), forKey: "lastPrice")
        }else {
            let intLast_price = stringtoDouble(str: last_price)
            dic.updateValue(String(intLast_price!), forKey: "lastPrice")
        }
        
        dic.updateValue(symbol, forKey: "simbol")
        dic.updateValue(delta_rate, forKey: "updnRate" )
        dic.updateValue(String(intLast_volume!), forKey: "totalVol")
        
        
        print("RT symbole : \(symbol)")
        
        let intlastVol = Int(last_volume) ?? 0
        
        for row in exchangListMager.jsonArray ?? [[String : Any]]() {
            
            let sym = row as [String : Any]
            
            let tempSymbole = sym["simbol"] as? String ?? ""
            
            print("RT symbol : \(tempSymbole)")
            
            print("RT symbols : \(symbol)")
            
            print("RT test : \(symbol == tempSymbole)")
            
            if symbol == tempSymbole {
                
                num = index
                
                let coinname = sym["coinName"] as? String ?? ""
                
                print("RT coinname : \(coinname)")
                
                dic.updateValue(coinname, forKey: "coinName" )
                
                exchangListMager.jsonArray![index] = dic
                
//                if intlastVol != 0 {
//                    exchangListMager.jsonArray![index] = dic
//                }else {
//                    if let intLast_price = Int(last_price){
//                        exchangListMager.jsonArray![index].updateValue(String(intLast_price), forKey: "lastPrice")
//                    }else {
//                        let intLast_price = stringtoDouble(str: last_price)
//                        exchangListMager.jsonArray![index].updateValue(String(intLast_price!), forKey: "lastPrice")
//                    }
//                }
                
                self.realTimeSelect = true
                print("RT coinname : \(coinname)")
                print("RT index : \(index)")
                
            }
            
            index = index + 1
        }
 
        print("RT Tick : \(dic)")
        let indexpath = IndexPath(row: num, section: 0)
        
        DispatchQueue.main.async {
            self.coinListTableView.reloadRows(at: [indexpath], with: .none)
        }
        
    }
    
    func exchangDetailRT(bt: NPriceTick) {
       
    }
    
    func openListRT(bt: NOpenInfo) {
        
    }
    
    func trandeListRT(bt: NPriceInfo) {
        
    }
    
    func krwChangeRT(value: Double) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard  let tempjson = exchangListMager.jsonArray else {
            return 0
        }
        
        return exchangListMager.jsonArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = tableView.dequeueReusableCell(withIdentifier: "exchangeCell") as? ExchangListCell
        let cell = tableView.dequeueReusableCell(withIdentifier: "newExchangCell", for: indexPath) as? ExchangListCell
        
        cell?.selectionStyle = .none
        cell?.layoutMargins = UIEdgeInsets.zero
        
        
        let dataTemp = exchangListMager.jsonArray![indexPath.row] as [String : Any]
        //print(dataTemp)
        
        let coinsim = dataTemp["simbol"] as? String ?? ""
        let coinname = dataTemp["coinName"] as? String ?? ""
        let coinper = dataTemp["updnRate"] as? String ?? ""
        var updnPrice = dataTemp["updnPrice"] as? String ?? "0"
        let coinvol = dataTemp["totalVol"] as? String ?? ""
        let coinvalue = dataTemp["lastPrice"] as? String ?? ""
        let updnSign = dataTemp["updnSign"] as? String ?? "0"
        
        print("trandeupdnsign : \(updnSign)")
        
        let str = coinvalue.insertComma
        
        var simboleStr = ""
        var marketStr = ""
        
        if coinsim.contains("_") {
            
            let indexStr = coinsim.index(of: "_")
        
            simboleStr = String(coinsim.prefix(upTo: indexStr!))
            marketStr = String(coinsim.suffix(from: indexStr!))
            
        }else {
            simboleStr = coinsim
            marketStr = "/KRW"
        }
        
//        let lastcoinsim = coinsim.replacingOccurrences(of: "_", with: "/")
        
        print("simbostr : \(simboleStr), makerStr : \(marketStr)")
    
        
        let attr = NSMutableAttributedString(string: simboleStr, attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 13), NSAttributedString.Key.foregroundColor : UIColor(rgb: 0x666666)])
        
        attr.append(NSAttributedString(string: marketStr, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12), NSAttributedString.Key.foregroundColor : UIColor(rgb: 0x999999)]))
        
        
        let perStr = self.stringChage(value1: "", value2: "%", origin: "\(String(format: "%0.2f", stringtoDouble(str: coinper.insertComma) ?? 0.0))")
        
        let widht = str.calcWidth(forHeight: 30, font: UIFont.systemFont(ofSize: 16))
        
        cell?.coinSymbol = coinsim
        
        cell?.simbol.attributedText = attr
        
        cell?.coinName.text = coinname
        cell?.coinName.font = UIFont.boldSystemFont(ofSize: 14)
        
        if coinsim == "BTC" {
            AppDelegate.BTC = stringtoDouble(str: coinvalue)!
        }else if coinsim == "ETH" {
            AppDelegate.ETH = stringtoDouble(str: coinvalue)!
        }
        
        if updnSign == "1" {
            cell?.coinValue.textColor = UIColor.DPPlusTextColor
            cell?.coinPer.textColor = UIColor.DPPlusTextColor
            cell?.perVsValue.textColor = UIColor.DPPlusTextColor
            //cell?.coinPerBackground.backgroundColor = UIColor.DPSellDefaultColor
        }else if updnSign == "-1"{
            cell?.coinValue.textColor = UIColor.DPMinTextColor
            cell?.coinPer.textColor = UIColor.DPMinTextColor
            cell?.perVsValue.textColor = UIColor.DPMinTextColor
            //cell?.coinPerBackground.backgroundColor = UIColor.DPBuyDefaultColor
        }else{
            cell?.coinValue.textColor = UIColor.DPmainTextColor
            cell?.coinPer.textColor = UIColor.DPmainTextColor
            cell?.perVsValue.textColor = UIColor.DPmainTextColor
        }
        
        print("krw coinper : \(coinper)")
//        cell?.coinPer.drawText(in: CGRect(x: 0.5, y: 0.5, width: 0.5, height: 0.5))
        cell?.coinPer.adjustsFontSizeToFitWidth = true
        cell?.coinPer.text = perStr
        
        cell?.coinVol.text = self.stringChage(value1: "", value2: "", origin: (coinvol.insertComma))
        cell?.coinVol.font = UIFont.systemFont(ofSize: 12)
        cell?.coinValue.numberOfLines = 2
        cell?.coinValue.text = str
        cell?.coinValue.adjustsFontSizeToFitWidth = true
        cell?.coinValue.sizeToFit()
        
        if updnPrice.contains("-") {
            updnPrice.replace(originalString: "-", withString: "")
        }
        
        let dbUpdnPrice = stringtoDouble(str: updnPrice) ?? 0.0
        
        if dbUpdnPrice == 0.0 {
           cell?.perVsValue.text = String(format: "%0.0f", dbUpdnPrice).insertComma
        }else if dbUpdnPrice < 100.0 && dbUpdnPrice > 10.0 {
            cell?.perVsValue.text = String(format: "%0.1f", dbUpdnPrice).insertComma
        }else if dbUpdnPrice < 10.0 {
            cell?.perVsValue.text = String(format: "%0.2f", dbUpdnPrice).insertComma
        }else{
            cell?.perVsValue.text = String(format: "%0.0f", dbUpdnPrice).insertComma
        }
        
        
        
//        let widht = .calcWidth(forHeight: 20, font: UIFont.systemFont(ofSize: 14))
//
//        cell?.coinValue.frame.size = CGSize(width: widht, height: (cell?.frame.height)! - 5)
        
        
        cell?.coinValue.frame.size = CGSize(width: widht, height: 30)
        
//        if (indexPath.row % 2) == 0  {
//
//            cell?.backgroundColor = UIColor.DPViewBackgroundColor
//
//        }else {
//            cell?.backgroundColor = UIColor.DPViewGrayBackgroundColor
//        }
        
        if realTimeSelect {
            print("RT animation start verty")
//                UIView.animate(withDuration: 10, animations: {
//                    print("RT animation")
//
//                    cell?.coinValue.layer.borderColor = UIColor.DPBuyDefaultColor.cgColor
//
//                }, completion: { (_) in
//                    print("RT animation completion")
//                    cell?.coinValue.layer.borderColor = UIColor.white.cgColor
//
//                })
            
            realTimeSelect = false
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dataTemp = exchangListMager.jsonArray![indexPath.row] as [String : Any]
        let coinSimbol = dataTemp["simbol"] as? String
        let coinName = dataTemp["coinName"] as? String
        didSelectRow?(coinSimbol!, coinName!, self.maket)
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return self.headerView
    }
    
    func stringChage(value1: String, value2: String, origin: String) -> String {
        
        var str : String = origin
        
        if value1 == "" {
            str = "\(str) \(value2)"
            print("krw coinper value 1 : \(str)")
        }else if value2 == ""{
            str = "\(value1) \(str)"
            print("krw coinper value 2 : \(str)")
        }else {
            str = "\(value1) \(str) \(value2)"
            print("krw coinper value 3 : \(str)")
        }
        
        
         print("krw coinper value result : \(str)")
        return str
    }
    
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imgUrl.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sliderCell", for: indexPath)
        if let vc = cell.viewWithTag(111) as? UIImageView {
            vc.contentMode = .scaleToFill
            let url = URL(string: imgUrl[indexPath.row])
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!)
                DispatchQueue.main.async {
                    if data != nil {
                        vc.image = UIImage(data: data!)
                    }
                    vc.contentMode = .scaleToFill
                }
            }
        }
        
        return cell
    }
    
    @objc
    func changeImage(){
        print("home view controller changeImage")
        if counter < imgUrl.count {
            let index = IndexPath.init(item: counter, section: 0)
            self.sliderCollectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
            pageView.currentPage = counter
            counter += 1
        }else {
            counter = 0
            let index = IndexPath.init(item: counter, section: 0)
            self.sliderCollectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
            pageView.currentPage = counter
            counter = 1
        }
        
    }
    
}

extension HomeViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = sliderCollectionView.frame.size
        return CGSize(width: size.width, height: size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func alertList(uid: String, sessionid: String, type: alerType){
        
        let urlComponents = URLComponents(string:"\(AppDelegate.url)/ajax/getPushLog?uid=\(uid)&sessionId=\(sessionid)&language=\(AppDelegate.appLang)")!
        
        print("urlcomponents : \(urlComponents)")
        
        let amo = Alamofire.request(urlComponents)
        amo.responseJSON { response in
            
            if let value = response.result.value as? [[String : Any]] {
                
                var message = ""
                
                if value.count == 0 {
                    message = "최신 등록된 공지사항이 없습니다."
                }else {
                    message = value[0]["msgText"] as? String ?? ""
                }
                
                self.notiTextField.text = message
            }
        }
    }
}
