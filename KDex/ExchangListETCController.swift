//
//  ExchangListKRWcontroller.swift
//  crevolc
//
//  Created by crovolc on 2017. 12. 28..
//  Copyright © 2017년 hanbiteni. All rights reserved.
//
//  사용하지 않는 컨트롤러

import UIKit

class ExchangListETCcontroller: UIViewController, UITableViewDelegate, UITableViewDataSource, RealTimeDelegate {
    
    var subscribeRS : NTcsReal = NTcsReal()
    
    var viewDelegate : ViewController?
    
    var searchValue = "" {
        didSet{
            exchangListMager.searchcoinList(market: self.maket, tableview: self.exahangTableView, activity: self.activity, search: searchValue, first: false)
           
        }
    }
    
    var torchEvent : UITapGestureRecognizer?
    
    @IBOutlet var activity: UIActivityIndicatorView!
    
    var exchangListMager = ExchangCoinListManager()
    
    private var realTimeSelect = false
    
    private var titleSortBool = true
    private var volSortBool = true
    private var priceSortBool = true
    private var rateSortBool = true
    
    private let maket = "ETH"
    
    @IBOutlet var exahangTableView: UITableView!
    
     var didSelectRow: ((String) -> ())?
    
    //table header start -------------------
    
    let headerView : UIView = {
        let uv = UIView()
        uv.backgroundColor = UIColor.DPtableHeaderBgColor
        return uv
    }()
    
    let favorImg : UIImageView = {
        let image = UIImage(named: "star_on")
        let img = UIImageView(image: image)
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    let AsortBtn : UIButton = {
        let bt = UIButton()
        let iamge = UIImage(named: "icn_list_sort_deactive-1")
        bt.backgroundColor =  UIColor.clear
        bt.translatesAutoresizingMaskIntoConstraints = false
        bt.setAttributedTitle(NSAttributedString(string: "trade_market_coin_name".localized, attributes:[NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.DPmainTextColor]), for: .normal)
        if AppDelegate.appLang == "en_US"{
            bt.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -55)
        }else {
            bt.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -75)
        }
        bt.titleEdgeInsets = UIEdgeInsets(top: 0, left: -40, bottom: 0, right: 0)
        bt.setImage(iamge, for: .normal)
        bt.addTarget(self, action: #selector(titleSort), for: .touchUpInside)
        return bt
    }()
    
    let BsortBtn : UIButton = {
        let bt = UIButton()
        let iamge = UIImage(named: "icn_list_sort_deactive-1")
        bt.translatesAutoresizingMaskIntoConstraints = false
//        bt.setAttributedTitle(NSAttributedString(string: "trade_market_rate".localized, attributes:[NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14)], NSAttributedString.Key.foregroundColor : UIColor.DPmainTextColor), for: .normal)
        bt.setAttributedTitle(NSAttributedString(string: "trade_market_rate".localized, attributes:[NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14)]), for: .normal)
        if AppDelegate.appLang == "en_US"{
            bt.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -65)
        }else {
            bt.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -70)
        }
        bt.titleEdgeInsets = UIEdgeInsets(top: 0, left: -45, bottom: 0, right: 0)
        bt.setImage(iamge, for: .normal)
        bt.addTarget(self, action: #selector(volSort), for: .touchUpInside)
        bt.backgroundColor =  UIColor.clear
        
        return bt
    }()
    
    let CsortBtn : UIButton = {
        let bt = UIButton()
        let iamge = UIImage(named: "icn_list_sort_deactive-1")
        bt.translatesAutoresizingMaskIntoConstraints = false
        bt.setAttributedTitle(NSAttributedString(string: "trade_market_last_price".localized, attributes:[NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.DPmainTextColor]), for: .normal)
        bt.setImage(iamge, for: .normal)
        if AppDelegate.appLang == "en_US"{
            bt.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -125)
        }else {
            bt.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -70)
        }
        bt.titleEdgeInsets = UIEdgeInsets(top: 0, left: -45, bottom: 0, right: 0)
        bt.addTarget(self, action: #selector(priceSort), for: .touchUpInside)
        bt.backgroundColor = UIColor.clear
        return bt
    }()
    
    let DsortBtn : UIButton = {
        let bt = UIButton()
        let iamge = UIImage(named: "icn_list_sort_deactive-1")
        bt.translatesAutoresizingMaskIntoConstraints = false
        bt.setAttributedTitle(NSAttributedString(string: "trade_market_yesterday".localized, attributes:[NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.DPmainTextColor]), for: .normal)
        bt.setImage(iamge, for: .normal)
        if AppDelegate.appLang == "en_US"{
            bt.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -90)
        }else {
            bt.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -110)
        }
        bt.titleEdgeInsets = UIEdgeInsets(top: 0, left: -30, bottom: 0, right: 0)
        bt.addTarget(self, action: #selector(rateSort), for: .touchUpInside)
        bt.backgroundColor =  UIColor.clear
        return bt
    }()
    
    //table header end -------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 40)
        
        headerView.addSubview(favorImg)
        headerView.addSubview(AsortBtn)
        headerView.addSubview(BsortBtn)
        headerView.addSubview(CsortBtn)
        headerView.addSubview(DsortBtn)
        
        
        favorImg.leftAnchor.constraint(equalTo: headerView.leftAnchor, constant: 10).isActive = true
        favorImg.widthAnchor.constraint(equalToConstant: 20).isActive = true
        favorImg.heightAnchor.constraint(equalToConstant: 20).isActive = true
        favorImg.centerYAnchor.constraint(lessThanOrEqualTo: headerView.centerYAnchor).isActive = true
        AsortBtn.leftAnchor.constraint(equalTo: favorImg.rightAnchor, constant: 3).isActive = true
        AsortBtn.widthAnchor.constraint(equalToConstant: 60).isActive = true
        AsortBtn.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 5).isActive = true
        BsortBtn.leftAnchor.constraint(equalTo: AsortBtn.rightAnchor, constant: 3).isActive = true
        BsortBtn.widthAnchor.constraint(equalToConstant: 40).isActive = true
        BsortBtn.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 5).isActive = true
        CsortBtn.rightAnchor.constraint(equalTo: DsortBtn.leftAnchor, constant: -10).isActive = true
        CsortBtn.widthAnchor.constraint(equalToConstant: 60).isActive = true
        CsortBtn.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 5).isActive = true
        DsortBtn.rightAnchor.constraint(equalTo: headerView.rightAnchor, constant: -70).isActive = true
        DsortBtn.widthAnchor.constraint(equalToConstant: 70).isActive = true
        DsortBtn.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 5).isActive = true
        
        //self.exahangTableView.tableHeaderView = headerView
        
        self.exahangTableView.backgroundColor = UIColor.DPtableViewBgColor
        
        let nib = UINib(nibName: "ExchangListNib", bundle: nil)
        exahangTableView.register(nib, forCellReuseIdentifier: "newExchangCell")
        
    }
    
    override func viewDidAppear(_ animated: Bool) {

        let plist = UserDefaults.standard
        plist.set("ETC", forKey: "marketSimbol")
        plist.synchronize()
        
        exchangListMager.searchcoinList(market: self.maket, tableview: self.exahangTableView, activity: self.activity, search: AppDelegate.localSearch, first: true)
     
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if torchEvent != nil {
            print("touch event")
            exahangTableView.removeGestureRecognizer(torchEvent!)
        }
        
        setupKeyboardObservers()
        
        TcpSocket.sharedInstance.receiverThread?.delegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("coin list : \(exchangListMager.jsonArray?.count)")
        return exchangListMager.jsonArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = tableView.dequeueReusableCell(withIdentifier: "exchangeCell") as? ExchangListCell
        let cell = tableView.dequeueReusableCell(withIdentifier: "newExchangCell", for: indexPath) as? ExchangListCell
        
        cell?.selectionStyle = .none
        
        let dataTemp = exchangListMager.jsonArray![indexPath.row] as [String : Any]
        
        let coinsim = dataTemp["simbol"] as? String ?? ""
        let coinname = dataTemp["coinName"] as? String ?? ""
        let coinper = dataTemp["updnRate"] as? String
        let coinvol = dataTemp["totalVol"] as? String
        let coinvalue = dataTemp["openPrice"] as? String
        let doublecoinvalue = stringtoDouble(str: coinvalue!)
        
        let krwvalue = doublecoinvalue! * AppDelegate.BTC
        
        let intKrw = Int(krwvalue)
        
        let strKrw = String(intKrw)
        
        guard let str = coinvalue?.insertComma else {
            return cell!
        }
        
        var convalueText = ""
        
        if coinsim == "" || coinsim == nil {
            convalueText = "\(str)"
        }else {
            convalueText = "\(str) \n \(strKrw.insertComma)KRW"
        }
        
        let lastcoinsim = coinsim.replacingOccurrences(of: "_", with: "/")
        
        let widht = convalueText.calcWidth(forHeight: 20, font: UIFont.systemFont(ofSize: 14))
        
        cell?.coinValue.frame.size = CGSize(width: widht, height: (cell?.frame.height)! - 5)
        
        cell?.coinSymbol = coinsim
        cell?.simbol.text = "(\(lastcoinsim))"
        cell?.coinName.text = coinname
        //self.stringChage(value1: nil, value2: coinsim!, origin: coinname)
        cell?.coinPer.text = self.stringChage(value1: nil, value2: "%", origin: (coinper?.insertComma)!)
        cell?.coinVol.text = self.stringChage(value1: "Vol", value2: "볼룸", origin: (coinvol?.insertComma)!)
        cell?.coinValue.numberOfLines = 2
        cell?.coinValue.text = convalueText
        cell?.coinValue.adjustsFontSizeToFitWidth = true
        cell?.coinValue.sizeToFit()
        
        if (coinper?.contains("-"))! {
            cell?.coinValue.textColor = UIColor.DPMinTextColor
            cell?.coinPer.textColor = UIColor.DPMinTextColor
            //cell?.coinPerBackground.backgroundColor = UIColor.DPSellDefaultColor
        }else{
            cell?.coinValue.textColor = UIColor.DPPlusTextColor
            cell?.coinPer.textColor = UIColor.DPPlusTextColor
            //cell?.coinPerBackground.backgroundColor = UIColor.DPBuyDefaultColor
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dataTemp = exchangListMager.jsonArray![indexPath.row] as [String : Any]
        let coinSimbol = dataTemp["simbol"] as? String
        
        print("coinSimbol : \(coinSimbol)")
        didSelectRow?(coinSimbol!)
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return self.headerView
    }
    
    func stringChage(value1: String?, value2: String, origin: String) -> String {
        
        var str : String = origin
        
        if value1 == nil {
            str = "\(str) \(value2)"
            
        }else {
            str = "\(value1!) \(str) \(value2)"
        }
        
        return str
    }
    
    func stringtoDouble(str : String) -> Double?{
        let temp = str.components(separatedBy: [","]).joined()
        let double = Double(temp)
        return double ?? 0.0
    }
    
    func exchangListRT(bt: NPriceTick) {
        print("RT : exchangListETCRT")
        
        var index = 0
        var num = 0
        var dic = [String : Any]()
        
        let symbol = bt.symbol.toString().components(separatedBy: [" "]).joined()
        let delta_rate = bt.delta_rate.toString()
        let last_volume = bt.last_volume.toString()
        let last_price = bt.last_price.toString()
        let intLast_volume = Double(last_volume)
        
        if let intLast_price = Int(last_price){
            dic.updateValue(String(intLast_price), forKey: "lastPrice")
        }else {
            let intLast_price = Double(last_price)
            dic.updateValue(String(intLast_price!), forKey: "lastPrice")
        }
        
        dic.updateValue(symbol, forKey: "simbol")
        dic.updateValue(delta_rate, forKey: "updnRate" )
        dic.updateValue(String(intLast_volume!), forKey: "totalVol")
        
        print("RT symbole : \(symbol)")
        
        for row in exchangListMager.jsonArray! {
            
            let sym = row as [String : Any]
            
            let tempSymbole = sym["simbol"] as? String ?? ""
            
            print("RT symbol : \(tempSymbole)")
            
            print("RT symbols : \(symbol)")
            
            print("RT test : \(symbol == tempSymbole)")
            
            if symbol == tempSymbole {
                
                num = index
                
                let coinname = sym["coinName"] as? String ?? ""
                
                print("RT coinname : \(coinname)")
                
                dic.updateValue(coinname, forKey: "coinName" )
                
                exchangListMager.jsonArray![index] = dic
                
                self.realTimeSelect = true
                print("RT coinname : \(coinname)")
                print("RT index : \(index)")
                
            }
            
            index = index + 1
        }
        
        print("RT Tick : \(dic)")
        let indexpath = IndexPath(row: num, section: 0)
        
        DispatchQueue.main.async {
            self.exahangTableView.reloadRows(at: [indexpath], with: .none)
        }
        
    }
    
    func exchangDetailRT(bt: NPriceTick) {
        
    }
    
    func trandeListRT(bt: NPriceInfo) {
        
    }
    
    func openListRT(bt: NOpenInfo) {
        
    }
    
    func krwChangeRT(value: Double) {
        DispatchQueue.main.async {
            self.exahangTableView.reloadData()
        }
    }
    
    @objc func titleSort(){
        
        if titleSortBool {
            titleSortBool = false
            
            let arr = try exchangListMager.jsonArray?.sorted(by:{
                guard let num1 = $0["coinName"] as? String else {
                    return true
                }
                
                guard let num2 = $1["coinName"] as? String else {
                    return true
                }
                
                return num1 > num2
                
            })
            
            exchangListMager.jsonArray = arr
            exahangTableView.reloadData();
        }else {
            titleSortBool = true
            
            let arr = try exchangListMager.jsonArray?.sorted(by:{
                guard let num1 = $0["coinName"] as? String else {
                    return true
                }
                
                guard let num2 = $1["coinName"] as? String else {
                    return true
                }
                
                return num1 < num2
                
            })
            
            exchangListMager.jsonArray = arr
            exahangTableView.reloadData();
        }
        
    }
    
    @objc func volSort(){
        
        if volSortBool {
            volSortBool = false
            
            let arr = try exchangListMager.jsonArray?.sorted(by:{
                guard let num1 = $0["totalVol"] as? String else {
                    return true
                }
                
                guard let num2 = $1["totalVol"] as? String else {
                    return true
                }
                
                return num1 > num2
                
            })
            
            exchangListMager.jsonArray = arr
            exahangTableView.reloadData();
        }else {
            volSortBool = true
            
            let arr = try exchangListMager.jsonArray?.sorted(by:{
                guard let num1 = $0["totalVol"] as? String else {
                    return true
                }
                
                guard let num2 = $1["totalVol"] as? String else {
                    return true
                }
                
                return num1 < num2
                
            })
            
            exchangListMager.jsonArray = arr
            exahangTableView.reloadData();
        }
        
    }
    
    @objc func priceSort(){
        
        if priceSortBool {
            priceSortBool = false
            
            let arr = try exchangListMager.jsonArray?.sorted(by:{
                guard let num1 = $0["lastPrice"] as? String else {
                    return true
                }
                
                guard let num2 = $1["lastPrice"] as? String else {
                    return true
                }
                
                return num1 > num2
                
            })
            
            exchangListMager.jsonArray = arr
            exahangTableView.reloadData();
        }else {
            priceSortBool = true
            
            let arr = try exchangListMager.jsonArray?.sorted(by:{
                guard let num1 = $0["lastPrice"] as? String else {
                    return true
                }
                
                guard let num2 = $1["lastPrice"] as? String else {
                    return true
                }
                
                return num1 < num2
                
            })
            
            exchangListMager.jsonArray = arr
            exahangTableView.reloadData();
        }
        
    }
    
    @objc func rateSort(){
        
        if rateSortBool {
            rateSortBool = false
            
            let arr = try exchangListMager.jsonArray?.sorted(by:{
                guard let num1 = $0["updnRate"] as? String else {
                    return true
                }
                
                guard let num2 = $1["updnRate"] as? String else {
                    return true
                }
                
                return num1 > num2
                
            })
            
            exchangListMager.jsonArray = arr
            exahangTableView.reloadData();
        }else {
            rateSortBool = true
            
            let arr = try exchangListMager.jsonArray?.sorted(by:{
                guard let num1 = $0["updnRate"] as? String else {
                    return true
                }
                
                guard let num2 = $1["updnRate"] as? String else {
                    return true
                }
                
                return num1 < num2
                
            })
            
            exchangListMager.jsonArray = arr
            exahangTableView.reloadData();
        }
        
    }
    
    @objc func hideKeyboard() {
        print("touchevent ")
        viewDelegate?.hideKeyboard()
    }
    
    func setupKeyboardObservers(){
        print("setupKeyboardObservers")
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    @objc func handleKeyboardWillShow(_ notification: Notification) {
        print("show keyborad")
        
        torchEvent = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        exahangTableView.addGestureRecognizer(torchEvent!)
    }
    
    @objc func handleKeyboardWillHide(_ notification: Notification)  {
        print("hide keyborad")
        if torchEvent != nil {
            print("touch event")
            exahangTableView.removeGestureRecognizer(torchEvent!)
        }
    }
    
}
