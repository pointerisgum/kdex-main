//
//  ExchangConcluDetail.swift
//  crevolc
//
//  Created by crovolc on 2018. 1. 2..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

class ExchangConcluDetail : UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var coinSimbol : String? //코인 아이디
    
    var coinName : String? //코인 이름
    
    var jsonArray : [[String : Any]]?
    
    var concluArray = [[String : Any]]()
    
    var jsondic : NSMutableDictionary?

    @IBOutlet var exConTable: UITableView!
    
    var activity = UIActivityIndicatorView()
    
    var allconluBool : Bool = false
    
    var marksimbol : String?
    
    //테이블 헤더 작업 -----------
    
    let headerView : UIView = {
        let uv = UIView()
        uv.backgroundColor = UIColor.DPtableHeaderBgColor
        return uv
    }()
    
    let tb_Label : UILabel = {
        let lb = UILabel()
        lb.text = "fragment_pending_pendinglist".localized
        lb.textColor = UIColor.DPmainTextColor
        lb.textAlignment = .left
        lb.baselineAdjustment = .alignCenters
        lb.font = UIFont.boldSystemFont(ofSize: 14)
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    let tb_allBtn : UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("fragment_pending_all_cancel".localized, for: .normal)
        btn.setTitleColor(UIColor.white, for: .normal)
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
        btn.backgroundColor = UIColor.DPSellButtonColor
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(selectConclu), for: .touchUpInside)
        return btn
    }()
    
    let tb_selectBtn : UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("fragment_pending_select_cancel".localized, for: .normal)
        btn.setTitleColor(UIColor.white, for: .normal)
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.backgroundColor = UIColor.DPExchangeButtonColor
        btn.addTarget(self, action: #selector(selectConclu), for: .touchUpInside)
        
        return btn
    }()
    
    //테이블 헤더 작업 end -----------
    
    let footerView: UIView = {
        let uv = UIView()
        uv.backgroundColor = UIColor.DPtableFooterBgColor
        return uv
    }()
    
    let footer_Label : UILabel = {
        let lb = UILabel()
        lb.text = "pending_list_no_list_txt".localized
        lb.textColor = UIColor.DPmainTextColor
        lb.textAlignment = .center
        lb.font = UIFont.systemFont(ofSize: 16)
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activity.frame = CGRect(x: self.view.frame.width/2 - 50, y: self.exConTable.frame.height/2 - 100, width: 100, height: 100)
        activity.style = .whiteLarge
        activity.color = UIColor.DPActivityColor
        activity.hidesWhenStopped = true
        exConTable.addSubview(activity)
        
        exConTable.backgroundColor = UIColor.clear
        exConTable.layoutMargins = UIEdgeInsets.zero
        exConTable.separatorInset = UIEdgeInsets.zero
        self.view.backgroundColor = UIColor.DPViewBackgroundColor
        setupheaderView()
        chatMoving()
        
        let plist = UserDefaults.standard
        self.marksimbol = plist.string(forKey: "marketSimbol")
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationController?.navigationBar.topItem?.title = "\(coinName)(\(coinSimbol!.replacingOccurrences(of: "_", with: "/")))"
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        chatMoving()
    }
    
    //매칭 테이블(listTable)헤더 설정
    func setupheaderView(){
        
        headerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50)
        
        [tb_Label, tb_allBtn, tb_selectBtn].forEach { self.headerView.addSubview($0) }
        
        footerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 100)
        footerView.backgroundColor = UIColor.DPtableFooterBgColor
        
        footerView.addSubview(footer_Label)
        
        setupconstrains()
        
    }
    
    func setupconstrains() {
        
        self.tb_Label.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 10).isActive = true
        self.tb_Label.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 10).isActive = true
        self.tb_Label.widthAnchor.constraint(equalToConstant: 100).isActive = true
        self.tb_Label.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        self.tb_selectBtn.topAnchor.constraint(equalTo: self.tb_Label.topAnchor).isActive = true
        self.tb_selectBtn.widthAnchor.constraint(equalToConstant: 80).isActive = true
        self.tb_selectBtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        self.tb_selectBtn.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: -10).isActive = true
        
        self.tb_allBtn.topAnchor.constraint(equalTo: self.tb_Label.topAnchor).isActive = true
        self.tb_allBtn.widthAnchor.constraint(equalToConstant: 80).isActive = true
        self.tb_allBtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        self.tb_allBtn.trailingAnchor.constraint(equalTo: tb_selectBtn.leadingAnchor, constant: -10).isActive = true
        
        self.footer_Label.topAnchor.constraint(equalTo: footerView.topAnchor).isActive = true
        self.footer_Label.leadingAnchor.constraint(equalTo: footerView.leadingAnchor).isActive = true
        self.footer_Label.rightAnchor.constraint(equalTo: footerView.rightAnchor).isActive = true
        self.footer_Label.bottomAnchor.constraint(equalTo: footerView.bottomAnchor).isActive = true
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.jsonArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return self.headerView
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return self.footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ex_ConcluCell") as? ExchangConcluCell
        cell?.selectionStyle = .none
        cell?.layoutMargins = UIEdgeInsets.zero
        cell?.backgroundColor = UIColor.DPViewBackgroundColor
        
        
        guard let tempData = self.jsonArray?[indexPath.row] else {
            return cell!
        }
        
        print("\(tempData["ordrNo"])")
        let buyType = tempData["byslTp"] as? String ?? ""
        let dateStr = tempData["ordrTime"] as? String ?? ""
        let ordrPrc = tempData["ordrPrc"] as? String ?? ""
        let doubleOrdrPrc = stringtoDouble(str: ordrPrc) ?? 0.0
        var simbole = tempData["instCd"] as? String ?? ""
        let mtchPrc = tempData["mtchPrc"] as? String ?? "0"
        let mtchQty = tempData["remnQty"] as? String ?? "0" //미체결수량
        let doubleMtcQty = stringtoDouble(str: mtchQty) ?? 0.0
        let ordrQty = tempData["ordrQty"] as? String ?? "0"
        let doubleOrdrQty = stringtoDouble(str: ordrQty) ?? 0.0
        
        var market = ""
        
        cell?.coinLabel.text = simbole.replacingOccurrences(of: "_", with: "/")
        cell?.dateLabel.text = self.makeTime(date: dateStr ?? "")
        
        if simbole.contains("_") {
            let simMaket = simbole.components(separatedBy: "_")
            simbole = simMaket[0]
            market = simMaket[1]
        }else {
            market = "KRW"
        }
        
        if market == "KRW" {
            cell?.orderPriceLabe.text = "\(ordrPrc.insertComma) \(market)"
        }else {
            cell?.orderPriceLabe.text = "\(String(format: "%0.8f", doubleOrdrPrc)) \(market)"
        }
        
        cell?.orderCountLabel.text = "\(ordrQty) \(simbole)"
        cell?.confirmCountLabel.text = "\(doubleOrdrQty - doubleMtcQty) \(simbole)"
        cell?.concluCountLabel.text = "\(mtchQty) \(simbole)"
        
        if buyType == "B" {
            cell?.sellBuyLabel.text = "fragment_title_bid".localized
            cell?.sellBuyLabel.textColor = UIColor.DPBuyDefaultColor
        }else {
            cell?.sellBuyLabel.text = "fragment_title_ask".localized
            cell?.sellBuyLabel.textColor = UIColor.DPSellDefaultColor
        }
        
        let isCheck = (tempData["isClick"] as? String) ?? ""
        print("con detail select index : \(indexPath.row) \(isCheck)")
        
        if isCheck == "Y" {
            let deimg = UIImage(named: "ico_ch_on")
            cell?.isSelectBtn.image = deimg
        }else {
            let img = UIImage(named: "ico_ch_off")
            cell?.isSelectBtn.image = img
        }
        
        cell?.selectionStyle = .none
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("con detail select index : \(indexPath.row)")
        let cell = tableView.cellForRow(at: indexPath) as? ExchangConcluCell
       
        let img = UIImage(named: "ico_ch_off")
        let deimg = UIImage(named: "ico_ch_on")
        
        if (cell?.selectBtn)! {
            print("con detail isclick N ")
            cell?.isSelectBtn.image = img
            self.jsonArray![indexPath.row].updateValue("N", forKey: "isClick")
            cell?.selectBtn = false
        }else{
            print("con detail isclick Y ")
            cell?.isSelectBtn.image = deimg
            self.jsonArray![indexPath.row].updateValue("Y", forKey: "isClick")
            cell?.selectBtn = true
        }
        
        print("con detail : \(self.jsonArray![indexPath.row]) ")
        
    }
    
    func chatMoving(){
        print("inout chatMoving")
        
        let symbole = coinSimbol!
        
        activity.startAnimating()
        
        let urlComponents = URLComponents(string: "\(AppDelegate.url)/auth/matchingReady?simbol=\(symbole)&sessionId=\(AppDelegate.sessionid)&uid=\(AppDelegate.uid)")
        
        print("urlComponents : \(urlComponents)")
        
        let amo = Alamofire.request(urlComponents!)
        amo.responseJSON(completionHandler:
            { response in
                
                self.jsonArray = [[String : Any]]()
                
                print(response.result.isSuccess)
                
                guard let tempJson = response.result.value as? [[String : Any]] else {
                    return
                }
                
                for row in tempJson {
                    
                    print("con detail row : \(row)")
                    var value = row
                    value.updateValue("N", forKey: "isClick")
                    
                    print("con detail value : \(value)")
                    
                    self.jsonArray?.append(value)
                }
                
                print("conclu jsonarray count : \(self.jsonArray?.count)")
                
                self.activity.stopAnimating()
                
                self.exConTable.reloadData()
                
                if self.jsonArray?.count == 0 {
                    self.footer_Label.isHidden = false
                    self.footerView.isHidden = false
                    
                }else {
                    self.footer_Label.isHidden = true
                    self.footerView.isHidden = true
                }
                
        })
        
    }
    
    //날짜 형식 포멧
    func makeTime(date : String) -> String {
        
        let df : DateFormatter = DateFormatter()
        
        df.dateFormat = "yyyyMMddHHmmss"
        
        let date_time : NSDate = (df.date(from: date) as? NSDate)!
        
        df.dateFormat = "yy.MM.dd HH:mm:ss"
        
        let newDate : String = df.string(from: date_time as Date) as String
        
        return newDate
    }
    
    @objc func selectConclu(sender : UIButton){
        
        if sender == tb_allBtn {
            print("con detail allconluBool")
            if allconluBool {
                allconluBool = false
                
            }else{
                allconluBool = true
            }
        }
        
        if self.jsonArray?.count == 0 {
            showToast(message: "pending_list_no_list_txt".localized, textcolor: UIColor.white, frame: CGRect(x: self.view.frame.size.width/2 - 100, y: self.view.frame.size.height - 100  , width: 200, height: 35), time: 1, animation: true)
            
        }else {
            
            let url = URL(string: "\(AppDelegate.url)/auth/orderCancle")
            
            var request = URLRequest(url: url!)
            
            request.httpMethod = "POST"
            
            for row in self.jsonArray! {
                
                print("con detail select action: \(row["isClick"])")
                
                guard let isCli = row["isClick"] as? String else {
                    return
                }
                
                if allconluBool {
                    
                        httppostAction(row: row)
                    
                }else {
                    
                    if  isCli == "Y" {
                        
                        httppostAction(row: row)
                        
                    }
                
                }
            }
            
        }
        
    }
//
    func httppostAction(row : [String : Any]){

        let url = URL(string: "\(AppDelegate.url)/auth/orderCancle")

        var request = URLRequest(url: url!)

        request.httpMethod = "POST"

        let param = "uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&simbol=\(coinSimbol!)&oriOrderNo=\(row["ordrNo"]!)&cancleOrderQty=\(row["remnQty"]!)&waysType=M"

        print("param : \(param)")

        let paramData = param.data(using: .utf8)

        request.httpBody = paramData

        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")

        request.setValue(String(describing: paramData!.count), forHTTPHeaderField: "Content-Length")
        
        let session = URLSession.shared

        let task = session.dataTask(with:request as URLRequest) {data, response, error in

            let result = String(data: data!, encoding: String.Encoding.utf8) as? String

            if error == nil {

                print("con detail inout success concluDetail : \(result)")
                
                self.chatMoving()

            }else{
                
                print("error:\(error)")
            
            }

        }
        
        task.resume()
        
    }
    
    func stringtoDouble(str : String) -> Double?{
        let temp = str.components(separatedBy: [","]).joined()
        let double = Double(temp)
        return double ?? 0.0
    }
 
}
