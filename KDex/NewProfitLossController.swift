//
//  NewProfitLossController.swift
//  KDex
//
//  Created by park heewon on 2018. 7. 9..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class NewProfitLossController: UIViewController, UITableViewDataSource, UITableViewDelegate, CalendarProtocol {
    
    var jsonArray : [[String : Any]]?
    
    let activity = UIActivityIndicatorView()
    
    @IBOutlet var totalProfitValue: UILabel!
    
    @IBOutlet var totalProfitKRW: UILabel!
    
    @IBOutlet var totalBuyPriceValue: UILabel!
    
    @IBOutlet var totalSellPriceValue: UILabel!
    
    @IBOutlet var totalRateValue: UILabel!
    
    @IBOutlet var totlaRatePercet: UILabel!
    
    @IBOutlet var barVi: UIView!
    
    @IBOutlet var topVi: UIView!
    
    @IBOutlet var leftVi: UIView!
    
    @IBOutlet var rightVi: UIView!
    
    @IBOutlet var contentVi: UIView!
    
    /////////////
    
    fileprivate let gregorian = Calendar(identifier: .gregorian)
    
    fileprivate let formatter : DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    var openBool = false
    
    var selectBtnTag = 0 {
        didSet {
            print("selectBtn tag : \(selectBtnTag)")
        }
    }
    
    //검색 높이 조절 레이아웃
    var height = NSLayoutConstraint()
    
    @IBOutlet var searchRangLabel: UILabel!
    
    @IBOutlet var startDateLabel: UILabel!
    
    @IBOutlet var endDateLabel: UILabel!
    
    @IBOutlet var searchViContent: UIView!
    
    @IBOutlet var searchOpenBtn: UIButton!
    
    @IBOutlet var termBtnSt: UIStackView!
    
    @IBOutlet var startDateVi: UIView!
    
    @IBOutlet var endDateVi: UIView!
    
    @IBOutlet var searchBtn: UIButton!
    
    @IBOutlet var andLabel: UILabel!
    
    @IBOutlet var startVi: UIView!
    
    @IBOutlet var endVi: UIView!
    
    @IBOutlet var selectBtns: [UIButton]!
    
    @IBAction func selectBtnsAction(_ sender: UIButton) {
        print("sender tag : \(sender.tag)")
        
        selectBtnTag = sender.tag
        
        selectBtns.forEach { (btn) in
            if btn.tag == sender.tag {
                
                btn.setTitleColor(UIColor.white, for: .normal)
                btn.backgroundColor = UIColor(rgb: 0x00b9b2)
                
            }else {
                
                btn.setTitleColor(UIColor(rgb: 0x666666), for: .normal)
                btn.backgroundColor = UIColor(rgb: 0xeaeaea)
                
            }
            
        }
        
        let endDate = endDateLabel.text ?? ""
        let tempEndDate = calendarDate(str: endDate)
        
        switch sender.tag {
        case 0:
            endDateLabel.text = makeDate(date: Date())
            startDateLabel.text = makeDate(date: Date())
            searchRangLabel.text = "\(startDateLabel.text ?? "") ~ \(endDateLabel.text ?? "")"
            self.chatMoving()
        case 1:
            let tempDate = self.gregorian.date(byAdding: .day, value: -7, to:tempEndDate) ?? Date()
            startDateLabel.text = makeDate(date: tempDate)
            searchRangLabel.text = "\(startDateLabel.text ?? "") ~ \(endDateLabel.text ?? "")"
            self.chatMoving()
        case 2:
            let tempDate = self.gregorian.date(byAdding: .month, value: -1, to:tempEndDate) ?? Date()
            startDateLabel.text = makeDate(date: tempDate)
            searchRangLabel.text = "\(startDateLabel.text ?? "") ~ \(endDateLabel.text ?? "")"
            self.chatMoving()
        case 3:
            let tempDate = self.gregorian.date(byAdding: .month, value: -3, to:tempEndDate) ?? Date()
            startDateLabel.text = makeDate(date: tempDate)
            searchRangLabel.text = "\(startDateLabel.text ?? "") ~ \(endDateLabel.text ?? "")"
            self.chatMoving()
        case 4:
            let tempDate = self.gregorian.date(byAdding: .month, value: -6, to:tempEndDate) ?? Date()
            startDateLabel.text = makeDate(date: tempDate)
            searchRangLabel.text = "\(startDateLabel.text ?? "") ~ \(endDateLabel.text ?? "")"
            self.chatMoving()
        case 5:
            let tempDate = self.gregorian.date(byAdding: .year, value: -1, to:tempEndDate) ?? Date()
            startDateLabel.text = makeDate(date: tempDate)
            searchRangLabel.text = "\(startDateLabel.text ?? "") ~ \(endDateLabel.text ?? "")"
            self.chatMoving()
        default:
            print("default")
        }
        
    }
    
    @IBAction func selectCalendaAction(_ sender: UIButton) {
        
        let popOverVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "calendarPop") as! CalendaController
        popOverVc.delegate = self
        
        popOverVc.startDate = self.calendarDate(str: startDateLabel.text ?? "")
        popOverVc.endDate = self.calendarDate(str: endDateLabel.text ?? "")
        popOverVc.startCompanyDate = self.calendarDate(str: "2010.01.01")
        
        if sender.tag == 11 {
            popOverVc.type = .start
        }else {
            popOverVc.type = .end
        }
        
        self.addChild(popOverVc)
        popOverVc.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        
        self.view.addSubview(popOverVc.view)
        popOverVc.didMove(toParent: self)
        
    }
    
    @IBAction func dropBtnAction(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "tester", message: nil, preferredStyle: .actionSheet)
        
        alert.addAction((UIAlertAction(title: "cancel", style: .cancel, handler: nil)))
        
        self.present(alert, animated: false, completion: nil)
        
    }
    
    /////////////
    
    @IBAction func searchOpenBtnAction(_ sender: Any) {
        
        if openBool {
            
            openBool = false
            
            NSLayoutConstraint.deactivate([self.height])
            self.height.constant = 1
            NSLayoutConstraint.activate([self.height])
            
            
            termBtnSt.isHidden = true
            startDateVi.isHidden = true
            endDateVi.isHidden = true
            searchBtn.isHidden = true
            andLabel.isHidden = true
            
            
        }else {
            
            openBool = true
            
            NSLayoutConstraint.deactivate([self.height])
            self.height.constant = 100
            NSLayoutConstraint.activate([self.height])
            
            termBtnSt.isHidden = false
            startDateVi.isHidden = false
            endDateVi.isHidden = false
            searchBtn.isHidden = false
            andLabel.isHidden = false
            
        }
        
    }
    
    @IBAction func searchBtnAction(_ sender: Any) {
        
        selectBtns.forEach { (btn) in
            
            btn.setTitleColor(UIColor(rgb: 0x666666), for: .normal)
            btn.backgroundColor = UIColor(rgb: 0xeaeaea)
            
        }
        
        self.chatMoving()
    }
    
    /////////////
    
    // ----- 테이블 헤더 뷰 -----
    var headerView : UIView = {
        let uv = UIView()
        uv.backgroundColor = UIColor.DPSubCompanyColor
        return uv
    }()
    
    let tb_dateLabel : UILabel = {
        let lb = UILabel()
        lb.text = "profit_day".localized
        lb.textColor = UIColor.DPmainTextColor
        lb.textAlignment = .center
        lb.baselineAdjustment = .alignCenters
        lb.font = UIFont.systemFont(ofSize: 14)
        lb.translatesAutoresizingMaskIntoConstraints = false
        
        return lb
    }()
    
    let tb_priceLabel : UILabel = {
        let lb = UILabel()
        lb.text = "profit_trans_krw".localized
        lb.textColor = UIColor.DPmainTextColor
        lb.textAlignment = .center
        lb.baselineAdjustment = .alignCenters
        lb.font = UIFont.systemFont(ofSize: 14)
        
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    let tb_countLabel : UILabel = {
        let lb = UILabel()
        lb.text = "profit_rate_per".localized
        lb.textColor = UIColor.DPmainTextColor
        lb.textAlignment = .center
        lb.baselineAdjustment = .alignCenters
        lb.font = UIFont.systemFont(ofSize: 14)
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    let blankVi : UIView = {
        let uv = UIView()
        uv.backgroundColor = UIColor.DPgrayBackColor
        uv.translatesAutoresizingMaskIntoConstraints = false
        return uv
    }()
    
    
    // ----- 테이블 헤더뷰 작업 완료 ------
    
    let footerView: UIView = {
        let uv = UIView()
        uv.backgroundColor = UIColor(rgb: 0xf3f4f5)
        return uv
    }()
    
    let footer_Label : UILabel = {
        let lb = UILabel()
        lb.text = "profit_nothing".localized
        lb.textColor = UIColor.DPmainTextColor
        lb.textAlignment = .center
        lb.font = UIFont.systemFont(ofSize: 16)
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    @IBOutlet var tableVi: UITableView!
 

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableVi.delegate = self
        tableVi.dataSource = self
        tableVi.allowsSelection = false
        
        [leftVi, rightVi].forEach {
            $0?.layer.masksToBounds = true
            $0?.layer.cornerRadius = 6
            $0?.layer.borderWidth = 1
        }
        
        ///////////
        
//        totalBuyPriceValue.sizeToFit()
//        totalSellPriceValue.sizeToFit()
        
        totalSellPriceValue.adjustsFontSizeToFitWidth = true
        totalBuyPriceValue.adjustsFontSizeToFitWidth = true
        
        endDateLabel.text = makeDate(date: Date())
        startDateLabel.text = makeDate(date: Date())
        
        searchRangLabel.text = "\(startDateLabel.text ?? "") ~ \(endDateLabel.text ?? "")"
        
        selectBtns.forEach { (btn) in
            if btn.tag == 0 {
                
                btn.setTitleColor(UIColor.white, for: .normal)
                btn.backgroundColor = UIColor(rgb: 0x00b9b2)
                
            }else {
                
                btn.setTitleColor(UIColor(rgb: 0x666666), for: .normal)
                btn.backgroundColor = UIColor(rgb: 0xeaeaea)
                
            }
        }
        
        [startVi, endVi].forEach {
            $0?.layer.borderColor = UIColor(rgb: 0xeaeaea).cgColor
            $0?.layer.borderWidth = 1
        }
        
        searchViContent.translatesAutoresizingMaskIntoConstraints = false
        
        if openBool {
            height = searchViContent.heightAnchor.constraint(equalToConstant: 128)
            NSLayoutConstraint.activate([self.height])
            
            termBtnSt.isHidden = false
            startDateVi.isHidden = false
            endDateVi.isHidden = false
            searchBtn.isHidden = false
            andLabel.isHidden = false
            
            
            self.view.bringSubviewToFront(searchViContent)
            
        }else {
            
            height = searchViContent.heightAnchor.constraint(equalToConstant: 1)
            NSLayoutConstraint.activate([self.height])
            
            termBtnSt.isHidden = true
            startDateVi.isHidden = true
            endDateVi.isHidden = true
            searchBtn.isHidden = true
            andLabel.isHidden = true
            
            
        }
        
        
        ///////////
        
        
        footerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 100)
        
//        headerView.frame = CGRect(x: 0, y: 0, width: self.tableVi.frame.width, height: 40)

        footerView.addSubview(footer_Label)
        
        footerView.add(border: .top, color: UIColor.DPLineBgColor, width: 1)
        
        tableVi.register(UINib(nibName: "profitCell", bundle: nil), forCellReuseIdentifier: "profitCellId")
        
//        jsonArray = [["date" : "20180823","tradePl" : "241351","tradeFee" : "143235","profit" : "12344","balance" : "1234214","rate" : "0.234"]]
        
        tableVi.separatorStyle = .none
        tableVi.separatorColor = UIColor.DPLineBgColor
        tableVi.backgroundColor = UIColor(rgb: 0xf3f4f5)
        
        setupconstrains()
//        setupheaderContstrains()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableVi.separatorInset = UIEdgeInsets.zero
        tableVi.layoutMargins = UIEdgeInsets.zero
        
        chatMoving()
    }
    
    func setupconstrains(){
        self.footer_Label.topAnchor.constraint(equalTo: footerView.topAnchor).isActive = true
        self.footer_Label.leadingAnchor.constraint(equalTo: footerView.leadingAnchor).isActive = true
        self.footer_Label.rightAnchor.constraint(equalTo: footerView.rightAnchor).isActive = true
        self.footer_Label.bottomAnchor.constraint(equalTo: footerView.bottomAnchor).isActive = true
        
    }
    
    //사용안하는 메소드
    func setupheaderContstrains(){
        
//        self.tb_dateLabeladd(border: .right, color: UIColor.DPtableLineColor, width: 1)
//        self.tb_priceLabel.add(border: .right, color: UIColor.DPtableLineColor, width: 1)
//        self.tb_countLabel.add(border: .right, color: UIColor.DPtableLineColor, width: 1)
//        self.tb_countLabel.textAlignment = .center
        
        self.tb_priceLabel.add(border: .left, color: UIColor.DPtableLineColor, width: 1)
        
        [tb_dateLabel, tb_priceLabel, tb_countLabel, blankVi].forEach {
            self.headerView.addSubview($0)
            $0.layer.borderColor = UIColor.DPLineBgColor.cgColor
            $0.layer.borderWidth = 1
        }
        
        NSLayoutConstraint.activate([
            
            self.tb_dateLabel.leadingAnchor.constraint(equalTo: headerView.leadingAnchor),
            self.tb_dateLabel.topAnchor.constraint(equalTo: headerView.topAnchor),
            self.tb_dateLabel.bottomAnchor.constraint(equalTo: headerView.bottomAnchor),
            self.tb_dateLabel.widthAnchor.constraint(equalToConstant: 80),
            
            self.blankVi.trailingAnchor.constraint(equalTo: headerView.trailingAnchor),
            self.blankVi.topAnchor.constraint(equalTo: headerView.topAnchor),
            self.blankVi.bottomAnchor.constraint(equalTo: headerView.bottomAnchor),
            self.blankVi.widthAnchor.constraint(equalToConstant: 30),
            
            self.tb_countLabel.rightAnchor.constraint(equalTo: blankVi.leftAnchor, constant: 1),
            self.tb_countLabel.topAnchor.constraint(equalTo: headerView.topAnchor),
            self.tb_countLabel.bottomAnchor.constraint(equalTo: headerView.bottomAnchor),
            self.tb_countLabel.widthAnchor.constraint(equalToConstant: 111),
            
            self.tb_priceLabel.trailingAnchor.constraint(equalTo: tb_countLabel.leadingAnchor, constant: 1),
            self.tb_priceLabel.topAnchor.constraint(equalTo: headerView.topAnchor),
            self.tb_priceLabel.bottomAnchor.constraint(equalTo: headerView.bottomAnchor),
//            self.tb_priceLabel.leadingAnchor.constraint(equalTo: tb_dateLabel.trailingAnchor)
            self.tb_priceLabel.leadingAnchor.constraint(equalTo: tb_dateLabel.trailingAnchor, constant: -1)
            
            ])
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return jsonArray?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let tempJson = jsonArray![indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "profitCellId", for: indexPath) as? profitCell
        
        var symbol = tempJson["symbol"] as? String ?? ""
        let buyAmt = tempJson["buyAmt"] as? String ?? ""
        let sellAmt = tempJson["sellAmt"] as? String ?? ""
        let profitAmt = tempJson["profitAmt"] as? String ?? ""
        let rate = tempJson["rate"] as? String ?? ""
        let dbRate = stringtoDouble(str: rate) ?? 0.0
        
        if symbol.contains("_") {
            let strIndex = symbol.index(of: "_")
            symbol = String(symbol.prefix(upTo: strIndex!))
        }
        
        cell?.coinSimbol.text = symbol
        cell?.evalBuyPrice.text = "\(self.intToStringComma(data: buyAmt)) KRW"
        cell?.evalSellPrice.text = "\(self.intToStringComma(data: sellAmt)) KRW"
        cell?.profite.text = self.intToStringComma(data: profitAmt) + " KRW"
        cell?.rate.text = "\(String(format: "%0.2f", dbRate))%"
        
        if dbRate < 0 {
            cell?.rate.textColor = UIColor.DPMinTextColor
        }else if dbRate == 0{
            cell?.rate.textColor = UIColor.DPmainTextColor
        }else {
            cell?.rate.textColor = UIColor.DPPlusTextColor
        }
        
        if profitAmt.contains("-") {
            cell?.profite.textColor = UIColor.DPMinTextColor
        }else if profitAmt == "0" {
            cell?.profite.textColor = UIColor.DPmainTextColor
        }else {
            cell?.profite.textColor = UIColor.DPPlusTextColor
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        let tempJson = jsonArray![indexPath.row]
//
//        let showPopVc = UIStoryboard(name: "Invest", bundle: nil).instantiateViewController(withIdentifier: "showProfitPoP") as!
//        ShowProfitController
//
//        showPopVc.baseDt = tempJson["date"] as? String ?? ""
//
//        self.addChildViewController(showPopVc)
//
//        showPopVc.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
//        self.view.addSubview(showPopVc.view)
//        showPopVc.didMove(toParentViewController: self)
        
    }
    
    func chatMoving(){
        
        activity.startAnimating()
        
        let tempStartDate = formatter.date(from: self.startDateLabel.text ?? "") ?? Date()
        let tempEndDate = formatter.date(from: self.endDateLabel.text ?? "") ?? Date()
        // 데이터 포맷터
        let dateFormatter = DateFormatter()
        // 한국 Locale
        dateFormatter.locale = Locale(identifier: "ko_KR")
        dateFormatter.dateFormat = "yyyyMMdd"
        let startDateStr = dateFormatter.string(from: tempStartDate)
        let endDateStr = dateFormatter.string(from: tempEndDate)
        
        let urlComponents = URLComponents(string:"\(AppDelegate.url)auth/profitList?uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&startDt=\(startDateStr)&endDt=\(endDateStr)")!
        
        print("urlComponents : \(urlComponents)")
        
        let amo = Alamofire.request(urlComponents)
        amo.responseJSON(completionHandler:
            { response in
                
                
                guard let tempjson = response.result.value as? [[String : Any]] else {
                    return
                }
                
                self.jsonArray = tempjson

//                let arr = self.jsonArray?.sorted(by: {
//
//                    guard let num1 = $0["date"] as? String else {
//                        return true
//                    }
//
//                    guard let num2 = $1["date"] as? String else {
//                        return true
//                    }
//
//                    return num1 > num2
//
//                })
//
//                self.jsonArray = arr
//
//                print("count : \(self.jsonArray?.count)")

                self.activity.stopAnimating()

                //테이블 리로드
                self.tableVi.reloadData()

                if self.jsonArray?.count == 0 {
                    self.footer_Label.isHidden = false
                    self.footerView.isHidden = false
                }else {
                    self.footer_Label.isHidden = true
                    self.footerView.isHidden = true
                }
                
                self.setupCaculate(arr: self.jsonArray!)
                
        })
        
    }
    
    func setupCaculate(arr: [[String : Any]]){
        
        var totalProfit = 0.0
        var totalBuyPrice = 0.0
        var totalSellPrice = 0.0
        var profitRate = 0.0
        
        arr.forEach {
            
            let buyAmt = $0["buyAmt"] as? String ?? ""
            let sellAmt = $0["sellAmt"] as? String ?? ""
            let profitAmt = $0["profitAmt"] as? String ?? ""
            
            totalBuyPrice += stringtoDouble(str: buyAmt) ?? 0.0
            totalSellPrice += stringtoDouble(str: sellAmt) ?? 0.0
            totalProfit += stringtoDouble(str: profitAmt) ?? 0.0
            
        }
        
        if totalBuyPrice == 0 {
            profitRate = 0
        }else {
            profitRate = (totalProfit / totalBuyPrice) * 100
        }
        
        totalBuyPriceValue.text = "\(Int(totalBuyPrice.roundToPlaces(places: 0)))".insertComma
        totalSellPriceValue.text = "\(Int(totalSellPrice.roundToPlaces(places: 0)))".insertComma
        totalProfitValue.text = "\(Int(totalProfit.roundToPlaces(places: 0)))".insertComma
        totalRateValue.text = String(format: "%0.2f", profitRate)
        
        
        if totalProfit > 0 {
            self.totalProfitValue.textColor = UIColor.DPPlusTextColor
            self.totalProfitKRW.textColor = UIColor.DPPlusTextColor
            self.barVi.backgroundColor = UIColor.DPPlusTextColor
            self.totalRateValue.textColor = UIColor.DPPlusTextColor
            self.totlaRatePercet.textColor = UIColor.DPPlusTextColor
            
            
            [leftVi, rightVi].forEach {
                $0?.layer.borderColor = UIColor.DPPlusTextColor.cgColor
            }
            
        }else if totalProfit == 0 {
            self.totalProfitValue.textColor = UIColor.DPmainTextColor
            self.totalProfitKRW.textColor = UIColor.DPmainTextColor
            self.barVi.backgroundColor = UIColor.DPNormalTextColor
            self.totalRateValue.textColor = UIColor.DPmainTextColor
            self.totlaRatePercet.textColor = UIColor.DPmainTextColor
            
            
            [leftVi, rightVi].forEach {
                $0?.layer.borderColor = UIColor.DPNormalTextColor.cgColor
            }
            
        }else {
            self.totalProfitValue.textColor = UIColor.DPMinTextColor
            self.totalProfitKRW.textColor = UIColor.DPMinTextColor
            self.barVi.backgroundColor = UIColor.DPMinTextColor
            self.totalRateValue.textColor = UIColor.DPMinTextColor
            self.totlaRatePercet.textColor = UIColor.DPMinTextColor
            
            
            [leftVi, rightVi].forEach {
                $0?.layer.borderColor = UIColor.DPMinTextColor.cgColor
            }
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 100
    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        return headerView
//    }
//    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 40
//    }
    
    
    //날짜 형식 포멧
    func makeTime(date : String) -> String {
        
        let df : DateFormatter = DateFormatter()
        
        df.dateFormat = "yyyyMMdd"
     
        let date_time = df.date(from: date)
        
        df.dateFormat = "yy.MM.dd"
     
        let newDate : String = df.string(from: date_time!)
        
        return newDate
    }
    
    
    func intToStringComma(data : String) -> String {
        
        let doubleData = stringtoDouble(str: data) ?? 0.0
        let intData = Int(doubleData)
        
        let strData = "\(intData)"
        
        return strData.insertComma
        
    }
    
    func stringtoDouble(str : String) -> Double?{
        let temp = str.components(separatedBy: [","]).joined()
        let double = Double(temp)
        return double ?? 0.0
    }
    
    func makeDate(date : Date) -> String {
        
        let df : DateFormatter = DateFormatter()
        df.dateFormat = "yyyy.MM.dd"
        
        let strDate = df.string(from: date) as String
        
        return strDate
    }
    
    func calendarDate(str : String) -> Date {
        
        var strDate = str
        
        strDate.replace(originalString: ".", withString: "-")
        
        let tempdate = formatter.date(from: strDate) ?? Date()
        
        return tempdate
        
    }
    
    func dateLabelEdit(date: Date, type: calendarType) {

        if type == .start {
            startDateLabel.text = makeDate(date: date)
            searchRangLabel.text = "\(makeDate(date: date)) ~ \(endDateLabel.text ?? "")"
        }else {
            endDateLabel.text = makeDate(date: date)
            searchRangLabel.text = "\(startDateLabel.text ?? "") ~ \(makeDate(date: date))"
        }
        
    }
    
}
