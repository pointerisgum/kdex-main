//
//  NewHodingController.swift
//  KDex
//
//  Created by park heewon on 2018. 6. 25..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import Kingfisher

class NewHodingController: UIViewController, UITableViewDelegate, UITableViewDataSource, RealTimeDelegate{
    
    var allAccet = 0.0 //총보유자산
    var allAmount = 0.0 //총매수
    var allTest = 0.0 //총평가
    var allTestProfit = 0.0 //평가손익
    var profitRate = 0.0 //수익률
    
    var walletArray = [[String : Any]]()

    var coinListArray = [[String : Any]]()
    
    let footerView: UIView = {
        let uv = UIView()
        uv.backgroundColor = UIColor(rgb: 0xf3f4f5)
        return uv
    }()
    
    let footer_Label : UILabel = {
        let lb = UILabel()
        lb.text = "trade_list_no_list_txt".localized
        lb.textColor = UIColor.DPmainTextColor
        lb.textAlignment = .center
        lb.font = UIFont.systemFont(ofSize: 16)
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    @IBOutlet var barVi: UIView!
    
    @IBOutlet var topVi: UIView!
    
    @IBOutlet var leftVi: UIView!
    
    @IBOutlet var rightVi: UIView!
    
    @IBOutlet var contentVi: UIView!
    
    @IBOutlet var openQty: UILabel! //보유코인
    
    @IBOutlet var openAmt: UILabel! //총 매수금액
    
    @IBOutlet var evalAmt: UILabel! //총 평가금액
    
    @IBOutlet var evalPl: UILabel! //총 평가손익
    
    @IBOutlet var evalPlRt: UILabel! //총 수익률
    
    @IBOutlet var holdingTb: UITableView!
    
    @IBOutlet var saveKrw: UILabel! //KRW 보유수량
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        evalPl.adjustsFontSizeToFitWidth = true
        openAmt.adjustsFontSizeToFitWidth = true
        evalAmt.adjustsFontSizeToFitWidth = true
        
        holdingTb.delegate = self
        holdingTb.dataSource = self
        
        holdingTb.allowsSelection = false
        
        holdingTb.backgroundColor = UIColor(rgb: 0xf3f4f5)
        
        [leftVi, rightVi].forEach {
            $0?.layer.masksToBounds = true
            $0?.layer.cornerRadius = 6
            $0?.layer.borderWidth = 1
        }
        
        footerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 100)
        footerView.backgroundColor = UIColor(rgb: 0xf3f4f5)
        
        footerView.addSubview(footer_Label)
        
//        openQty.textColor = UIColor.DPKrwTextColor
        
        self.contentVi.add(border: .bottom, color: UIColor.DPLineBgColor, width: 1)
        
        holdingTb.register(UINib(nibName: "NewHodingCell", bundle: nil), forCellReuseIdentifier: "hodingCellId")
        
        setupconstrains()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        holdingTb.layoutMargins = .zero
        holdingTb.separatorInset = .zero
        holdingTb.separatorStyle = .none
        
        walletList()
        
        TcpSocket.sharedInstance.receiverThread?.delegate = self
    }
    
    func setupconstrains(){
        self.footer_Label.topAnchor.constraint(equalTo: footerView.topAnchor).isActive = true
        self.footer_Label.leadingAnchor.constraint(equalTo: footerView.leadingAnchor).isActive = true
        self.footer_Label.rightAnchor.constraint(equalTo: footerView.rightAnchor).isActive = true
        self.footer_Label.bottomAnchor.constraint(equalTo: footerView.bottomAnchor).isActive = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.coinListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tempValue = coinListArray[indexPath.row]
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "hodingCellId", for: indexPath) as? NewHodingCell else {
            return UITableViewCell()
        }
        cell.delegate = self
        cell.cellindex = indexPath.row
        
        let imageName = tempValue["simbol"] as? String ?? ""
        let openQty = tempValue["openQty"] as? String ?? ""
        let openAmt = tempValue["openAmt"] as? String ?? ""
        let argPrice = tempValue["argPrice"] as? String ?? ""
        let evalAmt = tempValue["evalAmt"] as? String ?? ""
        let evalPlRt = tempValue["evalPlRt"] as? String ?? ""
        let evalPl = tempValue["evalPl"] as? String ?? ""
        let flag = tempValue["editFlag"] as? String ?? ""
        
        var dbEvalPlRT = stringtoDouble(str: evalPlRt) ?? 0.0
        
        if flag == "1" {
            cell.editFlaglb.text = "my_asset_eval_mod_done".localized
            cell.editFlaglb.textColor = UIColor.DPMinTextColor
        }else {
            cell.editFlaglb.text = "my_asset_eval_mod".localized
            cell.editFlaglb.textColor = UIColor.DPmainTextColor
        }
        
        
        cell.img.kf.setImage(with: AppDelegate.dataImageUrl(str: imageName))
        
        cell.coinName.text = tempValue["simbolName"] as? String ?? ""
        cell.coinSimbole.text = tempValue["simbol"] as? String ?? ""
        cell.allAccetCl.text = "\(openQty.insertComma) \(imageName)"
        cell.buyPriceCl.text = "\(self.intToStringComma(data: openAmt)) KRW"
        cell.buyEvalCl.text = "\(self.intToStringComma(data: argPrice)) KRW"
        cell.allTestCl.text = "\(self.intToStringComma(data: evalAmt)) KRW"
        cell.profitRateCl.text = "\(String(format: "%.2f", dbEvalPlRT.roundToPlaces(places: 2)).insertComma)%"
        cell.allTestProfitCl.text = "\(self.intToStringComma(data: evalPl)) KRW"
        
        
        
        let dbevalPl = stringtoDouble(str: evalPl) ?? 0.0
        let dbevalPlRt = stringtoDouble(str: evalPlRt) ?? 0.0
        
        if dbevalPl < 0 {
             cell.allTestProfitCl.textColor = UIColor.DPMinTextColor
        }else if dbevalPl == 0 {
             cell.allTestProfitCl.textColor = UIColor.DPmainTextColor
        }else {
             cell.allTestProfitCl.textColor = UIColor.DPPlusTextColor
        }
        
        if dbevalPlRt < 0 {
            cell.profitRateCl.textColor = UIColor.DPMinTextColor
        }else if dbevalPlRt == 0 {
            cell.profitRateCl.textColor = UIColor.DPmainTextColor
        }else {
            cell.profitRateCl.textColor = UIColor.DPPlusTextColor
        }
        
//        openQty : 보유수량
//        openAmt : 매수금액
//        argPrice : 매수평균가
//        evalAmt : 평가금액
//        evalPlRt : 손익율
//        evalPl : 평가손익
       
        return cell
    }
    
    func evalBtnDelegateAction(index : Int){
        
        let walletInfo = self.coinListArray[index]
        
        let showPopVc = UIStoryboard(name: "Invest", bundle: nil).instantiateViewController(withIdentifier: "showHoldingVI") as!
        ShowHoldingController
        
        showPopVc.walletInfo = walletInfo
        
        self.addChild(showPopVc)
        
        showPopVc.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        self.view.addSubview(showPopVc.view)
        showPopVc.didMove(toParent: self)
        showPopVc.superViewController = self
        
        self.view.endEditing(true)
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 144
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 100
    }
    
    func walletList() {
        
        //        activity.startAnimating()
        
        let url = "\(AppDelegate.url)/auth/walletList?simbol&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&type=2"
        
        print("walleturl \(url)")
        let amo = Alamofire.request(url)
        amo.responseJSON(completionHandler: { response in
            
            guard let tempArray = response.result.value as? [[String : Any]] else {
                return
            }
            
            self.walletArray = tempArray
            
            self.totalValueSetup()
            
        })
    }
    
    func totalValueSetup() {
        
        allAccet = 0.0 //총보유자산
        allAmount = 0.0 //총매수
        allTest = 0.0 //총평가
        allTestProfit = 0.0 //평가손익
        profitRate = 0.0 //수익률
        
        self.coinListArray.removeAll()
        
        var tempAmount = 0
        
        for row in self.walletArray {
            
            let rowsimbole = row["simbol"] as? String ?? ""
            let type = row["coinType"] as? String ?? ""
            
//            if rowsimbole != "KRW" && rowsimbole != "KDP" && type != "2" {
//            if rowsimbole != "KDP" && type != "2" {
            if type != "2" {
                
                guard let price = self.stringtoDouble(str: row["lastPrice"] as? String ?? "") else {
                    return
                }
                
                var qty = 0.0
                
                if rowsimbole == "KRW" {
                    guard let tempqty = self.stringtoDouble(str: row["dpoQty"] as? String ?? "") else {
                        return
                    }
                    
                   qty = tempqty
                    
                }else {
                    
                    guard let tempqty = self.stringtoDouble(str: row["openQty"] as? String ?? "") else {
                        return
                    }
                    
                    qty = tempqty
                    
                }
                
               
                print("coin price: \(price), qty: \(qty)")
                
                var amount = price * qty
                
                tempAmount = tempAmount + Int(amount.roundToPlaces(places: 0))
                
                if rowsimbole == "KRW" {
                    self.saveKrw.text = String(format: "%.0f", qty).insertComma + " KRW"
                }else {

                    if rowsimbole != AppDelegate.removeCoin {
                        self.coinListArray.append(row)
                    }
                    
                }
                
            }
            
        }
        
        self.allAccet = self.stringtoDouble(str: "\(tempAmount)") ?? 0
        
        self.openQty.text = "\(tempAmount)".insertComma + " KRW"
        
        coinList()
        
    }
    
    func coinList(){
        
        //        openQty : 보유수량
        //        openAmt : 매수금액
        //        argPrice : 매수평균가
        //        evalAmt : 평가금액
        //        evalPlRt : 손익율
        //        evalPl : 평가손익
        
        
        for row in coinListArray {
           
//            let value = row["argPrice"] as? String ?? "" //매수평균가
//
//            let openQty = row["openQty"] as? String ?? "" //보유수량
//            let lastPrice = row["lastPrice"] as? String ?? "" //현재가
            let evalAmt = row["evalAmt"] as? String ?? "" // 평가금액
            let openAmt = row["openAmt"] as? String ?? "" // 매수금액
            
            
//            let dbOpenQty = stringtoDouble(str: openQty) ?? 0.0
//            let dblastPrice = stringtoDouble(str: lastPrice) ?? 0.0
//            let dbargPrice = stringtoDouble(str: value) ?? 0.0
            
            var dbevalAmt = stringtoDouble(str: evalAmt) ?? 0.0
            var dbopenAmt = stringtoDouble(str: openAmt) ?? 0.0
            
//            let dbopenAmt = dbOpenQty * dbargPrice //매수금액
//            let dbevalAmt = dbOpenQty * dblastPrice //평가금액
//            let dbevalPl = dbevalAmt - dbopenAmt //손익

            self.allTest = self.allTest + dbevalAmt.roundToPlaces(places: 0)
            self.allAmount = self.allAmount + dbopenAmt.roundToPlaces(places: 0)
            
        }
        
        self.allTestProfit = self.allTest - self.allAmount
        
        if self.allAmount == 0 {
            self.profitRate = 0
        }else {
            self.profitRate = (self.allTestProfit / self.allAmount) * 100
        }
        
        self.openAmt.text = "\(Int(self.allAmount.roundToPlaces(places: 0)))".insertComma + " KRW"
        self.evalAmt.text = "\(Int(self.allTest.roundToPlaces(places: 0)))".insertComma + " KRW"
        self.evalPl.text = "\(Int(self.allTestProfit.roundToPlaces(places: 0)))".insertComma + " KRW"
        self.evalPlRt.text = "\(String(format: "%.2f", self.profitRate.roundToPlaces(places: 2)).insertComma) %"
        
        if self.allTestProfit < 0 {
            self.evalPl.textColor = UIColor.DPMinTextColor
        }else if self.allTestProfit == 0 {
            self.evalPl.textColor = UIColor(rgb: 0x444444)
        }else {
            self.evalPl.textColor = UIColor.DPPlusTextColor
        }
        
        if self.profitRate < 0 {
            
            self.evalPlRt.textColor = UIColor.DPMinTextColor
            self.barVi.backgroundColor = UIColor.DPMinTextColor
            
            [leftVi, rightVi].forEach {
                $0?.layer.borderColor = UIColor(rgb: 0x2c5ac1).cgColor
            }
            
            
        }else if self.profitRate == 0 {
            
            self.evalPlRt.textColor = UIColor(rgb: 0x444444)
            self.barVi.backgroundColor = UIColor(rgb: 0xe9a01b)
            
            [leftVi, rightVi].forEach {
                $0?.layer.borderColor = UIColor(rgb: 0xe9a01b).cgColor
            }
            
            
        }else {
            
            self.evalPlRt.textColor = UIColor.DPPlusTextColor
            self.barVi.backgroundColor = UIColor.DPPlusTextColor
            
            [leftVi, rightVi].forEach {
                $0?.layer.borderColor = UIColor(rgb: 0xe4292b).cgColor
            }
            
        }
        
        if self.walletArray.count == 0 {
            self.footer_Label.isHidden = false
            self.footerView.isHidden = false
            
        }else {
            self.footer_Label.isHidden = true
            self.footerView.isHidden = true
        }
        
        self.holdingTb.reloadData()
    }
    
    public func stringtoDouble(str : String) -> Double?{
        let temp = str.components(separatedBy: [","]).joined()
        let double = Double(temp)
        return double ?? 0.0
    }
    
    func exchangListRT(bt: NPriceTick) {
        
        print("RT sidebar real Time")
        
        var index = 0
        var num = 0
        var dic = [String : Any]()
        
        let realSymbol = bt.symbol.toString().components(separatedBy: [" "]).joined()
        let delta_rate = bt.delta_rate.toString()
        let last_volume = bt.last_volume.toString()
        let last_price = bt.last_price.toString() //현재가
        let intLast_volume = stringtoDouble(str: last_volume)
        let doubleLastPrice = stringtoDouble(str: last_price) ?? 0
        
        dic.updateValue(delta_rate, forKey: "lastPrice" )
        
        var tempAmount = 0
        
        for row in self.walletArray {
            //                let tempKrwValue = row["openAmt"] as? String ?? ""
            num = index
            
            guard let simbol = row["simbol"] as? String else {
                return
            }
//            guard let price = self.stringtoDouble(str: row["lastPrice"] as? String ?? "") else {
//                return
//            }
            guard let qty = self.stringtoDouble(str: row["openQty"] as? String ?? "") else {
                return
            }
            guard let openAmt = self.stringtoDouble(str: row["openAmt"] as? String ?? "") else {
                return
            }
//            guard let evalAmt = self.stringtoDouble(str: row["evalAmt"] as? String ?? "") else {
//                return
//            }
            
//                            var oldLatPrice = price //과거 시세
//            let oldEvalQty =  evalAmt / price //평가수량
            
            var amount = 0.0
            var tempEvalAmt = 0.0
            var tempargPrice = 0.0
            var tempEvalPl = 0.0
            var tempEvalPlRt = 0.0
            print("RT sidebar real Time simbol : \(simbol)")
            print("RT sidebar real Time realsimbol : \(realSymbol)")
//            print("RT sidebar real Time price : \(price)")
            print("RT sidebar real Time doubleLastPrice : \(doubleLastPrice)")
            print("RT sidebar real Time last_volume : \(last_volume)")
            
            if simbol == realSymbol {
                
                //        openQty : 보유수량
                //        openAmt : 매수금액
                //        argPrice : 매수평균가
                //        evalAmt : 평가금액
                //        evalPlRt : 손익율
                //        evalPl : 평가손익
                
                print("RT sidebar real Time true")
                tempEvalAmt = qty * doubleLastPrice //현재 평가액
                tempEvalPl = tempEvalAmt - openAmt
                
                if openAmt == 0 {
                    tempEvalPlRt = 0
                }else {
                    tempEvalPlRt = (tempEvalPl/openAmt) * 100
                }
                
//                self.allTest = tempEvalAmt + self.allTest
//                amount = qty * doubleLastPrice
                
                self.walletArray[num].updateValue("\(doubleLastPrice)", forKey: "lastPrice")
                self.walletArray[num].updateValue("\(tempEvalAmt)", forKey: "evalAmt")
                self.walletArray[num].updateValue("\(tempEvalPlRt)", forKey: "evalPlRt")
                self.walletArray[num].updateValue("\(tempEvalPl)", forKey: "evalPl")
                
            }

            
            index = index + 1
            
        }
//
        AppDelegate.walletListArray = self.walletArray
        
        DispatchQueue.main.async {
            
            self.totalValueSetup()

        }
        
    }
    
    func exchangDetailRT(bt: NPriceTick) {
        
    }
    
    func trandeListRT(bt: NPriceInfo) {
        
    }
    
    func openListRT(bt: NOpenInfo) {
        
    }
    
    func krwChangeRT(value: Double) {
        
    }
    
    func intToStringComma(data : String) -> String {
        
        var doubleData = stringtoDouble(str: data) ?? 0.0
        let intData = Int(doubleData.roundToPlaces(places: 0))
        
        let strData = "\(intData)"
        
        return strData.insertComma
        
    }
    
}
