//
//  WalletExchageViewController.swift
//  crevolc
//
//  Created by crovolc on 2018. 1. 9..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import Kingfisher

class WalletExchageViewController : UIViewController, UITextFieldDelegate, RealTimeDelegate, EmailAuthDelegate {
    
    @IBOutlet var exchCoinView: UIView! // 교환 코인 뷰
    @IBOutlet var exchCoinImg: UIImageView! // 교환코인 이미지
    @IBOutlet var exchCoinName: UILabel! // 교환 코인 이름
    @IBOutlet var exchCoinPrice: UILabel! // 교환 코인 한화 기준값
    @IBOutlet var eschCoinSimbol: UILabel! // 교환 코인 심볼
    @IBOutlet var exchCoinQty: UITextField! // 코인 수량 설정 필드
    @IBOutlet var exchQtyView: UIView! // 코인 수량 입력 뷰
    @IBOutlet var exchKrw: UILabel! //교환 코인 수량에 따른 한화 표시
    @IBOutlet var exchKrwLb: UILabel!
    
    @IBOutlet var exchCoinValue: UILabel! //교환 코인 전체 수량
    @IBOutlet var exchCoinKRWValue: UILabel! // 교환 코인 한화 표시
    @IBOutlet var exchDownBtn: UILabel!
    @IBOutlet var exchTopLabel: UILabel!
    
    @IBOutlet var receCoinView: UIView! // 받는 코인 뷰
    @IBOutlet var receCoinImg: UIImageView! //받는 코인 이미지
    @IBOutlet var receCoinName: UILabel! // 받는 코인 이름
    @IBOutlet var receCoinPrice: UILabel! // 받는 코인 한화 기준값
    @IBOutlet var receCoinQty: UITextField! // 받는 코인 수량 필드
    @IBOutlet var receCoinSimbol: UILabel! // 받는 코인 심볼
    @IBOutlet var receKrw: UILabel! // 받는 코인 수량에 따른 한화 표시
    @IBOutlet var receQtyView: UIView! // 받는 코인 수량 필드 뷰
    @IBOutlet var receFeeLabel: UILabel! // 수수료
    @IBOutlet var receDownBtn: UILabel!
    @IBOutlet var receTopLabel: UILabel!
    @IBOutlet var receFeeTextLabel: UILabel!
    
    @IBOutlet var exchangeBtn: UIButton!
    
    @IBOutlet var predictionFeeLabel: UILabel! //에상 전송수수료
    @IBOutlet var prefictionFeeTextLabel: UILabel! //예상전송수수료 라벨
    
    @IBOutlet var kdmpKrwLabel: UILabel!
    
    
    let activity = UIActivityIndicatorView()
    
    let walletListTb : UITableView = {
        let tb = UITableView()
        return tb
    }()
    
    var exchangeIsBool = false
    
    var minCoinQty = "0"
    
    var coinsimbol = "" //교환 코인 심볼
    
    var coinName = "" //코인 심볼 이름
    
    var pointAddsimbol = "" //충전 코인 심볼
    
    var pointSimbolpravatekey = ""
    
    var openQty = "" //교환 코인 현재 수량
    
    var pandingQty = "" //교환 코인 팬딩값
    
    var lastprice = "" //교환 코인 기준값
    
    var receiveLastprice = "" //받는 코인 기준값
    
    var receiveSimbole = "" //받는 코인 심볼
    
    var exRate = 0.0 //지갑 코인 교환률
    
    //    var ethwalletAddr = "" //ETH 지갑 주소
    
    var fee = 0.0
    
    var coinValueDelete = ""
    
    var exchangeWalletIndex = 0
    
    var currentWalletInfo = [String : Any]()
    
    var cellId = "walletCellId"
    
    override func viewDidLoad() {
        print("wallet exchange simbole : \(coinsimbol)")
        
        DBManager.sharedInstance.selecttWalletSimbol(simbol: coinsimbol)
        
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.DPViewBackgroundColor
        
        minCoinQtyCount(simbol: coinsimbol)
        initsetup()
        
        exchCoinQty.delegate = self
        walletListTb.register(WalletListCell.self, forCellReuseIdentifier: cellId)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        walletOtherSeting()
        userAmount()
        TcpSocket.sharedInstance.receiverThread?.delegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        AppDelegate.walletOtehrListArray.removeAll()
    }
    
    func initsetup() {
        
        [exchCoinView, exchQtyView, receCoinView, receQtyView].forEach {
            $0?.layer.borderColor = UIColor.DPLineBgColor.cgColor
            $0?.layer.borderWidth = 1
        }
        
        let tab = UITapGestureRecognizer(target: self, action: #selector(alertAction))
        let exchtab = UITapGestureRecognizer(target: self, action: #selector(exchAertAction))
        
        if self.coinsimbol == "KDP" && !self.exchangeIsBool  {
            
            exchDownBtn.isHidden = false
            receDownBtn.isHidden = true
            exchCoinView.addGestureRecognizer(exchtab)
            exchTopLabel.text = "내 코인지갑"
            receTopLabel.text = "충전될 포인트"
            receFeeTextLabel.text = "최소 충전 포인트"
            receFeeLabel.text = "1,000 KDP"
            receFeeLabel.textColor = UIColor(rgb: 0x8B90A6)
            
            exchangeBtn.setTitle("충 전", for: .normal)
            
            self.setupNaviTitle()
            
        }else if self.coinsimbol == "KDMP" && !self.exchangeIsBool  {
            
            exchDownBtn.isHidden = false
            receDownBtn.isHidden = true
            exchCoinView.addGestureRecognizer(exchtab)
            exchTopLabel.text = "내 코인지갑"
            receTopLabel.text = "충전될 포인트"
            receFeeTextLabel.text = "최소 충전 포인트"
            receFeeLabel.text = "200 KDMP"
            receFeeLabel.textColor = UIColor(rgb: 0x8B90A6)
            kdmpKrwLabel.text = "KDMP"
            exchangeBtn.setTitle("충 전", for: .normal)
            
            exchCoinPrice.isHidden = true
            exchKrw.isHidden = true
            exchKrwLb.isHidden = true
            
            self.setupNaviTitle()
            
        }else if self.coinsimbol == "KDA" && !self.exchangeIsBool {
            
            exchDownBtn.isHidden = false
            receDownBtn.isHidden = true
            exchCoinView.addGestureRecognizer(exchtab)
            exchTopLabel.text = "내 코인지갑"
            receTopLabel.text = "환전 KDA"
            receFeeTextLabel.text = "최소 환전"
            receFeeLabel.text = "1,000 KDA"
            receFeeLabel.textColor = UIColor(rgb: 0x8B90A6)
            
            exchangeBtn.setTitle("환 전", for: .normal)
            
            self.setupNaviTitle()
            
        }else {
            
            if self.coinsimbol == "KRW" {
                predictionFeeLabel.isHidden = true
                prefictionFeeTextLabel.isHidden = true
            }else {
                predictionFeeLabel.isHidden = false
                prefictionFeeTextLabel.isHidden = false
            }
            
            exchDownBtn.isHidden = true
            receDownBtn.isHidden = false
            receCoinView.addGestureRecognizer(tab)
            exchTopLabel.text = "exchange_send_wallet".localized
            receTopLabel.text = "exchange_recv_wallet".localized
            receFeeTextLabel.text = "wallet_manage_fee".localized //교환수수료
            receFeeLabel.text = "0.15 %"
            receFeeLabel.textColor = UIColor.DPPlusTextColor
            
            exchangeBtn.setTitle("wallet_manage_trade".localized, for: .normal)
            
        }
        
        exchCoinQty.addTarget(self, action: #selector(textfieldChageEvent), for: .editingChanged)
        exchCoinQty.addTarget(self, action: #selector(testfieldDoneAction), for: .editingDidEnd)
        //receCoinQty.addTarget(self, action: #selector(textfieldChageEvent), for: .editingChanged)
        
        activity.frame = CGRect(x: self.view.frame.width/2 - 50, y: self.view.frame.height/2 - 100, width: 100, height: 100)
        activity.style = .whiteLarge
        activity.color = UIColor.DPActivityColor
        activity.hidesWhenStopped = true
        
        view.addSubview(activity)
    }
    
    
    func initCoinInfo(){
        
        if coinsimbol == "KDP" && !self.exchangeIsBool {
            print("wallet coinsimboe KDP")
            
            self.exchSetup()
            
            var tempJson = [String : Any]()
            
            for row in AppDelegate.walletListArray {
                let simbole = row["simbol"] as? String ?? ""
                
                if coinsimbol == simbole {
                    tempJson = row
                }
            }
            
            receCoinQty.isEnabled = false
            
            receiveLastprice = tempJson["lastPrice"] as? String ?? ""
            receiveSimbole = tempJson["simbol"] as? String ?? ""
            let receQtyValue = self.receCoinQty.text ?? ""
            let receKRWValue = self.changeKRW(pri: receiveLastprice, qty: receQtyValue, isKrw: false)
            let IntReceiveLastprice = "\(Int(stringtoDouble(str: receiveLastprice) ?? 0.0))".insertComma
            
            receCoinName.text = tempJson["simbolName"] as? String ?? ""
            receCoinPrice.text = "\(IntReceiveLastprice) KRW"
            receCoinSimbol.text = receiveSimbole
            receKrw.text = receKRWValue
            receCoinImg.kf.setImage(with: AppDelegate.dataImageUrl(str: "\(receiveSimbole)"))
            
            if self.receCoinQty.text == "0" {
                self.receCoinQty.textColor = UIColor.DPPlusTextColor
            }else {
                self.receCoinQty.textColor = UIColor.DPmainTextColor
            }
            
            
            
            //            receCoinImg.image = AppDelegate.dataImage(str: "\(receiveSimbole)")
            //                UIImage(named: "\(receiveSimbole)")
            
        }else if coinsimbol == "KDMP" && !self.exchangeIsBool {
            print("wallet coinsimboe KDMP : simbole : \(eschCoinSimbol.text!)")
            
            self.exchSetup()
            
            var tempJson = [String : Any]()
            
            for row in AppDelegate.walletListArray {
                let simbole = row["simbol"] as? String ?? ""
                
                if coinsimbol == simbole {
                    tempJson = row
                }
            }
            
            receCoinQty.isEnabled = false
            
            let tempstrLast = tempJson["lastPrice"] as? String ?? ""
            
            let tempdbStrLast = stringtoDouble(str: tempstrLast) ?? 0.0
            
            receiveLastprice = tempJson["lastPrice"] as? String ?? ""
            print("test KDMP lastprice : \(receiveLastprice)")
            
            receiveSimbole = tempJson["simbol"] as? String ?? ""
            let receQtyValue = self.receCoinQty.text ?? ""
            let receKRWValue = self.changeKRW(pri: receiveLastprice, qty: receQtyValue, isKrw: false)
//            let IntReceiveLastprice = "\(Int(stringtoDouble(str: receiveLastprice) ?? 0.0))".insertComma
            
            let IntReceiveLastprice = 200
            
            receCoinName.text = tempJson["simbolName"] as? String ?? ""
            receCoinPrice.text = "\(IntReceiveLastprice) \(eschCoinSimbol.text!)"
            receCoinSimbol.text = receiveSimbole
            receKrw.text = receKRWValue
            receCoinImg.kf.setImage(with: AppDelegate.dataImageUrl(str: "\(receiveSimbole)"))
            
            if self.receCoinQty.text == "0" {
                self.receCoinQty.textColor = UIColor.DPPlusTextColor
            }else {
                self.receCoinQty.textColor = UIColor.DPmainTextColor
            }
            
            //            receCoinImg.image = AppDelegate.dataImage(str: "\(receiveSimbole)")
            //                UIImage(named: "\(receiveSimbole)")
            
        }else if coinsimbol == "KDA" && !self.exchangeIsBool {
            
            print("wallet coinsimboe KDA")
            
            self.exchSetup()
            
            var tempJson = [String : Any]()
            
            for row in AppDelegate.walletListArray {
                let simbole = row["simbol"] as? String ?? ""
                
                if coinsimbol == simbole {
                    tempJson = row
                }
            }
            
            receCoinQty.isEnabled = false
            
            receiveLastprice = tempJson["lastPrice"] as? String ?? ""
            receiveSimbole = tempJson["simbol"] as? String ?? ""
            let receQtyValue = self.receCoinQty.text ?? ""
            let receKRWValue = self.changeKRW(pri: receiveLastprice, qty: receQtyValue, isKrw: false)
            let IntReceiveLastprice = "\(Int(stringtoDouble(str: receiveLastprice) ?? 0.0))".insertComma
            
            receCoinName.text = tempJson["simbolName"] as? String ?? ""
            receCoinPrice.text = "\(IntReceiveLastprice) KRW"
            receCoinSimbol.text = receiveSimbole
            receKrw.text = receKRWValue
            receCoinImg.kf.setImage(with: AppDelegate.dataImageUrl(str: "\(receiveSimbole)"))
            
            if self.receCoinQty.text == "0" {
                self.receCoinQty.textColor = UIColor.DPPlusTextColor
            }else {
                self.receCoinQty.textColor = UIColor.DPmainTextColor
            }
            
            
            
            //            receCoinImg.image = AppDelegate.dataImage(str: "\(receiveSimbole)")
            //                UIImage(named: "\(receiveSimbole)")
            
        }else {
            
            print("wallet coinsimboe KDP Other")
            
            for row in AppDelegate.walletListArray {
                let simbole = row["simbol"] as? String ?? ""
                
                if coinsimbol == simbole {
                    self.currentWalletInfo = row
                }
            }
            
            print("wallet mywallet info : \(AppDelegate.myWalletInfo)")
            print("wallet current info : \(self.currentWalletInfo)")
            
            //lastprice = currentWalletInfo["lastPrice"] as? String ?? ""
            let simbolname = currentWalletInfo["simbolName"] as? String ?? ""
            
            if coinsimbol == "KRW" || coinsimbol == "KDP" {
                lastprice = "1"
            }
            
            let Intlastprice = "\(Int(stringtoDouble(str: lastprice) ?? 0.0))".insertComma
            // let openQty = currentWalletInfo["openQty"] as? String ?? "0"
            
            
            let KRWValue = self.changeKRW(pri: lastprice, qty: openQty, isKrw: true)
            let KRWValueFirst = self.changeKRWFirst(pri: lastprice, qty: openQty, isKrw: true)
            
            let exchangQtyValue = self.exchCoinQty.text ?? ""
            let exchangKRWValue = self.changeKRW(pri: lastprice, qty: exchangQtyValue, isKrw: false)
            //let pandingQty = currentWalletInfo["pdngQty"] as? String ?? "0"
            let pandingKRW = self.changeKRW(pri: lastprice, qty: pandingQty, isKrw: false)
            let doubleAmountCoinValue = stringtoDouble(str: openQty)
            let doublePaddingQty = stringtoDouble(str: pandingQty) ?? 0
            
            var msg = ""
            
            if AppDelegate.appLang != "en_US" {
                msg = "최소 교환 수량 \(minCoinQty) 입니다."
            }else {
                msg = "Minimum replacement quantity \(minCoinQty)."
            }
            
            exchCoinQty.attributedPlaceholder = NSAttributedString(string: msg, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.DPSellDefaultColor])
            
            var coinValueQtyStr = ""
            
            if coinsimbol == "KRW" || coinsimbol == "KDP" || coinsimbol == "KDMP" {
                coinValueQtyStr = String(format: "%.0f", doubleAmountCoinValue!).insertComma
                
            }else {
                coinValueQtyStr = String(format: "%.8f", doubleAmountCoinValue!)
            }
            
            print("coin Value : \(coinValueQtyStr)")
            
            let attrStr = NSMutableAttributedString(string: "\(coinsimbol) \(coinValueQtyStr)", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.DPmainTextColor])
            
            //        if pandingQty != "" && pandingQty != "0" {
            //            attrStr.append(NSAttributedString(string: "(\(String(format: "%.8f", doublePaddingQty)))", attributes: [NSAttributedString.Key.foregroundColor : UIColor.DPBuyDefaultColor]))
            //        }
            
            let krwAttrStr = NSMutableAttributedString(string: KRWValue , attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14)])
            let krwAttrStrFirst = NSMutableAttributedString(string: KRWValueFirst , attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14)])
            
            //        if pandingKRW != "" && pandingKRW != "0" {
            //            krwAttrStr.append(NSAttributedString(string: "(\(pandingKRW))", attributes: [NSAttributedString.Key.foregroundColor : UIColor.DPBuyDefaultColor]))
            //        }
            
            exchCoinImg.kf.setImage(with: AppDelegate.dataImageUrl(str: "\(coinsimbol)"))
            //          exchCoinImg.image = AppDelegate.dataImage(str: "\(coinsimbol)")
            //          UIImage(named: "\(coinsimbol)")
            exchCoinName.text = simbolname
            exchCoinPrice.text = "\(Intlastprice) KRW"
            exchCoinValue.attributedText = attrStr
            exchCoinKRWValue.attributedText = krwAttrStrFirst
            eschCoinSimbol.text = coinsimbol
            exchKrw.text = exchangKRWValue.insertComma
            
            predictionCoinQtyCount(simbol: coinsimbol, lastprice: lastprice)
            
            self.receiveSetup()
            
        }
    }
    
    @objc fileprivate func alertAction(){
        let contentVC = WalletListController()
        
        contentVC.delegate = self
        
        contentVC.cellSelectReActionBool = false
        
        let alert = UIAlertController(title: "pop_wallet".localized, message: nil, preferredStyle: .alert)
        
        alert.setValue(contentVC, forKeyPath: "contentViewController")
        
        let action = UIAlertAction(title: "OK", style: .default) { (_) in
            self.receiveSetup()
        }
        
        alert.addAction(action)
        
        self.present(alert, animated: false, completion: nil)
        
    }
    
    @objc func exchAertAction() {
        
        let contentVC = WalletListController()
        
        contentVC.delegate = self
        
        contentVC.cellSelectReActionBool = true
        
        let alert = UIAlertController(title: "pop_wallet".localized, message: nil, preferredStyle: .alert)
        
        alert.setValue(contentVC, forKeyPath: "contentViewController")
        
        let action = UIAlertAction(title: "OK", style: .default) { (_) in
            self.exchSetup()
        }
        
        alert.addAction(action)
        
        self.present(alert, animated: false, completion: nil)
        
    }
    
    func testalert(index: IndexPath){
        print("wallet index row : \(index.row)")
        self.exchangeWalletIndex = index.row
        self.exchCoinQty.text = "0"
        
    }
    
    func changeKRW(pri: String, qty : String, isKrw: Bool) -> String {
        var str = ""
        
        guard let pdouble = stringtoDouble(str: pri) else{
            return ""
        }
        guard let qdouble = stringtoDouble(str: qty) else {
            return ""
        }
        
        var roundAmount = pdouble * qdouble
        let amount = Int(roundAmount.roundToPlaces(places: 0))
        
        if isKrw {
            let tempStr = "\(amount)".insertComma
            str = "\(tempStr) KRW"
        }else{
            str = "\(amount)".insertComma
        }
        
        return str
    }
    
    func changeKRWFirst(pri: String, qty : String, isKrw: Bool) -> String {
        var str = ""
        
        guard let pdouble = stringtoDouble(str: pri) else{
            return ""
        }
        guard let qdouble = stringtoDouble(str: qty) else {
            return ""
        }
        
        var roundAmount = pdouble * qdouble
        let amount = Int(roundAmount.roundToPlaces(places: 0))
        
        if isKrw {
            let tempStr = "\(amount)".insertComma
            str = "KRW \(tempStr) "
        }else{
            str = "\(amount)".insertComma
        }
        
        return str
    }
    
    func stringtoDouble(str : String) -> Double?{
        let temp = str.components(separatedBy: [","]).joined()
        let double = Double(temp)
        return double ?? 0.0
    }
    
    @objc func testfieldDoneAction() {
        let temp = exchCoinQty.text ?? ""
        guard let tempInt = stringtoDouble(str: temp) else {
            return
        }
        
        if temp == "" {
            exchCoinQty.text = "0"
        }else if self.coinsimbol == "KRW" || self.coinsimbol == "KDP" || self.coinsimbol == "KDMP" {
            exchCoinQty.text = "\(String(format: "%.0f", tempInt))".insertComma
        }else {
            exchCoinQty.text = "\(String(format: "%.8f", tempInt))"
        }
        
    }
    
    @objc func textfieldChageEvent() {
        
        let num1 = self.stringtoDouble(str: openQty)!
        let num3 = self.stringtoDouble(str: self.exchCoinQty.text!)!
        
        
        print("wallet excha : \(num1),  and \(num3)")
        
        if num3 > num1 {
            self.exchCoinQty.text = "\(num1)"
        }
        
        let tempstr = self.exchCoinQty.text!
        let doubleTempStr = stringtoDouble(str: self.exchCoinQty.text!)
        let strIndex = self.exchCoinQty.text?.index(of: ".")
        var strLen = ""
        
        if strIndex != nil {
            strLen = String(tempstr.suffix(from: strIndex!))
        }
        
        if strLen.count > 9 {
            let tempsttr = self.stringtoDouble(str: (tempstr.substring(from: 0, to: ((tempstr.count)) - 1))) ?? 0
            exchCoinQty.text = "\(String(format: "%.8f", tempsttr))"
        }
        
        exchangeCountAction()
        
    }
    
    
    @IBAction func exchangeBtnAction(_ sender: Any) {
        
        let exchangQtyfl = self.exchCoinQty.text ?? "0"
        let receQtyfl = self.receCoinQty.text ?? "0"
        
        let walletPk = AlertVO.shared.privKey1
        let pointPk = AlertVO.shared.pointAddprivKey
        
        let tmpRate = String(format: "%.8f", self.exRate)
        
        //        var tempurl = ""
        //
        //        var param = ""
        //
        //        if coinsimbol == "KDP" && !self.exchangeIsBool {
        //            tempurl = "\(AppDelegate.url)/auth/walletExchange"
        //
        //            param = "uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&fromSimbol=\(pointAddsimbol)&toSimbol=\(receiveSimbole)&qty=\(self.exchCoinQty.text ?? "0")&rate=\(tmpRate)&waysType=M&privKey=\(pointPk ?? "")"
        //
        //
        //
        //        }else if coinsimbol == "KDA" && !self.exchangeIsBool {
        //
        //            print("testert kda")
        //
        ////            self.sendTakeAction()
        //
        //            return
        //
        //        }else {
        //
        //            tempurl = "\(AppDelegate.url)/auth/walletExchange"
        //
        //            param = "uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&fromSimbol=\(coinsimbol)&toSimbol=\(receiveSimbole)&qty=\(self.exchCoinQty.text ?? "0")&rate=\(tmpRate)&waysType=M&privKey=\(walletPk ?? "")"
        //
        //        }
        //
        //
        //        print("wallet param : \(param)")
        //        let url = URL(string: "\(tempurl)")
        //
        //        var request = URLRequest(url: url!)
        //
        //        request.httpMethod = "POST"
        //
        //        print("wallet exchange param : \(param)")
        //        print("wallet exchange point privkey : \(pointPk)")
        //        print("wallet exchange privkey : \(walletPk)")
        //        let paramData = param.data(using: .utf8)
        //
        //        request.httpBody = paramData
        //
        //        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        //
        //        request.setValue(String(describing: paramData!.count), forHTTPHeaderField: "Content-Length")
        
        let intCoin = stringtoDouble(str: exchangQtyfl) ?? 0.0
        let receCoin = stringtoDouble(str: receQtyfl) ?? 0.0
        let minCoinAble = stringtoDouble(str: minCoinQty) ?? 0.0
        let maxCoinAble = stringtoDouble(str: openQty) ?? 0.0
        
        print("wallet exchangcoin : \(exchangQtyfl)")
        
        print("wallet exchangcoin max: \(maxCoinAble)")
        
        print("wallet exchangcoin min: \(minCoinAble)")
        
        if exchangQtyfl == "" || intCoin == 0.0 {
            let alert = UIAlertController(title: "pop_walletSend".localized, message: "pop_wallet_input_volum".localized, preferredStyle: .alert)
            let action = UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: false, completion: nil)
            //        }else if intCoin < minCoinAble && coinsimbol != "KDP" {
        }else if intCoin < minCoinAble {
            let alert = UIAlertController(title: "pop_walletSend".localized, message: "pop_wallet_min_not_send".localized, preferredStyle: .alert)
            let action = UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: false, completion: nil)
        }else if intCoin > maxCoinAble {
            let alert = UIAlertController(title: "pop_walletSend".localized, message: "pop_wallet_have_coin_fot".localized, preferredStyle: .alert)
            let action = UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: false, completion: nil)
        }else if receCoin < 1000.0 && coinsimbol == "KDP" && !self.exchangeIsBool   {
            let alert = UIAlertController(title: "pop_walletSend".localized, message: "pop_wallet_able_point".localized, preferredStyle: .alert)
            let action = UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: false, completion: nil)
            
        }else if (receCoin / 200 ) < 200.0 && coinsimbol == "KDMP" && !self.exchangeIsBool   {
            let alert = UIAlertController(title: "pop_walletSend".localized, message: "pop_wallet_able_KDMP_point".localized, preferredStyle: .alert)
            let action = UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: false, completion: nil)
            
        }
            
            //        else if receCoin < 1000.0 && coinsimbol == "KDA" && !self.exchangeIsBool   {
            //            //추가 확인요
            //            let alert = UIAlertController(title: "pop_walletSend".localized, message: "pop_wallet_able_point".localized, preferredStyle: .alert)
            //            let action = UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil)
            //            alert.addAction(action)
            //            self.present(alert, animated: false, completion: nil)
            //        }
            
        else {
            
            let alert = UIAlertController(title: "pop_wallet".localized, message: "코인을 교환 하시겠습니까? ", preferredStyle: .alert)
            
            let okBtn = UIAlertAction(title: "common_yes_btn".localized, style: .default) { (_) in
                
                //                let passPopVc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "sendPassVi") as! SendLockController
                if self.coinsimbol == "KDMP" {
                    
                    self.requstAction()
                    
                }else {
                    
                    let passPopVc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "emailAuthID") as! EmailAuthController
                    
                    passPopVc.authdelegate = self
                    passPopVc.alertBool = true
                    
                    self.addChild(passPopVc)
                    
                    passPopVc.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
                    self.view.addSubview(passPopVc.view)
                    passPopVc.didMove(toParent: self)
                    
                    self.view.endEditing(true)
                    
                }
                
                
            }
            
            let cancelBtn = UIAlertAction(title: "common_no_btn".localized, style: .cancel, handler: nil)
            
            alert.addAction(okBtn)
            alert.addAction(cancelBtn)
            
            self.present(alert, animated: false, completion: nil)
            
            //        self.activity.startAnimating()
            //        //laucnhing
            //        let session = URLSession.shared
            //        let task = session.dataTask(with:request as URLRequest) {data, response, error in
            //
            //            if error == nil {
            //
            //                DispatchQueue.main.async {
            //
            //                    do {
            //                        let object = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
            //
            //                        guard let jsonObject = object else { return }
            //                        print("inout result msg : \(jsonObject["returnMsg"] ?? "")")
            //
            //                        let msg = "\(jsonObject["returnMsg"] ?? "")"
            //
            //                        let alert = UIAlertController(title: "pop_wallet_exchange".localized, message: msg, preferredStyle: .alert)
            //                        alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: { (_) in
            //                            //self.chatMoving()
            //                        }))
            //
            //                        self.present(alert, animated: false, completion: nil)
            //
            //                        self.exchCoinQty.text = ""
            //                        self.receCoinQty.text = ""
            //
            //                        self.activity.stopAnimating()
            //                    }catch{
            //
            //                        let msg = "pop_wallet_registert_not_reply".localized
            //                        let alert = UIAlertController(title: "pop_wallet_exchange".localized, message: msg, preferredStyle: .alert)
            //                        alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
            //                        self.present(alert, animated: false, completion: nil)
            //
            //                        self.activity.stopAnimating()
            //                    }
            //
            //                    self.view.endEditing(true)
            //
            //                }
            //
            //            }else{
            //                print("error:\(error)")
            //
            //                self.activity.stopAnimating()
            //            }
            //
            //        }
            //        task.resume()
            
        }
        
    }
    
    
    func receiveSetup() {
        print("wallet receiveSetup")
        
        if AppDelegate.walletOtehrListArray.count != 0 {
            
            print("wallet receive count : \(AppDelegate.walletOtehrListArray.count)")
            
            let tempJson = AppDelegate.walletOtehrListArray[exchangeWalletIndex]
            
            receCoinQty.isEnabled = false
            
            receiveLastprice = tempJson["lastPrice"] as? String ?? ""
            receiveSimbole = tempJson["simbol"] as? String ?? ""
            
            DBManager.sharedInstance.selecttWalletSimbol(simbol: receiveSimbole)
            
            let receQtyValue = self.receCoinQty.text ?? ""
            let receKRWValue = self.changeKRW(pri: receiveLastprice, qty: receQtyValue, isKrw: false)
            let IntReceiveLastprice = "\(Int(stringtoDouble(str: receiveLastprice) ?? 0.0))".insertComma
            
            receCoinName.text = tempJson["simbolName"] as? String ?? ""
            receCoinPrice.text = "\(IntReceiveLastprice) KRW"
            receCoinSimbol.text = receiveSimbole
            receKrw.text = receKRWValue
            receCoinImg.image = AppDelegate.dataImage(str: "\(receiveSimbole)")
            //                    UIImage(named: "\(receiveSimbole)")
            
            self.exchangeCountAction()
        }
        
    }
    
    func exchSetup() {
        print("wallet exchSetup")
        
        if AppDelegate.walletOtehrListArray.count != 0 {
            
            let tempJson = AppDelegate.walletOtehrListArray[exchangeWalletIndex]
            
            self.currentWalletInfo = tempJson
            
            print("wallet mywallet info : \(AppDelegate.myWalletInfo)")
            print("wallet current info : \(self.currentWalletInfo)")
            
            openQty = currentWalletInfo["wthrAbleQty"] as? String ?? ""
            pandingQty = currentWalletInfo["dpoPdngQty"] as? String ?? ""
            lastprice = currentWalletInfo["lastPrice"] as? String ?? ""
            let currentsimbol = currentWalletInfo["simbol"] as? String ?? ""
            pointAddsimbol = currentsimbol
            
            DBManager.sharedInstance.selectPointAddSimbol(simbol: currentsimbol)
            
            var currentLastPrice = currentWalletInfo["lastPrice"] as? String ?? ""
            
            
            //임시 BTR 한화 가격이 없어 1로 설정 작업
            if currentsimbol == "BTR" || currentsimbol == "EYEAL" {
                currentLastPrice = "1"
                
                exchCoinPrice.isHidden = true
                exchKrw.isHidden = true
                exchKrwLb.isHidden = true
                exchCoinKRWValue.isHidden = true
                
            }else {
                
                exchCoinPrice.isHidden = false
                exchKrw.isHidden = false
                exchKrwLb.isHidden = false
                exchCoinKRWValue.isHidden = false
            }
            
            
            let currentOpenQty = currentWalletInfo["wthrAbleQty"] as? String ?? ""
            let currentPandingQty = currentWalletInfo["dpoPdngQty"] as? String ?? ""
            let simbolname = currentWalletInfo["simbolName"] as? String ?? ""
            let Intlastprice = "\(Int(stringtoDouble(str: currentLastPrice) ?? 0.0))".insertComma
            // let openQty = currentWalletInfo["openQty"] as? String ?? "0"
            
            
            let KRWValue = self.changeKRW(pri: currentLastPrice, qty: currentOpenQty, isKrw: true)
            let exchangQtyValue = self.exchCoinQty.text ?? ""
            let exchangKRWValue = self.changeKRW(pri: currentLastPrice, qty: exchangQtyValue, isKrw: false)
            //let pandingQty = currentWalletInfo["pdngQty"] as? String ?? "0"
            let pandingKRW = self.changeKRW(pri: currentLastPrice, qty: currentPandingQty, isKrw: false)
            let doubleAmountCoinValue = stringtoDouble(str: currentOpenQty)
            let doublePaddingQty = stringtoDouble(str: currentPandingQty) ?? 0
            
            //            exchCoinQty.attributedPlaceholder = NSAttributedString(string: "최소 교환 수량 \(minCoinQty) 입니다.", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.DPSellDefaultColor])
            //
            let attrStr = NSMutableAttributedString(string: "\(currentsimbol) \(String(format: "%.8f", doubleAmountCoinValue!))", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.DPmainTextColor])
            
            //        if pandingQty != "" && pandingQty != "0" {
            //            attrStr.append(NSAttributedString(string: "(\(String(format: "%.8f", doublePaddingQty)))", attributes: [NSAttributedString.Key.foregroundColor : UIColor.DPBuyDefaultColor]))
            //        }
            
            let krwAttrStr = NSMutableAttributedString(string: KRWValue , attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14)])
            
            //        if pandingKRW != "" && pandingKRW != "0" {
            //            krwAttrStr.append(NSAttributedString(string: "(\(pandingKRW))", attributes: [NSAttributedString.Key.foregroundColor : UIColor.DPBuyDefaultColor]))
            //        }
            
            exchCoinImg.image = AppDelegate.dataImage(str: "\(currentsimbol)")
            //                UIImage(named: "\(currentsimbol)")
            exchCoinName.text = simbolname
            exchCoinPrice.text = "\(Intlastprice) KRW"
            
            
            exchCoinValue.attributedText = attrStr
            exchCoinKRWValue.attributedText = krwAttrStr
            eschCoinSimbol.text = currentsimbol
            exchKrw.text = exchangKRWValue.insertComma
            
            minCoinQtyCount(simbol: currentsimbol)
            
            if coinsimbol == "KDMP" {
                
                predictionCoinQtyCount(simbol: "ETH", lastprice: currentLastPrice)
                
            }else {
                
                predictionCoinQtyCount(simbol: eschCoinSimbol.text ?? "", lastprice: currentLastPrice)
                
            }
            
            self.exchangeCountAction()
            
        }
        
    }
    
    func minCoinQtyCount(simbol : String){
        
        guard let exCoinSimbole = self.eschCoinSimbol.text else {
            return
        }
        
        let url = "\(AppDelegate.url)/auth/getSendFee?uid=\(AppDelegate.uid)&sessionid=\(AppDelegate.sessionid)&simbol=\(simbol)&procType=2&calcType=0&gasLimit=0&gwei=0"
        
        print("minCoinQtyCount \(url)")
        let amo = Alamofire.request(url)
        amo.responseJSON{ response in
            if response.result.isSuccess == true {
                
                let result = response.result.value as? [String : Any]
                
                self.minCoinQty = result!["minQty"] as? String ?? "0"
                print("wallet minCoinQty : \(self.minCoinQty)")
                self.fee = self.stringtoDouble(str: result!["fee"] as? String ?? "0") ?? 0
                
                if self.coinsimbol == "KDP" && !self.exchangeIsBool {
                    self.receFeeLabel.text = "1,000 KDP"
                }else if self.coinsimbol == "KDMP" && !self.exchangeIsBool {
                    if exCoinSimbole == "EYEAL" {
                        self.receCoinPrice.text = "1 \(self.eschCoinSimbol.text!) = 1000 KDMP"
                    }else {
                        self.receCoinPrice.text = "1 \(self.eschCoinSimbol.text!) = 200 KDMP"
                    }
                    self.receFeeLabel.text = "200 \(exCoinSimbole)"
                }else if self.coinsimbol == "KDA" && !self.exchangeIsBool {
                    self.receFeeLabel.text = "\(String(format: "%0.2f", self.fee)) %"
                }else{
                    self.receFeeLabel.text = "\(String(format: "%0.2f", self.fee)) %"
                }
                
            }
        }
    }
    
    func predictionCoinQtyCount(simbol : String, lastprice : String){
        
        let url = "\(AppDelegate.url)/auth/getSendFee?uid=\(AppDelegate.uid)&sessionid=\(AppDelegate.sessionid)&simbol=\(simbol)&procType=1&calcType=2&gasLimit=0&gwei=0"
        
        print("minCoinQtyCount \(url)")
        let amo = Alamofire.request(url)
        amo.responseJSON{ response in
            if response.result.isSuccess == true {
                
                let result = response.result.value as? [String : Any]
                
                print("prediction \(lastprice)" )
                
                let dbLastPrice = self.stringtoDouble(str: lastprice) ?? 0.0
                let preFee = self.stringtoDouble(str: result!["fee"] as? String ?? "0") ?? 0
                
                var preFeeWon = dbLastPrice * preFee
                
                let strPreFeeWon = "\(Int(preFeeWon.roundToPlaces(places: 0)))".insertComma
                
                var tempValueStr = ""
                
                if self.coinsimbol == "KRW" || self.coinsimbol == "KDP"{
                    
                    tempValueStr = String(format: "%.0f \(simbol)", preFee).insertComma
                    
                }else {
                    tempValueStr = String(format: "%.8f \(simbol)", preFee)
                }
                
                
                let attrStr = NSMutableAttributedString(attributedString: NSAttributedString(string: tempValueStr))
                
                if self.coinsimbol != "KDMP" {
                    attrStr.append(NSAttributedString(string: " (₩ \(strPreFeeWon))"))
                }
                
                self.predictionFeeLabel.attributedText = attrStr
                
            }
        }
    }
    
    //테스트 확인중
    func exchangeCountAction(){
        
        print("exh lastprice : \(lastprice), receive : \(receiveLastprice)")
        
        var recevePrice = stringtoDouble(str: receiveLastprice) ?? 0.0
        
        if self.coinsimbol == "KDMP" && !self.exchangeIsBool {
            recevePrice = 200.0
        }
        
        let crrentPrice = stringtoDouble(str: lastprice) ?? 0.0
        
        let Qty = stringtoDouble(str: exchCoinQty.text ?? "") ?? 0.0
        var amount = 0.0
        
        print("exh price : \(crrentPrice), receve: \(recevePrice), exchCoinQty: \(Qty)")
        
        var exchQtyKrw = Qty * crrentPrice
        let exchKRWStr = "\(Int(exchQtyKrw.roundToPlaces(places: 0)))"
        exchKrw.text = exchKRWStr.insertComma
        
        if recevePrice != 0.0 {
            
            exRate = crrentPrice / recevePrice
            
            print("exh fee : \(self.fee)")
            
            amount = Qty * exRate * ( 1 - self.fee / 100)
            
            print("exh amoun : \(amount)")
            
            if amount == 0.0 {
                receCoinQty.text = "0"
            }else {
                if coinsimbol == "KDP" {
                    
                    receCoinQty.text = "\(String(format: "%.0f", exchQtyKrw.roundToPlaces(places: 0)).insertComma)"
                    
                    if exchQtyKrw < 1000.0 {
                        self.receCoinQty.textColor = UIColor.DPPlusTextColor
                    }else {
                        self.receCoinQty.textColor = UIColor.DPmainTextColor
                    }
                    
                }else if coinsimbol == "KDMP" {
                    
                    print("exh amoun KDMP")
                    
                    receCoinQty.text = "\(String(format: "%.0f", Qty * 200).insertComma)"
                    
                    if Qty * 200 < 40000.0
                    {
                        print("exh amoun KDMP : \(exchQtyKrw)")
                        self.receCoinQty.textColor = UIColor.DPPlusTextColor
                    }else {
                        print("exh amoun KDMP : \(exchQtyKrw)")
                        self.receCoinQty.textColor = UIColor.DPmainTextColor
                    }
                    
                    
                }else {
                    receCoinQty.text = "\(String(format: "%.8f", amount))"
                }
            }
            
        }else {
            receCoinQty.text = "0"
        }
        
        var recDoubleAmount = amount * recevePrice
        let recKRWStr = "\(Int(recDoubleAmount.roundToPlaces(places: 0)))"
        
        if coinsimbol == "KDP"{
            
            receKrw.text = "\(String(format: "%.0f", exchQtyKrw.roundToPlaces(places: 0)).insertComma)"
            
        }else if coinsimbol == "KDMP" {
            
            receKrw.text = "\(String(format: "%.0f", Qty * 200).insertComma)"
            
        }else{
            receKrw.text = recKRWStr.insertComma
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        print("text gooo main is gooodi ")
        var allowCharatorSet = CharacterSet.decimalDigits
        let charaterset = CharacterSet(charactersIn: string)
        allowCharatorSet.insert(charactersIn: ".")
        
        return allowCharatorSet.isSuperset(of: charaterset)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        return true
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        
    }
    
    func userAmount() {
        
        let activity = UIActivityIndicatorView()
        activity.style = .whiteLarge
        activity.color = UIColor.DPActivityColor
        activity.hidesWhenStopped = true
        activity.startAnimating()
        
        var url = ""
        
        if coinsimbol == "KRW" {
            url = "\(AppDelegate.url)auth/userAmount?market=KRW&simbol=\(coinsimbol)&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)"
        }else if coinsimbol == "KDP"{
            url = "\(AppDelegate.url)auth/userAmount?market=KDP&simbol=\(coinsimbol)&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)"
        }else if coinsimbol == "KDMP"{
            url = "\(AppDelegate.url)auth/userAmount?market=KDMP&simbol=\(coinsimbol)&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)"
            
        }else{
            url = "\(AppDelegate.url)auth/userAbleAmount?simbol=\(coinsimbol)&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)"
        }
        
        print("wallet url : \(url)")
        let amo = Alamofire.request(url)
        amo.responseJSON(completionHandler: { response in
            
            print("wallet userAbleAmount")
            var buyjson = response.result.value as? [String : Any]
            
            if self.coinsimbol != "KDP" && self.coinsimbol != "KRW" && self.coinsimbol != "KDMP"{
                
                self.openQty = buyjson?["wthrAbleQty"] as? String ?? ""
                self.pandingQty = buyjson?["dpoPdngQty"] as? String ?? ""
                
            }else{
                self.openQty = buyjson?["wthrAbleAmount"] as? String ?? ""
                //                self.pandingQty = buyjson?["dpoPdngQty"] as? String ?? ""
            }
            
            self.initCoinInfo()
            activity.stopAnimating()
        })
    }
    
    func walletOtherSeting(){
        print("wallet other seting ")
        
        AppDelegate.walletOtehrListArray.removeAll()
        
        for row in AppDelegate.walletListArray {
            
            let simbole = row["simbol"] as? String ?? ""
            let coinType = row["coinType"] as? String ?? ""
            
            if coinsimbol != simbole && coinType != "2" && coinsimbol != "KDMP"{
                
                if coinsimbol == "KRW" {
                    
                    if simbole == "ETH" {
                        AppDelegate.walletOtehrListArray.append(row)
                    }
                    
                }else if coinsimbol == "KDP" || coinsimbol == "KDA" {
                    if simbole == "KRW" || simbole == "ETH" {
                        AppDelegate.walletOtehrListArray.append(row)
                    }
                    
                }else if coinsimbol == "ETH" {
                    if simbole == "KRW" {
                        AppDelegate.walletOtehrListArray.append(row)
                    }
                }else if coinsimbol == "520"{
                    if simbole == "BTC" || simbole == "ETH" {
                        AppDelegate.walletOtehrListArray.append(row)
                    }
                }
                
            }else {
                
                if coinsimbol == "KDMP" {
                    if simbole == "KDA" || simbole == "BTR" || simbole == "EYEAL" {
                        
                        AppDelegate.walletOtehrListArray.append(row)
                    }
                }
                
            }
        }
        
        print("wallet other count : \(AppDelegate.walletOtehrListArray.count)")
    }
    
    func exchangListRT(bt: NPriceTick) {
        
        print("RT : wallet list --------------------------------------")
        
        var index = 0
        var num = 0
        var dic = [String : Any]()
        
        // 값 수정 ~~ 확인
        let symbol = bt.symbol.toString().components(separatedBy: [" "]).joined()
        let delta_rate = bt.delta_rate.toString()
        let last_volume = bt.last_volume.toString()
        let last_price = bt.last_price.toString()
        
        print("RT : wallet list last_price : \(last_price)")
        
        let intLast_volume = stringtoDouble(str: last_volume)
        
        //        dic.updateValue(symbol, forKey: "simbol")
        //        dic.updateValue(delta_rate, forKey: "updnRate" )
        //        dic.updateValue(String(intLast_volume!), forKey: "totalVol")
        
        print("RT : wallet list symbole : \(symbol)")
        
        var realcoinsimbol = ""
        
        if coinsimbol != "KDP" && coinsimbol != "KDMP" {
            realcoinsimbol = coinsimbol
        }else {
            realcoinsimbol = pointAddsimbol
        }
        
        for row in AppDelegate.walletListArray {
            
            let sym = row as [String : Any]
            
            let tempSymbole = sym["simbol"] as? String ?? ""
            
            print("RT : wallet list symbol : \(tempSymbole)")
            
            print("RT : wallet list symbols : \(symbol)")
            
            print("RT : wallet list test : \(symbol == tempSymbole)")
            
            if symbol == tempSymbole {
                
                if symbol == realcoinsimbol {
                    
                    self.lastprice = last_price
                    
                    let KRWValue = self.changeKRW(pri: last_price, qty: self.openQty, isKrw: true)
                    
                    let krwAttrStr = NSMutableAttributedString(string: KRWValue , attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14)])
                    
                    DispatchQueue.main.async {
                        
                        let exchangQtyValue = self.exchCoinQty.text ?? ""
                        let exchangKRWValue = self.changeKRW(pri: last_price, qty: exchangQtyValue, isKrw: false)
                        
                        self.exchCoinPrice.text = "\(last_price.insertComma) KRW"
                        self.exchCoinKRWValue.attributedText = krwAttrStr
                        self.exchKrw.text = exchangKRWValue.insertComma
                    }
                    
                }else if symbol == receiveSimbole  {
                    
                    DispatchQueue.main.async {
                        
                        let receQtyValue = self.receCoinQty.text ?? ""
                        let receKRWValue = self.changeKRW(pri: last_price, qty: receQtyValue, isKrw: false)
                        
                        self.receCoinPrice.text = "\(last_price.insertComma) KRW"
                        self.receKrw.text = receKRWValue.insertComma
                        
                    }
                    
                }
                
                num = index
                
                let coinname = sym["coinName"] as? String ?? ""
                
                print("RT coinname : \(coinname)")
                
                //                dic.updateValue(coinname, forKey: "coinName" )
                
                if let intLast_price = Int(last_price){
                    AppDelegate.walletListArray[index].updateValue(String(intLast_price), forKey: "lastPrice")
                }else {
                    let intLast_price = stringtoDouble(str: last_price)
                    AppDelegate.walletListArray[index].updateValue(String(intLast_price!), forKey: "lastPrice")
                }
                
                //                self.realTimeBool = true
                print("RT coinname : \(coinname)")
                print("RT index : \(index)")
                
            }
            
            index = index + 1
        }
        
        walletOtherSeting()
        
        DispatchQueue.main.async {
            self.exchangeCountAction()
        }
    }
    
    func exchangDetailRT(bt: NPriceTick) {
        
    }
    
    func trandeListRT(bt: NPriceInfo) {
        
    }
    
    func openListRT(bt: NOpenInfo) {
        
    }
    
    func krwChangeRT(value: Double) {
        
    }
    
    func setupNaviTitle(){
        let vi = UIView()
        
        let nTitle = UILabel(frame: CGRect(x: 0, y: 0, width: 400, height: 37))
        
        nTitle.textAlignment = .center
        nTitle.font = UIFont.boldSystemFont(ofSize: 15)
        nTitle.textColor = UIColor.DPTitleTextColor
        nTitle.text = "\(self.coinName)"
        nTitle.textAlignment = .left
        
        vi.addSubview(nTitle)
        
        self.navigationItem.titleView = vi
        
        self.navigationItem.titleView?.frame.size = CGSize(width: 400, height: 37)
        
        self.navigationController?.navigationBar.barTintColor =  UIColor.DPNaviBarTintColor
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = ""
        
    }
    
    func intToStringComma(data : String) -> String {
        
        var doubleData = stringtoDouble(str: data) ?? 0.0
        let intData = Int(doubleData.roundToPlaces(places: 0))
        
        let strData = "\(intData)"
        
        return strData.insertComma
        
    }
    
    //    //KDA 환전신청
    //    func sendTakeAction() {
    //
    //        self.activity.startAnimating()
    //
    //        let url = URL(string: "\(AppDelegate.url)/auth/walletSend")
    //
    //        var request = URLRequest(url: url!)
    //
    //        request.httpMethod = "POST"
    //
    //        let param = "uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&simbol=KDA&addr=\(self.ethwalletAddr)&waysType=M&privKey=\(AlertVO.shared.privKey1)"
    //
    //        print("wallet send param : \(param)")
    //        let paramData = param.data(using: .utf8)
    //
    //        request.httpBody = paramData
    //
    //        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
    //
    //        request.setValue(String(describing: paramData!.count), forHTTPHeaderField: "Content-Length")
    //
    //        //laucnhing
    //        let session = URLSession.shared
    //
    //        let task = session.dataTask(with:request as URLRequest) {data, response, error in
    //
    //            if error == nil {
    //
    //                print("buy sueccess")
    //                DispatchQueue.main.async {
    //
    //                    //activity.stopAnimating()
    //                    do {
    //                        let object = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
    //
    //                        guard let jsonObject = object else { return }
    //
    //                        //self.sendAddress.text = ""
    ////                        self.sendQty.text = ""
    ////                        self.gasLimitField.text = ""
    ////                        self.sendSpeedfield.text = ""
    ////                        self.sendAddress.text = ""
    ////                        self.isAddress = false
    ////                        self.isChecked = false
    ////                        self.sendfee = ""
    ////                        self.speedBtntap = 0
    ////                        self.manuBtn.titleLabel?.text = "선택"
    ////                        self.sendFees.text = "\(String(format: "%.8f", 0.0))"
    ////                        self.totalAmount.text = "\(String(format: "%.8f", 0.0))"
    ////                        self.isAddress = false
    ////
    //                        print("jsonobject : \(jsonObject)")
    //
    //                        let sendResult = jsonObject["returnYn"] as? String ?? "N"
    //
    //
    //                        print("send result : \(sendResult)")
    //
    //                        if sendResult == "N" {
    //
    //                            let msg = jsonObject["returnMsg"] as? String ?? ""
    //                            let alert = UIAlertController(title: "pop_wallet".localized, message: msg, preferredStyle: .alert)
    //                            alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: { (_) in
    //                            }))
    //
    //                            self.present(alert, animated: false, completion: nil)
    //
    //                        }else {
    //
    //                            let msg = jsonObject["returnMsg"] as? String ?? ""
    //                            let alert = UIAlertController(title: "pop_wallet".localized, message: msg, preferredStyle: .alert)
    //                            alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: { (_) in
    //                            }))
    //
    //                            self.present(alert, animated: false, completion: nil)
    //
    //                        }
    //
    //                        self.activity.stopAnimating()
    //
    //                    }catch{
    //                        print("cCaught an error:\(error)")
    //
    //                        let msg = "pop_wallet_send_not_reply".localized
    //                        let alert = UIAlertController(title: "pop_wallet".localized, message: msg, preferredStyle: .alert)
    //                        alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
    //                        self.present(alert, animated: false, completion: nil)
    //
    //                        self.activity.stopAnimating()
    //
    //                    }
    //                }
    //            }else{
    //                print("error:\(error)")
    //                let msg = "pop_wallet_send_not_reply".localized
    //                let alert = UIAlertController(title: "pop_wallet".localized, message: msg, preferredStyle: .alert)
    //                alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
    //
    //                self.present(alert, animated: false, completion: nil)
    //
    //                self.activity.stopAnimating()
    //
    //            }
    //
    //        }
    //
    //        task.resume()
    //
    //    }
    
    //    func ethwalletInfo() {
    //        let activity = UIActivityIndicatorView()
    //        activity.activityIndicatorViewStyle = .whiteLarge
    //        activity.color = UIColor.DPActivityColor
    //        activity.hidesWhenStopped = true
    //        activity.startAnimating()
    //
    //        let url = "\(AppDelegate.url)/auth/walletInfo?simbol=ETH&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)"
    //
    //        print(url)
    //        let amo = Alamofire.request(url)
    //        amo.responseJSON(completionHandler: { response in
    //
    //            print("buy result :\(response.result.value)")
    //
    //            if response.result.isSuccess == true {
    //
    //                var result = response.result.value as? [String : Any]
    //
    //                self.ethwalletAddr = (result!["wletAddr"] as? String) ?? ""
    //
    //            }
    //            activity.stopAnimating()
    //        })
    //    }
    
    func requstAction() {
        let exchangQtyfl = self.exchCoinQty.text ?? "0"
        let receQtyfl = self.receCoinQty.text ?? "0"
        
        let walletPk = AlertVO.shared.privKey1
        let pointPk = AlertVO.shared.pointAddprivKey
        
        let tmpRate = String(format: "%.8f", self.exRate)
        
        var tempurl = ""
        
        var param = ""
        
        if coinsimbol == "KDP" && !self.exchangeIsBool {
            tempurl = "\(AppDelegate.url)/auth/walletExchange"
            
            let tmepStr = "\(self.exchCoinQty.text ?? "0")".trimmingCharacters(in: [","])
            
            if receiveSimbole == "KDP" {
                
                param = "uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&fromSimbol=\(pointAddsimbol)&toSimbol=\(receiveSimbole)&qty=\(tmepStr)&rate=\(tmpRate)&waysType=M&privKey=\(pointPk ?? "")&exType=3"
                
            }else {
                
                param = "uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&fromSimbol=\(pointAddsimbol)&toSimbol=\(receiveSimbole)&qty=\(tmepStr)&rate=\(tmpRate)&waysType=M&privKey=\(pointPk ?? "")&exType=4"
                
            }
            
        } else if coinsimbol == "KDMP" && !self.exchangeIsBool {
            tempurl = "\(AppDelegate.url)/auth/walletExchange"
            
            let tmepStr = "\(self.exchCoinQty.text ?? "0")".trimmingCharacters(in: [","])
            
            if receiveSimbole == "KDMP" {
                param = "uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&fromSimbol=\(pointAddsimbol)&toSimbol=\(receiveSimbole)&qty=\(tmepStr)&rate=\(tmpRate)&waysType=M&privKey=\(pointPk ?? "")&exType=5"
                
                
                
            }else {
                param = "uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&fromSimbol=\(pointAddsimbol)&toSimbol=\(receiveSimbole)&qty=\(tmepStr)&rate=\(tmpRate)&waysType=M&privKey=\(pointPk ?? "")&exType=5"
                
            }
            
        }else if coinsimbol == "KDA" && !self.exchangeIsBool {
            
            print("testert kda")
            
            //            self.sendTakeAction()
            
            return
            
        }else {
            
            tempurl = "\(AppDelegate.url)/auth/walletExchange"
            
            var exTp = 4
            
            if coinsimbol == "KRW" {
                exTp = 1
            }else if coinsimbol == "KDP"{
                exTp = 2
            }else if coinsimbol == "KDMP"{
                exTp = 5
            }
            
            param = "uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&fromSimbol=\(coinsimbol)&toSimbol=\(receiveSimbole)&qty=\(self.exchCoinQty.text ?? "0")&rate=\(tmpRate)&waysType=M&privKey=\(walletPk ?? "")&exType=\(exTp)"
            
            
        }
        
        print("exchage param : \(param)")
        
        print("wallet param : \(param)")
        let url = URL(string: "\(tempurl)")
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        print("wallet exchange param : \(param)")
        print("wallet exchange point privkey : \(pointPk)")
        print("wallet exchange privkey : \(walletPk)")
        let paramData = param.data(using: .utf8)
        
        request.httpBody = paramData
        
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        request.setValue(String(describing: paramData!.count), forHTTPHeaderField: "Content-Length")
        
        
        self.activity.startAnimating()
        //laucnhing
        let session = URLSession.shared
        let task = session.dataTask(with:request as URLRequest) {data, response, error in
            
            if error == nil {
                
                DispatchQueue.main.async {
                    
                    do {
                        let object = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        
                        guard let jsonObject = object else { return }
                        print("inout result msg : \(jsonObject["returnMsg"] ?? "")")
                        
                        let msg = "\(jsonObject["returnMsg"] ?? "")"
                        
                        let alert = UIAlertController(title: "pop_wallet_exchange".localized, message: msg, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: { (_) in
                            //self.chatMoving()
                        }))
                        
                        self.present(alert, animated: false, completion: nil)
                        
                        self.exchCoinQty.text = ""
                        self.receCoinQty.text = ""
                        
                        self.activity.stopAnimating()
                    }catch{
                        
                        let msg = "pop_wallet_registert_not_reply".localized
                        let alert = UIAlertController(title: "pop_wallet_exchange".localized, message: msg, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .cancel, handler: nil))
                        self.present(alert, animated: false, completion: nil)
                        
                        self.activity.stopAnimating()
                    }
                    
                    self.view.endEditing(true)
                    
                }
                
            }else{
                print("error:\(error)")
                
                self.activity.stopAnimating()
            }
            
        }
        task.resume()
        
    }
    
}
