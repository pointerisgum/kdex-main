//
//  ExchangChartView.swift
//  KDex
//
//  Created by park heewon on 2018. 5. 30..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//
//

import UIKit
import Foundation
import WebKit

class ExchangChartView : UIViewController, WKUIDelegate, WKNavigationDelegate, WKScriptMessageHandler {

    var webView : WKWebView! //웹 뷰
    
    var coinSimbol : String? //코인 아이디
    
    var coinName : String? //코인 네임
    
    var activityIndicator : UIActivityIndicatorView = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initNextbtn()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        webViewSetup()
    }
    
    override func loadView() {
        super.loadView()
        
        webView = WKWebView()
        webView.backgroundColor = UIColor.DPViewBackgroundColor
        webView.translatesAutoresizingMaskIntoConstraints = false
        
        webView.uiDelegate = self
        webView.navigationDelegate = self
        
        self.view.addSubview( self.webView!)
        
    }
    
    func webViewSetup(){
        
        var tempSimbole = ""
        
        if (coinSimbol?.contains("_"))! {
            tempSimbole = coinSimbol ?? ""
        }else {
            tempSimbole = "\(coinSimbol ?? "")_KRW"
        }
        
        let myUrl = "\(AppDelegate.url)/mobile/chartFull?simbol=\(tempSimbole)&uid=\(AppDelegate.uid)"
        
        print("chart : \(myUrl)")
        
        let url = URL(string: myUrl)
        
        let request = URLRequest(url: url!)
        
        webView.load(request)
        
        webView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        webView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        webView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        webView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        
    }
    
    func initNextbtn(){
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.topItem?.title = "\(self.coinName!)(\(self.coinSimbol!.replacingOccurrences(of: "_", with: "/")))"
    }
    
    @available(iOS 8.0, *)
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping () -> Void) {
        
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .default, handler: { (action) in
            completionHandler()
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @available(iOS 8.0, *)
    func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping (Bool) -> Void) {
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .default, handler: { (action) in
            completionHandler(true)
        }))
        alertController.addAction(UIAlertAction(title: "common_no_btn".localized, style: .default, handler: { (action) in
            completionHandler(false)
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @available(iOS 8.0, *)
    func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping (String?) -> Void) {
        let alertController = UIAlertController(title: "", message: prompt, preferredStyle: .alert)
        alertController.addTextField { (textField) in
            textField.text = defaultText
        }
        
        alertController.addAction(UIAlertAction(title: "common_yes_btn".localized, style: .default, handler: { (action) in
            if let text = alertController.textFields?.first?.text {
                completionHandler(text)
            } else {
                completionHandler(defaultText)
            }
        }))
        
        alertController.addAction(UIAlertAction(title: "common_no_btn".localized, style: .default, handler: { (action) in
            completionHandler(nil)
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @available(iOS 8.0, *)
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
        activityIndicator.frame = CGRect(x: (self.view.frame.width - 100) / 2 , y: (self.view.frame.height - 100) / 2, width: 100, height: 100)
        activityIndicator.style = .whiteLarge
        activityIndicator.color = UIColor(rgb: 0x00A6ED)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
    }
    
    @available(iOS 8.0, *)
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.activityIndicator.removeFromSuperview()
    }
    
    public func webViewWebContentProcessDidTerminate(_ webView: WKWebView) {
        // 중복적으로 리로드가 일어나지 않도록 처리 필요.
        self.webView.reload()
    }
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
         //script에서 ios zhem
    }
    
}
