//
//  AddressListController.swift
//  KDex
//
//  Created by park heewon on 2018. 6. 8..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

class AddressListController : UIViewController, UITableViewDelegate, UITableViewDataSource {

    var delegate : WalletSendViewController?
    
    var coinSimbol = ""
    
    var coinType = ""
    
    var sendAddr = ""
    
    var isAddress = false
    
    var tempJson = [[String : Any]]()
    
    @IBOutlet var contentView: UIView!
    
    @IBOutlet var addresssTableView: UITableView!
    
    @IBOutlet var textfieldContentView: UIView!
    
    @IBOutlet var addAddressBtn: UIButton!
    
    @IBOutlet var closeBtn: UIButton!
    
    @IBOutlet var addressName: UITextField!
    
    @IBOutlet var address: UITextField!
    
    @IBOutlet var saveBtn: UIButton!
    
    @IBAction func saveAction(_ sender: Any) {
        
        let name = addressName.text ?? ""
        let addr = address.text ?? ""
        
        self.view.endEditing(true)
        
        if isAddress {
            self.addAddress(name: name, address: addr)
            
            self.addressName.text = ""
            self.address.text = ""
            
        }else if name == "" {
            showToast(message: "send_adderess_book_blank_inform".localized, textcolor: UIColor.white, frame: CGRect(x: self.view.frame.size.width/2 - 100, y: self.view.frame.size.height - 100  , width: 200, height: 35), time: 1, animation: true)
        }else if addr == "" {
            showToast(message: "send_adderess_book_blank_inform".localized, textcolor: UIColor.white, frame: CGRect(x: self.view.frame.size.width/2 - 100, y: self.view.frame.size.height - 100  , width: 200, height: 35), time: 1, animation: true)
        }else if isAddress != true {
            showToast(message: "send_adderess_book_not_match_inform".localized, textcolor: UIColor.white, frame: CGRect(x: self.view.frame.size.width/2 - 100, y: self.view.frame.size.height - 100  , width: 200, height: 35), time: 1, animation: true)
        }
        
    }
    
    @IBAction func addAddressAction(_ sender: Any) {
        
        print("self heigth : \(self.contentView.frame.height)")
        
        let height = self.contentView.frame.height
        
        contentView.translatesAutoresizingMaskIntoConstraints = true
        //84
        
        address.text = self.sendAddr
        
        if height == 292 {
            textfieldContentView.isHidden = true
            UIView.animate(withDuration: 0.25) {
                self.contentView.frame.size.height = 208
            }
        }else {
            textfieldContentView.isHidden = false
            self.textfieldContentView.add(border: .top, color: UIColor.DPLineBgColor, width: 1)
            UIView.animate(withDuration: 0.25) {
                self.contentView.frame.size.height = 292
            }
        }
        
    }
    
    
    @IBAction func closeAction(_ sender: Any) {
        
        self.closeAnimate()

    }
    
    let footerview : UIView = {
        let vi = UIView()
        vi.backgroundColor = UIColor.white
        return vi
    }()
    
    let footerLabel : UILabel = {
        let lb = UILabel()
        lb.text = "send_adderess_book_no_address".localized
        lb.textColor = UIColor.DPmainTextColor
        lb.font = UIFont.systemFont(ofSize: 14)
        lb.textAlignment = .center
        lb.translatesAutoresizingMaskIntoConstraints = false
        
        return lb
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("address viewdidload")
        
        addresssTableView.delegate = self
        addresssTableView.dataSource = self
        
        contentView.backgroundColor = UIColor.DPPopupBgColor
        textfieldContentView.backgroundColor = UIColor.clear
        
        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
        
        let nib = UINib(nibName: "AddressCellNib", bundle: nil)
        
        addresssTableView.register(nib, forCellReuseIdentifier: "addressCell")
        addresssTableView.layoutMargins = UIEdgeInsets.zero
        addresssTableView.separatorInset = UIEdgeInsets.zero
        addresssTableView.backgroundColor = UIColor.white

        
        contentView.layer.cornerRadius = 10
        contentView.clipsToBounds = true
        
        footerview.frame = CGRect(x: 0, y: 0, width: self.contentView.frame.width, height: 100)
        
        footerview.addSubview(footerLabel)
        
        self.footerLabel.topAnchor.constraint(equalTo: footerview.topAnchor).isActive = true
        self.footerLabel.leadingAnchor.constraint(equalTo: footerview.leadingAnchor).isActive = true
        self.footerLabel.rightAnchor.constraint(equalTo: footerview.rightAnchor).isActive = true
        self.footerLabel.bottomAnchor.constraint(equalTo: footerview.bottomAnchor).isActive = true
        
        addresssTableView.add(border: .top, color: UIColor.DPLineBgColor, width: 1)
        addresssTableView.add(border: .bottom, color: UIColor.DPLineBgColor, width: 1)

        addAddressBtn.layer.cornerRadius = 2
        addAddressBtn.clipsToBounds = true
        addAddressBtn.layer.borderColor = UIColor.DPGreenColor.cgColor
        addAddressBtn.layer.borderWidth = 1
        addAddressBtn.setTitleColor(UIColor.DPGreenColor, for: .normal)
        
        closeBtn.layer.cornerRadius = 2
        closeBtn.clipsToBounds = true
        
        closeBtn.layer.borderColor = UIColor.DPmainTextColor.cgColor
        closeBtn.layer.borderWidth = 1
        closeBtn.setTitleColor(UIColor.DPmainTextColor, for: .normal)
        
        
        saveBtn.layer.cornerRadius = 2
        saveBtn.clipsToBounds = true
        saveBtn.layer.borderColor = UIColor.DPGreenColor.cgColor
        saveBtn.layer.borderWidth = 1
        saveBtn.setTitleColor(UIColor.DPGreenColor, for: .normal)
        
        address.addTarget(self, action: #selector(textfieldsendChageEvent), for: .editingChanged)
        
        textfieldContentView.isHidden = true
        
        addressList()
        
        self.showAnimate()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        print("address viewwillapppear")
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.heightAnchor.constraint(equalToConstant: 208).isActive = true
        
        //셀프 사이징
        addresssTableView.estimatedRowHeight = 60
        addresssTableView.rowHeight = UITableView.automaticDimension
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.tempJson.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let temp = self.tempJson[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "addressCell") as? AddressCell
        cell?.backgroundColor = UIColor.white
        
        cell?.delete = self
        cell?.address.numberOfLines = 0
        cell?.address.text = temp["addr"] as? String ?? ""
        cell?.addressName.text = temp["addrName"] as? String ?? ""
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let addr = self.tempJson[indexPath.row]
        
        delegate?.sendAddress.text = addr["addr"] as? String ?? ""
        delegate?.addressfind()
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return footerview
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 100
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func addressList() {
        
        let url = "\(AppDelegate.url)/auth/getAddrList?simbol=\(self.coinSimbol)&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)"
        
        print("wallet inputAddr : \(url)")
        
        let amo = Alamofire.request(url)
        amo.responseJSON(completionHandler: { response in
            
            self.tempJson = (response.result.value as? [[String: Any]])!
            
            if self.tempJson.count == 0 {
                self.footerLabel.isHidden = false
                self.footerview.isHidden = false
                
            }else {
                self.footerLabel.isHidden = true
                self.footerview.isHidden = true
        
            }
            
            self.addresssTableView.reloadData()
            
        })
        
    }
    
    func showAnimate() {
        
        self.view.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.25) {
            
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform.init(scaleX: 1.0, y: 1.0)
            
        }
        
    }
    
    func closeAnimate() {
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0
        }) { (finished: Bool) in
            if(finished)
            {
                self.view.removeFromSuperview()
            }
        }
        
    }
    
    private func addAddress(name: String, address: String) {
        
        let url = "\(AppDelegate.url)/auth/inputAddr?simbol=\(coinSimbol)&uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&inputAddrName=\(name)&inputAddr=\(address)"
        
        print("wallet inputAddr : \(url)")
        
        let amo = Alamofire.request(url)
        
        amo.responseJSON(completionHandler: { response in
            
            guard let result = response.result.value else {
                return
            }
            
            print("wallet addaddress result : \(result)")
            
            self.showToast(message: "pop_wallet_address_save".localized, textcolor: UIColor.white, frame: CGRect(x: self.view.frame.size.width/2 - 100, y: self.view.frame.size.height - 100  , width: 200, height: 35), time: 1, animation: true)
            
            self.addressList()
        })
        
    }
    
    @objc func textfieldsendChageEvent(){
        let addressCount = address.text?.count ?? 0
        
        print("wallet shouldChangeCharactersIn count : \(address.text)")
        
        if addressCount > 20 {
            self.addressfind()
        }else {
            self.address.textColor = UIColor.DPPlusTextColor
        }
        
    }
    
    func addressfind(){
//        let url = "\(AppDelegate.url)/auth/checkWalletAddr?uid=\(AppDelegate.uid)&sessionid=\(AppDelegate.sessionid)&simbol=\(coinSimbol)&addr=\(address.text ?? "")"
//
        var url = ""
        
        if coinType == "2" {
            "\(AppDelegate.url)/auth/checkWalletAddr?uid=\(AppDelegate.uid)&sessionid=\(AppDelegate.sessionid)&simbol=ETH&addr=\(address.text ?? "")"
            print("url : \(url)")
            
        }else {
            "\(AppDelegate.url)/auth/checkWalletAddr?uid=\(AppDelegate.uid)&sessionid=\(AppDelegate.sessionid)&simbol=\(coinSimbol)&addr=\(address.text ?? "")"
            print("url : \(url)")
        }
        
        print("url : \(url)")
        let amo = Alamofire.request(url)
        amo.responseString { response in
            guard let result = response.result.value else{
                return
            }
            print("wallet result : \(result)")
            
            if result == "Y" {
                self.isAddress = true
                self.address.textColor = UIColor.DPMinTextColor
            }else {
                self.isAddress = false
                self.address.textColor = UIColor.DPPlusTextColor
            }
        }
    }
    
}
