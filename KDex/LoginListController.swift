//
//  LoginListController.swift
//  DEXnPAY
//
//  Created by park heewon on 2018. 3. 28..
//  Copyright © 2018년 hanbiteni. All rights reserved.
//

import UIKit
import Foundation
import Alamofire


class LoginListController: UIViewController, UITableViewDelegate, UITableViewDataSource, CalendarProtocol {

//    let headerView : UIView = {
//        let view = UIView()
//
//        return view
//    }()
    
    var loginArray = [[String : Any]]()
    
    let footerView : UIView = {
        let view = UIView()
        
        return view
    }()
    
//    let tb_dateLabel : UILabel = {
//        let lb = UILabel()
//        lb.text = "일시"
//        lb.textColor = UIColor.DPmainTextColor
//        lb.textAlignment = .center
//        lb.baselineAdjustment = .alignCenters
//        lb.translatesAutoresizingMaskIntoConstraints = false
//        lb.font = UIFont.boldSystemFont(ofSize: 14)
//        return lb
//    }()
//
//    let tb_IpLabel : UILabel = {
//        let lb = UILabel()
//        lb.text = "로그인IP"
//        lb.textColor = UIColor.DPmainTextColor
//        lb.textAlignment = .center
//        lb.baselineAdjustment = .alignCenters
//        lb.translatesAutoresizingMaskIntoConstraints = false
//        lb.font = UIFont.boldSystemFont(ofSize: 14)
//        return lb
//    }()
//
//    let tb_OsLabel : UILabel = {
//        let lb = UILabel()
//        lb.text = "운영체제/브라우저"
//        lb.textColor = UIColor.DPmainTextColor
//        lb.textAlignment = .center
//        lb.baselineAdjustment = .alignCenters
//        lb.translatesAutoresizingMaskIntoConstraints = false
//        lb.font = UIFont.boldSystemFont(ofSize: 14)
//
//        return lb
//    }()
    
    /////////////
    
    fileprivate let gregorian = Calendar(identifier: .gregorian)
    
    fileprivate let formatter : DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    var openBool = false
    
    var selectBtnTag = 0 {
        didSet {
            print("selectBtn tag : \(selectBtnTag)")
        }
    }
    
    //검색 높이 조절 레이아웃
    var height = NSLayoutConstraint()
    
    @IBOutlet var searchRangLabel: UILabel!
    
    @IBOutlet var startDateLabel: UILabel!
    
    @IBOutlet var endDateLabel: UILabel!
    
    @IBOutlet var searchViContent: UIView!
    
    @IBOutlet var searchOpenBtn: UIButton!
    
    @IBOutlet var termBtnSt: UIStackView!
    
    @IBOutlet var startDateVi: UIView!
    
    @IBOutlet var endDateVi: UIView!
    
    @IBOutlet var searchBtn: UIButton!
    
    @IBOutlet var andLabel: UILabel!
    
    @IBOutlet var startVi: UIView!
    
    @IBOutlet var endVi: UIView!
    
    @IBOutlet var selectBtns: [UIButton]!
    
    @IBAction func selectBtnsAction(_ sender: UIButton) {
        print("sender tag : \(sender.tag)")
        
        selectBtnTag = sender.tag
        
        selectBtns.forEach { (btn) in
            if btn.tag == sender.tag {
                
                btn.setTitleColor(UIColor.white, for: .normal)
                btn.backgroundColor = UIColor(rgb: 0x00b9b2)
                
            }else {
                
                btn.setTitleColor(UIColor(rgb: 0x666666), for: .normal)
                btn.backgroundColor = UIColor(rgb: 0xeaeaea)
                
            }
            
        }
        
        let endDate = endDateLabel.text ?? ""
        let tempEndDate = calendarDate(str: endDate)
        
        switch sender.tag {
        case 0:
            endDateLabel.text = makeDate(date: Date())
            startDateLabel.text = makeDate(date: Date())
            searchRangLabel.text = "\(startDateLabel.text ?? "") ~ \(endDateLabel.text ?? "")"
            self.loginList()
        case 1:
            let tempDate = self.gregorian.date(byAdding: .day, value: -7, to:tempEndDate) ?? Date()
            startDateLabel.text = makeDate(date: tempDate)
            searchRangLabel.text = "\(startDateLabel.text ?? "") ~ \(endDateLabel.text ?? "")"
            self.loginList()
        case 2:
            let tempDate = self.gregorian.date(byAdding: .month, value: -1, to:tempEndDate) ?? Date()
            startDateLabel.text = makeDate(date: tempDate)
            searchRangLabel.text = "\(startDateLabel.text ?? "") ~ \(endDateLabel.text ?? "")"
            self.loginList()
        case 3:
            let tempDate = self.gregorian.date(byAdding: .month, value: -3, to:tempEndDate) ?? Date()
            startDateLabel.text = makeDate(date: tempDate)
            searchRangLabel.text = "\(startDateLabel.text ?? "") ~ \(endDateLabel.text ?? "")"
            self.loginList()
        case 4:
            let tempDate = self.gregorian.date(byAdding: .month, value: -6, to:tempEndDate) ?? Date()
            startDateLabel.text = makeDate(date: tempDate)
            searchRangLabel.text = "\(startDateLabel.text ?? "") ~ \(endDateLabel.text ?? "")"
            self.loginList()
        case 5:
            let tempDate = self.gregorian.date(byAdding: .year, value: -1, to:tempEndDate) ?? Date()
            startDateLabel.text = makeDate(date: tempDate)
            searchRangLabel.text = "\(startDateLabel.text ?? "") ~ \(endDateLabel.text ?? "")"
            self.loginList()
        default:
            print("default")
        }
        
    }
    
    @IBAction func selectCalendaAction(_ sender: UIButton) {
        
        let popOverVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "calendarPop") as! CalendaController
        popOverVc.delegate = self
        
        popOverVc.startDate = self.calendarDate(str: startDateLabel.text ?? "")
        popOverVc.endDate = self.calendarDate(str: endDateLabel.text ?? "")
        popOverVc.startCompanyDate = self.calendarDate(str: "2010.01.01")
        
        if sender.tag == 11 {
            popOverVc.type = .start
        }else {
            popOverVc.type = .end
        }
        
        self.addChild(popOverVc)
        popOverVc.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.width)
        
        self.view.addSubview(popOverVc.view)
        popOverVc.didMove(toParent: self)
        
    }
    
    @IBAction func dropBtnAction(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "tester", message: nil, preferredStyle: .actionSheet)
        
        alert.addAction((UIAlertAction(title: "cancel", style: .cancel, handler: nil)))
        
        self.present(alert, animated: false, completion: nil)
        
    }
    
    /////////////
    
    @IBAction func searchOpenBtnAction(_ sender: Any) {
        
        if openBool {
            
            openBool = false
            
            NSLayoutConstraint.deactivate([self.height])
            self.height.constant = 1
            NSLayoutConstraint.activate([self.height])
            
            
            termBtnSt.isHidden = true
            startDateVi.isHidden = true
            endDateVi.isHidden = true
            searchBtn.isHidden = true
            andLabel.isHidden = true
            
            
        }else {
            
            openBool = true
            
            NSLayoutConstraint.deactivate([self.height])
            self.height.constant = 100
            NSLayoutConstraint.activate([self.height])
            
            termBtnSt.isHidden = false
            startDateVi.isHidden = false
            endDateVi.isHidden = false
            searchBtn.isHidden = false
            andLabel.isHidden = false
            
        }
        
    }
    
    @IBAction func searchBtnAction(_ sender: Any) {
        
        selectBtns.forEach { (btn) in
            
            btn.setTitleColor(UIColor(rgb: 0x666666), for: .normal)
            btn.backgroundColor = UIColor(rgb: 0xeaeaea)
            
        }
        
        self.loginList()
    }
    
    /////////////
    
    @IBOutlet var LoginTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.black
        self.view.addSubview(LoginTable)
        
        self.navigationController?.navigationBar.barTintColor = UIColor.DPNaviBarTintColor
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = "main_myinfo".localized
        self.navigationController?.addShadowToBar(vi: self)
        
        LoginTable.delegate = self
        LoginTable.dataSource = self
        LoginTable.layoutMargins = UIEdgeInsets.zero
        LoginTable.separatorInset = UIEdgeInsets.zero
        
        
        ///////////
        
        endDateLabel.text = makeDate(date: Date())
        startDateLabel.text = makeDate(date: Date())
        
        searchRangLabel.text = "\(startDateLabel.text ?? "") ~ \(endDateLabel.text ?? "")"
        
        selectBtns.forEach { (btn) in
            if btn.tag == 0 {
                
                btn.setTitleColor(UIColor.white, for: .normal)
                btn.backgroundColor = UIColor(rgb: 0x00b9b2)
                
            }else {
                
                btn.setTitleColor(UIColor(rgb: 0x666666), for: .normal)
                btn.backgroundColor = UIColor(rgb: 0xeaeaea)
                
            }
        }
        
        [startVi, endVi].forEach {
            $0?.layer.borderColor = UIColor(rgb: 0xeaeaea).cgColor
            $0?.layer.borderWidth = 1
        }
        
        searchViContent.translatesAutoresizingMaskIntoConstraints = false
        
        if openBool {
            height = searchViContent.heightAnchor.constraint(equalToConstant: 128)
            NSLayoutConstraint.activate([self.height])
            
            termBtnSt.isHidden = false
            startDateVi.isHidden = false
            endDateVi.isHidden = false
            searchBtn.isHidden = false
            andLabel.isHidden = false
            
            
            self.view.bringSubviewToFront(searchViContent)
            
        }else {
            
            height = searchViContent.heightAnchor.constraint(equalToConstant: 1)
            NSLayoutConstraint.activate([self.height])
            
            termBtnSt.isHidden = true
            startDateVi.isHidden = true
            endDateVi.isHidden = true
            searchBtn.isHidden = true
            andLabel.isHidden = true
            
            
        }
        
        
        ///////////
        
//        self.headerView.backgroundColor = UIColor.DPtableHeaderBgColor
//
//        [tb_dateLabel, tb_IpLabel, tb_OsLabel].forEach{headerView.addSubview($0)}
//
//        tb_dateLabel.leftAnchor.constraint(equalTo: headerView.leftAnchor, constant: 15).isActive = true
//        tb_dateLabel.widthAnchor.constraint(equalToConstant: 50).isActive = true
//        tb_dateLabel.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 10).isActive = true
//
//        tb_OsLabel.leftAnchor.constraint(equalTo: tb_dateLabel.rightAnchor, constant: 20).isActive = true
//        tb_OsLabel.widthAnchor.constraint(equalToConstant: 120).isActive = true
//        tb_OsLabel.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 10).isActive = true
//
//        tb_IpLabel.rightAnchor.constraint(equalTo: headerView.rightAnchor, constant: -50).isActive = true
//        tb_IpLabel.widthAnchor.constraint(equalToConstant: 60).isActive = true
//        tb_IpLabel.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 10).isActive = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        tableVi.separatorInset = UIEdgeInsets.zero
//        tableVi.layoutMargins = UIEdgeInsets.zero
        
        loginList()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.loginArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let value = loginArray[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "loginCell", for: indexPath) as? LoginCell
        cell?.layoutMargins = UIEdgeInsets.zero
        
        cell?.deviceType.text = value["deviceNm"] as? String ?? ""
        cell?.ipaddress.text = value["pcAddr"] as? String ?? ""
        cell?.connectDate.text = self.makeTime(date: value["connTm"] as? String ?? "")
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 90
    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        return self.headerView
//    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 40
//    }
//
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return self.footerView
    }
    
    func loginList() {
        
        //activity.startAnimating()
        
        let tempStartDate = formatter.date(from: self.startDateLabel.text ?? "") ?? Date()
        let tempEndDate = formatter.date(from: self.endDateLabel.text ?? "") ?? Date()
        // 데이터 포맷터
        let dateFormatter = DateFormatter()
        // 한국 Locale
        dateFormatter.locale = Locale(identifier: "ko_KR")
        dateFormatter.dateFormat = "yyyyMMdd"
        let startDateStr = dateFormatter.string(from: tempStartDate)
        let endDateStr = dateFormatter.string(from: tempEndDate)
        
        
        let url = "\(AppDelegate.url)auth/getAccessLog?uid=\(AppDelegate.uid)&sessionId=\(AppDelegate.sessionid)&conndt1=\(startDateStr)&conndt2=\(endDateStr)"
        
        print("device url \(url)")
        
        let amo = Alamofire.request(url)
        amo.responseJSON(completionHandler: { response in
            
            guard let tempArray = response.result.value as? [[String : Any]] else {
                return
            }
            
            self.loginArray = tempArray
            
            self.LoginTable.reloadData()
            
        })
    }
    
    //날짜 형식 포멧
    func makeTime(date : String) -> String {
        
        let tempStr = date.substring(range: NSRange(location: 0, length: 14))
        
        let df : DateFormatter = DateFormatter()
        df.dateFormat = "yyyyMMddHHmmss"
        
        guard let formData = df.date(from: tempStr) else {
            return "0.0.0 00:00"
        }
        
        let date_time : NSDate = formData as NSDate
        
        df.dateFormat = "yyyy. MM. dd. HH:mm:ss"
        
        let newDate : String = df.string(from: date_time as Date) as String
        
        return newDate
    }
    
    func makeDate(date : Date) -> String {
        
        let df : DateFormatter = DateFormatter()
        df.dateFormat = "yyyy.MM.dd"
        
        let strDate = df.string(from: date) as String
        
        return strDate
    }
    
    func calendarDate(str : String) -> Date {
        
        var strDate = str
        
        strDate.replace(originalString: ".", withString: "-")
        
        let tempdate = formatter.date(from: strDate) ?? Date()
        
        return tempdate
        
    }
    
    func dateLabelEdit(date: Date, type: calendarType) {
        
        if type == .start {
            startDateLabel.text = makeDate(date: date)
            searchRangLabel.text = "\(makeDate(date: date)) ~ \(endDateLabel.text ?? "")"
        }else {
            endDateLabel.text = makeDate(date: date)
            searchRangLabel.text = "\(startDateLabel.text ?? "") ~ \(makeDate(date: date))"
        }
        
    }
    
}
